#ifndef OPTIONS_H
#define OPTIONS_H

#include "eliproto.h"

extern long Options[];

extern void InitOptions ELI_ARG((void));
/* Load options from a file
 *   On exit-
 *     Options is a (possibly empty) set containing the option characters
 ***/

#endif
