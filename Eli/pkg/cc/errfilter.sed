# Error formats:
# cc:  "fred.c", line 9: an error message
# gcc: fred.c:9: an error message
# CC:  xx Error(s) detected
# CC:  xx Warning(s) detected
# Remove extra gcc messages,e.g.:
# gcc2.4.5:	/^In file included from .*:$/d
# gcc2.5.7:	/^In file included from .*,$/,/from .*:[0-9][0-9]*:$/d
# cc: SC3.0 15 Dec 1993: identifier redeclared
# cc: SC* license messages for SPARCompiler

/^In file included from .*/d
/^  *from .*/d

s;^cc: ;;
s;^cpp: ;;
/^\([^:]*\): *\([0-9]*\): *\([0-9]*\):/{
  s/^\([^:]*\): *\([0-9]*\): *\([0-9]*\):/\1 \2 \3/p
  d
}
/^\([^:]*\): *\([0-9]*\):/{
  s/^\([^:]*\): *\([0-9]*\):/\1 \2 0/p
  d
}
/^"\([^"]*\)". line \([0-9]*\):/{
  s/^"\([^"]*\)". line \([0-9]*\):/\1 \2 0/p
  d
}
s/^\([^ ]*\) \([^ ]*\) 0\([0-9]*\):/\1 \2 \3/p
