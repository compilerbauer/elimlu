
#ifndef _VISITPROCS_H
#define _VISITPROCS_H

#include "HEAD.h"
#include "node.h"
#include "treecon.h"

#include "eliproto.h"


extern void LIGA_ATTREVAL ELI_ARG((NODEPTR));
extern void _VS0Empty ELI_ARG((NODEPTR _currn));
extern void _VS1LST_BottomUpRulerule_39 ELI_ARG((_TPPLST_BottomUpRulerule_39 _currn));
extern void _VS2LST_BottomUpRulerule_39 ELI_ARG((_TPPLST_BottomUpRulerule_39 _currn));
extern void _VS3LST_BottomUpRulerule_39 ELI_ARG((_TPPLST_BottomUpRulerule_39 _currn));
extern void _VS4LST_BottomUpRulerule_39 ELI_ARG((_TPPLST_BottomUpRulerule_39 _currn));
extern void _VS5LST_BottomUpRulerule_39 ELI_ARG((_TPPLST_BottomUpRulerule_39 _currn));
extern void _VS6LST_BottomUpRulerule_39 ELI_ARG((_TPPLST_BottomUpRulerule_39 _currn));
extern void _VS7LST_BottomUpRulerule_39 ELI_ARG((_TPPLST_BottomUpRulerule_39 _currn));
extern void _VS8LST_BottomUpRulerule_39 ELI_ARG((_TPPLST_BottomUpRulerule_39 _currn));
extern void _VS1LST_MapChainsrule_39 ELI_ARG((_TPPLST_MapChainsrule_39 _currn));
extern void _VS2LST_MapChainsrule_39 ELI_ARG((_TPPLST_MapChainsrule_39 _currn));
#define _VS3LST_MapChainsrule_39 _VS3LST_BottomUpRulerule_39

#define _VS4LST_MapChainsrule_39 _VS4LST_BottomUpRulerule_39

#define _VS5LST_MapChainsrule_39 _VS5LST_BottomUpRulerule_39

extern void _VS6LST_MapChainsrule_39 ELI_ARG((_TPPLST_MapChainsrule_39 _currn));
extern void _VS7LST_MapChainsrule_39 ELI_ARG((_TPPLST_MapChainsrule_39 _currn));
extern void _VS8LST_MapChainsrule_39 ELI_ARG((_TPPLST_MapChainsrule_39 _currn));
#define _VS1LST_MapRulerule_39 _VS1LST_MapChainsrule_39

#define _VS2LST_MapRulerule_39 _VS2LST_MapChainsrule_39

extern void _VS3LST_MapRulerule_39 ELI_ARG((_TPPLST_MapRulerule_39 _currn));
#define _VS4LST_MapRulerule_39 _VS4LST_BottomUpRulerule_39

#define _VS5LST_MapRulerule_39 _VS5LST_BottomUpRulerule_39

extern void _VS6LST_MapRulerule_39 ELI_ARG((_TPPLST_MapRulerule_39 _currn));
extern void _VS7LST_MapRulerule_39 ELI_ARG((_TPPLST_MapRulerule_39 _currn));
extern void _VS8LST_MapRulerule_39 ELI_ARG((_TPPLST_MapRulerule_39 _currn));
#define _VS1LST_MapSymbolrule_39 _VS1LST_MapChainsrule_39

extern void _VS2LST_MapSymbolrule_39 ELI_ARG((_TPPLST_MapSymbolrule_39 _currn));
#define _VS3LST_MapSymbolrule_39 _VS3LST_BottomUpRulerule_39

#define _VS4LST_MapSymbolrule_39 _VS4LST_BottomUpRulerule_39

#define _VS5LST_MapSymbolrule_39 _VS5LST_BottomUpRulerule_39

extern void _VS6LST_MapSymbolrule_39 ELI_ARG((_TPPLST_MapSymbolrule_39 _currn));
extern void _VS7LST_MapSymbolrule_39 ELI_ARG((_TPPLST_MapSymbolrule_39 _currn));
extern void _VS8LST_MapSymbolrule_39 ELI_ARG((_TPPLST_MapSymbolrule_39 _currn));
#define _VS1LST_AbsProdrule_39 _VS1LST_MapChainsrule_39

#define _VS2LST_AbsProdrule_39 _VS2LST_MapSymbolrule_39

#define _VS3LST_AbsProdrule_39 _VS3LST_BottomUpRulerule_39

extern void _VS4LST_AbsProdrule_39 ELI_ARG((_TPPLST_AbsProdrule_39 _currn));
#define _VS5LST_AbsProdrule_39 _VS5LST_BottomUpRulerule_39

extern void _VS6LST_AbsProdrule_39 ELI_ARG((_TPPLST_AbsProdrule_39 _currn));
extern void _VS7LST_AbsProdrule_39 ELI_ARG((_TPPLST_AbsProdrule_39 _currn));
extern void _VS8LST_AbsProdrule_39 ELI_ARG((_TPPLST_AbsProdrule_39 _currn));
#define _VS1LST_ConProdrule_39 _VS1LST_MapChainsrule_39

#define _VS2LST_ConProdrule_39 _VS2LST_MapChainsrule_39

#define _VS3LST_ConProdrule_39 _VS3LST_BottomUpRulerule_39

#define _VS4LST_ConProdrule_39 _VS4LST_BottomUpRulerule_39

extern void _VS5LST_ConProdrule_39 ELI_ARG((_TPPLST_ConProdrule_39 _currn));
extern void _VS6LST_ConProdrule_39 ELI_ARG((_TPPLST_ConProdrule_39 _currn));
extern void _VS7LST_ConProdrule_39 ELI_ARG((_TPPLST_ConProdrule_39 _currn));
extern void _VS8LST_ConProdrule_39 ELI_ARG((_TPPLST_ConProdrule_39 _currn));
#define _VS1LST_0rule_39 _VS0Empty

#define _VS2LST_0rule_39 _VS0Empty

#define _VS3LST_0rule_39 _VS0Empty

#define _VS4LST_0rule_39 _VS0Empty

#define _VS5LST_0rule_39 _VS0Empty

extern void _VS6LST_0rule_39 ELI_ARG((_TPPLST_0rule_39 _currn));
extern void _VS7LST_0rule_39 ELI_ARG((_TPPLST_0rule_39 _currn));
extern void _VS8LST_0rule_39 ELI_ARG((_TPPLST_0rule_39 _currn));
extern void _VS1LST_ConElementrule_38 ELI_ARG((_TPPLST_ConElementrule_38 _currn));
extern void _VS2LST_ConElementrule_38 ELI_ARG((_TPPLST_ConElementrule_38 _currn));
extern void _VS3LST_ConElementrule_38 ELI_ARG((_TPPLST_ConElementrule_38 _currn));
extern void _VS4LST_ConElementrule_38 ELI_ARG((_TPPLST_ConElementrule_38 _currn));
extern void _VS5LST_ConElementrule_38 ELI_ARG((_TPPLST_ConElementrule_38 _currn));
extern void _VS1LST_0rule_38 ELI_ARG((_TPPLST_0rule_38 _currn));
#define _VS2LST_0rule_38 _VS0Empty

#define _VS3LST_0rule_38 _VS0Empty

extern void _VS4LST_0rule_38 ELI_ARG((_TPPLST_0rule_38 _currn));
extern void _VS5LST_0rule_38 ELI_ARG((_TPPLST_0rule_38 _currn));
extern void _VS1LST_AbsElementrule_37 ELI_ARG((_TPPLST_AbsElementrule_37 _currn));
extern void _VS2LST_AbsElementrule_37 ELI_ARG((_TPPLST_AbsElementrule_37 _currn));
extern void _VS1LST_0rule_37 ELI_ARG((_TPPLST_0rule_37 _currn));
extern void _VS2LST_0rule_37 ELI_ARG((_TPPLST_0rule_37 _currn));
#define _VS1LST_AbsAltrule_36 _VS1LST_MapChainsrule_39

#define _VS1LST_0rule_36 _VS0Empty

#define _VS1LST_MapMemberrule_35 _VS1LST_MapChainsrule_39

#define _VS2LST_MapMemberrule_35 _VS2LST_MapSymbolrule_39

#define _VS1LST_0rule_35 _VS0Empty

#define _VS2LST_0rule_35 _VS0Empty

#define _VS1LST_MapElementrule_34 _VS1LST_MapChainsrule_39

#define _VS2LST_MapElementrule_34 _VS2LST_MapSymbolrule_39

#define _VS1LST_0rule_34 _VS0Empty

#define _VS2LST_0rule_34 _VS0Empty

#define _VS1LST_MapTextrule_33 _VS1LST_MapChainsrule_39

extern void _VS2LST_MapTextrule_33 ELI_ARG((_TPPLST_MapTextrule_33 _currn));
#define _VS1LST_MapPositionrule_33 _VS1LST_BottomUpRulerule_39

extern void _VS2LST_MapPositionrule_33 ELI_ARG((_TPPLST_MapPositionrule_33 _currn));
#define _VS1LST_0rule_33 _VS0Empty

extern void _VS2LST_0rule_33 ELI_ARG((_TPPLST_0rule_33 _currn));
extern void _VS1rule_1 ELI_ARG((_TPPrule_1 _currn));
extern void _VS1rule_2 ELI_ARG((_TPPrule_2 _currn));
extern void _VS2rule_2 ELI_ARG((_TPPrule_2 _currn));
extern void _VS1rule_3 ELI_ARG((_TPPrule_3 _currn,int* _AS0Sym));
extern void _VS1rule_4 ELI_ARG((_TPPrule_4 _currn));
extern void _VS2rule_4 ELI_ARG((_TPPrule_4 _currn));
extern void _VS1rule_5 ELI_ARG((_TPPrule_5 _currn));
#define _VS2rule_5 _VS0Empty

extern void _VS1rule_6 ELI_ARG((_TPPrule_6 _currn));
#define _VS2rule_6 _VS0Empty

extern void _VS1rule_7 ELI_ARG((_TPPrule_7 _currn));
#define _VS2rule_7 _VS0Empty

extern void _VS1rule_8 ELI_ARG((_TPPrule_8 _currn));
extern void _VS2rule_8 ELI_ARG((_TPPrule_8 _currn));
extern void _VS1rule_9 ELI_ARG((_TPPrule_9 _currn));
extern void _VS2rule_9 ELI_ARG((_TPPrule_9 _currn));
extern void _VS3rule_9 ELI_ARG((_TPPrule_9 _currn));
extern void _VS1rule_10 ELI_ARG((_TPPrule_10 _currn));
extern void _VS2rule_10 ELI_ARG((_TPPrule_10 _currn));
extern void _VS1rule_11 ELI_ARG((_TPPrule_11 _currn));
#define _VS2rule_11 _VS0Empty

extern void _VS1rule_12 ELI_ARG((_TPPrule_12 _currn));
extern void _VS2rule_12 ELI_ARG((_TPPrule_12 _currn));
extern void _VS3rule_12 ELI_ARG((_TPPrule_12 _currn));
extern void _VS1rule_13 ELI_ARG((_TPPrule_13 _currn));
extern void _VS2rule_13 ELI_ARG((_TPPrule_13 _currn));
extern void _VS1rule_14 ELI_ARG((_TPPrule_14 _currn));
extern void _VS2rule_14 ELI_ARG((_TPPrule_14 _currn));
extern void _VS1rule_15 ELI_ARG((_TPPrule_15 _currn));
extern void _VS2rule_15 ELI_ARG((_TPPrule_15 _currn));
extern void _VS3rule_15 ELI_ARG((_TPPrule_15 _currn));
extern void _VS4rule_15 ELI_ARG((_TPPrule_15 _currn));
extern void _VS1rule_16 ELI_ARG((_TPPrule_16 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey));
extern void _VS2rule_16 ELI_ARG((_TPPrule_16 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_17 ELI_ARG((_TPPrule_17 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey));
extern void _VS2rule_17 ELI_ARG((_TPPrule_17 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_18 ELI_ARG((_TPPrule_18 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey));
extern void _VS2rule_18 ELI_ARG((_TPPrule_18 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_19 ELI_ARG((_TPPrule_19 _currn));
extern void _VS2rule_19 ELI_ARG((_TPPrule_19 _currn));
#define _VS3rule_19 _VS0Empty

extern void _VS4rule_19 ELI_ARG((_TPPrule_19 _currn));
extern void _VS1rule_20 ELI_ARG((_TPPrule_20 _currn));
extern void _VS2rule_20 ELI_ARG((_TPPrule_20 _currn));
extern void _VS3rule_20 ELI_ARG((_TPPrule_20 _currn));
extern void _VS1rule_21 ELI_ARG((_TPPrule_21 _currn));
extern void _VS2rule_21 ELI_ARG((_TPPrule_21 _currn));
extern void _VS3rule_21 ELI_ARG((_TPPrule_21 _currn));
extern void _VS4rule_21 ELI_ARG((_TPPrule_21 _currn));
extern void _VS5rule_21 ELI_ARG((_TPPrule_21 _currn,PTGNode* _AS0Pgram));
extern void _VS6rule_21 ELI_ARG((_TPPrule_21 _currn,PTGNode* _AS0ConOut,PTGNode* _AS0AbsSOut,PTGNode* _AS0AbsTOut,PTGNode* _AS0StartOut,PTGNode* _AS0Pgram,PTGNode* _AS0_const11));
extern void _VS1rule_22 ELI_ARG((_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS2rule_22 ELI_ARG((_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS3rule_22 ELI_ARG((_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS4rule_22 ELI_ARG((_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey));
extern void _VS5rule_22 ELI_ARG((_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_23 ELI_ARG((_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS2rule_23 ELI_ARG((_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS3rule_23 ELI_ARG((_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS4rule_23 ELI_ARG((_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey));
extern void _VS5rule_23 ELI_ARG((_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_24 ELI_ARG((_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS2rule_24 ELI_ARG((_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS3rule_24 ELI_ARG((_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS4rule_24 ELI_ARG((_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey));
extern void _VS5rule_24 ELI_ARG((_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_25 ELI_ARG((_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS2rule_25 ELI_ARG((_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS3rule_25 ELI_ARG((_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS4rule_25 ELI_ARG((_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey));
extern void _VS5rule_25 ELI_ARG((_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_26 ELI_ARG((_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS2rule_26 ELI_ARG((_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS3rule_26 ELI_ARG((_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS4rule_26 ELI_ARG((_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey));
extern void _VS5rule_26 ELI_ARG((_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_27 ELI_ARG((_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS2rule_27 ELI_ARG((_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS3rule_27 ELI_ARG((_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS4rule_27 ELI_ARG((_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey));
extern void _VS5rule_27 ELI_ARG((_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_28 ELI_ARG((_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS2rule_28 ELI_ARG((_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS3rule_28 ELI_ARG((_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount));
extern void _VS4rule_28 ELI_ARG((_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey));
extern void _VS5rule_28 ELI_ARG((_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut));
extern void _VS1rule_29 ELI_ARG((_TPPrule_29 _currn));
extern void _VS2rule_29 ELI_ARG((_TPPrule_29 _currn));
extern void _VS3rule_29 ELI_ARG((_TPPrule_29 _currn));
extern void _VS4rule_29 ELI_ARG((_TPPrule_29 _currn,PTGNode* _AS0Pgram));
extern void _VS5rule_29 ELI_ARG((_TPPrule_29 _currn,PTGNode* _AS0ConOut,PTGNode* _AS0AbsTOut,PTGNode* _AS0StartOut,PTGNode* _AS0Pgram,PTGNode* _AS0_const11));
extern void _VS1rule_30 ELI_ARG((_TPPrule_30 _currn));
#define _VS2rule_30 _VS0Empty

extern void _VS1rule_31 ELI_ARG((_TPPrule_31 _currn));
extern void _VS1rule_32 ELI_ARG((_TPPrule_32 _currn));
extern void _VS2rule_32 ELI_ARG((_TPPrule_32 _currn));
extern void _VS3rule_32 ELI_ARG((_TPPrule_32 _currn));
#define _VS1rule_33 _VS1rule_4

extern void _VS2rule_33 ELI_ARG((_TPPrule_33 _currn));
#define _VS1rule_34 _VS1rule_4

#define _VS2rule_34 _VS2rule_10

#define _VS1rule_35 _VS1rule_4

#define _VS2rule_35 _VS2rule_10

#define _VS1rule_36 _VS1rule_4

extern void _VS1rule_37 ELI_ARG((_TPPrule_37 _currn));
extern void _VS2rule_37 ELI_ARG((_TPPrule_37 _currn));
extern void _VS1rule_38 ELI_ARG((_TPPrule_38 _currn));
#define _VS2rule_38 _VS2rule_10

extern void _VS3rule_38 ELI_ARG((_TPPrule_38 _currn));
extern void _VS4rule_38 ELI_ARG((_TPPrule_38 _currn));
extern void _VS5rule_38 ELI_ARG((_TPPrule_38 _currn));
extern void _VS1rule_39 ELI_ARG((_TPPrule_39 _currn));
extern void _VS1rule_04 ELI_ARG((_TPPrule_04 _currn));
#define _VS2rule_04 _VS0Empty

#define _VS1rule_03 _VS0Empty

#define _VS1rule_02 _VS0Empty

extern void _VS1rule_01 ELI_ARG((_TPPrule_01 _currn));
#endif
