
#include "HEAD.h"
#include "err.h"
#include "node.h"
#include "visitprocs.h"
#include "attrpredef.h"

#include "visitmap.h"

#ifdef MONITOR
#include "attr_mon_dapto.h"
#include "liga_dapto.h"
#endif

#ifndef _VisitVarDecl
#define _VisitVarDecl()
#endif

#ifndef _VisitEntry
#define _VisitEntry()
#endif

#ifndef _VisitExit
#define _VisitExit()
#endif


#if defined(__STDC__) || defined(__cplusplus)
#define _CALL_VS_(args) (void (*)args)
#else
#define _CALL_VS_(args) 
#endif
DefTableKey* _IG_incl23;
int* _IG_incl16;
DefTableKey* _IG_incl12;
DefTableKey* _IG_incl11;
int* _IG_incl8;
int* _IG_incl3;
DefTableKey* _IG_incl1;
Environment* _IG_incl0;
PTGNode _AVLST_Source__const11;
PTGNode _AVLST_Source__const12;
PTGNode _AVLST_Source__const13;
PTGNode _AVLST_Source__const14;
PTGNode _AVLST_Source__const15;
PTGNode _AVLST_Source__const16;
int _AVLST_Source__const18;
int _AVLST_Source__const28;
int _AVLST_ConAlt__const5;
int _AVLST_ConAlt__const6;
DefTableKey _AVLST_ConAlt__const8;
PTGNode _AVLST_ConAlt__const11;
tOilSetSig _AVLST_ConAltsetsig_post;
SymbolPListPtr _AVMapText_cSymbolPListPtr_post;
int _AVLST_AbsElements__const2;
int _AVLST_AbsElements__const3;
DefTableKey _AVLST_AbsElements__const7;
PTGNode _AVLST_AbsElements__const11;
tOilArgSig _AVLST_AbsElementssig_post;
DefTableKeyList _AVMapMemberssymset_post;
int _AVLST_MapReorderAlt__const0;
int _AVLST_MapReorderAlt__const1;
intList _AVLST_MapReorderAltpos_post;
Binding _AVMapLHS_Bind;
int _AVMapLHS_Sym;
DefTableKey _AVMapLHS_Key;
int _AVMapReorder__const0;
int _AVMapReorder__const1;
SymbolPList _AVMapReorder__SymbolPauxList;
intList _AVMapReorder_olist;
SymbolPList _AVMapReorder_SymbolPList;
SymbolPList _AVMapProd__SymbolPauxList;
SymbolPList _AVMapProd_SymbolPList;
DefTableKey _AVMapProd_Key;
DefTableKey _AVMapAbs_Key;
Binding _AVAbsLHS_Bind;
PTGNode _AVAbsSignature__const11;
SymbolPList _AVAbsSignature__SymbolPauxList;
SymbolPList _AVAbsSignature_SymbolPList;
Binding _AVMaptoId_Bind;
Binding _AVAbsRuleId_Bind;
int _AVAbsRuleId_Sym;
DefTableKey _AVAbsRuleId_Key;
Binding _AVConLHS_Bind;
int _AVMapText_SymbolPTakeIt;
Binding _AVMapText_Bind;
SymbolP _AVMapText_SymbolPElem;
SymbolP _AVMapText_Symbol;
SymbolP _AVMapPosition_SymbolPElem;
int _AVMapReorderAlt__const0;
int _AVMapReorderAlt__const1;
int _AVMapElement_SymbolPTakeIt;
Binding _AVMapElement_Bind;
int _AVMapElement_Sym;
int _AVMapElement_Class;
DefTableKey _AVMapElement_Key;
SymbolP _AVMapElement_SymbolPElem;
SymbolP _AVMapElement_Symbol;
DefTableKey _AVMapMember_Key_RuleAttr_103;
int _AVMapMember_Sym;
Binding _AVMapMember_Bind;
int _AVAbsElement_SymbolPTakeIt;
Binding _AVAbsElement_Bind;
int _AVAbsElement_IsFirstOcc;
int _AVAbsElement_Sym;
int _AVAbsElement_Class;
SymbolP _AVAbsElement_SymbolPElem;
SymbolP _AVAbsElement_Symbol;
PTGNode _AVAbsElements__const11;
int _AVConElement_SymbolPTakeIt;
Binding _AVConElement_Bind;
int _AVConElement_IsFirstOcc;
int _AVConElement_Sym;
int _AVConElement_Class;
SymbolP _AVConElement_SymbolPElem;
SymbolP _AVConElement_Symbol;
int _AVConAlt__const5;
int _AVConAlt__const6;
PTGNode _AVConAlt__const11;
int _AVBottomUpRule_Sym;
Binding _AVBottomUpRule_Bind;
DefTableKey _AVBottomUpRule_Key;
DefTableKeyList _AVMapSymbol_KeyList;
DefTableKey _AVConProd_Key_RuleAttr_119;
tOilSetSig _AVConProd_signature_RuleAttr_119;
SymbolPList _AVConProd__SymbolPauxList;
SymbolPList _AVConProd_SymbolPList;
PTGNode _AVSource_SymOut;
PTGNode _AVSource_Pgram;
PTGNode _AVSource_StartOut;
PTGNode _AVSource_AbsTOut;
PTGNode _AVSource_AbsSOut;
PTGNode _AVSource_ConOut;
int _AVRuleIdUse_Sym;
Binding _AVRuleIdUse_Bind;
DefTableKey _AVRuleIdUse_Key;
int _AVAbsAlt_SymbolPTakeIt;
Binding _AVAbsAlt_Bind;
int _AVAbsAlt_Sym;
DefTableKey _AVAbsAlt_Key;
SymbolP _AVAbsAlt_SymbolPElem;
SymbolP _AVAbsAlt_Symbol;

#if defined(__STDC__) || defined(__cplusplus)
void LIGA_ATTREVAL (NODEPTR _currn)
#else
void LIGA_ATTREVAL (_currn) NODEPTR _currn;
#endif
{(*(VS1MAP[_currn->_prod])) ((NODEPTR)_currn);}
/*SPC(0)*/

#if defined(__STDC__) || defined(__cplusplus)
void _VS0Empty(NODEPTR _currn)
#else
void _VS0Empty(_currn) NODEPTR _currn;
#endif
{ _VisitVarDecl()
_VisitEntry();

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1LST_BottomUpRulerule_39(_TPPLST_BottomUpRulerule_39 _currn)
#else
void _VS1LST_BottomUpRulerule_39(_currn )
_TPPLST_BottomUpRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2LST_BottomUpRulerule_39(_TPPLST_BottomUpRulerule_39 _currn)
#else
void _VS2LST_BottomUpRulerule_39(_currn )
_TPPLST_BottomUpRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3LST_BottomUpRulerule_39(_TPPLST_BottomUpRulerule_39 _currn)
#else
void _VS3LST_BottomUpRulerule_39(_currn )
_TPPLST_BottomUpRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4LST_BottomUpRulerule_39(_TPPLST_BottomUpRulerule_39 _currn)
#else
void _VS4LST_BottomUpRulerule_39(_currn )
_TPPLST_BottomUpRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS4MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5LST_BottomUpRulerule_39(_TPPLST_BottomUpRulerule_39 _currn)
#else
void _VS5LST_BottomUpRulerule_39(_currn )
_TPPLST_BottomUpRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS5MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS6LST_BottomUpRulerule_39(_TPPLST_BottomUpRulerule_39 _currn)
#else
void _VS6LST_BottomUpRulerule_39(_currn )
_TPPLST_BottomUpRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS6MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const18=_AVLST_Source__const18;
/*SPC(476)*/
_AVLST_Source__const28=ADD(IDENTICAL(_currn->_desc1->_ATGotBottomUp), _AVLST_Source__const28);
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS7LST_BottomUpRulerule_39(_TPPLST_BottomUpRulerule_39 _currn)
#else
void _VS7LST_BottomUpRulerule_39(_currn )
_TPPLST_BottomUpRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS7MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const12=_AVLST_Source__const12;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS8LST_BottomUpRulerule_39(_TPPLST_BottomUpRulerule_39 _currn)
#else
void _VS8LST_BottomUpRulerule_39(_currn )
_TPPLST_BottomUpRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS8MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const11=_AVLST_Source__const11;
/*SPC(476)*/
_AVLST_Source__const13=_AVLST_Source__const13;
/*SPC(476)*/
_AVLST_Source__const14=_AVLST_Source__const14;
/*SPC(476)*/
_AVLST_Source__const15=_AVLST_Source__const15;
/*SPC(476)*/
_AVLST_Source__const16=_AVLST_Source__const16;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1LST_MapChainsrule_39(_TPPLST_MapChainsrule_39 _currn)
#else
void _VS1LST_MapChainsrule_39(_currn )
_TPPLST_MapChainsrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2LST_MapChainsrule_39(_TPPLST_MapChainsrule_39 _currn)
#else
void _VS2LST_MapChainsrule_39(_currn )
_TPPLST_MapChainsrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS6LST_MapChainsrule_39(_TPPLST_MapChainsrule_39 _currn)
#else
void _VS6LST_MapChainsrule_39(_currn )
_TPPLST_MapChainsrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS6MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const18=_AVLST_Source__const18;
/*SPC(476)*/
_AVLST_Source__const28=_AVLST_Source__const28;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS7LST_MapChainsrule_39(_TPPLST_MapChainsrule_39 _currn)
#else
void _VS7LST_MapChainsrule_39(_currn )
_TPPLST_MapChainsrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS7MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const12=_AVLST_Source__const12;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS8LST_MapChainsrule_39(_TPPLST_MapChainsrule_39 _currn)
#else
void _VS8LST_MapChainsrule_39(_currn )
_TPPLST_MapChainsrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS8MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const11=_AVLST_Source__const11;
/*SPC(476)*/
_AVLST_Source__const13=_AVLST_Source__const13;
/*SPC(476)*/
_AVLST_Source__const14=_AVLST_Source__const14;
/*SPC(476)*/
_AVLST_Source__const15=_AVLST_Source__const15;
/*SPC(476)*/
_AVLST_Source__const16=_AVLST_Source__const16;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3LST_MapRulerule_39(_TPPLST_MapRulerule_39 _currn)
#else
void _VS3LST_MapRulerule_39(_currn )
_TPPLST_MapRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS6LST_MapRulerule_39(_TPPLST_MapRulerule_39 _currn)
#else
void _VS6LST_MapRulerule_39(_currn )
_TPPLST_MapRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS6MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const18=_AVLST_Source__const18;
/*SPC(476)*/
_AVLST_Source__const28=_AVLST_Source__const28;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS7LST_MapRulerule_39(_TPPLST_MapRulerule_39 _currn)
#else
void _VS7LST_MapRulerule_39(_currn )
_TPPLST_MapRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS7MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const12=_AVLST_Source__const12;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS8LST_MapRulerule_39(_TPPLST_MapRulerule_39 _currn)
#else
void _VS8LST_MapRulerule_39(_currn )
_TPPLST_MapRulerule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS8MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const11=_AVLST_Source__const11;
/*SPC(476)*/
_AVLST_Source__const13=_AVLST_Source__const13;
/*SPC(476)*/
_AVLST_Source__const14=_AVLST_Source__const14;
/*SPC(476)*/
_AVLST_Source__const15=_AVLST_Source__const15;
/*SPC(476)*/
_AVLST_Source__const16=_AVLST_Source__const16;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2LST_MapSymbolrule_39(_TPPLST_MapSymbolrule_39 _currn)
#else
void _VS2LST_MapSymbolrule_39(_currn )
_TPPLST_MapSymbolrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS6LST_MapSymbolrule_39(_TPPLST_MapSymbolrule_39 _currn)
#else
void _VS6LST_MapSymbolrule_39(_currn )
_TPPLST_MapSymbolrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS6MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const18=_AVLST_Source__const18;
/*SPC(476)*/
_AVLST_Source__const28=_AVLST_Source__const28;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS7LST_MapSymbolrule_39(_TPPLST_MapSymbolrule_39 _currn)
#else
void _VS7LST_MapSymbolrule_39(_currn )
_TPPLST_MapSymbolrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS7MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const12=_AVLST_Source__const12;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS8LST_MapSymbolrule_39(_TPPLST_MapSymbolrule_39 _currn)
#else
void _VS8LST_MapSymbolrule_39(_currn )
_TPPLST_MapSymbolrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS8MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const11=_AVLST_Source__const11;
/*SPC(476)*/
_AVLST_Source__const13=_AVLST_Source__const13;
/*SPC(476)*/
_AVLST_Source__const14=_AVLST_Source__const14;
/*SPC(476)*/
_AVLST_Source__const15=_AVLST_Source__const15;
/*SPC(476)*/
_AVLST_Source__const16=_AVLST_Source__const16;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4LST_AbsProdrule_39(_TPPLST_AbsProdrule_39 _currn)
#else
void _VS4LST_AbsProdrule_39(_currn )
_TPPLST_AbsProdrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS4MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS6LST_AbsProdrule_39(_TPPLST_AbsProdrule_39 _currn)
#else
void _VS6LST_AbsProdrule_39(_currn )
_TPPLST_AbsProdrule_39 _currn;

#endif
{
PTGNode _AS1_const11;
PTGNode _AS1Pgram;
PTGNode _AS1StartOut;
PTGNode _AS1AbsTOut;
PTGNode _AS1AbsSOut;
PTGNode _AS1ConOut;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS4MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS6MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const18=ADD(IDENTICAL(_currn->_desc1->_ATaxiom), _AVLST_Source__const18);
/*SPC(476)*/
_AVLST_Source__const28=_AVLST_Source__const28;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS7LST_AbsProdrule_39(_TPPLST_AbsProdrule_39 _currn)
#else
void _VS7LST_AbsProdrule_39(_currn )
_TPPLST_AbsProdrule_39 _currn;

#endif
{
PTGNode _AS1_const11;
PTGNode _AS1Pgram;
PTGNode _AS1StartOut;
PTGNode _AS1AbsTOut;
PTGNode _AS1AbsSOut;
PTGNode _AS1ConOut;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,PTGNode*)) (VS5MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1Pgram)));
(*(_CALL_VS_((NODEPTR )) (VS7MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const12=PTGSeq(IDENTICAL(_AS1Pgram), _AVLST_Source__const12);
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS8LST_AbsProdrule_39(_TPPLST_AbsProdrule_39 _currn)
#else
void _VS8LST_AbsProdrule_39(_currn )
_TPPLST_AbsProdrule_39 _currn;

#endif
{
PTGNode _AS1_const11;
PTGNode _AS1Pgram;
PTGNode _AS1StartOut;
PTGNode _AS1AbsTOut;
PTGNode _AS1AbsSOut;
PTGNode _AS1ConOut;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,PTGNode*,PTGNode*,PTGNode*,PTGNode*,PTGNode*,PTGNode*)) (VS6MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1ConOut)),(&( _AS1AbsSOut)),(&( _AS1AbsTOut)),(&( _AS1StartOut)),(&( _AS1Pgram)),(&( _AS1_const11)));
(*(_CALL_VS_((NODEPTR )) (VS8MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const11=PTGSeq(_AS1_const11, _AVLST_Source__const11);
/*SPC(476)*/
_AVLST_Source__const13=PTGSeq(IDENTICAL(_AS1StartOut), _AVLST_Source__const13);
/*SPC(476)*/
_AVLST_Source__const14=PTGSeq(IDENTICAL(_AS1AbsTOut), _AVLST_Source__const14);
/*SPC(476)*/
_AVLST_Source__const15=PTGSeq(IDENTICAL(_AS1AbsSOut), _AVLST_Source__const15);
/*SPC(476)*/
_AVLST_Source__const16=PTGSeq(IDENTICAL(_AS1ConOut), _AVLST_Source__const16);
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5LST_ConProdrule_39(_TPPLST_ConProdrule_39 _currn)
#else
void _VS5LST_ConProdrule_39(_currn )
_TPPLST_ConProdrule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS5MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS6LST_ConProdrule_39(_TPPLST_ConProdrule_39 _currn)
#else
void _VS6LST_ConProdrule_39(_currn )
_TPPLST_ConProdrule_39 _currn;

#endif
{
PTGNode _AS1_const11;
PTGNode _AS1Pgram;
PTGNode _AS1StartOut;
PTGNode _AS1AbsTOut;
PTGNode _AS1ConOut;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS6MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const18=ADD(IDENTICAL(_currn->_desc1->_ATaxiom), _AVLST_Source__const18);
/*SPC(476)*/
_AVLST_Source__const28=_AVLST_Source__const28;
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS7LST_ConProdrule_39(_TPPLST_ConProdrule_39 _currn)
#else
void _VS7LST_ConProdrule_39(_currn )
_TPPLST_ConProdrule_39 _currn;

#endif
{
PTGNode _AS1_const11;
PTGNode _AS1Pgram;
PTGNode _AS1StartOut;
PTGNode _AS1AbsTOut;
PTGNode _AS1ConOut;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,PTGNode*)) (VS4MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1Pgram)));
(*(_CALL_VS_((NODEPTR )) (VS7MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const12=PTGSeq(IDENTICAL(_AS1Pgram), _AVLST_Source__const12);
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS8LST_ConProdrule_39(_TPPLST_ConProdrule_39 _currn)
#else
void _VS8LST_ConProdrule_39(_currn )
_TPPLST_ConProdrule_39 _currn;

#endif
{
PTGNode _AS1_const11;
PTGNode _AS1Pgram;
PTGNode _AS1StartOut;
PTGNode _AS1AbsTOut;
PTGNode _AS1ConOut;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,PTGNode*,PTGNode*,PTGNode*,PTGNode*,PTGNode*)) (VS5MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1ConOut)),(&( _AS1AbsTOut)),(&( _AS1StartOut)),(&( _AS1Pgram)),(&( _AS1_const11)));
(*(_CALL_VS_((NODEPTR )) (VS8MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_Source__const11=PTGSeq(_AS1_const11, _AVLST_Source__const11);
/*SPC(476)*/
_AVLST_Source__const13=PTGSeq(IDENTICAL(_AS1StartOut), _AVLST_Source__const13);
/*SPC(476)*/
_AVLST_Source__const14=PTGSeq(IDENTICAL(_AS1AbsTOut), _AVLST_Source__const14);
/*SPC(476)*/
_AVLST_Source__const15=PTGSeq(IDENTICAL(_AS1AbsTOut), _AVLST_Source__const15);
/*SPC(476)*/
_AVLST_Source__const16=PTGSeq(IDENTICAL(_AS1ConOut), _AVLST_Source__const16);
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS6LST_0rule_39(_TPPLST_0rule_39 _currn)
#else
void _VS6LST_0rule_39(_currn )
_TPPLST_0rule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_Source__const18=ZERO();
/*SPC(476)*/
_AVLST_Source__const28=ZERO();
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS7LST_0rule_39(_TPPLST_0rule_39 _currn)
#else
void _VS7LST_0rule_39(_currn )
_TPPLST_0rule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_Source__const12=PTGNull();
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS8LST_0rule_39(_TPPLST_0rule_39 _currn)
#else
void _VS8LST_0rule_39(_currn )
_TPPLST_0rule_39 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_Source__const11=PTGNull();
/*SPC(476)*/
_AVLST_Source__const13=PTGNull();
/*SPC(476)*/
_AVLST_Source__const14=PTGNull();
/*SPC(476)*/
_AVLST_Source__const15=PTGNull();
/*SPC(476)*/
_AVLST_Source__const16=PTGNull();
/*SPC(476)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1LST_ConElementrule_38(_TPPLST_ConElementrule_38 _currn)
#else
void _VS1LST_ConElementrule_38(_currn )
_TPPLST_ConElementrule_38 _currn;

#endif
{
PTGNode _AS1SymOut;
DefTableKey _AS1SiblingKey;
int _AS1ntcount;
int _AS1argcount;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,int*,int*)) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1argcount)),(&( _AS1ntcount)));
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_ConAlt__const5=ADD(IDENTICAL(_AS1argcount), _AVLST_ConAlt__const5);
/*SPC(479)*/
_AVLST_ConAlt__const6=ADD(IDENTICAL(_AS1ntcount), _AVLST_ConAlt__const6);
/*SPC(479)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2LST_ConElementrule_38(_TPPLST_ConElementrule_38 _currn)
#else
void _VS2LST_ConElementrule_38(_currn )
_TPPLST_ConElementrule_38 _currn;

#endif
{
PTGNode _AS1SymOut;
DefTableKey _AS1SiblingKey;
int _AS1ntcount;
int _AS1argcount;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,int*,int*)) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1argcount)),(&( _AS1ntcount)));
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3LST_ConElementrule_38(_TPPLST_ConElementrule_38 _currn)
#else
void _VS3LST_ConElementrule_38(_currn )
_TPPLST_ConElementrule_38 _currn;

#endif
{
PTGNode _AS1SymOut;
DefTableKey _AS1SiblingKey;
int _AS1ntcount;
int _AS1argcount;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,int*,int*)) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1argcount)),(&( _AS1ntcount)));
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4LST_ConElementrule_38(_TPPLST_ConElementrule_38 _currn)
#else
void _VS4LST_ConElementrule_38(_currn )
_TPPLST_ConElementrule_38 _currn;

#endif
{
PTGNode _AS1SymOut;
DefTableKey _AS1SiblingKey;
int _AS1ntcount;
int _AS1argcount;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,int*,int*,DefTableKey*)) (VS4MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1argcount)),(&( _AS1ntcount)),(&( _AS1SiblingKey)));
(*(_CALL_VS_((NODEPTR )) (VS4MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_ConAlt__const8=CombineSiblings(IDENTICAL(_AS1SiblingKey), _AVLST_ConAlt__const8);
/*SPC(479)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5LST_ConElementrule_38(_TPPLST_ConElementrule_38 _currn)
#else
void _VS5LST_ConElementrule_38(_currn )
_TPPLST_ConElementrule_38 _currn;

#endif
{
PTGNode _AS1SymOut;
DefTableKey _AS1SiblingKey;
int _AS1ntcount;
int _AS1argcount;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,int*,int*,DefTableKey*,PTGNode*)) (VS5MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1argcount)),(&( _AS1ntcount)),(&( _AS1SiblingKey)),(&( _AS1SymOut)));
(*(_CALL_VS_((NODEPTR )) (VS5MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_ConAlt__const11=PTGSeq(IDENTICAL(_AS1SymOut), _AVLST_ConAlt__const11);
/*SPC(479)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1LST_0rule_38(_TPPLST_0rule_38 _currn)
#else
void _VS1LST_0rule_38(_currn )
_TPPLST_0rule_38 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_ConAlt__const5=ZERO();
/*SPC(479)*/
_AVLST_ConAlt__const6=ZERO();
/*SPC(479)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4LST_0rule_38(_TPPLST_0rule_38 _currn)
#else
void _VS4LST_0rule_38(_currn )
_TPPLST_0rule_38 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_ConAlt__const8=ZeroKey();
/*SPC(479)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5LST_0rule_38(_TPPLST_0rule_38 _currn)
#else
void _VS5LST_0rule_38(_currn )
_TPPLST_0rule_38 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_ConAlt__const11=PTGNull();
/*SPC(479)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1LST_AbsElementrule_37(_TPPLST_AbsElementrule_37 _currn)
#else
void _VS1LST_AbsElementrule_37(_currn )
_TPPLST_AbsElementrule_37 _currn;

#endif
{
PTGNode _AS1SymOut;
DefTableKey _AS1SiblingKey;
int _AS1argcount;
int _AS1ntcount;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,int*,int*,DefTableKey*)) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1ntcount)),(&( _AS1argcount)),(&( _AS1SiblingKey)));
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_AbsElements__const2=ADD(IDENTICAL(_AS1argcount), _AVLST_AbsElements__const2);
/*SPC(480)*/
_AVLST_AbsElements__const3=ADD(IDENTICAL(_AS1ntcount), _AVLST_AbsElements__const3);
/*SPC(480)*/
_AVLST_AbsElements__const7=CombineSiblings(IDENTICAL(_AS1SiblingKey), _AVLST_AbsElements__const7);
/*SPC(480)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2LST_AbsElementrule_37(_TPPLST_AbsElementrule_37 _currn)
#else
void _VS2LST_AbsElementrule_37(_currn )
_TPPLST_AbsElementrule_37 _currn;

#endif
{
PTGNode _AS1SymOut;
DefTableKey _AS1SiblingKey;
int _AS1argcount;
int _AS1ntcount;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,int*,int*,DefTableKey*,PTGNode*)) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1ntcount)),(&( _AS1argcount)),(&( _AS1SiblingKey)),(&( _AS1SymOut)));
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_AbsElements__const11=PTGSeq(IDENTICAL(_AS1SymOut), _AVLST_AbsElements__const11);
/*SPC(480)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1LST_0rule_37(_TPPLST_0rule_37 _currn)
#else
void _VS1LST_0rule_37(_currn )
_TPPLST_0rule_37 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_AbsElements__const2=ZERO();
/*SPC(480)*/
_AVLST_AbsElements__const3=ZERO();
/*SPC(480)*/
_AVLST_AbsElements__const7=ZeroKey();
/*SPC(480)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2LST_0rule_37(_TPPLST_0rule_37 _currn)
#else
void _VS2LST_0rule_37(_currn )
_TPPLST_0rule_37 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_AbsElements__const11=PTGNull();
/*SPC(480)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2LST_MapTextrule_33(_TPPLST_MapTextrule_33 _currn)
#else
void _VS2LST_MapTextrule_33(_currn )
_TPPLST_MapTextrule_33 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_MapReorderAlt__const0=_AVLST_MapReorderAlt__const0;
/*SPC(484)*/
_AVLST_MapReorderAlt__const1=ADD(ARGTOONE(_currn->_desc1->_ATSym), _AVLST_MapReorderAlt__const1);
/*SPC(484)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2LST_MapPositionrule_33(_TPPLST_MapPositionrule_33 _currn)
#else
void _VS2LST_MapPositionrule_33(_currn )
_TPPLST_MapPositionrule_33 _currn;

#endif
{
int _AS1Sym;

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR ,int*)) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1,(&( _AS1Sym)));
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVLST_MapReorderAlt__const0=ADD(ARGTOONE(_AS1Sym), _AVLST_MapReorderAlt__const0);
/*SPC(484)*/
_AVLST_MapReorderAlt__const1=ADD(ARGTOONE(_AS1Sym), _AVLST_MapReorderAlt__const1);
/*SPC(484)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2LST_0rule_33(_TPPLST_0rule_33 _currn)
#else
void _VS2LST_0rule_33(_currn )
_TPPLST_0rule_33 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_MapReorderAlt__const0=ZERO();
/*SPC(484)*/
_AVLST_MapReorderAlt__const1=ZERO();
/*SPC(484)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_1(_TPPrule_1 _currn)
#else
void _VS1rule_1(_currn )
_TPPrule_1 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
VOIDEN(ASSIGN(MapQChains, 1));
/*SPC(1103)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_2(_TPPrule_2 _currn)
#else
void _VS1rule_2(_currn )
_TPPrule_2 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMapText_Bind=BindIdn((* _IG_incl0), _currn->_ATSym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVMapText_Bind);
/*SPC(222)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_2(_TPPrule_2 _currn)
#else
void _VS2rule_2(_currn )
_TPPrule_2 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMapText_SymbolPTakeIt=1;
/*SPC(290)*/
_AVMapText_Symbol=CreateSymbol();
/*SPC(1051)*/
_AVMapText_SymbolPElem=
(SymbolKeySet(_AVMapText_Symbol, _currn->_ATKey), SymbolClassSet(_AVMapText_Symbol, TEXT), _AVMapText_Symbol)
;
/*SPC(1052)*/
_AVMapText_cSymbolPListPtr_post=
((_AVMapText_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVMapText_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_3(_TPPrule_3 _currn,int* _AS0Sym)
#else
void _VS1rule_3(_currn ,_AS0Sym)
_TPPrule_3 _currn;
int* _AS0Sym;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_MapReorderAltpos_post=
((ElemInintList(_currn->_ATTERM_1, _AVLST_MapReorderAltpos_post, intCmp)
) ? (
(message(FATAL, "Duplicates not allowed", 0, (&( _currn->_AT_pos))), _AVLST_MapReorderAltpos_post)

) : (AppElintList(_AVLST_MapReorderAltpos_post, _currn->_ATTERM_1)))
;
/*SPC(1091)*/
(* _AS0Sym)=_currn->_ATTERM_1;
/*SPC(1080)*/
_AVMapPosition_SymbolPElem=GetNthNT(GetRHS((* _IG_incl1), NULLSymbolPList), (* _AS0Sym));
/*SPC(1065)*/
_AVMapText_cSymbolPListPtr_post=
((SymbolPValid(_AVMapPosition_SymbolPElem)
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVMapPosition_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(1058)*/

if (AND(GT(_currn->_ATTERM_1, (* _IG_incl3)), NE((* _IG_incl1), NoKey))) {
message(FATAL, "Ordering number is larger than number of nonterminals", 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(1087)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_4(_TPPrule_4 _currn)
#else
void _VS1rule_4(_currn )
_TPPrule_4 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_4(_TPPrule_4 _currn)
#else
void _VS2rule_4(_currn )
_TPPrule_4 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMapReorder__SymbolPauxList=NULLSymbolPList;
/*SPC(284)*/
_AVLST_MapReorderAltpos_post=NULLintList;
/*SPC(1076)*/
_AVMapText_cSymbolPListPtr_post=_SymbolPListADDROF(_AVMapReorder__SymbolPauxList);
/*SPC(285)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVMapReorder__const0=_AVMapReorderAlt__const0;
/*SPC(1073)*/
_AVMapReorder__const1=_AVMapReorderAlt__const1;
/*SPC(1073)*/
_AVMapReorder_olist=_AVLST_MapReorderAltpos_post;
/*SPC(1075)*/
_AVMapReorder_SymbolPList=_AVMapReorder__SymbolPauxList;
/*SPC(286)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_5(_TPPrule_5 _currn)
#else
void _VS1rule_5(_currn )
_TPPrule_5 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMapElement_SymbolPTakeIt=1;
/*SPC(290)*/
_AVMapElement_Sym=_currn->_ATTERM_1;
/*SPC(1043)*/
_AVMapElement_Class=TEXT;
/*SPC(1044)*/
_AVMapElement_Symbol=CreateSymbol();
/*SPC(1007)*/
_AVMapElement_Bind=BindingInEnv((* _IG_incl0), _AVMapElement_Sym);
/*SPC(232)*/
_AVMapElement_Key=KeyOf(_AVMapElement_Bind);
/*SPC(236)*/
_AVMapElement_SymbolPElem=
(SymbolKeySet(_AVMapElement_Symbol, _AVMapElement_Key), SymbolClassSet(_AVMapElement_Symbol, _AVMapElement_Class), _AVMapElement_Symbol)
;
/*SPC(1008)*/
_AVMapText_cSymbolPListPtr_post=
((_AVMapElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVMapElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

if (EQ(_AVMapElement_Key, NoKey)) {
message(FATAL, "Symbol is not present in grammar", 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(1031)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_6(_TPPrule_6 _currn)
#else
void _VS1rule_6(_currn )
_TPPrule_6 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMapElement_SymbolPTakeIt=1;
/*SPC(290)*/
_AVMapElement_Sym=_currn->_ATTERM_1;
/*SPC(1038)*/
_AVMapElement_Class=IDENTIFIER;
/*SPC(1039)*/
_AVMapElement_Symbol=CreateSymbol();
/*SPC(1007)*/
_AVMapElement_Bind=BindingInEnv((* _IG_incl0), _AVMapElement_Sym);
/*SPC(232)*/
_AVMapElement_Key=KeyOf(_AVMapElement_Bind);
/*SPC(236)*/
_AVMapElement_SymbolPElem=
(SymbolKeySet(_AVMapElement_Symbol, _AVMapElement_Key), SymbolClassSet(_AVMapElement_Symbol, _AVMapElement_Class), _AVMapElement_Symbol)
;
/*SPC(1008)*/
_AVMapText_cSymbolPListPtr_post=
((_AVMapElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVMapElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

if (EQ(_AVMapElement_Key, NoKey)) {
message(FATAL, "Symbol is not present in grammar", 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(1031)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_7(_TPPrule_7 _currn)
#else
void _VS1rule_7(_currn )
_TPPrule_7 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMapLHS_Sym=_currn->_ATTERM_1;
/*SPC(1024)*/
_AVMapLHS_Bind=BindingInEnv((* _IG_incl0), _AVMapLHS_Sym);
/*SPC(232)*/
_AVMapLHS_Key=KeyOf(_AVMapLHS_Bind);
/*SPC(236)*/

if (EQ(_AVMapLHS_Key, NoKey)) {
message(FATAL, "Symbol is not present in grammar", 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(1031)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_8(_TPPrule_8 _currn)
#else
void _VS1rule_8(_currn )
_TPPrule_8 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMapProd__SymbolPauxList=NULLSymbolPList;
/*SPC(284)*/
_AVMapText_cSymbolPListPtr_post=_SymbolPListADDROF(_AVMapProd__SymbolPauxList);
/*SPC(285)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
_AVMapProd_SymbolPList=_AVMapProd__SymbolPauxList;
/*SPC(286)*/
_AVMapProd_Key=FindConRule(_AVMapLHS_Key, _AVMapProd_SymbolPList);
/*SPC(1015)*/

if (EQ(_AVMapProd_Key, NoKey)) {
message(FATAL, "Concrete rule is not present in grammar", 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(1020)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_8(_TPPrule_8 _currn)
#else
void _VS2rule_8(_currn )
_TPPrule_8 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_9(_TPPrule_9 _currn)
#else
void _VS1rule_9(_currn )
_TPPrule_9 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_9(_TPPrule_9 _currn)
#else
void _VS2rule_9(_currn )
_TPPrule_9 _currn;

#endif
{
int* _IL_incl3;
DefTableKey* _IL_incl1;

_VisitVarDecl()
_VisitEntry();
_IL_incl3=_IG_incl3;_IG_incl3= &(_currn->_ATntcount);
_IL_incl1=_IG_incl1;_IG_incl1= &(_currn->_ATKey);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_currn->_ATKey=_AVMapProd_Key;
/*SPC(1000)*/
_currn->_ATntcount=GetNTCount(_AVMapProd_Key, 0);
/*SPC(998)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);

if (NE(_AVMapProd_Key, NoKey)) {
ResetMappedRHS(_AVMapProd_Key, _AVMapReorder_SymbolPList);
ResetOrder(_AVMapProd_Key, _AVMapReorder_olist);
ResetNTCount(_AVMapProd_Key, _AVMapReorder__const0);
ResetArgCount(_AVMapProd_Key, _AVMapReorder__const1);
ConMatch(_AVMapProd_Key, CreateSignature(_AVMapReorder_SymbolPList), _AVMapAbs_Key, (&( _currn->_AT_pos)));
;

} else {
}
;
/*SPC(959)*/

if (AND(EQ(GetMatch(_AVMapProd_Key, NoKey), NoKey), NE(_AVMapProd_Key, NoKey))) {
message(FATAL, "Couldn't determine a mapping.", 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(987)*/
_IG_incl3=_IL_incl3;
_IG_incl1=_IL_incl1;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_9(_TPPrule_9 _currn)
#else
void _VS3rule_9(_currn )
_TPPrule_9 _currn;

#endif
{
int* _IL_incl3;
DefTableKey* _IL_incl1;

_VisitVarDecl()
_VisitEntry();
_IL_incl3=_IG_incl3;_IG_incl3= &(_currn->_ATntcount);
_IL_incl1=_IG_incl1;_IG_incl1= &(_currn->_ATKey);

if ((* _IG_incl8)) {
message(FATAL, "Rule mappings are not allowed in the presence of bottomup constraints", 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(996)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
_IG_incl3=_IL_incl3;
_IG_incl1=_IL_incl1;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_10(_TPPrule_10 _currn)
#else
void _VS1rule_10(_currn )
_TPPrule_10 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVMapAbs_Key=_AVRuleIdUse_Key;
/*SPC(950)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_10(_TPPrule_10 _currn)
#else
void _VS2rule_10(_currn )
_TPPrule_10 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_11(_TPPrule_11 _currn)
#else
void _VS1rule_11(_currn )
_TPPrule_11 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMapAbs_Key=NoKey;
/*SPC(946)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_12(_TPPrule_12 _currn)
#else
void _VS1rule_12(_currn )
_TPPrule_12 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_12(_TPPrule_12 _currn)
#else
void _VS2rule_12(_currn )
_TPPrule_12 _currn;

#endif
{
DefTableKey* _IL_incl11;

_VisitVarDecl()
_VisitEntry();
_IL_incl11=_IG_incl11;_IG_incl11= &(_currn->_ATKey);
_currn->_ATKey=KResetStr(_currn->_desc1->_ATKey, _currn->_desc1->_ATSym);
/*SPC(920)*/
_AVMapMemberssymset_post=GetAbsMap(_currn->_ATKey, NULLDefTableKeyList);
/*SPC(878)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVMapSymbol_KeyList=_AVMapMemberssymset_post;
/*SPC(877)*/
ResetAbsMap(_currn->_ATKey, _AVMapSymbol_KeyList);
/*SPC(940)*/
_IG_incl11=_IL_incl11;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_12(_TPPrule_12 _currn)
#else
void _VS3rule_12(_currn )
_TPPrule_12 _currn;

#endif
{
DefTableKey* _IL_incl11;

_VisitVarDecl()
_VisitEntry();
_IL_incl11=_IG_incl11;_IG_incl11= &(_currn->_ATKey);

if (GetIsCon(_currn->_ATKey, 0)) {
IsEqClass(_currn->_ATKey, GetNonTerm(_currn->_ATKey, 0), T_ERROR);

} else {
}
;

if (EQ(GetEqClass(_currn->_ATKey, 0), T_ERROR)) {
message(FATAL, "Equivalence classes cannot have both terminals and nonterminals", 0, (&( _currn->_AT_pos)));

} else {

if (AND(AND(GetIsAbs(_currn->_ATKey, 0), GetNonTerm(_currn->_ATKey, 0)), EQ(GetEqClass(_currn->_ATKey, 0), T_TERM))) {
message(FATAL, "Abstract nonterminal equivalenced to concrete terminals", 0, (&( _currn->_AT_pos)));

} else {
}
;
}
;
;
/*SPC(935)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_IG_incl11=_IL_incl11;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_13(_TPPrule_13 _currn)
#else
void _VS1rule_13(_currn )
_TPPrule_13 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMapMember_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVMapMember_Bind=BindingInEnv((* _IG_incl0), _AVMapMember_Sym);
/*SPC(232)*/
_currn->_ATKey=KeyOf(_AVMapMember_Bind);
/*SPC(236)*/
_AVMapMemberssymset_post=
((NE(_currn->_ATKey, (* _IG_incl11))
) ? (AddToSetDefTableKeyList(_currn->_ATKey, _AVMapMemberssymset_post, DefTableKeyCmp)
) : (_AVMapMemberssymset_post))
;
/*SPC(884)*/
_AVMapMember_Key_RuleAttr_103=GetMap(_currn->_ATKey, NoKey);
/*SPC(909)*/

if (EQ(_currn->_ATKey, NoKey)) {
;

} else {
IsEqClass((* _IG_incl11), GetNonTerm(_currn->_ATKey, 0), T_ERROR);
}
;
/*SPC(903)*/

if (AND(NE(_AVMapMember_Key_RuleAttr_103, NoKey), NE(_AVMapMember_Key_RuleAttr_103, (* _IG_incl11)))) {
message(ERROR, "Symbol cannot be member of more than one equivalence class", 0, (&( _currn->_AT_pos)));

} else {
ResetMap(_currn->_ATKey, (* _IG_incl11));
}
;
/*SPC(910)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_13(_TPPrule_13 _currn)
#else
void _VS2rule_13(_currn )
_TPPrule_13 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (EQ(_currn->_ATKey, NoKey)) {
message(FATAL, "No such symbol", 0, (&( _currn->_AT_pos)));

} else {

if (GetIsAbs(_currn->_ATKey, 0)) {
message(FATAL, "Abstract syntax symbols may not appear on the rhs of an equivalence class", 0, (&( _currn->_AT_pos)));

} else {
VOIDEN(0);
}
;
}
;
/*SPC(897)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_14(_TPPrule_14 _currn)
#else
void _VS1rule_14(_currn )
_TPPrule_14 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVBottomUpRule_Key=KeyInEnv((* _IG_incl0), _currn->_ATTERM_1);
/*SPC(825)*/
ResetBottomUp(_AVBottomUpRule_Key, 1);
/*SPC(831)*/
_currn->_ATGotBottomUp=1;
/*SPC(830)*/

if (EQ(_AVBottomUpRule_Key, NoKey)) {
message(FATAL, "Rule name must appear in the abstract syntax.", 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(829)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_14(_TPPrule_14 _currn)
#else
void _VS2rule_14(_currn )
_TPPrule_14 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVBottomUpRule_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVBottomUpRule_Bind=BindingInEnv((* _IG_incl0), _AVBottomUpRule_Sym);
/*SPC(232)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_15(_TPPrule_15 _currn)
#else
void _VS1rule_15(_currn )
_TPPrule_15 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsSignature__SymbolPauxList=NULLSymbolPList;
/*SPC(284)*/
_AVMapText_cSymbolPListPtr_post=_SymbolPListADDROF(_AVAbsSignature__SymbolPauxList);
/*SPC(285)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_currn->_AT_const4=_currn->_desc1->_ATKey;
/*SPC(795)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_15(_TPPrule_15 _currn)
#else
void _VS2rule_15(_currn )
_TPPrule_15 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsSignature_SymbolPList=_AVAbsSignature__SymbolPauxList;
/*SPC(286)*/
_currn->_ATargcount=0;
/*SPC(803)*/
ResetType((* _IG_incl12), T_LISTOF);
ResetRHS((* _IG_incl12), _AVAbsSignature_SymbolPList);
;
/*SPC(796)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_currn->_ATargsig=NullArgSig;
/*SPC(802)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_15(_TPPrule_15 _currn)
#else
void _VS3rule_15(_currn )
_TPPrule_15 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
ListofMatch((* _IG_incl12), (&( _currn->_AT_pos)));
/*SPC(1129)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_15(_TPPrule_15 _currn)
#else
void _VS4rule_15(_currn )
_TPPrule_15 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVAbsSignature__const11=PTGNull();
/*SPC(795)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_16(_TPPrule_16 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey)
#else
void _VS1rule_16(_currn ,_AS0ntcount,_AS0argcount,_AS0SiblingKey)
_TPPrule_16 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0argcount;
int* _AS0ntcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsElement_SymbolPTakeIt=1;
/*SPC(290)*/
(* _AS0argcount)=0;
/*SPC(792)*/
_AVAbsElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVAbsElement_Class=GEN_IDENT;
/*SPC(787)*/
_AVAbsElement_Symbol=CreateSymbol();
/*SPC(745)*/
(* _AS0ntcount)=0;
/*SPC(788)*/
_AVAbsElement_Bind=BindIdn((* _IG_incl0), _AVAbsElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVAbsElement_Bind);
/*SPC(222)*/
_AVAbsElement_SymbolPElem=
(SymbolKeySet(_AVAbsElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVAbsElement_Symbol, _AVAbsElement_Class), ResetStr(_currn->_ATKey, _AVAbsElement_Sym), _AVAbsElement_Symbol)
;
/*SPC(746)*/
_AVMapText_cSymbolPListPtr_post=
((_AVAbsElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVAbsElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/
(* _AS0SiblingKey)=NoKey;
/*SPC(1339)*/
ResetIsAbs(_currn->_ATKey, 1);
/*SPC(1295)*/
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_16(_TPPrule_16 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS2rule_16(_currn ,_AS0ntcount,_AS0argcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_16 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0argcount;
int* _AS0ntcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=PTGNULL;
/*SPC(1557)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_17(_TPPrule_17 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey)
#else
void _VS1rule_17(_currn ,_AS0ntcount,_AS0argcount,_AS0SiblingKey)
_TPPrule_17 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0argcount;
int* _AS0ntcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsElement_SymbolPTakeIt=1;
/*SPC(290)*/
(* _AS0argcount)=1;
/*SPC(783)*/
_AVAbsElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVAbsElement_Class=TEXT;
/*SPC(778)*/
_AVAbsElement_Symbol=CreateSymbol();
/*SPC(745)*/
(* _AS0ntcount)=0;
/*SPC(779)*/
_AVAbsElement_Bind=BindIdn((* _IG_incl0), _AVAbsElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVAbsElement_Bind);
/*SPC(222)*/
_AVAbsElement_SymbolPElem=
(SymbolKeySet(_AVAbsElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVAbsElement_Symbol, _AVAbsElement_Class), ResetStr(_currn->_ATKey, _AVAbsElement_Sym), _AVAbsElement_Symbol)
;
/*SPC(746)*/
_AVLST_AbsElementssig_post=SigAppend(_AVLST_AbsElementssig_post, _currn->_ATKey);
/*SPC(782)*/
_AVMapText_cSymbolPListPtr_post=
((_AVAbsElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVAbsElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/
(* _AS0SiblingKey)=_currn->_ATKey;
/*SPC(1335)*/
ResetIsAbs(_currn->_ATKey, 1);
/*SPC(1295)*/
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_17(_TPPrule_17 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS2rule_17(_currn ,_AS0ntcount,_AS0argcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_17 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0argcount;
int* _AS0ntcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=
((_AVAbsElement_IsFirstOcc
) ? (PTGEol(PTGId(_currn->_ATTERM_1))
) : (PTGNULL))
;
/*SPC(1560)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_18(_TPPrule_18 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey)
#else
void _VS1rule_18(_currn ,_AS0ntcount,_AS0argcount,_AS0SiblingKey)
_TPPrule_18 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0argcount;
int* _AS0ntcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsElement_SymbolPTakeIt=1;
/*SPC(290)*/
(* _AS0argcount)=1;
/*SPC(774)*/
_AVAbsElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVAbsElement_Class=IDENTIFIER;
/*SPC(769)*/
_AVAbsElement_Symbol=CreateSymbol();
/*SPC(745)*/
(* _AS0ntcount)=1;
/*SPC(770)*/
_AVAbsElement_Bind=BindIdn((* _IG_incl0), _AVAbsElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVAbsElement_Bind);
/*SPC(222)*/
_AVAbsElement_SymbolPElem=
(SymbolKeySet(_AVAbsElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVAbsElement_Symbol, _AVAbsElement_Class), ResetStr(_currn->_ATKey, _AVAbsElement_Sym), _AVAbsElement_Symbol)
;
/*SPC(746)*/
_AVLST_AbsElementssig_post=SigAppend(_AVLST_AbsElementssig_post, _currn->_ATKey);
/*SPC(773)*/
_AVMapText_cSymbolPListPtr_post=
((_AVAbsElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVAbsElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/
(* _AS0SiblingKey)=_currn->_ATKey;
/*SPC(1335)*/
ResetIsAbs(_currn->_ATKey, 1);
/*SPC(1295)*/
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_18(_TPPrule_18 _currn,int* _AS0ntcount,int* _AS0argcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS2rule_18(_currn ,_AS0ntcount,_AS0argcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_18 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0argcount;
int* _AS0ntcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=
((AND(_AVAbsElement_IsFirstOcc, AND(NOT(GetNonTerm(_currn->_ATKey, 0)), GetConSym(_currn->_ATKey, 0)))
) ? (PTGEol(PTGId(_currn->_ATTERM_1))
) : (PTGNULL))
;
/*SPC(1567)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_19(_TPPrule_19 _currn)
#else
void _VS1rule_19(_currn )
_TPPrule_19 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsSignature__SymbolPauxList=NULLSymbolPList;
/*SPC(284)*/
_AVMapText_cSymbolPListPtr_post=_SymbolPListADDROF(_AVAbsSignature__SymbolPauxList);
/*SPC(285)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVLST_AbsElementssig_post=OilNewArgSig();
/*SPC(759)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_currn->_AT_const4=_currn->_desc1->_ATKey;
/*SPC(752)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_19(_TPPrule_19 _currn)
#else
void _VS2rule_19(_currn )
_TPPrule_19 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsSignature_SymbolPList=_AVAbsSignature__SymbolPauxList;
/*SPC(286)*/
_currn->_ATargcount=_currn->_desc2->_AT_const2;
/*SPC(755)*/
ResetType((* _IG_incl12), T_NONLISTOF);
ResetRHS((* _IG_incl12), _AVAbsSignature_SymbolPList);
ResetArgCount((* _IG_incl12), _currn->_ATargcount);
ResetNTCount((* _IG_incl12), _currn->_desc2->_AT_const3);
;
/*SPC(735)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_currn->_ATargsig=SigAppend(
((EQ(_currn->_ATargcount, 0)
) ? (SigAppend(_currn->_desc2->_ATsig_post, OilTypeName(EmptyType))
) : (_currn->_desc2->_ATsig_post))
, _currn->_desc1->_ATKey);
/*SPC(760)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_19(_TPPrule_19 _currn)
#else
void _VS4rule_19(_currn )
_TPPrule_19 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVAbsSignature__const11=_AVAbsElements__const11;
/*SPC(752)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_20(_TPPrule_20 _currn)
#else
void _VS1rule_20(_currn )
_TPPrule_20 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsLHS_Bind=BindIdn((* _IG_incl0), _currn->_ATSym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVAbsLHS_Bind);
/*SPC(222)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_20(_TPPrule_20 _currn)
#else
void _VS2rule_20(_currn )
_TPPrule_20 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
ResetIsAbs(_currn->_ATKey, 1);
/*SPC(1279)*/
ResetNonTerm(_currn->_ATKey, 1);
/*SPC(1267)*/
ResetStr(_currn->_ATKey, _currn->_ATTERM_1);
ResetAbsRules(_currn->_ATKey, AppElDefTableKeyList(GetAbsRules(_currn->_ATKey, NULLDefTableKeyList), (* _IG_incl12)));
;
/*SPC(718)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_20(_TPPrule_20 _currn)
#else
void _VS3rule_20(_currn )
_TPPrule_20 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (AND(AND(GetStart(_currn->_ATKey, 0), NE((* _IG_incl16), 1)), NOT(GetRootErr(_currn->_ATKey, 0)))) {
ResetRootErr(_currn->_ATKey, 1);
message(ERROR, CatStrInd("One of the multiple roots: ", _currn->_ATSym), 0, (&( _currn->_AT_pos)));
;

} else {
}
;
/*SPC(1275)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_21(_TPPrule_21 _currn)
#else
void _VS1rule_21(_currn )
_TPPrule_21 _currn;

#endif
{
DefTableKey* _IL_incl12;

_VisitVarDecl()
_VisitEntry();
_IL_incl12=_IG_incl12;_IG_incl12= &(_currn->_ATKey);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_currn->_ATKey=KResetLHS(KResetStr(_AVAbsRuleId_Key, _AVAbsRuleId_Sym), _currn->_desc2->_AT_const4);
/*SPC(694)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_IG_incl12=_IL_incl12;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_21(_TPPrule_21 _currn)
#else
void _VS2rule_21(_currn )
_TPPrule_21 _currn;

#endif
{
DefTableKey* _IL_incl12;

_VisitVarDecl()
_VisitEntry();
_IL_incl12=_IG_incl12;_IG_incl12= &(_currn->_ATKey);

if (NE(_currn->_desc2->_ATargsig, NullArgSig)) {
CreateOper(_currn->_desc2->_ATargcount, _currn->_ATKey, _currn->_desc2->_ATargsig);

} else {
}
;
/*SPC(703)*/
_IG_incl12=_IL_incl12;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_21(_TPPrule_21 _currn)
#else
void _VS3rule_21(_currn )
_TPPrule_21 _currn;

#endif
{
DefTableKey* _IL_incl12;

_VisitVarDecl()
_VisitEntry();
_IL_incl12=_IG_incl12;_IG_incl12= &(_currn->_ATKey);
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_currn->_ATaxiom=CheckAxiom(_currn->_desc2->_AT_const4, 1);
/*SPC(1245)*/

if (_currn->_ATaxiom) {
ComputeReach(_currn->_desc2->_AT_const4);

} else {
;
}
;
/*SPC(1250)*/
_IG_incl12=_IL_incl12;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_21(_TPPrule_21 _currn)
#else
void _VS4rule_21(_currn )
_TPPrule_21 _currn;

#endif
{
DefTableKey* _IL_incl12;

_VisitVarDecl()
_VisitEntry();
_IL_incl12=_IG_incl12;_IG_incl12= &(_currn->_ATKey);
MarkConSym(_currn->_ATKey);
/*SPC(1259)*/
_IG_incl12=_IL_incl12;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_21(_TPPrule_21 _currn,PTGNode* _AS0Pgram)
#else
void _VS5rule_21(_currn ,_AS0Pgram)
_TPPrule_21 _currn;
PTGNode* _AS0Pgram;

#endif
{
DefTableKey* _IL_incl12;

_VisitVarDecl()
_VisitEntry();
_IL_incl12=_IG_incl12;_IG_incl12= &(_currn->_ATKey);
(* _AS0Pgram)=GenConProd(_currn->_ATKey, 1, 1);
/*SPC(1521)*/
_IG_incl12=_IL_incl12;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS6rule_21(_TPPrule_21 _currn,PTGNode* _AS0ConOut,PTGNode* _AS0AbsSOut,PTGNode* _AS0AbsTOut,PTGNode* _AS0StartOut,PTGNode* _AS0Pgram,PTGNode* _AS0_const11)
#else
void _VS6rule_21(_currn ,_AS0ConOut,_AS0AbsSOut,_AS0AbsTOut,_AS0StartOut,_AS0Pgram,_AS0_const11)
_TPPrule_21 _currn;
PTGNode* _AS0_const11;
PTGNode* _AS0Pgram;
PTGNode* _AS0StartOut;
PTGNode* _AS0AbsTOut;
PTGNode* _AS0AbsSOut;
PTGNode* _AS0ConOut;

#endif
{
DefTableKey* _IL_incl12;

_VisitVarDecl()
_VisitEntry();
_IL_incl12=_IG_incl12;_IG_incl12= &(_currn->_ATKey);
(*(_CALL_VS_((NODEPTR )) (VS4MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(* _AS0_const11)=_AVAbsSignature__const11;
/*SPC(692)*/
(* _AS0StartOut)=
((AND(EQ(GetAbsMap(_currn->_desc2->_AT_const4, NULLDefTableKeyList), NULLDefTableKeyList), _currn->_ATaxiom)
) ? (PTGStart(PTGAsIs(StrStr(_currn->_desc2->_AT_const4)))
) : (PTGNULL))
;
/*SPC(1494)*/
(* _AS0AbsTOut)=GenAbsProd(_currn->_ATKey, 0);
/*SPC(1462)*/
(* _AS0AbsSOut)=
((GetReach(_currn->_ATKey, 0)
) ? ((* _AS0AbsTOut)
) : (PTGNULL))
;
/*SPC(1446)*/
(* _AS0ConOut)=GenConProd(_currn->_ATKey, 1, 0);
/*SPC(1423)*/
_IG_incl12=_IL_incl12;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_22(_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS1rule_22(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_22 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_SymbolPTakeIt=1;
/*SPC(290)*/
_AVConElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVConElement_Class=DOLLARMODTXT;
/*SPC(687)*/
_AVConElement_Symbol=CreateSymbol();
/*SPC(612)*/
(* _AS0ntcount)=0;
/*SPC(685)*/
(* _AS0argcount)=0;
/*SPC(686)*/
_AVConElement_Bind=BindIdn((* _IG_incl0), _AVConElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVConElement_Bind);
/*SPC(222)*/
_AVConElement_SymbolPElem=
(SymbolKeySet(_AVConElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVConElement_Symbol, _AVConElement_Class), ResetStr(_currn->_ATKey, _AVConElement_Sym), _AVConElement_Symbol)
;
/*SPC(613)*/
_AVMapText_cSymbolPListPtr_post=
((_AVConElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVConElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_22(_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS2rule_22(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_22 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_22(_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS3rule_22(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_22 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_22(_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey)
#else
void _VS4rule_22(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey)
_TPPrule_22 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
(* _AS0SiblingKey)=NoKey;
/*SPC(1369)*/
ResetIsCon(_currn->_ATKey, 1);
/*SPC(1306)*/
ResetConSym(_currn->_ATKey, 1);
/*SPC(1304)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_22(_TPPrule_22 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS5rule_22(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_22 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=
((AND(_AVConElement_IsFirstOcc, NOT(GetNonTerm(_currn->_ATKey, 0)))
) ? (PTGEol(PTGId(GetStr(_currn->_ATKey, 0)))
) : (PTGNULL))
;
/*SPC(1546)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_23(_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS1rule_23(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_23 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_SymbolPTakeIt=1;
/*SPC(290)*/
_AVConElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVConElement_Class=DOLLARMODIDN;
/*SPC(679)*/
_AVConElement_Symbol=CreateSymbol();
/*SPC(612)*/
(* _AS0ntcount)=0;
/*SPC(677)*/
(* _AS0argcount)=0;
/*SPC(678)*/
_AVConElement_Bind=BindIdn((* _IG_incl0), _AVConElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVConElement_Bind);
/*SPC(222)*/
_AVConElement_SymbolPElem=
(SymbolKeySet(_AVConElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVConElement_Symbol, _AVConElement_Class), ResetStr(_currn->_ATKey, _AVConElement_Sym), _AVConElement_Symbol)
;
/*SPC(613)*/
_AVMapText_cSymbolPListPtr_post=
((_AVConElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVConElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_23(_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS2rule_23(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_23 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_23(_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS3rule_23(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_23 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_23(_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey)
#else
void _VS4rule_23(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey)
_TPPrule_23 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
(* _AS0SiblingKey)=NoKey;
/*SPC(1365)*/
ResetIsCon(_currn->_ATKey, 1);
/*SPC(1306)*/
ResetConSym(_currn->_ATKey, 1);
/*SPC(1304)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_23(_TPPrule_23 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS5rule_23(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_23 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=
((AND(_AVConElement_IsFirstOcc, NOT(GetNonTerm(_currn->_ATKey, 0)))
) ? (PTGEol(PTGId(GetStr(_currn->_ATKey, 0)))
) : (PTGNULL))
;
/*SPC(1546)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_24(_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS1rule_24(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_24 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_SymbolPTakeIt=1;
/*SPC(290)*/
_AVConElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVConElement_Class=ATMODTXT;
/*SPC(671)*/
_AVConElement_Symbol=CreateSymbol();
/*SPC(612)*/
(* _AS0ntcount)=0;
/*SPC(669)*/
(* _AS0argcount)=0;
/*SPC(670)*/
_AVConElement_Bind=BindIdn((* _IG_incl0), _AVConElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVConElement_Bind);
/*SPC(222)*/
_AVConElement_SymbolPElem=
(SymbolKeySet(_AVConElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVConElement_Symbol, _AVConElement_Class), ResetStr(_currn->_ATKey, _AVConElement_Sym), _AVConElement_Symbol)
;
/*SPC(613)*/
_AVMapText_cSymbolPListPtr_post=
((_AVConElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVConElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_24(_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS2rule_24(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_24 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_24(_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS3rule_24(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_24 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_24(_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey)
#else
void _VS4rule_24(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey)
_TPPrule_24 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
(* _AS0SiblingKey)=NoKey;
/*SPC(1361)*/
ResetIsCon(_currn->_ATKey, 1);
/*SPC(1306)*/
ResetConSym(_currn->_ATKey, 1);
/*SPC(1304)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_24(_TPPrule_24 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS5rule_24(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_24 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=
((AND(_AVConElement_IsFirstOcc, NOT(GetNonTerm(_currn->_ATKey, 0)))
) ? (PTGEol(PTGId(GetStr(_currn->_ATKey, 0)))
) : (PTGNULL))
;
/*SPC(1546)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_25(_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS1rule_25(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_25 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_SymbolPTakeIt=1;
/*SPC(290)*/
_AVConElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVConElement_Class=ATMODIDN;
/*SPC(663)*/
_AVConElement_Symbol=CreateSymbol();
/*SPC(612)*/
(* _AS0ntcount)=0;
/*SPC(661)*/
(* _AS0argcount)=0;
/*SPC(662)*/
_AVConElement_Bind=BindIdn((* _IG_incl0), _AVConElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVConElement_Bind);
/*SPC(222)*/
_AVConElement_SymbolPElem=
(SymbolKeySet(_AVConElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVConElement_Symbol, _AVConElement_Class), ResetStr(_currn->_ATKey, _AVConElement_Sym), _AVConElement_Symbol)
;
/*SPC(613)*/
_AVMapText_cSymbolPListPtr_post=
((_AVConElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVConElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_25(_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS2rule_25(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_25 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_25(_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS3rule_25(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_25 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_25(_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey)
#else
void _VS4rule_25(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey)
_TPPrule_25 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
(* _AS0SiblingKey)=NoKey;
/*SPC(1357)*/
ResetIsCon(_currn->_ATKey, 1);
/*SPC(1306)*/
ResetConSym(_currn->_ATKey, 1);
/*SPC(1304)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_25(_TPPrule_25 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS5rule_25(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_25 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=
((AND(_AVConElement_IsFirstOcc, NOT(GetNonTerm(_currn->_ATKey, 0)))
) ? (PTGEol(PTGId(GetStr(_currn->_ATKey, 0)))
) : (PTGNULL))
;
/*SPC(1546)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_26(_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS1rule_26(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_26 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_SymbolPTakeIt=1;
/*SPC(290)*/
_AVConElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVConElement_Class=ACTION;
/*SPC(655)*/
_AVConElement_Symbol=CreateSymbol();
/*SPC(612)*/
(* _AS0ntcount)=0;
/*SPC(653)*/
(* _AS0argcount)=0;
/*SPC(654)*/
_AVConElement_Bind=BindIdn((* _IG_incl0), _AVConElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVConElement_Bind);
/*SPC(222)*/
_AVConElement_SymbolPElem=
(SymbolKeySet(_AVConElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVConElement_Symbol, _AVConElement_Class), ResetStr(_currn->_ATKey, _AVConElement_Sym), _AVConElement_Symbol)
;
/*SPC(613)*/
_AVMapText_cSymbolPListPtr_post=
((_AVConElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVConElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_26(_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS2rule_26(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_26 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_26(_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS3rule_26(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_26 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_26(_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey)
#else
void _VS4rule_26(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey)
_TPPrule_26 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
(* _AS0SiblingKey)=NoKey;
/*SPC(1353)*/
ResetIsCon(_currn->_ATKey, 1);
/*SPC(1306)*/
ResetConSym(_currn->_ATKey, 1);
/*SPC(1304)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_26(_TPPrule_26 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS5rule_26(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_26 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=PTGNULL;
/*SPC(1554)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_27(_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS1rule_27(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_27 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_SymbolPTakeIt=1;
/*SPC(290)*/
_AVConElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVConElement_Class=TEXT;
/*SPC(644)*/
_AVConElement_Symbol=CreateSymbol();
/*SPC(612)*/
(* _AS0ntcount)=0;
/*SPC(645)*/
(* _AS0argcount)=1;
/*SPC(646)*/
_AVConElement_Bind=BindIdn((* _IG_incl0), _AVConElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVConElement_Bind);
/*SPC(222)*/
_AVConElement_SymbolPElem=
(SymbolKeySet(_AVConElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVConElement_Symbol, _AVConElement_Class), ResetStr(_currn->_ATKey, _AVConElement_Sym), _AVConElement_Symbol)
;
/*SPC(613)*/
_AVMapText_cSymbolPListPtr_post=
((_AVConElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVConElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_27(_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS2rule_27(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_27 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_27(_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS3rule_27(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_27 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_ConAltsetsig_post=OilAddSetSig(CreateTS(_currn->_ATKey), _AVLST_ConAltsetsig_post);
/*SPC(1178)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_27(_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey)
#else
void _VS4rule_27(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey)
_TPPrule_27 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
(* _AS0SiblingKey)=
((EQ(GetDuplicate((* _IG_incl23), NoKey), NoKey)
) ? (NoKey
) : (_currn->_ATKey))
;
/*SPC(1345)*/
ResetIsCon(_currn->_ATKey, 1);
/*SPC(1306)*/
ResetConSym(_currn->_ATKey, 1);
/*SPC(1304)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_27(_TPPrule_27 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS5rule_27(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_27 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=
((AND(_AVConElement_IsFirstOcc, NOT(GetNonTerm(_currn->_ATKey, 0)))
) ? (PTGEol(PTGId(GetStr(_currn->_ATKey, 0)))
) : (PTGNULL))
;
/*SPC(1546)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_28(_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS1rule_28(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_28 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_SymbolPTakeIt=1;
/*SPC(290)*/
_AVConElement_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVConElement_Class=IDENTIFIER;
/*SPC(630)*/
_AVConElement_Symbol=CreateSymbol();
/*SPC(612)*/
(* _AS0ntcount)=1;
/*SPC(631)*/
(* _AS0argcount)=1;
/*SPC(632)*/
_AVConElement_Bind=BindIdn((* _IG_incl0), _AVConElement_Sym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVConElement_Bind);
/*SPC(222)*/
_AVConElement_SymbolPElem=
(SymbolKeySet(_AVConElement_Symbol, _currn->_ATKey), SymbolClassSet(_AVConElement_Symbol, _AVConElement_Class), ResetStr(_currn->_ATKey, _AVConElement_Sym), _AVConElement_Symbol)
;
/*SPC(613)*/
_AVMapText_cSymbolPListPtr_post=
((_AVConElement_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVConElement_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_28(_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS2rule_28(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_28 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
ResetIsRHS(_currn->_ATKey, 1);
/*SPC(1291)*/
ResetRHSConRules(_currn->_ATKey, AddToSetDefTableKeyList((* _IG_incl23), GetRHSConRules(_currn->_ATKey, NULLDefTableKeyList), DefTableKeyCmp));
/*SPC(636)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_28(_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount)
#else
void _VS3rule_28(_currn ,_AS0argcount,_AS0ntcount)
_TPPrule_28 _currn;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVLST_ConAltsetsig_post=OilAddSetSig(CreateTS(_currn->_ATKey), _AVLST_ConAltsetsig_post);
/*SPC(1170)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_28(_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey)
#else
void _VS4rule_28(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey)
_TPPrule_28 _currn;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
(* _AS0SiblingKey)=
((EQ(GetDuplicate((* _IG_incl23), NoKey), NoKey)
) ? (NoKey
) : (_currn->_ATKey))
;
/*SPC(1345)*/
ResetIsCon(_currn->_ATKey, 1);
/*SPC(1306)*/
ResetConSym(_currn->_ATKey, 1);
/*SPC(1304)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_28(_TPPrule_28 _currn,int* _AS0argcount,int* _AS0ntcount,DefTableKey* _AS0SiblingKey,PTGNode* _AS0SymOut)
#else
void _VS5rule_28(_currn ,_AS0argcount,_AS0ntcount,_AS0SiblingKey,_AS0SymOut)
_TPPrule_28 _currn;
PTGNode* _AS0SymOut;
DefTableKey* _AS0SiblingKey;
int* _AS0ntcount;
int* _AS0argcount;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConElement_IsFirstOcc=SetGetFirstOcc(_currn->_ATKey, 1, 0);
/*SPC(1391)*/
(* _AS0SymOut)=
((AND(_AVConElement_IsFirstOcc, NOT(GetNonTerm(_currn->_ATKey, 0)))
) ? (PTGEol(PTGId(GetStr(_currn->_ATKey, 0)))
) : (PTGNULL))
;
/*SPC(1546)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_29(_TPPrule_29 _currn)
#else
void _VS1rule_29(_currn )
_TPPrule_29 _currn;

#endif
{
DefTableKey* _IL_incl23;

_VisitVarDecl()
_VisitEntry();
_IL_incl23=_IG_incl23;_IG_incl23= &(_currn->_ATKey);
_AVConProd__SymbolPauxList=NULLSymbolPList;
/*SPC(284)*/
_AVMapText_cSymbolPListPtr_post=_SymbolPListADDROF(_AVConProd__SymbolPauxList);
/*SPC(285)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVConProd_Key_RuleAttr_119=NewKey();
/*SPC(592)*/
_AVConProd_SymbolPList=_AVConProd__SymbolPauxList;
/*SPC(286)*/
_currn->_ATargcount=_AVConAlt__const5;
/*SPC(593)*/
_currn->_ATKey=
(ResetRHS(_AVConProd_Key_RuleAttr_119, _AVConProd_SymbolPList), ResetLHS(_AVConProd_Key_RuleAttr_119, _currn->_desc1->_ATKey), ResetArgCount(_AVConProd_Key_RuleAttr_119, _currn->_ATargcount), ResetNTCount(_AVConProd_Key_RuleAttr_119, _AVConAlt__const6), _AVConProd_Key_RuleAttr_119)
;
/*SPC(595)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_IG_incl23=_IL_incl23;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_29(_TPPrule_29 _currn)
#else
void _VS2rule_29(_currn )
_TPPrule_29 _currn;

#endif
{
DefTableKey* _IL_incl23;

_VisitVarDecl()
_VisitEntry();
_IL_incl23=_IG_incl23;_IG_incl23= &(_currn->_ATKey);
_AVLST_ConAltsetsig_post=OilNewSetSig();
/*SPC(1165)*/
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVConProd_signature_RuleAttr_119=
((EQ(_currn->_ATargcount, 0)
) ? (OilAddSetSig(OilTypeToSet(EmptyType), _AVLST_ConAltsetsig_post)
) : (_AVLST_ConAltsetsig_post))
;
/*SPC(1160)*/
_currn->_ATaxiom=CheckAxiom(_currn->_desc1->_ATKey, 0);
/*SPC(1230)*/

if (_currn->_ATaxiom) {
ComputeReach(_currn->_desc1->_ATKey);

} else {
;
}
;
/*SPC(1235)*/

if (EQ(GetMappedRHS(_currn->_ATKey, NULLSymbolPList), NULLSymbolPList)) {
ConMatch(_currn->_ATKey, _AVConProd_signature_RuleAttr_119, NoKey, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(1146)*/
_IG_incl23=_IL_incl23;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_29(_TPPrule_29 _currn)
#else
void _VS3rule_29(_currn )
_TPPrule_29 _currn;

#endif
{
DefTableKey* _IL_incl23;

_VisitVarDecl()
_VisitEntry();
_IL_incl23=_IG_incl23;_IG_incl23= &(_currn->_ATKey);
(*(_CALL_VS_((NODEPTR )) (VS4MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_IG_incl23=_IL_incl23;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_29(_TPPrule_29 _currn,PTGNode* _AS0Pgram)
#else
void _VS4rule_29(_currn ,_AS0Pgram)
_TPPrule_29 _currn;
PTGNode* _AS0Pgram;

#endif
{
DefTableKey* _IL_incl23;

_VisitVarDecl()
_VisitEntry();
_IL_incl23=_IG_incl23;_IG_incl23= &(_currn->_ATKey);
(* _AS0Pgram)=GenConProd(_currn->_ATKey, 0, 1);
/*SPC(1505)*/
_IG_incl23=_IL_incl23;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_29(_TPPrule_29 _currn,PTGNode* _AS0ConOut,PTGNode* _AS0AbsTOut,PTGNode* _AS0StartOut,PTGNode* _AS0Pgram,PTGNode* _AS0_const11)
#else
void _VS5rule_29(_currn ,_AS0ConOut,_AS0AbsTOut,_AS0StartOut,_AS0Pgram,_AS0_const11)
_TPPrule_29 _currn;
PTGNode* _AS0_const11;
PTGNode* _AS0Pgram;
PTGNode* _AS0StartOut;
PTGNode* _AS0AbsTOut;
PTGNode* _AS0ConOut;

#endif
{
DefTableKey* _IL_incl23;

_VisitVarDecl()
_VisitEntry();
_IL_incl23=_IG_incl23;_IG_incl23= &(_currn->_ATKey);
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS5MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(* _AS0_const11)=_AVConAlt__const11;
/*SPC(589)*/
(* _AS0StartOut)=
((_currn->_ATaxiom
) ? (PTGStart(PTGAsIs(StrStr(_currn->_desc1->_ATKey)))
) : (PTGNULL))
;
/*SPC(1489)*/
(* _AS0AbsTOut)=GenAbsProd(_currn->_ATKey, 1);
/*SPC(1467)*/
(* _AS0ConOut)=GenConProd(_currn->_ATKey, 0, 0);
/*SPC(1410)*/
_IG_incl23=_IL_incl23;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_30(_TPPrule_30 _currn)
#else
void _VS1rule_30(_currn )
_TPPrule_30 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVMaptoId_Bind=BindIdn((* _IG_incl0), _currn->_ATSym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVMaptoId_Bind);
/*SPC(222)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_31(_TPPrule_31 _currn)
#else
void _VS1rule_31(_currn )
_TPPrule_31 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsRuleId_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVAbsRuleId_Bind=BindIdn((* _IG_incl0), _AVAbsRuleId_Sym);
/*SPC(220)*/
_AVAbsRuleId_Key=KeyOf(_AVAbsRuleId_Bind);
/*SPC(222)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_32(_TPPrule_32 _currn)
#else
void _VS1rule_32(_currn )
_TPPrule_32 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVConLHS_Bind=BindIdn((* _IG_incl0), _currn->_ATSym);
/*SPC(220)*/
_currn->_ATKey=KeyOf(_AVConLHS_Bind);
/*SPC(222)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_32(_TPPrule_32 _currn)
#else
void _VS2rule_32(_currn )
_TPPrule_32 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
ResetIsCon(_currn->_ATKey, 1);
/*SPC(1286)*/
ResetConSym(_currn->_ATKey, 1);
/*SPC(1284)*/
ResetNonTerm(_currn->_ATKey, 1);
/*SPC(1267)*/
ResetStr(_currn->_ATKey, _currn->_ATSym);
ResetConRules(_currn->_ATKey, AppElDefTableKeyList(GetConRules(_currn->_ATKey, NULLDefTableKeyList), (* _IG_incl23)));
;
/*SPC(622)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_32(_TPPrule_32 _currn)
#else
void _VS3rule_32(_currn )
_TPPrule_32 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (AND(AND(GetStart(_currn->_ATKey, 0), NE((* _IG_incl16), 1)), NOT(GetRootErr(_currn->_ATKey, 0)))) {
ResetRootErr(_currn->_ATKey, 1);
message(ERROR, CatStrInd("One of the multiple roots: ", _currn->_ATSym), 0, (&( _currn->_AT_pos)));
;

} else {
}
;
/*SPC(1275)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_33(_TPPrule_33 _currn)
#else
void _VS2rule_33(_currn )
_TPPrule_33 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVMapReorderAlt__const0=_AVLST_MapReorderAlt__const0;
/*SPC(484)*/
_AVMapReorderAlt__const1=_AVLST_MapReorderAlt__const1;
/*SPC(484)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_37(_TPPrule_37 _currn)
#else
void _VS1rule_37(_currn )
_TPPrule_37 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_currn->_AT_const2=_AVLST_AbsElements__const2;
/*SPC(480)*/
_currn->_AT_const3=_AVLST_AbsElements__const3;
/*SPC(480)*/
_currn->_ATsig_post=_AVLST_AbsElementssig_post;
/*SPC(0)*/
VOIDEN(_AVLST_AbsElements__const7);
/*SPC(1323)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_37(_TPPrule_37 _currn)
#else
void _VS2rule_37(_currn )
_TPPrule_37 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVAbsElements__const11=_AVLST_AbsElements__const11;
/*SPC(480)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_38(_TPPrule_38 _currn)
#else
void _VS1rule_38(_currn )
_TPPrule_38 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVConAlt__const5=_AVLST_ConAlt__const5;
/*SPC(479)*/
_AVConAlt__const6=_AVLST_ConAlt__const6;
/*SPC(479)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS3rule_38(_TPPrule_38 _currn)
#else
void _VS3rule_38(_currn )
_TPPrule_38 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS4rule_38(_TPPrule_38 _currn)
#else
void _VS4rule_38(_currn )
_TPPrule_38 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS4MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
VOIDEN(_AVLST_ConAlt__const8);
/*SPC(1329)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS5rule_38(_TPPrule_38 _currn)
#else
void _VS5rule_38(_currn )
_TPPrule_38 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS5MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVConAlt__const11=_AVLST_ConAlt__const11;
/*SPC(479)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_39(_TPPrule_39 _currn)
#else
void _VS1rule_39(_currn )
_TPPrule_39 _currn;

#endif
{
int* _IL_incl16;
int* _IL_incl8;
Environment* _IL_incl0;

_VisitVarDecl()
_VisitEntry();
_IL_incl16=_IG_incl16;_IG_incl16= &(_currn->_ATAxiomCnt);
_IL_incl8=_IG_incl8;_IG_incl8= &(_currn->_ATGotBottomUp);
_IL_incl0=_IG_incl0;_IG_incl0= &(_currn->_ATEnv);
_currn->_ATEnv=Initialize();
/*SPC(564)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS3MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS4MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS5MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS6MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_currn->_ATAxiomCnt=_AVLST_Source__const18;
/*SPC(1219)*/
_currn->_ATGotBottomUp=_AVLST_Source__const28;
/*SPC(818)*/

if (EQ(_currn->_ATAxiomCnt, 0)) {
message(ERROR, "Could not determine the start symbol", 0, NoPosition);

} else {
}
;
/*SPC(1225)*/
(*(_CALL_VS_((NODEPTR )) (VS7MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVSource_Pgram=_AVLST_Source__const12;
/*SPC(1482)*/
(*(_CALL_VS_((NODEPTR )) (VS8MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVSource_SymOut=_AVLST_Source__const11;
/*SPC(1539)*/
_AVSource_StartOut=_AVLST_Source__const13;
/*SPC(1479)*/
_AVSource_AbsTOut=_AVLST_Source__const14;
/*SPC(1455)*/
_AVSource_AbsSOut=_AVLST_Source__const15;
/*SPC(1439)*/
_AVSource_ConOut=_AVLST_Source__const16;
/*SPC(1403)*/
PTGOutFile("consyntax", _AVSource_ConOut);
/*SPC(1406)*/
PTGOutFile("absyntax", _AVSource_AbsSOut);
/*SPC(1442)*/
PTGOutFile("abstree", _AVSource_AbsTOut);
/*SPC(1458)*/
PTGOutFile("pgram", PTGSeq(_AVSource_StartOut, _AVSource_Pgram));
/*SPC(1485)*/
PTGOutFile("symbols", _AVSource_SymOut);
/*SPC(1542)*/
_IG_incl16=_IL_incl16;
_IG_incl8=_IL_incl8;
_IG_incl0=_IL_incl0;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_04(_TPPrule_04 _currn)
#else
void _VS1rule_04(_currn )
_TPPrule_04 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVRuleIdUse_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVRuleIdUse_Bind=BindingInEnv((* _IG_incl0), _AVRuleIdUse_Sym);
/*SPC(232)*/
_AVRuleIdUse_Key=KeyOf(_AVRuleIdUse_Bind);
/*SPC(236)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_01(_TPPrule_01 _currn)
#else
void _VS1rule_01(_currn )
_TPPrule_01 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVAbsAlt_SymbolPTakeIt=1;
/*SPC(290)*/
_AVAbsAlt_Sym=_currn->_ATTERM_1;
/*SPC(491)*/
_AVAbsAlt_Symbol=CreateSymbol();
/*SPC(807)*/
_AVAbsAlt_Bind=BindIdn((* _IG_incl0), _AVAbsAlt_Sym);
/*SPC(220)*/
_AVAbsAlt_Key=KeyOf(_AVAbsAlt_Bind);
/*SPC(222)*/
_AVAbsAlt_SymbolPElem=
(SymbolKeySet(_AVAbsAlt_Symbol, _AVAbsAlt_Key), SymbolClassSet(_AVAbsAlt_Symbol, IDENTIFIER), ResetStr(_AVAbsAlt_Key, _AVAbsAlt_Sym), _AVAbsAlt_Symbol)
;
/*SPC(808)*/
_AVMapText_cSymbolPListPtr_post=
((_AVAbsAlt_SymbolPTakeIt
) ? (RefEndConsSymbolPList(_AVMapText_cSymbolPListPtr_post, _AVAbsAlt_SymbolPElem)
) : (_AVMapText_cSymbolPListPtr_post))
;
/*SPC(291)*/
ResetIsAbs(_AVAbsAlt_Key, 1);
/*SPC(1299)*/
ResetIsRHS(_AVAbsAlt_Key, 1);
/*SPC(1291)*/

_VisitExit();
}

