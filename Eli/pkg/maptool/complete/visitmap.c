
/* mapping of RULEcodes to visit procedures */

#include "visitprocs.h"

#include "visitmap.h"

_VPROCPTR VS8MAP[] = {
(_VPROCPTR)_VS8LST_0rule_39, (_VPROCPTR)_VS8LST_ConProdrule_39, (_VPROCPTR)_VS8LST_AbsProdrule_39, (_VPROCPTR)_VS8LST_MapSymbolrule_39, 
(_VPROCPTR)_VS8LST_MapRulerule_39, (_VPROCPTR)_VS8LST_MapChainsrule_39, (_VPROCPTR)_VS8LST_BottomUpRulerule_39
};
_VPROCPTR VS7MAP[] = {
(_VPROCPTR)_VS7LST_0rule_39, (_VPROCPTR)_VS7LST_ConProdrule_39, (_VPROCPTR)_VS7LST_AbsProdrule_39, (_VPROCPTR)_VS7LST_MapSymbolrule_39, 
(_VPROCPTR)_VS7LST_MapRulerule_39, (_VPROCPTR)_VS7LST_MapChainsrule_39, (_VPROCPTR)_VS7LST_BottomUpRulerule_39
};
_VPROCPTR VS6MAP[] = {
(_VPROCPTR)_VS6LST_0rule_39, (_VPROCPTR)_VS6LST_ConProdrule_39, (_VPROCPTR)_VS6LST_AbsProdrule_39, (_VPROCPTR)_VS6LST_MapSymbolrule_39, 
(_VPROCPTR)_VS6LST_MapRulerule_39, (_VPROCPTR)_VS6LST_MapChainsrule_39, (_VPROCPTR)_VS6LST_BottomUpRulerule_39, (_VPROCPTR)_VS6rule_21
};
_VPROCPTR VS5MAP[] = {
(_VPROCPTR)_VS5LST_0rule_39, (_VPROCPTR)_VS5LST_ConProdrule_39, (_VPROCPTR)_VS5LST_AbsProdrule_39, (_VPROCPTR)_VS5LST_MapSymbolrule_39, 
(_VPROCPTR)_VS5LST_MapRulerule_39, (_VPROCPTR)_VS5LST_MapChainsrule_39, (_VPROCPTR)_VS5LST_BottomUpRulerule_39, (_VPROCPTR)_VS5rule_21, (_VPROCPTR)_VS5rule_38, 
(_VPROCPTR)_VS5rule_29, (_VPROCPTR)_VS5rule_28, (_VPROCPTR)_VS5rule_27, (_VPROCPTR)_VS5rule_26, (_VPROCPTR)_VS5rule_25, 
(_VPROCPTR)_VS5rule_24, (_VPROCPTR)_VS5rule_23, (_VPROCPTR)_VS5rule_22, (_VPROCPTR)_VS5LST_0rule_38, (_VPROCPTR)_VS5LST_ConElementrule_38
};
_VPROCPTR VS4MAP[] = {
(_VPROCPTR)_VS4LST_0rule_39, (_VPROCPTR)_VS4LST_ConProdrule_39, (_VPROCPTR)_VS4LST_AbsProdrule_39, (_VPROCPTR)_VS4LST_MapSymbolrule_39, 
(_VPROCPTR)_VS4LST_MapRulerule_39, (_VPROCPTR)_VS4LST_MapChainsrule_39, (_VPROCPTR)_VS4LST_BottomUpRulerule_39, (_VPROCPTR)_VS4rule_21, (_VPROCPTR)_VS4rule_38, 
(_VPROCPTR)_VS4rule_29, (_VPROCPTR)_VS4rule_28, (_VPROCPTR)_VS4rule_27, (_VPROCPTR)_VS4rule_26, (_VPROCPTR)_VS4rule_25, 
(_VPROCPTR)_VS4rule_24, (_VPROCPTR)_VS4rule_23, (_VPROCPTR)_VS4rule_22, (_VPROCPTR)_VS4LST_0rule_38, (_VPROCPTR)_VS4LST_ConElementrule_38, 
(_VPROCPTR)_VS4rule_19, (_VPROCPTR)_VS4rule_15
};
_VPROCPTR VS3MAP[] = {
(_VPROCPTR)_VS3LST_0rule_39, (_VPROCPTR)_VS3LST_ConProdrule_39, (_VPROCPTR)_VS3LST_AbsProdrule_39, (_VPROCPTR)_VS3LST_MapSymbolrule_39, 
(_VPROCPTR)_VS3LST_MapRulerule_39, (_VPROCPTR)_VS3LST_MapChainsrule_39, (_VPROCPTR)_VS3LST_BottomUpRulerule_39, (_VPROCPTR)_VS3rule_21, (_VPROCPTR)_VS3rule_38, 
(_VPROCPTR)_VS3rule_29, (_VPROCPTR)_VS3rule_28, (_VPROCPTR)_VS3rule_27, (_VPROCPTR)_VS3rule_26, (_VPROCPTR)_VS3rule_25, 
(_VPROCPTR)_VS3rule_24, (_VPROCPTR)_VS3rule_23, (_VPROCPTR)_VS3rule_22, (_VPROCPTR)_VS3LST_0rule_38, (_VPROCPTR)_VS3LST_ConElementrule_38, 
(_VPROCPTR)_VS3rule_19, (_VPROCPTR)_VS3rule_15, (_VPROCPTR)_VS3rule_32, (_VPROCPTR)_VS3rule_20, (_VPROCPTR)_VS3rule_12, 
(_VPROCPTR)_VS3rule_9
};
_VPROCPTR VS2MAP[] = {
(_VPROCPTR)_VS2LST_0rule_39, (_VPROCPTR)_VS2LST_ConProdrule_39, (_VPROCPTR)_VS2LST_AbsProdrule_39, (_VPROCPTR)_VS2LST_MapSymbolrule_39, 
(_VPROCPTR)_VS2LST_MapRulerule_39, (_VPROCPTR)_VS2LST_MapChainsrule_39, (_VPROCPTR)_VS2LST_BottomUpRulerule_39, (_VPROCPTR)_VS2rule_21, (_VPROCPTR)_VS2rule_38, 
(_VPROCPTR)_VS2rule_29, (_VPROCPTR)_VS2rule_28, (_VPROCPTR)_VS2rule_27, (_VPROCPTR)_VS2rule_26, (_VPROCPTR)_VS2rule_25, 
(_VPROCPTR)_VS2rule_24, (_VPROCPTR)_VS2rule_23, (_VPROCPTR)_VS2rule_22, (_VPROCPTR)_VS2LST_0rule_38, (_VPROCPTR)_VS2LST_ConElementrule_38, 
(_VPROCPTR)_VS2rule_19, (_VPROCPTR)_VS2rule_15, (_VPROCPTR)_VS2rule_32, (_VPROCPTR)_VS2rule_20, (_VPROCPTR)_VS2rule_12, 
(_VPROCPTR)_VS2rule_9, (_VPROCPTR)_VS2rule_04, (_VPROCPTR)_VS2rule_37, (_VPROCPTR)_VS2rule_35, (_VPROCPTR)_VS2rule_34, 
(_VPROCPTR)_VS2rule_33, (_VPROCPTR)_VS2rule_30, (_VPROCPTR)_VS2rule_18, (_VPROCPTR)_VS2rule_17, (_VPROCPTR)_VS2rule_16, 
(_VPROCPTR)_VS2rule_14, (_VPROCPTR)_VS2rule_13, (_VPROCPTR)_VS2rule_11, (_VPROCPTR)_VS2rule_10, (_VPROCPTR)_VS2rule_8, 
(_VPROCPTR)_VS2rule_7, (_VPROCPTR)_VS2rule_6, (_VPROCPTR)_VS2rule_5, (_VPROCPTR)_VS2rule_4, (_VPROCPTR)_VS2rule_2, 
(_VPROCPTR)_VS2LST_0rule_33, (_VPROCPTR)_VS2LST_MapPositionrule_33, (_VPROCPTR)_VS2LST_MapTextrule_33, (_VPROCPTR)_VS2LST_0rule_34, (_VPROCPTR)_VS2LST_MapElementrule_34, 
(_VPROCPTR)_VS2LST_0rule_35, (_VPROCPTR)_VS2LST_MapMemberrule_35, (_VPROCPTR)_VS2LST_0rule_37, (_VPROCPTR)_VS2LST_AbsElementrule_37
};
_VPROCPTR VS1MAP[] = {
(_VPROCPTR)_VS1LST_0rule_39, (_VPROCPTR)_VS1LST_ConProdrule_39, (_VPROCPTR)_VS1LST_AbsProdrule_39, (_VPROCPTR)_VS1LST_MapSymbolrule_39, 
(_VPROCPTR)_VS1LST_MapRulerule_39, (_VPROCPTR)_VS1LST_MapChainsrule_39, (_VPROCPTR)_VS1LST_BottomUpRulerule_39, (_VPROCPTR)_VS1rule_21, (_VPROCPTR)_VS1rule_38, 
(_VPROCPTR)_VS1rule_29, (_VPROCPTR)_VS1rule_28, (_VPROCPTR)_VS1rule_27, (_VPROCPTR)_VS1rule_26, (_VPROCPTR)_VS1rule_25, 
(_VPROCPTR)_VS1rule_24, (_VPROCPTR)_VS1rule_23, (_VPROCPTR)_VS1rule_22, (_VPROCPTR)_VS1LST_0rule_38, (_VPROCPTR)_VS1LST_ConElementrule_38, 
(_VPROCPTR)_VS1rule_19, (_VPROCPTR)_VS1rule_15, (_VPROCPTR)_VS1rule_32, (_VPROCPTR)_VS1rule_20, (_VPROCPTR)_VS1rule_12, 
(_VPROCPTR)_VS1rule_9, (_VPROCPTR)_VS1rule_04, (_VPROCPTR)_VS1rule_37, (_VPROCPTR)_VS1rule_35, (_VPROCPTR)_VS1rule_34, 
(_VPROCPTR)_VS1rule_33, (_VPROCPTR)_VS1rule_30, (_VPROCPTR)_VS1rule_18, (_VPROCPTR)_VS1rule_17, (_VPROCPTR)_VS1rule_16, 
(_VPROCPTR)_VS1rule_14, (_VPROCPTR)_VS1rule_13, (_VPROCPTR)_VS1rule_11, (_VPROCPTR)_VS1rule_10, (_VPROCPTR)_VS1rule_8, 
(_VPROCPTR)_VS1rule_7, (_VPROCPTR)_VS1rule_6, (_VPROCPTR)_VS1rule_5, (_VPROCPTR)_VS1rule_4, (_VPROCPTR)_VS1rule_2, 
(_VPROCPTR)_VS1LST_0rule_33, (_VPROCPTR)_VS1LST_MapPositionrule_33, (_VPROCPTR)_VS1LST_MapTextrule_33, (_VPROCPTR)_VS1LST_0rule_34, (_VPROCPTR)_VS1LST_MapElementrule_34, 
(_VPROCPTR)_VS1LST_0rule_35, (_VPROCPTR)_VS1LST_MapMemberrule_35, (_VPROCPTR)_VS1LST_0rule_37, (_VPROCPTR)_VS1LST_AbsElementrule_37, (_VPROCPTR)_VS1rule_01, 
(_VPROCPTR)_VS1rule_02, (_VPROCPTR)_VS1rule_03, (_VPROCPTR)_VS1rule_39, (_VPROCPTR)_VS1rule_36, (_VPROCPTR)_VS1rule_31, 
(_VPROCPTR)_VS1rule_3, (_VPROCPTR)_VS1rule_1, (_VPROCPTR)_VS1LST_0rule_36, (_VPROCPTR)_VS1LST_AbsAltrule_36
};
