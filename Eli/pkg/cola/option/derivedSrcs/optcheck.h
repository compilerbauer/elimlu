/***********************************************************************************\
*										    *
*   Option-Tool for Parser Generators						    *
*   Author: M.Jung 								    *
*   										    *
*   Headerfile defining Prototypes for Value Checking                               *
\***********************************************************************************/


#ifndef OPTCHECK_H
#define OPTCHECK_H

#if defined(__STDC__) || defined(__cplusplus)
int testval(int, int, POSITION *);
#else
int testval();
#endif

#endif
