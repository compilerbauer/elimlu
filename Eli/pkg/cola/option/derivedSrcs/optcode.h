/*****************************************************************************\
*								       	     *
*   Option-Tool for Parser Generators					     *
*   Author: M.Jung 							     *
*   									     *
*   Code-Generation.							     *
\*****************************************************************************/


#ifndef OPTCODE_H
#define OPTCODE_H

PTGNode DirectOptionCode();
PTGNode TableOptionCode();
PTGNode FrontendOptionCode();
PTGNode TableOptionCode2();

#endif
