
#include "HEAD.h"
#include "err.h"
#include "node.h"
#include "visitprocs.h"
#include "attrpredef.h"

#include "visitmap.h"

#ifdef MONITOR
#include "attr_mon_dapto.h"
#include "liga_dapto.h"
#endif

#ifndef _VisitVarDecl
#define _VisitVarDecl()
#endif

#ifndef _VisitEntry
#define _VisitEntry()
#endif

#ifndef _VisitExit
#define _VisitExit()
#endif


#if defined(__STDC__) || defined(__cplusplus)
#define _CALL_VS_(args) (void (*)args)
#else
#define _CALL_VS_(args) 
#endif
int* _IG_incl3;
int* _IG_incl5;
Environment* _IG_incl2;
ProdSymbolListPtr _AVSyntLit_cProdSymbolListPtr_post;
DefTableKey _AVRuleSpecId_Key;
DefTableKey _AVRuleSpec_Key;
RuleProd _AVRuleSpec_Rule;
int _AVSyntLit_ProdSymbolTakeIt;
ProdSymbol _AVSyntLit_ProdSymbolElem;
ProdSymbolList _AVProduction__ProdSymbolauxList;
int _AVProduction_IsListof;
ProdSymbolList _AVProduction_ProdSymbolList;
int _AVSymClass_IsCLASSSym;
int _AVSymClass_IsTREESym;
Binding _AVRuleId_Bind;
Binding _AVTypeId_Bind;
int _AVSyntId_ProdSymbolTakeIt;
Binding _AVSyntId_Bind;
ProdSymbol _AVSyntId_ProdSymbolElem;
int _AVSyntId_IsGenSymbol;
Binding _AVSymbolId_Bind;
Binding _AVSymbolDefId_Bind;

#if defined(__STDC__) || defined(__cplusplus)
void LIGA_ATTREVAL (NODEPTR _currn)
#else
void LIGA_ATTREVAL (_currn) NODEPTR _currn;
#endif
{(*(VS1MAP[_currn->_prod])) ((NODEPTR)_currn);}
/*SPC(0)*/

#if defined(__STDC__) || defined(__cplusplus)
void _VS0Empty(NODEPTR _currn)
#else
void _VS0Empty(_currn) NODEPTR _currn;
#endif
{ _VisitVarDecl()
_VisitEntry();

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_1(_TPPrule_1 _currn)
#else
void _VS1rule_1(_currn )
_TPPrule_1 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVProduction__ProdSymbolauxList=NULLProdSymbolList;
/*SPC(111)*/
_AVSyntLit_cProdSymbolListPtr_post=_ProdSymbolListADDROF(_AVProduction__ProdSymbolauxList);
/*SPC(112)*/
_AVSyntId_IsGenSymbol=0;
/*SPC(381)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVProduction_IsListof=1;
/*SPC(436)*/
_AVProduction_ProdSymbolList=_AVProduction__ProdSymbolauxList;
/*SPC(113)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_1(_TPPrule_1 _currn)
#else
void _VS2rule_1(_currn )
_TPPrule_1 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_2(_TPPrule_2 _currn)
#else
void _VS1rule_2(_currn )
_TPPrule_2 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVProduction__ProdSymbolauxList=NULLProdSymbolList;
/*SPC(111)*/
_AVSyntLit_cProdSymbolListPtr_post=_ProdSymbolListADDROF(_AVProduction__ProdSymbolauxList);
/*SPC(112)*/
_AVSyntId_IsGenSymbol=0;
/*SPC(381)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_AVProduction_IsListof=0;
/*SPC(432)*/
_AVProduction_ProdSymbolList=_AVProduction__ProdSymbolauxList;
/*SPC(113)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_2(_TPPrule_2 _currn)
#else
void _VS2rule_2(_currn )
_TPPrule_2 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_3(_TPPrule_3 _currn)
#else
void _VS1rule_3(_currn )
_TPPrule_3 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVRuleSpecId_Key=NoKey;
/*SPC(411)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_4(_TPPrule_4 _currn)
#else
void _VS1rule_4(_currn )
_TPPrule_4 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_AVRuleSpecId_Key=_currn->_desc1->_ATKey;
/*SPC(407)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_4(_TPPrule_4 _currn)
#else
void _VS2rule_4(_currn )
_TPPrule_4 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsSymbol(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as symbol identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(261)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_5(_TPPrule_5 _currn)
#else
void _VS1rule_5(_currn )
_TPPrule_5 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
_currn->_ATRuleInstance=MakeRuleProd(_AVRuleSpecId_Key, _AVProduction_ProdSymbolList, _AVProduction_IsListof, (&( _currn->_AT_pos)));
/*SPC(399)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_5(_TPPrule_5 _currn)
#else
void _VS2rule_5(_currn )
_TPPrule_5 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
_AVRuleSpec_Key=RuleKeyOfRuleProd(_currn->_ATRuleInstance);
/*SPC(425)*/
_AVRuleSpec_Rule=GetRule(_AVRuleSpec_Key, NoRuleProd);
/*SPC(427)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_6(_TPPrule_6 _currn)
#else
void _VS1rule_6(_currn )
_TPPrule_6 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSyntId_IsGenSymbol=1;
/*SPC(388)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_6(_TPPrule_6 _currn)
#else
void _VS2rule_6(_currn )
_TPPrule_6 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_7(_TPPrule_7 _currn)
#else
void _VS1rule_7(_currn )
_TPPrule_7 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSyntLit_ProdSymbolTakeIt=1;
/*SPC(117)*/
_AVSyntLit_ProdSymbolElem=MakeProdLiteral(_currn->_ATTERM_1);
/*SPC(370)*/
_AVSyntLit_cProdSymbolListPtr_post=
((_AVSyntLit_ProdSymbolTakeIt
) ? (RefEndConsProdSymbolList(_AVSyntLit_cProdSymbolListPtr_post, _AVSyntLit_ProdSymbolElem)
) : (_AVSyntLit_cProdSymbolListPtr_post))
;
/*SPC(118)*/

if (EQ(0, strlen(StringTable(_currn->_ATTERM_1)))) {
message(ERROR, "Literal terminal may not be the empty string", 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(376)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_8(_TPPrule_8 _currn)
#else
void _VS1rule_8(_currn )
_TPPrule_8 _currn;

#endif
{
int* _IL_incl3;
int* _IL_incl5;

_VisitVarDecl()
_VisitEntry();
_IL_incl3=_IG_incl3;_IG_incl3= &(_currn->_ATIsCLASSSym);
_IL_incl5=_IG_incl5;_IG_incl5= &(_currn->_ATIsTREESym);
_currn->_ATIsCLASSSym=0;
/*SPC(320)*/
_currn->_ATIsTREESym=1;
/*SPC(319)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_IG_incl3=_IL_incl3;
_IG_incl5=_IL_incl5;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_8(_TPPrule_8 _currn)
#else
void _VS2rule_8(_currn )
_TPPrule_8 _currn;

#endif
{
int* _IL_incl3;
int* _IL_incl5;

_VisitVarDecl()
_VisitEntry();
_IL_incl3=_IG_incl3;_IG_incl3= &(_currn->_ATIsCLASSSym);
_IL_incl5=_IG_incl5;_IG_incl5= &(_currn->_ATIsTREESym);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

if (GetIsSymbol(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as symbol identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(261)*/

if (GetIsRule(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
_IG_incl3=_IL_incl3;
_IG_incl5=_IL_incl5;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_9(_TPPrule_9 _currn)
#else
void _VS1rule_9(_currn )
_TPPrule_9 _currn;

#endif
{
int* _IL_incl3;
int* _IL_incl5;

_VisitVarDecl()
_VisitEntry();
_IL_incl3=_IG_incl3;_IG_incl3= &(_currn->_ATIsCLASSSym);
_IL_incl5=_IG_incl5;_IG_incl5= &(_currn->_ATIsTREESym);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_currn->_ATIsCLASSSym=_AVSymClass_IsCLASSSym;
/*SPC(316)*/
_currn->_ATIsTREESym=_AVSymClass_IsTREESym;
/*SPC(315)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc4->_prod])))((NODEPTR) _currn->_desc4);
_IG_incl3=_IL_incl3;
_IG_incl5=_IL_incl5;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_9(_TPPrule_9 _currn)
#else
void _VS2rule_9(_currn )
_TPPrule_9 _currn;

#endif
{
int* _IL_incl3;
int* _IL_incl5;

_VisitVarDecl()
_VisitEntry();
_IL_incl3=_IG_incl3;_IG_incl3= &(_currn->_ATIsCLASSSym);
_IL_incl5=_IG_incl5;_IG_incl5= &(_currn->_ATIsTREESym);

if (GetIsRule(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc4->_prod])))((NODEPTR) _currn->_desc4);
_IG_incl3=_IL_incl3;
_IG_incl5=_IL_incl5;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_10(_TPPrule_10 _currn)
#else
void _VS1rule_10(_currn )
_TPPrule_10 _currn;

#endif
{
int* _IL_incl3;
int* _IL_incl5;

_VisitVarDecl()
_VisitEntry();
_IL_incl3=_IG_incl3;_IG_incl3= &(_currn->_ATIsCLASSSym);
_IL_incl5=_IG_incl5;_IG_incl5= &(_currn->_ATIsTREESym);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
_currn->_ATIsCLASSSym=_AVSymClass_IsCLASSSym;
/*SPC(311)*/
_currn->_ATIsTREESym=_AVSymClass_IsTREESym;
/*SPC(310)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
_IG_incl3=_IL_incl3;
_IG_incl5=_IL_incl5;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_10(_TPPrule_10 _currn)
#else
void _VS2rule_10(_currn )
_TPPrule_10 _currn;

#endif
{
int* _IL_incl3;
int* _IL_incl5;

_VisitVarDecl()
_VisitEntry();
_IL_incl3=_IG_incl3;_IG_incl3= &(_currn->_ATIsCLASSSym);
_IL_incl5=_IG_incl5;_IG_incl5= &(_currn->_ATIsTREESym);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
_IG_incl3=_IL_incl3;
_IG_incl5=_IL_incl5;

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_11(_TPPrule_11 _currn)
#else
void _VS1rule_11(_currn )
_TPPrule_11 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSymClass_IsCLASSSym=1;
/*SPC(303)*/
_AVSymClass_IsTREESym=0;
/*SPC(306)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_12(_TPPrule_12 _currn)
#else
void _VS1rule_12(_currn )
_TPPrule_12 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSymClass_IsCLASSSym=0;
/*SPC(306)*/
_AVSymClass_IsTREESym=1;
/*SPC(300)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_13(_TPPrule_13 _currn)
#else
void _VS1rule_13(_currn )
_TPPrule_13 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0125(_TPPrule_0125 _currn)
#else
void _VS1rule_0125(_currn )
_TPPrule_0125 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVTypeId_Bind=BindIdn((* _IG_incl2), _currn->_ATSym);
/*SPC(47)*/
_currn->_ATKey=KeyOf(_AVTypeId_Bind);
/*SPC(49)*/
ResetIsType(_currn->_ATKey, 1);
/*SPC(253)*/
ResetNameSym(_currn->_ATKey, _currn->_ATSym);
SetOnceCoord(_currn->_ATKey, (&( _currn->_AT_pos)));
;
/*SPC(215)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0124(_TPPrule_0124 _currn)
#else
void _VS1rule_0124(_currn )
_TPPrule_0124 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0123(_TPPrule_0123 _currn)
#else
void _VS1rule_0123(_currn )
_TPPrule_0123 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSyntId_IsGenSymbol=0;
/*SPC(381)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_0123(_TPPrule_0123 _currn)
#else
void _VS2rule_0123(_currn )
_TPPrule_0123 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0122(_TPPrule_0122 _currn)
#else
void _VS1rule_0122(_currn )
_TPPrule_0122 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_0122(_TPPrule_0122 _currn)
#else
void _VS2rule_0122(_currn )
_TPPrule_0122 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0120(_TPPrule_0120 _currn)
#else
void _VS1rule_0120(_currn )
_TPPrule_0120 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSyntId_ProdSymbolTakeIt=1;
/*SPC(117)*/
_AVSyntId_Bind=BindIdn((* _IG_incl2), _currn->_ATSym);
/*SPC(47)*/
_currn->_ATKey=KeyOf(_AVSyntId_Bind);
/*SPC(49)*/
ResetIsSymbol(_currn->_ATKey, 1);
/*SPC(249)*/
ResetNameSym(_currn->_ATKey, _currn->_ATSym);
SetOnceCoord(_currn->_ATKey, (&( _currn->_AT_pos)));
;
/*SPC(215)*/
_AVSyntId_ProdSymbolElem=MakeProdSymbol(_currn->_ATKey, _AVSyntId_IsGenSymbol, (&( _currn->_AT_pos)));
/*SPC(383)*/
_AVSyntLit_cProdSymbolListPtr_post=
((_AVSyntId_ProdSymbolTakeIt
) ? (RefEndConsProdSymbolList(_AVSyntLit_cProdSymbolListPtr_post, _AVSyntId_ProdSymbolElem)
) : (_AVSyntLit_cProdSymbolListPtr_post))
;
/*SPC(118)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0116(_TPPrule_0116 _currn)
#else
void _VS1rule_0116(_currn )
_TPPrule_0116 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
ResetIsSymbol(_currn->_desc1->_ATKey, 1);
/*SPC(237)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_0116(_TPPrule_0116 _currn)
#else
void _VS2rule_0116(_currn )
_TPPrule_0116 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0113(_TPPrule_0113 _currn)
#else
void _VS1rule_0113(_currn )
_TPPrule_0113 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSymClass_IsCLASSSym=0;
/*SPC(306)*/
_AVSymClass_IsTREESym=0;
/*SPC(306)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0112(_TPPrule_0112 _currn)
#else
void _VS1rule_0112(_currn )
_TPPrule_0112 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0111(_TPPrule_0111 _currn)
#else
void _VS1rule_0111(_currn )
_TPPrule_0111 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSymbolId_Bind=BindIdn((* _IG_incl2), _currn->_ATSym);
/*SPC(47)*/
_currn->_ATKey=KeyOf(_AVSymbolId_Bind);
/*SPC(49)*/
ResetNameSym(_currn->_ATKey, _currn->_ATSym);
SetOnceCoord(_currn->_ATKey, (&( _currn->_AT_pos)));
;
/*SPC(215)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_0110(_TPPrule_0110 _currn)
#else
void _VS2rule_0110(_currn )
_TPPrule_0110 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_0109(_TPPrule_0109 _currn)
#else
void _VS1rule_0109(_currn )
_TPPrule_0109 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSymbolDefId_Bind=BindIdn((* _IG_incl2), _currn->_ATSym);
/*SPC(47)*/
_currn->_ATKey=KeyOf(_AVSymbolDefId_Bind);
/*SPC(49)*/
ResetIsSymbol(_currn->_ATKey, 1);
/*SPC(233)*/
ResetNameSym(_currn->_ATKey, _currn->_ATSym);
SetOnceCoord(_currn->_ATKey, (&( _currn->_AT_pos)));
;
/*SPC(215)*/

if ((* _IG_incl5)) {
ResetIsTREESym(_currn->_ATKey, 1);

} else {
}
;

if ((* _IG_incl3)) {
ResetIsCLASSSym(_currn->_ATKey, 1);

} else {
}
;
;
/*SPC(325)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_0109(_TPPrule_0109 _currn)
#else
void _VS2rule_0109(_currn )
_TPPrule_0109 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (AND(GetIsTREESym(_currn->_ATKey, 0), AND((* _IG_incl3), NOT(GetIsCLASSReported(_currn->_ATKey, 0))))) {
ResetIsCLASSReported(_currn->_ATKey, 1);
message(ERROR, CatStrInd("Occurs as TREE symbol, too: ", _currn->_ATSym), 0, (&( _currn->_AT_pos)));
;

} else {
}
;
/*SPC(359)*/

if (AND(GetIsCLASSSym(_currn->_ATKey, 0), AND((* _IG_incl5), NOT(GetIsTREEReported(_currn->_ATKey, 0))))) {
ResetIsTREEReported(_currn->_ATKey, 1);
message(ERROR, CatStrInd("Occurs as CLASS symbol, too: ", _currn->_ATSym), 0, (&( _currn->_AT_pos)));
;

} else {
}
;
/*SPC(348)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_098(_TPPrule_098 _currn)
#else
void _VS1rule_098(_currn )
_TPPrule_098 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
ResetIsSymbol(_currn->_desc1->_ATKey, 1);
/*SPC(237)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_098(_TPPrule_098 _currn)
#else
void _VS2rule_098(_currn )
_TPPrule_098 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_090(_TPPrule_090 _currn)
#else
void _VS1rule_090(_currn )
_TPPrule_090 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVRuleId_Bind=BindIdn((* _IG_incl2), _currn->_ATSym);
/*SPC(47)*/
_currn->_ATKey=KeyOf(_AVRuleId_Bind);
/*SPC(49)*/
ResetIsRule(_currn->_ATKey, 1);
/*SPC(257)*/
ResetNameSym(_currn->_ATKey, _currn->_ATSym);
SetOnceCoord(_currn->_ATKey, (&( _currn->_AT_pos)));
;
/*SPC(215)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_088(_TPPrule_088 _currn)
#else
void _VS1rule_088(_currn )
_TPPrule_088 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc4->_prod])))((NODEPTR) _currn->_desc4);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_088(_TPPrule_088 _currn)
#else
void _VS2rule_088(_currn )
_TPPrule_088 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc4->_prod])))((NODEPTR) _currn->_desc4);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_087(_TPPrule_087 _currn)
#else
void _VS1rule_087(_currn )
_TPPrule_087 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_087(_TPPrule_087 _currn)
#else
void _VS2rule_087(_currn )
_TPPrule_087 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_083(_TPPrule_083 _currn)
#else
void _VS1rule_083(_currn )
_TPPrule_083 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
ResetIsSymbol(_currn->_desc1->_ATKey, 1);
/*SPC(237)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_083(_TPPrule_083 _currn)
#else
void _VS2rule_083(_currn )
_TPPrule_083 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_072(_TPPrule_072 _currn)
#else
void _VS1rule_072(_currn )
_TPPrule_072 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
ResetIsSymbol(_currn->_desc1->_ATKey, 1);
/*SPC(237)*/

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_072(_TPPrule_072 _currn)
#else
void _VS2rule_072(_currn )
_TPPrule_072 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_065(_TPPrule_065 _currn)
#else
void _VS1rule_065(_currn )
_TPPrule_065 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_065(_TPPrule_065 _currn)
#else
void _VS2rule_065(_currn )
_TPPrule_065 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_064(_TPPrule_064 _currn)
#else
void _VS1rule_064(_currn )
_TPPrule_064 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_064(_TPPrule_064 _currn)
#else
void _VS2rule_064(_currn )
_TPPrule_064 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

if (GetIsRule(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_052(_TPPrule_052 _currn)
#else
void _VS1rule_052(_currn )
_TPPrule_052 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_052(_TPPrule_052 _currn)
#else
void _VS2rule_052(_currn )
_TPPrule_052 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsSymbol(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as symbol identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(261)*/

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_029(_TPPrule_029 _currn)
#else
void _VS1rule_029(_currn )
_TPPrule_029 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_029(_TPPrule_029 _currn)
#else
void _VS2rule_029(_currn )
_TPPrule_029 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsSymbol(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as symbol identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(261)*/

if (GetIsRule(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_023(_TPPrule_023 _currn)
#else
void _VS1rule_023(_currn )
_TPPrule_023 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_023(_TPPrule_023 _currn)
#else
void _VS2rule_023(_currn )
_TPPrule_023 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsSymbol(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as symbol identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(261)*/

if (GetIsRule(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_012(_TPPrule_012 _currn)
#else
void _VS1rule_012(_currn )
_TPPrule_012 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_012(_TPPrule_012 _currn)
#else
void _VS2rule_012(_currn )
_TPPrule_012 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsSymbol(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as symbol identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(261)*/

if (GetIsRule(_currn->_desc2->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc2->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc2->_prod])))((NODEPTR) _currn->_desc2);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_010(_TPPrule_010 _currn)
#else
void _VS2rule_010(_currn )
_TPPrule_010 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc3->_prod])))((NODEPTR) _currn->_desc3);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_05(_TPPrule_05 _currn)
#else
void _VS1rule_05(_currn )
_TPPrule_05 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();
_AVSyntId_IsGenSymbol=0;
/*SPC(381)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS2rule_05(_TPPrule_05 _currn)
#else
void _VS2rule_05(_currn )
_TPPrule_05 _currn;

#endif
{

_VisitVarDecl()
_VisitEntry();

if (GetIsRule(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as rule identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(281)*/

if (GetIsType(_currn->_desc1->_ATKey, 0)) {
message(ERROR, CatStrInd("Used as type identifier elsewhere: ", _currn->_desc1->_ATSym), 0, (&( _currn->_AT_pos)));

} else {
}
;
/*SPC(271)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);

_VisitExit();
}

#if defined(__STDC__) || defined(__cplusplus)
void _VS1rule_01(_TPPrule_01 _currn)
#else
void _VS1rule_01(_currn )
_TPPrule_01 _currn;

#endif
{
Environment* _IL_incl2;

_VisitVarDecl()
_VisitEntry();
_IL_incl2=_IG_incl2;_IG_incl2= &(_currn->_ATEnv);
_currn->_ATEnv=RootEnv;
/*SPC(12)*/
(*(_CALL_VS_((NODEPTR )) (VS1MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
MakeGrammar(_currn->_ATEnv);
/*SPC(416)*/
(*(_CALL_VS_((NODEPTR )) (VS2MAP[_currn->_desc1->_prod])))((NODEPTR) _currn->_desc1);
PTGOutFile("ligaprod", OutputRules());
/*SPC(442)*/
_IG_incl2=_IL_incl2;

_VisitExit();
}

