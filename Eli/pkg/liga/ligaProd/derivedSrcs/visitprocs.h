
#ifndef _VISITPROCS_H
#define _VISITPROCS_H

#include "HEAD.h"
#include "node.h"
#include "treecon.h"

#include "eliproto.h"


extern void LIGA_ATTREVAL ELI_ARG((NODEPTR));
extern void _VS0Empty ELI_ARG((NODEPTR _currn));
extern void _VS1rule_1 ELI_ARG((_TPPrule_1 _currn));
extern void _VS2rule_1 ELI_ARG((_TPPrule_1 _currn));
extern void _VS1rule_2 ELI_ARG((_TPPrule_2 _currn));
extern void _VS2rule_2 ELI_ARG((_TPPrule_2 _currn));
extern void _VS1rule_3 ELI_ARG((_TPPrule_3 _currn));
#define _VS2rule_3 _VS0Empty

extern void _VS1rule_4 ELI_ARG((_TPPrule_4 _currn));
extern void _VS2rule_4 ELI_ARG((_TPPrule_4 _currn));
extern void _VS1rule_5 ELI_ARG((_TPPrule_5 _currn));
extern void _VS2rule_5 ELI_ARG((_TPPrule_5 _currn));
extern void _VS1rule_6 ELI_ARG((_TPPrule_6 _currn));
extern void _VS2rule_6 ELI_ARG((_TPPrule_6 _currn));
extern void _VS1rule_7 ELI_ARG((_TPPrule_7 _currn));
extern void _VS1rule_8 ELI_ARG((_TPPrule_8 _currn));
extern void _VS2rule_8 ELI_ARG((_TPPrule_8 _currn));
extern void _VS1rule_9 ELI_ARG((_TPPrule_9 _currn));
extern void _VS2rule_9 ELI_ARG((_TPPrule_9 _currn));
extern void _VS1rule_10 ELI_ARG((_TPPrule_10 _currn));
extern void _VS2rule_10 ELI_ARG((_TPPrule_10 _currn));
extern void _VS1rule_11 ELI_ARG((_TPPrule_11 _currn));
extern void _VS1rule_12 ELI_ARG((_TPPrule_12 _currn));
extern void _VS1rule_13 ELI_ARG((_TPPrule_13 _currn));
extern void _VS1rule_0125 ELI_ARG((_TPPrule_0125 _currn));
#define _VS2rule_0125 _VS0Empty

extern void _VS1rule_0124 ELI_ARG((_TPPrule_0124 _currn));
#define _VS2rule_0124 _VS0Empty

extern void _VS1rule_0123 ELI_ARG((_TPPrule_0123 _currn));
extern void _VS2rule_0123 ELI_ARG((_TPPrule_0123 _currn));
extern void _VS1rule_0122 ELI_ARG((_TPPrule_0122 _currn));
extern void _VS2rule_0122 ELI_ARG((_TPPrule_0122 _currn));
#define _VS1rule_0121 _VS0Empty

#define _VS2rule_0121 _VS0Empty

extern void _VS1rule_0120 ELI_ARG((_TPPrule_0120 _currn));
#define _VS2rule_0120 _VS0Empty

#define _VS1rule_0119 _VS0Empty

#define _VS2rule_0119 _VS0Empty

#define _VS1rule_0118 _VS0Empty

#define _VS2rule_0118 _VS0Empty

#define _VS1rule_0117 _VS0Empty

#define _VS2rule_0117 _VS0Empty

extern void _VS1rule_0116 ELI_ARG((_TPPrule_0116 _currn));
extern void _VS2rule_0116 ELI_ARG((_TPPrule_0116 _currn));
#define _VS1rule_0115 _VS0Empty

#define _VS2rule_0115 _VS0Empty

#define _VS1rule_0114 _VS0Empty

#define _VS2rule_0114 _VS0Empty

extern void _VS1rule_0113 ELI_ARG((_TPPrule_0113 _currn));
extern void _VS1rule_0112 ELI_ARG((_TPPrule_0112 _currn));
extern void _VS1rule_0111 ELI_ARG((_TPPrule_0111 _currn));
#define _VS2rule_0111 _VS0Empty

#define _VS1rule_0110 _VS1rule_0112

extern void _VS2rule_0110 ELI_ARG((_TPPrule_0110 _currn));
extern void _VS1rule_0109 ELI_ARG((_TPPrule_0109 _currn));
extern void _VS2rule_0109 ELI_ARG((_TPPrule_0109 _currn));
#define _VS1rule_0108 _VS1rule_0112

#define _VS2rule_0108 _VS2rule_0110

#define _VS1rule_0107 _VS0Empty

#define _VS2rule_0107 _VS0Empty

#define _VS1rule_0106 _VS1rule_0112

#define _VS2rule_0106 _VS2rule_0110

#define _VS1rule_0105 _VS1rule_0112

#define _VS2rule_0105 _VS2rule_0110

#define _VS1rule_0104 _VS1rule_0112

#define _VS2rule_0104 _VS2rule_0110

#define _VS1rule_0103 _VS1rule_0122

#define _VS2rule_0103 _VS2rule_0122

#define _VS1rule_0102 _VS0Empty

#define _VS2rule_0102 _VS0Empty

#define _VS1rule_0101 _VS1rule_0112

#define _VS2rule_0101 _VS2rule_0110

#define _VS1rule_0100 _VS1rule_0112

#define _VS2rule_0100 _VS2rule_0110

#define _VS1rule_099 _VS1rule_0112

#define _VS2rule_099 _VS2rule_0110

extern void _VS1rule_098 ELI_ARG((_TPPrule_098 _currn));
extern void _VS2rule_098 ELI_ARG((_TPPrule_098 _currn));
#define _VS1rule_097 _VS1rule_0122

#define _VS2rule_097 _VS2rule_0122

#define _VS1rule_096 _VS1rule_0112

#define _VS2rule_096 _VS2rule_0110

#define _VS1rule_095 _VS1rule_0112

#define _VS2rule_095 _VS2rule_0110

#define _VS1rule_094 _VS1rule_0112

#define _VS2rule_094 _VS2rule_0110

#define _VS1rule_093 _VS1rule_0112

#define _VS2rule_093 _VS2rule_0110

#define _VS1rule_092 _VS0Empty

#define _VS2rule_092 _VS0Empty

#define _VS1rule_091 _VS0Empty

#define _VS2rule_091 _VS0Empty

extern void _VS1rule_090 ELI_ARG((_TPPrule_090 _currn));
#define _VS2rule_090 _VS0Empty

#define _VS1rule_089 _VS0Empty

extern void _VS1rule_088 ELI_ARG((_TPPrule_088 _currn));
extern void _VS2rule_088 ELI_ARG((_TPPrule_088 _currn));
extern void _VS1rule_087 ELI_ARG((_TPPrule_087 _currn));
extern void _VS2rule_087 ELI_ARG((_TPPrule_087 _currn));
#define _VS1rule_086 _VS1rule_0112

#define _VS2rule_086 _VS2rule_0110

#define _VS1rule_085 _VS1rule_0112

#define _VS2rule_085 _VS2rule_0110

#define _VS1rule_084 _VS1rule_0112

#define _VS2rule_084 _VS2rule_0110

extern void _VS1rule_083 ELI_ARG((_TPPrule_083 _currn));
extern void _VS2rule_083 ELI_ARG((_TPPrule_083 _currn));
#define _VS1rule_082 _VS1rule_0122

#define _VS2rule_082 _VS2rule_0122

#define _VS1rule_081 _VS1rule_0112

#define _VS2rule_081 _VS2rule_0110

#define _VS1rule_080 _VS1rule_0112

#define _VS2rule_080 _VS2rule_0110

#define _VS1rule_079 _VS1rule_0112

#define _VS2rule_079 _VS2rule_0110

#define _VS1rule_078 _VS1rule_0122

#define _VS2rule_078 _VS2rule_0122

#define _VS1rule_077 _VS1rule_0112

#define _VS2rule_077 _VS2rule_0110

#define _VS1rule_076 _VS1rule_0112

#define _VS2rule_076 _VS2rule_0110

#define _VS1rule_075 _VS0Empty

#define _VS2rule_075 _VS0Empty

#define _VS1rule_074 _VS1rule_0112

#define _VS2rule_074 _VS2rule_0110

#define _VS1rule_073 _VS1rule_087

#define _VS2rule_073 _VS2rule_087

extern void _VS1rule_072 ELI_ARG((_TPPrule_072 _currn));
extern void _VS2rule_072 ELI_ARG((_TPPrule_072 _currn));
#define _VS1rule_071 _VS1rule_0122

#define _VS2rule_071 _VS2rule_0122

#define _VS1rule_070 _VS1rule_0112

#define _VS2rule_070 _VS2rule_0110

#define _VS1rule_069 _VS1rule_0112

#define _VS2rule_069 _VS2rule_0110

#define _VS1rule_068 _VS0Empty

#define _VS2rule_068 _VS0Empty

#define _VS1rule_067 _VS0Empty

#define _VS1rule_066 _VS0Empty

extern void _VS1rule_065 ELI_ARG((_TPPrule_065 _currn));
extern void _VS2rule_065 ELI_ARG((_TPPrule_065 _currn));
extern void _VS1rule_064 ELI_ARG((_TPPrule_064 _currn));
extern void _VS2rule_064 ELI_ARG((_TPPrule_064 _currn));
#define _VS1rule_063 _VS1rule_0112

#define _VS2rule_063 _VS0Empty

#define _VS1rule_062 _VS1rule_0112

#define _VS2rule_062 _VS0Empty

#define _VS1rule_061 _VS1rule_0112

#define _VS2rule_061 _VS2rule_0110

#define _VS1rule_060 _VS0Empty

#define _VS2rule_060 _VS0Empty

#define _VS1rule_059 _VS1rule_0112

#define _VS2rule_059 _VS2rule_0110

#define _VS1rule_058 _VS1rule_0122

#define _VS2rule_058 _VS2rule_0122

#define _VS1rule_057 _VS1rule_0112

#define _VS2rule_057 _VS2rule_0110

#define _VS1rule_056 _VS0Empty

#define _VS2rule_056 _VS0Empty

#define _VS1rule_055 _VS0Empty

#define _VS2rule_055 _VS0Empty

#define _VS1rule_054 _VS0Empty

#define _VS2rule_054 _VS0Empty

#define _VS1rule_053 _VS1rule_0112

#define _VS2rule_053 _VS2rule_0110

extern void _VS1rule_052 ELI_ARG((_TPPrule_052 _currn));
extern void _VS2rule_052 ELI_ARG((_TPPrule_052 _currn));
#define _VS1rule_051 _VS0Empty

#define _VS2rule_051 _VS0Empty

#define _VS1rule_050 _VS1rule_0112

#define _VS2rule_050 _VS2rule_0110

#define _VS1rule_049 _VS1rule_0112

#define _VS2rule_049 _VS2rule_0110

#define _VS1rule_048 _VS1rule_0112

#define _VS2rule_048 _VS2rule_0110

#define _VS1rule_047 _VS1rule_0112

#define _VS2rule_047 _VS2rule_0110

#define _VS1rule_046 _VS1rule_0122

#define _VS2rule_046 _VS2rule_0122

#define _VS1rule_045 _VS1rule_0112

#define _VS2rule_045 _VS2rule_0110

#define _VS1rule_044 _VS1rule_0112

#define _VS2rule_044 _VS0Empty

#define _VS1rule_043 _VS1rule_0112

#define _VS2rule_043 _VS2rule_0110

#define _VS1rule_042 _VS1rule_0112

#define _VS2rule_042 _VS2rule_0110

#define _VS1rule_041 _VS1rule_0112

#define _VS2rule_041 _VS2rule_0110

#define _VS1rule_040 _VS1rule_0112

#define _VS2rule_040 _VS2rule_0110

#define _VS1rule_039 _VS1rule_0112

#define _VS2rule_039 _VS2rule_0110

#define _VS1rule_038 _VS1rule_0122

#define _VS2rule_038 _VS2rule_0122

#define _VS1rule_037 _VS0Empty

#define _VS2rule_037 _VS0Empty

#define _VS1rule_036 _VS1rule_0112

#define _VS2rule_036 _VS2rule_0110

#define _VS1rule_035 _VS1rule_0112

#define _VS2rule_035 _VS2rule_0110

#define _VS1rule_034 _VS1rule_0112

#define _VS2rule_034 _VS2rule_0110

#define _VS1rule_033 _VS0Empty

#define _VS2rule_033 _VS0Empty

#define _VS1rule_032 _VS0Empty

#define _VS1rule_031 _VS0Empty

#define _VS1rule_030 _VS0Empty

extern void _VS1rule_029 ELI_ARG((_TPPrule_029 _currn));
extern void _VS2rule_029 ELI_ARG((_TPPrule_029 _currn));
#define _VS1rule_028 _VS1rule_0122

#define _VS1rule_027 _VS1rule_0112

#define _VS1rule_026 _VS0Empty

#define _VS1rule_025 _VS0Empty

#define _VS1rule_024 _VS1rule_0122

#define _VS2rule_024 _VS2rule_0110

extern void _VS1rule_023 ELI_ARG((_TPPrule_023 _currn));
extern void _VS2rule_023 ELI_ARG((_TPPrule_023 _currn));
#define _VS1rule_022 _VS1rule_0122

#define _VS1rule_021 _VS1rule_0112

#define _VS1rule_020 _VS0Empty

#define _VS1rule_019 _VS1rule_0112

#define _VS2rule_019 _VS2rule_0110

#define _VS1rule_018 _VS0Empty

#define _VS2rule_018 _VS0Empty

#define _VS1rule_017 _VS1rule_0122

#define _VS2rule_017 _VS2rule_0122

#define _VS1rule_016 _VS1rule_0112

#define _VS2rule_016 _VS2rule_0110

#define _VS1rule_015 _VS1rule_0122

#define _VS1rule_014 _VS1rule_0112

#define _VS1rule_013 _VS0Empty

extern void _VS1rule_012 ELI_ARG((_TPPrule_012 _currn));
extern void _VS2rule_012 ELI_ARG((_TPPrule_012 _currn));
#define _VS1rule_011 _VS1rule_0122

#define _VS2rule_011 _VS2rule_0122

#define _VS1rule_010 _VS1rule_087

extern void _VS2rule_010 ELI_ARG((_TPPrule_010 _currn));
#define _VS1rule_09 _VS1rule_0122

#define _VS2rule_09 _VS2rule_0122

#define _VS1rule_08 _VS1rule_0112

#define _VS2rule_08 _VS0Empty

#define _VS1rule_07 _VS0Empty

#define _VS1rule_06 _VS0Empty

extern void _VS1rule_05 ELI_ARG((_TPPrule_05 _currn));
extern void _VS2rule_05 ELI_ARG((_TPPrule_05 _currn));
#define _VS1rule_04 _VS1rule_0122

#define _VS2rule_04 _VS2rule_0122

#define _VS1rule_03 _VS1rule_0112

#define _VS2rule_03 _VS2rule_0110

#define _VS1rule_02 _VS0Empty

#define _VS2rule_02 _VS0Empty

extern void _VS1rule_01 ELI_ARG((_TPPrule_01 _currn));
#endif
