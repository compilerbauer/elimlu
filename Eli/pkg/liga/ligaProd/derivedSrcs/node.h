
/* definition of tree node structure */

#ifndef NODE_H
#define NODE_H
#include "err.h"
#include "nodeptr.h" /* defines NODEPTR */
#include "HEAD.h"

#ifdef MONITOR
#define _NODECOMMON int _prod; POSITION _coord; int _uid;
#else
#define _NODECOMMON int _prod;
#endif
struct NODEPTR_struct {
        _NODECOMMON
#ifdef __cplusplus
        void* operator new(size_t size);
#endif
};

typedef struct _TSSpec* _TSPSpec;
typedef struct _TSShieldSyms* _TSPShieldSyms;
typedef struct _TSShieldSym* _TSPShieldSym;
typedef struct _TSShieldClause* _TSPShieldClause;
typedef struct _TSShield* _TSPShield;
typedef struct _TSSubtree* _TSPSubtree;
typedef struct _TSRemoteClause* _TSPRemoteClause;
typedef struct _TSRemoteAttr* _TSPRemoteAttr;
typedef struct _TSRemoteAttrs* _TSPRemoteAttrs;
typedef struct _TSParams* _TSPParams;
typedef struct _TSParam* _TSPParam;
typedef struct _TSInheritSym* _TSPInheritSym;
typedef struct _TSInheritSyms* _TSPInheritSyms;
typedef struct _TSG1* _TSPG1;
typedef struct _TSParamsOpt* _TSPParamsOpt;
typedef struct _TSExpression* _TSPExpression;
typedef struct _TSExpandOpt* _TSPExpandOpt;
typedef struct _TSDependence* _TSPDependence;
typedef struct _TSDepClause* _TSPDepClause;
typedef struct _TSDepAttrs* _TSPDepAttrs;
typedef struct _TSRhsAttrs* _TSPRhsAttrs;
typedef struct _TSRemoteExpression* _TSPRemoteExpression;
typedef struct _TSDepAttr* _TSPDepAttr;
typedef struct _TSPlainComp* _TSPPlainComp;
typedef struct _TSCompute* _TSPCompute;
typedef struct _TSComputation* _TSPComputation;
typedef struct _TSComputations* _TSPComputations;
typedef struct _TSChainSpec* _TSPChainSpec;
typedef struct _TSChainNames* _TSPChainNames;
typedef struct _TSChainName* _TSPChainName;
typedef struct _TSSymOcc* _TSPSymOcc;
typedef struct _TSAttrSpec* _TSPAttrSpec;
typedef struct _TSAttrNames* _TSPAttrNames;
typedef struct _TSAttrName* _TSPAttrName;
typedef struct _TSAttrDefs* _TSPAttrDefs;
typedef struct _TSAttrDefId* _TSPAttrDefId;
typedef struct _TSClass* _TSPClass;
typedef struct _TSAttrDefIds* _TSPAttrDefIds;
typedef struct _TSAttrDef* _TSPAttrDef;
typedef struct _TSLoop* _TSPLoop;
typedef struct _TSExpressionDep* _TSPExpressionDep;
typedef struct _TSDefAttr* _TSPDefAttr;
typedef struct _TSAttrComp* _TSPAttrComp;
typedef struct _TSAttrUseId* _TSPAttrUseId;
typedef struct _TSAttr* _TSPAttr;
typedef struct _TSAsgnTok* _TSPAsgnTok;
typedef struct _TSAlt* _TSPAlt;
typedef struct _TSSpecs* _TSPSpecs;
typedef struct _TSAlts* _TSPAlts;
typedef struct _TSSyntUnits* _TSPSyntUnits;
typedef struct _TSRuleSpecId* _TSPRuleSpecId;
typedef struct _TSRuleSpec* _TSPRuleSpec;
typedef struct _TSSyntUnit* _TSPSyntUnit;
typedef struct _TSSyntLit* _TSPSyntLit;
typedef struct _TSProduction* _TSPProduction;
typedef struct _TSCompPart* _TSPCompPart;
typedef struct _TSInheritOpt* _TSPInheritOpt;
typedef struct _TSAttrDefsOpt* _TSPAttrDefsOpt;
typedef struct _TSSymbolDefIds* _TSPSymbolDefIds;
typedef struct _TSSymClass* _TSPSymClass;
typedef struct _TSTermSpec* _TSPTermSpec;
typedef struct _TSSymCompSpec* _TSPSymCompSpec;
typedef struct _TSSymAttrSpec* _TSPSymAttrSpec;
typedef struct _TSIndex* _TSPIndex;
typedef struct _TSSymbolRef* _TSPSymbolRef;
typedef struct _TSAG* _TSPAG;
typedef struct _TSRuleId* _TSPRuleId;
typedef struct _TSTypeId* _TSPTypeId;
typedef struct _TSSyntId* _TSPSyntId;
typedef struct _TSSymbolId* _TSPSymbolId;
typedef struct _TSSymbolDefId* _TSPSymbolDefId;
typedef struct _TPrule_1* _TPPrule_1;
typedef struct _TPrule_2* _TPPrule_2;
typedef struct _TPrule_3* _TPPrule_3;
typedef struct _TPrule_4* _TPPrule_4;
typedef struct _TPrule_5* _TPPrule_5;
typedef struct _TPrule_6* _TPPrule_6;
typedef struct _TPrule_7* _TPPrule_7;
typedef struct _TPrule_8* _TPPrule_8;
typedef struct _TPrule_9* _TPPrule_9;
typedef struct _TPrule_10* _TPPrule_10;
typedef struct _TPrule_11* _TPPrule_11;
typedef struct _TPrule_12* _TPPrule_12;
typedef struct _TPrule_13* _TPPrule_13;
typedef struct _TPrule_0125* _TPPrule_0125;
typedef struct _TPrule_0124* _TPPrule_0124;
typedef struct _TPrule_0123* _TPPrule_0123;
typedef struct _TPrule_0122* _TPPrule_0122;
typedef struct _TPrule_0121* _TPPrule_0121;
typedef struct _TPrule_0120* _TPPrule_0120;
typedef struct _TPrule_0119* _TPPrule_0119;
typedef struct _TPrule_0118* _TPPrule_0118;
typedef struct _TPrule_0117* _TPPrule_0117;
typedef struct _TPrule_0116* _TPPrule_0116;
typedef struct _TPrule_0115* _TPPrule_0115;
typedef struct _TPrule_0114* _TPPrule_0114;
typedef struct _TPrule_0113* _TPPrule_0113;
typedef struct _TPrule_0112* _TPPrule_0112;
typedef struct _TPrule_0111* _TPPrule_0111;
typedef struct _TPrule_0110* _TPPrule_0110;
typedef struct _TPrule_0109* _TPPrule_0109;
typedef struct _TPrule_0108* _TPPrule_0108;
typedef struct _TPrule_0107* _TPPrule_0107;
typedef struct _TPrule_0106* _TPPrule_0106;
typedef struct _TPrule_0105* _TPPrule_0105;
typedef struct _TPrule_0104* _TPPrule_0104;
typedef struct _TPrule_0103* _TPPrule_0103;
typedef struct _TPrule_0102* _TPPrule_0102;
typedef struct _TPrule_0101* _TPPrule_0101;
typedef struct _TPrule_0100* _TPPrule_0100;
typedef struct _TPrule_099* _TPPrule_099;
typedef struct _TPrule_098* _TPPrule_098;
typedef struct _TPrule_097* _TPPrule_097;
typedef struct _TPrule_096* _TPPrule_096;
typedef struct _TPrule_095* _TPPrule_095;
typedef struct _TPrule_094* _TPPrule_094;
typedef struct _TPrule_093* _TPPrule_093;
typedef struct _TPrule_092* _TPPrule_092;
typedef struct _TPrule_091* _TPPrule_091;
typedef struct _TPrule_090* _TPPrule_090;
typedef struct _TPrule_089* _TPPrule_089;
typedef struct _TPrule_088* _TPPrule_088;
typedef struct _TPrule_087* _TPPrule_087;
typedef struct _TPrule_086* _TPPrule_086;
typedef struct _TPrule_085* _TPPrule_085;
typedef struct _TPrule_084* _TPPrule_084;
typedef struct _TPrule_083* _TPPrule_083;
typedef struct _TPrule_082* _TPPrule_082;
typedef struct _TPrule_081* _TPPrule_081;
typedef struct _TPrule_080* _TPPrule_080;
typedef struct _TPrule_079* _TPPrule_079;
typedef struct _TPrule_078* _TPPrule_078;
typedef struct _TPrule_077* _TPPrule_077;
typedef struct _TPrule_076* _TPPrule_076;
typedef struct _TPrule_075* _TPPrule_075;
typedef struct _TPrule_074* _TPPrule_074;
typedef struct _TPrule_073* _TPPrule_073;
typedef struct _TPrule_072* _TPPrule_072;
typedef struct _TPrule_071* _TPPrule_071;
typedef struct _TPrule_070* _TPPrule_070;
typedef struct _TPrule_069* _TPPrule_069;
typedef struct _TPrule_068* _TPPrule_068;
typedef struct _TPrule_067* _TPPrule_067;
typedef struct _TPrule_066* _TPPrule_066;
typedef struct _TPrule_065* _TPPrule_065;
typedef struct _TPrule_064* _TPPrule_064;
typedef struct _TPrule_063* _TPPrule_063;
typedef struct _TPrule_062* _TPPrule_062;
typedef struct _TPrule_061* _TPPrule_061;
typedef struct _TPrule_060* _TPPrule_060;
typedef struct _TPrule_059* _TPPrule_059;
typedef struct _TPrule_058* _TPPrule_058;
typedef struct _TPrule_057* _TPPrule_057;
typedef struct _TPrule_056* _TPPrule_056;
typedef struct _TPrule_055* _TPPrule_055;
typedef struct _TPrule_054* _TPPrule_054;
typedef struct _TPrule_053* _TPPrule_053;
typedef struct _TPrule_052* _TPPrule_052;
typedef struct _TPrule_051* _TPPrule_051;
typedef struct _TPrule_050* _TPPrule_050;
typedef struct _TPrule_049* _TPPrule_049;
typedef struct _TPrule_048* _TPPrule_048;
typedef struct _TPrule_047* _TPPrule_047;
typedef struct _TPrule_046* _TPPrule_046;
typedef struct _TPrule_045* _TPPrule_045;
typedef struct _TPrule_044* _TPPrule_044;
typedef struct _TPrule_043* _TPPrule_043;
typedef struct _TPrule_042* _TPPrule_042;
typedef struct _TPrule_041* _TPPrule_041;
typedef struct _TPrule_040* _TPPrule_040;
typedef struct _TPrule_039* _TPPrule_039;
typedef struct _TPrule_038* _TPPrule_038;
typedef struct _TPrule_037* _TPPrule_037;
typedef struct _TPrule_036* _TPPrule_036;
typedef struct _TPrule_035* _TPPrule_035;
typedef struct _TPrule_034* _TPPrule_034;
typedef struct _TPrule_033* _TPPrule_033;
typedef struct _TPrule_032* _TPPrule_032;
typedef struct _TPrule_031* _TPPrule_031;
typedef struct _TPrule_030* _TPPrule_030;
typedef struct _TPrule_029* _TPPrule_029;
typedef struct _TPrule_028* _TPPrule_028;
typedef struct _TPrule_027* _TPPrule_027;
typedef struct _TPrule_026* _TPPrule_026;
typedef struct _TPrule_025* _TPPrule_025;
typedef struct _TPrule_024* _TPPrule_024;
typedef struct _TPrule_023* _TPPrule_023;
typedef struct _TPrule_022* _TPPrule_022;
typedef struct _TPrule_021* _TPPrule_021;
typedef struct _TPrule_020* _TPPrule_020;
typedef struct _TPrule_019* _TPPrule_019;
typedef struct _TPrule_018* _TPPrule_018;
typedef struct _TPrule_017* _TPPrule_017;
typedef struct _TPrule_016* _TPPrule_016;
typedef struct _TPrule_015* _TPPrule_015;
typedef struct _TPrule_014* _TPPrule_014;
typedef struct _TPrule_013* _TPPrule_013;
typedef struct _TPrule_012* _TPPrule_012;
typedef struct _TPrule_011* _TPPrule_011;
typedef struct _TPrule_010* _TPPrule_010;
typedef struct _TPrule_09* _TPPrule_09;
typedef struct _TPrule_08* _TPPrule_08;
typedef struct _TPrule_07* _TPPrule_07;
typedef struct _TPrule_06* _TPPrule_06;
typedef struct _TPrule_05* _TPPrule_05;
typedef struct _TPrule_04* _TPPrule_04;
typedef struct _TPrule_03* _TPPrule_03;
typedef struct _TPrule_02* _TPPrule_02;
typedef struct _TPrule_01* _TPPrule_01;

struct _TSSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSShieldSyms
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSShieldSym
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSShieldClause
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSShield
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSubtree
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRemoteClause
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRemoteAttr
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRemoteAttrs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSParams
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSParam
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSInheritSym
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSInheritSyms
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSG1
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSParamsOpt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSExpression
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSExpandOpt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSDependence
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSDepClause
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSDepAttrs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRhsAttrs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRemoteExpression
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSDepAttr
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSPlainComp
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSCompute
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSComputation
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSComputations
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSChainSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSChainNames
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSChainName
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSymOcc
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrNames
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrName
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrDefs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrDefId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSClass
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrDefIds
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrDef
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSLoop
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSExpressionDep
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSDefAttr
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrComp
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrUseId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttr
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAsgnTok
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAlt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSpecs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAlts
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSyntUnits
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRuleSpecId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRuleSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
RuleProd _ATRuleInstance;
};

struct _TSSyntUnit
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSyntLit
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSProduction
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSCompPart
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSInheritOpt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrDefsOpt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSymbolDefIds
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSymClass
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSTermSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TSSymCompSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TSSymAttrSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TSIndex
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSymbolRef
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAG
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
Environment _ATEnv;
};

struct _TSRuleId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TSTypeId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TSSyntId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TSSymbolId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TSSymbolDefId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TPrule_1
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
_TSPAlts _desc2;
POSITION _AT_pos;
};

struct _TPrule_2
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
_TSPSyntUnits _desc2;
POSITION _AT_pos;
};

struct _TPrule_3
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_4
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRuleId _desc1;
POSITION _AT_pos;
};

struct _TPrule_5
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
RuleProd _ATRuleInstance;
_TSPRuleSpecId _desc1;
_TSPProduction _desc2;
_TSPCompPart _desc3;
POSITION _AT_pos;
};

struct _TPrule_6
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
POSITION _AT_pos;
};

struct _TPrule_7
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_8
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
_TSPSymbolDefIds _desc1;
_TSPTypeId _desc2;
POSITION _AT_pos;
};

struct _TPrule_9
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
_TSPSymClass _desc1;
_TSPSymbolDefId _desc2;
_TSPInheritOpt _desc3;
_TSPCompPart _desc4;
POSITION _AT_pos;
};

struct _TPrule_10
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
_TSPSymClass _desc1;
_TSPSymbolDefIds _desc2;
_TSPAttrDefsOpt _desc3;
};

struct _TPrule_11
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_12
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_13
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolId _desc1;
_TSPIndex _desc2;
};

struct _TPrule_0125
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_0124
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntLit _desc1;
};

struct _TPrule_0123
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
POSITION _AT_pos;
};

struct _TPrule_0122
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntUnits _desc1;
_TSPSyntUnit _desc2;
};

struct _TPrule_0121
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_0120
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_0119
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_0118
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_0117
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_0116
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolId _desc1;
_TSPIndex _desc2;
POSITION _AT_pos;
};

struct _TPrule_0115
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_0114
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_0113
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_0112
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPIndex _desc1;
};

struct _TPrule_0111
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_0110
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPG1 _desc1;
};

struct _TPrule_0109
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_0108
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymOcc _desc1;
};

struct _TPrule_0107
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_0106
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPTermSpec _desc1;
};

struct _TPrule_0105
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymCompSpec _desc1;
};

struct _TPrule_0104
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymAttrSpec _desc1;
};

struct _TPrule_0103
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSpecs _desc1;
_TSPSpec _desc2;
};

struct _TPrule_0102
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_0101
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRuleSpec _desc1;
};

struct _TPrule_0100
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPChainSpec _desc1;
};

struct _TPrule_099
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrSpec _desc1;
};

struct _TPrule_098
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolId _desc1;
POSITION _AT_pos;
};

struct _TPrule_097
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldSyms _desc1;
_TSPShieldSym _desc2;
};

struct _TPrule_096
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldSym _desc1;
};

struct _TPrule_095
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldClause _desc1;
};

struct _TPrule_094
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldSyms _desc1;
};

struct _TPrule_093
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldSym _desc1;
};

struct _TPrule_092
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_091
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_090
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_089
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_088
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSubtree _desc1;
_TSPRemoteClause _desc2;
_TSPShield _desc3;
_TSPExpandOpt _desc4;
};

struct _TPrule_087
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSubtree _desc1;
_TSPRemoteClause _desc2;
_TSPShield _desc3;
};

struct _TPrule_086
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteClause _desc1;
};

struct _TPrule_085
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteAttrs _desc1;
};

struct _TPrule_084
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteAttr _desc1;
};

struct _TPrule_083
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolId _desc1;
_TSPAttrUseId _desc2;
POSITION _AT_pos;
};

struct _TPrule_082
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteAttr _desc1;
_TSPRemoteAttrs _desc2;
};

struct _TPrule_081
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteAttr _desc1;
};

struct _TPrule_080
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPLoop _desc1;
};

struct _TPrule_079
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPExpressionDep _desc1;
};

struct _TPrule_078
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPParam _desc1;
_TSPParams _desc2;
};

struct _TPrule_077
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPParam _desc1;
};

struct _TPrule_076
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPParams _desc1;
};

struct _TPrule_075
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_074
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPExpressionDep _desc1;
};

struct _TPrule_073
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPExpressionDep _desc1;
_TSPAttr _desc2;
_TSPExpressionDep _desc3;
};

struct _TPrule_072
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolId _desc1;
POSITION _AT_pos;
};

struct _TPrule_071
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPInheritSyms _desc1;
_TSPInheritSym _desc2;
};

struct _TPrule_070
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPInheritSym _desc1;
};

struct _TPrule_069
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPInheritSyms _desc1;
};

struct _TPrule_068
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_067
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_066
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_065
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolDefId _desc1;
POSITION _AT_pos;
};

struct _TPrule_064
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPG1 _desc1;
_TSPSymbolDefId _desc2;
POSITION _AT_pos;
};

struct _TPrule_063
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolRef _desc1;
};

struct _TPrule_062
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRhsAttrs _desc1;
};

struct _TPrule_061
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteExpression _desc1;
};

struct _TPrule_060
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_059
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPParamsOpt _desc1;
};

struct _TPrule_058
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPExpression _desc1;
_TSPDepClause _desc2;
};

struct _TPrule_057
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPExpression _desc1;
};

struct _TPrule_056
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_055
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_054
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_053
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttr _desc1;
};

struct _TPrule_052
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPTypeId _desc1;
POSITION _AT_pos;
};

struct _TPrule_051
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_050
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDepAttrs _desc1;
};

struct _TPrule_049
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDepAttr _desc1;
};

struct _TPrule_048
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDependence _desc1;
};

struct _TPrule_047
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDependence _desc1;
};

struct _TPrule_046
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDepAttr _desc1;
_TSPDepAttrs _desc2;
};

struct _TPrule_045
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDepAttr _desc1;
};

struct _TPrule_044
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRhsAttrs _desc1;
};

struct _TPrule_043
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteExpression _desc1;
};

struct _TPrule_042
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttr _desc1;
};

struct _TPrule_041
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttr _desc1;
};

struct _TPrule_040
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPPlainComp _desc1;
};

struct _TPrule_039
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrComp _desc1;
};

struct _TPrule_038
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPComputation _desc1;
_TSPComputations _desc2;
};

struct _TPrule_037
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_036
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPCompute _desc1;
};

struct _TPrule_035
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPCompute _desc1;
};

struct _TPrule_034
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPComputations _desc1;
};

struct _TPrule_033
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_032
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_031
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_030
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_029
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPChainNames _desc1;
_TSPTypeId _desc2;
POSITION _AT_pos;
};

struct _TPrule_028
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPChainNames _desc1;
_TSPChainName _desc2;
};

struct _TPrule_027
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPChainName _desc1;
};

struct _TPrule_026
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_025
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_024
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymOcc _desc1;
_TSPAttrUseId _desc2;
};

struct _TPrule_023
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrNames _desc1;
_TSPTypeId _desc2;
_TSPClass _desc3;
POSITION _AT_pos;
};

struct _TPrule_022
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrNames _desc1;
_TSPAttrName _desc2;
};

struct _TPrule_021
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrName _desc1;
};

struct _TPrule_020
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_019
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDefs _desc1;
};

struct _TPrule_018
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_017
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDefs _desc1;
_TSPAttrDef _desc2;
};

struct _TPrule_016
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDef _desc1;
};

struct _TPrule_015
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDefIds _desc1;
_TSPAttrDefId _desc2;
};

struct _TPrule_014
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDefId _desc1;
};

struct _TPrule_013
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_012
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDefIds _desc1;
_TSPTypeId _desc2;
_TSPClass _desc3;
POSITION _AT_pos;
};

struct _TPrule_011
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDefAttr _desc1;
_TSPLoop _desc2;
};

struct _TPrule_010
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDefAttr _desc1;
_TSPAsgnTok _desc2;
_TSPExpressionDep _desc3;
};

struct _TPrule_09
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDefAttr _desc1;
_TSPExpressionDep _desc2;
};

struct _TPrule_08
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrUseId _desc1;
};

struct _TPrule_07
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_06
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_05
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
POSITION _AT_pos;
};

struct _TPrule_04
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAlts _desc1;
_TSPAlt _desc2;
};

struct _TPrule_03
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAlt _desc1;
};

struct _TPrule_02
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_01
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
Environment _ATEnv;
_TSPSpecs _desc1;
};

#undef _NODECOMMON
#endif
