
/* implementation of tree construction functions */

#include "node.h"

#include "nodecode.h"

#include "attrpredef.h"

#include "visitmap.h"

#include "treeact.h"

#ifdef MONITOR
#include "attr_mon_dapto.h"
#include "MONTblStack.h"
#endif

#include <stdlib.h>

#define _USE_OBSTACK 1

/* use of obstack: */

#if _USE_OBSTACK

#include "obstack.h"
static struct obstack TreeSpace;
static void *_TreeBase;

#ifdef __cplusplus
void* NODEPTR_struct::operator new(size_t size)
{
	return obstack_alloc(&TreeSpace, size);
}
#else
#if defined(__STDC__) || defined(__cplusplus)
char* TreeNodeAlloc(int size)
#else
char* TreeNodeAlloc(size) int size;
#endif
{
	return (char *)(obstack_alloc(&TreeSpace, size));
}
#endif

void InitTree()
{
	obstack_init(&TreeSpace);
	_TreeBase=obstack_alloc(&TreeSpace,0);
}

void FreeTree()
{
	obstack_free(&TreeSpace, _TreeBase);
	_TreeBase=obstack_alloc(&TreeSpace,0);
}

#else

#include <stdio.h>

#ifdef __cplusplus
void* NODEPTR_struct::operator new(size_t size)
{
	void *retval = malloc(size);
	if (retval) return retval;
	fprintf(stderr, "*** DEADLY: No more memory.\n");
	exit(1);
}
#else
#if defined(__STDC__) || defined(__cplusplus)
char* TreeNodeAlloc(int size)
#else
char* TreeNodeAlloc(size) int size;
#endif
{
	char *retval = (char *) malloc(size);
	if (retval) return retval;
	fprintf(stderr, "*** DEADLY: No more memory.\n");
	exit(1);
}
#endif

void InitTree() { }

void FreeTree() { }

#endif

#ifdef MONITOR
#define _SETCOORD(node) \
        node->_coord = _coordref ? *_coordref : NoCoord;
#define _COPYCOORD(node) \
        node->_coord = _currn->_desc1->_coord;
#else
#define _SETCOORD(node)
#define _COPYCOORD(node)
#endif
#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSpec)) return (_currn);
if (IsSymb (_currn, SYMBAttrSpec)) return (Mkrule_099(_coordref, _currn));
if (IsSymb (_currn, SYMBChainSpec)) return (Mkrule_0100(_coordref, _currn));
if (IsSymb (_currn, SYMBRuleSpec)) return (Mkrule_0101(_coordref, _currn));
if (IsSymb (_currn, SYMBSymAttrSpec)) return (Mkrule_0104(_coordref, _currn));
if (IsSymb (_currn, SYMBSymCompSpec)) return (Mkrule_0105(_coordref, _currn));
if (IsSymb (_currn, SYMBTermSpec)) return (Mkrule_0106(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkShieldSyms (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkShieldSyms (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBShieldSyms)) return (_currn);
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_096(_coordref, _currn));
if (IsSymb (_currn, SYMBShieldSym)) return (Mkrule_096(_coordref, _currn));
return(NULLNODEPTR);
}/* MkShieldSyms */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkShieldSym (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkShieldSym (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBShieldSym)) return (_currn);
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_098(_coordref, _currn));
return(NULLNODEPTR);
}/* MkShieldSym */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkShieldClause (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkShieldClause (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBShieldClause)) return (_currn);
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_093(_coordref, _currn));
if (IsSymb (_currn, SYMBShieldSym)) return (Mkrule_093(_coordref, _currn));
return(NULLNODEPTR);
}/* MkShieldClause */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkShield (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkShield (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBShield)) return (_currn);
return(NULLNODEPTR);
}/* MkShield */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSubtree (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSubtree (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSubtree)) return (_currn);
if (IsSymb (_currn, SYMBSymOcc)) return (Mkrule_0108(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSubtree */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemoteClause (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemoteClause (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemoteClause)) return (_currn);
if (IsSymb (_currn, SYMBRemoteAttr)) return (Mkrule_084(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemoteClause */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemoteAttr (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemoteAttr (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemoteAttr)) return (_currn);
return(NULLNODEPTR);
}/* MkRemoteAttr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemoteAttrs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemoteAttrs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemoteAttrs)) return (_currn);
if (IsSymb (_currn, SYMBRemoteAttr)) return (Mkrule_081(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemoteAttrs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkParams (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkParams (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBParams)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_077(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_077(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_077(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_077(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_077(_coordref, _currn));
if (IsSymb (_currn, SYMBParam)) return (Mkrule_077(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_077(_coordref, _currn));
return(NULLNODEPTR);
}/* MkParams */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkParam (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkParam (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBParam)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_074(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_074(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_074(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_074(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_074(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_074(_coordref, _currn));
return(NULLNODEPTR);
}/* MkParam */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkInheritSym (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkInheritSym (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBInheritSym)) return (_currn);
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_072(_coordref, _currn));
return(NULLNODEPTR);
}/* MkInheritSym */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkInheritSyms (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkInheritSyms (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBInheritSyms)) return (_currn);
if (IsSymb (_currn, SYMBInheritSym)) return (Mkrule_070(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_070(_coordref, _currn));
return(NULLNODEPTR);
}/* MkInheritSyms */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkG1 (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkG1 (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBG1)) return (_currn);
if (IsSymb (_currn, SYMBSymbolDefId)) return (Mkrule_065(_coordref, _currn));
return(NULLNODEPTR);
}/* MkG1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkParamsOpt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkParamsOpt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBParamsOpt)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_076(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_076(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_076(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_076(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_076(_coordref, _currn));
if (IsSymb (_currn, SYMBParam)) return (Mkrule_076(_coordref, _currn));
if (IsSymb (_currn, SYMBParams)) return (Mkrule_076(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_076(_coordref, _currn));
return(NULLNODEPTR);
}/* MkParamsOpt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBExpression)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_053(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_061(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_062(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_063(_coordref, _currn));
return(NULLNODEPTR);
}/* MkExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkExpandOpt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkExpandOpt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBExpandOpt)) return (_currn);
return(NULLNODEPTR);
}/* MkExpandOpt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDependence (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDependence (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDependence)) return (_currn);
if (IsSymb (_currn, SYMBDepAttr)) return (Mkrule_049(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_049(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_049(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_049(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDependence */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDepClause (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDepClause (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDepClause)) return (_currn);
return(NULLNODEPTR);
}/* MkDepClause */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDepAttrs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDepAttrs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDepAttrs)) return (_currn);
if (IsSymb (_currn, SYMBDepAttr)) return (Mkrule_045(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_045(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_045(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_045(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDepAttrs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRhsAttrs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRhsAttrs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRhsAttrs)) return (_currn);
return(NULLNODEPTR);
}/* MkRhsAttrs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemoteExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemoteExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemoteExpression)) return (_currn);
return(NULLNODEPTR);
}/* MkRemoteExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDepAttr (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDepAttr (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDepAttr)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_042(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_043(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_044(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDepAttr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPlainComp (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPlainComp (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPlainComp)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_079(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_079(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_079(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_079(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_079(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_079(_coordref, _currn));
if (IsSymb (_currn, SYMBLoop)) return (Mkrule_080(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPlainComp */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkCompute (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkCompute (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBCompute)) return (_currn);
if (IsSymb (_currn, SYMBAttrComp)) return (Mkrule_039(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_040(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_040(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_040(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_040(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_040(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_040(_coordref, _currn));
if (IsSymb (_currn, SYMBLoop)) return (Mkrule_040(_coordref, _currn));
if (IsSymb (_currn, SYMBPlainComp)) return (Mkrule_040(_coordref, _currn));
return(NULLNODEPTR);
}/* MkCompute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkComputation (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkComputation (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBComputation)) return (_currn);
if (IsSymb (_currn, SYMBAttrComp)) return (Mkrule_035(_coordref, _currn));
if (IsSymb (_currn, SYMBCompute)) return (Mkrule_035(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_035(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_035(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_035(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_035(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_035(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_035(_coordref, _currn));
if (IsSymb (_currn, SYMBLoop)) return (Mkrule_035(_coordref, _currn));
if (IsSymb (_currn, SYMBPlainComp)) return (Mkrule_035(_coordref, _currn));
return(NULLNODEPTR);
}/* MkComputation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkComputations (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkComputations (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBComputations)) return (_currn);
return(NULLNODEPTR);
}/* MkComputations */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkChainSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkChainSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBChainSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkChainSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkChainNames (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkChainNames (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBChainNames)) return (_currn);
if (IsSymb (_currn, SYMBChainName)) return (Mkrule_027(_coordref, _currn));
return(NULLNODEPTR);
}/* MkChainNames */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkChainName (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkChainName (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBChainName)) return (_currn);
return(NULLNODEPTR);
}/* MkChainName */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymOcc (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymOcc (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymOcc)) return (_currn);
return(NULLNODEPTR);
}/* MkSymOcc */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrNames (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrNames (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrNames)) return (_currn);
if (IsSymb (_currn, SYMBAttrName)) return (Mkrule_021(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAttrNames */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrName (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrName (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrName)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrName */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDefs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDefs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDefs)) return (_currn);
if (IsSymb (_currn, SYMBAttrDef)) return (Mkrule_016(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAttrDefs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDefId)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBClass)) return (_currn);
return(NULLNODEPTR);
}/* MkClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDefIds (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDefIds (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDefIds)) return (_currn);
if (IsSymb (_currn, SYMBAttrDefId)) return (Mkrule_014(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAttrDefIds */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDef (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDef (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDef)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkLoop (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkLoop (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBLoop)) return (_currn);
return(NULLNODEPTR);
}/* MkLoop */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkExpressionDep (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkExpressionDep (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBExpressionDep)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_057(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_057(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_057(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_057(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_057(_coordref, _currn));
return(NULLNODEPTR);
}/* MkExpressionDep */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDefAttr (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDefAttr (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDefAttr)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_041(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDefAttr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrComp (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrComp (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrComp)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrComp */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrUseId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrUseId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrUseId)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrUseId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttr (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttr (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttr)) return (_currn);
return(NULLNODEPTR);
}/* MkAttr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAsgnTok (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAsgnTok (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAsgnTok)) return (_currn);
return(NULLNODEPTR);
}/* MkAsgnTok */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAlt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAlt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAlt)) return (_currn);
if (IsSymb (_currn, SYMBSyntId)) return (Mkrule_05(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAlt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSpecs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSpecs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSpecs)) return (_currn);
return(NULLNODEPTR);
}/* MkSpecs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAlts (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAlts (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAlts)) return (_currn);
if (IsSymb (_currn, SYMBAlt)) return (Mkrule_03(_coordref, _currn));
if (IsSymb (_currn, SYMBSyntId)) return (Mkrule_03(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAlts */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSyntUnits (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSyntUnits (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSyntUnits)) return (_currn);
return(NULLNODEPTR);
}/* MkSyntUnits */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRuleSpecId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRuleSpecId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRuleSpecId)) return (_currn);
if (IsSymb (_currn, SYMBRuleId)) return (Mkrule_4(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRuleSpecId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRuleSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRuleSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRuleSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkRuleSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSyntUnit (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSyntUnit (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSyntUnit)) return (_currn);
if (IsSymb (_currn, SYMBSyntId)) return (Mkrule_0123(_coordref, _currn));
if (IsSymb (_currn, SYMBSyntLit)) return (Mkrule_0124(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSyntUnit */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSyntLit (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSyntLit (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSyntLit)) return (_currn);
return(NULLNODEPTR);
}/* MkSyntLit */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkProduction (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkProduction (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBProduction)) return (_currn);
return(NULLNODEPTR);
}/* MkProduction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkCompPart (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkCompPart (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBCompPart)) return (_currn);
return(NULLNODEPTR);
}/* MkCompPart */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkInheritOpt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkInheritOpt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBInheritOpt)) return (_currn);
return(NULLNODEPTR);
}/* MkInheritOpt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDefsOpt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDefsOpt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDefsOpt)) return (_currn);
if (IsSymb (_currn, SYMBAttrDef)) return (Mkrule_019(_coordref, _currn));
if (IsSymb (_currn, SYMBAttrDefs)) return (Mkrule_019(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAttrDefsOpt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbolDefIds (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbolDefIds (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbolDefIds)) return (_currn);
if (IsSymb (_currn, SYMBSymbolDefId)) return (Mkrule_0110(_coordref, _currn));
if (IsSymb (_currn, SYMBG1)) return (Mkrule_0110(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbolDefIds */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymClass)) return (_currn);
return(NULLNODEPTR);
}/* MkSymClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkTermSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkTermSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBTermSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkTermSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymCompSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymCompSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymCompSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkSymCompSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymAttrSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymAttrSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymAttrSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkSymAttrSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkIndex (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkIndex (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBIndex)) return (_currn);
return(NULLNODEPTR);
}/* MkIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbolRef (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbolRef (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbolRef)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbolRef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAG (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAG (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAG)) return (_currn);
if (IsSymb (_currn, SYMBSpecs)) return (Mkrule_01(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAG */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRuleId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRuleId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRuleId)) return (_currn);
return(NULLNODEPTR);
}/* MkRuleId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkTypeId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkTypeId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBTypeId)) return (_currn);
return(NULLNODEPTR);
}/* MkTypeId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSyntId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSyntId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSyntId)) return (_currn);
return(NULLNODEPTR);
}/* MkSyntId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbolId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbolId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbolId)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbolId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbolDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbolDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbolDefId)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbolDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_1 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_1 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_1 _currn;
#ifdef __cplusplus
_currn = new _TPrule_1;
#else
_currn = (_TPPrule_1) TreeNodeAlloc (sizeof (struct _TPrule_1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_1;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_1: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_currn->_desc2 = (_TSPAlts) MkAlts (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_1: root of subtree no. 2 can not be made a Alts node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_1;
return ( (NODEPTR) _currn);
}/* Mkrule_1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_2 _currn;
#ifdef __cplusplus
_currn = new _TPrule_2;
#else
_currn = (_TPPrule_2) TreeNodeAlloc (sizeof (struct _TPrule_2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_2;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_2: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_currn->_desc2 = (_TSPSyntUnits) MkSyntUnits (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_2: root of subtree no. 2 can not be made a SyntUnits node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_2;
return ( (NODEPTR) _currn);
}/* Mkrule_2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_3 (POSITION *_coordref)
#else
NODEPTR Mkrule_3 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_3 _currn;
#ifdef __cplusplus
_currn = new _TPrule_3;
#else
_currn = (_TPPrule_3) TreeNodeAlloc (sizeof (struct _TPrule_3));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_3;
_SETCOORD(_currn)
_TERMACT_rule_3;
return ( (NODEPTR) _currn);
}/* Mkrule_3 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_4 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_4 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_4 _currn;
#ifdef __cplusplus
_currn = new _TPrule_4;
#else
_currn = (_TPPrule_4) TreeNodeAlloc (sizeof (struct _TPrule_4));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_4;
_currn->_desc1 = (_TSPRuleId) MkRuleId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_4: root of subtree no. 1 can not be made a RuleId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_4;
return ( (NODEPTR) _currn);
}/* Mkrule_4 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_5 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_5 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_5 _currn;
#ifdef __cplusplus
_currn = new _TPrule_5;
#else
_currn = (_TPPrule_5) TreeNodeAlloc (sizeof (struct _TPrule_5));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_5;
_currn->_desc1 = (_TSPRuleSpecId) MkRuleSpecId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_5: root of subtree no. 1 can not be made a RuleSpecId node ", 0, _coordref);
_currn->_desc2 = (_TSPProduction) MkProduction (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_5: root of subtree no. 2 can not be made a Production node ", 0, _coordref);
_currn->_desc3 = (_TSPCompPart) MkCompPart (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_5: root of subtree no. 3 can not be made a CompPart node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_5;
return ( (NODEPTR) _currn);
}/* Mkrule_5 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_6 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_6 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_6 _currn;
#ifdef __cplusplus
_currn = new _TPrule_6;
#else
_currn = (_TPPrule_6) TreeNodeAlloc (sizeof (struct _TPrule_6));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_6;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_6: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_6;
return ( (NODEPTR) _currn);
}/* Mkrule_6 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_7 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_7 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_7 _currn;
#ifdef __cplusplus
_currn = new _TPrule_7;
#else
_currn = (_TPPrule_7) TreeNodeAlloc (sizeof (struct _TPrule_7));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_7;
_SETCOORD(_currn)
_TERMACT_rule_7;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "P_String", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_7 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_8 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_8 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_8 _currn;
#ifdef __cplusplus
_currn = new _TPrule_8;
#else
_currn = (_TPPrule_8) TreeNodeAlloc (sizeof (struct _TPrule_8));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_8;
_currn->_desc1 = (_TSPSymbolDefIds) MkSymbolDefIds (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_8: root of subtree no. 1 can not be made a SymbolDefIds node ", 0, _coordref);
_currn->_desc2 = (_TSPTypeId) MkTypeId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_8: root of subtree no. 2 can not be made a TypeId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_8;
return ( (NODEPTR) _currn);
}/* Mkrule_8 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_9 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3, NODEPTR _desc4)
#else
NODEPTR Mkrule_9 (_coordref,_desc1,_desc2,_desc3,_desc4)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
	NODEPTR _desc4;
#endif
{	_TPPrule_9 _currn;
#ifdef __cplusplus
_currn = new _TPrule_9;
#else
_currn = (_TPPrule_9) TreeNodeAlloc (sizeof (struct _TPrule_9));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_9;
_currn->_desc1 = (_TSPSymClass) MkSymClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_9: root of subtree no. 1 can not be made a SymClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbolDefId) MkSymbolDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_9: root of subtree no. 2 can not be made a SymbolDefId node ", 0, _coordref);
_currn->_desc3 = (_TSPInheritOpt) MkInheritOpt (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_9: root of subtree no. 3 can not be made a InheritOpt node ", 0, _coordref);
_currn->_desc4 = (_TSPCompPart) MkCompPart (_coordref, _desc4);	
if (((NODEPTR)_currn->_desc4) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_9: root of subtree no. 4 can not be made a CompPart node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_9;
return ( (NODEPTR) _currn);
}/* Mkrule_9 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_10 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_10 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_10 _currn;
#ifdef __cplusplus
_currn = new _TPrule_10;
#else
_currn = (_TPPrule_10) TreeNodeAlloc (sizeof (struct _TPrule_10));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_10;
_currn->_desc1 = (_TSPSymClass) MkSymClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_10: root of subtree no. 1 can not be made a SymClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbolDefIds) MkSymbolDefIds (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_10: root of subtree no. 2 can not be made a SymbolDefIds node ", 0, _coordref);
_currn->_desc3 = (_TSPAttrDefsOpt) MkAttrDefsOpt (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_10: root of subtree no. 3 can not be made a AttrDefsOpt node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_10;
return ( (NODEPTR) _currn);
}/* Mkrule_10 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_11 (POSITION *_coordref)
#else
NODEPTR Mkrule_11 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_11 _currn;
#ifdef __cplusplus
_currn = new _TPrule_11;
#else
_currn = (_TPPrule_11) TreeNodeAlloc (sizeof (struct _TPrule_11));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_11;
_SETCOORD(_currn)
_TERMACT_rule_11;
return ( (NODEPTR) _currn);
}/* Mkrule_11 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_12 (POSITION *_coordref)
#else
NODEPTR Mkrule_12 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_12 _currn;
#ifdef __cplusplus
_currn = new _TPrule_12;
#else
_currn = (_TPPrule_12) TreeNodeAlloc (sizeof (struct _TPrule_12));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_12;
_SETCOORD(_currn)
_TERMACT_rule_12;
return ( (NODEPTR) _currn);
}/* Mkrule_12 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_13 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_13 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_13 _currn;
#ifdef __cplusplus
_currn = new _TPrule_13;
#else
_currn = (_TPPrule_13) TreeNodeAlloc (sizeof (struct _TPrule_13));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_13;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_13: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_currn->_desc2 = (_TSPIndex) MkIndex (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_13: root of subtree no. 2 can not be made a Index node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_13;
return ( (NODEPTR) _currn);
}/* Mkrule_13 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0125 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_0125 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_0125 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0125;
#else
_currn = (_TPPrule_0125) TreeNodeAlloc (sizeof (struct _TPrule_0125));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0125;
_SETCOORD(_currn)
_TERMACT_rule_0125;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_0125 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0124 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0124 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0124 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0124;
#else
_currn = (_TPPrule_0124) TreeNodeAlloc (sizeof (struct _TPrule_0124));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0124;
_currn->_desc1 = (_TSPSyntLit) MkSyntLit (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0124: root of subtree no. 1 can not be made a SyntLit node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_0124;
return ( (NODEPTR) _currn);
}/* Mkrule_0124 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0123 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0123 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0123 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0123;
#else
_currn = (_TPPrule_0123) TreeNodeAlloc (sizeof (struct _TPrule_0123));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0123;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0123: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_0123;
return ( (NODEPTR) _currn);
}/* Mkrule_0123 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0122 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_0122 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_0122 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0122;
#else
_currn = (_TPPrule_0122) TreeNodeAlloc (sizeof (struct _TPrule_0122));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0122;
_currn->_desc1 = (_TSPSyntUnits) MkSyntUnits (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0122: root of subtree no. 1 can not be made a SyntUnits node ", 0, _coordref);
_currn->_desc2 = (_TSPSyntUnit) MkSyntUnit (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0122: root of subtree no. 2 can not be made a SyntUnit node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_0122;
return ( (NODEPTR) _currn);
}/* Mkrule_0122 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0121 (POSITION *_coordref)
#else
NODEPTR Mkrule_0121 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_0121 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0121;
#else
_currn = (_TPPrule_0121) TreeNodeAlloc (sizeof (struct _TPrule_0121));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0121;
_SETCOORD(_currn)
_TERMACT_rule_0121;
return ( (NODEPTR) _currn);
}/* Mkrule_0121 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0120 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_0120 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_0120 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0120;
#else
_currn = (_TPPrule_0120) TreeNodeAlloc (sizeof (struct _TPrule_0120));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0120;
_SETCOORD(_currn)
_TERMACT_rule_0120;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_0120 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0119 (POSITION *_coordref)
#else
NODEPTR Mkrule_0119 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_0119 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0119;
#else
_currn = (_TPPrule_0119) TreeNodeAlloc (sizeof (struct _TPrule_0119));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0119;
_SETCOORD(_currn)
_TERMACT_rule_0119;
return ( (NODEPTR) _currn);
}/* Mkrule_0119 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0118 (POSITION *_coordref)
#else
NODEPTR Mkrule_0118 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_0118 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0118;
#else
_currn = (_TPPrule_0118) TreeNodeAlloc (sizeof (struct _TPrule_0118));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0118;
_SETCOORD(_currn)
_TERMACT_rule_0118;
return ( (NODEPTR) _currn);
}/* Mkrule_0118 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0117 (POSITION *_coordref)
#else
NODEPTR Mkrule_0117 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_0117 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0117;
#else
_currn = (_TPPrule_0117) TreeNodeAlloc (sizeof (struct _TPrule_0117));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0117;
_SETCOORD(_currn)
_TERMACT_rule_0117;
return ( (NODEPTR) _currn);
}/* Mkrule_0117 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0116 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_0116 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_0116 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0116;
#else
_currn = (_TPPrule_0116) TreeNodeAlloc (sizeof (struct _TPrule_0116));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0116;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0116: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_currn->_desc2 = (_TSPIndex) MkIndex (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0116: root of subtree no. 2 can not be made a Index node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_0116;
return ( (NODEPTR) _currn);
}/* Mkrule_0116 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0115 (POSITION *_coordref)
#else
NODEPTR Mkrule_0115 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_0115 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0115;
#else
_currn = (_TPPrule_0115) TreeNodeAlloc (sizeof (struct _TPrule_0115));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0115;
_SETCOORD(_currn)
_TERMACT_rule_0115;
return ( (NODEPTR) _currn);
}/* Mkrule_0115 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0114 (POSITION *_coordref)
#else
NODEPTR Mkrule_0114 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_0114 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0114;
#else
_currn = (_TPPrule_0114) TreeNodeAlloc (sizeof (struct _TPrule_0114));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0114;
_SETCOORD(_currn)
_TERMACT_rule_0114;
return ( (NODEPTR) _currn);
}/* Mkrule_0114 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0113 (POSITION *_coordref)
#else
NODEPTR Mkrule_0113 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_0113 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0113;
#else
_currn = (_TPPrule_0113) TreeNodeAlloc (sizeof (struct _TPrule_0113));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0113;
_SETCOORD(_currn)
_TERMACT_rule_0113;
return ( (NODEPTR) _currn);
}/* Mkrule_0113 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0112 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0112 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0112 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0112;
#else
_currn = (_TPPrule_0112) TreeNodeAlloc (sizeof (struct _TPrule_0112));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0112;
_currn->_desc1 = (_TSPIndex) MkIndex (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0112: root of subtree no. 1 can not be made a Index node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_0112;
return ( (NODEPTR) _currn);
}/* Mkrule_0112 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0111 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_0111 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_0111 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0111;
#else
_currn = (_TPPrule_0111) TreeNodeAlloc (sizeof (struct _TPrule_0111));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0111;
_SETCOORD(_currn)
_TERMACT_rule_0111;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_0111 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0110 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0110 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0110 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0110;
#else
_currn = (_TPPrule_0110) TreeNodeAlloc (sizeof (struct _TPrule_0110));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0110;
_currn->_desc1 = (_TSPG1) MkG1 (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0110: root of subtree no. 1 can not be made a G1 node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_0110;
return ( (NODEPTR) _currn);
}/* Mkrule_0110 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0109 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_0109 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_0109 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0109;
#else
_currn = (_TPPrule_0109) TreeNodeAlloc (sizeof (struct _TPrule_0109));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0109;
_SETCOORD(_currn)
_TERMACT_rule_0109;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_0109 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0108 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0108 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0108 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0108;
#else
_currn = (_TPPrule_0108) TreeNodeAlloc (sizeof (struct _TPrule_0108));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0108;
_currn->_desc1 = (_TSPSymOcc) MkSymOcc (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0108: root of subtree no. 1 can not be made a SymOcc node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_0108;
return ( (NODEPTR) _currn);
}/* Mkrule_0108 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0107 (POSITION *_coordref)
#else
NODEPTR Mkrule_0107 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_0107 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0107;
#else
_currn = (_TPPrule_0107) TreeNodeAlloc (sizeof (struct _TPrule_0107));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0107;
_SETCOORD(_currn)
_TERMACT_rule_0107;
return ( (NODEPTR) _currn);
}/* Mkrule_0107 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0106 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0106 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0106 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0106;
#else
_currn = (_TPPrule_0106) TreeNodeAlloc (sizeof (struct _TPrule_0106));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0106;
_currn->_desc1 = (_TSPTermSpec) MkTermSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0106: root of subtree no. 1 can not be made a TermSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_0106;
return ( (NODEPTR) _currn);
}/* Mkrule_0106 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0105 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0105 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0105 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0105;
#else
_currn = (_TPPrule_0105) TreeNodeAlloc (sizeof (struct _TPrule_0105));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0105;
_currn->_desc1 = (_TSPSymCompSpec) MkSymCompSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0105: root of subtree no. 1 can not be made a SymCompSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_0105;
return ( (NODEPTR) _currn);
}/* Mkrule_0105 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0104 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0104 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0104 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0104;
#else
_currn = (_TPPrule_0104) TreeNodeAlloc (sizeof (struct _TPrule_0104));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0104;
_currn->_desc1 = (_TSPSymAttrSpec) MkSymAttrSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0104: root of subtree no. 1 can not be made a SymAttrSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_0104;
return ( (NODEPTR) _currn);
}/* Mkrule_0104 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0103 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_0103 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_0103 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0103;
#else
_currn = (_TPPrule_0103) TreeNodeAlloc (sizeof (struct _TPrule_0103));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0103;
_currn->_desc1 = (_TSPSpecs) MkSpecs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0103: root of subtree no. 1 can not be made a Specs node ", 0, _coordref);
_currn->_desc2 = (_TSPSpec) MkSpec (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0103: root of subtree no. 2 can not be made a Spec node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_0103;
return ( (NODEPTR) _currn);
}/* Mkrule_0103 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0102 (POSITION *_coordref)
#else
NODEPTR Mkrule_0102 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_0102 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0102;
#else
_currn = (_TPPrule_0102) TreeNodeAlloc (sizeof (struct _TPrule_0102));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0102;
_SETCOORD(_currn)
_TERMACT_rule_0102;
return ( (NODEPTR) _currn);
}/* Mkrule_0102 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0101 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0101 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0101 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0101;
#else
_currn = (_TPPrule_0101) TreeNodeAlloc (sizeof (struct _TPrule_0101));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0101;
_currn->_desc1 = (_TSPRuleSpec) MkRuleSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0101: root of subtree no. 1 can not be made a RuleSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_0101;
return ( (NODEPTR) _currn);
}/* Mkrule_0101 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_0100 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_0100 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_0100 _currn;
#ifdef __cplusplus
_currn = new _TPrule_0100;
#else
_currn = (_TPPrule_0100) TreeNodeAlloc (sizeof (struct _TPrule_0100));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_0100;
_currn->_desc1 = (_TSPChainSpec) MkChainSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_0100: root of subtree no. 1 can not be made a ChainSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_0100;
return ( (NODEPTR) _currn);
}/* Mkrule_0100 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_099 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_099 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_099 _currn;
#ifdef __cplusplus
_currn = new _TPrule_099;
#else
_currn = (_TPPrule_099) TreeNodeAlloc (sizeof (struct _TPrule_099));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_099;
_currn->_desc1 = (_TSPAttrSpec) MkAttrSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_099: root of subtree no. 1 can not be made a AttrSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_099;
return ( (NODEPTR) _currn);
}/* Mkrule_099 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_098 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_098 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_098 _currn;
#ifdef __cplusplus
_currn = new _TPrule_098;
#else
_currn = (_TPPrule_098) TreeNodeAlloc (sizeof (struct _TPrule_098));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_098;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_098: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_098;
return ( (NODEPTR) _currn);
}/* Mkrule_098 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_097 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_097 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_097 _currn;
#ifdef __cplusplus
_currn = new _TPrule_097;
#else
_currn = (_TPPrule_097) TreeNodeAlloc (sizeof (struct _TPrule_097));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_097;
_currn->_desc1 = (_TSPShieldSyms) MkShieldSyms (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_097: root of subtree no. 1 can not be made a ShieldSyms node ", 0, _coordref);
_currn->_desc2 = (_TSPShieldSym) MkShieldSym (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_097: root of subtree no. 2 can not be made a ShieldSym node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_097;
return ( (NODEPTR) _currn);
}/* Mkrule_097 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_096 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_096 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_096 _currn;
#ifdef __cplusplus
_currn = new _TPrule_096;
#else
_currn = (_TPPrule_096) TreeNodeAlloc (sizeof (struct _TPrule_096));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_096;
_currn->_desc1 = (_TSPShieldSym) MkShieldSym (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_096: root of subtree no. 1 can not be made a ShieldSym node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_096;
return ( (NODEPTR) _currn);
}/* Mkrule_096 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_095 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_095 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_095 _currn;
#ifdef __cplusplus
_currn = new _TPrule_095;
#else
_currn = (_TPPrule_095) TreeNodeAlloc (sizeof (struct _TPrule_095));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_095;
_currn->_desc1 = (_TSPShieldClause) MkShieldClause (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_095: root of subtree no. 1 can not be made a ShieldClause node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_095;
return ( (NODEPTR) _currn);
}/* Mkrule_095 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_094 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_094 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_094 _currn;
#ifdef __cplusplus
_currn = new _TPrule_094;
#else
_currn = (_TPPrule_094) TreeNodeAlloc (sizeof (struct _TPrule_094));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_094;
_currn->_desc1 = (_TSPShieldSyms) MkShieldSyms (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_094: root of subtree no. 1 can not be made a ShieldSyms node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_094;
return ( (NODEPTR) _currn);
}/* Mkrule_094 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_093 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_093 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_093 _currn;
#ifdef __cplusplus
_currn = new _TPrule_093;
#else
_currn = (_TPPrule_093) TreeNodeAlloc (sizeof (struct _TPrule_093));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_093;
_currn->_desc1 = (_TSPShieldSym) MkShieldSym (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_093: root of subtree no. 1 can not be made a ShieldSym node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_093;
return ( (NODEPTR) _currn);
}/* Mkrule_093 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_092 (POSITION *_coordref)
#else
NODEPTR Mkrule_092 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_092 _currn;
#ifdef __cplusplus
_currn = new _TPrule_092;
#else
_currn = (_TPPrule_092) TreeNodeAlloc (sizeof (struct _TPrule_092));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_092;
_SETCOORD(_currn)
_TERMACT_rule_092;
return ( (NODEPTR) _currn);
}/* Mkrule_092 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_091 (POSITION *_coordref)
#else
NODEPTR Mkrule_091 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_091 _currn;
#ifdef __cplusplus
_currn = new _TPrule_091;
#else
_currn = (_TPPrule_091) TreeNodeAlloc (sizeof (struct _TPrule_091));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_091;
_SETCOORD(_currn)
_TERMACT_rule_091;
return ( (NODEPTR) _currn);
}/* Mkrule_091 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_090 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_090 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_090 _currn;
#ifdef __cplusplus
_currn = new _TPrule_090;
#else
_currn = (_TPPrule_090) TreeNodeAlloc (sizeof (struct _TPrule_090));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_090;
_SETCOORD(_currn)
_TERMACT_rule_090;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_090 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_089 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_089 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_089 _currn;
#ifdef __cplusplus
_currn = new _TPrule_089;
#else
_currn = (_TPPrule_089) TreeNodeAlloc (sizeof (struct _TPrule_089));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_089;
_SETCOORD(_currn)
_TERMACT_rule_089;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_089 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_088 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3, NODEPTR _desc4)
#else
NODEPTR Mkrule_088 (_coordref,_desc1,_desc2,_desc3,_desc4)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
	NODEPTR _desc4;
#endif
{	_TPPrule_088 _currn;
#ifdef __cplusplus
_currn = new _TPrule_088;
#else
_currn = (_TPPrule_088) TreeNodeAlloc (sizeof (struct _TPrule_088));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_088;
_currn->_desc1 = (_TSPSubtree) MkSubtree (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_088: root of subtree no. 1 can not be made a Subtree node ", 0, _coordref);
_currn->_desc2 = (_TSPRemoteClause) MkRemoteClause (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_088: root of subtree no. 2 can not be made a RemoteClause node ", 0, _coordref);
_currn->_desc3 = (_TSPShield) MkShield (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_088: root of subtree no. 3 can not be made a Shield node ", 0, _coordref);
_currn->_desc4 = (_TSPExpandOpt) MkExpandOpt (_coordref, _desc4);	
if (((NODEPTR)_currn->_desc4) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_088: root of subtree no. 4 can not be made a ExpandOpt node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_088;
return ( (NODEPTR) _currn);
}/* Mkrule_088 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_087 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_087 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_087 _currn;
#ifdef __cplusplus
_currn = new _TPrule_087;
#else
_currn = (_TPPrule_087) TreeNodeAlloc (sizeof (struct _TPrule_087));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_087;
_currn->_desc1 = (_TSPSubtree) MkSubtree (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_087: root of subtree no. 1 can not be made a Subtree node ", 0, _coordref);
_currn->_desc2 = (_TSPRemoteClause) MkRemoteClause (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_087: root of subtree no. 2 can not be made a RemoteClause node ", 0, _coordref);
_currn->_desc3 = (_TSPShield) MkShield (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_087: root of subtree no. 3 can not be made a Shield node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_087;
return ( (NODEPTR) _currn);
}/* Mkrule_087 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_086 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_086 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_086 _currn;
#ifdef __cplusplus
_currn = new _TPrule_086;
#else
_currn = (_TPPrule_086) TreeNodeAlloc (sizeof (struct _TPrule_086));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_086;
_currn->_desc1 = (_TSPRemoteClause) MkRemoteClause (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_086: root of subtree no. 1 can not be made a RemoteClause node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_086;
return ( (NODEPTR) _currn);
}/* Mkrule_086 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_085 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_085 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_085 _currn;
#ifdef __cplusplus
_currn = new _TPrule_085;
#else
_currn = (_TPPrule_085) TreeNodeAlloc (sizeof (struct _TPrule_085));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_085;
_currn->_desc1 = (_TSPRemoteAttrs) MkRemoteAttrs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_085: root of subtree no. 1 can not be made a RemoteAttrs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_085;
return ( (NODEPTR) _currn);
}/* Mkrule_085 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_084 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_084 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_084 _currn;
#ifdef __cplusplus
_currn = new _TPrule_084;
#else
_currn = (_TPPrule_084) TreeNodeAlloc (sizeof (struct _TPrule_084));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_084;
_currn->_desc1 = (_TSPRemoteAttr) MkRemoteAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_084: root of subtree no. 1 can not be made a RemoteAttr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_084;
return ( (NODEPTR) _currn);
}/* Mkrule_084 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_083 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_083 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_083 _currn;
#ifdef __cplusplus
_currn = new _TPrule_083;
#else
_currn = (_TPPrule_083) TreeNodeAlloc (sizeof (struct _TPrule_083));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_083;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_083: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrUseId) MkAttrUseId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_083: root of subtree no. 2 can not be made a AttrUseId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_083;
return ( (NODEPTR) _currn);
}/* Mkrule_083 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_082 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_082 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_082 _currn;
#ifdef __cplusplus
_currn = new _TPrule_082;
#else
_currn = (_TPPrule_082) TreeNodeAlloc (sizeof (struct _TPrule_082));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_082;
_currn->_desc1 = (_TSPRemoteAttr) MkRemoteAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_082: root of subtree no. 1 can not be made a RemoteAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPRemoteAttrs) MkRemoteAttrs (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_082: root of subtree no. 2 can not be made a RemoteAttrs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_082;
return ( (NODEPTR) _currn);
}/* Mkrule_082 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_081 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_081 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_081 _currn;
#ifdef __cplusplus
_currn = new _TPrule_081;
#else
_currn = (_TPPrule_081) TreeNodeAlloc (sizeof (struct _TPrule_081));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_081;
_currn->_desc1 = (_TSPRemoteAttr) MkRemoteAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_081: root of subtree no. 1 can not be made a RemoteAttr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_081;
return ( (NODEPTR) _currn);
}/* Mkrule_081 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_080 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_080 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_080 _currn;
#ifdef __cplusplus
_currn = new _TPrule_080;
#else
_currn = (_TPPrule_080) TreeNodeAlloc (sizeof (struct _TPrule_080));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_080;
_currn->_desc1 = (_TSPLoop) MkLoop (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_080: root of subtree no. 1 can not be made a Loop node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_080;
return ( (NODEPTR) _currn);
}/* Mkrule_080 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_079 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_079 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_079 _currn;
#ifdef __cplusplus
_currn = new _TPrule_079;
#else
_currn = (_TPPrule_079) TreeNodeAlloc (sizeof (struct _TPrule_079));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_079;
_currn->_desc1 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_079: root of subtree no. 1 can not be made a ExpressionDep node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_079;
return ( (NODEPTR) _currn);
}/* Mkrule_079 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_078 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_078 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_078 _currn;
#ifdef __cplusplus
_currn = new _TPrule_078;
#else
_currn = (_TPPrule_078) TreeNodeAlloc (sizeof (struct _TPrule_078));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_078;
_currn->_desc1 = (_TSPParam) MkParam (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_078: root of subtree no. 1 can not be made a Param node ", 0, _coordref);
_currn->_desc2 = (_TSPParams) MkParams (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_078: root of subtree no. 2 can not be made a Params node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_078;
return ( (NODEPTR) _currn);
}/* Mkrule_078 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_077 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_077 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_077 _currn;
#ifdef __cplusplus
_currn = new _TPrule_077;
#else
_currn = (_TPPrule_077) TreeNodeAlloc (sizeof (struct _TPrule_077));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_077;
_currn->_desc1 = (_TSPParam) MkParam (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_077: root of subtree no. 1 can not be made a Param node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_077;
return ( (NODEPTR) _currn);
}/* Mkrule_077 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_076 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_076 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_076 _currn;
#ifdef __cplusplus
_currn = new _TPrule_076;
#else
_currn = (_TPPrule_076) TreeNodeAlloc (sizeof (struct _TPrule_076));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_076;
_currn->_desc1 = (_TSPParams) MkParams (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_076: root of subtree no. 1 can not be made a Params node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_076;
return ( (NODEPTR) _currn);
}/* Mkrule_076 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_075 (POSITION *_coordref)
#else
NODEPTR Mkrule_075 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_075 _currn;
#ifdef __cplusplus
_currn = new _TPrule_075;
#else
_currn = (_TPPrule_075) TreeNodeAlloc (sizeof (struct _TPrule_075));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_075;
_SETCOORD(_currn)
_TERMACT_rule_075;
return ( (NODEPTR) _currn);
}/* Mkrule_075 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_074 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_074 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_074 _currn;
#ifdef __cplusplus
_currn = new _TPrule_074;
#else
_currn = (_TPPrule_074) TreeNodeAlloc (sizeof (struct _TPrule_074));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_074;
_currn->_desc1 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_074: root of subtree no. 1 can not be made a ExpressionDep node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_074;
return ( (NODEPTR) _currn);
}/* Mkrule_074 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_073 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_073 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_073 _currn;
#ifdef __cplusplus
_currn = new _TPrule_073;
#else
_currn = (_TPPrule_073) TreeNodeAlloc (sizeof (struct _TPrule_073));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_073;
_currn->_desc1 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_073: root of subtree no. 1 can not be made a ExpressionDep node ", 0, _coordref);
_currn->_desc2 = (_TSPAttr) MkAttr (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_073: root of subtree no. 2 can not be made a Attr node ", 0, _coordref);
_currn->_desc3 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_073: root of subtree no. 3 can not be made a ExpressionDep node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_073;
return ( (NODEPTR) _currn);
}/* Mkrule_073 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_072 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_072 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_072 _currn;
#ifdef __cplusplus
_currn = new _TPrule_072;
#else
_currn = (_TPPrule_072) TreeNodeAlloc (sizeof (struct _TPrule_072));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_072;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_072: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_072;
return ( (NODEPTR) _currn);
}/* Mkrule_072 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_071 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_071 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_071 _currn;
#ifdef __cplusplus
_currn = new _TPrule_071;
#else
_currn = (_TPPrule_071) TreeNodeAlloc (sizeof (struct _TPrule_071));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_071;
_currn->_desc1 = (_TSPInheritSyms) MkInheritSyms (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_071: root of subtree no. 1 can not be made a InheritSyms node ", 0, _coordref);
_currn->_desc2 = (_TSPInheritSym) MkInheritSym (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_071: root of subtree no. 2 can not be made a InheritSym node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_071;
return ( (NODEPTR) _currn);
}/* Mkrule_071 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_070 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_070 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_070 _currn;
#ifdef __cplusplus
_currn = new _TPrule_070;
#else
_currn = (_TPPrule_070) TreeNodeAlloc (sizeof (struct _TPrule_070));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_070;
_currn->_desc1 = (_TSPInheritSym) MkInheritSym (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_070: root of subtree no. 1 can not be made a InheritSym node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_070;
return ( (NODEPTR) _currn);
}/* Mkrule_070 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_069 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_069 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_069 _currn;
#ifdef __cplusplus
_currn = new _TPrule_069;
#else
_currn = (_TPPrule_069) TreeNodeAlloc (sizeof (struct _TPrule_069));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_069;
_currn->_desc1 = (_TSPInheritSyms) MkInheritSyms (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_069: root of subtree no. 1 can not be made a InheritSyms node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_069;
return ( (NODEPTR) _currn);
}/* Mkrule_069 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_068 (POSITION *_coordref)
#else
NODEPTR Mkrule_068 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_068 _currn;
#ifdef __cplusplus
_currn = new _TPrule_068;
#else
_currn = (_TPPrule_068) TreeNodeAlloc (sizeof (struct _TPrule_068));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_068;
_SETCOORD(_currn)
_TERMACT_rule_068;
return ( (NODEPTR) _currn);
}/* Mkrule_068 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_067 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_067 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_067 _currn;
#ifdef __cplusplus
_currn = new _TPrule_067;
#else
_currn = (_TPPrule_067) TreeNodeAlloc (sizeof (struct _TPrule_067));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_067;
_SETCOORD(_currn)
_TERMACT_rule_067;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "C_Integer", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_067 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_066 (POSITION *_coordref)
#else
NODEPTR Mkrule_066 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_066 _currn;
#ifdef __cplusplus
_currn = new _TPrule_066;
#else
_currn = (_TPPrule_066) TreeNodeAlloc (sizeof (struct _TPrule_066));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_066;
_SETCOORD(_currn)
_TERMACT_rule_066;
return ( (NODEPTR) _currn);
}/* Mkrule_066 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_065 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_065 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_065 _currn;
#ifdef __cplusplus
_currn = new _TPrule_065;
#else
_currn = (_TPPrule_065) TreeNodeAlloc (sizeof (struct _TPrule_065));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_065;
_currn->_desc1 = (_TSPSymbolDefId) MkSymbolDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_065: root of subtree no. 1 can not be made a SymbolDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_065;
return ( (NODEPTR) _currn);
}/* Mkrule_065 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_064 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_064 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_064 _currn;
#ifdef __cplusplus
_currn = new _TPrule_064;
#else
_currn = (_TPPrule_064) TreeNodeAlloc (sizeof (struct _TPrule_064));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_064;
_currn->_desc1 = (_TSPG1) MkG1 (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_064: root of subtree no. 1 can not be made a G1 node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbolDefId) MkSymbolDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_064: root of subtree no. 2 can not be made a SymbolDefId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_064;
return ( (NODEPTR) _currn);
}/* Mkrule_064 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_063 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_063 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_063 _currn;
#ifdef __cplusplus
_currn = new _TPrule_063;
#else
_currn = (_TPPrule_063) TreeNodeAlloc (sizeof (struct _TPrule_063));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_063;
_currn->_desc1 = (_TSPSymbolRef) MkSymbolRef (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_063: root of subtree no. 1 can not be made a SymbolRef node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_063;
return ( (NODEPTR) _currn);
}/* Mkrule_063 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_062 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_062 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_062 _currn;
#ifdef __cplusplus
_currn = new _TPrule_062;
#else
_currn = (_TPPrule_062) TreeNodeAlloc (sizeof (struct _TPrule_062));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_062;
_currn->_desc1 = (_TSPRhsAttrs) MkRhsAttrs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_062: root of subtree no. 1 can not be made a RhsAttrs node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_062;
return ( (NODEPTR) _currn);
}/* Mkrule_062 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_061 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_061 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_061 _currn;
#ifdef __cplusplus
_currn = new _TPrule_061;
#else
_currn = (_TPPrule_061) TreeNodeAlloc (sizeof (struct _TPrule_061));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_061;
_currn->_desc1 = (_TSPRemoteExpression) MkRemoteExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_061: root of subtree no. 1 can not be made a RemoteExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_061;
return ( (NODEPTR) _currn);
}/* Mkrule_061 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_060 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_060 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_060 _currn;
#ifdef __cplusplus
_currn = new _TPrule_060;
#else
_currn = (_TPPrule_060) TreeNodeAlloc (sizeof (struct _TPrule_060));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_060;
_SETCOORD(_currn)
_TERMACT_rule_060;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "P_String", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_060 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_059 (POSITION *_coordref, int _TERM1, NODEPTR _desc1)
#else
NODEPTR Mkrule_059 (_coordref, _TERM1,_desc1)
	POSITION *_coordref;
	int _TERM1;
	NODEPTR _desc1;
#endif
{	_TPPrule_059 _currn;
#ifdef __cplusplus
_currn = new _TPrule_059;
#else
_currn = (_TPPrule_059) TreeNodeAlloc (sizeof (struct _TPrule_059));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_059;
_currn->_desc1 = (_TSPParamsOpt) MkParamsOpt (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_059: root of subtree no. 1 can not be made a ParamsOpt node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_059;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_059 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_058 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_058 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_058 _currn;
#ifdef __cplusplus
_currn = new _TPrule_058;
#else
_currn = (_TPPrule_058) TreeNodeAlloc (sizeof (struct _TPrule_058));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_058;
_currn->_desc1 = (_TSPExpression) MkExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_058: root of subtree no. 1 can not be made a Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPDepClause) MkDepClause (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_058: root of subtree no. 2 can not be made a DepClause node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_058;
return ( (NODEPTR) _currn);
}/* Mkrule_058 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_057 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_057 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_057 _currn;
#ifdef __cplusplus
_currn = new _TPrule_057;
#else
_currn = (_TPPrule_057) TreeNodeAlloc (sizeof (struct _TPrule_057));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_057;
_currn->_desc1 = (_TSPExpression) MkExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_057: root of subtree no. 1 can not be made a Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_057;
return ( (NODEPTR) _currn);
}/* Mkrule_057 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_056 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_056 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_056 _currn;
#ifdef __cplusplus
_currn = new _TPrule_056;
#else
_currn = (_TPPrule_056) TreeNodeAlloc (sizeof (struct _TPrule_056));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_056;
_SETCOORD(_currn)
_TERMACT_rule_056;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "C_String", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_056 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_055 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_055 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_055 _currn;
#ifdef __cplusplus
_currn = new _TPrule_055;
#else
_currn = (_TPPrule_055) TreeNodeAlloc (sizeof (struct _TPrule_055));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_055;
_SETCOORD(_currn)
_TERMACT_rule_055;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "C_Integer", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_055 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_054 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_054 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_054 _currn;
#ifdef __cplusplus
_currn = new _TPrule_054;
#else
_currn = (_TPPrule_054) TreeNodeAlloc (sizeof (struct _TPrule_054));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_054;
_SETCOORD(_currn)
_TERMACT_rule_054;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "C_Float", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_054 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_053 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_053 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_053 _currn;
#ifdef __cplusplus
_currn = new _TPrule_053;
#else
_currn = (_TPPrule_053) TreeNodeAlloc (sizeof (struct _TPrule_053));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_053;
_currn->_desc1 = (_TSPAttr) MkAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_053: root of subtree no. 1 can not be made a Attr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_053;
return ( (NODEPTR) _currn);
}/* Mkrule_053 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_052 (POSITION *_coordref, NODEPTR _desc1, int _TERM1, int _TERM2, int _TERM3)
#else
NODEPTR Mkrule_052 (_coordref,_desc1, _TERM1, _TERM2, _TERM3)
	POSITION *_coordref;
	NODEPTR _desc1;
	int _TERM1;
	int _TERM2;
	int _TERM3;
#endif
{	_TPPrule_052 _currn;
#ifdef __cplusplus
_currn = new _TPrule_052;
#else
_currn = (_TPPrule_052) TreeNodeAlloc (sizeof (struct _TPrule_052));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_052;
_currn->_desc1 = (_TSPTypeId) MkTypeId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_052: root of subtree no. 1 can not be made a TypeId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_052;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier[1]", _TERM1);
#endif

#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier[2]", _TERM2);
#endif

#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier[3]", _TERM3);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_052 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_051 (POSITION *_coordref)
#else
NODEPTR Mkrule_051 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_051 _currn;
#ifdef __cplusplus
_currn = new _TPrule_051;
#else
_currn = (_TPPrule_051) TreeNodeAlloc (sizeof (struct _TPrule_051));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_051;
_SETCOORD(_currn)
_TERMACT_rule_051;
return ( (NODEPTR) _currn);
}/* Mkrule_051 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_050 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_050 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_050 _currn;
#ifdef __cplusplus
_currn = new _TPrule_050;
#else
_currn = (_TPPrule_050) TreeNodeAlloc (sizeof (struct _TPrule_050));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_050;
_currn->_desc1 = (_TSPDepAttrs) MkDepAttrs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_050: root of subtree no. 1 can not be made a DepAttrs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_050;
return ( (NODEPTR) _currn);
}/* Mkrule_050 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_049 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_049 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_049 _currn;
#ifdef __cplusplus
_currn = new _TPrule_049;
#else
_currn = (_TPPrule_049) TreeNodeAlloc (sizeof (struct _TPrule_049));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_049;
_currn->_desc1 = (_TSPDepAttr) MkDepAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_049: root of subtree no. 1 can not be made a DepAttr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_049;
return ( (NODEPTR) _currn);
}/* Mkrule_049 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_048 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_048 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_048 _currn;
#ifdef __cplusplus
_currn = new _TPrule_048;
#else
_currn = (_TPPrule_048) TreeNodeAlloc (sizeof (struct _TPrule_048));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_048;
_currn->_desc1 = (_TSPDependence) MkDependence (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_048: root of subtree no. 1 can not be made a Dependence node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_048;
return ( (NODEPTR) _currn);
}/* Mkrule_048 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_047 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_047 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_047 _currn;
#ifdef __cplusplus
_currn = new _TPrule_047;
#else
_currn = (_TPPrule_047) TreeNodeAlloc (sizeof (struct _TPrule_047));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_047;
_currn->_desc1 = (_TSPDependence) MkDependence (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_047: root of subtree no. 1 can not be made a Dependence node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_047;
return ( (NODEPTR) _currn);
}/* Mkrule_047 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_046 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_046 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_046 _currn;
#ifdef __cplusplus
_currn = new _TPrule_046;
#else
_currn = (_TPPrule_046) TreeNodeAlloc (sizeof (struct _TPrule_046));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_046;
_currn->_desc1 = (_TSPDepAttr) MkDepAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_046: root of subtree no. 1 can not be made a DepAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPDepAttrs) MkDepAttrs (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_046: root of subtree no. 2 can not be made a DepAttrs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_046;
return ( (NODEPTR) _currn);
}/* Mkrule_046 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_045 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_045 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_045 _currn;
#ifdef __cplusplus
_currn = new _TPrule_045;
#else
_currn = (_TPPrule_045) TreeNodeAlloc (sizeof (struct _TPrule_045));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_045;
_currn->_desc1 = (_TSPDepAttr) MkDepAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_045: root of subtree no. 1 can not be made a DepAttr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_045;
return ( (NODEPTR) _currn);
}/* Mkrule_045 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_044 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_044 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_044 _currn;
#ifdef __cplusplus
_currn = new _TPrule_044;
#else
_currn = (_TPPrule_044) TreeNodeAlloc (sizeof (struct _TPrule_044));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_044;
_currn->_desc1 = (_TSPRhsAttrs) MkRhsAttrs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_044: root of subtree no. 1 can not be made a RhsAttrs node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_044;
return ( (NODEPTR) _currn);
}/* Mkrule_044 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_043 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_043 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_043 _currn;
#ifdef __cplusplus
_currn = new _TPrule_043;
#else
_currn = (_TPPrule_043) TreeNodeAlloc (sizeof (struct _TPrule_043));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_043;
_currn->_desc1 = (_TSPRemoteExpression) MkRemoteExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_043: root of subtree no. 1 can not be made a RemoteExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_043;
return ( (NODEPTR) _currn);
}/* Mkrule_043 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_042 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_042 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_042 _currn;
#ifdef __cplusplus
_currn = new _TPrule_042;
#else
_currn = (_TPPrule_042) TreeNodeAlloc (sizeof (struct _TPrule_042));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_042;
_currn->_desc1 = (_TSPAttr) MkAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_042: root of subtree no. 1 can not be made a Attr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_042;
return ( (NODEPTR) _currn);
}/* Mkrule_042 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_041 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_041 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_041 _currn;
#ifdef __cplusplus
_currn = new _TPrule_041;
#else
_currn = (_TPPrule_041) TreeNodeAlloc (sizeof (struct _TPrule_041));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_041;
_currn->_desc1 = (_TSPAttr) MkAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_041: root of subtree no. 1 can not be made a Attr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_041;
return ( (NODEPTR) _currn);
}/* Mkrule_041 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_040 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_040 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_040 _currn;
#ifdef __cplusplus
_currn = new _TPrule_040;
#else
_currn = (_TPPrule_040) TreeNodeAlloc (sizeof (struct _TPrule_040));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_040;
_currn->_desc1 = (_TSPPlainComp) MkPlainComp (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_040: root of subtree no. 1 can not be made a PlainComp node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_040;
return ( (NODEPTR) _currn);
}/* Mkrule_040 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_039 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_039 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_039 _currn;
#ifdef __cplusplus
_currn = new _TPrule_039;
#else
_currn = (_TPPrule_039) TreeNodeAlloc (sizeof (struct _TPrule_039));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_039;
_currn->_desc1 = (_TSPAttrComp) MkAttrComp (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_039: root of subtree no. 1 can not be made a AttrComp node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_039;
return ( (NODEPTR) _currn);
}/* Mkrule_039 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_038 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_038 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_038 _currn;
#ifdef __cplusplus
_currn = new _TPrule_038;
#else
_currn = (_TPPrule_038) TreeNodeAlloc (sizeof (struct _TPrule_038));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_038;
_currn->_desc1 = (_TSPComputation) MkComputation (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_038: root of subtree no. 1 can not be made a Computation node ", 0, _coordref);
_currn->_desc2 = (_TSPComputations) MkComputations (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_038: root of subtree no. 2 can not be made a Computations node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_038;
return ( (NODEPTR) _currn);
}/* Mkrule_038 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_037 (POSITION *_coordref)
#else
NODEPTR Mkrule_037 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_037 _currn;
#ifdef __cplusplus
_currn = new _TPrule_037;
#else
_currn = (_TPPrule_037) TreeNodeAlloc (sizeof (struct _TPrule_037));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_037;
_SETCOORD(_currn)
_TERMACT_rule_037;
return ( (NODEPTR) _currn);
}/* Mkrule_037 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_036 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_036 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_036 _currn;
#ifdef __cplusplus
_currn = new _TPrule_036;
#else
_currn = (_TPPrule_036) TreeNodeAlloc (sizeof (struct _TPrule_036));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_036;
_currn->_desc1 = (_TSPCompute) MkCompute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_036: root of subtree no. 1 can not be made a Compute node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_036;
return ( (NODEPTR) _currn);
}/* Mkrule_036 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_035 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_035 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_035 _currn;
#ifdef __cplusplus
_currn = new _TPrule_035;
#else
_currn = (_TPPrule_035) TreeNodeAlloc (sizeof (struct _TPrule_035));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_035;
_currn->_desc1 = (_TSPCompute) MkCompute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_035: root of subtree no. 1 can not be made a Compute node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_035;
return ( (NODEPTR) _currn);
}/* Mkrule_035 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_034 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_034 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_034 _currn;
#ifdef __cplusplus
_currn = new _TPrule_034;
#else
_currn = (_TPPrule_034) TreeNodeAlloc (sizeof (struct _TPrule_034));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_034;
_currn->_desc1 = (_TSPComputations) MkComputations (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_034: root of subtree no. 1 can not be made a Computations node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_034;
return ( (NODEPTR) _currn);
}/* Mkrule_034 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_033 (POSITION *_coordref)
#else
NODEPTR Mkrule_033 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_033 _currn;
#ifdef __cplusplus
_currn = new _TPrule_033;
#else
_currn = (_TPPrule_033) TreeNodeAlloc (sizeof (struct _TPrule_033));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_033;
_SETCOORD(_currn)
_TERMACT_rule_033;
return ( (NODEPTR) _currn);
}/* Mkrule_033 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_032 (POSITION *_coordref)
#else
NODEPTR Mkrule_032 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_032 _currn;
#ifdef __cplusplus
_currn = new _TPrule_032;
#else
_currn = (_TPPrule_032) TreeNodeAlloc (sizeof (struct _TPrule_032));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_032;
_SETCOORD(_currn)
_TERMACT_rule_032;
return ( (NODEPTR) _currn);
}/* Mkrule_032 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_031 (POSITION *_coordref)
#else
NODEPTR Mkrule_031 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_031 _currn;
#ifdef __cplusplus
_currn = new _TPrule_031;
#else
_currn = (_TPPrule_031) TreeNodeAlloc (sizeof (struct _TPrule_031));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_031;
_SETCOORD(_currn)
_TERMACT_rule_031;
return ( (NODEPTR) _currn);
}/* Mkrule_031 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_030 (POSITION *_coordref)
#else
NODEPTR Mkrule_030 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_030 _currn;
#ifdef __cplusplus
_currn = new _TPrule_030;
#else
_currn = (_TPPrule_030) TreeNodeAlloc (sizeof (struct _TPrule_030));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_030;
_SETCOORD(_currn)
_TERMACT_rule_030;
return ( (NODEPTR) _currn);
}/* Mkrule_030 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_029 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_029 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_029 _currn;
#ifdef __cplusplus
_currn = new _TPrule_029;
#else
_currn = (_TPPrule_029) TreeNodeAlloc (sizeof (struct _TPrule_029));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_029;
_currn->_desc1 = (_TSPChainNames) MkChainNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_029: root of subtree no. 1 can not be made a ChainNames node ", 0, _coordref);
_currn->_desc2 = (_TSPTypeId) MkTypeId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_029: root of subtree no. 2 can not be made a TypeId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_029;
return ( (NODEPTR) _currn);
}/* Mkrule_029 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_028 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_028 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_028 _currn;
#ifdef __cplusplus
_currn = new _TPrule_028;
#else
_currn = (_TPPrule_028) TreeNodeAlloc (sizeof (struct _TPrule_028));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_028;
_currn->_desc1 = (_TSPChainNames) MkChainNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_028: root of subtree no. 1 can not be made a ChainNames node ", 0, _coordref);
_currn->_desc2 = (_TSPChainName) MkChainName (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_028: root of subtree no. 2 can not be made a ChainName node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_028;
return ( (NODEPTR) _currn);
}/* Mkrule_028 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_027 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_027 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_027 _currn;
#ifdef __cplusplus
_currn = new _TPrule_027;
#else
_currn = (_TPPrule_027) TreeNodeAlloc (sizeof (struct _TPrule_027));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_027;
_currn->_desc1 = (_TSPChainName) MkChainName (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_027: root of subtree no. 1 can not be made a ChainName node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_027;
return ( (NODEPTR) _currn);
}/* Mkrule_027 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_026 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_026 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_026 _currn;
#ifdef __cplusplus
_currn = new _TPrule_026;
#else
_currn = (_TPPrule_026) TreeNodeAlloc (sizeof (struct _TPrule_026));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_026;
_SETCOORD(_currn)
_TERMACT_rule_026;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_026 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_025 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_025 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_025 _currn;
#ifdef __cplusplus
_currn = new _TPrule_025;
#else
_currn = (_TPPrule_025) TreeNodeAlloc (sizeof (struct _TPrule_025));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_025;
_SETCOORD(_currn)
_TERMACT_rule_025;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_025 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_024 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_024 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_024 _currn;
#ifdef __cplusplus
_currn = new _TPrule_024;
#else
_currn = (_TPPrule_024) TreeNodeAlloc (sizeof (struct _TPrule_024));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_024;
_currn->_desc1 = (_TSPSymOcc) MkSymOcc (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_024: root of subtree no. 1 can not be made a SymOcc node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrUseId) MkAttrUseId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_024: root of subtree no. 2 can not be made a AttrUseId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_024;
return ( (NODEPTR) _currn);
}/* Mkrule_024 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_023 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_023 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_023 _currn;
#ifdef __cplusplus
_currn = new _TPrule_023;
#else
_currn = (_TPPrule_023) TreeNodeAlloc (sizeof (struct _TPrule_023));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_023;
_currn->_desc1 = (_TSPAttrNames) MkAttrNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_023: root of subtree no. 1 can not be made a AttrNames node ", 0, _coordref);
_currn->_desc2 = (_TSPTypeId) MkTypeId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_023: root of subtree no. 2 can not be made a TypeId node ", 0, _coordref);
_currn->_desc3 = (_TSPClass) MkClass (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_023: root of subtree no. 3 can not be made a Class node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_023;
return ( (NODEPTR) _currn);
}/* Mkrule_023 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_022 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_022 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_022 _currn;
#ifdef __cplusplus
_currn = new _TPrule_022;
#else
_currn = (_TPPrule_022) TreeNodeAlloc (sizeof (struct _TPrule_022));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_022;
_currn->_desc1 = (_TSPAttrNames) MkAttrNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_022: root of subtree no. 1 can not be made a AttrNames node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrName) MkAttrName (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_022: root of subtree no. 2 can not be made a AttrName node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_022;
return ( (NODEPTR) _currn);
}/* Mkrule_022 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_021 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_021 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_021 _currn;
#ifdef __cplusplus
_currn = new _TPrule_021;
#else
_currn = (_TPPrule_021) TreeNodeAlloc (sizeof (struct _TPrule_021));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_021;
_currn->_desc1 = (_TSPAttrName) MkAttrName (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_021: root of subtree no. 1 can not be made a AttrName node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_021;
return ( (NODEPTR) _currn);
}/* Mkrule_021 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_020 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_020 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_020 _currn;
#ifdef __cplusplus
_currn = new _TPrule_020;
#else
_currn = (_TPPrule_020) TreeNodeAlloc (sizeof (struct _TPrule_020));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_020;
_SETCOORD(_currn)
_TERMACT_rule_020;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_020 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_019 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_019 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_019 _currn;
#ifdef __cplusplus
_currn = new _TPrule_019;
#else
_currn = (_TPPrule_019) TreeNodeAlloc (sizeof (struct _TPrule_019));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_019;
_currn->_desc1 = (_TSPAttrDefs) MkAttrDefs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_019: root of subtree no. 1 can not be made a AttrDefs node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_019;
return ( (NODEPTR) _currn);
}/* Mkrule_019 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_018 (POSITION *_coordref)
#else
NODEPTR Mkrule_018 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_018 _currn;
#ifdef __cplusplus
_currn = new _TPrule_018;
#else
_currn = (_TPPrule_018) TreeNodeAlloc (sizeof (struct _TPrule_018));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_018;
_SETCOORD(_currn)
_TERMACT_rule_018;
return ( (NODEPTR) _currn);
}/* Mkrule_018 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_017 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_017 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_017 _currn;
#ifdef __cplusplus
_currn = new _TPrule_017;
#else
_currn = (_TPPrule_017) TreeNodeAlloc (sizeof (struct _TPrule_017));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_017;
_currn->_desc1 = (_TSPAttrDefs) MkAttrDefs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_017: root of subtree no. 1 can not be made a AttrDefs node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrDef) MkAttrDef (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_017: root of subtree no. 2 can not be made a AttrDef node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_017;
return ( (NODEPTR) _currn);
}/* Mkrule_017 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_016 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_016 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_016 _currn;
#ifdef __cplusplus
_currn = new _TPrule_016;
#else
_currn = (_TPPrule_016) TreeNodeAlloc (sizeof (struct _TPrule_016));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_016;
_currn->_desc1 = (_TSPAttrDef) MkAttrDef (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_016: root of subtree no. 1 can not be made a AttrDef node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_016;
return ( (NODEPTR) _currn);
}/* Mkrule_016 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_015 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_015 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_015 _currn;
#ifdef __cplusplus
_currn = new _TPrule_015;
#else
_currn = (_TPPrule_015) TreeNodeAlloc (sizeof (struct _TPrule_015));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_015;
_currn->_desc1 = (_TSPAttrDefIds) MkAttrDefIds (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_015: root of subtree no. 1 can not be made a AttrDefIds node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrDefId) MkAttrDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_015: root of subtree no. 2 can not be made a AttrDefId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_015;
return ( (NODEPTR) _currn);
}/* Mkrule_015 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_014 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_014 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_014 _currn;
#ifdef __cplusplus
_currn = new _TPrule_014;
#else
_currn = (_TPPrule_014) TreeNodeAlloc (sizeof (struct _TPrule_014));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_014;
_currn->_desc1 = (_TSPAttrDefId) MkAttrDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_014: root of subtree no. 1 can not be made a AttrDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_014;
return ( (NODEPTR) _currn);
}/* Mkrule_014 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_013 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_013 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_013 _currn;
#ifdef __cplusplus
_currn = new _TPrule_013;
#else
_currn = (_TPPrule_013) TreeNodeAlloc (sizeof (struct _TPrule_013));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_013;
_SETCOORD(_currn)
_TERMACT_rule_013;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_013 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_012 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_012 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_012 _currn;
#ifdef __cplusplus
_currn = new _TPrule_012;
#else
_currn = (_TPPrule_012) TreeNodeAlloc (sizeof (struct _TPrule_012));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_012;
_currn->_desc1 = (_TSPAttrDefIds) MkAttrDefIds (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_012: root of subtree no. 1 can not be made a AttrDefIds node ", 0, _coordref);
_currn->_desc2 = (_TSPTypeId) MkTypeId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_012: root of subtree no. 2 can not be made a TypeId node ", 0, _coordref);
_currn->_desc3 = (_TSPClass) MkClass (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_012: root of subtree no. 3 can not be made a Class node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_012;
return ( (NODEPTR) _currn);
}/* Mkrule_012 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_011 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_011 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_011 _currn;
#ifdef __cplusplus
_currn = new _TPrule_011;
#else
_currn = (_TPPrule_011) TreeNodeAlloc (sizeof (struct _TPrule_011));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_011;
_currn->_desc1 = (_TSPDefAttr) MkDefAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_011: root of subtree no. 1 can not be made a DefAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPLoop) MkLoop (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_011: root of subtree no. 2 can not be made a Loop node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_011;
return ( (NODEPTR) _currn);
}/* Mkrule_011 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_010 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_010 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_010 _currn;
#ifdef __cplusplus
_currn = new _TPrule_010;
#else
_currn = (_TPPrule_010) TreeNodeAlloc (sizeof (struct _TPrule_010));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_010;
_currn->_desc1 = (_TSPDefAttr) MkDefAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_010: root of subtree no. 1 can not be made a DefAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPAsgnTok) MkAsgnTok (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_010: root of subtree no. 2 can not be made a AsgnTok node ", 0, _coordref);
_currn->_desc3 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_010: root of subtree no. 3 can not be made a ExpressionDep node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_010;
return ( (NODEPTR) _currn);
}/* Mkrule_010 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_09 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_09 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_09 _currn;
#ifdef __cplusplus
_currn = new _TPrule_09;
#else
_currn = (_TPPrule_09) TreeNodeAlloc (sizeof (struct _TPrule_09));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_09;
_currn->_desc1 = (_TSPDefAttr) MkDefAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_09: root of subtree no. 1 can not be made a DefAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_09: root of subtree no. 2 can not be made a ExpressionDep node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_09;
return ( (NODEPTR) _currn);
}/* Mkrule_09 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_08 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_08 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_08 _currn;
#ifdef __cplusplus
_currn = new _TPrule_08;
#else
_currn = (_TPPrule_08) TreeNodeAlloc (sizeof (struct _TPrule_08));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_08;
_currn->_desc1 = (_TSPAttrUseId) MkAttrUseId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_08: root of subtree no. 1 can not be made a AttrUseId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_08;
return ( (NODEPTR) _currn);
}/* Mkrule_08 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_07 (POSITION *_coordref)
#else
NODEPTR Mkrule_07 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_07 _currn;
#ifdef __cplusplus
_currn = new _TPrule_07;
#else
_currn = (_TPPrule_07) TreeNodeAlloc (sizeof (struct _TPrule_07));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_07;
_SETCOORD(_currn)
_TERMACT_rule_07;
return ( (NODEPTR) _currn);
}/* Mkrule_07 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_06 (POSITION *_coordref)
#else
NODEPTR Mkrule_06 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_06 _currn;
#ifdef __cplusplus
_currn = new _TPrule_06;
#else
_currn = (_TPPrule_06) TreeNodeAlloc (sizeof (struct _TPrule_06));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_06;
_SETCOORD(_currn)
_TERMACT_rule_06;
return ( (NODEPTR) _currn);
}/* Mkrule_06 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_05 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_05 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_05 _currn;
#ifdef __cplusplus
_currn = new _TPrule_05;
#else
_currn = (_TPPrule_05) TreeNodeAlloc (sizeof (struct _TPrule_05));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_05;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_05: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_05;
return ( (NODEPTR) _currn);
}/* Mkrule_05 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_04 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_04 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_04 _currn;
#ifdef __cplusplus
_currn = new _TPrule_04;
#else
_currn = (_TPPrule_04) TreeNodeAlloc (sizeof (struct _TPrule_04));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_04;
_currn->_desc1 = (_TSPAlts) MkAlts (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_04: root of subtree no. 1 can not be made a Alts node ", 0, _coordref);
_currn->_desc2 = (_TSPAlt) MkAlt (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_04: root of subtree no. 2 can not be made a Alt node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_04;
return ( (NODEPTR) _currn);
}/* Mkrule_04 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_03 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_03 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_03 _currn;
#ifdef __cplusplus
_currn = new _TPrule_03;
#else
_currn = (_TPPrule_03) TreeNodeAlloc (sizeof (struct _TPrule_03));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_03;
_currn->_desc1 = (_TSPAlt) MkAlt (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_03: root of subtree no. 1 can not be made a Alt node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_03;
return ( (NODEPTR) _currn);
}/* Mkrule_03 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_02 (POSITION *_coordref)
#else
NODEPTR Mkrule_02 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_02 _currn;
#ifdef __cplusplus
_currn = new _TPrule_02;
#else
_currn = (_TPPrule_02) TreeNodeAlloc (sizeof (struct _TPrule_02));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_02;
_SETCOORD(_currn)
_TERMACT_rule_02;
return ( (NODEPTR) _currn);
}/* Mkrule_02 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_01 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_01 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_01 _currn;
#ifdef __cplusplus
_currn = new _TPrule_01;
#else
_currn = (_TPPrule_01) TreeNodeAlloc (sizeof (struct _TPrule_01));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_01;
_currn->_desc1 = (_TSPSpecs) MkSpecs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_01: root of subtree no. 1 can not be made a Specs node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_01;
return ( (NODEPTR) _currn);
}/* Mkrule_01 */
