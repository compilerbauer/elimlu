/***********************************************************************\
*	keywords.c							*
*	strings with keywords for LIGA option handler			*
*									*
*	Written 06/20/90 - 07/23/91	by HaDeS			*
\***********************************************************************/

#include <stdio.h>
#include "keywords.h"

keywords exp1kw= {
	"CONST_ATTR_NAME", "INCL_ATTR_NAME",
	"CHAIN_PRE_ATTR_NAME", "CHAIN_POST_ATTR_NAME", NULL
};

keywords exp2kw= {
	"INCLUDINGS_SEPARATE", "INFO", NULL
};

keywords exp3kw= {
	"ON", "OFF", NULL
};

keywords ord1kw= {
	"PARTITION", "TOPOLOGICAL", NULL
};

keywords ord2kw= {
	"EARLY", "LATE", NULL
};

keywords ord3kw= {
	"DIRECT_SYMBOL", "TRANSITIVE_SYMBOL", "INDUCED_SYMBOL",
	"DIRECT_RULE", "TRANSITIVE_RULE", "INDUCED_RULE", "PARTITIONED_RULE",
	"PARTITION", "VISIT_SEQUENCE", NULL
};

keywords ord4kw= {
	"COMPLETE", "BOTTOM_UP", "TOP_DOWN", NULL
};

keywords opt1kw= {
	"OFF", "INFO", "MORE_GLOBALS", "NO_VARIABLES", "NO_STACKS", "NO_GROUPING", NULL
};

keywords opt2kw= {
	"VARIABLE", "STACK", "ALL", NULL
};

keywords opt3kw= {
	"GLOBAL", "GROUP", NULL
};

keywords opt4kw= {
	"STACK", "VAR", NULL
};

keywords be1kw= {
	"READABLE", "KEEP_TREE", "FREE_TREE", NULL
};

keywords be2kw= {
	"SPLIT_CASE", "ATSTACK_SIZE", "NODESTACK_SIZE", NULL
};

