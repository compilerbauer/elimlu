/***********************************************************************\
*	various.c							*
*	various functions for the option handler			*
*									*
*	Written 06/19/90 - 02/21/91	by HaDeS			*
\***********************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "err.h"
#include "opt_liga.h"

extern  POSITION        curpos;

/*
** FUNCTION DEFINITIONS
*/

void	errormesg (msg)
char *msg;
{
	message (ERROR, msg, 0, &curpos);
} /* errormesg */

char	*getelistring (id)
int id;
{
	char	*newstr;

	newstr= (char *) malloc(strlen(strng[id])+1);
	return (strcpy(newstr, strng[id]));
} /* getelistring */

char	*str2upper (str)
char *str;
{
	char	*c;

	for (c= str; *c; c++)
		if (islower(*c))
			*c= toupper(*c);
	return (str);
} /* str2upper */

int     cmpidkw (id, kws)
int id;
keywords kws;
/* compare identifier to keywords       */
{
	int     result;
	int     i;
	char    *keystr;
	char    **cmpstr;

	result= -1;
	keystr= str2upper (getelistring (id));
	for (cmpstr= kws, i=0; (*cmpstr); cmpstr++, i++)
	if (!strcmp (keystr, (*cmpstr)))
	result= i;

	return (result);
} /* cmpidkw */

