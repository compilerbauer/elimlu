/***********************************************************************\
*	conflicts.h							*
*	Header file for test function for the option handler		*
*									*
*	Written 08/08/90 - 10/15/90	by HaDeS			*
\***********************************************************************/

#ifndef CONFLICTS_H_INCL
#define CONFLICTS_H_INCL
#ifdef ELI_ARG
#undef ELI_ARG
#endif

#if defined(__STDC__) || defined(__cplusplus)
#define ELI_ARG(proto)    proto
#else
#define ELI_ARG(proto)    ()
#endif

extern	int	Conflict_Test ELI_ARG((OptCollect));

#endif

