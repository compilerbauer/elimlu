/***********************************************************************\
*	keywords.h							*
*	strings with keywords for LIGA option handler			*
*									*
*	Written 06/20/90 - 07/23/90	by HaDeS			*
\***********************************************************************/

#ifndef _KEYWORDS_H_INCL
#define _KEYWORDS_H_INCL

typedef	char	*keywords[];

extern	keywords	exp1kw;
extern	keywords	exp2kw;
extern	keywords	exp3kw;
extern	keywords	ord1kw;
extern	keywords	ord2kw;
extern	keywords	ord3kw;
extern	keywords	ord4kw;
extern	keywords	opt1kw;
extern	keywords	opt2kw;
extern	keywords	opt3kw;
extern	keywords	opt4kw;
extern	keywords	be1kw;
extern	keywords	be2kw;

#endif

