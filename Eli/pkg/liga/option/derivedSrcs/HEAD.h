#ifndef HEAD_H1
#define HEAD_H1
/*
 * Header file for CLP
 */

#include "clp.h"
#endif

#ifndef HEAD_H2
#define HEAD_H2

/***********************************************************************\
*	option.head							*
*	Header file for specification of the LIDO option handler	*
*									*
*	Written 11/13/90 - 07/23/91	by HaDeS			*
\***********************************************************************/

#ifndef _OPTION_HEAD_INCL
#define _OPTION_HEAD_INCL


#include <string.h>
#include "csm.h"
#include "option_enums.h"
#include "option_types.h"
#include "keywords.h"
#include "various.h"
#include "optlists.h"
#include "output.h"
#include "conflicts.h"
#include "err.h"
#include "opt_liga.h"

#endif

#endif

#ifndef HEAD_H3
#define HEAD_H3
#include "pdl_gen.h"
#endif

#ifndef HEAD_H4
#define HEAD_H4
/* Type definitions for the property list module */

#include "deftbl.h"

#endif

#ifndef HEAD_H5
#define HEAD_H5
/* (C) Copyright 1997 University of Paderborn */

/* This file is part of the Eli Module Library.

The Eli Module Library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

The Eli Module Library is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with the Eli Module Library; see the file COPYING.LIB.
If not, write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Eli into the
   directory resulting from a :source derivation, you may use that
   created file as a part of that directory without restriction. */

#include "treecon.h"

#endif

#ifndef HEAD_H6
#define HEAD_H6

#include "DefTableKeyList.h"
#endif

#ifndef HEAD_H7
#define HEAD_H7

#include "VoidPtrList.h"
#endif

