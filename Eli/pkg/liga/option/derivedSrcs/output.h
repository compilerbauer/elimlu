/***********************************************************************\
*	output.h							*
*	Header file for output function for the option handler		*
*									*
*	Written 07/02/90 - 11/14/90	by HaDeS			*
\***********************************************************************/

#ifndef _OUTPUT_H_INCL
#define _OUTPUT_H_INCL

#define	EXPFNAME	"expand.options"
#define	ORDFNAME	"order.options"
#define	OPTFNAME	"optim.options"
#define	BEFNAME		"backend.options"

#ifdef ELI_ARG
#undef ELI_ARG
#endif

#if defined(__STDC__) || defined(__cplusplus)
#define ELI_ARG(proto)    proto
#else
#define ELI_ARG(proto)    ()
#endif

extern	void	Output ELI_ARG((OptCollect, int));

#endif

