
/* definition of tree node structure */

#ifndef NODE_H
#define NODE_H
#include "err.h"
#include "nodeptr.h" /* defines NODEPTR */
#include "HEAD.h"

#ifdef MONITOR
#define _NODECOMMON int _prod; POSITION _coord; int _uid;
#else
#define _NODECOMMON int _prod;
#endif
struct NODEPTR_struct {
        _NODECOMMON
#ifdef __cplusplus
        void* operator new(size_t size);
#endif
};

typedef struct _TSSpec* _TSPSpec;
typedef struct _TSShieldSyms* _TSPShieldSyms;
typedef struct _TSRemoteAttrs* _TSPRemoteAttrs;
typedef struct _TSParams* _TSPParams;
typedef struct _TSInheritSyms* _TSPInheritSyms;
typedef struct _TSG1* _TSPG1;
typedef struct _TSDependence* _TSPDependence;
typedef struct _TSDepAttrs* _TSPDepAttrs;
typedef struct _TSComputations* _TSPComputations;
typedef struct _TSAttrDefs* _TSPAttrDefs;
typedef struct _TSSpecs* _TSPSpecs;
typedef struct _TSAsgnTok* _TSPAsgnTok;
typedef struct _TSParam* _TSPParam;
typedef struct _TSParamsOpt* _TSPParamsOpt;
typedef struct _TSRhsAttrs* _TSPRhsAttrs;
typedef struct _TSDepAttr* _TSPDepAttr;
typedef struct _TSDepClause* _TSPDepClause;
typedef struct _TSExpression* _TSPExpression;
typedef struct _TSExpandOpt* _TSPExpandOpt;
typedef struct _TSSubtree* _TSPSubtree;
typedef struct _TSRemoteExpression* _TSPRemoteExpression;
typedef struct _TSRemoteClause* _TSPRemoteClause;
typedef struct _TSShieldClause* _TSPShieldClause;
typedef struct _TSShield* _TSPShield;
typedef struct _TSComputation* _TSPComputation;
typedef struct _TSAttrComp* _TSPAttrComp;
typedef struct _TSShieldSym* _TSPShieldSym;
typedef struct _TSDefAttr* _TSPDefAttr;
typedef struct _TSCompute* _TSPCompute;
typedef struct _TSPlainComp* _TSPPlainComp;
typedef struct _TSChainNames* _TSPChainNames;
typedef struct _TSChainSpec* _TSPChainSpec;
typedef struct _TSAttrNames* _TSPAttrNames;
typedef struct _TSAttrSpec* _TSPAttrSpec;
typedef struct _TSAttrName* _TSPAttrName;
typedef struct _TSAttrDefIds* _TSPAttrDefIds;
typedef struct _TSClass* _TSPClass;
typedef struct _TSAttrDef* _TSPAttrDef;
typedef struct _TSExpressionDep* _TSPExpressionDep;
typedef struct _TSLoop* _TSPLoop;
typedef struct _TSAttrDefId* _TSPAttrDefId;
typedef struct _TSChainName* _TSPChainName;
typedef struct _TSSymOcc* _TSPSymOcc;
typedef struct _TSAttr* _TSPAttr;
typedef struct _TSAttrUseId* _TSPAttrUseId;
typedef struct _TSRemoteAttr* _TSPRemoteAttr;
typedef struct _TSAlt* _TSPAlt;
typedef struct _TSInheritSym* _TSPInheritSym;
typedef struct _TSAlts* _TSPAlts;
typedef struct _TSSyntUnits* _TSPSyntUnits;
typedef struct _TSRuleSpecId* _TSPRuleSpecId;
typedef struct _TSRuleSpec* _TSPRuleSpec;
typedef struct _TSSyntUnit* _TSPSyntUnit;
typedef struct _TSSyntLit* _TSPSyntLit;
typedef struct _TSProduction* _TSPProduction;
typedef struct _TSCompPart* _TSPCompPart;
typedef struct _TSInheritOpt* _TSPInheritOpt;
typedef struct _TSAttrDefsOpt* _TSPAttrDefsOpt;
typedef struct _TSSymbolDefIds* _TSPSymbolDefIds;
typedef struct _TSSymClass* _TSPSymClass;
typedef struct _TSTermSpec* _TSPTermSpec;
typedef struct _TSSymCompSpec* _TSPSymCompSpec;
typedef struct _TSSymAttrSpec* _TSPSymAttrSpec;
typedef struct _TSIndex* _TSPIndex;
typedef struct _TSSymbolRef* _TSPSymbolRef;
typedef struct _TSAG* _TSPAG;
typedef struct _TSRuleId* _TSPRuleId;
typedef struct _TSTypeId* _TSPTypeId;
typedef struct _TSSyntId* _TSPSyntId;
typedef struct _TSSymbolId* _TSPSymbolId;
typedef struct _TSSymbolDefId* _TSPSymbolDefId;
typedef struct _TPrule_1* _TPPrule_1;
typedef struct _TPrule_2* _TPPrule_2;
typedef struct _TPrule_3* _TPPrule_3;
typedef struct _TPrule_4* _TPPrule_4;
typedef struct _TPrule_5* _TPPrule_5;
typedef struct _TPrule_6* _TPPrule_6;
typedef struct _TPrule_7* _TPPrule_7;
typedef struct _TPrule_8* _TPPrule_8;
typedef struct _TPrule_9* _TPPrule_9;
typedef struct _TPrule_10* _TPPrule_10;
typedef struct _TPrule_11* _TPPrule_11;
typedef struct _TPrule_12* _TPPrule_12;
typedef struct _TPrule_13* _TPPrule_13;
typedef struct _TPrule_14* _TPPrule_14;
typedef struct _TPrule_15* _TPPrule_15;
typedef struct _TPrule_16* _TPPrule_16;
typedef struct _TPrule_17* _TPPrule_17;
typedef struct _TPrule_18* _TPPrule_18;
typedef struct _TPrule_19* _TPPrule_19;
typedef struct _TPrule_20* _TPPrule_20;
typedef struct _TPrule_21* _TPPrule_21;
typedef struct _TPrule_22* _TPPrule_22;
typedef struct _TPrule_23* _TPPrule_23;
typedef struct _TPrule_24* _TPPrule_24;
typedef struct _TPrule_25* _TPPrule_25;
typedef struct _TPrule_26* _TPPrule_26;
typedef struct _TPrule_27* _TPPrule_27;
typedef struct _TPrule_28* _TPPrule_28;
typedef struct _TPrule_29* _TPPrule_29;
typedef struct _TPrule_30* _TPPrule_30;
typedef struct _TPrule_31* _TPPrule_31;
typedef struct _TPrule_32* _TPPrule_32;
typedef struct _TPrule_33* _TPPrule_33;
typedef struct _TPrule_34* _TPPrule_34;
typedef struct _TPrule_35* _TPPrule_35;
typedef struct _TPrule_36* _TPPrule_36;
typedef struct _TPrule_37* _TPPrule_37;
typedef struct _TPrule_38* _TPPrule_38;
typedef struct _TPrule_39* _TPPrule_39;
typedef struct _TPrule_40* _TPPrule_40;
typedef struct _TPrule_41* _TPPrule_41;
typedef struct _TPrule_42* _TPPrule_42;
typedef struct _TPrule_43* _TPPrule_43;
typedef struct _TPrule_44* _TPPrule_44;
typedef struct _TPrule_45* _TPPrule_45;
typedef struct _TPrule_46* _TPPrule_46;
typedef struct _TPrule_47* _TPPrule_47;
typedef struct _TPrule_48* _TPPrule_48;
typedef struct _TPrule_49* _TPPrule_49;
typedef struct _TPrule_50* _TPPrule_50;
typedef struct _TPrule_51* _TPPrule_51;
typedef struct _TPrule_52* _TPPrule_52;
typedef struct _TPrule_53* _TPPrule_53;
typedef struct _TPrule_54* _TPPrule_54;
typedef struct _TPrule_55* _TPPrule_55;
typedef struct _TPrule_56* _TPPrule_56;
typedef struct _TPrule_57* _TPPrule_57;
typedef struct _TPrule_58* _TPPrule_58;
typedef struct _TPrule_59* _TPPrule_59;
typedef struct _TPrule_60* _TPPrule_60;
typedef struct _TPrule_61* _TPPrule_61;
typedef struct _TPrule_62* _TPPrule_62;
typedef struct _TPrule_63* _TPPrule_63;
typedef struct _TPrule_075* _TPPrule_075;
typedef struct _TPrule_074* _TPPrule_074;
typedef struct _TPrule_073* _TPPrule_073;
typedef struct _TPrule_072* _TPPrule_072;
typedef struct _TPrule_071* _TPPrule_071;
typedef struct _TPrule_070* _TPPrule_070;
typedef struct _TPrule_069* _TPPrule_069;
typedef struct _TPrule_068* _TPPrule_068;
typedef struct _TPrule_067* _TPPrule_067;
typedef struct _TPrule_066* _TPPrule_066;
typedef struct _TPrule_065* _TPPrule_065;
typedef struct _TPrule_064* _TPPrule_064;
typedef struct _TPrule_063* _TPPrule_063;
typedef struct _TPrule_062* _TPPrule_062;
typedef struct _TPrule_061* _TPPrule_061;
typedef struct _TPrule_060* _TPPrule_060;
typedef struct _TPrule_059* _TPPrule_059;
typedef struct _TPrule_058* _TPPrule_058;
typedef struct _TPrule_057* _TPPrule_057;
typedef struct _TPrule_056* _TPPrule_056;
typedef struct _TPrule_055* _TPPrule_055;
typedef struct _TPrule_054* _TPPrule_054;
typedef struct _TPrule_053* _TPPrule_053;
typedef struct _TPrule_052* _TPPrule_052;
typedef struct _TPrule_051* _TPPrule_051;
typedef struct _TPrule_050* _TPPrule_050;
typedef struct _TPrule_049* _TPPrule_049;
typedef struct _TPrule_048* _TPPrule_048;
typedef struct _TPrule_047* _TPPrule_047;
typedef struct _TPrule_046* _TPPrule_046;
typedef struct _TPrule_045* _TPPrule_045;
typedef struct _TPrule_044* _TPPrule_044;
typedef struct _TPrule_043* _TPPrule_043;
typedef struct _TPrule_042* _TPPrule_042;
typedef struct _TPrule_041* _TPPrule_041;
typedef struct _TPrule_040* _TPPrule_040;
typedef struct _TPrule_039* _TPPrule_039;
typedef struct _TPrule_038* _TPPrule_038;
typedef struct _TPrule_037* _TPPrule_037;
typedef struct _TPrule_036* _TPPrule_036;
typedef struct _TPrule_035* _TPPrule_035;
typedef struct _TPrule_034* _TPPrule_034;
typedef struct _TPrule_033* _TPPrule_033;
typedef struct _TPrule_032* _TPPrule_032;
typedef struct _TPrule_031* _TPPrule_031;
typedef struct _TPrule_030* _TPPrule_030;
typedef struct _TPrule_029* _TPPrule_029;
typedef struct _TPrule_028* _TPPrule_028;
typedef struct _TPrule_027* _TPPrule_027;
typedef struct _TPrule_026* _TPPrule_026;
typedef struct _TPrule_025* _TPPrule_025;
typedef struct _TPrule_024* _TPPrule_024;
typedef struct _TPrule_023* _TPPrule_023;
typedef struct _TPrule_022* _TPPrule_022;
typedef struct _TPrule_021* _TPPrule_021;
typedef struct _TPrule_020* _TPPrule_020;
typedef struct _TPrule_019* _TPPrule_019;
typedef struct _TPrule_018* _TPPrule_018;
typedef struct _TPrule_017* _TPPrule_017;
typedef struct _TPrule_016* _TPPrule_016;
typedef struct _TPrule_015* _TPPrule_015;
typedef struct _TPrule_014* _TPPrule_014;
typedef struct _TPrule_013* _TPPrule_013;
typedef struct _TPrule_012* _TPPrule_012;
typedef struct _TPrule_011* _TPPrule_011;
typedef struct _TPrule_010* _TPPrule_010;
typedef struct _TPrule_09* _TPPrule_09;
typedef struct _TPrule_08* _TPPrule_08;
typedef struct _TPrule_07* _TPPrule_07;
typedef struct _TPrule_06* _TPPrule_06;
typedef struct _TPrule_05* _TPPrule_05;
typedef struct _TPrule_04* _TPPrule_04;
typedef struct _TPrule_03* _TPPrule_03;
typedef struct _TPrule_02* _TPPrule_02;
typedef struct _TPrule_01* _TPPrule_01;

struct _TSSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSShieldSyms
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRemoteAttrs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSParams
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSInheritSyms
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSG1
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSDependence
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSDepAttrs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSComputations
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrDefs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSpecs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAsgnTok
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATisAccu;
};

struct _TSParam
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSParamsOpt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRhsAttrs
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
PExpr _ATrepr;
};

struct _TSDepAttr
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSDepClause
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSExpression
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSExpandOpt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSubtree
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRemoteExpression
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATRemoteKey;
};

struct _TSRemoteClause
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSShieldClause
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSShield
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSComputation
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrComp
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _AT_const4;
int _ATisAccu;
};

struct _TSShieldSym
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSDefAttr
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATAttrKey;
Binding _ATBind;
};

struct _TSCompute
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATisAccu;
int _ATIsBottomUp;
DefTableKey _ATKey;
Binding _ATBind;
int _ATIsUpperSymbComp;
};

struct _TSPlainComp
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATBUAssignAttr;
Binding _ATBind;
};

struct _TSChainNames
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSChainSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATType;
};

struct _TSAttrNames
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATAttrClass;
DefTableKey _ATType;
};

struct _TSAttrName
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATKey;
int _ATSym;
};

struct _TSAttrDefIds
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSClass
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATAttrClass;
};

struct _TSAttrDef
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATAttrClass;
DefTableKey _ATAttrType;
};

struct _TSExpressionDep
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSLoop
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrDefId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSChainName
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TSSymOcc
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
ProdSymbol _ATProdSymbol;
DefTableKey _ATKey;
};

struct _TSAttr
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _AT_const7;
DefTableKey _AT_const8;
int _ATIsHEADAcc;
ProdSymbol _ATProdSymbol;
int _ATIsDefining;
};

struct _TSAttrUseId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TSRemoteAttr
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAlt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSInheritSym
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAlts
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSyntUnits
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRuleSpecId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSRuleSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsRule;
DefTableKey _ATKey;
RuleProd _ATRuleInstance;
};

struct _TSSyntUnit
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSyntLit
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSProduction
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSCompPart
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSInheritOpt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAttrDefsOpt
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSSymbolDefIds
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKeyList _ATDefTableKeyList;
};

struct _TSSymClass
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TSTermSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATTypeKey;
int _ATIsTerm;
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TSSymCompSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
Environment _ATAttrScope;
Environment _ATHEADScope;
Environment _ATLowerScope;
Environment _ATUpperScope;
DefTableKey _ATKey;
int _ATIsRule;
DefTableKey _ATTypeKey;
int _ATIsTerm;
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TSSymAttrSpec
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKeyList _ATSymbolKeyList;
DefTableKey _ATTypeKey;
int _ATIsTerm;
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TSIndex
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATValue;
};

struct _TSSymbolRef
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TSAG
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
Environment _ATSymbolEnv;
Environment _ATAttrNameEnv;
Environment _ATChainScope;
Environment _ATAttrEnv;
Environment _ATEnv;
};

struct _TSRuleId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TSTypeId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TSSyntId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TSSymbolId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
Binding _ATBind;
int _ATSym;
DefTableKey _ATKey;
};

struct _TSSymbolDefId
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
};

struct _TPrule_1
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATisAccu;
};

struct _TPrule_2
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATisAccu;
};

struct _TPrule_3
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _AT_const4;
int _ATisAccu;
_TSPDefAttr _desc1;
_TSPAsgnTok _desc2;
_TSPExpressionDep _desc3;
POSITION _AT_pos;
};

struct _TPrule_4
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
PExpr _ATrepr;
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_5
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_6
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_7
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_8
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_9
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolRef _desc1;
};

struct _TPrule_10
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteExpression _desc1;
};

struct _TPrule_11
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRhsAttrs _desc1;
};

struct _TPrule_12
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttr _desc1;
};

struct _TPrule_13
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPParamsOpt _desc1;
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_14
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPExpression _desc1;
_TSPDepClause _desc2;
POSITION _AT_pos;
};

struct _TPrule_15
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPExpression _desc1;
};

struct _TPrule_16
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_17
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPTypeId _desc1;
POSITION _AT_pos;
int _ATTERM_3;
int _ATTERM_2;
int _ATTERM_1;
};

struct _TPrule_18
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymOcc _desc1;
};

struct _TPrule_19
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATRemoteKey;
_TSPSubtree _desc1;
_TSPRemoteClause _desc2;
_TSPShield _desc3;
POSITION _AT_pos;
};

struct _TPrule_20
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATRemoteKey;
_TSPSubtree _desc1;
_TSPRemoteClause _desc2;
_TSPShield _desc3;
_TSPExpandOpt _desc4;
POSITION _AT_pos;
};

struct _TPrule_21
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATRemoteKey;
_TSPRemoteClause _desc1;
POSITION _AT_pos;
};

struct _TPrule_22
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldClause _desc1;
};

struct _TPrule_23
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_24
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPCompute _desc1;
};

struct _TPrule_25
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATisAccu;
int _ATIsBottomUp;
DefTableKey _ATKey;
Binding _ATBind;
int _ATIsUpperSymbComp;
_TSPPlainComp _desc1;
};

struct _TPrule_26
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _AT_const4;
int _ATisAccu;
_TSPDefAttr _desc1;
_TSPExpressionDep _desc2;
POSITION _AT_pos;
};

struct _TPrule_27
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATisAccu;
int _ATIsBottomUp;
DefTableKey _ATKey;
Binding _ATBind;
int _ATIsUpperSymbComp;
_TSPAttrComp _desc1;
};

struct _TPrule_28
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolId _desc1;
POSITION _AT_pos;
};

struct _TPrule_29
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATValue;
};

struct _TPrule_30
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATValue;
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_31
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATAttrKey;
Binding _ATBind;
_TSPAttr _desc1;
POSITION _AT_pos;
};

struct _TPrule_32
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPIndex _desc1;
POSITION _AT_pos;
};

struct _TPrule_33
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
ProdSymbol _ATProdSymbol;
DefTableKey _ATKey;
_TSPSymbolId _desc1;
_TSPIndex _desc2;
POSITION _AT_pos;
};

struct _TPrule_34
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
ProdSymbol _ATProdSymbol;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_35
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
ProdSymbol _ATProdSymbol;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_36
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
ProdSymbol _ATProdSymbol;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_37
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
ProdSymbol _ATProdSymbol;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_38
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATType;
_TSPChainNames _desc1;
_TSPTypeId _desc2;
POSITION _AT_pos;
};

struct _TPrule_39
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATAttrClass;
DefTableKey _ATType;
_TSPAttrNames _desc1;
_TSPTypeId _desc2;
_TSPClass _desc3;
POSITION _AT_pos;
};

struct _TPrule_40
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATAttrClass;
};

struct _TPrule_41
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATAttrClass;
};

struct _TPrule_42
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATAttrClass;
};

struct _TPrule_43
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATAttrClass;
DefTableKey _ATAttrType;
_TSPAttrDefIds _desc1;
_TSPTypeId _desc2;
_TSPClass _desc3;
POSITION _AT_pos;
};

struct _TPrule_44
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPExpressionDep _desc1;
_TSPAttr _desc2;
_TSPExpressionDep _desc3;
POSITION _AT_pos;
};

struct _TPrule_45
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_46
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _AT_const7;
DefTableKey _AT_const8;
int _ATIsHEADAcc;
ProdSymbol _ATProdSymbol;
int _ATIsDefining;
_TSPAttrUseId _desc1;
POSITION _AT_pos;
};

struct _TPrule_47
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _AT_const7;
DefTableKey _AT_const8;
int _ATIsHEADAcc;
ProdSymbol _ATProdSymbol;
int _ATIsDefining;
_TSPSymOcc _desc1;
_TSPAttrUseId _desc2;
POSITION _AT_pos;
};

struct _TPrule_48
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolId _desc1;
_TSPAttrUseId _desc2;
POSITION _AT_pos;
int _ATuniqueRemAttr_RuleAttr_152;
};

struct _TPrule_49
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
POSITION _AT_pos;
};

struct _TPrule_50
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolId _desc1;
POSITION _AT_pos;
};

struct _TPrule_51
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
_TSPAlts _desc2;
POSITION _AT_pos;
};

struct _TPrule_52
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
_TSPSyntUnits _desc2;
POSITION _AT_pos;
};

struct _TPrule_53
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_54
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRuleId _desc1;
POSITION _AT_pos;
};

struct _TPrule_55
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsRule;
DefTableKey _ATKey;
RuleProd _ATRuleInstance;
_TSPRuleSpecId _desc1;
_TSPProduction _desc2;
_TSPCompPart _desc3;
POSITION _AT_pos;
};

struct _TPrule_56
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
POSITION _AT_pos;
};

struct _TPrule_57
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_58
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATTypeKey;
int _ATIsTerm;
int _ATIsCLASSSym;
int _ATIsTREESym;
_TSPSymbolDefIds _desc1;
_TSPTypeId _desc2;
POSITION _AT_pos;
};

struct _TPrule_59
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
Environment _ATAttrScope;
Environment _ATHEADScope;
Environment _ATLowerScope;
Environment _ATUpperScope;
DefTableKey _ATKey;
int _ATIsRule;
DefTableKey _ATTypeKey;
int _ATIsTerm;
int _ATIsCLASSSym;
int _ATIsTREESym;
_TSPSymClass _desc1;
_TSPSymbolDefId _desc2;
_TSPInheritOpt _desc3;
_TSPCompPart _desc4;
POSITION _AT_pos;
};

struct _TPrule_60
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKeyList _ATSymbolKeyList;
DefTableKey _ATTypeKey;
int _ATIsTerm;
int _ATIsCLASSSym;
int _ATIsTREESym;
_TSPSymClass _desc1;
_TSPSymbolDefIds _desc2;
_TSPAttrDefsOpt _desc3;
};

struct _TPrule_61
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TPrule_62
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TPrule_63
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolId _desc1;
_TSPIndex _desc2;
POSITION _AT_pos;
};

struct _TPrule_075
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_074
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntLit _desc1;
};

struct _TPrule_073
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntId _desc1;
POSITION _AT_pos;
};

struct _TPrule_072
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSyntUnits _desc1;
_TSPSyntUnit _desc2;
};

struct _TPrule_071
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_070
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_069
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
ProdSymbol _ATProdSymbol;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_068
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATIsCLASSSym;
int _ATIsTREESym;
};

struct _TPrule_067
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
Binding _ATBind;
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_066
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKeyList _ATDefTableKeyList;
_TSPG1 _desc1;
};

struct _TPrule_065
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_064
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_063
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPTermSpec _desc1;
};

struct _TPrule_062
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymCompSpec _desc1;
};

struct _TPrule_061
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymAttrSpec _desc1;
};

struct _TPrule_060
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSpecs _desc1;
_TSPSpec _desc2;
};

struct _TPrule_059
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_058
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRuleSpec _desc1;
};

struct _TPrule_057
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPChainSpec _desc1;
};

struct _TPrule_056
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrSpec _desc1;
};

struct _TPrule_055
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldSyms _desc1;
_TSPShieldSym _desc2;
};

struct _TPrule_054
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldSym _desc1;
};

struct _TPrule_053
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldSyms _desc1;
};

struct _TPrule_052
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPShieldSym _desc1;
};

struct _TPrule_051
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_050
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_049
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteAttrs _desc1;
};

struct _TPrule_048
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteAttr _desc1;
};

struct _TPrule_047
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteAttr _desc1;
_TSPRemoteAttrs _desc2;
};

struct _TPrule_046
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteAttr _desc1;
};

struct _TPrule_045
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATBUAssignAttr;
Binding _ATBind;
_TSPLoop _desc1;
POSITION _AT_pos;
};

struct _TPrule_044
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATBUAssignAttr;
Binding _ATBind;
_TSPExpressionDep _desc1;
POSITION _AT_pos;
};

struct _TPrule_043
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPParam _desc1;
_TSPParams _desc2;
};

struct _TPrule_042
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPParam _desc1;
};

struct _TPrule_041
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPParams _desc1;
};

struct _TPrule_040
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_039
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPExpressionDep _desc1;
};

struct _TPrule_038
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPInheritSyms _desc1;
_TSPInheritSym _desc2;
};

struct _TPrule_037
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPInheritSym _desc1;
};

struct _TPrule_036
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPInheritSyms _desc1;
};

struct _TPrule_035
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_034
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPSymbolDefId _desc1;
POSITION _AT_pos;
};

struct _TPrule_033
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPG1 _desc1;
_TSPSymbolDefId _desc2;
POSITION _AT_pos;
};

struct _TPrule_032
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDepAttrs _desc1;
};

struct _TPrule_031
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDepAttr _desc1;
};

struct _TPrule_030
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDependence _desc1;
};

struct _TPrule_029
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDependence _desc1;
};

struct _TPrule_028
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDepAttr _desc1;
_TSPDepAttrs _desc2;
};

struct _TPrule_027
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPDepAttr _desc1;
};

struct _TPrule_026
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRhsAttrs _desc1;
};

struct _TPrule_025
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPRemoteExpression _desc1;
};

struct _TPrule_024
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttr _desc1;
};

struct _TPrule_023
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPComputation _desc1;
_TSPComputations _desc2;
};

struct _TPrule_022
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_021
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPCompute _desc1;
};

struct _TPrule_020
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPComputations _desc1;
};

struct _TPrule_019
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_018
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPChainNames _desc1;
_TSPChainName _desc2;
};

struct _TPrule_017
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPChainName _desc1;
};

struct _TPrule_016
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _ATSym;
DefTableKey _ATKey;
POSITION _AT_pos;
};

struct _TPrule_015
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrNames _desc1;
_TSPAttrName _desc2;
};

struct _TPrule_014
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrName _desc1;
};

struct _TPrule_013
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
DefTableKey _ATKey;
int _ATSym;
POSITION _AT_pos;
};

struct _TPrule_012
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDefs _desc1;
};

struct _TPrule_011
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_010
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDefs _desc1;
_TSPAttrDef _desc2;
};

struct _TPrule_09
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDef _desc1;
};

struct _TPrule_08
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDefIds _desc1;
_TSPAttrDefId _desc2;
};

struct _TPrule_07
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAttrDefId _desc1;
};

struct _TPrule_06
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
POSITION _AT_pos;
int _ATTERM_1;
};

struct _TPrule_05
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
int _AT_const4;
int _ATisAccu;
_TSPDefAttr _desc1;
_TSPLoop _desc2;
POSITION _AT_pos;
};

struct _TPrule_04
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAlts _desc1;
_TSPAlt _desc2;
};

struct _TPrule_03
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
_TSPAlt _desc1;
};

struct _TPrule_02
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
};

struct _TPrule_01
#ifdef __cplusplus
	: public NODEPTR_struct {
#else
{
_NODECOMMON
#endif
Environment _ATSymbolEnv;
Environment _ATAttrNameEnv;
Environment _ATChainScope;
Environment _ATAttrEnv;
Environment _ATEnv;
_TSPSpecs _desc1;
};

#undef _NODECOMMON
#endif
