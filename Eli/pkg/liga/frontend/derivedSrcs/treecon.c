
/* implementation of tree construction functions */

#include "node.h"

#include "nodecode.h"

#include "attrpredef.h"

#include "visitmap.h"

#include "treeact.h"

#ifdef MONITOR
#include "attr_mon_dapto.h"
#include "MONTblStack.h"
#endif

#include <stdlib.h>

#define _USE_OBSTACK 1

/* use of obstack: */

#if _USE_OBSTACK

#include "obstack.h"
static struct obstack TreeSpace;
static void *_TreeBase;

#ifdef __cplusplus
void* NODEPTR_struct::operator new(size_t size)
{
	return obstack_alloc(&TreeSpace, size);
}
#else
#if defined(__STDC__) || defined(__cplusplus)
char* TreeNodeAlloc(int size)
#else
char* TreeNodeAlloc(size) int size;
#endif
{
	return (char *)(obstack_alloc(&TreeSpace, size));
}
#endif

void InitTree()
{
	obstack_init(&TreeSpace);
	_TreeBase=obstack_alloc(&TreeSpace,0);
}

void FreeTree()
{
	obstack_free(&TreeSpace, _TreeBase);
	_TreeBase=obstack_alloc(&TreeSpace,0);
}

#else

#include <stdio.h>

#ifdef __cplusplus
void* NODEPTR_struct::operator new(size_t size)
{
	void *retval = malloc(size);
	if (retval) return retval;
	fprintf(stderr, "*** DEADLY: No more memory.\n");
	exit(1);
}
#else
#if defined(__STDC__) || defined(__cplusplus)
char* TreeNodeAlloc(int size)
#else
char* TreeNodeAlloc(size) int size;
#endif
{
	char *retval = (char *) malloc(size);
	if (retval) return retval;
	fprintf(stderr, "*** DEADLY: No more memory.\n");
	exit(1);
}
#endif

void InitTree() { }

void FreeTree() { }

#endif

#ifdef MONITOR
#define _SETCOORD(node) \
        node->_coord = _coordref ? *_coordref : NoCoord;
#define _COPYCOORD(node) \
        node->_coord = _currn->_desc1->_coord;
#else
#define _SETCOORD(node)
#define _COPYCOORD(node)
#endif
#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSpec)) return (_currn);
if (IsSymb (_currn, SYMBAttrSpec)) return (Mkrule_056(_coordref, _currn));
if (IsSymb (_currn, SYMBChainSpec)) return (Mkrule_057(_coordref, _currn));
if (IsSymb (_currn, SYMBRuleSpec)) return (Mkrule_058(_coordref, _currn));
if (IsSymb (_currn, SYMBSymAttrSpec)) return (Mkrule_061(_coordref, _currn));
if (IsSymb (_currn, SYMBSymCompSpec)) return (Mkrule_062(_coordref, _currn));
if (IsSymb (_currn, SYMBTermSpec)) return (Mkrule_063(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkShieldSyms (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkShieldSyms (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBShieldSyms)) return (_currn);
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_054(_coordref, _currn));
if (IsSymb (_currn, SYMBShieldSym)) return (Mkrule_054(_coordref, _currn));
return(NULLNODEPTR);
}/* MkShieldSyms */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemoteAttrs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemoteAttrs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemoteAttrs)) return (_currn);
if (IsSymb (_currn, SYMBRemoteAttr)) return (Mkrule_046(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemoteAttrs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkParams (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkParams (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBParams)) return (_currn);
if (IsSymb (_currn, SYMBParam)) return (Mkrule_042(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_042(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_042(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_042(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_042(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_042(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_042(_coordref, _currn));
return(NULLNODEPTR);
}/* MkParams */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkInheritSyms (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkInheritSyms (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBInheritSyms)) return (_currn);
if (IsSymb (_currn, SYMBInheritSym)) return (Mkrule_037(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_037(_coordref, _currn));
return(NULLNODEPTR);
}/* MkInheritSyms */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkG1 (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkG1 (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBG1)) return (_currn);
if (IsSymb (_currn, SYMBSymbolDefId)) return (Mkrule_034(_coordref, _currn));
return(NULLNODEPTR);
}/* MkG1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDependence (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDependence (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDependence)) return (_currn);
if (IsSymb (_currn, SYMBDepAttr)) return (Mkrule_031(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_031(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_031(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_031(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDependence */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDepAttrs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDepAttrs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDepAttrs)) return (_currn);
if (IsSymb (_currn, SYMBDepAttr)) return (Mkrule_027(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_027(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_027(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_027(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDepAttrs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkComputations (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkComputations (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBComputations)) return (_currn);
return(NULLNODEPTR);
}/* MkComputations */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDefs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDefs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDefs)) return (_currn);
if (IsSymb (_currn, SYMBAttrDef)) return (Mkrule_09(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAttrDefs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSpecs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSpecs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSpecs)) return (_currn);
return(NULLNODEPTR);
}/* MkSpecs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAsgnTok (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAsgnTok (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAsgnTok)) return (_currn);
return(NULLNODEPTR);
}/* MkAsgnTok */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkParam (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkParam (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBParam)) return (_currn);
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_039(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_039(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_039(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_039(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_039(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_039(_coordref, _currn));
return(NULLNODEPTR);
}/* MkParam */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkParamsOpt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkParamsOpt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBParamsOpt)) return (_currn);
if (IsSymb (_currn, SYMBParam)) return (Mkrule_041(_coordref, _currn));
if (IsSymb (_currn, SYMBParams)) return (Mkrule_041(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_041(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_041(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_041(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_041(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_041(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_041(_coordref, _currn));
return(NULLNODEPTR);
}/* MkParamsOpt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRhsAttrs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRhsAttrs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRhsAttrs)) return (_currn);
return(NULLNODEPTR);
}/* MkRhsAttrs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDepAttr (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDepAttr (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDepAttr)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_024(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_026(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_025(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDepAttr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDepClause (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDepClause (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDepClause)) return (_currn);
return(NULLNODEPTR);
}/* MkDepClause */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBExpression)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_12(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_11(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_10(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_9(_coordref, _currn));
return(NULLNODEPTR);
}/* MkExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkExpandOpt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkExpandOpt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBExpandOpt)) return (_currn);
return(NULLNODEPTR);
}/* MkExpandOpt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSubtree (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSubtree (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSubtree)) return (_currn);
if (IsSymb (_currn, SYMBSymOcc)) return (Mkrule_18(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSubtree */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemoteExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemoteExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemoteExpression)) return (_currn);
return(NULLNODEPTR);
}/* MkRemoteExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemoteClause (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemoteClause (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemoteClause)) return (_currn);
if (IsSymb (_currn, SYMBRemoteAttr)) return (Mkrule_048(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemoteClause */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkShieldClause (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkShieldClause (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBShieldClause)) return (_currn);
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_052(_coordref, _currn));
if (IsSymb (_currn, SYMBShieldSym)) return (Mkrule_052(_coordref, _currn));
return(NULLNODEPTR);
}/* MkShieldClause */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkShield (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkShield (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBShield)) return (_currn);
return(NULLNODEPTR);
}/* MkShield */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkComputation (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkComputation (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBComputation)) return (_currn);
if (IsSymb (_currn, SYMBLoop)) return (Mkrule_021(_coordref, _currn));
if (IsSymb (_currn, SYMBAttrComp)) return (Mkrule_021(_coordref, _currn));
if (IsSymb (_currn, SYMBPlainComp)) return (Mkrule_021(_coordref, _currn));
if (IsSymb (_currn, SYMBCompute)) return (Mkrule_021(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_021(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_021(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_021(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_021(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_021(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_021(_coordref, _currn));
return(NULLNODEPTR);
}/* MkComputation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrComp (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrComp (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrComp)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrComp */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkShieldSym (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkShieldSym (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBShieldSym)) return (_currn);
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_28(_coordref, _currn));
return(NULLNODEPTR);
}/* MkShieldSym */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDefAttr (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDefAttr (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDefAttr)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_31(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDefAttr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkCompute (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkCompute (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBCompute)) return (_currn);
if (IsSymb (_currn, SYMBLoop)) return (Mkrule_25(_coordref, _currn));
if (IsSymb (_currn, SYMBAttrComp)) return (Mkrule_27(_coordref, _currn));
if (IsSymb (_currn, SYMBPlainComp)) return (Mkrule_25(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_25(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_25(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_25(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_25(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_25(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_25(_coordref, _currn));
return(NULLNODEPTR);
}/* MkCompute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPlainComp (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPlainComp (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPlainComp)) return (_currn);
if (IsSymb (_currn, SYMBLoop)) return (Mkrule_045(_coordref, _currn));
if (IsSymb (_currn, SYMBExpressionDep)) return (Mkrule_044(_coordref, _currn));
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_044(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_044(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_044(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_044(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_044(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPlainComp */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkChainNames (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkChainNames (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBChainNames)) return (_currn);
if (IsSymb (_currn, SYMBChainName)) return (Mkrule_017(_coordref, _currn));
return(NULLNODEPTR);
}/* MkChainNames */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkChainSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkChainSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBChainSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkChainSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrNames (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrNames (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrNames)) return (_currn);
if (IsSymb (_currn, SYMBAttrName)) return (Mkrule_014(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAttrNames */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrName (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrName (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrName)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrName */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDefIds (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDefIds (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDefIds)) return (_currn);
if (IsSymb (_currn, SYMBAttrDefId)) return (Mkrule_07(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAttrDefIds */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBClass)) return (_currn);
return(NULLNODEPTR);
}/* MkClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDef (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDef (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDef)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkExpressionDep (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkExpressionDep (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBExpressionDep)) return (_currn);
if (IsSymb (_currn, SYMBAttr)) return (Mkrule_15(_coordref, _currn));
if (IsSymb (_currn, SYMBRhsAttrs)) return (Mkrule_15(_coordref, _currn));
if (IsSymb (_currn, SYMBRemoteExpression)) return (Mkrule_15(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbolRef)) return (Mkrule_15(_coordref, _currn));
if (IsSymb (_currn, SYMBExpression)) return (Mkrule_15(_coordref, _currn));
return(NULLNODEPTR);
}/* MkExpressionDep */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkLoop (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkLoop (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBLoop)) return (_currn);
return(NULLNODEPTR);
}/* MkLoop */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDefId)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkChainName (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkChainName (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBChainName)) return (_currn);
return(NULLNODEPTR);
}/* MkChainName */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymOcc (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymOcc (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymOcc)) return (_currn);
return(NULLNODEPTR);
}/* MkSymOcc */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttr (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttr (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttr)) return (_currn);
return(NULLNODEPTR);
}/* MkAttr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrUseId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrUseId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrUseId)) return (_currn);
return(NULLNODEPTR);
}/* MkAttrUseId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemoteAttr (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemoteAttr (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemoteAttr)) return (_currn);
return(NULLNODEPTR);
}/* MkRemoteAttr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAlt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAlt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAlt)) return (_currn);
if (IsSymb (_currn, SYMBSyntId)) return (Mkrule_49(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAlt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkInheritSym (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkInheritSym (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBInheritSym)) return (_currn);
if (IsSymb (_currn, SYMBSymbolId)) return (Mkrule_50(_coordref, _currn));
return(NULLNODEPTR);
}/* MkInheritSym */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAlts (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAlts (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAlts)) return (_currn);
if (IsSymb (_currn, SYMBSyntId)) return (Mkrule_03(_coordref, _currn));
if (IsSymb (_currn, SYMBAlt)) return (Mkrule_03(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAlts */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSyntUnits (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSyntUnits (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSyntUnits)) return (_currn);
return(NULLNODEPTR);
}/* MkSyntUnits */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRuleSpecId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRuleSpecId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRuleSpecId)) return (_currn);
if (IsSymb (_currn, SYMBRuleId)) return (Mkrule_54(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRuleSpecId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRuleSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRuleSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRuleSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkRuleSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSyntUnit (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSyntUnit (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSyntUnit)) return (_currn);
if (IsSymb (_currn, SYMBSyntLit)) return (Mkrule_074(_coordref, _currn));
if (IsSymb (_currn, SYMBSyntId)) return (Mkrule_073(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSyntUnit */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSyntLit (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSyntLit (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSyntLit)) return (_currn);
return(NULLNODEPTR);
}/* MkSyntLit */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkProduction (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkProduction (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBProduction)) return (_currn);
return(NULLNODEPTR);
}/* MkProduction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkCompPart (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkCompPart (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBCompPart)) return (_currn);
return(NULLNODEPTR);
}/* MkCompPart */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkInheritOpt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkInheritOpt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBInheritOpt)) return (_currn);
return(NULLNODEPTR);
}/* MkInheritOpt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttrDefsOpt (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttrDefsOpt (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttrDefsOpt)) return (_currn);
if (IsSymb (_currn, SYMBAttrDef)) return (Mkrule_012(_coordref, _currn));
if (IsSymb (_currn, SYMBAttrDefs)) return (Mkrule_012(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAttrDefsOpt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbolDefIds (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbolDefIds (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbolDefIds)) return (_currn);
if (IsSymb (_currn, SYMBSymbolDefId)) return (Mkrule_066(_coordref, _currn));
if (IsSymb (_currn, SYMBG1)) return (Mkrule_066(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbolDefIds */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymClass)) return (_currn);
return(NULLNODEPTR);
}/* MkSymClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkTermSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkTermSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBTermSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkTermSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymCompSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymCompSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymCompSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkSymCompSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymAttrSpec (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymAttrSpec (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymAttrSpec)) return (_currn);
return(NULLNODEPTR);
}/* MkSymAttrSpec */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkIndex (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkIndex (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBIndex)) return (_currn);
return(NULLNODEPTR);
}/* MkIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbolRef (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbolRef (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbolRef)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbolRef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAG (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAG (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAG)) return (_currn);
if (IsSymb (_currn, SYMBSpecs)) return (Mkrule_01(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAG */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRuleId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRuleId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRuleId)) return (_currn);
return(NULLNODEPTR);
}/* MkRuleId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkTypeId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkTypeId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBTypeId)) return (_currn);
return(NULLNODEPTR);
}/* MkTypeId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSyntId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSyntId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSyntId)) return (_currn);
return(NULLNODEPTR);
}/* MkSyntId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbolId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbolId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbolId)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbolId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbolDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbolDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbolDefId)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbolDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_1 (POSITION *_coordref)
#else
NODEPTR Mkrule_1 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_1 _currn;
#ifdef __cplusplus
_currn = new _TPrule_1;
#else
_currn = (_TPPrule_1) TreeNodeAlloc (sizeof (struct _TPrule_1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_1;
_SETCOORD(_currn)
_TERMACT_rule_1;
return ( (NODEPTR) _currn);
}/* Mkrule_1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_2 (POSITION *_coordref)
#else
NODEPTR Mkrule_2 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_2 _currn;
#ifdef __cplusplus
_currn = new _TPrule_2;
#else
_currn = (_TPPrule_2) TreeNodeAlloc (sizeof (struct _TPrule_2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_2;
_SETCOORD(_currn)
_TERMACT_rule_2;
return ( (NODEPTR) _currn);
}/* Mkrule_2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_3 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_3 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_3 _currn;
#ifdef __cplusplus
_currn = new _TPrule_3;
#else
_currn = (_TPPrule_3) TreeNodeAlloc (sizeof (struct _TPrule_3));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_3;
_currn->_desc1 = (_TSPDefAttr) MkDefAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_3: root of subtree no. 1 can not be made a DefAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPAsgnTok) MkAsgnTok (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_3: root of subtree no. 2 can not be made a AsgnTok node ", 0, _coordref);
_currn->_desc3 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_3: root of subtree no. 3 can not be made a ExpressionDep node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_3;
return ( (NODEPTR) _currn);
}/* Mkrule_3 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_4 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_4 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_4 _currn;
#ifdef __cplusplus
_currn = new _TPrule_4;
#else
_currn = (_TPPrule_4) TreeNodeAlloc (sizeof (struct _TPrule_4));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_4;
_SETCOORD(_currn)
_TERMACT_rule_4;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_4 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_5 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_5 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_5 _currn;
#ifdef __cplusplus
_currn = new _TPrule_5;
#else
_currn = (_TPPrule_5) TreeNodeAlloc (sizeof (struct _TPrule_5));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_5;
_SETCOORD(_currn)
_TERMACT_rule_5;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "C_Integer", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_5 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_6 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_6 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_6 _currn;
#ifdef __cplusplus
_currn = new _TPrule_6;
#else
_currn = (_TPPrule_6) TreeNodeAlloc (sizeof (struct _TPrule_6));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_6;
_SETCOORD(_currn)
_TERMACT_rule_6;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "C_Float", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_6 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_7 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_7 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_7 _currn;
#ifdef __cplusplus
_currn = new _TPrule_7;
#else
_currn = (_TPPrule_7) TreeNodeAlloc (sizeof (struct _TPrule_7));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_7;
_SETCOORD(_currn)
_TERMACT_rule_7;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "C_String", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_7 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_8 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_8 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_8 _currn;
#ifdef __cplusplus
_currn = new _TPrule_8;
#else
_currn = (_TPPrule_8) TreeNodeAlloc (sizeof (struct _TPrule_8));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_8;
_SETCOORD(_currn)
_TERMACT_rule_8;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "P_String", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_8 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_9 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_9 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_9 _currn;
#ifdef __cplusplus
_currn = new _TPrule_9;
#else
_currn = (_TPPrule_9) TreeNodeAlloc (sizeof (struct _TPrule_9));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_9;
_currn->_desc1 = (_TSPSymbolRef) MkSymbolRef (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_9: root of subtree no. 1 can not be made a SymbolRef node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_9;
return ( (NODEPTR) _currn);
}/* Mkrule_9 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_10 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_10 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_10 _currn;
#ifdef __cplusplus
_currn = new _TPrule_10;
#else
_currn = (_TPPrule_10) TreeNodeAlloc (sizeof (struct _TPrule_10));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_10;
_currn->_desc1 = (_TSPRemoteExpression) MkRemoteExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_10: root of subtree no. 1 can not be made a RemoteExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_10;
return ( (NODEPTR) _currn);
}/* Mkrule_10 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_11 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_11 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_11 _currn;
#ifdef __cplusplus
_currn = new _TPrule_11;
#else
_currn = (_TPPrule_11) TreeNodeAlloc (sizeof (struct _TPrule_11));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_11;
_currn->_desc1 = (_TSPRhsAttrs) MkRhsAttrs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_11: root of subtree no. 1 can not be made a RhsAttrs node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_11;
return ( (NODEPTR) _currn);
}/* Mkrule_11 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_12 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_12 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_12 _currn;
#ifdef __cplusplus
_currn = new _TPrule_12;
#else
_currn = (_TPPrule_12) TreeNodeAlloc (sizeof (struct _TPrule_12));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_12;
_currn->_desc1 = (_TSPAttr) MkAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_12: root of subtree no. 1 can not be made a Attr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_12;
return ( (NODEPTR) _currn);
}/* Mkrule_12 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_13 (POSITION *_coordref, int _TERM1, NODEPTR _desc1)
#else
NODEPTR Mkrule_13 (_coordref, _TERM1,_desc1)
	POSITION *_coordref;
	int _TERM1;
	NODEPTR _desc1;
#endif
{	_TPPrule_13 _currn;
#ifdef __cplusplus
_currn = new _TPrule_13;
#else
_currn = (_TPPrule_13) TreeNodeAlloc (sizeof (struct _TPrule_13));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_13;
_currn->_desc1 = (_TSPParamsOpt) MkParamsOpt (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_13: root of subtree no. 1 can not be made a ParamsOpt node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_13;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_13 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_14 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_14 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_14 _currn;
#ifdef __cplusplus
_currn = new _TPrule_14;
#else
_currn = (_TPPrule_14) TreeNodeAlloc (sizeof (struct _TPrule_14));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_14;
_currn->_desc1 = (_TSPExpression) MkExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_14: root of subtree no. 1 can not be made a Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPDepClause) MkDepClause (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_14: root of subtree no. 2 can not be made a DepClause node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_14;
return ( (NODEPTR) _currn);
}/* Mkrule_14 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_15 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_15 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_15 _currn;
#ifdef __cplusplus
_currn = new _TPrule_15;
#else
_currn = (_TPPrule_15) TreeNodeAlloc (sizeof (struct _TPrule_15));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_15;
_currn->_desc1 = (_TSPExpression) MkExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_15: root of subtree no. 1 can not be made a Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_15;
return ( (NODEPTR) _currn);
}/* Mkrule_15 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_16 (POSITION *_coordref)
#else
NODEPTR Mkrule_16 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_16 _currn;
#ifdef __cplusplus
_currn = new _TPrule_16;
#else
_currn = (_TPPrule_16) TreeNodeAlloc (sizeof (struct _TPrule_16));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_16;
_SETCOORD(_currn)
_TERMACT_rule_16;
return ( (NODEPTR) _currn);
}/* Mkrule_16 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_17 (POSITION *_coordref, NODEPTR _desc1, int _TERM1, int _TERM2, int _TERM3)
#else
NODEPTR Mkrule_17 (_coordref,_desc1, _TERM1, _TERM2, _TERM3)
	POSITION *_coordref;
	NODEPTR _desc1;
	int _TERM1;
	int _TERM2;
	int _TERM3;
#endif
{	_TPPrule_17 _currn;
#ifdef __cplusplus
_currn = new _TPrule_17;
#else
_currn = (_TPPrule_17) TreeNodeAlloc (sizeof (struct _TPrule_17));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_17;
_currn->_desc1 = (_TSPTypeId) MkTypeId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_17: root of subtree no. 1 can not be made a TypeId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_17;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier[1]", _TERM1);
#endif

#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier[2]", _TERM2);
#endif

#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier[3]", _TERM3);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_17 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_18 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_18 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_18 _currn;
#ifdef __cplusplus
_currn = new _TPrule_18;
#else
_currn = (_TPPrule_18) TreeNodeAlloc (sizeof (struct _TPrule_18));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_18;
_currn->_desc1 = (_TSPSymOcc) MkSymOcc (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_18: root of subtree no. 1 can not be made a SymOcc node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_18;
return ( (NODEPTR) _currn);
}/* Mkrule_18 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_19 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_19 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_19 _currn;
#ifdef __cplusplus
_currn = new _TPrule_19;
#else
_currn = (_TPPrule_19) TreeNodeAlloc (sizeof (struct _TPrule_19));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_19;
_currn->_desc1 = (_TSPSubtree) MkSubtree (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_19: root of subtree no. 1 can not be made a Subtree node ", 0, _coordref);
_currn->_desc2 = (_TSPRemoteClause) MkRemoteClause (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_19: root of subtree no. 2 can not be made a RemoteClause node ", 0, _coordref);
_currn->_desc3 = (_TSPShield) MkShield (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_19: root of subtree no. 3 can not be made a Shield node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_19;
return ( (NODEPTR) _currn);
}/* Mkrule_19 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_20 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3, NODEPTR _desc4)
#else
NODEPTR Mkrule_20 (_coordref,_desc1,_desc2,_desc3,_desc4)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
	NODEPTR _desc4;
#endif
{	_TPPrule_20 _currn;
#ifdef __cplusplus
_currn = new _TPrule_20;
#else
_currn = (_TPPrule_20) TreeNodeAlloc (sizeof (struct _TPrule_20));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_20;
_currn->_desc1 = (_TSPSubtree) MkSubtree (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_20: root of subtree no. 1 can not be made a Subtree node ", 0, _coordref);
_currn->_desc2 = (_TSPRemoteClause) MkRemoteClause (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_20: root of subtree no. 2 can not be made a RemoteClause node ", 0, _coordref);
_currn->_desc3 = (_TSPShield) MkShield (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_20: root of subtree no. 3 can not be made a Shield node ", 0, _coordref);
_currn->_desc4 = (_TSPExpandOpt) MkExpandOpt (_coordref, _desc4);	
if (((NODEPTR)_currn->_desc4) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_20: root of subtree no. 4 can not be made a ExpandOpt node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_20;
return ( (NODEPTR) _currn);
}/* Mkrule_20 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_21 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_21 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_21 _currn;
#ifdef __cplusplus
_currn = new _TPrule_21;
#else
_currn = (_TPPrule_21) TreeNodeAlloc (sizeof (struct _TPrule_21));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_21;
_currn->_desc1 = (_TSPRemoteClause) MkRemoteClause (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_21: root of subtree no. 1 can not be made a RemoteClause node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_21;
return ( (NODEPTR) _currn);
}/* Mkrule_21 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_22 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_22 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_22 _currn;
#ifdef __cplusplus
_currn = new _TPrule_22;
#else
_currn = (_TPPrule_22) TreeNodeAlloc (sizeof (struct _TPrule_22));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_22;
_currn->_desc1 = (_TSPShieldClause) MkShieldClause (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_22: root of subtree no. 1 can not be made a ShieldClause node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_22;
return ( (NODEPTR) _currn);
}/* Mkrule_22 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_23 (POSITION *_coordref)
#else
NODEPTR Mkrule_23 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_23 _currn;
#ifdef __cplusplus
_currn = new _TPrule_23;
#else
_currn = (_TPPrule_23) TreeNodeAlloc (sizeof (struct _TPrule_23));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_23;
_SETCOORD(_currn)
_TERMACT_rule_23;
return ( (NODEPTR) _currn);
}/* Mkrule_23 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_24 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_24 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_24 _currn;
#ifdef __cplusplus
_currn = new _TPrule_24;
#else
_currn = (_TPPrule_24) TreeNodeAlloc (sizeof (struct _TPrule_24));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_24;
_currn->_desc1 = (_TSPCompute) MkCompute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_24: root of subtree no. 1 can not be made a Compute node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_24;
return ( (NODEPTR) _currn);
}/* Mkrule_24 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_25 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_25 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_25 _currn;
#ifdef __cplusplus
_currn = new _TPrule_25;
#else
_currn = (_TPPrule_25) TreeNodeAlloc (sizeof (struct _TPrule_25));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_25;
_currn->_desc1 = (_TSPPlainComp) MkPlainComp (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_25: root of subtree no. 1 can not be made a PlainComp node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_25;
return ( (NODEPTR) _currn);
}/* Mkrule_25 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_26 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_26 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_26 _currn;
#ifdef __cplusplus
_currn = new _TPrule_26;
#else
_currn = (_TPPrule_26) TreeNodeAlloc (sizeof (struct _TPrule_26));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_26;
_currn->_desc1 = (_TSPDefAttr) MkDefAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_26: root of subtree no. 1 can not be made a DefAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_26: root of subtree no. 2 can not be made a ExpressionDep node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_26;
return ( (NODEPTR) _currn);
}/* Mkrule_26 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_27 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_27 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_27 _currn;
#ifdef __cplusplus
_currn = new _TPrule_27;
#else
_currn = (_TPPrule_27) TreeNodeAlloc (sizeof (struct _TPrule_27));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_27;
_currn->_desc1 = (_TSPAttrComp) MkAttrComp (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_27: root of subtree no. 1 can not be made a AttrComp node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_27;
return ( (NODEPTR) _currn);
}/* Mkrule_27 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_28 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_28 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_28 _currn;
#ifdef __cplusplus
_currn = new _TPrule_28;
#else
_currn = (_TPPrule_28) TreeNodeAlloc (sizeof (struct _TPrule_28));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_28;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_28: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_28;
return ( (NODEPTR) _currn);
}/* Mkrule_28 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_29 (POSITION *_coordref)
#else
NODEPTR Mkrule_29 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_29 _currn;
#ifdef __cplusplus
_currn = new _TPrule_29;
#else
_currn = (_TPPrule_29) TreeNodeAlloc (sizeof (struct _TPrule_29));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_29;
_SETCOORD(_currn)
_TERMACT_rule_29;
return ( (NODEPTR) _currn);
}/* Mkrule_29 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_30 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_30 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_30 _currn;
#ifdef __cplusplus
_currn = new _TPrule_30;
#else
_currn = (_TPPrule_30) TreeNodeAlloc (sizeof (struct _TPrule_30));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_30;
_SETCOORD(_currn)
_TERMACT_rule_30;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "C_Integer", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_30 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_31 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_31 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_31 _currn;
#ifdef __cplusplus
_currn = new _TPrule_31;
#else
_currn = (_TPPrule_31) TreeNodeAlloc (sizeof (struct _TPrule_31));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_31;
_currn->_desc1 = (_TSPAttr) MkAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_31: root of subtree no. 1 can not be made a Attr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_31;
return ( (NODEPTR) _currn);
}/* Mkrule_31 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_32 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_32 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_32 _currn;
#ifdef __cplusplus
_currn = new _TPrule_32;
#else
_currn = (_TPPrule_32) TreeNodeAlloc (sizeof (struct _TPrule_32));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_32;
_currn->_desc1 = (_TSPIndex) MkIndex (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_32: root of subtree no. 1 can not be made a Index node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_32;
return ( (NODEPTR) _currn);
}/* Mkrule_32 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_33 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_33 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_33 _currn;
#ifdef __cplusplus
_currn = new _TPrule_33;
#else
_currn = (_TPPrule_33) TreeNodeAlloc (sizeof (struct _TPrule_33));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_33;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_33: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_currn->_desc2 = (_TSPIndex) MkIndex (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_33: root of subtree no. 2 can not be made a Index node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_33;
return ( (NODEPTR) _currn);
}/* Mkrule_33 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_34 (POSITION *_coordref)
#else
NODEPTR Mkrule_34 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_34 _currn;
#ifdef __cplusplus
_currn = new _TPrule_34;
#else
_currn = (_TPPrule_34) TreeNodeAlloc (sizeof (struct _TPrule_34));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_34;
_SETCOORD(_currn)
_TERMACT_rule_34;
return ( (NODEPTR) _currn);
}/* Mkrule_34 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_35 (POSITION *_coordref)
#else
NODEPTR Mkrule_35 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_35 _currn;
#ifdef __cplusplus
_currn = new _TPrule_35;
#else
_currn = (_TPPrule_35) TreeNodeAlloc (sizeof (struct _TPrule_35));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_35;
_SETCOORD(_currn)
_TERMACT_rule_35;
return ( (NODEPTR) _currn);
}/* Mkrule_35 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_36 (POSITION *_coordref)
#else
NODEPTR Mkrule_36 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_36 _currn;
#ifdef __cplusplus
_currn = new _TPrule_36;
#else
_currn = (_TPPrule_36) TreeNodeAlloc (sizeof (struct _TPrule_36));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_36;
_SETCOORD(_currn)
_TERMACT_rule_36;
return ( (NODEPTR) _currn);
}/* Mkrule_36 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_37 (POSITION *_coordref)
#else
NODEPTR Mkrule_37 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_37 _currn;
#ifdef __cplusplus
_currn = new _TPrule_37;
#else
_currn = (_TPPrule_37) TreeNodeAlloc (sizeof (struct _TPrule_37));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_37;
_SETCOORD(_currn)
_TERMACT_rule_37;
return ( (NODEPTR) _currn);
}/* Mkrule_37 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_38 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_38 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_38 _currn;
#ifdef __cplusplus
_currn = new _TPrule_38;
#else
_currn = (_TPPrule_38) TreeNodeAlloc (sizeof (struct _TPrule_38));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_38;
_currn->_desc1 = (_TSPChainNames) MkChainNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_38: root of subtree no. 1 can not be made a ChainNames node ", 0, _coordref);
_currn->_desc2 = (_TSPTypeId) MkTypeId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_38: root of subtree no. 2 can not be made a TypeId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_38;
return ( (NODEPTR) _currn);
}/* Mkrule_38 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_39 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_39 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_39 _currn;
#ifdef __cplusplus
_currn = new _TPrule_39;
#else
_currn = (_TPPrule_39) TreeNodeAlloc (sizeof (struct _TPrule_39));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_39;
_currn->_desc1 = (_TSPAttrNames) MkAttrNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_39: root of subtree no. 1 can not be made a AttrNames node ", 0, _coordref);
_currn->_desc2 = (_TSPTypeId) MkTypeId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_39: root of subtree no. 2 can not be made a TypeId node ", 0, _coordref);
_currn->_desc3 = (_TSPClass) MkClass (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_39: root of subtree no. 3 can not be made a Class node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_39;
return ( (NODEPTR) _currn);
}/* Mkrule_39 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_40 (POSITION *_coordref)
#else
NODEPTR Mkrule_40 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_40 _currn;
#ifdef __cplusplus
_currn = new _TPrule_40;
#else
_currn = (_TPPrule_40) TreeNodeAlloc (sizeof (struct _TPrule_40));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_40;
_SETCOORD(_currn)
_TERMACT_rule_40;
return ( (NODEPTR) _currn);
}/* Mkrule_40 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_41 (POSITION *_coordref)
#else
NODEPTR Mkrule_41 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_41 _currn;
#ifdef __cplusplus
_currn = new _TPrule_41;
#else
_currn = (_TPPrule_41) TreeNodeAlloc (sizeof (struct _TPrule_41));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_41;
_SETCOORD(_currn)
_TERMACT_rule_41;
return ( (NODEPTR) _currn);
}/* Mkrule_41 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_42 (POSITION *_coordref)
#else
NODEPTR Mkrule_42 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_42 _currn;
#ifdef __cplusplus
_currn = new _TPrule_42;
#else
_currn = (_TPPrule_42) TreeNodeAlloc (sizeof (struct _TPrule_42));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_42;
_SETCOORD(_currn)
_TERMACT_rule_42;
return ( (NODEPTR) _currn);
}/* Mkrule_42 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_43 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_43 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_43 _currn;
#ifdef __cplusplus
_currn = new _TPrule_43;
#else
_currn = (_TPPrule_43) TreeNodeAlloc (sizeof (struct _TPrule_43));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_43;
_currn->_desc1 = (_TSPAttrDefIds) MkAttrDefIds (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_43: root of subtree no. 1 can not be made a AttrDefIds node ", 0, _coordref);
_currn->_desc2 = (_TSPTypeId) MkTypeId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_43: root of subtree no. 2 can not be made a TypeId node ", 0, _coordref);
_currn->_desc3 = (_TSPClass) MkClass (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_43: root of subtree no. 3 can not be made a Class node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_43;
return ( (NODEPTR) _currn);
}/* Mkrule_43 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_44 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_44 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_44 _currn;
#ifdef __cplusplus
_currn = new _TPrule_44;
#else
_currn = (_TPPrule_44) TreeNodeAlloc (sizeof (struct _TPrule_44));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_44;
_currn->_desc1 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_44: root of subtree no. 1 can not be made a ExpressionDep node ", 0, _coordref);
_currn->_desc2 = (_TSPAttr) MkAttr (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_44: root of subtree no. 2 can not be made a Attr node ", 0, _coordref);
_currn->_desc3 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_44: root of subtree no. 3 can not be made a ExpressionDep node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_44;
return ( (NODEPTR) _currn);
}/* Mkrule_44 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_45 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_45 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_45 _currn;
#ifdef __cplusplus
_currn = new _TPrule_45;
#else
_currn = (_TPPrule_45) TreeNodeAlloc (sizeof (struct _TPrule_45));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_45;
_SETCOORD(_currn)
_TERMACT_rule_45;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_45 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_46 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_46 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_46 _currn;
#ifdef __cplusplus
_currn = new _TPrule_46;
#else
_currn = (_TPPrule_46) TreeNodeAlloc (sizeof (struct _TPrule_46));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_46;
_currn->_desc1 = (_TSPAttrUseId) MkAttrUseId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_46: root of subtree no. 1 can not be made a AttrUseId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_46;
return ( (NODEPTR) _currn);
}/* Mkrule_46 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_47 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_47 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_47 _currn;
#ifdef __cplusplus
_currn = new _TPrule_47;
#else
_currn = (_TPPrule_47) TreeNodeAlloc (sizeof (struct _TPrule_47));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_47;
_currn->_desc1 = (_TSPSymOcc) MkSymOcc (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_47: root of subtree no. 1 can not be made a SymOcc node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrUseId) MkAttrUseId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_47: root of subtree no. 2 can not be made a AttrUseId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_47;
return ( (NODEPTR) _currn);
}/* Mkrule_47 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_48 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_48 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_48 _currn;
#ifdef __cplusplus
_currn = new _TPrule_48;
#else
_currn = (_TPPrule_48) TreeNodeAlloc (sizeof (struct _TPrule_48));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_48;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_48: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrUseId) MkAttrUseId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_48: root of subtree no. 2 can not be made a AttrUseId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_48;
return ( (NODEPTR) _currn);
}/* Mkrule_48 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_49 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_49 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_49 _currn;
#ifdef __cplusplus
_currn = new _TPrule_49;
#else
_currn = (_TPPrule_49) TreeNodeAlloc (sizeof (struct _TPrule_49));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_49;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_49: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_49;
return ( (NODEPTR) _currn);
}/* Mkrule_49 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_50 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_50 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_50 _currn;
#ifdef __cplusplus
_currn = new _TPrule_50;
#else
_currn = (_TPPrule_50) TreeNodeAlloc (sizeof (struct _TPrule_50));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_50;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_50: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_50;
return ( (NODEPTR) _currn);
}/* Mkrule_50 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_51 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_51 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_51 _currn;
#ifdef __cplusplus
_currn = new _TPrule_51;
#else
_currn = (_TPPrule_51) TreeNodeAlloc (sizeof (struct _TPrule_51));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_51;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_51: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_currn->_desc2 = (_TSPAlts) MkAlts (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_51: root of subtree no. 2 can not be made a Alts node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_51;
return ( (NODEPTR) _currn);
}/* Mkrule_51 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_52 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_52 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_52 _currn;
#ifdef __cplusplus
_currn = new _TPrule_52;
#else
_currn = (_TPPrule_52) TreeNodeAlloc (sizeof (struct _TPrule_52));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_52;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_52: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_currn->_desc2 = (_TSPSyntUnits) MkSyntUnits (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_52: root of subtree no. 2 can not be made a SyntUnits node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_52;
return ( (NODEPTR) _currn);
}/* Mkrule_52 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_53 (POSITION *_coordref)
#else
NODEPTR Mkrule_53 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_53 _currn;
#ifdef __cplusplus
_currn = new _TPrule_53;
#else
_currn = (_TPPrule_53) TreeNodeAlloc (sizeof (struct _TPrule_53));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_53;
_SETCOORD(_currn)
_TERMACT_rule_53;
return ( (NODEPTR) _currn);
}/* Mkrule_53 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_54 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_54 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_54 _currn;
#ifdef __cplusplus
_currn = new _TPrule_54;
#else
_currn = (_TPPrule_54) TreeNodeAlloc (sizeof (struct _TPrule_54));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_54;
_currn->_desc1 = (_TSPRuleId) MkRuleId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_54: root of subtree no. 1 can not be made a RuleId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_54;
return ( (NODEPTR) _currn);
}/* Mkrule_54 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_55 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_55 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_55 _currn;
#ifdef __cplusplus
_currn = new _TPrule_55;
#else
_currn = (_TPPrule_55) TreeNodeAlloc (sizeof (struct _TPrule_55));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_55;
_currn->_desc1 = (_TSPRuleSpecId) MkRuleSpecId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_55: root of subtree no. 1 can not be made a RuleSpecId node ", 0, _coordref);
_currn->_desc2 = (_TSPProduction) MkProduction (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_55: root of subtree no. 2 can not be made a Production node ", 0, _coordref);
_currn->_desc3 = (_TSPCompPart) MkCompPart (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_55: root of subtree no. 3 can not be made a CompPart node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_55;
return ( (NODEPTR) _currn);
}/* Mkrule_55 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_56 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_56 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_56 _currn;
#ifdef __cplusplus
_currn = new _TPrule_56;
#else
_currn = (_TPPrule_56) TreeNodeAlloc (sizeof (struct _TPrule_56));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_56;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_56: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_56;
return ( (NODEPTR) _currn);
}/* Mkrule_56 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_57 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_57 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_57 _currn;
#ifdef __cplusplus
_currn = new _TPrule_57;
#else
_currn = (_TPPrule_57) TreeNodeAlloc (sizeof (struct _TPrule_57));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_57;
_SETCOORD(_currn)
_TERMACT_rule_57;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "P_String", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_57 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_58 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_58 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_58 _currn;
#ifdef __cplusplus
_currn = new _TPrule_58;
#else
_currn = (_TPPrule_58) TreeNodeAlloc (sizeof (struct _TPrule_58));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_58;
_currn->_desc1 = (_TSPSymbolDefIds) MkSymbolDefIds (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_58: root of subtree no. 1 can not be made a SymbolDefIds node ", 0, _coordref);
_currn->_desc2 = (_TSPTypeId) MkTypeId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_58: root of subtree no. 2 can not be made a TypeId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_58;
return ( (NODEPTR) _currn);
}/* Mkrule_58 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_59 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3, NODEPTR _desc4)
#else
NODEPTR Mkrule_59 (_coordref,_desc1,_desc2,_desc3,_desc4)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
	NODEPTR _desc4;
#endif
{	_TPPrule_59 _currn;
#ifdef __cplusplus
_currn = new _TPrule_59;
#else
_currn = (_TPPrule_59) TreeNodeAlloc (sizeof (struct _TPrule_59));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_59;
_currn->_desc1 = (_TSPSymClass) MkSymClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_59: root of subtree no. 1 can not be made a SymClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbolDefId) MkSymbolDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_59: root of subtree no. 2 can not be made a SymbolDefId node ", 0, _coordref);
_currn->_desc3 = (_TSPInheritOpt) MkInheritOpt (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_59: root of subtree no. 3 can not be made a InheritOpt node ", 0, _coordref);
_currn->_desc4 = (_TSPCompPart) MkCompPart (_coordref, _desc4);	
if (((NODEPTR)_currn->_desc4) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_59: root of subtree no. 4 can not be made a CompPart node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_59;
return ( (NODEPTR) _currn);
}/* Mkrule_59 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_60 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR Mkrule_60 (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrule_60 _currn;
#ifdef __cplusplus
_currn = new _TPrule_60;
#else
_currn = (_TPPrule_60) TreeNodeAlloc (sizeof (struct _TPrule_60));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_60;
_currn->_desc1 = (_TSPSymClass) MkSymClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_60: root of subtree no. 1 can not be made a SymClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbolDefIds) MkSymbolDefIds (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_60: root of subtree no. 2 can not be made a SymbolDefIds node ", 0, _coordref);
_currn->_desc3 = (_TSPAttrDefsOpt) MkAttrDefsOpt (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_60: root of subtree no. 3 can not be made a AttrDefsOpt node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_60;
return ( (NODEPTR) _currn);
}/* Mkrule_60 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_61 (POSITION *_coordref)
#else
NODEPTR Mkrule_61 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_61 _currn;
#ifdef __cplusplus
_currn = new _TPrule_61;
#else
_currn = (_TPPrule_61) TreeNodeAlloc (sizeof (struct _TPrule_61));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_61;
_SETCOORD(_currn)
_TERMACT_rule_61;
return ( (NODEPTR) _currn);
}/* Mkrule_61 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_62 (POSITION *_coordref)
#else
NODEPTR Mkrule_62 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_62 _currn;
#ifdef __cplusplus
_currn = new _TPrule_62;
#else
_currn = (_TPPrule_62) TreeNodeAlloc (sizeof (struct _TPrule_62));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_62;
_SETCOORD(_currn)
_TERMACT_rule_62;
return ( (NODEPTR) _currn);
}/* Mkrule_62 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_63 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_63 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_63 _currn;
#ifdef __cplusplus
_currn = new _TPrule_63;
#else
_currn = (_TPPrule_63) TreeNodeAlloc (sizeof (struct _TPrule_63));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_63;
_currn->_desc1 = (_TSPSymbolId) MkSymbolId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_63: root of subtree no. 1 can not be made a SymbolId node ", 0, _coordref);
_currn->_desc2 = (_TSPIndex) MkIndex (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_63: root of subtree no. 2 can not be made a Index node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_63;
return ( (NODEPTR) _currn);
}/* Mkrule_63 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_075 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_075 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_075 _currn;
#ifdef __cplusplus
_currn = new _TPrule_075;
#else
_currn = (_TPPrule_075) TreeNodeAlloc (sizeof (struct _TPrule_075));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_075;
_SETCOORD(_currn)
_TERMACT_rule_075;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_075 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_074 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_074 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_074 _currn;
#ifdef __cplusplus
_currn = new _TPrule_074;
#else
_currn = (_TPPrule_074) TreeNodeAlloc (sizeof (struct _TPrule_074));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_074;
_currn->_desc1 = (_TSPSyntLit) MkSyntLit (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_074: root of subtree no. 1 can not be made a SyntLit node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_074;
return ( (NODEPTR) _currn);
}/* Mkrule_074 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_073 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_073 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_073 _currn;
#ifdef __cplusplus
_currn = new _TPrule_073;
#else
_currn = (_TPPrule_073) TreeNodeAlloc (sizeof (struct _TPrule_073));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_073;
_currn->_desc1 = (_TSPSyntId) MkSyntId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_073: root of subtree no. 1 can not be made a SyntId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_073;
return ( (NODEPTR) _currn);
}/* Mkrule_073 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_072 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_072 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_072 _currn;
#ifdef __cplusplus
_currn = new _TPrule_072;
#else
_currn = (_TPPrule_072) TreeNodeAlloc (sizeof (struct _TPrule_072));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_072;
_currn->_desc1 = (_TSPSyntUnits) MkSyntUnits (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_072: root of subtree no. 1 can not be made a SyntUnits node ", 0, _coordref);
_currn->_desc2 = (_TSPSyntUnit) MkSyntUnit (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_072: root of subtree no. 2 can not be made a SyntUnit node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_072;
return ( (NODEPTR) _currn);
}/* Mkrule_072 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_071 (POSITION *_coordref)
#else
NODEPTR Mkrule_071 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_071 _currn;
#ifdef __cplusplus
_currn = new _TPrule_071;
#else
_currn = (_TPPrule_071) TreeNodeAlloc (sizeof (struct _TPrule_071));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_071;
_SETCOORD(_currn)
_TERMACT_rule_071;
return ( (NODEPTR) _currn);
}/* Mkrule_071 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_070 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_070 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_070 _currn;
#ifdef __cplusplus
_currn = new _TPrule_070;
#else
_currn = (_TPPrule_070) TreeNodeAlloc (sizeof (struct _TPrule_070));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_070;
_SETCOORD(_currn)
_TERMACT_rule_070;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_070 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_069 (POSITION *_coordref)
#else
NODEPTR Mkrule_069 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_069 _currn;
#ifdef __cplusplus
_currn = new _TPrule_069;
#else
_currn = (_TPPrule_069) TreeNodeAlloc (sizeof (struct _TPrule_069));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_069;
_SETCOORD(_currn)
_TERMACT_rule_069;
return ( (NODEPTR) _currn);
}/* Mkrule_069 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_068 (POSITION *_coordref)
#else
NODEPTR Mkrule_068 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_068 _currn;
#ifdef __cplusplus
_currn = new _TPrule_068;
#else
_currn = (_TPPrule_068) TreeNodeAlloc (sizeof (struct _TPrule_068));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_068;
_SETCOORD(_currn)
_TERMACT_rule_068;
return ( (NODEPTR) _currn);
}/* Mkrule_068 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_067 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_067 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_067 _currn;
#ifdef __cplusplus
_currn = new _TPrule_067;
#else
_currn = (_TPPrule_067) TreeNodeAlloc (sizeof (struct _TPrule_067));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_067;
_SETCOORD(_currn)
_TERMACT_rule_067;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_067 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_066 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_066 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_066 _currn;
#ifdef __cplusplus
_currn = new _TPrule_066;
#else
_currn = (_TPPrule_066) TreeNodeAlloc (sizeof (struct _TPrule_066));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_066;
_currn->_desc1 = (_TSPG1) MkG1 (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_066: root of subtree no. 1 can not be made a G1 node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_066;
return ( (NODEPTR) _currn);
}/* Mkrule_066 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_065 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_065 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_065 _currn;
#ifdef __cplusplus
_currn = new _TPrule_065;
#else
_currn = (_TPPrule_065) TreeNodeAlloc (sizeof (struct _TPrule_065));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_065;
_SETCOORD(_currn)
_TERMACT_rule_065;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_065 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_064 (POSITION *_coordref)
#else
NODEPTR Mkrule_064 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_064 _currn;
#ifdef __cplusplus
_currn = new _TPrule_064;
#else
_currn = (_TPPrule_064) TreeNodeAlloc (sizeof (struct _TPrule_064));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_064;
_SETCOORD(_currn)
_TERMACT_rule_064;
return ( (NODEPTR) _currn);
}/* Mkrule_064 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_063 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_063 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_063 _currn;
#ifdef __cplusplus
_currn = new _TPrule_063;
#else
_currn = (_TPPrule_063) TreeNodeAlloc (sizeof (struct _TPrule_063));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_063;
_currn->_desc1 = (_TSPTermSpec) MkTermSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_063: root of subtree no. 1 can not be made a TermSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_063;
return ( (NODEPTR) _currn);
}/* Mkrule_063 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_062 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_062 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_062 _currn;
#ifdef __cplusplus
_currn = new _TPrule_062;
#else
_currn = (_TPPrule_062) TreeNodeAlloc (sizeof (struct _TPrule_062));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_062;
_currn->_desc1 = (_TSPSymCompSpec) MkSymCompSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_062: root of subtree no. 1 can not be made a SymCompSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_062;
return ( (NODEPTR) _currn);
}/* Mkrule_062 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_061 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_061 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_061 _currn;
#ifdef __cplusplus
_currn = new _TPrule_061;
#else
_currn = (_TPPrule_061) TreeNodeAlloc (sizeof (struct _TPrule_061));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_061;
_currn->_desc1 = (_TSPSymAttrSpec) MkSymAttrSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_061: root of subtree no. 1 can not be made a SymAttrSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_061;
return ( (NODEPTR) _currn);
}/* Mkrule_061 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_060 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_060 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_060 _currn;
#ifdef __cplusplus
_currn = new _TPrule_060;
#else
_currn = (_TPPrule_060) TreeNodeAlloc (sizeof (struct _TPrule_060));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_060;
_currn->_desc1 = (_TSPSpecs) MkSpecs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_060: root of subtree no. 1 can not be made a Specs node ", 0, _coordref);
_currn->_desc2 = (_TSPSpec) MkSpec (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_060: root of subtree no. 2 can not be made a Spec node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_060;
return ( (NODEPTR) _currn);
}/* Mkrule_060 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_059 (POSITION *_coordref)
#else
NODEPTR Mkrule_059 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_059 _currn;
#ifdef __cplusplus
_currn = new _TPrule_059;
#else
_currn = (_TPPrule_059) TreeNodeAlloc (sizeof (struct _TPrule_059));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_059;
_SETCOORD(_currn)
_TERMACT_rule_059;
return ( (NODEPTR) _currn);
}/* Mkrule_059 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_058 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_058 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_058 _currn;
#ifdef __cplusplus
_currn = new _TPrule_058;
#else
_currn = (_TPPrule_058) TreeNodeAlloc (sizeof (struct _TPrule_058));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_058;
_currn->_desc1 = (_TSPRuleSpec) MkRuleSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_058: root of subtree no. 1 can not be made a RuleSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_058;
return ( (NODEPTR) _currn);
}/* Mkrule_058 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_057 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_057 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_057 _currn;
#ifdef __cplusplus
_currn = new _TPrule_057;
#else
_currn = (_TPPrule_057) TreeNodeAlloc (sizeof (struct _TPrule_057));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_057;
_currn->_desc1 = (_TSPChainSpec) MkChainSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_057: root of subtree no. 1 can not be made a ChainSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_057;
return ( (NODEPTR) _currn);
}/* Mkrule_057 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_056 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_056 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_056 _currn;
#ifdef __cplusplus
_currn = new _TPrule_056;
#else
_currn = (_TPPrule_056) TreeNodeAlloc (sizeof (struct _TPrule_056));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_056;
_currn->_desc1 = (_TSPAttrSpec) MkAttrSpec (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_056: root of subtree no. 1 can not be made a AttrSpec node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_056;
return ( (NODEPTR) _currn);
}/* Mkrule_056 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_055 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_055 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_055 _currn;
#ifdef __cplusplus
_currn = new _TPrule_055;
#else
_currn = (_TPPrule_055) TreeNodeAlloc (sizeof (struct _TPrule_055));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_055;
_currn->_desc1 = (_TSPShieldSyms) MkShieldSyms (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_055: root of subtree no. 1 can not be made a ShieldSyms node ", 0, _coordref);
_currn->_desc2 = (_TSPShieldSym) MkShieldSym (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_055: root of subtree no. 2 can not be made a ShieldSym node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_055;
return ( (NODEPTR) _currn);
}/* Mkrule_055 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_054 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_054 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_054 _currn;
#ifdef __cplusplus
_currn = new _TPrule_054;
#else
_currn = (_TPPrule_054) TreeNodeAlloc (sizeof (struct _TPrule_054));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_054;
_currn->_desc1 = (_TSPShieldSym) MkShieldSym (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_054: root of subtree no. 1 can not be made a ShieldSym node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_054;
return ( (NODEPTR) _currn);
}/* Mkrule_054 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_053 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_053 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_053 _currn;
#ifdef __cplusplus
_currn = new _TPrule_053;
#else
_currn = (_TPPrule_053) TreeNodeAlloc (sizeof (struct _TPrule_053));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_053;
_currn->_desc1 = (_TSPShieldSyms) MkShieldSyms (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_053: root of subtree no. 1 can not be made a ShieldSyms node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_053;
return ( (NODEPTR) _currn);
}/* Mkrule_053 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_052 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_052 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_052 _currn;
#ifdef __cplusplus
_currn = new _TPrule_052;
#else
_currn = (_TPPrule_052) TreeNodeAlloc (sizeof (struct _TPrule_052));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_052;
_currn->_desc1 = (_TSPShieldSym) MkShieldSym (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_052: root of subtree no. 1 can not be made a ShieldSym node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_052;
return ( (NODEPTR) _currn);
}/* Mkrule_052 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_051 (POSITION *_coordref)
#else
NODEPTR Mkrule_051 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_051 _currn;
#ifdef __cplusplus
_currn = new _TPrule_051;
#else
_currn = (_TPPrule_051) TreeNodeAlloc (sizeof (struct _TPrule_051));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_051;
_SETCOORD(_currn)
_TERMACT_rule_051;
return ( (NODEPTR) _currn);
}/* Mkrule_051 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_050 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_050 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_050 _currn;
#ifdef __cplusplus
_currn = new _TPrule_050;
#else
_currn = (_TPPrule_050) TreeNodeAlloc (sizeof (struct _TPrule_050));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_050;
_SETCOORD(_currn)
_TERMACT_rule_050;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_050 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_049 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_049 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_049 _currn;
#ifdef __cplusplus
_currn = new _TPrule_049;
#else
_currn = (_TPPrule_049) TreeNodeAlloc (sizeof (struct _TPrule_049));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_049;
_currn->_desc1 = (_TSPRemoteAttrs) MkRemoteAttrs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_049: root of subtree no. 1 can not be made a RemoteAttrs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_049;
return ( (NODEPTR) _currn);
}/* Mkrule_049 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_048 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_048 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_048 _currn;
#ifdef __cplusplus
_currn = new _TPrule_048;
#else
_currn = (_TPPrule_048) TreeNodeAlloc (sizeof (struct _TPrule_048));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_048;
_currn->_desc1 = (_TSPRemoteAttr) MkRemoteAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_048: root of subtree no. 1 can not be made a RemoteAttr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_048;
return ( (NODEPTR) _currn);
}/* Mkrule_048 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_047 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_047 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_047 _currn;
#ifdef __cplusplus
_currn = new _TPrule_047;
#else
_currn = (_TPPrule_047) TreeNodeAlloc (sizeof (struct _TPrule_047));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_047;
_currn->_desc1 = (_TSPRemoteAttr) MkRemoteAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_047: root of subtree no. 1 can not be made a RemoteAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPRemoteAttrs) MkRemoteAttrs (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_047: root of subtree no. 2 can not be made a RemoteAttrs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_047;
return ( (NODEPTR) _currn);
}/* Mkrule_047 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_046 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_046 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_046 _currn;
#ifdef __cplusplus
_currn = new _TPrule_046;
#else
_currn = (_TPPrule_046) TreeNodeAlloc (sizeof (struct _TPrule_046));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_046;
_currn->_desc1 = (_TSPRemoteAttr) MkRemoteAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_046: root of subtree no. 1 can not be made a RemoteAttr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_046;
return ( (NODEPTR) _currn);
}/* Mkrule_046 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_045 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_045 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_045 _currn;
#ifdef __cplusplus
_currn = new _TPrule_045;
#else
_currn = (_TPPrule_045) TreeNodeAlloc (sizeof (struct _TPrule_045));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_045;
_currn->_desc1 = (_TSPLoop) MkLoop (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_045: root of subtree no. 1 can not be made a Loop node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_045;
return ( (NODEPTR) _currn);
}/* Mkrule_045 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_044 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_044 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_044 _currn;
#ifdef __cplusplus
_currn = new _TPrule_044;
#else
_currn = (_TPPrule_044) TreeNodeAlloc (sizeof (struct _TPrule_044));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_044;
_currn->_desc1 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_044: root of subtree no. 1 can not be made a ExpressionDep node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_044;
return ( (NODEPTR) _currn);
}/* Mkrule_044 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_043 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_043 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_043 _currn;
#ifdef __cplusplus
_currn = new _TPrule_043;
#else
_currn = (_TPPrule_043) TreeNodeAlloc (sizeof (struct _TPrule_043));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_043;
_currn->_desc1 = (_TSPParam) MkParam (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_043: root of subtree no. 1 can not be made a Param node ", 0, _coordref);
_currn->_desc2 = (_TSPParams) MkParams (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_043: root of subtree no. 2 can not be made a Params node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_043;
return ( (NODEPTR) _currn);
}/* Mkrule_043 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_042 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_042 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_042 _currn;
#ifdef __cplusplus
_currn = new _TPrule_042;
#else
_currn = (_TPPrule_042) TreeNodeAlloc (sizeof (struct _TPrule_042));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_042;
_currn->_desc1 = (_TSPParam) MkParam (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_042: root of subtree no. 1 can not be made a Param node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_042;
return ( (NODEPTR) _currn);
}/* Mkrule_042 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_041 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_041 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_041 _currn;
#ifdef __cplusplus
_currn = new _TPrule_041;
#else
_currn = (_TPPrule_041) TreeNodeAlloc (sizeof (struct _TPrule_041));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_041;
_currn->_desc1 = (_TSPParams) MkParams (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_041: root of subtree no. 1 can not be made a Params node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_041;
return ( (NODEPTR) _currn);
}/* Mkrule_041 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_040 (POSITION *_coordref)
#else
NODEPTR Mkrule_040 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_040 _currn;
#ifdef __cplusplus
_currn = new _TPrule_040;
#else
_currn = (_TPPrule_040) TreeNodeAlloc (sizeof (struct _TPrule_040));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_040;
_SETCOORD(_currn)
_TERMACT_rule_040;
return ( (NODEPTR) _currn);
}/* Mkrule_040 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_039 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_039 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_039 _currn;
#ifdef __cplusplus
_currn = new _TPrule_039;
#else
_currn = (_TPPrule_039) TreeNodeAlloc (sizeof (struct _TPrule_039));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_039;
_currn->_desc1 = (_TSPExpressionDep) MkExpressionDep (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_039: root of subtree no. 1 can not be made a ExpressionDep node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_039;
return ( (NODEPTR) _currn);
}/* Mkrule_039 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_038 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_038 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_038 _currn;
#ifdef __cplusplus
_currn = new _TPrule_038;
#else
_currn = (_TPPrule_038) TreeNodeAlloc (sizeof (struct _TPrule_038));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_038;
_currn->_desc1 = (_TSPInheritSyms) MkInheritSyms (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_038: root of subtree no. 1 can not be made a InheritSyms node ", 0, _coordref);
_currn->_desc2 = (_TSPInheritSym) MkInheritSym (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_038: root of subtree no. 2 can not be made a InheritSym node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_038;
return ( (NODEPTR) _currn);
}/* Mkrule_038 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_037 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_037 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_037 _currn;
#ifdef __cplusplus
_currn = new _TPrule_037;
#else
_currn = (_TPPrule_037) TreeNodeAlloc (sizeof (struct _TPrule_037));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_037;
_currn->_desc1 = (_TSPInheritSym) MkInheritSym (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_037: root of subtree no. 1 can not be made a InheritSym node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_037;
return ( (NODEPTR) _currn);
}/* Mkrule_037 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_036 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_036 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_036 _currn;
#ifdef __cplusplus
_currn = new _TPrule_036;
#else
_currn = (_TPPrule_036) TreeNodeAlloc (sizeof (struct _TPrule_036));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_036;
_currn->_desc1 = (_TSPInheritSyms) MkInheritSyms (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_036: root of subtree no. 1 can not be made a InheritSyms node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_036;
return ( (NODEPTR) _currn);
}/* Mkrule_036 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_035 (POSITION *_coordref)
#else
NODEPTR Mkrule_035 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_035 _currn;
#ifdef __cplusplus
_currn = new _TPrule_035;
#else
_currn = (_TPPrule_035) TreeNodeAlloc (sizeof (struct _TPrule_035));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_035;
_SETCOORD(_currn)
_TERMACT_rule_035;
return ( (NODEPTR) _currn);
}/* Mkrule_035 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_034 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_034 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_034 _currn;
#ifdef __cplusplus
_currn = new _TPrule_034;
#else
_currn = (_TPPrule_034) TreeNodeAlloc (sizeof (struct _TPrule_034));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_034;
_currn->_desc1 = (_TSPSymbolDefId) MkSymbolDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_034: root of subtree no. 1 can not be made a SymbolDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_034;
return ( (NODEPTR) _currn);
}/* Mkrule_034 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_033 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_033 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_033 _currn;
#ifdef __cplusplus
_currn = new _TPrule_033;
#else
_currn = (_TPPrule_033) TreeNodeAlloc (sizeof (struct _TPrule_033));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_033;
_currn->_desc1 = (_TSPG1) MkG1 (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_033: root of subtree no. 1 can not be made a G1 node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbolDefId) MkSymbolDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_033: root of subtree no. 2 can not be made a SymbolDefId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_033;
return ( (NODEPTR) _currn);
}/* Mkrule_033 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_032 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_032 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_032 _currn;
#ifdef __cplusplus
_currn = new _TPrule_032;
#else
_currn = (_TPPrule_032) TreeNodeAlloc (sizeof (struct _TPrule_032));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_032;
_currn->_desc1 = (_TSPDepAttrs) MkDepAttrs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_032: root of subtree no. 1 can not be made a DepAttrs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_032;
return ( (NODEPTR) _currn);
}/* Mkrule_032 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_031 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_031 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_031 _currn;
#ifdef __cplusplus
_currn = new _TPrule_031;
#else
_currn = (_TPPrule_031) TreeNodeAlloc (sizeof (struct _TPrule_031));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_031;
_currn->_desc1 = (_TSPDepAttr) MkDepAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_031: root of subtree no. 1 can not be made a DepAttr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_031;
return ( (NODEPTR) _currn);
}/* Mkrule_031 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_030 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_030 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_030 _currn;
#ifdef __cplusplus
_currn = new _TPrule_030;
#else
_currn = (_TPPrule_030) TreeNodeAlloc (sizeof (struct _TPrule_030));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_030;
_currn->_desc1 = (_TSPDependence) MkDependence (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_030: root of subtree no. 1 can not be made a Dependence node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_030;
return ( (NODEPTR) _currn);
}/* Mkrule_030 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_029 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_029 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_029 _currn;
#ifdef __cplusplus
_currn = new _TPrule_029;
#else
_currn = (_TPPrule_029) TreeNodeAlloc (sizeof (struct _TPrule_029));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_029;
_currn->_desc1 = (_TSPDependence) MkDependence (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_029: root of subtree no. 1 can not be made a Dependence node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_029;
return ( (NODEPTR) _currn);
}/* Mkrule_029 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_028 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_028 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_028 _currn;
#ifdef __cplusplus
_currn = new _TPrule_028;
#else
_currn = (_TPPrule_028) TreeNodeAlloc (sizeof (struct _TPrule_028));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_028;
_currn->_desc1 = (_TSPDepAttr) MkDepAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_028: root of subtree no. 1 can not be made a DepAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPDepAttrs) MkDepAttrs (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_028: root of subtree no. 2 can not be made a DepAttrs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_028;
return ( (NODEPTR) _currn);
}/* Mkrule_028 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_027 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_027 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_027 _currn;
#ifdef __cplusplus
_currn = new _TPrule_027;
#else
_currn = (_TPPrule_027) TreeNodeAlloc (sizeof (struct _TPrule_027));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_027;
_currn->_desc1 = (_TSPDepAttr) MkDepAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_027: root of subtree no. 1 can not be made a DepAttr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_027;
return ( (NODEPTR) _currn);
}/* Mkrule_027 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_026 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_026 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_026 _currn;
#ifdef __cplusplus
_currn = new _TPrule_026;
#else
_currn = (_TPPrule_026) TreeNodeAlloc (sizeof (struct _TPrule_026));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_026;
_currn->_desc1 = (_TSPRhsAttrs) MkRhsAttrs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_026: root of subtree no. 1 can not be made a RhsAttrs node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_026;
return ( (NODEPTR) _currn);
}/* Mkrule_026 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_025 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_025 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_025 _currn;
#ifdef __cplusplus
_currn = new _TPrule_025;
#else
_currn = (_TPPrule_025) TreeNodeAlloc (sizeof (struct _TPrule_025));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_025;
_currn->_desc1 = (_TSPRemoteExpression) MkRemoteExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_025: root of subtree no. 1 can not be made a RemoteExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_025;
return ( (NODEPTR) _currn);
}/* Mkrule_025 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_024 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_024 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_024 _currn;
#ifdef __cplusplus
_currn = new _TPrule_024;
#else
_currn = (_TPPrule_024) TreeNodeAlloc (sizeof (struct _TPrule_024));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_024;
_currn->_desc1 = (_TSPAttr) MkAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_024: root of subtree no. 1 can not be made a Attr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_024;
return ( (NODEPTR) _currn);
}/* Mkrule_024 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_023 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_023 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_023 _currn;
#ifdef __cplusplus
_currn = new _TPrule_023;
#else
_currn = (_TPPrule_023) TreeNodeAlloc (sizeof (struct _TPrule_023));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_023;
_currn->_desc1 = (_TSPComputation) MkComputation (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_023: root of subtree no. 1 can not be made a Computation node ", 0, _coordref);
_currn->_desc2 = (_TSPComputations) MkComputations (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_023: root of subtree no. 2 can not be made a Computations node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_023;
return ( (NODEPTR) _currn);
}/* Mkrule_023 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_022 (POSITION *_coordref)
#else
NODEPTR Mkrule_022 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_022 _currn;
#ifdef __cplusplus
_currn = new _TPrule_022;
#else
_currn = (_TPPrule_022) TreeNodeAlloc (sizeof (struct _TPrule_022));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_022;
_SETCOORD(_currn)
_TERMACT_rule_022;
return ( (NODEPTR) _currn);
}/* Mkrule_022 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_021 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_021 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_021 _currn;
#ifdef __cplusplus
_currn = new _TPrule_021;
#else
_currn = (_TPPrule_021) TreeNodeAlloc (sizeof (struct _TPrule_021));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_021;
_currn->_desc1 = (_TSPCompute) MkCompute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_021: root of subtree no. 1 can not be made a Compute node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_021;
return ( (NODEPTR) _currn);
}/* Mkrule_021 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_020 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_020 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_020 _currn;
#ifdef __cplusplus
_currn = new _TPrule_020;
#else
_currn = (_TPPrule_020) TreeNodeAlloc (sizeof (struct _TPrule_020));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_020;
_currn->_desc1 = (_TSPComputations) MkComputations (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_020: root of subtree no. 1 can not be made a Computations node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_020;
return ( (NODEPTR) _currn);
}/* Mkrule_020 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_019 (POSITION *_coordref)
#else
NODEPTR Mkrule_019 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_019 _currn;
#ifdef __cplusplus
_currn = new _TPrule_019;
#else
_currn = (_TPPrule_019) TreeNodeAlloc (sizeof (struct _TPrule_019));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_019;
_SETCOORD(_currn)
_TERMACT_rule_019;
return ( (NODEPTR) _currn);
}/* Mkrule_019 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_018 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_018 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_018 _currn;
#ifdef __cplusplus
_currn = new _TPrule_018;
#else
_currn = (_TPPrule_018) TreeNodeAlloc (sizeof (struct _TPrule_018));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_018;
_currn->_desc1 = (_TSPChainNames) MkChainNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_018: root of subtree no. 1 can not be made a ChainNames node ", 0, _coordref);
_currn->_desc2 = (_TSPChainName) MkChainName (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_018: root of subtree no. 2 can not be made a ChainName node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_018;
return ( (NODEPTR) _currn);
}/* Mkrule_018 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_017 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_017 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_017 _currn;
#ifdef __cplusplus
_currn = new _TPrule_017;
#else
_currn = (_TPPrule_017) TreeNodeAlloc (sizeof (struct _TPrule_017));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_017;
_currn->_desc1 = (_TSPChainName) MkChainName (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_017: root of subtree no. 1 can not be made a ChainName node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_017;
return ( (NODEPTR) _currn);
}/* Mkrule_017 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_016 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_016 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_016 _currn;
#ifdef __cplusplus
_currn = new _TPrule_016;
#else
_currn = (_TPPrule_016) TreeNodeAlloc (sizeof (struct _TPrule_016));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_016;
_SETCOORD(_currn)
_TERMACT_rule_016;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_016 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_015 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_015 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_015 _currn;
#ifdef __cplusplus
_currn = new _TPrule_015;
#else
_currn = (_TPPrule_015) TreeNodeAlloc (sizeof (struct _TPrule_015));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_015;
_currn->_desc1 = (_TSPAttrNames) MkAttrNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_015: root of subtree no. 1 can not be made a AttrNames node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrName) MkAttrName (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_015: root of subtree no. 2 can not be made a AttrName node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_015;
return ( (NODEPTR) _currn);
}/* Mkrule_015 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_014 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_014 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_014 _currn;
#ifdef __cplusplus
_currn = new _TPrule_014;
#else
_currn = (_TPPrule_014) TreeNodeAlloc (sizeof (struct _TPrule_014));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_014;
_currn->_desc1 = (_TSPAttrName) MkAttrName (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_014: root of subtree no. 1 can not be made a AttrName node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_014;
return ( (NODEPTR) _currn);
}/* Mkrule_014 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_013 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_013 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_013 _currn;
#ifdef __cplusplus
_currn = new _TPrule_013;
#else
_currn = (_TPPrule_013) TreeNodeAlloc (sizeof (struct _TPrule_013));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_013;
_SETCOORD(_currn)
_TERMACT_rule_013;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_013 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_012 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_012 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_012 _currn;
#ifdef __cplusplus
_currn = new _TPrule_012;
#else
_currn = (_TPPrule_012) TreeNodeAlloc (sizeof (struct _TPrule_012));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_012;
_currn->_desc1 = (_TSPAttrDefs) MkAttrDefs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_012: root of subtree no. 1 can not be made a AttrDefs node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_012;
return ( (NODEPTR) _currn);
}/* Mkrule_012 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_011 (POSITION *_coordref)
#else
NODEPTR Mkrule_011 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_011 _currn;
#ifdef __cplusplus
_currn = new _TPrule_011;
#else
_currn = (_TPPrule_011) TreeNodeAlloc (sizeof (struct _TPrule_011));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_011;
_SETCOORD(_currn)
_TERMACT_rule_011;
return ( (NODEPTR) _currn);
}/* Mkrule_011 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_010 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_010 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_010 _currn;
#ifdef __cplusplus
_currn = new _TPrule_010;
#else
_currn = (_TPPrule_010) TreeNodeAlloc (sizeof (struct _TPrule_010));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_010;
_currn->_desc1 = (_TSPAttrDefs) MkAttrDefs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_010: root of subtree no. 1 can not be made a AttrDefs node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrDef) MkAttrDef (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_010: root of subtree no. 2 can not be made a AttrDef node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_010;
return ( (NODEPTR) _currn);
}/* Mkrule_010 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_09 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_09 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_09 _currn;
#ifdef __cplusplus
_currn = new _TPrule_09;
#else
_currn = (_TPPrule_09) TreeNodeAlloc (sizeof (struct _TPrule_09));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_09;
_currn->_desc1 = (_TSPAttrDef) MkAttrDef (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_09: root of subtree no. 1 can not be made a AttrDef node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_09;
return ( (NODEPTR) _currn);
}/* Mkrule_09 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_08 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_08 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_08 _currn;
#ifdef __cplusplus
_currn = new _TPrule_08;
#else
_currn = (_TPPrule_08) TreeNodeAlloc (sizeof (struct _TPrule_08));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_08;
_currn->_desc1 = (_TSPAttrDefIds) MkAttrDefIds (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_08: root of subtree no. 1 can not be made a AttrDefIds node ", 0, _coordref);
_currn->_desc2 = (_TSPAttrDefId) MkAttrDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_08: root of subtree no. 2 can not be made a AttrDefId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_08;
return ( (NODEPTR) _currn);
}/* Mkrule_08 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_07 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_07 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_07 _currn;
#ifdef __cplusplus
_currn = new _TPrule_07;
#else
_currn = (_TPPrule_07) TreeNodeAlloc (sizeof (struct _TPrule_07));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_07;
_currn->_desc1 = (_TSPAttrDefId) MkAttrDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_07: root of subtree no. 1 can not be made a AttrDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_07;
return ( (NODEPTR) _currn);
}/* Mkrule_07 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_06 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_06 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_06 _currn;
#ifdef __cplusplus
_currn = new _TPrule_06;
#else
_currn = (_TPPrule_06) TreeNodeAlloc (sizeof (struct _TPrule_06));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_06;
_SETCOORD(_currn)
_TERMACT_rule_06;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "Identifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_06 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_05 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_05 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_05 _currn;
#ifdef __cplusplus
_currn = new _TPrule_05;
#else
_currn = (_TPPrule_05) TreeNodeAlloc (sizeof (struct _TPrule_05));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_05;
_currn->_desc1 = (_TSPDefAttr) MkDefAttr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_05: root of subtree no. 1 can not be made a DefAttr node ", 0, _coordref);
_currn->_desc2 = (_TSPLoop) MkLoop (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_05: root of subtree no. 2 can not be made a Loop node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_05;
return ( (NODEPTR) _currn);
}/* Mkrule_05 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_04 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR Mkrule_04 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrule_04 _currn;
#ifdef __cplusplus
_currn = new _TPrule_04;
#else
_currn = (_TPPrule_04) TreeNodeAlloc (sizeof (struct _TPrule_04));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_04;
_currn->_desc1 = (_TSPAlts) MkAlts (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_04: root of subtree no. 1 can not be made a Alts node ", 0, _coordref);
_currn->_desc2 = (_TSPAlt) MkAlt (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_04: root of subtree no. 2 can not be made a Alt node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rule_04;
return ( (NODEPTR) _currn);
}/* Mkrule_04 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_03 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_03 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_03 _currn;
#ifdef __cplusplus
_currn = new _TPrule_03;
#else
_currn = (_TPPrule_03) TreeNodeAlloc (sizeof (struct _TPrule_03));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_03;
_currn->_desc1 = (_TSPAlt) MkAlt (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_03: root of subtree no. 1 can not be made a Alt node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_03;
return ( (NODEPTR) _currn);
}/* Mkrule_03 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_02 (POSITION *_coordref)
#else
NODEPTR Mkrule_02 (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrule_02 _currn;
#ifdef __cplusplus
_currn = new _TPrule_02;
#else
_currn = (_TPPrule_02) TreeNodeAlloc (sizeof (struct _TPrule_02));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_02;
_SETCOORD(_currn)
_TERMACT_rule_02;
return ( (NODEPTR) _currn);
}/* Mkrule_02 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_01 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR Mkrule_01 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrule_01 _currn;
#ifdef __cplusplus
_currn = new _TPrule_01;
#else
_currn = (_TPPrule_01) TreeNodeAlloc (sizeof (struct _TPrule_01));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_01;
_currn->_desc1 = (_TSPSpecs) MkSpecs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rule_01: root of subtree no. 1 can not be made a Specs node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rule_01;
return ( (NODEPTR) _currn);
}/* Mkrule_01 */
