
#ifndef _VISITPROCS_H
#define _VISITPROCS_H

#include "HEAD.h"
#include "node.h"
#include "treecon.h"

#include "eliproto.h"


extern void LIGA_ATTREVAL ELI_ARG((NODEPTR));
extern void _VS0Empty ELI_ARG((NODEPTR _currn));
extern void _VS1rule_1 ELI_ARG((_TPPrule_1 _currn));
extern void _VS1rule_2 ELI_ARG((_TPPrule_2 _currn));
extern void _VS1rule_3 ELI_ARG((_TPPrule_3 _currn));
extern void _VS2rule_3 ELI_ARG((_TPPrule_3 _currn));
extern void _VS3rule_3 ELI_ARG((_TPPrule_3 _currn));
extern void _VS4rule_3 ELI_ARG((_TPPrule_3 _currn));
extern void _VS5rule_3 ELI_ARG((_TPPrule_3 _currn));
extern void _VS1rule_4 ELI_ARG((_TPPrule_4 _currn));
extern void _VS1rule_5 ELI_ARG((_TPPrule_5 _currn));
#define _VS2rule_5 _VS0Empty

#define _VS3rule_5 _VS0Empty

extern void _VS4rule_5 ELI_ARG((_TPPrule_5 _currn));
extern void _VS1rule_6 ELI_ARG((_TPPrule_6 _currn));
#define _VS2rule_6 _VS0Empty

#define _VS3rule_6 _VS0Empty

extern void _VS4rule_6 ELI_ARG((_TPPrule_6 _currn));
extern void _VS1rule_7 ELI_ARG((_TPPrule_7 _currn));
#define _VS2rule_7 _VS0Empty

#define _VS3rule_7 _VS0Empty

extern void _VS4rule_7 ELI_ARG((_TPPrule_7 _currn));
extern void _VS1rule_8 ELI_ARG((_TPPrule_8 _currn));
#define _VS2rule_8 _VS0Empty

#define _VS3rule_8 _VS0Empty

extern void _VS4rule_8 ELI_ARG((_TPPrule_8 _currn));
extern void _VS1rule_9 ELI_ARG((_TPPrule_9 _currn));
extern void _VS2rule_9 ELI_ARG((_TPPrule_9 _currn));
#define _VS3rule_9 _VS0Empty

extern void _VS4rule_9 ELI_ARG((_TPPrule_9 _currn));
extern void _VS1rule_10 ELI_ARG((_TPPrule_10 _currn));
extern void _VS2rule_10 ELI_ARG((_TPPrule_10 _currn));
extern void _VS3rule_10 ELI_ARG((_TPPrule_10 _currn));
extern void _VS4rule_10 ELI_ARG((_TPPrule_10 _currn));
extern void _VS1rule_11 ELI_ARG((_TPPrule_11 _currn));
#define _VS2rule_11 _VS0Empty

#define _VS3rule_11 _VS0Empty

extern void _VS4rule_11 ELI_ARG((_TPPrule_11 _currn));
extern void _VS1rule_12 ELI_ARG((_TPPrule_12 _currn));
extern void _VS2rule_12 ELI_ARG((_TPPrule_12 _currn));
extern void _VS3rule_12 ELI_ARG((_TPPrule_12 _currn));
extern void _VS4rule_12 ELI_ARG((_TPPrule_12 _currn));
extern void _VS1rule_13 ELI_ARG((_TPPrule_13 _currn));
extern void _VS2rule_13 ELI_ARG((_TPPrule_13 _currn));
extern void _VS3rule_13 ELI_ARG((_TPPrule_13 _currn));
extern void _VS4rule_13 ELI_ARG((_TPPrule_13 _currn));
extern void _VS1rule_14 ELI_ARG((_TPPrule_14 _currn));
extern void _VS2rule_14 ELI_ARG((_TPPrule_14 _currn));
extern void _VS3rule_14 ELI_ARG((_TPPrule_14 _currn));
extern void _VS4rule_14 ELI_ARG((_TPPrule_14 _currn,PExpr* _AS0repr));
extern void _VS1rule_15 ELI_ARG((_TPPrule_15 _currn));
extern void _VS2rule_15 ELI_ARG((_TPPrule_15 _currn));
extern void _VS3rule_15 ELI_ARG((_TPPrule_15 _currn));
extern void _VS4rule_15 ELI_ARG((_TPPrule_15 _currn,PExpr* _AS0repr));
#define _VS1rule_16 _VS0Empty

extern void _VS2rule_16 ELI_ARG((_TPPrule_16 _currn));
extern void _VS1rule_17 ELI_ARG((_TPPrule_17 _currn));
extern void _VS2rule_17 ELI_ARG((_TPPrule_17 _currn));
extern void _VS1rule_18 ELI_ARG((_TPPrule_18 _currn));
extern void _VS2rule_18 ELI_ARG((_TPPrule_18 _currn));
extern void _VS3rule_18 ELI_ARG((_TPPrule_18 _currn));
extern void _VS1rule_19 ELI_ARG((_TPPrule_19 _currn));
extern void _VS2rule_19 ELI_ARG((_TPPrule_19 _currn));
extern void _VS3rule_19 ELI_ARG((_TPPrule_19 _currn));
extern void _VS4rule_19 ELI_ARG((_TPPrule_19 _currn));
extern void _VS1rule_20 ELI_ARG((_TPPrule_20 _currn));
extern void _VS2rule_20 ELI_ARG((_TPPrule_20 _currn));
extern void _VS3rule_20 ELI_ARG((_TPPrule_20 _currn));
extern void _VS4rule_20 ELI_ARG((_TPPrule_20 _currn));
extern void _VS1rule_21 ELI_ARG((_TPPrule_21 _currn));
extern void _VS2rule_21 ELI_ARG((_TPPrule_21 _currn));
extern void _VS3rule_21 ELI_ARG((_TPPrule_21 _currn));
extern void _VS4rule_21 ELI_ARG((_TPPrule_21 _currn));
#define _VS1rule_22 _VS1rule_18

#define _VS2rule_22 _VS2rule_18

extern void _VS3rule_22 ELI_ARG((_TPPrule_22 _currn));
#define _VS1rule_23 _VS0Empty

#define _VS2rule_23 _VS0Empty

extern void _VS3rule_23 ELI_ARG((_TPPrule_23 _currn));
#define _VS1rule_24 _VS1rule_18

#define _VS2rule_24 _VS2rule_18

extern void _VS3rule_24 ELI_ARG((_TPPrule_24 _currn));
extern void _VS4rule_24 ELI_ARG((_TPPrule_24 _currn));
extern void _VS5rule_24 ELI_ARG((_TPPrule_24 _currn,PTGNode* _AS0_const20));
extern void _VS1rule_25 ELI_ARG((_TPPrule_25 _currn));
extern void _VS2rule_25 ELI_ARG((_TPPrule_25 _currn));
extern void _VS3rule_25 ELI_ARG((_TPPrule_25 _currn));
extern void _VS4rule_25 ELI_ARG((_TPPrule_25 _currn));
extern void _VS5rule_25 ELI_ARG((_TPPrule_25 _currn));
extern void _VS1rule_26 ELI_ARG((_TPPrule_26 _currn));
extern void _VS2rule_26 ELI_ARG((_TPPrule_26 _currn));
extern void _VS3rule_26 ELI_ARG((_TPPrule_26 _currn));
extern void _VS4rule_26 ELI_ARG((_TPPrule_26 _currn));
extern void _VS5rule_26 ELI_ARG((_TPPrule_26 _currn));
extern void _VS1rule_27 ELI_ARG((_TPPrule_27 _currn));
extern void _VS2rule_27 ELI_ARG((_TPPrule_27 _currn));
extern void _VS3rule_27 ELI_ARG((_TPPrule_27 _currn));
extern void _VS4rule_27 ELI_ARG((_TPPrule_27 _currn));
extern void _VS5rule_27 ELI_ARG((_TPPrule_27 _currn));
extern void _VS1rule_28 ELI_ARG((_TPPrule_28 _currn));
extern void _VS2rule_28 ELI_ARG((_TPPrule_28 _currn));
extern void _VS3rule_28 ELI_ARG((_TPPrule_28 _currn));
extern void _VS1rule_29 ELI_ARG((_TPPrule_29 _currn));
extern void _VS1rule_30 ELI_ARG((_TPPrule_30 _currn));
extern void _VS1rule_31 ELI_ARG((_TPPrule_31 _currn));
extern void _VS2rule_31 ELI_ARG((_TPPrule_31 _currn));
extern void _VS3rule_31 ELI_ARG((_TPPrule_31 _currn));
extern void _VS4rule_31 ELI_ARG((_TPPrule_31 _currn));
extern void _VS5rule_31 ELI_ARG((_TPPrule_31 _currn));
extern void _VS1rule_32 ELI_ARG((_TPPrule_32 _currn));
#define _VS2rule_32 _VS0Empty

extern void _VS3rule_32 ELI_ARG((_TPPrule_32 _currn));
extern void _VS1rule_33 ELI_ARG((_TPPrule_33 _currn));
extern void _VS2rule_33 ELI_ARG((_TPPrule_33 _currn));
extern void _VS3rule_33 ELI_ARG((_TPPrule_33 _currn));
extern void _VS4rule_33 ELI_ARG((_TPPrule_33 _currn));
extern void _VS1rule_34 ELI_ARG((_TPPrule_34 _currn));
#define _VS2rule_34 _VS0Empty

extern void _VS3rule_34 ELI_ARG((_TPPrule_34 _currn));
#define _VS4rule_34 _VS0Empty

extern void _VS1rule_35 ELI_ARG((_TPPrule_35 _currn));
#define _VS2rule_35 _VS0Empty

extern void _VS3rule_35 ELI_ARG((_TPPrule_35 _currn));
#define _VS4rule_35 _VS0Empty

extern void _VS1rule_36 ELI_ARG((_TPPrule_36 _currn));
#define _VS2rule_36 _VS0Empty

extern void _VS3rule_36 ELI_ARG((_TPPrule_36 _currn));
extern void _VS4rule_36 ELI_ARG((_TPPrule_36 _currn));
extern void _VS1rule_37 ELI_ARG((_TPPrule_37 _currn));
#define _VS2rule_37 _VS0Empty

extern void _VS3rule_37 ELI_ARG((_TPPrule_37 _currn));
extern void _VS4rule_37 ELI_ARG((_TPPrule_37 _currn));
extern void _VS1rule_38 ELI_ARG((_TPPrule_38 _currn));
extern void _VS2rule_38 ELI_ARG((_TPPrule_38 _currn));
extern void _VS3rule_38 ELI_ARG((_TPPrule_38 _currn));
extern void _VS1rule_39 ELI_ARG((_TPPrule_39 _currn));
extern void _VS2rule_39 ELI_ARG((_TPPrule_39 _currn));
extern void _VS3rule_39 ELI_ARG((_TPPrule_39 _currn));
extern void _VS1rule_40 ELI_ARG((_TPPrule_40 _currn));
extern void _VS1rule_41 ELI_ARG((_TPPrule_41 _currn));
extern void _VS1rule_42 ELI_ARG((_TPPrule_42 _currn));
extern void _VS1rule_43 ELI_ARG((_TPPrule_43 _currn));
extern void _VS2rule_43 ELI_ARG((_TPPrule_43 _currn));
extern void _VS3rule_43 ELI_ARG((_TPPrule_43 _currn));
extern void _VS1rule_44 ELI_ARG((_TPPrule_44 _currn));
extern void _VS2rule_44 ELI_ARG((_TPPrule_44 _currn));
extern void _VS3rule_44 ELI_ARG((_TPPrule_44 _currn));
extern void _VS4rule_44 ELI_ARG((_TPPrule_44 _currn));
extern void _VS1rule_45 ELI_ARG((_TPPrule_45 _currn));
extern void _VS1rule_46 ELI_ARG((_TPPrule_46 _currn));
#define _VS2rule_46 _VS0Empty

extern void _VS3rule_46 ELI_ARG((_TPPrule_46 _currn));
extern void _VS4rule_46 ELI_ARG((_TPPrule_46 _currn,PExpr* _AS0repr));
extern void _VS1rule_47 ELI_ARG((_TPPrule_47 _currn));
extern void _VS2rule_47 ELI_ARG((_TPPrule_47 _currn));
extern void _VS3rule_47 ELI_ARG((_TPPrule_47 _currn));
extern void _VS4rule_47 ELI_ARG((_TPPrule_47 _currn,PExpr* _AS0repr));
extern void _VS1rule_48 ELI_ARG((_TPPrule_48 _currn));
extern void _VS2rule_48 ELI_ARG((_TPPrule_48 _currn));
extern void _VS3rule_48 ELI_ARG((_TPPrule_48 _currn));
extern void _VS4rule_48 ELI_ARG((_TPPrule_48 _currn));
extern void _VS5rule_48 ELI_ARG((_TPPrule_48 _currn));
extern void _VS1rule_49 ELI_ARG((_TPPrule_49 _currn));
extern void _VS2rule_49 ELI_ARG((_TPPrule_49 _currn));
extern void _VS3rule_49 ELI_ARG((_TPPrule_49 _currn));
extern void _VS1rule_50 ELI_ARG((_TPPrule_50 _currn));
extern void _VS2rule_50 ELI_ARG((_TPPrule_50 _currn));
extern void _VS3rule_50 ELI_ARG((_TPPrule_50 _currn));
extern void _VS4rule_50 ELI_ARG((_TPPrule_50 _currn));
extern void _VS1rule_51 ELI_ARG((_TPPrule_51 _currn));
extern void _VS2rule_51 ELI_ARG((_TPPrule_51 _currn));
#define _VS3rule_51 _VS0Empty

extern void _VS4rule_51 ELI_ARG((_TPPrule_51 _currn));
extern void _VS1rule_52 ELI_ARG((_TPPrule_52 _currn));
extern void _VS2rule_52 ELI_ARG((_TPPrule_52 _currn));
extern void _VS3rule_52 ELI_ARG((_TPPrule_52 _currn));
extern void _VS4rule_52 ELI_ARG((_TPPrule_52 _currn));
extern void _VS1rule_53 ELI_ARG((_TPPrule_53 _currn));
#define _VS2rule_53 _VS0Empty

extern void _VS1rule_54 ELI_ARG((_TPPrule_54 _currn));
extern void _VS2rule_54 ELI_ARG((_TPPrule_54 _currn));
extern void _VS1rule_55 ELI_ARG((_TPPrule_55 _currn));
extern void _VS2rule_55 ELI_ARG((_TPPrule_55 _currn));
extern void _VS3rule_55 ELI_ARG((_TPPrule_55 _currn));
extern void _VS4rule_55 ELI_ARG((_TPPrule_55 _currn));
extern void _VS5rule_55 ELI_ARG((_TPPrule_55 _currn));
extern void _VS1rule_56 ELI_ARG((_TPPrule_56 _currn));
extern void _VS2rule_56 ELI_ARG((_TPPrule_56 _currn));
extern void _VS3rule_56 ELI_ARG((_TPPrule_56 _currn));
extern void _VS4rule_56 ELI_ARG((_TPPrule_56 _currn));
extern void _VS1rule_57 ELI_ARG((_TPPrule_57 _currn));
extern void _VS1rule_58 ELI_ARG((_TPPrule_58 _currn));
extern void _VS2rule_58 ELI_ARG((_TPPrule_58 _currn));
extern void _VS3rule_58 ELI_ARG((_TPPrule_58 _currn));
extern void _VS1rule_59 ELI_ARG((_TPPrule_59 _currn));
extern void _VS2rule_59 ELI_ARG((_TPPrule_59 _currn));
extern void _VS3rule_59 ELI_ARG((_TPPrule_59 _currn));
extern void _VS4rule_59 ELI_ARG((_TPPrule_59 _currn));
extern void _VS5rule_59 ELI_ARG((_TPPrule_59 _currn));
extern void _VS1rule_60 ELI_ARG((_TPPrule_60 _currn));
extern void _VS2rule_60 ELI_ARG((_TPPrule_60 _currn));
extern void _VS3rule_60 ELI_ARG((_TPPrule_60 _currn));
extern void _VS4rule_60 ELI_ARG((_TPPrule_60 _currn));
extern void _VS1rule_61 ELI_ARG((_TPPrule_61 _currn));
extern void _VS1rule_62 ELI_ARG((_TPPrule_62 _currn));
extern void _VS1rule_63 ELI_ARG((_TPPrule_63 _currn));
extern void _VS2rule_63 ELI_ARG((_TPPrule_63 _currn));
extern void _VS3rule_63 ELI_ARG((_TPPrule_63 _currn));
extern void _VS1rule_075 ELI_ARG((_TPPrule_075 _currn));
#define _VS2rule_075 _VS0Empty

extern void _VS1rule_074 ELI_ARG((_TPPrule_074 _currn));
#define _VS2rule_074 _VS0Empty

#define _VS3rule_074 _VS0Empty

#define _VS4rule_074 _VS0Empty

extern void _VS1rule_073 ELI_ARG((_TPPrule_073 _currn));
extern void _VS2rule_073 ELI_ARG((_TPPrule_073 _currn));
#define _VS3rule_073 _VS0Empty

extern void _VS4rule_073 ELI_ARG((_TPPrule_073 _currn));
extern void _VS1rule_072 ELI_ARG((_TPPrule_072 _currn));
extern void _VS2rule_072 ELI_ARG((_TPPrule_072 _currn));
extern void _VS3rule_072 ELI_ARG((_TPPrule_072 _currn));
extern void _VS4rule_072 ELI_ARG((_TPPrule_072 _currn));
#define _VS1rule_071 _VS0Empty

#define _VS2rule_071 _VS0Empty

#define _VS3rule_071 _VS0Empty

#define _VS4rule_071 _VS0Empty

extern void _VS1rule_070 ELI_ARG((_TPPrule_070 _currn));
extern void _VS2rule_070 ELI_ARG((_TPPrule_070 _currn));
extern void _VS3rule_070 ELI_ARG((_TPPrule_070 _currn));
extern void _VS1rule_069 ELI_ARG((_TPPrule_069 _currn));
#define _VS2rule_069 _VS0Empty

extern void _VS3rule_069 ELI_ARG((_TPPrule_069 _currn));
extern void _VS4rule_069 ELI_ARG((_TPPrule_069 _currn));
extern void _VS1rule_068 ELI_ARG((_TPPrule_068 _currn));
extern void _VS1rule_067 ELI_ARG((_TPPrule_067 _currn));
extern void _VS2rule_067 ELI_ARG((_TPPrule_067 _currn));
#define _VS3rule_067 _VS0Empty

extern void _VS1rule_066 ELI_ARG((_TPPrule_066 _currn));
extern void _VS2rule_066 ELI_ARG((_TPPrule_066 _currn));
extern void _VS3rule_066 ELI_ARG((_TPPrule_066 _currn));
extern void _VS1rule_065 ELI_ARG((_TPPrule_065 _currn));
extern void _VS2rule_065 ELI_ARG((_TPPrule_065 _currn));
extern void _VS3rule_065 ELI_ARG((_TPPrule_065 _currn));
extern void _VS4rule_065 ELI_ARG((_TPPrule_065 _currn));
extern void _VS5rule_065 ELI_ARG((_TPPrule_065 _currn));
#define _VS1rule_064 _VS0Empty

#define _VS2rule_064 _VS0Empty

extern void _VS3rule_064 ELI_ARG((_TPPrule_064 _currn));
#define _VS1rule_063 _VS1rule_18

#define _VS2rule_063 _VS2rule_18

#define _VS3rule_063 _VS0Empty

#define _VS4rule_063 _VS0Empty

extern void _VS5rule_063 ELI_ARG((_TPPrule_063 _currn));
#define _VS1rule_062 _VS1rule_18

#define _VS2rule_062 _VS2rule_18

extern void _VS3rule_062 ELI_ARG((_TPPrule_062 _currn));
#define _VS4rule_062 _VS4rule_24

extern void _VS5rule_062 ELI_ARG((_TPPrule_062 _currn));
#define _VS1rule_061 _VS1rule_18

#define _VS2rule_061 _VS2rule_18

#define _VS3rule_061 _VS3rule_062

#define _VS4rule_061 _VS0Empty

extern void _VS5rule_061 ELI_ARG((_TPPrule_061 _currn));
#define _VS1rule_060 _VS1rule_072

#define _VS2rule_060 _VS2rule_072

#define _VS3rule_060 _VS3rule_072

#define _VS4rule_060 _VS4rule_072

extern void _VS5rule_060 ELI_ARG((_TPPrule_060 _currn));
#define _VS1rule_059 _VS0Empty

#define _VS2rule_059 _VS0Empty

#define _VS3rule_059 _VS0Empty

#define _VS4rule_059 _VS0Empty

extern void _VS5rule_059 ELI_ARG((_TPPrule_059 _currn));
#define _VS1rule_058 _VS1rule_18

#define _VS2rule_058 _VS2rule_18

#define _VS3rule_058 _VS3rule_062

#define _VS4rule_058 _VS4rule_24

extern void _VS5rule_058 ELI_ARG((_TPPrule_058 _currn));
#define _VS1rule_057 _VS1rule_18

#define _VS2rule_057 _VS0Empty

#define _VS3rule_057 _VS2rule_18

#define _VS4rule_057 _VS0Empty

extern void _VS5rule_057 ELI_ARG((_TPPrule_057 _currn));
#define _VS1rule_056 _VS1rule_18

#define _VS2rule_056 _VS0Empty

#define _VS3rule_056 _VS2rule_18

#define _VS4rule_056 _VS0Empty

extern void _VS5rule_056 ELI_ARG((_TPPrule_056 _currn));
#define _VS1rule_055 _VS1rule_072

#define _VS2rule_055 _VS2rule_072

#define _VS3rule_055 _VS3rule_072

#define _VS1rule_054 _VS1rule_18

#define _VS2rule_054 _VS2rule_18

#define _VS3rule_054 _VS3rule_062

#define _VS1rule_053 _VS1rule_18

#define _VS2rule_053 _VS2rule_18

#define _VS3rule_053 _VS3rule_062

#define _VS1rule_052 _VS1rule_18

#define _VS2rule_052 _VS2rule_18

#define _VS3rule_052 _VS3rule_062

#define _VS1rule_051 _VS0Empty

#define _VS2rule_051 _VS0Empty

#define _VS3rule_051 _VS0Empty

extern void _VS1rule_050 ELI_ARG((_TPPrule_050 _currn));
#define _VS2rule_050 _VS0Empty

#define _VS1rule_049 _VS1rule_18

#define _VS2rule_049 _VS2rule_18

#define _VS3rule_049 _VS3rule_062

extern void _VS4rule_049 ELI_ARG((_TPPrule_049 _currn));
extern void _VS5rule_049 ELI_ARG((_TPPrule_049 _currn));
#define _VS1rule_048 _VS1rule_18

#define _VS2rule_048 _VS2rule_18

#define _VS3rule_048 _VS3rule_062

extern void _VS4rule_048 ELI_ARG((_TPPrule_048 _currn));
#define _VS5rule_048 _VS5rule_049

#define _VS1rule_047 _VS1rule_072

#define _VS2rule_047 _VS2rule_072

#define _VS3rule_047 _VS3rule_072

#define _VS4rule_047 _VS4rule_072

extern void _VS5rule_047 ELI_ARG((_TPPrule_047 _currn));
#define _VS1rule_046 _VS1rule_18

#define _VS2rule_046 _VS2rule_18

#define _VS3rule_046 _VS3rule_062

#define _VS4rule_046 _VS4rule_24

#define _VS5rule_046 _VS5rule_049

extern void _VS1rule_045 ELI_ARG((_TPPrule_045 _currn));
extern void _VS2rule_045 ELI_ARG((_TPPrule_045 _currn));
extern void _VS3rule_045 ELI_ARG((_TPPrule_045 _currn));
extern void _VS4rule_045 ELI_ARG((_TPPrule_045 _currn));
extern void _VS1rule_044 ELI_ARG((_TPPrule_044 _currn));
extern void _VS2rule_044 ELI_ARG((_TPPrule_044 _currn));
extern void _VS3rule_044 ELI_ARG((_TPPrule_044 _currn));
extern void _VS4rule_044 ELI_ARG((_TPPrule_044 _currn));
#define _VS1rule_043 _VS1rule_072

#define _VS2rule_043 _VS2rule_072

#define _VS3rule_043 _VS3rule_072

extern void _VS4rule_043 ELI_ARG((_TPPrule_043 _currn));
#define _VS1rule_042 _VS1rule_18

#define _VS2rule_042 _VS2rule_18

#define _VS3rule_042 _VS3rule_062

extern void _VS4rule_042 ELI_ARG((_TPPrule_042 _currn));
#define _VS1rule_041 _VS1rule_18

#define _VS2rule_041 _VS2rule_18

#define _VS3rule_041 _VS3rule_062

extern void _VS4rule_041 ELI_ARG((_TPPrule_041 _currn,PExprList* _AS0_PExprauxList));
#define _VS1rule_040 _VS0Empty

#define _VS2rule_040 _VS0Empty

#define _VS3rule_040 _VS0Empty

extern void _VS4rule_040 ELI_ARG((_TPPrule_040 _currn,PExprList* _AS0_PExprauxList));
#define _VS1rule_039 _VS1rule_18

#define _VS2rule_039 _VS2rule_18

#define _VS3rule_039 _VS3rule_062

extern void _VS4rule_039 ELI_ARG((_TPPrule_039 _currn,PExprListPtr* _AS0_cPExprListPtr_pre));
#define _VS1rule_038 _VS1rule_072

#define _VS2rule_038 _VS2rule_072

#define _VS3rule_038 _VS3rule_072

#define _VS4rule_038 _VS4rule_072

#define _VS1rule_037 _VS1rule_18

#define _VS2rule_037 _VS2rule_18

#define _VS3rule_037 _VS3rule_062

#define _VS4rule_037 _VS4rule_24

#define _VS1rule_036 _VS1rule_18

#define _VS2rule_036 _VS2rule_18

#define _VS3rule_036 _VS3rule_062

#define _VS4rule_036 _VS4rule_24

#define _VS1rule_035 _VS0Empty

#define _VS2rule_035 _VS0Empty

#define _VS3rule_035 _VS0Empty

#define _VS4rule_035 _VS0Empty

extern void _VS1rule_034 ELI_ARG((_TPPrule_034 _currn));
extern void _VS2rule_034 ELI_ARG((_TPPrule_034 _currn));
extern void _VS3rule_034 ELI_ARG((_TPPrule_034 _currn));
extern void _VS1rule_033 ELI_ARG((_TPPrule_033 _currn));
extern void _VS2rule_033 ELI_ARG((_TPPrule_033 _currn));
extern void _VS3rule_033 ELI_ARG((_TPPrule_033 _currn));
#define _VS1rule_032 _VS1rule_18

#define _VS2rule_032 _VS2rule_18

#define _VS3rule_032 _VS3rule_062

#define _VS4rule_032 _VS4rule_24

#define _VS1rule_031 _VS1rule_18

#define _VS2rule_031 _VS2rule_18

#define _VS3rule_031 _VS3rule_062

#define _VS4rule_031 _VS4rule_24

#define _VS1rule_030 _VS1rule_18

#define _VS2rule_030 _VS2rule_18

#define _VS3rule_030 _VS3rule_062

extern void _VS4rule_030 ELI_ARG((_TPPrule_030 _currn));
#define _VS1rule_029 _VS1rule_18

#define _VS2rule_029 _VS2rule_18

#define _VS3rule_029 _VS3rule_062

extern void _VS4rule_029 ELI_ARG((_TPPrule_029 _currn));
#define _VS1rule_028 _VS1rule_072

#define _VS2rule_028 _VS2rule_072

#define _VS3rule_028 _VS3rule_072

#define _VS4rule_028 _VS4rule_072

#define _VS1rule_027 _VS1rule_18

#define _VS2rule_027 _VS2rule_18

#define _VS3rule_027 _VS3rule_062

#define _VS4rule_027 _VS4rule_24

#define _VS1rule_026 _VS1rule_18

#define _VS2rule_026 _VS0Empty

#define _VS3rule_026 _VS0Empty

extern void _VS4rule_026 ELI_ARG((_TPPrule_026 _currn));
#define _VS1rule_025 _VS1rule_18

#define _VS2rule_025 _VS2rule_18

#define _VS3rule_025 _VS3rule_062

extern void _VS4rule_025 ELI_ARG((_TPPrule_025 _currn));
#define _VS1rule_024 _VS1rule_18

#define _VS2rule_024 _VS2rule_18

extern void _VS3rule_024 ELI_ARG((_TPPrule_024 _currn));
extern void _VS4rule_024 ELI_ARG((_TPPrule_024 _currn));
#define _VS1rule_023 _VS1rule_072

#define _VS2rule_023 _VS2rule_072

#define _VS3rule_023 _VS3rule_072

#define _VS4rule_023 _VS4rule_072

extern void _VS5rule_023 ELI_ARG((_TPPrule_023 _currn));
#define _VS1rule_022 _VS0Empty

#define _VS2rule_022 _VS0Empty

#define _VS3rule_022 _VS0Empty

#define _VS4rule_022 _VS0Empty

extern void _VS5rule_022 ELI_ARG((_TPPrule_022 _currn));
#define _VS1rule_021 _VS1rule_18

#define _VS2rule_021 _VS2rule_18

extern void _VS3rule_021 ELI_ARG((_TPPrule_021 _currn));
#define _VS4rule_021 _VS4rule_24

extern void _VS5rule_021 ELI_ARG((_TPPrule_021 _currn,PTGNode* _AS0_const20));
#define _VS1rule_020 _VS1rule_18

#define _VS2rule_020 _VS2rule_18

#define _VS3rule_020 _VS3rule_062

#define _VS4rule_020 _VS4rule_24

extern void _VS5rule_020 ELI_ARG((_TPPrule_020 _currn));
#define _VS1rule_019 _VS0Empty

#define _VS2rule_019 _VS0Empty

#define _VS3rule_019 _VS0Empty

#define _VS4rule_019 _VS0Empty

extern void _VS5rule_019 ELI_ARG((_TPPrule_019 _currn));
#define _VS1rule_018 _VS1rule_072

#define _VS2rule_018 _VS2rule_072

#define _VS1rule_017 _VS1rule_18

#define _VS2rule_017 _VS2rule_18

extern void _VS1rule_016 ELI_ARG((_TPPrule_016 _currn));
extern void _VS2rule_016 ELI_ARG((_TPPrule_016 _currn));
#define _VS1rule_015 _VS1rule_072

#define _VS2rule_015 _VS2rule_072

#define _VS1rule_014 _VS1rule_18

#define _VS2rule_014 _VS2rule_18

extern void _VS1rule_013 ELI_ARG((_TPPrule_013 _currn));
extern void _VS2rule_013 ELI_ARG((_TPPrule_013 _currn));
#define _VS1rule_012 _VS1rule_18

#define _VS2rule_012 _VS2rule_18

#define _VS3rule_012 _VS3rule_062

#define _VS1rule_011 _VS0Empty

#define _VS2rule_011 _VS0Empty

#define _VS3rule_011 _VS0Empty

#define _VS1rule_010 _VS1rule_072

#define _VS2rule_010 _VS2rule_072

#define _VS3rule_010 _VS3rule_072

#define _VS1rule_09 _VS1rule_18

#define _VS2rule_09 _VS2rule_18

#define _VS3rule_09 _VS3rule_062

#define _VS1rule_08 _VS1rule_072

#define _VS1rule_07 _VS1rule_18

extern void _VS1rule_06 ELI_ARG((_TPPrule_06 _currn));
extern void _VS1rule_05 ELI_ARG((_TPPrule_05 _currn));
extern void _VS2rule_05 ELI_ARG((_TPPrule_05 _currn));
extern void _VS3rule_05 ELI_ARG((_TPPrule_05 _currn));
extern void _VS4rule_05 ELI_ARG((_TPPrule_05 _currn));
extern void _VS5rule_05 ELI_ARG((_TPPrule_05 _currn));
#define _VS1rule_04 _VS1rule_072

#define _VS2rule_04 _VS2rule_072

#define _VS3rule_04 _VS3rule_072

#define _VS1rule_03 _VS1rule_18

#define _VS2rule_03 _VS2rule_18

#define _VS3rule_03 _VS3rule_062

#define _VS1rule_02 _VS0Empty

#define _VS2rule_02 _VS0Empty

#define _VS3rule_02 _VS0Empty

extern void _VS1rule_01 ELI_ARG((_TPPrule_01 _currn));
#endif
