#!/bin/sh
# Copyright 2014, The Regents of the University of Colorado

# This file is part of the Eli translator construction system.

# Eli is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2, or (at your option) any later
# version.

# Eli is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License along
# with Eli; see the file COPYING.  If not, write to the Free Software
# Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

# EXEC (lldb.sh) (:sources :dir_of :vir_dir)
#      (:sources +incl=(:includes) :map=:incl.all :union :dir_of :vir_dir)
#      (:exe) (+core)
#   NEEDS (:sources) (:sources +incl=(:includes) :map=:incl.all :union)
#   => (:lldb);

echo '#! /bin/sh' > lldb
echo '${ELI_DEBUGGER-lldb}' $3 >> lldb
echo 'exit 0' >> lldb

chmod +x lldb

exit 0
