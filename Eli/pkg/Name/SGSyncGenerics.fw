@=~
~p typesetter = texinfo
~p maximum_input_line_length = infinity

These specifications are common for all instances of ScopeGraphs.

Phase order:
============

The computations of this file synchronize the global execution phases for all
instances of ScopeGraphs:

Any computation which is to be executed before name analysis begins
should specify the accumulating postcondition ROOTCLASS.beforeNameAnalysis.

The global timer beforeTimeGlob is started at the end of initAllScopeGraphsInstances.
ROOTCLASS.AllScopeGraphsInitialized is the precondition for the phase of
graph construction.
Each ScopeGraphs instance contributes postconditions of scope graph construction
to the accumulating attribute ROOTCLASS.ReadyForFPCalls.
If there are calls of FP-functions in user computations, they have to depend on
it and contribute a dependence to the accumulating attribute ROOTCLASS.GotAllFPCalls.

The worklist phase and the oracle phase are performed by calls of the
function FPSolveItems. For each instance of ScopeGraphs a call of FPSolveItems
is executed for each node that has the role |NAME|OutSideInDeps.
At least |NAME|RootScope has that role.
The postcondition of an FPSolveItems call is |NAME|OutSideInDeps.|NAME|FPSolved,
i.e. all edge tips are bound and all edges are created.
From then on the computations may use the completed graph, in particular the
GC lookup functions.
It may be necessary to schedule computations which depend on the completed graph
(|NAME|OutSideInDeps.|NAME|FPSolved), and provide a precondition for GC lookup
(the accumulating attribute |NAME|OutSideInDeps.|NAME|BindTypeNames).
For example, computation of an order of classes for disambiguation of inherited
bindings in the presence of multiple inheritance.

The GC phase is decomposed in a sub-phase where first the type names are looked up,
then the types of typed entities are set (|NAME|SetTypeOfEntity.|NAME|TypeIsSet),
and finally all other names are bound, in particular those which rely on typed
qualifiers. Then the postcondition |NAME|OutSideInDeps.|NAME|GotAllBindings
is established.

If there is a nested node with the role |NAME|OutSideInDeps, its |NAME|OutSideEdge
is created, then the |NAME|OutSideInDeps computations for that subtree
are scheduled recursively beginning with the phase of FP function calls.
(The scope graph without inheritance edges has been constructed for all subtrees
in the first global phase.)

The time measurement for the calls of FP functions in the |NAME|RootScope context
of all instances is synchronized and accumulated. The timing of FP calls at
inner nodes is not measured separatly.
The time measurement for one ScopeGraphs instance accumulates all worklist time
and all oracle time for all calls of FPSolveItems in that instance.


ROOTCLASS.AllScopeGraphsInstancesDone indicates that all instances have bound all their
GeneralNames. doneAllScopeGraphsInstances () stops the global timer, computes the
for complete name analysis, and sets the precondition for post-processing,
i.e. output of measured values by finalizeAllScopeGraphsInstances().


~O~<SyncGenerics.lido~>==~{
CLASS SYMBOL ROOTCLASS COMPUTE
   SYNT.beforeNameAnalysis += "yes";
   
   /* precondition for each name analysis instance: */
   SYNT.AllScopeGraphsInitialized = initAllScopeGraphsInstances ()
        <- SYNT.beforeNameAnalysis;

   /* Synchronizes FPSolveItems calls for all instances on root level only;
      needed for timing: */
   SYNT.StartFPCalls = allInstancesStartFPCalls ()
      <- (SYNT.ReadyForFPCalls, SYNT.AllScopeGraphsInitialized);

   SYNT.GotAllFPCalls += "yes";
   
   SYNT.StartFPSolveItems += allInstancesFPCallsDone ()
      <- (SYNT.GotAllFPCalls, SYNT.StartFPCalls);

/* Finalization: */
   /* postcondition established by completion of each name analysis instance: */
   SYNT.AllScopeGraphsInstancesDone += "yes";

   /* postcondition for postprocessing: */
   SYNT.PostProcessAllScopeGraphsInstances = doneAllScopeGraphsInstances ()
      <- SYNT.AllScopeGraphsInstancesDone;

   /* postcondition established by finalization of each name analysis instance: */

   SYNT.AllScopeGraphsInstancesFinalized += SYNT.PostProcessAllScopeGraphsInstances;
   finalizeAllScopeGraphsInstances () <- SYNT.AllScopeGraphsInstancesFinalized;
END;
~}

Properties common for all instances of ScopeGraphs:
The properties are set by NABindIdn ord by addEdge:
~O~<EntityProps.pdl~>~{~-
Sym: int [KReset];
Coord: CoordPtr [KReset]; "err.h"
BoundInNode: ScopeNodePtr [KReset]; "ModelExport.h"
BoundInNodeTuple: NodeTuplePtr [KReset]; "ModelExport.h"
GraphIndex: int [KReset];

/* properties set for each edge key: */
TailNodeTuple: NodeTuplePtr; "ModelExport.h"
TipNodeTuple:  NodeTuplePtr; "ModelExport.h"
EdgeLabel:     int;

TypeOfNotYetSet -> Coord = {NullCoordRef};

~}

Entities as modules or struct types may have a property OwnedNodeTuple,
which is an n-tuple of scope nodes:
~O~<NameAna.pdl~>~{~-
OwnedNodeTuple: NodeTuplePtr [KReset, VReset]; "ModelExport.h"

errorImportCollides;
  /* an imported definition collides with a local definition */
OpenEdgeCode;
WrongQualifier;
  /* avalanche error; has been indicated earlier */
~}
