# Copyright (c) 1999 Anthony M.Sloane 
# envmod.tcl
# Monitoring support for envmod.

# This file is part of the Eli translator construction system. 

# Eli is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2, or (at your option) any later
# version.

# Eli is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License along
# with Eli; see the file COPYING.  If not, write to the Free Software
# Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

set n(SGEnvironment,desc) "Identifier scoping environment"

proc n_SGEnvironment_open {text env} {
    n_say "$text"
    if {$env == 0} {
	n_say "\n  NoEnv\n"
    } else {
	set inh 0
	set env [n_hextodec $env]
	set r [n_send get_scope_info $env]
	if {[lindex $r 0] != 0} {
	    n_say " (parent: "
	    n_SGEnvironment_say [lindex $r 0]
	    n_say ")"
	}
	n_say "\n"
	if {[lindex $r 1] != 0} {
	    n_say "  Associated key: "
	    n_DefTableKey_say [lindex $r 1]
	    n_say "\n"
	}
	if {[lindex $r 2] != 0} {
	    set inh [lindex $r 2]
	}
	set r [lreplace $r 0 2]
	set c 0
	foreach {i j} $r {
	    n_say "  "
	    n_say_val DefTableKey $j
	    set s [n_send get_string $i]
	    if {[string compare "{*** Illegal string table index ***}" $s] == 0} {
		n_say " int:$i\n"
	    } else {
		n_say " $s\n"
	    }
	    incr c
	}
	if {$c == 0} {
	    n_say "  No bindings\n"
	}
	if {$inh != 0} {
	    n_say "  Inherits from"
	    set r [n_send get_inheritance_info $inh]
	    set c 0
	    foreach {i j} $r {
		n_say "\n    "
		n_say_val SGInheritPtr $j
		n_say " (label [expr $i + 1])"
		incr c
	    }
	    if {$c == 0} {
		n_say " nothing\n"
	    }
	    n_say "\n"
	}
    }
}

proc n_SGEnvironment_say {env} {
    n_say "SGEnvironment:0x[n_dectohex $env]"
}

set n(SGBinding,desc) "Identifier binding"

proc n_Binding_open {text bind} {
    n_say "$text"
    if {$bind == 0} {
	n_say "\n  SGNoBinding\n"
    } else {
	set bind [n_hextodec $bind]
	set r [n_send get_binding_info $bind]
	n_say "\n  IdnOf = "
	set s [n_send get_string [lindex $r 0]]
	if {[string compare "{*** Illegal string table index ***}" $s] == 0} {
	    n_say " int:[lindex $p 0]"
	} else {
	    n_say " $s"
	}
	n_say "\n  KeyOf = "
	n_say_val DefTableKey [lindex $r 1]
	n_say "\n  EnvOf = "
	n_SGEnvironment_say [lindex $r 2]
	n_say "\n"
    }
}

proc n_SGBinding_say {bind} {
    n_say "SGBinding:0x[n_dectohex $bind]"
}

set n(SGInheritPtr,desc) "List of superclasses"

proc n_SGInheritPtr_open {text cl} {
    n_say "$text\n"
    if {$cl == 0} {
	n_say "  No superclasses\n"
    } else {
	set cl [n_hextodec $cl]
	set r [n_send get_super_info $cl]
	foreach {i} $r {
	    n_say "  "
	    n_SGEnvironment_say $i
	    n_say "\n"
	}
    }
}

proc n_SGInheritPtr_say {cl} {
    n_say "SGInheritPtr:0x[n_dectohex $cl]"
}
