set n(ScopeNodePtr,desc) "Scope node pointer"

proc n_ScopeNodePtr_say {ptr} {
  n_say "ScopeNodePtr:0x[n_dectohex $ptr]"
}

proc n_ScopeNodePtr_open {text ptr} {
  n_say "$text"
  if {$ptr == 0} {
    n_say "\n  NoScopeNode\n"
  } else {
    n_say "\n"
    set ptr [n_hextodec $ptr]
    set r [n_send get_ScopeNode_info $ptr]
    n_say "  scope: "
    n_say_val SGEnvironment [lindex $r 0]
    n_say "\n"
    n_say "  parent: "
    n_say_val ScopeNodePtr [lindex $r 1]
    n_say "\n"
    n_say "  edges: "
    n_say_val EdgePtrList [lindex $r 2]
    n_say "\n"
    n_say "  owner: "
    n_say_val DefTableKey [lindex $r 3]
    n_say "\n"
    n_say "  line: [lindex $r 4]\n"
    n_say "\n"
  }
}

set n(NodeTuplePtr,desc) "node tuple"

proc n_NodeTuplePtr_say {ptr} {
  n_say "NodeTuplePtr:0x[n_dectohex $ptr]"
}

proc n_NodeTuplePtr_open {text ptr} {
  n_say "$text"
  if {$ptr == 0} {
    n_say "\n  NoNodeTuple\n"
  } else {
    n_say "\n"
    set ptr [n_hextodec $ptr]
    set r [n_send get_NodeTuple_info $ptr]
    set j 0
    foreach {i} $r {
      n_say "  node\[$j\]: "
      n_say_val ScopeNodePtr $i
      n_say "\n"
      incr j
    }
  }
}

set n(EdgePtr,desc) "Inheritance edge pointer"

proc n_EdgePtr_say {ptr} {
  n_say "EdgePtr:0x[n_dectohex $ptr]"
}

proc n_EdgePtr_open {text ptr} {
  n_say "$text"
  if {$ptr == 0} {
    n_say "\n  No Edge\n"
  } else {
    n_say "\n"
    set ptr [n_hextodec $ptr]
    set r [n_send get_Edge_info $ptr]
    n_say "  label: [lindex $r 0]\n"
    n_say "  active: [lindex $r 1]\n"
    n_say "  scn: "
    n_say_val ScopeNodePtr [lindex $r 2]
    n_say "\n"
  }
}

set n(FPItemPtr,desc) "Fixed-point operation pointer"

proc n_FPItemPtr_say {ptr} {
  n_say "FPItemPtr:0x[n_dectohex $ptr]"
}

proc n_FPItemPtr_open {text ptr} {
  n_say "$text"
  if {$ptr == 0} {
    n_say "\n  No FPItem\n"
  } else {
    n_say "\n"
    set ptr [n_hextodec $ptr]
    set r [n_send get_Item_info $ptr]
    set o [list "0" \
                "FPOprPlainIdent" \
                "FPOprQualIdent" \
		"3" \
		"FPOprGetType" \
		"FPOprAddEdge" \
		"FPOprInsertDef"]
    n_say "  id: [lindex $r 0]\n"
    n_say "  done: [lindex $r 1]\n"
    n_say "  mark: [lindex $r 2]\n"
    n_say "  line: [lindex $r 3]\n"
    n_say "  useItem: "
    n_say_val FPItemPtr [lindex $r 4]
    n_say "\n"
    n_say "  next: "
    n_say_val FPItemPtr [lindex $r 5]
    n_say "\n"
    n_say "  fpOpr: [lindex $o [lindex $r 6]]\n"
    n_say "  nodes: "
    n_say_val NodeTuplePtr [lindex $r 7]
    n_say "\n"
    n_say "  fpGraphInd: [lindex $r 8]\n"
    n_say "  useKey: "
    n_say_val DefTableKey [lindex $r 9]
    n_say "\n"
    n_say "  key: "
    n_say_val DefTableKey [lindex $r 10]
    n_say "\n"
  }
}
