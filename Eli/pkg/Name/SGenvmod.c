/* Copyright 1997, The Regents of the University of Colorado */

/* This file is part of the Eli Module Library.

The Eli Module Library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

The Eli Module Library is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with the Eli Module Library; see the file COPYING.LIB.
If not, write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Eli into the
   directory resulting from a :source derivation, you may use that
   created file as a part of that directory without restriction. */

#include <stdlib.h>
#include "err.h"
#include "obstack.h"
#include "SGenvmod.h"
#ifndef NORESTORE
#include "obsave.h"
#endif
#ifdef MONITOR
#include "SGenvmod_dapto.h"
#endif

#include "LangSpecFct.h"
/* The functions of this interface file implement language specific
   extension points which are called by some lookup functions
*/


#ifdef TEST
#define TEST
/* detailed test output is produced */
#include <stdio.h>
#include "csm.h"
#include "err.h"
#include "ModelExport.h"
#include "pdl_gen.h"
#endif

#ifdef TIMEENV
/* time measure for calls of SGKeyInEnv and KeyInInhGraph 
   is taken and reported by printTime
*/
#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>
/* variables for timing measures: */

static struct rusage timeBeforeEnv, timeBeforePath, timeTmp;
static struct timeval diffTimeTmp, EnvTime, PathTime;

static int KeyInEnvCnt = 0;
static int KeyInInhGraphCnt = 0;
static long envPathTimeSec = 0, envPathTimeUsec = 0;
static long envTimeSec = 0, envTimeUsec = 0;

#define TIMEBEFORE(BEFORE)\
      getrusage(RUSAGE_SELF, &BEFORE);

#define TIMEAFTER(TIME,BEFORE)			\
      getrusage(RUSAGE_SELF, &timeTmp);\
      timersub(&timeTmp.ru_utime, &BEFORE.ru_utime, &diffTimeTmp);\
      TIME = diffTimeTmp;

static int MaxIdListLength = 0;
static int IdMaxIdListLength = 0;

void SGcountMaxIdListLength (SGEnvironment env) {
  /* on exit
       MaxIdListLength is set to the maximum of longest id list
       for an id in the class access mechanism of env and MaxIdListLength
  */
  int maxidn, labdx, lg, id;
  SGStkPtr lst;
  if (env == NoSGEnv) return;
  for (labdx = 0; labdx < MaxKindsPathEdge; labdx++) {
    maxidn = env->access->MaxClassIdn[labdx];
    for (id = 1; id < maxidn; id++) {
      lst = (((SGStkPtr *)obstack_base((env)->access->ClassIdnTbl[labdx]))[id]);
       lg = 0;
       while (lst)
	 { lg++; lst = lst->out;}
       if (lg > MaxIdListLength) {
	 MaxIdListLength = lg;
	 IdMaxIdListLength = id;
       }
    }
  }
}

void SGprintTime () {
  printf ("%s has maximal list length in path access %d\n",
	  StringTable (IdMaxIdListLength), MaxIdListLength);
  printf ("Calls of SGKeyInEnv      %d\n", SGKeyInEnvCnt);
  printf ("Calls of KeyInInhGraph %d\n", KeyInInhGraphCnt);
  printf("Time for calls of SGKeyInEnv %ld seconds + %ld microseconds\n",
         envTimeSec, envTimeUsec);
  printf("Time for calls of KeyInInhGraph %ld seconds + %ld microseconds\n",
         envPathTimeSec, envPathTimeUsec);
}
#endif

#ifdef PROTO_OK
SGBinding SGDefinitionsOf (SGEnvironment env)
  /* the sequence of bindings of the environment env */
#else
SGBinding SGDefinitionsOf (env) SGEnvironment env;
#endif
{ return (env ? env->relate: NoSGBinding); }

#ifdef PROTO_OK
SGBinding SGNextDefinition (SGBinding b)
  /* the next binding within the sequence of an environment */
#else
SGBinding SGNextDefinition (b) SGBinding b;
#endif
{ return (b ? b->nxt : NoSGBinding); }

#ifdef PROTO_OK
int SGIdnOf (SGBinding b)
  /* the identifier of a binding */
#else
int SGIdnOf (b) SGBinding b;
#endif
{ return (b ? b->idn : 0); }

#ifdef PROTO_OK
DefTableKey SGKeyOf (SGBinding b)
  /* the key of a binding */
#else
DefTableKey SGKeyOf (b) SGBinding b;
#endif
{ return (b ? b->key : NoKey); }

#ifdef PROTO_OK
SGEnvironment SGEnvOf (SGBinding b)
  /* the environment of a binding */
#else
SGEnvironment SGEnvOf (b) SGBinding b;
#endif
{ return (b ? b->env : NoSGEnv); }


#define NoClassNo -1
#define ClassVisited -2

static struct EnvmodData 
{
    SGStkPtr FreeStackElements;
    void *baseptr;
} state = {
    (SGStkPtr)0,
    (void *)0
};

static Obstack space;

#ifndef NORESTORE
/* Routine to register adresses of dynamically allocated obstacks */

static Obstack stack_adresses = /* Adresses of allocated obstacks, growing object */
   obstack_empty_chunk(4096, OBSTACK_PTR_ALIGN);

/* Obstacks must be allocated from outside of the 'space'-obstack, since normal
 * save/restore does not work for obstack-Pointers without memory leaks */

static Obstack stacks =         /* Dynamically allocated obstacks. */
   obstack_empty_chunk(4096, OBSTACK_PTR_ALIGN);

static ObstackP NEW_OBSTACK ELI_ARG((void))
{
    ObstackP obst = (ObstackP) obstack_alloc(&stacks, sizeof(Obstack));
    obstack_grow(&stack_adresses, &obst, sizeof(ObstackP));
    
    return obst;
}
#else
#define NEW_OBSTACK() (ObstackP)obstack_alloc(&space, sizeof(Obstack))
#endif 

#ifdef MONITOR
#define MON_BINDING(i,k,e) _dapto_binding_made (e, i, k)
#else
#define MON_BINDING(i,k,e) /* empty */
#endif

#define MAKE_BINDING(r,i,k,e) \
  { r = (SGBinding)obstack_alloc(&space, sizeof(struct _SGRelElt)); \
    r->nxt = e->relate; e->relate = r; r->env = e; r->key = k; r->idn = i; \
    MON_BINDING (i, k, e); }

#define CHANGE_KEY_IN_BINDING(r,k) r->key = k;

/* Macros to maintain the nesting access structure: */
#define IDN_STK(e,i) (((SGStkPtr *)obstack_base(((e)->access->IdnTbl)))[i])

#define IDN_ACCESSIBLE(i,e) \
  while ((i) >= (e)->access->MaxIdn) { \
    obstack_blank(((e)->access->IdnTbl), sizeof(SGStkPtr)); \
    IDN_STK((e), (e)->access->MaxIdn) = (SGStkPtr)0; \
    (e)->access->MaxIdn++; \
  }

#define PUSH_IDN_STK(i,e,r) \
  { SGStkPtr s = NewStkElt(); s->binding = (r); \
    s->out = IDN_STK((e), (i)); IDN_STK((e), (i)) = s; }

/* Macros to maintain the inheritance access structure: */

/* the array element which points to the binding list for identifier i
   in the class access mechanism of environment e for edges of kind l+1 
*/
#define CLASS_IDN_STK(e,i,l)					\
  (((SGStkPtr *)obstack_base((e)->access->ClassIdnTbl[l]))[i])

/* make sure that the class access mechanism of environment e for 
   edges of kind l+1 has an  array element for identifier i
*/
#define CLASS_ACCESSIBLE(i,e,l)		    \
  while ((i) >= (e)->access->MaxClassIdn[l]) { \
    obstack_blank((e)->access->ClassIdnTbl[l], sizeof(SGStkPtr)); \
    CLASS_IDN_STK((e), (e)->access->MaxClassIdn[l], l) = (SGStkPtr)0;	\
    env->access->MaxClassIdn[l]++; \
  }

/* inserts the binding r in the binding list for identifier i
   in the class access mechanism of environment e for edges of kind l+1
*/
#define INSERT_CLASS_IDN_STK(e,i,r,l)		     \
{ SGStkPtr s = NewStkElt(), after = (SGStkPtr)0, before; \
  int clno = (e)->classdescr->classno[l]; \
  CLASS_ACCESSIBLE ((i), (e), (l))	  \
  before = CLASS_IDN_STK((e), (i), (l));  \
  s->binding = r; \
  while (before && \
         before->binding->env->classdescr->classno[l] > (clno)) { \
    after = before; before = before->out; \
  } \
  s->out = before; \
  if (after) after->out = s; \
  else CLASS_IDN_STK((e), (i), (l)) = s;	\
}

static void
#ifdef PROTO_OK
InitSpace(void)
#else
InitSpace()
#endif
{ obstack_init(&space);
  state.baseptr = obstack_alloc(&space, 0);
}

static SGStkPtr
#ifdef PROTO_OK
NewStkElt(void)
#else
NewStkElt()
#endif
/* Obtain a new definition stack element
 *   On exit-
 *     NewStkElt points to an element with undefined contents
 **/
{ if (state.FreeStackElements) {
    SGStkPtr tmp = state.FreeStackElements;
    state.FreeStackElements = state.FreeStackElements->out; 
    return tmp;
  }
  return (SGStkPtr)obstack_alloc(&space, sizeof(struct _SGStkElt));
}

SGEnvironment
#ifdef PROTO_OK
NewSGEnv(void)
#else
NewSGEnv()
#endif
/* Establish a new environment
 *   On exit-
 *     NewSGEnv=new environment
 ***/
 { SGEnvironment e; int i;

  if (!state.baseptr) InitSpace();
  e = (SGEnvironment)obstack_alloc(&space, sizeof(struct _SGEnvImpl));
  e->relate = NoSGBinding; e->parent = (SGEnvironment)0; e->key = NoKey;
  e->level = 0; e->classdescr = (SGClassDescriptor)0; e->nested = 1;
  e->haveusedbindings = 0;
  e->access = (SGAccessMechanism)obstack_alloc(&space, sizeof(struct _SGAccessMechanism)); 
  e->access->IdnTbl = NEW_OBSTACK(); obstack_init((e->access->IdnTbl)); 
  e->access->MaxIdn = 0; e->access->CurrEnv = e;

  for (i = 0; i<MaxKindsPathEdge; i++) {
    e->access->ClassIdnTbl[i] = (ObstackP)0; e->access->MaxClassIdn[i] = 0;
    e->access->NextClassNo[i] = 0;
  }
  e->access->Classes = (SGClassDescriptor)0;

#ifdef MONITOR
  _dapto_env_created (e, NULL);
#endif

  return e;
}

static void
#ifdef PROTO_OK
EnterClassGraphs (SGClassDescriptor cld, int label)
#else
EnterClassGraphs(cld, label) SGClassDescriptor cld; int label;
#endif
/* Make the class access mechanism for graphs of edge kind label 
   reflect cld and all its super classes
 *   On entry-
 *     the class cld is not yet entered in the class access mechanism
 *     all inheritances (direct and indirect) for cld have been established
 *     the inheritance relation may be cyclic
 *   On exit-
 *     all bindings which are inherited along edges of kind label
 *       are entered in the access mechanism for label.
 */
{ SGInheritPtr inhlist; SGBinding r;
  SGEnvironment env = cld->env;
  int i;
   /* recursively enter all classes which cld inherits from via edges of kind label
      and which are not yet entered; 
      follow multiple inheritance edges in depth first order: 
   */
  if (cld->classno[label-1] !=  NoClassNo) return;
  if (cld->visiting) return;
  cld->visiting = 1; /* avoid to get caught in cycles 
                        we need not distinguish the labels, because
                        they are not visited interleaved
                     */
  
   for (inhlist = cld->inhlist[label-1]; inhlist; inhlist = inhlist->nxt) {
       if (inhlist->fromcl->classno[label-1] == NoClassNo) 
          EnterClassGraphs (inhlist->fromcl, label);
   }

  /* set a new class number; indicates that cld has been entered: */
  cld->classno[label-1] = (env->access->NextClassNo[label-1])++;
#ifdef TEST
  printf ("EnterClassGraphs label-1 = %d, number %d\n", label-1, cld->classno[label-1]);
#endif
  cld->visiting = 0; /* EnterClassGraphs will not revisit this node due to a 
                        cycle, because its classNo is set now */
  
  /* compute transitive inheritance sets: (c is in inhset of c!)*/
  cld->inhset[label-1] = AddElemToBitSet(cld->classno[label-1], NullBitSet);
     for (inhlist = cld->inhlist[label-1]; inhlist; inhlist = inhlist->nxt) {
        (void)UnionToBitSet(cld->inhset[label-1], inhlist->fromcl->inhset[label-1]);
     }
     /* if the graph is acyclic, the inheritance sets are transitively closed;
        for cyclic graphs they will be closed by sweeps before
        the access mechanism is used in Phase 4
     */

  /* push all bindings of cld->env on their idn stacks: */
  for (r = env->relate; r; r = r->nxt) {
      INSERT_CLASS_IDN_STK(env,r->idn,r,label-1)
#ifdef TEST
      printf (" inserted %s into graph kind %d; scope of %s\n",
	      StringTable (r->idn), label-1, 
	      StringTable(GetSym(GetNodeOwnerKey(GetBoundIn(SGKeyOf(r),NoScopeNode)),0)));
#endif
/*
    SGStkPtr s = NewStkElt();
    s->binding = r; 
    CLASS_ACCESSIBLE(r->idn, env, label-1)
      s->out = CLASS_IDN_STK(env, r->idn, label-1); 
    CLASS_IDN_STK(env, r->idn, label-1) = s;
*/
  }/* for bindings */
#ifdef TEST
  printf ("end EnterClassGraphs label %d, number %d\n", label-1, cld->classno[label-1]);
#endif
}/* EnterClassGraphs */

void
#ifdef PROTO_OK
SGEnterClasses(SGClassDescriptor cld)
#else
SGEnterClasses(cld) SGClassDescriptor cld;
#endif
/* Make the class access mechanism reflect cld and all its super classes
 *   On entry-
 *     the class cld is not yet entered in the class access mechanism
 *     all inheritances for cld have been established
 *     the inheritance relation may be cyclic
 */
{ int label;

  for (label = 1; label <= MaxKindsPathEdge; label++) 
    EnterClassGraphs (cld, label);

}/* SGEnterClasses */

static int doenterclasses = 0;

static void
#ifdef PROTO_OK
EnterEnv(SGEnvironment env)
#else
EnterEnv(env) SGEnvironment env;
#endif
/* Make the state of the array reflect env
 *   On entry-
 *     The access of the array reflects the parent of env
 **/
{ SGBinding r;

  for (r = env->relate; r;  r = r->nxt) {
    IDN_ACCESSIBLE(r->idn, env)
    PUSH_IDN_STK(r->idn, env, r)
  }
  env->nested = 1;
  env->access->CurrEnv = env;
  if (doenterclasses) env->haveusedbindings = 1;
  if (env->classdescr &&
      doenterclasses &&
      env->classdescr->classno[0] == NoClassNo) {
    SGEnterClasses (env->classdescr);
  }
}/* EnterEnv */
 
static void
#ifdef PROTO_OK
LeaveEnv(SGEnvironment env)
#else
LeaveEnv(env) SGEnvironment env;
#endif
/* Make the access of the array reflect the parent of env
 *   On entry-
 *     The access of the array reflects env
 **/
{ SGBinding r;

  for (r = env->relate; r; r = r->nxt) {
    SGStkPtr s = IDN_STK(env, r->idn);
    IDN_STK(env, r->idn) = s->out;
    s->out = state.FreeStackElements; state.FreeStackElements = s;
  }
  env->nested = 0;
  env->access->CurrEnv = env->parent;
}/* LeaveEnv */
 
static void
#ifdef PROTO_OK
ForceEnv(SGEnvironment env)
#else
ForceEnv(env) SGEnvironment env;
#endif
/* Make certain that the access of the array reflects env
 ***/
{
  if (env) {
    if (env->nested) {
      while (env->access->CurrEnv != env) LeaveEnv(env->access->CurrEnv);
    } else {
      ForceEnv(env->parent); EnterEnv(env);
    }
  }
}/* ForceEnv */
 
SGEnvironment
#ifdef PROTO_OK
NewSGScope(SGEnvironment env)
#else
NewSGScope(env) SGEnvironment env;
#endif
/* Establish a new scope within an environment
 *   On exit-
 *     NewSGScope=new environment that is a child of env
 ***/
{
  SGEnvironment e;
 
  if (!env) return NoSGEnv;
  e = (SGEnvironment)obstack_alloc(&space, sizeof(struct _SGEnvImpl));
  e->relate = NoSGBinding; e->parent = env; e->nested = 0;
  e->haveusedbindings = 0;
  e->access = env->access; e->key = NoKey;
  e->classdescr = (SGClassDescriptor)0;
  e->level = e->parent->level+1;

#ifdef MONITOR
  _dapto_env_created (e, env);
#endif

  return e;
}
 
static void
#ifdef PROTO_OK
MakeClass(SGEnvironment env)
#else
MakeClass(env) SGEnvironment env;
#endif
/* Prepare a scope to be used in inheritance
 *   On entry-
 *     env is the scope to be used in inheritance
 *   On exit-
 *     env may be used in inheritance
 *     The class descriptor is initialized
 *     The class access mechanism is initialized
 ***/
{ int i;
  if (env == NoSGEnv || env->classdescr) return;
#ifdef TEST
  printf ("MakeClass\n");
#endif

  env->classdescr =
    (SGClassDescriptor)obstack_alloc(&space, sizeof(struct _SGClassDescriptor));
  for (i = 0; i<MaxKindsPathEdge; i++) {
      env->classdescr->classno[i] = NoClassNo;
      env->classdescr->visiting = 0;
      env->classdescr->inhlist[i] = (SGInheritPtr)0;
      env->classdescr->inhset[i] = NewBitSet();
  }
  env->classdescr->env = env;

  /* append this class to the list of all classes in access mechanism: */
  env->classdescr->nxt = env->access->Classes;
  env->access->Classes = env->classdescr;

  /* initialize class access structure: */
  for (i = 0; i<MaxKindsPathEdge; i++) {
    if (!(env->access->ClassIdnTbl[i])) {
      env->access->ClassIdnTbl[i] = NEW_OBSTACK();
      obstack_init(env->access->ClassIdnTbl[i]);
    }
  }
#ifdef TEST
  printf ("MakeClass end\n");
#endif
}/* MakeClass */

int
#ifdef PROTO_OK
SGInheritsfrom(SGEnvironment tocl, SGEnvironment fromcl, int label)
#else
SGInheritsfrom(tocl, fromcl, label) SGEnvironment tocl, fromcl; int label;
#endif
/* Checks the completed inheritance DAG
 * for existance of an inheritance path
 *   On entry-
 *     tocl and fromcl are SGEnvironments
 *     label distinguishes INH and IOD
 *   On exit-
 *     1 is returned if tocl == fromcl or if there is an
 *     inheritance path from fromcl to tocl;
 *     otherwise 0 is returned.
 *     No further inheritance may be established to tocl or fromcl
 *     after this call.
 ***/
{ 
  if (!tocl || !fromcl) return 0;
  MakeClass(tocl); MakeClass(fromcl);
  if (fromcl->classdescr->classno[0] == NoClassNo) 
     SGEnterClasses (fromcl->classdescr);
  if (tocl->classdescr->classno[0] == NoClassNo) 
     SGEnterClasses (tocl->classdescr);

  if (tocl == fromcl) return 1; /* shortcuts the following check */
  return
     ElemInBitSet (fromcl->classdescr->classno[label-1], 
                   tocl->classdescr->inhset[label-1]);
}/* SGInheritsfrom */

int
#ifdef PROTO_OK
SGInheritClass(SGEnvironment subcl, SGEnvironment supercl, int label)
#else
  SGInheritClass(subcl, supercl, label) SGEnvironment subcl, supercl; int label;
#endif
/* Establish an inheritance from the class supercl to subcl
 *   On entry-
 *     subcl and supercl are SGEnvironments
 *     label distinguishes kinds of path edges
 *     bindings must not yet have been sought in subcl
 *   On exit-
 *     the inheritance from supercl to subcl is established and
 *     1 is returned if subcl and supercl are both nested in
 *     the same SGEnvironment hierarchy and if
 *     subcl != supercl and if
 *     subcl has not been searched before;
 *     otherwise no new inheritance is established and
 *     0 is returned.
 *     The inheritance from the class supercl to subcl may establish a cycle.
 ***/
{ SGInheritPtr inhf;

#ifdef TEST
  /*  if (!subcl || !supercl || subcl->access != supercl->access)*/
  printf ("SGInheritClass edge kind %d, !subcl %d, !supercl %d, diff access %d\n", 
	  label, !subcl, !supercl, subcl->access != supercl->access);
  if (subcl && supercl && (subcl == supercl)) printf ("SGInheritClass adds a loop!!\n");
#endif
  if (!subcl || !supercl || subcl->access != supercl->access) return 0;
  /* environments exist and are in the same name space: */

  /* We ignore that condition:
  if (subcl->haveusedbindings) return 0;
     subcl may have been searched before */

  /* make sure they are classes: */
  MakeClass(subcl); MakeClass(supercl);

  /* create the edge: */
  inhf = (SGInheritPtr)obstack_alloc(&space, sizeof(struct _SGInheritance));
  inhf->fromcl = supercl->classdescr;
  inhf->nxt = subcl->classdescr->inhlist[label-1]; 
  subcl->classdescr->inhlist[label-1] = inhf;
#ifdef TEST
  printf ("SGInheritClass returns 1\n");
#endif
  return 1;
}

DefTableKey
#ifdef PROTO_OK
SGBindKeyCtrl(SGEnvironment env, int idn, DefTableKey key, int* ctrl)
#else
  SGBindKeyCtrl(env, idn, key, ctrl) SGEnvironment env; int idn; DefTableKey key; int* ctrl;
#endif
/* Bind an identifier to a given key in a scope, entering that scope
   6 cases are distinguished:          
   Scope contains a                    on exit       
   binding bnd ==...        key ==...    bnd ==...         NABind returns ctrl
      (id, NoKey)             NoKey      (id, NoKey)       NoKey           0
      (id, NoKey)             a          (id, a)           a               2
      (id, b)                 NoKey      (id, b)           b               0
      (id, b)                 a          (id, b)           b               0
      no such bnd             NoKey      (id, c=NewKey())  c               1
                                            if ctrl == 0
      no such bnd             NoKey      (id, NoKey)       NoKey           1
                                            if ctrl == 1
      no such bnd             a          (id, a)           a               1

   bnd== (id, NoKey) is a placeholder for an announced imported
   definition.
   ctrl indicates that (1) a new binding is created, (2) a binding is changed,
   (0) nothing is changed
   Case 4 may indicate a collision between a binding (id, b) in the 
   scope and another (id, a) to be bound by this call. The former is
   kept. The situation may be recognized on the call side by
   comparing the argument for k to the result.
 ***/
{ SGBinding r; int i; DefTableKey rk;

  if (!env) return NoKey;
  ForceEnv(env);

  IDN_ACCESSIBLE(idn, env)

    if (IDN_STK(env, idn) && SGEnvOf(IDN_STK(env, idn)->binding)==env) {
      /* there is a binding of id in this env */
      rk = SGKeyOf(IDN_STK(env, idn)->binding);
      if (rk == NoKey) {
	/* place holder for an imported key */
        if (key == NoKey) {
	  /* collides with a local def */
	  *ctrl = 0; return NoKey;
	} else {
	  /* substitute the imported key */
	  *ctrl = 2;
	  IDN_STK(env, idn)->binding->key = key;
	  return key;
	}
      } else {
	/* id is already bound, if key != NoKey caller must check result */
	*ctrl = 0;
	return rk;
      }
    } else {
      /* there is no binding of id in this env */
      if (key == NoKey && *ctrl == 0) key = NewKey ();
      MAKE_BINDING(r, idn, key, env)
      PUSH_IDN_STK(r->idn, env, r)

      if (env->classdescr)
        for (i = 0; i<MaxKindsPathEdge; i++)
           if (env->classdescr->classno[i] != NoClassNo) {
              /* class has been inserted into the class access;
                 this new binding is added to the class access: */
              INSERT_CLASS_IDN_STK (env, idn, r, i)
           }
      *ctrl = 1;
      return key;
    }
}/* SGBindKeyCtrl */

static DefTableKey
#ifdef PROTO_OK
KeyInInhGraph (SGEnvironment env, int idn, DefTableKey usekeyArg, int allGraphs)
#else
  KeyInInhGraph (env, idn, usekeyArg, allGraphs) 
    SGEnvironment env; int idn; DefTableKey usekeyArg; int allGraphs;
#endif
/* renamed from BindingInInhGraph, result type modified */
/*   Find all bindings for an identifier in the inheritance graphs of env,
 *   but not those which are overridden by accepted ones, and
 *   disambiguate them.
 *   If no binding is found in the graph of kind i and allGraphs holds,
 *   then the graph of kind i+1 is searched, too.
 *   (bindings in the scope of env itself are not considered here)
 ***/
{ SGBinding r;
  SGStkPtr stk; int clno; BitSet inhset; SGBinding inhbind;
  DefTableKeyList dl = NULLDefTableKeyList;
  DefTableKey ak = NoKey;
  int continuation;
  BitSet overridden;
  int label;

#ifdef TEST
  printf ("KeyInInhGraph with allGraphs: %d idn %s use line %d\n", allGraphs,
	  StringTable (idn), LineOfPtr(GetCoord (usekeyArg, NullCoordRef)));
#endif

  /* prepare class access of env: */
  if (!env) return NoKey;
  if (!(env->classdescr))  return NoKey;
  if (env->classdescr->classno[0] == NoClassNo)
      /* inheritances of this class have not been processed yet */
      SGEnterClasses (env->classdescr);

  for (label = 1; label <= MaxKindsPathEdge; label++) {
    clno = env->classdescr->classno[label-1];

    /* search graph of label edges: */
    if (idn >= env->access->MaxClassIdn[label-1] ||
	env->classdescr->inhlist[label-1] == (SGInheritPtr)0) {
#ifdef TEST
    printf ("  no inh edge of label kind %d\n", label);
#endif
      if (allGraphs) continue; /* with label + 1 edges */
      else break;
    }

    inhset = env->classdescr->inhset[label-1]; /* clno and all super classes */
    overridden = NullBitSet;
    stk = CLASS_IDN_STK (env, idn, (label-1));
#ifdef TEST
  printf ("   label %d graph for id %s initially has stk %d\n", 
	  label, StringTable (idn), stk != 0);
#endif
    /* skip all definitions in classes that can not be a super class of env.
       (super classes have a smaller number than sub classes):
       May not be skipped, because cycles may violate that assumption.
    while (stk && clno <= stk->binding->env->classdescr->classno[label-1])
       stk = stk->out;
    */
#ifdef TEST
    printf ("   label %d graph for id %s has stk in older class %d\n", 
            label, StringTable (idn), stk != 0);
#endif
    /* find all inherited bindings on multiple kind label paths, 
       which are not overridden by previously accepted bindings:
    */
    while (stk) {
      inhbind = stk->binding; /* potential binding */
#ifdef TEST
/*!!!*/ printf ("   stk binding of id %s in scope of %s\n", 
		StringTable (idn),
		StringTable(GetSym(GetNodeOwnerKey(GetBoundIn(SGKeyOf(inhbind),NoScopeNode)),0)));
#endif
      if ( ElemInBitSet (inhbind->env->classdescr->classno[label-1], inhset) &&
	   !(ElemInBitSet (inhbind->env->classdescr->classno[label-1], overridden))) {
#ifdef TEST
    printf (" label %d graph for id %s has stk in non overridden super class %d bound in %s\n", 
            label, StringTable (idn), inhbind->env->classdescr->classno[label-1],
	    StringTable(GetSym(GetNodeOwnerKey(GetBoundIn(SGKeyOf(inhbind),NoScopeNode)),0)));
#endif
            /* is inherited and not overridden by an accepted binding */
            /* extension point: isAcceptable type 2: */
            /* decides whether inhbind becomes a candidate for disambiguation */
	     continuation = isAcceptablePath (SGKeyOf(inhbind), usekeyArg, label);
             if (continuation == IgnoreSkipPath) {
 #ifdef TEST
  printf ("   IgnoreSkipPath in label %d graph idn %s def line %d\n", 
	  label, StringTable (idn), LineOfPtr(GetCoord (SGKeyOf(inhbind), NullCoordRef)));
#endif
                /* rejected inhbind overrides bindings in its super classes: */
	        overridden = 
                  UnionToBitSet (overridden, inhbind->env->classdescr->inhset[label-1]);
              } else
              if (continuation == AcceptBinding) {
		/* accumulate this binding to list of accepted bindings: */
		dl = AppElDefTableKeyList (dl, SGKeyOf(inhbind));
#ifdef TEST
	printf ("   accepted label %d graph idn %s def line %d\n", label,
	  StringTable (idn), LineOfPtr(GetCoord (SGKeyOf(inhbind), NullCoordRef)));
#endif
                /* Known difference to LookupPaths in model graph:
                   inhbind is found only once, even if it may be reached via n 
                   other pathes from env. 
                   DisambiguatePaths should accept multiple occurrences of the
                   same key.
                   see Test/deviate/multInh.nl
                */
		/* accepted inhbind overrides bindings in its super classes: */
		overridden = 
                  UnionToBitSet (overridden, inhbind->env->classdescr->inhset[label-1]);
	      } else { /* continuation == IgnoreContinue */
#ifdef TEST
  printf ("   IgnoreContinue INH graph idn %s def line %d\n", 
	  StringTable (idn), LineOfPtr(GetCoord (SGKeyOf(inhbind), NullCoordRef)));
#endif
              }
        }
#ifdef TEST
      printf ("  check next binding of label %d if any\n", label);
#endif
        stk = stk->out;
      }/* while stk */
#ifdef TEST
      printf ("  no more bindings of label %d\n", label);
#endif

      if (dl == NULLDefTableKeyList) { /* no bindings found for this graph kind */
    if (allGraphs) continue; /* next graph kind */ else break;
  } else if (TailDefTableKeyList (dl) == NULLDefTableKeyList)
    ak = HeadDefTableKeyList (dl);  /* one binding found for this graph kind */
  else
    ak = DisambiguatePaths (dl, usekeyArg, label);
#ifdef TEST
    printf ("   disambig id %s is NoKey %d\n", 
            StringTable (idn), ak == NoKey);
#endif
    if (ak) return ak; /* a key is found */
  }/* for all graph kinds searched */

#ifdef TEST
  printf ("end KeyInInhGraph: NoKey\n");
#endif
  return NoKey;
}/* KeyInInhGraph */

DefTableKey
#ifdef PROTO_OK
SGKeyInEnv(SGEnvironment env, int idn, DefTableKey usekeyArg)
#else
SGKeyInEnv(env, idn, usekeyArg) SGEnvironment env; int idn; DefTableKey usekeyArg;
#endif
/* renamed from BindingInEnv, result type modified */
/* Find the key bound to an identifier in an environment
 *   use identifier stacks; do consider inheritance
 *    If idn is bound in the innermost scope of env then on exit-
 *       SGKeyInEnv= key bound to idn in env
 *    Else if idn is bound in some ancestor of env then on exit-   
 *       SGKeyInEnv=SGKeyInEnv(Parent(env),idn)
 *    Else on exit-
 *       SGKeyInEnv=NoKey
 *    Any class environment on the path from env to surrounding
 *       environments is checked for inherited definitions of idn
 ***/
{ SGStkPtr stk; SGEnvironment par;

#ifdef TEST
  printf ("SGKeyInEnv idn %s use line %d\n", 
	  StringTable (idn), LineOfPtr(GetCoord (usekeyArg, NullCoordRef)));
#endif
#ifdef TIMEENV
    SGKeyInEnvCnt++;
    TIMEBEFORE(timeBeforeEnv)
#endif

  if (!env || idn >= env->access->MaxIdn) return NoKey;
  doenterclasses = 1;
  ForceEnv(env);
  doenterclasses = 0;


  stk = IDN_STK(env, idn);
#ifdef TEST
  printf ("   there is a binding in env: %d\n", stk != 0); 
#endif
  /* stk != 0 means: there is a binding in env */

  do { /* iterate until an acceptable binding is found in
          an inherited or in a nested environment
       */
       /* an inherited binding is searched along all inheritance paths from par,
          where par are the class environments from the current one outward as long as
          par's nesting level is greater than that of the binding found on the idn stack;
          if no binding is found on the stack, all inheritances of parents are searched.
       */
       /* first it is checked whether there are inherited bindings that are
          not hidden by a binding in the nested environments:
      */
#ifdef TIMEENV
    TIMEBEFORE(timeBeforePath)
#endif
    par = env;
    while (par && (!stk || (par->level > SGEnvOf(stk->binding)->level))) {
#ifdef TEST
    printf ("  par->level: %d\n", par->level);
#endif    
      if (par->classdescr) {
#ifdef TIMEENV
    KeyInInhGraphCnt++;
#endif
        DefTableKey inhkey = KeyInInhGraph(par, idn, usekeyArg, 1 /* withIOD*/);
        /* isAcceptablePath and DisambiguatePaths are called in KeyInInhGraph */
        if (inhkey) {
#ifdef TIMEENV
    TIMEAFTER(EnvTime,timeBeforeEnv)
    TIMEAFTER(PathTime,timeBeforePath)
    envTimeSec  += (long) EnvTime.tv_sec;
    envTimeUsec += (long) EnvTime.tv_usec;
    envPathTimeSec  += (long) PathTime.tv_sec;
    envPathTimeUsec += (long) PathTime.tv_usec;
#endif
#ifdef TEST
          printf ("end SGKeyInEnv return key of inh binding\n");
#endif    
          return inhkey;
	}/* found an inh key */
      }/* a class */
    par = par->parent;
  }/* while parent ... */

#ifdef TIMEENV
    TIMEAFTER(PathTime,timeBeforePath)
    envPathTimeSec  += (long) PathTime.tv_sec;
    envPathTimeUsec += (long) PathTime.tv_usec;
#endif

#ifdef TEST
  printf ("SGKeyInEnv: nested id %s use %d: no inherits\n", 
	  StringTable (idn), LineOfPtr(GetCoord (usekeyArg, NullCoordRef)));
  if (stk) printf ("   there is an outer binding\n");
  else  printf ("   there is no outer binding\n");
#endif
    if (stk) { 
       /* there is a binding in the nest */
       /* extension point: isAcceptable type 3: */
       int continuation = isAcceptableSimple (SGKeyOf(stk->binding), usekeyArg);
#ifdef TEST
       printf ("   isAcceptableSimple nested id %s use %d def %d accepted? %d\n", 
	    StringTable (idn), LineOfPtr(GetCoord (usekeyArg, NullCoordRef)),  
            LineOfPtr(GetCoord (SGKeyOf(stk->binding), NullCoordRef)),
            continuation);
#endif

       if (continuation == AcceptBinding) {
#ifdef TIMEENV
    TIMEAFTER(EnvTime,timeBeforeEnv)
    envTimeSec  += (long) EnvTime.tv_sec;
    envTimeUsec += (long) EnvTime.tv_usec;
#endif
#ifdef TEST
    printf ("end SGKeyInEnv return key of parent binding\n");
#endif    
    return SGKeyOf(stk->binding);
       }
       if (continuation == IgnoreSkipPath) {
         /* IgnoreSkipPath: we are done with this scope, 
            ignore the rejected binding,
            and ignore this scope's inherited bindings: */
            env = SGEnvOf(stk->binding)->parent; 
            stk = stk->out;
#ifdef TEST
  printf ("   IgnoreSkipPath nested id %s use %d next iteration\n", 
	  StringTable (idn), LineOfPtr(GetCoord (usekeyArg, NullCoordRef)));
#endif
       } else {
           /* IgnoreContinue: */
         env = SGEnvOf(stk->binding); /* on next iteration check inh bindings from env on */ 
         stk = stk->out; /* set stk to next outer binding, if any */
#ifdef TEST
  printf ("   IgnoreContinue nested id %s use %d next iteration\n", 
	  StringTable (idn), LineOfPtr(GetCoord (usekeyArg, NullCoordRef)));
#endif
       }
    } else {
#ifdef TIMEENV
    TIMEAFTER(EnvTime,timeBeforeEnv)
    envTimeSec  += (long) EnvTime.tv_sec;
    envTimeUsec += (long) EnvTime.tv_usec;
#endif
#ifdef TEST
    printf ("end SGKeyInEnv return no key\n");
#endif    
      return NoKey; /* no binding in nest, none in inh graphs */
    }
  } while (stk || env);

#ifdef TIMEENV
    TIMEAFTER(EnvTime,timeBeforeEnv)
    envTimeSec  += (long) EnvTime.tv_sec;
    envTimeUsec += (long) EnvTime.tv_usec;
#endif
  return NoKey;
}/* SGKeyInEnv */

#include "DefTableKeyList.h"


DefTableKey
#ifdef PROTO_OK
SGKeyInScope(SGEnvironment env, int idn, DefTableKey usekeyArg)
#else
SGKeyInScope(env, idn, usekeyArg) SGEnvironment env; int idn; DefTableKey usekeyArg;
#endif
/* renamed from BindingInScope, result type modified */
/* Find the binding for an identifier in a scope
 *    If idn is bound in the innermost scope of env then on exit-   
 *       SGKeyInScope=key of the binding for idn
 *          in the innermost scope of env
 *    Else on exit- 
 *       SGKeyInScope=NoKey
 *    If env is a class environment inherited bindings are considered
 ***/
{ SGBinding r;
  int continuation;

#ifdef TEST
  printf ("SGKeyInScope idn %s use line %d\n", 
	  StringTable (idn), LineOfPtr(GetCoord (usekeyArg, NullCoordRef)));
#endif

  if (!env) return NoKey;

  env->haveusedbindings = 1;

  /* search for a local binding in env: */
  for (r = env->relate; r; r = r->nxt) 
    if (r->idn == idn) { 
      /* extension point: isAcceptable type 1: */
      continuation = isAcceptableQualified (SGKeyOf (r), usekeyArg);
      if (continuation == AcceptBinding) return SGKeyOf (r);
      else if (continuation == IgnoreSkipPath) return NoKey;
      else goto inhGraph; /* IgnoreContinue */
    }
  /* there is no acceptable binding in env, search inh graph: */
 inhGraph:
  return KeyInInhGraph (env, idn, usekeyArg, 0 /* inheritance only*/);
}/* SGKeyInScope */

DefTableKey
#ifdef PROTO_OK
SGKeyInScopeNoInh(SGEnvironment env, int idn)
#else
SGKeyInScopeNoInh(env, idn) SGEnvironment env; int idn;
#endif
/* renamed from BindingInScopeNoInh, result type modified */
/* Find the binding for an identifier in a scope
 *    If idn is bound in the innermost scope of env then on exit-   
 *       SGKeyInScopeNoInh=key of the binding for idn
 *          in the innermost scope of env
 *    Else on exit- 
 *       SGKeyInScopeNoInh=NoKey
 *    Inherited bindings are NOT considered
 ***/
{ SGBinding r;
#ifdef TEST
  printf ("SGKeyInScopeNoInh idn %s\n", StringTable (idn));
#endif
  for (r = env->relate; r; r = r->nxt) 
     if (r->idn == idn) 
       return SGKeyOf (r);

  return NoKey;
}/* SGKeyInScopeNoInh */

DefTableKey
#ifdef PROTO_OK
SGKeyInEnvNoInh(SGEnvironment env, int idn)
#else
SGKeyInEnvNoInh(env, idn) SGEnvironment env; int idn;
#endif
/* renamed from KeyInEnv */
/* Used for mapping idents to keys rather than for name analysis!!
 * Find the key for an identifier in an environment
 *   use identifier stacks; do not consider inheritance
 *   If idn is bound in the innermost scope of env then on exit-
 *     SGKeyInEnvNoInh=the key of the binding for idn in env
 *   Else if idn is bound in some ancestor of env then 
 *     SGKeyInEnvNoInh=SGKeyInEnvNoInh(Parent(env),idn)
 *   Else on exit-
 *     SGKeyInEnvNoInh=NoKey
  ***/
{ SGStkPtr stk; SGEnvironment par;

  if (!env || idn >= env->access->MaxIdn) return NoKey;
  doenterclasses = 0;
  ForceEnv(env);

  stk = IDN_STK(env, idn);
  /* stk != 0 means: there is a binding in env */

  return (stk ? SGKeyOf(stk->binding) : NoKey);
}/* KeyInEnvNoInh */



static
SGStkPtr
#ifdef PROTO_OK
NextInhStkPtr (SGEnvironment env, SGStkPtr stk)
#else
NextInhStkPtr (env, stk) SGEnvironment env; SGStkPtr stk;
#endif
/* On entry:
 *   ONLY graphs with label == 1 are considered!
 *   stk is an entry in the class access that refers to a binding
 *     of some identifier idn in an environment e which is
 *     inherited by an environment tocl that is env or is the
 *     nearest ancestor of env which inherits e.
 * On exit:
 *     NextInhStkPtr=nextstk represents the next binding to
 *     identifier idn in an environment ep which is also
 *     inherited by tocl but not to e,
 *     if any such binding exists;
 *     otherwise NextInhStkPtr=(SGStkPtr)0.
 ***/
{ SGBinding lastbinding = stk->binding;
  /* the class defining lastbinding: */
  int lastclno = SGEnvOf(lastbinding)->classdescr->classno[0];
  BitSet lastinhset = SGEnvOf(lastbinding)->classdescr->inhset[0];

  BitSet inhset; SGEnvironment par;
#ifdef TEST
  printf ("NextInhStkPtr\n");
#endif

  stk = stk->out;

  /* find the first parent of env that is a class which
     inherits the class that defines lastbinding: */
  par = env;
  while (!(par->classdescr) ||
         !ElemInBitSet (lastclno, par->classdescr->inhset[0]))
    par = par->parent;

  if (!par) return (SGStkPtr)0;

  /* classes that may contain definitions alternative to lastbinding: */
  inhset = par->classdescr->inhset[0];

  /* see if there are some which are not hidden by lastbinding: */
  while (stk &&
	 (!ElemInBitSet
	    (SGEnvOf(stk->binding)->classdescr->classno[0], inhset) ||
	     ElemInBitSet
	       (SGEnvOf(stk->binding)->classdescr->classno[0], lastinhset)))
    stk = stk->out;

  return stk;
}/* NextInhStkPtr */

DefTableKey
#ifdef PROTO_OK
SGDefineIdn(SGEnvironment env, int idn)
#else
SGDefineIdn(env, idn) SGEnvironment env; int idn;
#endif
/* renamed from BindIdn, result type modified */
/* Bind an identifier in a scope, entering that scope
 *    If env is not the current environment then enter it
 *    If idn is bound in the innermost scope of env then on exit-
 *       SGDefineIdn=pointer to the key of the binding for idn 
 *       in the innermost scope of env
 *    Else let n be a previously-unused definition table key
 *    Then on exit-
 *       SGDefineIdn=n, and a new binding (idn,n) is created in the innermost 
 *       scope of env
 ***/
{ SGBinding r; int i;

  if (!env) return NoKey;
#ifdef TEST
  printf ("SGDefineIdn %s\n", StringTable (idn));
#endif
  ForceEnv(env);

  IDN_ACCESSIBLE(idn, env)

  if (IDN_STK(env, idn) ? SGEnvOf(IDN_STK(env, idn)->binding)==env : 0)
    return SGKeyOf(IDN_STK(env, idn)->binding);

  MAKE_BINDING(r, idn, NewKey(), env)
  PUSH_IDN_STK(r->idn, env, r)

  if (env->classdescr)
     for (i = 0; i<MaxKindsPathEdge; i++)
        if (env->classdescr->classno[i] != NoClassNo) {
#ifdef TEST
          printf (" inserted late into graph %d; %s\n",
		  i, StringTable (idn));
#endif
	  /* class has been inserted into the class access;
              this new binding is added to the class access: */
           INSERT_CLASS_IDN_STK (env, idn, r, i)
        }
  return SGKeyOf(r);
}/* SGDefineIdn */

#ifndef NORESTORE
static struct savedata_str
{
    void *space;
    void *saved_stacks;
    int stackscount;
    void *state;
    void *stacks_base;    
} savedata;

/* Store handles for saved obstacks somewhere. We need a dynamic growing array
 * to store them and must be able to Save/Restore the structure. So use an obstack: */
Obstack saved_stacks = obstack_empty_chunk(4096, OBSTACK_PTR_ALIGN);

void *SaveModuleEnvmod()
{
    void *base;
    ObstackP *the_stacks;
    
    /* envmod contains many growing obstacks; each contains one ever-growing object */
    /* The obstack 'stacks' contains pointers to all these object stacks */
    /* Strategy: Call SaveObstack for all these stacks and save the pointer
     * to another obstack. Then save the obstack containing the pointers */

    /* Make obstack 'saved_stacks' empty. */
    obstack_next_free(&saved_stacks) = (char *)obstack_base(&saved_stacks);
    
    /* Extract an array of pointers to dynamically allocated obstacks */
    the_stacks = (ObstackP *)obstack_base(&stack_adresses);
    
    if (the_stacks)
    {
	ObstackP *stacks_end;
	
	savedata.stackscount = obstack_object_size(&stack_adresses) / sizeof(ObstackP);
	stacks_end = the_stacks + savedata.stackscount;

	/* For each of the obstacks */
	while (the_stacks != stacks_end)
	{
	    void *savedstack = SaveObstack(*the_stacks, (void *)0);
	    obstack_grow(&saved_stacks, &savedstack, sizeof(savedstack));
	    the_stacks++;
	}

        /* Save the dynamic array yielding one single handle */
	savedata.saved_stacks = SaveObstack(&saved_stacks, (void *)0);
    }
    else /* No dynamic obstacks exist. Mark that fact with a special value */
	savedata.saved_stacks = (void*)0;

    /* Mark current allocation level for the real obstacks */
    savedata.stacks_base = obstack_alloc(&stacks, 0);
    
    /* Now save the rest of the data */
    base = obstack_alloc(&space, 0);
    savedata.space = SaveObstack(&space, base);

    /* and the static information */
    savedata.state = SaveData(&state, sizeof(state));
    
    return SaveData(&savedata, sizeof(savedata));
}
    
void
#ifdef PROTO_OK
RestoreModuleEnvmod(void *base)
#else
RestoreModuleEnvmod(base) 
void *base;
#endif     
{
    ObstackP *the_stacks;

    /* This restores the savedata-structure */
    RestoreData(base);

    /* New obstacks may have been allocated in the meantime */
    the_stacks = (ObstackP *) obstack_base(&stack_adresses);
    if (the_stacks)
    {
	int stackscount_now = obstack_object_size(&stack_adresses) / sizeof(ObstackP);
	ObstackP *stacks_end = the_stacks + stackscount_now;

	the_stacks += savedata.stackscount;
	if (savedata.stackscount > stackscount_now) 
	    abort();

	/* For every newly appeard obstack, obstack_free will be called */
	while (the_stacks != stacks_end)
	{
	    obstack_free(*the_stacks, (void *)0);  /* 0-Pointer frees everything! */
	    the_stacks++;
	}

	/* Shorten the stacks-obstack so that this will not be done again. */
	the_stacks = (ObstackP *) obstack_base(&stack_adresses);
       	obstack_next_free(&stack_adresses) = (char *)(the_stacks + savedata.stackscount);
    }

    /* Now restore the previously existing stacks */
    /* the_stacks points to beginning of obstack-adress-array */

    if (savedata.stackscount > 0)
    {
	void **saved_stack;
	ObstackP *stacks_end = the_stacks + savedata.stackscount;
	
	/* Restores Data in saved_stacks */
	RestoreObstack(&saved_stacks, savedata.saved_stacks);
	
	saved_stack = (void **)obstack_base(&saved_stacks);
	while (the_stacks != stacks_end)
	{
	    RestoreObstack(*the_stacks, *saved_stack);
	    the_stacks++;
	    saved_stack++;
	}
    }

    /* Free Memory of stack control structures */
    obstack_free(&stacks, savedata.stacks_base);

    /* Now the rest of the data */
    RestoreObstack(&space, savedata.space);
    RestoreData(savedata.state);
}

#endif

    
