#include "deftbl.h"

DefTableKey
DisambiguateGraphs (DefTableKey G[], int N, DefTableKey app)
/* On entry-
 *   G[] is an array with N elements giving the result for each search
 *   app is the UseKey of the applied occurrence sought
 * On exit-
 *   DisambiguateGraphs is the selected key
 ***/
{ int i;
  DefTableKey result = NoKey;

  for (i = 0; i < N; i++) {
    if (G[i] != NoKey) {
      if (result != NoKey) return NoKey;
      result = G[i];
    }
  }
  return result;
}
