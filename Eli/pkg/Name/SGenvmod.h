#ifndef SGENVMOD_H
#define SGENVMOD_H

/* Copyright 1991, The Regents of the University of Colorado */

/* This file is part of the Eli Module Library.

The Eli Module Library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

The Eli Module Library is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with the Eli Module Library; see the file COPYING.LIB.
If not, write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Eli into the
   directory resulting from a :source derivation, you may use that
   created file as a part of that directory without restriction. */

#include "deftbl.h"
#include "obstack.h"
#include "BitSet.h"
#include "eliproto.h"

typedef struct _SGEnvImpl *SGEnvironment;	/* Set of regions */
typedef struct _SGRelElt *SGScope;	/* Single region */
typedef struct _SGRelElt *SGBinding;	/* Handle for binding data */
typedef struct _SGAccessMechanism *SGAccessMechanism;/* Constant-time access data */
typedef struct _SGClassDescriptor *SGClassDescriptor;/* Description of a class SGEnvironment */

struct _SGEnvImpl {		/* Addressing environment */
   int nested;			   /* 1 if currently in this environment */
   SGAccessMechanism access;	   /* Constant-time access data */
   SGEnvironment parent;	   /* Enclosing environment */
   SGScope relate;		   /* Current region */
   DefTableKey key;		   /* a key may be set to this env */
   int level;			   /* nesting level */
   SGClassDescriptor classdescr;   /* inheritance properties if this is a class */
   int haveusedbindings;	   /* lookups have been done */
};

struct _SGRelElt {		   /* SGBinding data */
   SGBinding nxt;		   /* Next binding for the current region */
   SGEnvironment env;		   /* Region containing this binding */
   DefTableKey key;		   /* Definition */
   int idn;			   /* Identifier */
};

typedef struct _SGStkElt *SGStkPtr;	/* List implementing a definition stack */
struct _SGStkElt {
   SGStkPtr out;		   /* Superseded definitions */
   SGBinding binding;		   /* SGBinding for this definition */
};

/* import MaxKindsPathEdge, affects SGAccessMechanism and SGClassDescriptor */
#include "ScopeGraphs.h"
#ifndef MaxKindsPathEdge
/* the default for the the number of kinds of paths edges, i.e. 1, 2: */
#define MaxKindsPathEdge	1
#endif

struct _SGAccessMechanism {	/* Constant-time access data */
/* nesting access structure: */
   ObstackP IdnTbl; int MaxIdn;	   /* Stacks of definitions */
/* inheritance access structures: */
   ObstackP ClassIdnTbl[MaxKindsPathEdge];	   /* inheritance access structures
						      one for each kind of path edges */
   int NextClassNo[MaxKindsPathEdge];		   /* number of next class to be entered */
   int MaxClassIdn[MaxKindsPathEdge];
   SGClassDescriptor Classes;		   /* list of all class descriptors */
/* general components: */
   SGEnvironment CurrEnv;		   /* SGEnvironment for the array nested */
};

typedef struct _SGInheritance *SGInheritPtr;
struct _SGClassDescriptor {	   /* Description of a class SGEnvironment */
   int classno[MaxKindsPathEdge];  /* number of this class w.r.t. edge kind */
/* inheritance lists, sets: */
   SGInheritPtr inhlist[MaxKindsPathEdge];   /* list of classes inherited from */
   BitSet inhset[MaxKindsPathEdge];	   /* transitive inheritance set */
   SGEnvironment env;		   /* this class SGEnvironment */
   SGClassDescriptor nxt;	   /* next in list of all class descriptors */
  int visiting;                    /* avoids looping in cyclic graphs */
};

struct _SGInheritance {		/* element in list of classes inherited from */
   SGClassDescriptor fromcl;		   /* inheritance from class (superclass) */
   SGInheritPtr nxt;		   /* next in list of classes inherited from */
};

#define NoSGEnv ((SGEnvironment)0)
#define NoSGScope ((SGScope)0)
#define NoSGBinding ((SGBinding)0)
#define NoSGInherit ((SGInheritPtr)0)

extern SGBinding     SGDefinitionsOf ELI_ARG((SGEnvironment env));
  /* the sequence of bindings of the environment env */
extern SGBinding     SGNextDefinition ELI_ARG((SGBinding b));
  /* the next binding within the sequence of an environment */

extern int         SGIdnOf ELI_ARG((SGBinding b));
  /* the identifier of the binding b */
extern DefTableKey SGKeyOf ELI_ARG((SGBinding b));
  /* the key of the binding b */
extern SGEnvironment SGEnvOf ELI_ARG((SGBinding b));
  /* the environment of a binding */
  
extern void SGEnterClasses ELI_ARG((SGClassDescriptor cld));

extern SGEnvironment NewSGEnv ELI_ARG((void));
/* Establish a new environment
 *    On exit-
 *       NewSGEnv=new environment
 ***/
 
extern SGEnvironment NewSGScope ELI_ARG((SGEnvironment env));
/* Establish a new scope within an environment
 *    On exit-
 *       NewSGScope=new environment that is a child of env
 ***/
 
extern int SGInheritClass  ELI_ARG((SGEnvironment subcl, SGEnvironment supercl, int label));
/* Establish an inheritance from the class supercl to subcl
 *    On entry-
 *     subcl and supercl are SGEnvironments
 *     label distinguishes kinds of path edges
 *     bindings must not yet have been sought in subcl
 *    On exit-
 *     the inheritance from supercl to subcl is established and
 *     1 is returned if subcl and supercl are both nested in
 *     the same SGEnvironment hierarchy and if
 *     subcl != supercl and if
 *     subcl has not been searched before;
 *     otherwise no new inheritance is established and
 *     0 is returned.
 *     The inheritance from the class supercl to subcl may establish a cycle.
 ***/

extern int SGInheritsfrom  ELI_ARG((SGEnvironment tocl, SGEnvironment fromcl, int label));
/* Checks the completed inheritance DAG
 * for existance of an inheritance path
 *    On entry-
 *       tocl and fromcl are SGEnvironments
 *       label distinguishes kinds of path edges
 *    On exit-
 *       1 is returned if tocl == fromcl or if there is an
 *       inheritance path from fromcl to tocl;
 *       otherwise 0 is returned.
 *       No further inheritance may be established to tocl or fromcl
 *       after this call.
 ***/

extern DefTableKey SGBindKeyCtrl
       ELI_ARG((SGEnvironment env, int idn, DefTableKey key, int* ctrl));

extern DefTableKey SGKeyInEnv ELI_ARG((SGEnvironment env, int idn, DefTableKey usekeyArg));
/* renamed from BindingInEnv, result type modified */
/* Find the key bound to an identifier in an environment
 *   use identifier stacks; do consider inheritance
 *    If idn is bound in the innermost scope of env then on exit-
 *       SGKeyInEnv= key bound to idn in env
 *    Else if idn is bound in some ancestor of env then on exit-   
 *       SGKeyInEnv=KeyInEnv(Parent(env),idn)
 *    Else on exit-
 *       SGKeyInEnv=NoKey
 *    Any class environment on the path from env to surrounding
 *       environments is checked for inherited definitions of idn
 ***/

extern DefTableKey SGKeyInScope ELI_ARG((SGEnvironment env, int idn, DefTableKey usekey));
/* renamed from BindingInScope, result type modified */
/* Find the binding for an identifier in a scope
 *    If idn is bound in the innermost scope of env then on exit-   
 *       SGKeyInScope=key of the binding for idn
 *          in the innermost scope of env
 *    Else on exit- 
 *       SGKeyInScope=NoKey
 *    If env is a class environment inherited bindings are considered
 ***/

extern DefTableKey SGKeyInScopeNoInh ELI_ARG((SGEnvironment env, int idn));
/* renamed from BindingInScopeNoInh, result type modified */
/* Find the binding for an identifier in a scope
 *    If idn is bound in the innermost scope of env then on exit-   
 *       SGKeyInScopeNoInh=key of the binding for idn
 *          in the innermost scope of env
 *    Else on exit- 
 *       SGKeyInScopeNoInh=NoKey
 *    Inherited bindings are NOT considered
 ***/

extern DefTableKey SGKeyInEnvNoInh ELI_ARG((SGEnvironment env, int idn));
/* renamed from KeyInEnv */
/* Used for mapping idents to keys rather than for name analysis!!
   Find the key for an identifier in an environment
 *   use identifier stacks; do not consider inheritance
 *   If idn is bound in the innermost scope of env then on exit-
 *     SGKeyInEnvNoInh=the key of the binding for idn in env
 *   Else if idn is bound in some ancestor of env then 
 *     SGKeyInEnvNoInh=SGKeyInEnvNoInh(Parent(env),idn)
 *   Else on exit-
 *     SGKeyInEnvNoInh=NoKey
 ***/

extern DefTableKey SGDefineIdn ELI_ARG((SGEnvironment env, int idn));
/* renamed from BindIdn, result type modified */
/* Bind an identifier in a scope, entering that scope
 *    If env is not the current environment then enter it
 *    If idn is bound in the innermost scope of env then on exit-
 *       SGDefineIdn=pointer to the key of the binding for idn 
 *       in the innermost scope of env
 *    Else let n be a previously-unused definition table key
 *    Then on exit-
 *       SGDefineIdn=n, and a new binding (idn,n) is created in the innermost 
 *       scope of env
 ***/



#ifdef TIMEENV
/* time measure for calls of KeyInEnv and KeyInInhGraph 
   is taken and reported by printTime
*/
extern void SGprintTime ();
extern void SGcountMaxIdListLength (SGEnvironment env);
#endif

#ifdef MONITOR
/* Monitoring support for structured values */

#define DAPTO_RESULTSGEnvironment(e) DAPTO_RESULT_PTR (e)
#define DAPTO_ARGSGEnvironment(e)    DAPTO_ARG_PTR (e, SGEnvironment)

#define DAPTO_RESULTSGBinding(e)     DAPTO_RESULT_PTR (e)
#define DAPTO_ARGSGBinding(e)        DAPTO_ARG_PTR (e, SGBinding)

#define DAPTO_RESULTSGClassDescriptor(e)      DAPTO_RESULT_PTR (e)
#define DAPTO_ARGSGClassDescriptor(e)         DAPTO_ARG_PTR (e, SGClassDescriptor)

#define DAPTO_RESULTSGInheritPtr(e)  DAPTO_RESULT_PTR (e)
#define DAPTO_ARGSGInheritPtr(e)     DAPTO_ARG_PTR (e, SGInheritPtr)

#endif

#endif
