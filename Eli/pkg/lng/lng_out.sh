#!/bin/sh
# $Id: clp_out.sh.in,v 1.1 2001/05/09 20:44:05 waite Exp $
# Copyright (c) 1990, The Regents of the University of Colorado
# Copyright 1995-1998, Anthony M. Sloane

# This file is part of the Eli translator construction system.

# Eli is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2, or (at your option) any later
# version.

# Eli is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License along
# with Eli; see the file COPYING.  If not, write to the Free Software
# Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
# 
# EXEC (lng_out.sh) (:lng_combine :names) (:lng_gen :name) 
#   => (:lng_out);
# :lng_out 'Results of LNG processing' => :FILE;
# 

ODIN_cmpd=$1;shift; ODIN_gen=$1;shift; 

if test -s lng_out 
then rm lng_out
else : 
fi 

touch lng_out

if test -s "$ODIN_cmpd"
then cat "${ODIN_cmpd}" >> lng_out
echo "lng_gen.hpp" >> lng_gen.specs 
echo "lng_gen.cc" >> lng_gen.specs 
echo "lng_gen.head" >> lng_gen.specs 
echo "lng_gen.finl" >> lng_gen.specs 
echo "lng_gen.lido" >> lng_gen.specs 

else
  echo "" &> lng_out
fi

