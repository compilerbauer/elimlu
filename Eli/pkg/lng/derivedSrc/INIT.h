#ifndef INIT_H1
#define INIT_H1

  obstack_init(&SrcFileStack);
#endif

#ifndef INIT_H2
#define INIT_H2



ADDNAME(DEBUG, "DEBUG");
ADDNAME(NOTE, "NOTE");
ADDNAME(FIXME, "FIXME"); 
ADDNAME(INFO, "INFO");
ADDNAME(WARNING, "WARNING");
ADDNAME(ERROR, "ERROR");
ADDNAME(FATAL, "FATAL");
ADDNAME(DEADLY, "DEADLY");

IS_DEADLY(DEADLY);
IS_ERROR(ERROR);

bool errng_has_error = false;
std::string errng_error = "";

if(ErrCutOff == NoKey) {
  messaging::severity_cutoff = -10;
}
else { 
  int namesym = GetClpValue(ErrCutOff, -1);
  std::string name = (namesym == -1? "" : StringTable(namesym));
  if(name.empty()) {
    messaging::severity_cutoff = -10;
    errng_has_error = true;
    errng_error = "Name is empty";
  }
  else {
    bool found_errcutoff = false; 
    for(auto sn : messaging::severity_names) {
      if(sn.second == name) {
        found_errcutoff = true;
        messaging::severity_cutoff = sn.first;
      }
    }
    if(!found_errcutoff) {
      errng_has_error = true;
      errng_error = "Unknown severity name : " + name;
      messaging::severity_cutoff = -10;
    }
  }
}


InitializeMessagingModule(safe_bool::s_true, &std::cerr, safe_bool::s_true, -1, 
                         safe_bool::s_false);


if(errng_has_error) {
  Report(WARNING, NoPosition, errng_error); 
}


#endif

#ifndef INIT_H3
#define INIT_H3
/* $Id: liga.init,v 4.3 1997/09/15 14:46:48 cogito Exp $ */
/* (C) Copyright 1997 University of Paderborn */

/* This file is part of the Eli Module Library.

The Eli Module Library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

The Eli Module Library is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with the Eli Module Library; see the file COPYING.LIB.
If not, write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Eli into the
   directory resulting from a :source derivation, you may use that
   created file as a part of that directory without restriction. */

InitTree();

#endif

