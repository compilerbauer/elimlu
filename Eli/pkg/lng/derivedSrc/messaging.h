
#ifndef MESSAGING_H
#define MESSAGING_H

#include "csm.h"
#include "err.h"
#include "envmod.h"
#include "source.h"
#include "deftbl.h"

#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>

#undef NOTE
#undef COMMENT
#undef WARNING
#undef ERROR
#undef FATAL
#undef DEADLY

#define DEBUG 0   /* Internal Debugging comment  */
#define NOTE 10     /* Internal Debugging note */
#define FIXME 15    /* Internal Fixme note */ 
#define INFO 20     /* "advice" for programmer wrt. coding style */
#define WARNING 30  /* non critical problem, i.e. multiple identical definitions */
#define ERROR 40    /* problem wrt. semantics, i.e. multiple non-identical defs  */
#define FATAL 50    /* hard problem while still having ability to do some checks */
#define DEADLY 100  /* kills the compiler, ICEs and hard inconsistencies */

typedef int severity;
typedef std::string sevname;

namespace messaging { 
  extern std::ostream* out;
  extern std::unordered_map<severity, sevname> severity_names;
  extern std::unordered_map<severity, int> severity_count;

  extern int severity_cutoff;
  extern int severity_is_error;
  extern int severity_is_deadly;
}

enum class safe_bool {
  s_true, s_false
};



extern int getErrs(); 


#define ADDNAME(a, b) messaging::severity_names[a] = b
#define IS_DEADLY(a) messaging::severity_is_deadly = a 
#define IS_ERROR(a) messaging::severity_is_error = a


void InitializeMessagingModule(safe_bool flush = safe_bool::s_true, 
                               std::ostream* o = &std::cerr, 
                               safe_bool ag = safe_bool::s_true, int cut = 300, 
                               safe_bool debug = safe_bool::s_false);


void Report(severity sev, CoordPtr p, const std::string msg, int agcond = 0, bool nobreak = false, bool only_once = false);
void ReportOnce(severity sev, CoordPtr p, const std::string msg, int agcond = 0, bool nobreak = false); 
void OptReport(bool val, severity sev, CoordPtr p, const std::string msg, int agcond = 0, bool nobreak = false); 

void InternalMessage(const std::string msg, CoordPtr coord = NoPosition);
extern std::string concat_str(const std::string a, std::string b);
extern std::string concat_int(const std::string a, int v);
extern std::string concat_ind(const std::string a, int ind);
extern std::string concat_bool(const std::string a, bool v);


template<class T> std::string concat_obj(std::string const& a, T const& obj) { 
  std::stringstream ss;
  ss << a;
  ss << obj;
  return ss.str(); 
}

#endif // MESSAGING_H

