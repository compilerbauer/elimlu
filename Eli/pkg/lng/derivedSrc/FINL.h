#ifndef FINL_H1
#define FINL_H1


/// The following code is (unfortunately) required because eli tries to 
/// return with ErrorCount[ERROR] > 0 

#undef DEBUG
#undef NOTE
#undef FIXME
#undef INFO
#undef WARNING
#undef ERROR
#undef FATAL
#undef DEADLY

/// taken from err.h 

#define NOTE    0       /* Nonstandard construct */
#define COMMENT 0       /* Obsolete */
#define WARNING 1       /* Repairable error */
#define ERROR   2       /* Unrepairable error */
#define FATAL   2       /* Obsolete */
#define DEADLY  3       /* Error that makes continuation impossible */

ErrorCount[ERROR] += getErrs(); 

#endif

