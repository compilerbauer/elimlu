
#ifndef _VISITPROCS_H
#define _VISITPROCS_H

#include "HEAD.h"
#include "node.h"
#include "treecon.h"

#include "eliproto.h"


extern void LIGA_ATTREVAL ELI_ARG((NODEPTR));
extern void _VS0Empty ELI_ARG((NODEPTR _currn));
extern void _VS1rule_07 ELI_ARG((_TPPrule_07 _currn));
#define _VS2rule_07 _VS0Empty

extern void _VS1rule_06 ELI_ARG((_TPPrule_06 _currn));
#define _VS2rule_06 _VS0Empty

extern void _VS1rule_05 ELI_ARG((_TPPrule_05 _currn));
#define _VS2rule_05 _VS0Empty

extern void _VS1rule_04 ELI_ARG((_TPPrule_04 _currn));
extern void _VS1rule_03 ELI_ARG((_TPPrule_03 _currn));
extern void _VS1rule_02 ELI_ARG((_TPPrule_02 _currn));
#define _VS2rule_02 _VS0Empty

extern void _VS1rule_01 ELI_ARG((_TPPrule_01 _currn));
extern void _VS1rProgramRoot ELI_ARG((_TPPrProgramRoot _currn));
extern void _VS1rProgram ELI_ARG((_TPPrProgram _currn));
extern void _VS1rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
extern void _VS2rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
extern void _VS3rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
extern void _VS4rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
extern void _VS5rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
extern void _VS6rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
extern void _VS7rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
extern void _VS8rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
extern void _VS9rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
extern void _VS10rDeclList2 ELI_ARG((_TPPrDeclList2 _currn));
#define _VS1rNoDecls _VS0Empty

#define _VS2rNoDecls _VS0Empty

#define _VS3rNoDecls _VS0Empty

#define _VS4rNoDecls _VS0Empty

extern void _VS5rNoDecls ELI_ARG((_TPPrNoDecls _currn));
#define _VS6rNoDecls _VS0Empty

extern void _VS7rNoDecls ELI_ARG((_TPPrNoDecls _currn));
#define _VS8rNoDecls _VS0Empty

#define _VS9rNoDecls _VS0Empty

extern void _VS10rNoDecls ELI_ARG((_TPPrNoDecls _currn));
#define _VS1rDeclLoad _VS0Empty

#define _VS2rDeclLoad _VS0Empty

#define _VS3rDeclLoad _VS0Empty

#define _VS4rDeclLoad _VS0Empty

#define _VS5rDeclLoad _VS0Empty

#define _VS6rDeclLoad _VS0Empty

#define _VS7rDeclLoad _VS0Empty

extern void _VS8rDeclLoad ELI_ARG((_TPPrDeclLoad _currn));
#define _VS9rDeclLoad _VS0Empty

extern void _VS10rDeclLoad ELI_ARG((_TPPrDeclLoad _currn));
extern void _VS1rLoadDecl ELI_ARG((_TPPrLoadDecl _currn));
extern void _VS1rDeclPTG ELI_ARG((_TPPrDeclPTG _currn));
extern void _VS2rDeclPTG ELI_ARG((_TPPrDeclPTG _currn));
#define _VS3rDeclPTG _VS0Empty

extern void _VS4rDeclPTG ELI_ARG((_TPPrDeclPTG _currn));
extern void _VS5rDeclPTG ELI_ARG((_TPPrDeclPTG _currn));
#define _VS6rDeclPTG _VS0Empty

#define _VS7rDeclPTG _VS0Empty

extern void _VS8rDeclPTG ELI_ARG((_TPPrDeclPTG _currn));
#define _VS9rDeclPTG _VS0Empty

extern void _VS10rDeclPTG ELI_ARG((_TPPrDeclPTG _currn));
extern void _VS1rPTGDecl ELI_ARG((_TPPrPTGDecl _currn));
extern void _VS2rPTGDecl ELI_ARG((_TPPrPTGDecl _currn));
extern void _VS3rPTGDecl ELI_ARG((_TPPrPTGDecl _currn));
extern void _VS4rPTGDecl ELI_ARG((_TPPrPTGDecl _currn));
extern void _VS5rPTGDecl ELI_ARG((_TPPrPTGDecl _currn));
extern void _VS6rPTGDecl ELI_ARG((_TPPrPTGDecl _currn));
extern void _VS1rPTGDefList2 ELI_ARG((_TPPrPTGDefList2 _currn));
extern void _VS2rPTGDefList2 ELI_ARG((_TPPrPTGDefList2 _currn));
extern void _VS3rPTGDefList2 ELI_ARG((_TPPrPTGDefList2 _currn));
extern void _VS4rPTGDefList2 ELI_ARG((_TPPrPTGDefList2 _currn));
extern void _VS5rPTGDefList2 ELI_ARG((_TPPrPTGDefList2 _currn));
extern void _VS6rPTGDefList2 ELI_ARG((_TPPrPTGDefList2 _currn));
#define _VS1rNoPTGDefs _VS0Empty

#define _VS2rNoPTGDefs _VS0Empty

#define _VS3rNoPTGDefs _VS0Empty

#define _VS4rNoPTGDefs _VS0Empty

#define _VS5rNoPTGDefs _VS0Empty

extern void _VS6rNoPTGDefs ELI_ARG((_TPPrNoPTGDefs _currn));
extern void _VS1rPTGDefinition ELI_ARG((_TPPrPTGDefinition _currn));
extern void _VS2rPTGDefinition ELI_ARG((_TPPrPTGDefinition _currn));
extern void _VS3rPTGDefinition ELI_ARG((_TPPrPTGDefinition _currn));
extern void _VS4rPTGDefinition ELI_ARG((_TPPrPTGDefinition _currn));
extern void _VS5rPTGDefinition ELI_ARG((_TPPrPTGDefinition _currn));
extern void _VS6rPTGDefinition ELI_ARG((_TPPrPTGDefinition _currn));
extern void _VS1rPTGDefIdentifier ELI_ARG((_TPPrPTGDefIdentifier _currn));
extern void _VS2rPTGDefIdentifier ELI_ARG((_TPPrPTGDefIdentifier _currn));
extern void _VS3rPTGDefIdentifier ELI_ARG((_TPPrPTGDefIdentifier _currn));
extern void _VS1rPTGDefTypename ELI_ARG((_TPPrPTGDefTypename _currn));
#define _VS2rPTGDefTypename _VS0Empty

extern void _VS3rPTGDefTypename ELI_ARG((_TPPrPTGDefTypename _currn));
#define _VS1rPTGPatternList2 _VS1rPTGDefList2

#define _VS2rPTGPatternList2 _VS2rPTGDefList2

#define _VS3rPTGPatternList2 _VS3rPTGDefList2

extern void _VS4rPTGPatternList2 ELI_ARG((_TPPrPTGPatternList2 _currn,IndexType* _AS0_const46,D_ptg_inserts* _AS0_const45));
extern void _VS5rPTGPatternList2 ELI_ARG((_TPPrPTGPatternList2 _currn,IndexType* _AS0_const46,D_ptg_inserts* _AS0_const45));
extern void _VS6rPTGPatternList2 ELI_ARG((_TPPrPTGPatternList2 _currn,IndexType* _AS0_const46,D_ptg_inserts* _AS0_const45,PTGNode* _AS0_const44,bool* _AS0_const42,StringTableKey* _AS0_const10));
#define _VS1rPTGPatternEmpty _VS1rPTGDecl

#define _VS2rPTGPatternEmpty _VS2rPTGDecl

#define _VS3rPTGPatternEmpty _VS3rPTGDecl

extern void _VS4rPTGPatternEmpty ELI_ARG((_TPPrPTGPatternEmpty _currn,IndexType* _AS0_const46,D_ptg_inserts* _AS0_const45));
extern void _VS5rPTGPatternEmpty ELI_ARG((_TPPrPTGPatternEmpty _currn,IndexType* _AS0_const46,D_ptg_inserts* _AS0_const45));
extern void _VS6rPTGPatternEmpty ELI_ARG((_TPPrPTGPatternEmpty _currn,IndexType* _AS0_const46,D_ptg_inserts* _AS0_const45,PTGNode* _AS0_const44,bool* _AS0_const42,StringTableKey* _AS0_const10));
#define _VS1rPTGPatternInsert _VS1rPTGDecl

#define _VS2rPTGPatternInsert _VS2rPTGDecl

#define _VS3rPTGPatternInsert _VS3rPTGDecl

extern void _VS4rPTGPatternInsert ELI_ARG((_TPPrPTGPatternInsert _currn));
#define _VS5rPTGPatternInsert _VS5rPTGDecl

extern void _VS6rPTGPatternInsert ELI_ARG((_TPPrPTGPatternInsert _currn));
#define _VS1rPTGPatternCall _VS1rPTGDecl

#define _VS2rPTGPatternCall _VS2rPTGDecl

#define _VS3rPTGPatternCall _VS3rPTGDecl

extern void _VS4rPTGPatternCall ELI_ARG((_TPPrPTGPatternCall _currn));
#define _VS5rPTGPatternCall _VS5rPTGDecl

extern void _VS6rPTGPatternCall ELI_ARG((_TPPrPTGPatternCall _currn));
#define _VS1rPTGPatternString _VS1rPTGDecl

#define _VS2rPTGPatternString _VS0Empty

#define _VS3rPTGPatternString _VS0Empty

extern void _VS4rPTGPatternString ELI_ARG((_TPPrPTGPatternString _currn));
#define _VS5rPTGPatternString _VS2rPTGDecl

extern void _VS6rPTGPatternString ELI_ARG((_TPPrPTGPatternString _currn));
#define _VS1rPTGPatternOptional _VS1rPTGDecl

#define _VS2rPTGPatternOptional _VS2rPTGDecl

#define _VS3rPTGPatternOptional _VS3rPTGDecl

extern void _VS4rPTGPatternOptional ELI_ARG((_TPPrPTGPatternOptional _currn));
#define _VS5rPTGPatternOptional _VS5rPTGDecl

extern void _VS6rPTGPatternOptional ELI_ARG((_TPPrPTGPatternOptional _currn));
extern void _VS1rPTGInsertion ELI_ARG((_TPPrPTGInsertion _currn));
extern void _VS2rPTGInsertion ELI_ARG((_TPPrPTGInsertion _currn));
extern void _VS3rPTGInsertion ELI_ARG((_TPPrPTGInsertion _currn));
extern void _VS4rPTGInsertion ELI_ARG((_TPPrPTGInsertion _currn));
extern void _VS5rPTGInsertion ELI_ARG((_TPPrPTGInsertion _currn));
extern void _VS6rPTGInsertion ELI_ARG((_TPPrPTGInsertion _currn));
extern void _VS1rPTGNoIndex ELI_ARG((_TPPrPTGNoIndex _currn));
#define _VS2rPTGNoIndex _VS0Empty

extern void _VS1rPTGHasIndex ELI_ARG((_TPPrPTGHasIndex _currn));
#define _VS2rPTGHasIndex _VS2rPTGDecl

extern void _VS1rPTGIndex ELI_ARG((_TPPrPTGIndex _currn));
#define _VS2rPTGIndex _VS2rPTGDecl

#define _VS1rPTGTypeNode _VS0Empty

#define _VS2rPTGTypeNode _VS0Empty

extern void _VS3rPTGTypeNode ELI_ARG((_TPPrPTGTypeNode _currn));
extern void _VS4rPTGTypeNode ELI_ARG((_TPPrPTGTypeNode _currn));
extern void _VS1rPTGTypeEli ELI_ARG((_TPPrPTGTypeEli _currn));
extern void _VS2rPTGTypeEli ELI_ARG((_TPPrPTGTypeEli _currn));
extern void _VS3rPTGTypeEli ELI_ARG((_TPPrPTGTypeEli _currn));
extern void _VS4rPTGTypeEli ELI_ARG((_TPPrPTGTypeEli _currn));
extern void _VS1rPTGTypeGeneral ELI_ARG((_TPPrPTGTypeGeneral _currn));
extern void _VS2rPTGTypeGeneral ELI_ARG((_TPPrPTGTypeGeneral _currn));
extern void _VS3rPTGTypeGeneral ELI_ARG((_TPPrPTGTypeGeneral _currn));
extern void _VS4rPTGTypeGeneral ELI_ARG((_TPPrPTGTypeGeneral _currn));
extern void _VS1rEliTypeInt ELI_ARG((_TPPrEliTypeInt _currn));
#define _VS2rEliTypeInt _VS0Empty

extern void _VS1rEliTypeString ELI_ARG((_TPPrEliTypeString _currn));
#define _VS2rEliTypeString _VS0Empty

extern void _VS1rEliTypePointer ELI_ARG((_TPPrEliTypePointer _currn));
#define _VS2rEliTypePointer _VS0Empty

extern void _VS1rEliTypeChar ELI_ARG((_TPPrEliTypeChar _currn));
#define _VS2rEliTypeChar _VS0Empty

extern void _VS1rEliTypeDouble ELI_ARG((_TPPrEliTypeDouble _currn));
#define _VS2rEliTypeDouble _VS0Empty

extern void _VS1rEliTypeFloat ELI_ARG((_TPPrEliTypeFloat _currn));
#define _VS2rEliTypeFloat _VS0Empty

extern void _VS1rEliTypeLong ELI_ARG((_TPPrEliTypeLong _currn));
#define _VS2rEliTypeLong _VS0Empty

extern void _VS1rEliTypeShort ELI_ARG((_TPPrEliTypeShort _currn));
#define _VS2rEliTypeShort _VS0Empty

extern void _VS1rTypeRefSimple ELI_ARG((_TPPrTypeRefSimple _currn));
extern void _VS2rTypeRefSimple ELI_ARG((_TPPrTypeRefSimple _currn));
extern void _VS3rTypeRefSimple ELI_ARG((_TPPrTypeRefSimple _currn));
extern void _VS4rTypeRefSimple ELI_ARG((_TPPrTypeRefSimple _currn));
extern void _VS5rTypeRefSimple ELI_ARG((_TPPrTypeRefSimple _currn));
#define _VS1rPTGCall _VS1rPTGDefList2

extern void _VS2rPTGCall ELI_ARG((_TPPrPTGCall _currn));
extern void _VS3rPTGCall ELI_ARG((_TPPrPTGCall _currn));
extern void _VS4rPTGCall ELI_ARG((_TPPrPTGCall _currn));
extern void _VS5rPTGCall ELI_ARG((_TPPrPTGCall _currn));
extern void _VS6rPTGCall ELI_ARG((_TPPrPTGCall _currn));
extern void _VS1rPTGFnRefIdentifier ELI_ARG((_TPPrPTGFnRefIdentifier _currn));
extern void _VS2rPTGFnRefIdentifier ELI_ARG((_TPPrPTGFnRefIdentifier _currn));
extern void _VS1rPTGFnRefTypename ELI_ARG((_TPPrPTGFnRefTypename _currn));
extern void _VS2rPTGFnRefTypename ELI_ARG((_TPPrPTGFnRefTypename _currn));
#define _VS1rPTGCallParamList2 _VS1rPTGDefList2

#define _VS2rPTGCallParamList2 _VS2rPTGDefList2

#define _VS3rPTGCallParamList2 _VS3rPTGDefList2

extern void _VS4rPTGCallParamList2 ELI_ARG((_TPPrPTGCallParamList2 _currn));
#define _VS5rPTGCallParamList2 _VS5rPTGDefList2

extern void _VS6rPTGCallParamList2 ELI_ARG((_TPPrPTGCallParamList2 _currn));
#define _VS1rPTGCallParamEmpty _VS0Empty

#define _VS2rPTGCallParamEmpty _VS0Empty

#define _VS3rPTGCallParamEmpty _VS0Empty

extern void _VS4rPTGCallParamEmpty ELI_ARG((_TPPrPTGCallParamEmpty _currn));
#define _VS5rPTGCallParamEmpty _VS0Empty

extern void _VS6rPTGCallParamEmpty ELI_ARG((_TPPrPTGCallParamEmpty _currn));
#define _VS1rPTGCallInsertion _VS1rPTGDecl

#define _VS2rPTGCallInsertion _VS2rPTGDecl

#define _VS3rPTGCallInsertion _VS3rPTGDecl

extern void _VS4rPTGCallInsertion ELI_ARG((_TPPrPTGCallInsertion _currn));
#define _VS5rPTGCallInsertion _VS5rPTGDecl

extern void _VS6rPTGCallInsertion ELI_ARG((_TPPrPTGCallInsertion _currn));
#define _VS1rPTGOptionalPatterns _VS1rPTGDecl

#define _VS2rPTGOptionalPatterns _VS2rPTGDecl

#define _VS3rPTGOptionalPatterns _VS3rPTGDecl

extern void _VS4rPTGOptionalPatterns ELI_ARG((_TPPrPTGOptionalPatterns _currn));
extern void _VS5rPTGOptionalPatterns ELI_ARG((_TPPrPTGOptionalPatterns _currn));
extern void _VS6rPTGOptionalPatterns ELI_ARG((_TPPrPTGOptionalPatterns _currn));
extern void _VS1rDeclPDL ELI_ARG((_TPPrDeclPDL _currn));
extern void _VS2rDeclPDL ELI_ARG((_TPPrDeclPDL _currn));
#define _VS3rDeclPDL _VS0Empty

extern void _VS4rDeclPDL ELI_ARG((_TPPrDeclPDL _currn));
extern void _VS5rDeclPDL ELI_ARG((_TPPrDeclPDL _currn));
#define _VS6rDeclPDL _VS0Empty

#define _VS7rDeclPDL _VS0Empty

extern void _VS8rDeclPDL ELI_ARG((_TPPrDeclPDL _currn));
#define _VS9rDeclPDL _VS0Empty

extern void _VS10rDeclPDL ELI_ARG((_TPPrDeclPDL _currn));
#define _VS1rPDLDecl _VS1rPTGDecl

#define _VS2rPDLDecl _VS2rPTGDecl

#define _VS3rPDLDecl _VS3rPTGDecl

#define _VS4rPDLDecl _VS4rPTGDecl

#define _VS5rPDLDecl _VS5rPTGDecl

extern void _VS6rPDLDecl ELI_ARG((_TPPrPDLDecl _currn));
#define _VS1rPDLDefinitionList2 _VS1rPTGDefList2

#define _VS2rPDLDefinitionList2 _VS2rPTGDefList2

#define _VS3rPDLDefinitionList2 _VS3rPTGDefList2

#define _VS4rPDLDefinitionList2 _VS4rPTGDefList2

#define _VS5rPDLDefinitionList2 _VS5rPTGDefList2

extern void _VS6rPDLDefinitionList2 ELI_ARG((_TPPrPDLDefinitionList2 _currn));
#define _VS1rPDLDefinitionListEmpty _VS0Empty

#define _VS2rPDLDefinitionListEmpty _VS0Empty

#define _VS3rPDLDefinitionListEmpty _VS0Empty

#define _VS4rPDLDefinitionListEmpty _VS0Empty

#define _VS5rPDLDefinitionListEmpty _VS0Empty

extern void _VS6rPDLDefinitionListEmpty ELI_ARG((_TPPrPDLDefinitionListEmpty _currn));
#define _VS1rPDLLoad _VS0Empty

#define _VS2rPDLLoad _VS0Empty

#define _VS3rPDLLoad _VS0Empty

#define _VS4rPDLLoad _VS0Empty

#define _VS5rPDLLoad _VS1rPTGDecl

extern void _VS6rPDLLoad ELI_ARG((_TPPrPDLLoad _currn));
#define _VS1rPDLDefInclude _VS0Empty

#define _VS2rPDLDefInclude _VS0Empty

#define _VS3rPDLDefInclude _VS0Empty

#define _VS4rPDLDefInclude _VS0Empty

#define _VS5rPDLDefInclude _VS1rPTGDecl

extern void _VS6rPDLDefInclude ELI_ARG((_TPPrPDLDefInclude _currn));
extern void _VS1rPDLInclude ELI_ARG((_TPPrPDLInclude _currn));
extern void _VS2rPDLInclude ELI_ARG((_TPPrPDLInclude _currn));
#define _VS1rPDLRealDef _VS1rPTGDecl

#define _VS2rPDLRealDef _VS2rPTGDecl

#define _VS3rPDLRealDef _VS3rPTGDecl

#define _VS4rPDLRealDef _VS4rPTGDecl

#define _VS5rPDLRealDef _VS0Empty

extern void _VS6rPDLRealDef ELI_ARG((_TPPrPDLRealDef _currn));
extern void _VS1rPDLDefProperties ELI_ARG((_TPPrPDLDefProperties _currn));
extern void _VS2rPDLDefProperties ELI_ARG((_TPPrPDLDefProperties _currn));
extern void _VS3rPDLDefProperties ELI_ARG((_TPPrPDLDefProperties _currn));
extern void _VS4rPDLDefProperties ELI_ARG((_TPPrPDLDefProperties _currn));
extern void _VS5rPDLDefProperties ELI_ARG((_TPPrPDLDefProperties _currn));
#define _VS1rPDLDefNameList2 _VS1rPTGDefList2

#define _VS2rPDLDefNameList2 _VS2rPTGDefList2

extern void _VS3rPDLDefNameList2 ELI_ARG((_TPPrPDLDefNameList2 _currn));
extern void _VS4rPDLDefNameList2 ELI_ARG((_TPPrPDLDefNameList2 _currn));
#define _VS1rPDLDefNameList1 _VS1rPTGDecl

#define _VS2rPDLDefNameList1 _VS2rPTGDecl

extern void _VS3rPDLDefNameList1 ELI_ARG((_TPPrPDLDefNameList1 _currn));
extern void _VS4rPDLDefNameList1 ELI_ARG((_TPPrPDLDefNameList1 _currn));
extern void _VS1rPDLDefTypename ELI_ARG((_TPPrPDLDefTypename _currn));
#define _VS2rPDLDefTypename _VS0Empty

extern void _VS3rPDLDefTypename ELI_ARG((_TPPrPDLDefTypename _currn));
extern void _VS4rPDLDefTypename ELI_ARG((_TPPrPDLDefTypename _currn));
extern void _VS1rPDLDefIdentifier ELI_ARG((_TPPrPDLDefIdentifier _currn));
extern void _VS2rPDLDefIdentifier ELI_ARG((_TPPrPDLDefIdentifier _currn));
extern void _VS3rPDLDefIdentifier ELI_ARG((_TPPrPDLDefIdentifier _currn));
extern void _VS4rPDLDefIdentifier ELI_ARG((_TPPrPDLDefIdentifier _currn));
extern void _VS1rPDLReferenceType ELI_ARG((_TPPrPDLReferenceType _currn));
extern void _VS2rPDLReferenceType ELI_ARG((_TPPrPDLReferenceType _currn));
extern void _VS3rPDLReferenceType ELI_ARG((_TPPrPDLReferenceType _currn));
extern void _VS4rPDLReferenceType ELI_ARG((_TPPrPDLReferenceType _currn));
extern void _VS1rDeclCopySource ELI_ARG((_TPPrDeclCopySource _currn));
#define _VS2rDeclCopySource _VS0Empty

#define _VS3rDeclCopySource _VS0Empty

#define _VS4rDeclCopySource _VS0Empty

#define _VS5rDeclCopySource _VS0Empty

#define _VS6rDeclCopySource _VS0Empty

#define _VS7rDeclCopySource _VS0Empty

extern void _VS8rDeclCopySource ELI_ARG((_TPPrDeclCopySource _currn));
#define _VS9rDeclCopySource _VS0Empty

extern void _VS10rDeclCopySource ELI_ARG((_TPPrDeclCopySource _currn));
extern void _VS1rDeclCopyHead ELI_ARG((_TPPrDeclCopyHead _currn));
#define _VS2rDeclCopyHead _VS0Empty

#define _VS3rDeclCopyHead _VS0Empty

#define _VS4rDeclCopyHead _VS0Empty

#define _VS5rDeclCopyHead _VS0Empty

#define _VS6rDeclCopyHead _VS0Empty

#define _VS7rDeclCopyHead _VS0Empty

extern void _VS8rDeclCopyHead ELI_ARG((_TPPrDeclCopyHead _currn));
#define _VS9rDeclCopyHead _VS0Empty

extern void _VS10rDeclCopyHead ELI_ARG((_TPPrDeclCopyHead _currn));
extern void _VS1rDeclCopyLido ELI_ARG((_TPPrDeclCopyLido _currn));
#define _VS2rDeclCopyLido _VS0Empty

#define _VS3rDeclCopyLido _VS0Empty

#define _VS4rDeclCopyLido _VS0Empty

#define _VS5rDeclCopyLido _VS0Empty

#define _VS6rDeclCopyLido _VS0Empty

#define _VS7rDeclCopyLido _VS0Empty

extern void _VS8rDeclCopyLido ELI_ARG((_TPPrDeclCopyLido _currn));
#define _VS9rDeclCopyLido _VS0Empty

extern void _VS10rDeclCopyLido ELI_ARG((_TPPrDeclCopyLido _currn));
extern void _VS1rCopySourcePre ELI_ARG((_TPPrCopySourcePre _currn));
extern void _VS1rCopySourcePost ELI_ARG((_TPPrCopySourcePost _currn));
extern void _VS1rCopyHeadPre ELI_ARG((_TPPrCopyHeadPre _currn));
extern void _VS1rCopyHeadPost ELI_ARG((_TPPrCopyHeadPost _currn));
extern void _VS1rCopyLido ELI_ARG((_TPPrCopyLido _currn));
extern void _VS1rDeclTypedef ELI_ARG((_TPPrDeclTypedef _currn));
extern void _VS2rDeclTypedef ELI_ARG((_TPPrDeclTypedef _currn));
extern void _VS3rDeclTypedef ELI_ARG((_TPPrDeclTypedef _currn));
extern void _VS4rDeclTypedef ELI_ARG((_TPPrDeclTypedef _currn));
#define _VS5rDeclTypedef _VS0Empty

#define _VS6rDeclTypedef _VS0Empty

#define _VS7rDeclTypedef _VS0Empty

extern void _VS8rDeclTypedef ELI_ARG((_TPPrDeclTypedef _currn));
#define _VS9rDeclTypedef _VS0Empty

extern void _VS10rDeclTypedef ELI_ARG((_TPPrDeclTypedef _currn));
extern void _VS1rTypedefDecl ELI_ARG((_TPPrTypedefDecl _currn));
extern void _VS2rTypedefDecl ELI_ARG((_TPPrTypedefDecl _currn));
extern void _VS3rTypedefDecl ELI_ARG((_TPPrTypedefDecl _currn));
extern void _VS4rTypedefDecl ELI_ARG((_TPPrTypedefDecl _currn));
extern void _VS5rTypedefDecl ELI_ARG((_TPPrTypedefDecl _currn));
extern void _VS1rTypingReference ELI_ARG((_TPPrTypingReference _currn));
extern void _VS2rTypingReference ELI_ARG((_TPPrTypingReference _currn));
extern void _VS3rTypingReference ELI_ARG((_TPPrTypingReference _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS4rTypingReference ELI_ARG((_TPPrTypingReference _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS5rTypingReference ELI_ARG((_TPPrTypingReference _currn,Environment* _AS0type_names_chn2_pre,StringTableKey* _AS0_const10));
extern void _VS1rTypingList ELI_ARG((_TPPrTypingList _currn));
extern void _VS2rTypingList ELI_ARG((_TPPrTypingList _currn));
extern void _VS3rTypingList ELI_ARG((_TPPrTypingList _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS4rTypingList ELI_ARG((_TPPrTypingList _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS5rTypingList ELI_ARG((_TPPrTypingList _currn,Environment* _AS0type_names_chn2_pre,StringTableKey* _AS0_const10));
extern void _VS1rTypingBasic ELI_ARG((_TPPrTypingBasic _currn));
#define _VS2rTypingBasic _VS0Empty

extern void _VS3rTypingBasic ELI_ARG((_TPPrTypingBasic _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS4rTypingBasic ELI_ARG((_TPPrTypingBasic _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS5rTypingBasic ELI_ARG((_TPPrTypingBasic _currn,Environment* _AS0type_names_chn2_pre,StringTableKey* _AS0_const10));
extern void _VS1rTypingTuple ELI_ARG((_TPPrTypingTuple _currn));
extern void _VS2rTypingTuple ELI_ARG((_TPPrTypingTuple _currn));
extern void _VS3rTypingTuple ELI_ARG((_TPPrTypingTuple _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS4rTypingTuple ELI_ARG((_TPPrTypingTuple _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS5rTypingTuple ELI_ARG((_TPPrTypingTuple _currn,Environment* _AS0type_names_chn2_pre,StringTableKey* _AS0_const10));
extern void _VS1rTypingMap ELI_ARG((_TPPrTypingMap _currn));
extern void _VS2rTypingMap ELI_ARG((_TPPrTypingMap _currn));
extern void _VS3rTypingMap ELI_ARG((_TPPrTypingMap _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS4rTypingMap ELI_ARG((_TPPrTypingMap _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS5rTypingMap ELI_ARG((_TPPrTypingMap _currn,Environment* _AS0type_names_chn2_pre,StringTableKey* _AS0_const10));
extern void _VS1rTupleType ELI_ARG((_TPPrTupleType _currn));
extern void _VS2rTupleType ELI_ARG((_TPPrTupleType _currn));
extern void _VS3rTupleType ELI_ARG((_TPPrTupleType _currn));
extern void _VS4rTupleType ELI_ARG((_TPPrTupleType _currn));
extern void _VS5rTupleType ELI_ARG((_TPPrTupleType _currn));
extern void _VS1rTypeConList2 ELI_ARG((_TPPrTypeConList2 _currn,StringTableKey* _AS0sym,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS2rTypeConList2 ELI_ARG((_TPPrTypeConList2 _currn,StringTableKey* _AS0sym,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS3rTypeConList2 ELI_ARG((_TPPrTypeConList2 _currn,tStringType* _AS0name,StringTableKey* _AS0sym,bool* _AS0_const114,PTGNode* _AS0_const78,int* _AS0_const75,StringTableKeyList* _AS0_const74,bool* _AS0_const70));
extern void _VS4rTypeConList2 ELI_ARG((_TPPrTypeConList2 _currn,tStringType* _AS0name,StringTableKey* _AS0sym,bool* _AS0_const114,PTGNode* _AS0_const78,int* _AS0_const75,StringTableKeyList* _AS0_const74,bool* _AS0_const70));
extern void _VS5rTypeConList2 ELI_ARG((_TPPrTypeConList2 _currn,tStringType* _AS0name,StringTableKey* _AS0sym,int* _AS0_const142,bool* _AS0_const141,bool* _AS0_const114,PTGNode* _AS0_const101,PTGNode* _AS0_const78,int* _AS0_const75,StringTableKeyList* _AS0_const74,bool* _AS0_const70,BindingList* _AS0_const66,bool* _AS0_const60,bool* _AS0_const42,StringTableKey* _AS0_const10));
extern void _VS1rTypeConList1 ELI_ARG((_TPPrTypeConList1 _currn,StringTableKey* _AS0sym,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS2rTypeConList1 ELI_ARG((_TPPrTypeConList1 _currn,StringTableKey* _AS0sym,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS3rTypeConList1 ELI_ARG((_TPPrTypeConList1 _currn,tStringType* _AS0name,StringTableKey* _AS0sym,bool* _AS0_const114,PTGNode* _AS0_const78,int* _AS0_const75,StringTableKeyList* _AS0_const74,bool* _AS0_const70));
extern void _VS4rTypeConList1 ELI_ARG((_TPPrTypeConList1 _currn,tStringType* _AS0name,StringTableKey* _AS0sym,bool* _AS0_const114,PTGNode* _AS0_const78,int* _AS0_const75,StringTableKeyList* _AS0_const74,bool* _AS0_const70));
extern void _VS5rTypeConList1 ELI_ARG((_TPPrTypeConList1 _currn,tStringType* _AS0name,StringTableKey* _AS0sym,int* _AS0_const142,bool* _AS0_const141,bool* _AS0_const114,PTGNode* _AS0_const101,PTGNode* _AS0_const78,int* _AS0_const75,StringTableKeyList* _AS0_const74,bool* _AS0_const70,BindingList* _AS0_const66,bool* _AS0_const60,bool* _AS0_const42,StringTableKey* _AS0_const10));
extern void _VS1rTypeListConstruction ELI_ARG((_TPPrTypeListConstruction _currn));
extern void _VS2rTypeListConstruction ELI_ARG((_TPPrTypeListConstruction _currn));
extern void _VS3rTypeListConstruction ELI_ARG((_TPPrTypeListConstruction _currn));
extern void _VS4rTypeListConstruction ELI_ARG((_TPPrTypeListConstruction _currn));
extern void _VS5rTypeListConstruction ELI_ARG((_TPPrTypeListConstruction _currn));
extern void _VS1rConstructSimple ELI_ARG((_TPPrConstructSimple _currn,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS2rConstructSimple ELI_ARG((_TPPrConstructSimple _currn,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS3rConstructSimple ELI_ARG((_TPPrConstructSimple _currn,tStringType* _AS0name,PTGNode* _AS0t_decl_code,bool* _AS0_const114,int* _AS0_const75,bool* _AS0_const70));
extern void _VS4rConstructSimple ELI_ARG((_TPPrConstructSimple _currn,tStringType* _AS0name,PTGNode* _AS0t_decl_code,bool* _AS0_const114,int* _AS0_const75,bool* _AS0_const70));
extern void _VS5rConstructSimple ELI_ARG((_TPPrConstructSimple _currn,tStringType* _AS0name,PTGNode* _AS0t_decl_code,int* _AS0_const142,bool* _AS0_const141,bool* _AS0_const114,PTGNode* _AS0_const101,int* _AS0_const75,bool* _AS0_const70,BindingList* _AS0_const66,bool* _AS0_const60,bool* _AS0_const42,StringTableKey* _AS0_const10));
extern void _VS1rTypeMapOf ELI_ARG((_TPPrTypeMapOf _currn));
extern void _VS2rTypeMapOf ELI_ARG((_TPPrTypeMapOf _currn));
extern void _VS3rTypeMapOf ELI_ARG((_TPPrTypeMapOf _currn));
extern void _VS4rTypeMapOf ELI_ARG((_TPPrTypeMapOf _currn));
extern void _VS5rTypeMapOf ELI_ARG((_TPPrTypeMapOf _currn));
extern void _VS1rDeclData ELI_ARG((_TPPrDeclData _currn));
extern void _VS2rDeclData ELI_ARG((_TPPrDeclData _currn));
extern void _VS3rDeclData ELI_ARG((_TPPrDeclData _currn));
extern void _VS4rDeclData ELI_ARG((_TPPrDeclData _currn));
#define _VS5rDeclData _VS0Empty

#define _VS6rDeclData _VS0Empty

#define _VS7rDeclData _VS0Empty

extern void _VS8rDeclData ELI_ARG((_TPPrDeclData _currn));
#define _VS9rDeclData _VS0Empty

extern void _VS10rDeclData ELI_ARG((_TPPrDeclData _currn));
extern void _VS1rDataDecl ELI_ARG((_TPPrDataDecl _currn));
extern void _VS2rDataDecl ELI_ARG((_TPPrDataDecl _currn));
extern void _VS3rDataDecl ELI_ARG((_TPPrDataDecl _currn));
extern void _VS4rDataDecl ELI_ARG((_TPPrDataDecl _currn));
extern void _VS5rDataDecl ELI_ARG((_TPPrDataDecl _currn));
extern void _VS1rDataName ELI_ARG((_TPPrDataName _currn));
extern void _VS2rDataName ELI_ARG((_TPPrDataName _currn));
extern void _VS3rDataName ELI_ARG((_TPPrDataName _currn));
extern void _VS4rDataName ELI_ARG((_TPPrDataName _currn));
extern void _VS5rDataName ELI_ARG((_TPPrDataName _currn));
extern void _VS1rDataConstructorList2 ELI_ARG((_TPPrDataConstructorList2 _currn));
extern void _VS2rDataConstructorList2 ELI_ARG((_TPPrDataConstructorList2 _currn));
extern void _VS3rDataConstructorList2 ELI_ARG((_TPPrDataConstructorList2 _currn));
extern void _VS4rDataConstructorList2 ELI_ARG((_TPPrDataConstructorList2 _currn));
extern void _VS5rDataConstructorList2 ELI_ARG((_TPPrDataConstructorList2 _currn));
extern void _VS1rDataConstructorList1 ELI_ARG((_TPPrDataConstructorList1 _currn));
extern void _VS2rDataConstructorList1 ELI_ARG((_TPPrDataConstructorList1 _currn));
extern void _VS3rDataConstructorList1 ELI_ARG((_TPPrDataConstructorList1 _currn));
extern void _VS4rDataConstructorList1 ELI_ARG((_TPPrDataConstructorList1 _currn));
extern void _VS5rDataConstructorList1 ELI_ARG((_TPPrDataConstructorList1 _currn));
extern void _VS1rDataConstructorIsSimple ELI_ARG((_TPPrDataConstructorIsSimple _currn));
#define _VS2rDataConstructorIsSimple _VS2rPTGDecl

extern void _VS3rDataConstructorIsSimple ELI_ARG((_TPPrDataConstructorIsSimple _currn));
#define _VS4rDataConstructorIsSimple _VS0Empty

extern void _VS5rDataConstructorIsSimple ELI_ARG((_TPPrDataConstructorIsSimple _currn));
extern void _VS1rDataConstructorIsComplex ELI_ARG((_TPPrDataConstructorIsComplex _currn));
#define _VS2rDataConstructorIsComplex _VS2rPTGDecl

extern void _VS3rDataConstructorIsComplex ELI_ARG((_TPPrDataConstructorIsComplex _currn));
#define _VS4rDataConstructorIsComplex _VS4rPTGDecl

extern void _VS5rDataConstructorIsComplex ELI_ARG((_TPPrDataConstructorIsComplex _currn));
extern void _VS1rDataConstructorComplex ELI_ARG((_TPPrDataConstructorComplex _currn));
extern void _VS2rDataConstructorComplex ELI_ARG((_TPPrDataConstructorComplex _currn));
extern void _VS3rDataConstructorComplex ELI_ARG((_TPPrDataConstructorComplex _currn));
extern void _VS4rDataConstructorComplex ELI_ARG((_TPPrDataConstructorComplex _currn));
extern void _VS5rDataConstructorComplex ELI_ARG((_TPPrDataConstructorComplex _currn));
extern void _VS1rDataConstructorArgumentList2 ELI_ARG((_TPPrDataConstructorArgumentList2 _currn));
extern void _VS2rDataConstructorArgumentList2 ELI_ARG((_TPPrDataConstructorArgumentList2 _currn));
extern void _VS3rDataConstructorArgumentList2 ELI_ARG((_TPPrDataConstructorArgumentList2 _currn));
extern void _VS4rDataConstructorArgumentList2 ELI_ARG((_TPPrDataConstructorArgumentList2 _currn));
extern void _VS5rDataConstructorArgumentList2 ELI_ARG((_TPPrDataConstructorArgumentList2 _currn));
extern void _VS1rDataConstructorArgumentList1 ELI_ARG((_TPPrDataConstructorArgumentList1 _currn));
extern void _VS2rDataConstructorArgumentList1 ELI_ARG((_TPPrDataConstructorArgumentList1 _currn));
extern void _VS3rDataConstructorArgumentList1 ELI_ARG((_TPPrDataConstructorArgumentList1 _currn));
extern void _VS4rDataConstructorArgumentList1 ELI_ARG((_TPPrDataConstructorArgumentList1 _currn));
extern void _VS5rDataConstructorArgumentList1 ELI_ARG((_TPPrDataConstructorArgumentList1 _currn));
extern void _VS1rDataConstructorArgument ELI_ARG((_TPPrDataConstructorArgument _currn));
extern void _VS2rDataConstructorArgument ELI_ARG((_TPPrDataConstructorArgument _currn));
extern void _VS3rDataConstructorArgument ELI_ARG((_TPPrDataConstructorArgument _currn));
extern void _VS4rDataConstructorArgument ELI_ARG((_TPPrDataConstructorArgument _currn));
extern void _VS5rDataConstructorArgument ELI_ARG((_TPPrDataConstructorArgument _currn));
extern void _VS1rDataConstructorSimple ELI_ARG((_TPPrDataConstructorSimple _currn));
extern void _VS2rDataConstructorSimple ELI_ARG((_TPPrDataConstructorSimple _currn));
extern void _VS1rDataConstructorDefId ELI_ARG((_TPPrDataConstructorDefId _currn));
extern void _VS2rDataConstructorDefId ELI_ARG((_TPPrDataConstructorDefId _currn));
extern void _VS1rDataDefId ELI_ARG((_TPPrDataDefId _currn));
extern void _VS1rTypeDefId ELI_ARG((_TPPrTypeDefId _currn));
extern void _VS2rTypeDefId ELI_ARG((_TPPrTypeDefId _currn));
extern void _VS1rDataRecordDecl ELI_ARG((_TPPrDataRecordDecl _currn));
extern void _VS2rDataRecordDecl ELI_ARG((_TPPrDataRecordDecl _currn));
extern void _VS3rDataRecordDecl ELI_ARG((_TPPrDataRecordDecl _currn));
extern void _VS4rDataRecordDecl ELI_ARG((_TPPrDataRecordDecl _currn));
extern void _VS5rDataRecordDecl ELI_ARG((_TPPrDataRecordDecl _currn));
extern void _VS1rDataRecordArgumentList2 ELI_ARG((_TPPrDataRecordArgumentList2 _currn));
#define _VS2rDataRecordArgumentList2 _VS2rPTGDefList2

#define _VS3rDataRecordArgumentList2 _VS3rPTGDefList2

#define _VS4rDataRecordArgumentList2 _VS4rPTGDefList2

extern void _VS5rDataRecordArgumentList2 ELI_ARG((_TPPrDataRecordArgumentList2 _currn));
extern void _VS1rDataRecordArgumentList1 ELI_ARG((_TPPrDataRecordArgumentList1 _currn));
#define _VS2rDataRecordArgumentList1 _VS2rPTGDecl

#define _VS3rDataRecordArgumentList1 _VS3rPTGDecl

#define _VS4rDataRecordArgumentList1 _VS4rPTGDecl

extern void _VS5rDataRecordArgumentList1 ELI_ARG((_TPPrDataRecordArgumentList1 _currn));
extern void _VS1rDataRecordArgument ELI_ARG((_TPPrDataRecordArgument _currn));
extern void _VS2rDataRecordArgument ELI_ARG((_TPPrDataRecordArgument _currn));
extern void _VS3rDataRecordArgument ELI_ARG((_TPPrDataRecordArgument _currn));
extern void _VS4rDataRecordArgument ELI_ARG((_TPPrDataRecordArgument _currn));
extern void _VS5rDataRecordArgument ELI_ARG((_TPPrDataRecordArgument _currn));
extern void _VS1rDataRecordArgumentDefId ELI_ARG((_TPPrDataRecordArgumentDefId _currn));
extern void _VS2rDataRecordArgumentDefId ELI_ARG((_TPPrDataRecordArgumentDefId _currn));
extern void _VS3rDataRecordArgumentDefId ELI_ARG((_TPPrDataRecordArgumentDefId _currn));
extern void _VS1rDeclFunctionType ELI_ARG((_TPPrDeclFunctionType _currn));
extern void _VS2rDeclFunctionType ELI_ARG((_TPPrDeclFunctionType _currn));
#define _VS3rDeclFunctionType _VS0Empty

extern void _VS4rDeclFunctionType ELI_ARG((_TPPrDeclFunctionType _currn));
extern void _VS5rDeclFunctionType ELI_ARG((_TPPrDeclFunctionType _currn));
#define _VS6rDeclFunctionType _VS0Empty

#define _VS7rDeclFunctionType _VS0Empty

extern void _VS8rDeclFunctionType ELI_ARG((_TPPrDeclFunctionType _currn));
#define _VS9rDeclFunctionType _VS0Empty

extern void _VS10rDeclFunctionType ELI_ARG((_TPPrDeclFunctionType _currn));
extern void _VS1rFunctionTypeDecl ELI_ARG((_TPPrFunctionTypeDecl _currn));
extern void _VS2rFunctionTypeDecl ELI_ARG((_TPPrFunctionTypeDecl _currn));
extern void _VS3rFunctionTypeDecl ELI_ARG((_TPPrFunctionTypeDecl _currn));
extern void _VS4rFunctionTypeDecl ELI_ARG((_TPPrFunctionTypeDecl _currn));
extern void _VS5rFunctionTypeDecl ELI_ARG((_TPPrFunctionTypeDecl _currn));
extern void _VS1rFunctionType ELI_ARG((_TPPrFunctionType _currn));
extern void _VS2rFunctionType ELI_ARG((_TPPrFunctionType _currn));
extern void _VS3rFunctionType ELI_ARG((_TPPrFunctionType _currn));
extern void _VS4rFunctionType ELI_ARG((_TPPrFunctionType _currn));
extern void _VS5rFunctionType ELI_ARG((_TPPrFunctionType _currn));
extern void _VS1rFunctionTypeList2 ELI_ARG((_TPPrFunctionTypeList2 _currn,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS2rFunctionTypeList2 ELI_ARG((_TPPrFunctionTypeList2 _currn,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS3rFunctionTypeList2 ELI_ARG((_TPPrFunctionTypeList2 _currn,tStringType* _AS0name,StringTableKeyList* _AS0arg_types,bool* _AS0_const114,int* _AS0_const76,int* _AS0_const75,bool* _AS0_const70));
extern void _VS4rFunctionTypeList2 ELI_ARG((_TPPrFunctionTypeList2 _currn,tStringType* _AS0name,StringTableKeyList* _AS0arg_types,StringTableKeyList* _AS0_const146,bool* _AS0_const114,int* _AS0_const76,int* _AS0_const75,bool* _AS0_const70));
extern void _VS5rFunctionTypeList2 ELI_ARG((_TPPrFunctionTypeList2 _currn,tStringType* _AS0name,StringTableKeyList* _AS0arg_types,StringTableKeyList* _AS0_const146,int* _AS0_const142,bool* _AS0_const141,bool* _AS0_const114,PTGNode* _AS0_const101,bool* _AS0_const79,int* _AS0_const76,int* _AS0_const75,bool* _AS0_const70,BindingList* _AS0_const66,bool* _AS0_const60,bool* _AS0_const42,StringTableKey* _AS0_const10));
extern void _VS1rFunctionTypeList1 ELI_ARG((_TPPrFunctionTypeList1 _currn,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS2rFunctionTypeList1 ELI_ARG((_TPPrFunctionTypeList1 _currn,bool* _AS0_const114,bool* _AS0_const70));
extern void _VS3rFunctionTypeList1 ELI_ARG((_TPPrFunctionTypeList1 _currn,tStringType* _AS0name,StringTableKeyList* _AS0arg_types,bool* _AS0_const114,int* _AS0_const76,int* _AS0_const75,bool* _AS0_const70));
extern void _VS4rFunctionTypeList1 ELI_ARG((_TPPrFunctionTypeList1 _currn,tStringType* _AS0name,StringTableKeyList* _AS0arg_types,StringTableKeyList* _AS0_const146,bool* _AS0_const114,int* _AS0_const76,int* _AS0_const75,bool* _AS0_const70));
extern void _VS5rFunctionTypeList1 ELI_ARG((_TPPrFunctionTypeList1 _currn,tStringType* _AS0name,StringTableKeyList* _AS0arg_types,StringTableKeyList* _AS0_const146,int* _AS0_const142,bool* _AS0_const141,bool* _AS0_const114,PTGNode* _AS0_const101,bool* _AS0_const79,int* _AS0_const76,int* _AS0_const75,bool* _AS0_const70,BindingList* _AS0_const66,bool* _AS0_const60,bool* _AS0_const42,StringTableKey* _AS0_const10));
extern void _VS1rReturnTypeSimple ELI_ARG((_TPPrReturnTypeSimple _currn));
extern void _VS2rReturnTypeSimple ELI_ARG((_TPPrReturnTypeSimple _currn));
extern void _VS3rReturnTypeSimple ELI_ARG((_TPPrReturnTypeSimple _currn));
extern void _VS4rReturnTypeSimple ELI_ARG((_TPPrReturnTypeSimple _currn));
extern void _VS5rReturnTypeSimple ELI_ARG((_TPPrReturnTypeSimple _currn));
extern void _VS1rFunctionDefId ELI_ARG((_TPPrFunctionDefId _currn));
extern void _VS2rFunctionDefId ELI_ARG((_TPPrFunctionDefId _currn));
extern void _VS3rFunctionDefId ELI_ARG((_TPPrFunctionDefId _currn));
extern void _VS4rFunctionDefId ELI_ARG((_TPPrFunctionDefId _currn));
extern void _VS1rTypeOfFunction ELI_ARG((_TPPrTypeOfFunction _currn));
extern void _VS2rTypeOfFunction ELI_ARG((_TPPrTypeOfFunction _currn));
extern void _VS3rTypeOfFunction ELI_ARG((_TPPrTypeOfFunction _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS4rTypeOfFunction ELI_ARG((_TPPrTypeOfFunction _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS5rTypeOfFunction ELI_ARG((_TPPrTypeOfFunction _currn,Environment* _AS0type_names_chn2_pre,StringTableKey* _AS0_const10));
extern void _VS1rVoidTyping ELI_ARG((_TPPrVoidTyping _currn));
#define _VS2rVoidTyping _VS0Empty

extern void _VS3rVoidTyping ELI_ARG((_TPPrVoidTyping _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS4rVoidTyping ELI_ARG((_TPPrVoidTyping _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS5rVoidTyping ELI_ARG((_TPPrVoidTyping _currn,Environment* _AS0type_names_chn2_pre,StringTableKey* _AS0_const10));
extern void _VS1rTypeTemplate ELI_ARG((_TPPrTypeTemplate _currn));
#define _VS2rTypeTemplate _VS0Empty

extern void _VS3rTypeTemplate ELI_ARG((_TPPrTypeTemplate _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS4rTypeTemplate ELI_ARG((_TPPrTypeTemplate _currn,Environment* _AS0type_names_chn2_pre));
extern void _VS5rTypeTemplate ELI_ARG((_TPPrTypeTemplate _currn,Environment* _AS0type_names_chn2_pre,StringTableKey* _AS0_const10));
extern void _VS1rTypeVariableDefId ELI_ARG((_TPPrTypeVariableDefId _currn));
extern void _VS2rTypeVariableDefId ELI_ARG((_TPPrTypeVariableDefId _currn));
extern void _VS3rTypeVariableDefId ELI_ARG((_TPPrTypeVariableDefId _currn));
extern void _VS1rDeclFunctionImplementation ELI_ARG((_TPPrDeclFunctionImplementation _currn));
extern void _VS2rDeclFunctionImplementation ELI_ARG((_TPPrDeclFunctionImplementation _currn));
#define _VS3rDeclFunctionImplementation _VS0Empty

extern void _VS4rDeclFunctionImplementation ELI_ARG((_TPPrDeclFunctionImplementation _currn));
extern void _VS5rDeclFunctionImplementation ELI_ARG((_TPPrDeclFunctionImplementation _currn));
extern void _VS6rDeclFunctionImplementation ELI_ARG((_TPPrDeclFunctionImplementation _currn));
extern void _VS7rDeclFunctionImplementation ELI_ARG((_TPPrDeclFunctionImplementation _currn));
extern void _VS8rDeclFunctionImplementation ELI_ARG((_TPPrDeclFunctionImplementation _currn));
extern void _VS9rDeclFunctionImplementation ELI_ARG((_TPPrDeclFunctionImplementation _currn));
extern void _VS10rDeclFunctionImplementation ELI_ARG((_TPPrDeclFunctionImplementation _currn));
extern void _VS1rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS2rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS3rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS4rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS5rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS6rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS7rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS8rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS9rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS10rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
extern void _VS11rFunctionImplementationDecl ELI_ARG((_TPPrFunctionImplementationDecl _currn));
#define _VS1rFunctionPatternList2 _VS1rPTGDefList2

#define _VS2rFunctionPatternList2 _VS2rPTGDefList2

#define _VS3rFunctionPatternList2 _VS3rPTGDefList2

extern void _VS4rFunctionPatternList2 ELI_ARG((_TPPrFunctionPatternList2 _currn));
extern void _VS5rFunctionPatternList2 ELI_ARG((_TPPrFunctionPatternList2 _currn));
extern void _VS6rFunctionPatternList2 ELI_ARG((_TPPrFunctionPatternList2 _currn));
extern void _VS7rFunctionPatternList2 ELI_ARG((_TPPrFunctionPatternList2 _currn));
#define _VS1rFunctionPatternListEmpty _VS0Empty

#define _VS2rFunctionPatternListEmpty _VS0Empty

#define _VS3rFunctionPatternListEmpty _VS0Empty

extern void _VS4rFunctionPatternListEmpty ELI_ARG((_TPPrFunctionPatternListEmpty _currn));
#define _VS5rFunctionPatternListEmpty _VS0Empty

#define _VS6rFunctionPatternListEmpty _VS0Empty

extern void _VS7rFunctionPatternListEmpty ELI_ARG((_TPPrFunctionPatternListEmpty _currn));
#define _VS1rFunctionPatternVar _VS0Empty

#define _VS2rFunctionPatternVar _VS0Empty

extern void _VS3rFunctionPatternVar ELI_ARG((_TPPrFunctionPatternVar _currn));
extern void _VS4rFunctionPatternVar ELI_ARG((_TPPrFunctionPatternVar _currn));
extern void _VS5rFunctionPatternVar ELI_ARG((_TPPrFunctionPatternVar _currn));
extern void _VS6rFunctionPatternVar ELI_ARG((_TPPrFunctionPatternVar _currn));
extern void _VS7rFunctionPatternVar ELI_ARG((_TPPrFunctionPatternVar _currn));
extern void _VS1rFunctionPatternIsConstant ELI_ARG((_TPPrFunctionPatternIsConstant _currn));
#define _VS2rFunctionPatternIsConstant _VS0Empty

#define _VS3rFunctionPatternIsConstant _VS0Empty

extern void _VS4rFunctionPatternIsConstant ELI_ARG((_TPPrFunctionPatternIsConstant _currn));
extern void _VS5rFunctionPatternIsConstant ELI_ARG((_TPPrFunctionPatternIsConstant _currn));
extern void _VS6rFunctionPatternIsConstant ELI_ARG((_TPPrFunctionPatternIsConstant _currn));
extern void _VS7rFunctionPatternIsConstant ELI_ARG((_TPPrFunctionPatternIsConstant _currn));
#define _VS1rFunctionPatternIsListPattern _VS0Empty

#define _VS2rFunctionPatternIsListPattern _VS0Empty

extern void _VS3rFunctionPatternIsListPattern ELI_ARG((_TPPrFunctionPatternIsListPattern _currn));
extern void _VS4rFunctionPatternIsListPattern ELI_ARG((_TPPrFunctionPatternIsListPattern _currn));
extern void _VS5rFunctionPatternIsListPattern ELI_ARG((_TPPrFunctionPatternIsListPattern _currn));
extern void _VS6rFunctionPatternIsListPattern ELI_ARG((_TPPrFunctionPatternIsListPattern _currn));
extern void _VS7rFunctionPatternIsListPattern ELI_ARG((_TPPrFunctionPatternIsListPattern _currn));
#define _VS1rFunctionPatternIsTuple _VS0Empty

#define _VS2rFunctionPatternIsTuple _VS0Empty

extern void _VS3rFunctionPatternIsTuple ELI_ARG((_TPPrFunctionPatternIsTuple _currn));
extern void _VS4rFunctionPatternIsTuple ELI_ARG((_TPPrFunctionPatternIsTuple _currn));
extern void _VS5rFunctionPatternIsTuple ELI_ARG((_TPPrFunctionPatternIsTuple _currn));
extern void _VS6rFunctionPatternIsTuple ELI_ARG((_TPPrFunctionPatternIsTuple _currn));
extern void _VS7rFunctionPatternIsTuple ELI_ARG((_TPPrFunctionPatternIsTuple _currn));
extern void _VS1rFunctionPatternIsIgnored ELI_ARG((_TPPrFunctionPatternIsIgnored _currn));
#define _VS2rFunctionPatternIsIgnored _VS0Empty

#define _VS3rFunctionPatternIsIgnored _VS0Empty

extern void _VS4rFunctionPatternIsIgnored ELI_ARG((_TPPrFunctionPatternIsIgnored _currn));
extern void _VS5rFunctionPatternIsIgnored ELI_ARG((_TPPrFunctionPatternIsIgnored _currn));
extern void _VS6rFunctionPatternIsIgnored ELI_ARG((_TPPrFunctionPatternIsIgnored _currn));
extern void _VS7rFunctionPatternIsIgnored ELI_ARG((_TPPrFunctionPatternIsIgnored _currn));
extern void _VS1rFunctionPatternIsSimpleCon ELI_ARG((_TPPrFunctionPatternIsSimpleCon _currn));
extern void _VS2rFunctionPatternIsSimpleCon ELI_ARG((_TPPrFunctionPatternIsSimpleCon _currn));
#define _VS3rFunctionPatternIsSimpleCon _VS0Empty

extern void _VS4rFunctionPatternIsSimpleCon ELI_ARG((_TPPrFunctionPatternIsSimpleCon _currn));
extern void _VS5rFunctionPatternIsSimpleCon ELI_ARG((_TPPrFunctionPatternIsSimpleCon _currn));
extern void _VS6rFunctionPatternIsSimpleCon ELI_ARG((_TPPrFunctionPatternIsSimpleCon _currn));
extern void _VS7rFunctionPatternIsSimpleCon ELI_ARG((_TPPrFunctionPatternIsSimpleCon _currn));
extern void _VS1rFunctionPatternIsComplex ELI_ARG((_TPPrFunctionPatternIsComplex _currn));
extern void _VS2rFunctionPatternIsComplex ELI_ARG((_TPPrFunctionPatternIsComplex _currn));
extern void _VS3rFunctionPatternIsComplex ELI_ARG((_TPPrFunctionPatternIsComplex _currn));
extern void _VS4rFunctionPatternIsComplex ELI_ARG((_TPPrFunctionPatternIsComplex _currn));
extern void _VS5rFunctionPatternIsComplex ELI_ARG((_TPPrFunctionPatternIsComplex _currn));
extern void _VS6rFunctionPatternIsComplex ELI_ARG((_TPPrFunctionPatternIsComplex _currn));
extern void _VS7rFunctionPatternIsComplex ELI_ARG((_TPPrFunctionPatternIsComplex _currn));
extern void _VS1rFunctionConstructorPatternComplex ELI_ARG((_TPPrFunctionConstructorPatternComplex _currn));
extern void _VS2rFunctionConstructorPatternComplex ELI_ARG((_TPPrFunctionConstructorPatternComplex _currn));
extern void _VS3rFunctionConstructorPatternComplex ELI_ARG((_TPPrFunctionConstructorPatternComplex _currn));
extern void _VS4rFunctionConstructorPatternComplex ELI_ARG((_TPPrFunctionConstructorPatternComplex _currn));
extern void _VS5rFunctionConstructorPatternComplex ELI_ARG((_TPPrFunctionConstructorPatternComplex _currn));
extern void _VS1rFunctionPatternArgBindList2 ELI_ARG((_TPPrFunctionPatternArgBindList2 _currn));
extern void _VS2rFunctionPatternArgBindList2 ELI_ARG((_TPPrFunctionPatternArgBindList2 _currn));
#define _VS3rFunctionPatternArgBindList2 _VS3rPTGDefList2

extern void _VS1rFunctionPatternArgBindList1 ELI_ARG((_TPPrFunctionPatternArgBindList1 _currn));
extern void _VS2rFunctionPatternArgBindList1 ELI_ARG((_TPPrFunctionPatternArgBindList1 _currn));
#define _VS3rFunctionPatternArgBindList1 _VS3rPTGDecl

extern void _VS1rFunctionPatternArgBind ELI_ARG((_TPPrFunctionPatternArgBind _currn));
extern void _VS2rFunctionPatternArgBind ELI_ARG((_TPPrFunctionPatternArgBind _currn));
extern void _VS3rFunctionPatternArgBind ELI_ARG((_TPPrFunctionPatternArgBind _currn));
extern void _VS1rFunctionPatternConstant ELI_ARG((_TPPrFunctionPatternConstant _currn));
extern void _VS2rFunctionPatternConstant ELI_ARG((_TPPrFunctionPatternConstant _currn));
extern void _VS3rFunctionPatternConstant ELI_ARG((_TPPrFunctionPatternConstant _currn));
extern void _VS1rFunctionConstructorPatternSimple ELI_ARG((_TPPrFunctionConstructorPatternSimple _currn));
extern void _VS2rFunctionConstructorPatternSimple ELI_ARG((_TPPrFunctionConstructorPatternSimple _currn));
extern void _VS3rFunctionConstructorPatternSimple ELI_ARG((_TPPrFunctionConstructorPatternSimple _currn));
#define _VS1rFunctionIngorePattern _VS0Empty

extern void _VS1rFunctionTuplePattern ELI_ARG((_TPPrFunctionTuplePattern _currn));
extern void _VS2rFunctionTuplePattern ELI_ARG((_TPPrFunctionTuplePattern _currn));
extern void _VS1rFunctionTuplePatternElementList2 ELI_ARG((_TPPrFunctionTuplePatternElementList2 _currn));
#define _VS2rFunctionTuplePatternElementList2 _VS2rPTGDefList2

extern void _VS1rFunctionTuplePatternElementList1 ELI_ARG((_TPPrFunctionTuplePatternElementList1 _currn));
#define _VS2rFunctionTuplePatternElementList1 _VS2rPTGDecl

extern void _VS1rFunctionPatternListHead ELI_ARG((_TPPrFunctionPatternListHead _currn));
extern void _VS2rFunctionPatternListHead ELI_ARG((_TPPrFunctionPatternListHead _currn));
extern void _VS1rFunctionPatternListHeadAndTail ELI_ARG((_TPPrFunctionPatternListHeadAndTail _currn));
extern void _VS2rFunctionPatternListHeadAndTail ELI_ARG((_TPPrFunctionPatternListHeadAndTail _currn));
extern void _VS1rFunctionTuplePatternElement ELI_ARG((_TPPrFunctionTuplePatternElement _currn));
extern void _VS2rFunctionTuplePatternElement ELI_ARG((_TPPrFunctionTuplePatternElement _currn));
extern void _VS1rRhsExpression ELI_ARG((_TPPrRhsExpression _currn));
extern void _VS2rRhsExpression ELI_ARG((_TPPrRhsExpression _currn));
extern void _VS3rRhsExpression ELI_ARG((_TPPrRhsExpression _currn));
extern void _VS4rRhsExpression ELI_ARG((_TPPrRhsExpression _currn));
extern void _VS5rRhsExpression ELI_ARG((_TPPrRhsExpression _currn));
extern void _VS6rRhsExpression ELI_ARG((_TPPrRhsExpression _currn));
extern void _VS7rRhsExpression ELI_ARG((_TPPrRhsExpression _currn));
extern void _VS1rFunctionVariableDefId ELI_ARG((_TPPrFunctionVariableDefId _currn));
extern void _VS2rFunctionVariableDefId ELI_ARG((_TPPrFunctionVariableDefId _currn));
extern void _VS3rFunctionVariableDefId ELI_ARG((_TPPrFunctionVariableDefId _currn));
extern void _VS4rFunctionVariableDefId ELI_ARG((_TPPrFunctionVariableDefId _currn));
extern void _VS1rFunctionExpressionChain ELI_ARG((_TPPrFunctionExpressionChain _currn));
extern void _VS2rFunctionExpressionChain ELI_ARG((_TPPrFunctionExpressionChain _currn));
extern void _VS3rFunctionExpressionChain ELI_ARG((_TPPrFunctionExpressionChain _currn));
extern void _VS4rFunctionExpressionChain ELI_ARG((_TPPrFunctionExpressionChain _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS5rFunctionExpressionChain ELI_ARG((_TPPrFunctionExpressionChain _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS6rFunctionExpressionChain ELI_ARG((_TPPrFunctionExpressionChain _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS7rFunctionExpressionChain ELI_ARG((_TPPrFunctionExpressionChain _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionChain ELI_ARG((_TPPrFunctionExpressionChain _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS1rFunctionExpressionIsBinop ELI_ARG((_TPPrFunctionExpressionIsBinop _currn));
extern void _VS2rFunctionExpressionIsBinop ELI_ARG((_TPPrFunctionExpressionIsBinop _currn));
extern void _VS3rFunctionExpressionIsBinop ELI_ARG((_TPPrFunctionExpressionIsBinop _currn));
extern void _VS4rFunctionExpressionIsBinop ELI_ARG((_TPPrFunctionExpressionIsBinop _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS5rFunctionExpressionIsBinop ELI_ARG((_TPPrFunctionExpressionIsBinop _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS6rFunctionExpressionIsBinop ELI_ARG((_TPPrFunctionExpressionIsBinop _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS7rFunctionExpressionIsBinop ELI_ARG((_TPPrFunctionExpressionIsBinop _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionIsBinop ELI_ARG((_TPPrFunctionExpressionIsBinop _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS1rFunctionExpressionBinopChain ELI_ARG((_TPPrFunctionExpressionBinopChain _currn));
extern void _VS2rFunctionExpressionBinopChain ELI_ARG((_TPPrFunctionExpressionBinopChain _currn));
extern void _VS3rFunctionExpressionBinopChain ELI_ARG((_TPPrFunctionExpressionBinopChain _currn));
extern void _VS4rFunctionExpressionBinopChain ELI_ARG((_TPPrFunctionExpressionBinopChain _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopChain ELI_ARG((_TPPrFunctionExpressionBinopChain _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopChain ELI_ARG((_TPPrFunctionExpressionBinopChain _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopChain ELI_ARG((_TPPrFunctionExpressionBinopChain _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopChain ELI_ARG((_TPPrFunctionExpressionBinopChain _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopOR ELI_ARG((_TPPrFunctionExpressionBinopOR _currn));
extern void _VS2rFunctionExpressionBinopOR ELI_ARG((_TPPrFunctionExpressionBinopOR _currn));
extern void _VS3rFunctionExpressionBinopOR ELI_ARG((_TPPrFunctionExpressionBinopOR _currn));
extern void _VS4rFunctionExpressionBinopOR ELI_ARG((_TPPrFunctionExpressionBinopOR _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopOR ELI_ARG((_TPPrFunctionExpressionBinopOR _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopOR ELI_ARG((_TPPrFunctionExpressionBinopOR _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopOR ELI_ARG((_TPPrFunctionExpressionBinopOR _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopOR ELI_ARG((_TPPrFunctionExpressionBinopOR _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopAND ELI_ARG((_TPPrFunctionExpressionBinopAND _currn));
extern void _VS2rFunctionExpressionBinopAND ELI_ARG((_TPPrFunctionExpressionBinopAND _currn));
extern void _VS3rFunctionExpressionBinopAND ELI_ARG((_TPPrFunctionExpressionBinopAND _currn));
extern void _VS4rFunctionExpressionBinopAND ELI_ARG((_TPPrFunctionExpressionBinopAND _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopAND ELI_ARG((_TPPrFunctionExpressionBinopAND _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopAND ELI_ARG((_TPPrFunctionExpressionBinopAND _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopAND ELI_ARG((_TPPrFunctionExpressionBinopAND _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopAND ELI_ARG((_TPPrFunctionExpressionBinopAND _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopEQ ELI_ARG((_TPPrFunctionExpressionBinopEQ _currn));
extern void _VS2rFunctionExpressionBinopEQ ELI_ARG((_TPPrFunctionExpressionBinopEQ _currn));
extern void _VS3rFunctionExpressionBinopEQ ELI_ARG((_TPPrFunctionExpressionBinopEQ _currn));
extern void _VS4rFunctionExpressionBinopEQ ELI_ARG((_TPPrFunctionExpressionBinopEQ _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopEQ ELI_ARG((_TPPrFunctionExpressionBinopEQ _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopEQ ELI_ARG((_TPPrFunctionExpressionBinopEQ _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopEQ ELI_ARG((_TPPrFunctionExpressionBinopEQ _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopEQ ELI_ARG((_TPPrFunctionExpressionBinopEQ _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopNE ELI_ARG((_TPPrFunctionExpressionBinopNE _currn));
extern void _VS2rFunctionExpressionBinopNE ELI_ARG((_TPPrFunctionExpressionBinopNE _currn));
extern void _VS3rFunctionExpressionBinopNE ELI_ARG((_TPPrFunctionExpressionBinopNE _currn));
extern void _VS4rFunctionExpressionBinopNE ELI_ARG((_TPPrFunctionExpressionBinopNE _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopNE ELI_ARG((_TPPrFunctionExpressionBinopNE _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopNE ELI_ARG((_TPPrFunctionExpressionBinopNE _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopNE ELI_ARG((_TPPrFunctionExpressionBinopNE _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopNE ELI_ARG((_TPPrFunctionExpressionBinopNE _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopLE ELI_ARG((_TPPrFunctionExpressionBinopLE _currn));
extern void _VS2rFunctionExpressionBinopLE ELI_ARG((_TPPrFunctionExpressionBinopLE _currn));
extern void _VS3rFunctionExpressionBinopLE ELI_ARG((_TPPrFunctionExpressionBinopLE _currn));
extern void _VS4rFunctionExpressionBinopLE ELI_ARG((_TPPrFunctionExpressionBinopLE _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopLE ELI_ARG((_TPPrFunctionExpressionBinopLE _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopLE ELI_ARG((_TPPrFunctionExpressionBinopLE _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopLE ELI_ARG((_TPPrFunctionExpressionBinopLE _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopLE ELI_ARG((_TPPrFunctionExpressionBinopLE _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopLT ELI_ARG((_TPPrFunctionExpressionBinopLT _currn));
extern void _VS2rFunctionExpressionBinopLT ELI_ARG((_TPPrFunctionExpressionBinopLT _currn));
extern void _VS3rFunctionExpressionBinopLT ELI_ARG((_TPPrFunctionExpressionBinopLT _currn));
extern void _VS4rFunctionExpressionBinopLT ELI_ARG((_TPPrFunctionExpressionBinopLT _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopLT ELI_ARG((_TPPrFunctionExpressionBinopLT _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopLT ELI_ARG((_TPPrFunctionExpressionBinopLT _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopLT ELI_ARG((_TPPrFunctionExpressionBinopLT _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopLT ELI_ARG((_TPPrFunctionExpressionBinopLT _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopGT ELI_ARG((_TPPrFunctionExpressionBinopGT _currn));
extern void _VS2rFunctionExpressionBinopGT ELI_ARG((_TPPrFunctionExpressionBinopGT _currn));
extern void _VS3rFunctionExpressionBinopGT ELI_ARG((_TPPrFunctionExpressionBinopGT _currn));
extern void _VS4rFunctionExpressionBinopGT ELI_ARG((_TPPrFunctionExpressionBinopGT _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopGT ELI_ARG((_TPPrFunctionExpressionBinopGT _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopGT ELI_ARG((_TPPrFunctionExpressionBinopGT _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopGT ELI_ARG((_TPPrFunctionExpressionBinopGT _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopGT ELI_ARG((_TPPrFunctionExpressionBinopGT _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopGE ELI_ARG((_TPPrFunctionExpressionBinopGE _currn));
extern void _VS2rFunctionExpressionBinopGE ELI_ARG((_TPPrFunctionExpressionBinopGE _currn));
extern void _VS3rFunctionExpressionBinopGE ELI_ARG((_TPPrFunctionExpressionBinopGE _currn));
extern void _VS4rFunctionExpressionBinopGE ELI_ARG((_TPPrFunctionExpressionBinopGE _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopGE ELI_ARG((_TPPrFunctionExpressionBinopGE _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopGE ELI_ARG((_TPPrFunctionExpressionBinopGE _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopGE ELI_ARG((_TPPrFunctionExpressionBinopGE _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopGE ELI_ARG((_TPPrFunctionExpressionBinopGE _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopConcat ELI_ARG((_TPPrFunctionExpressionBinopConcat _currn));
extern void _VS2rFunctionExpressionBinopConcat ELI_ARG((_TPPrFunctionExpressionBinopConcat _currn));
extern void _VS3rFunctionExpressionBinopConcat ELI_ARG((_TPPrFunctionExpressionBinopConcat _currn));
extern void _VS4rFunctionExpressionBinopConcat ELI_ARG((_TPPrFunctionExpressionBinopConcat _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopConcat ELI_ARG((_TPPrFunctionExpressionBinopConcat _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopConcat ELI_ARG((_TPPrFunctionExpressionBinopConcat _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopConcat ELI_ARG((_TPPrFunctionExpressionBinopConcat _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopConcat ELI_ARG((_TPPrFunctionExpressionBinopConcat _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopADD ELI_ARG((_TPPrFunctionExpressionBinopADD _currn));
extern void _VS2rFunctionExpressionBinopADD ELI_ARG((_TPPrFunctionExpressionBinopADD _currn));
extern void _VS3rFunctionExpressionBinopADD ELI_ARG((_TPPrFunctionExpressionBinopADD _currn));
extern void _VS4rFunctionExpressionBinopADD ELI_ARG((_TPPrFunctionExpressionBinopADD _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopADD ELI_ARG((_TPPrFunctionExpressionBinopADD _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopADD ELI_ARG((_TPPrFunctionExpressionBinopADD _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopADD ELI_ARG((_TPPrFunctionExpressionBinopADD _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopADD ELI_ARG((_TPPrFunctionExpressionBinopADD _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopSUB ELI_ARG((_TPPrFunctionExpressionBinopSUB _currn));
extern void _VS2rFunctionExpressionBinopSUB ELI_ARG((_TPPrFunctionExpressionBinopSUB _currn));
extern void _VS3rFunctionExpressionBinopSUB ELI_ARG((_TPPrFunctionExpressionBinopSUB _currn));
extern void _VS4rFunctionExpressionBinopSUB ELI_ARG((_TPPrFunctionExpressionBinopSUB _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopSUB ELI_ARG((_TPPrFunctionExpressionBinopSUB _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopSUB ELI_ARG((_TPPrFunctionExpressionBinopSUB _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopSUB ELI_ARG((_TPPrFunctionExpressionBinopSUB _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopSUB ELI_ARG((_TPPrFunctionExpressionBinopSUB _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopMUL ELI_ARG((_TPPrFunctionExpressionBinopMUL _currn));
extern void _VS2rFunctionExpressionBinopMUL ELI_ARG((_TPPrFunctionExpressionBinopMUL _currn));
extern void _VS3rFunctionExpressionBinopMUL ELI_ARG((_TPPrFunctionExpressionBinopMUL _currn));
extern void _VS4rFunctionExpressionBinopMUL ELI_ARG((_TPPrFunctionExpressionBinopMUL _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopMUL ELI_ARG((_TPPrFunctionExpressionBinopMUL _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopMUL ELI_ARG((_TPPrFunctionExpressionBinopMUL _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopMUL ELI_ARG((_TPPrFunctionExpressionBinopMUL _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopMUL ELI_ARG((_TPPrFunctionExpressionBinopMUL _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopDIV ELI_ARG((_TPPrFunctionExpressionBinopDIV _currn));
extern void _VS2rFunctionExpressionBinopDIV ELI_ARG((_TPPrFunctionExpressionBinopDIV _currn));
extern void _VS3rFunctionExpressionBinopDIV ELI_ARG((_TPPrFunctionExpressionBinopDIV _currn));
extern void _VS4rFunctionExpressionBinopDIV ELI_ARG((_TPPrFunctionExpressionBinopDIV _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopDIV ELI_ARG((_TPPrFunctionExpressionBinopDIV _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopDIV ELI_ARG((_TPPrFunctionExpressionBinopDIV _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopDIV ELI_ARG((_TPPrFunctionExpressionBinopDIV _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopDIV ELI_ARG((_TPPrFunctionExpressionBinopDIV _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopMOD ELI_ARG((_TPPrFunctionExpressionBinopMOD _currn));
extern void _VS2rFunctionExpressionBinopMOD ELI_ARG((_TPPrFunctionExpressionBinopMOD _currn));
extern void _VS3rFunctionExpressionBinopMOD ELI_ARG((_TPPrFunctionExpressionBinopMOD _currn));
extern void _VS4rFunctionExpressionBinopMOD ELI_ARG((_TPPrFunctionExpressionBinopMOD _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopMOD ELI_ARG((_TPPrFunctionExpressionBinopMOD _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopMOD ELI_ARG((_TPPrFunctionExpressionBinopMOD _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopMOD ELI_ARG((_TPPrFunctionExpressionBinopMOD _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopMOD ELI_ARG((_TPPrFunctionExpressionBinopMOD _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionBinopIsUnary ELI_ARG((_TPPrFunctionExpressionBinopIsUnary _currn));
extern void _VS2rFunctionExpressionBinopIsUnary ELI_ARG((_TPPrFunctionExpressionBinopIsUnary _currn));
extern void _VS3rFunctionExpressionBinopIsUnary ELI_ARG((_TPPrFunctionExpressionBinopIsUnary _currn));
extern void _VS4rFunctionExpressionBinopIsUnary ELI_ARG((_TPPrFunctionExpressionBinopIsUnary _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS5rFunctionExpressionBinopIsUnary ELI_ARG((_TPPrFunctionExpressionBinopIsUnary _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS6rFunctionExpressionBinopIsUnary ELI_ARG((_TPPrFunctionExpressionBinopIsUnary _currn,D_typePtr* _AS0expected_type,int* _AS0_const167));
extern void _VS7rFunctionExpressionBinopIsUnary ELI_ARG((_TPPrFunctionExpressionBinopIsUnary _currn,D_typePtr* _AS0expected_type,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionBinopIsUnary ELI_ARG((_TPPrFunctionExpressionBinopIsUnary _currn,D_typePtr* _AS0expected_type,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const165,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
#define _VS1rFunctionUnaryIsPostfix _VS1rPTGDecl

#define _VS2rFunctionUnaryIsPostfix _VS2rPTGDecl

#define _VS3rFunctionUnaryIsPostfix _VS3rPTGDecl

extern void _VS4rFunctionUnaryIsPostfix ELI_ARG((_TPPrFunctionUnaryIsPostfix _currn));
extern void _VS5rFunctionUnaryIsPostfix ELI_ARG((_TPPrFunctionUnaryIsPostfix _currn));
extern void _VS6rFunctionUnaryIsPostfix ELI_ARG((_TPPrFunctionUnaryIsPostfix _currn));
extern void _VS7rFunctionUnaryIsPostfix ELI_ARG((_TPPrFunctionUnaryIsPostfix _currn));
extern void _VS8rFunctionUnaryIsPostfix ELI_ARG((_TPPrFunctionUnaryIsPostfix _currn));
#define _VS1rFunctionUnaryIsIncrement _VS1rPTGDecl

#define _VS2rFunctionUnaryIsIncrement _VS2rPTGDecl

#define _VS3rFunctionUnaryIsIncrement _VS3rPTGDecl

extern void _VS4rFunctionUnaryIsIncrement ELI_ARG((_TPPrFunctionUnaryIsIncrement _currn));
extern void _VS5rFunctionUnaryIsIncrement ELI_ARG((_TPPrFunctionUnaryIsIncrement _currn));
extern void _VS6rFunctionUnaryIsIncrement ELI_ARG((_TPPrFunctionUnaryIsIncrement _currn));
extern void _VS7rFunctionUnaryIsIncrement ELI_ARG((_TPPrFunctionUnaryIsIncrement _currn));
extern void _VS8rFunctionUnaryIsIncrement ELI_ARG((_TPPrFunctionUnaryIsIncrement _currn));
#define _VS1rFunctionUnaryIsMinus _VS1rPTGDecl

#define _VS2rFunctionUnaryIsMinus _VS2rPTGDecl

#define _VS3rFunctionUnaryIsMinus _VS3rPTGDecl

extern void _VS4rFunctionUnaryIsMinus ELI_ARG((_TPPrFunctionUnaryIsMinus _currn));
extern void _VS5rFunctionUnaryIsMinus ELI_ARG((_TPPrFunctionUnaryIsMinus _currn));
extern void _VS6rFunctionUnaryIsMinus ELI_ARG((_TPPrFunctionUnaryIsMinus _currn));
extern void _VS7rFunctionUnaryIsMinus ELI_ARG((_TPPrFunctionUnaryIsMinus _currn));
extern void _VS8rFunctionUnaryIsMinus ELI_ARG((_TPPrFunctionUnaryIsMinus _currn));
#define _VS1rFunctionUnaryIsLocation _VS1rPTGDecl

#define _VS2rFunctionUnaryIsLocation _VS2rPTGDecl

#define _VS3rFunctionUnaryIsLocation _VS3rPTGDecl

extern void _VS4rFunctionUnaryIsLocation ELI_ARG((_TPPrFunctionUnaryIsLocation _currn));
extern void _VS5rFunctionUnaryIsLocation ELI_ARG((_TPPrFunctionUnaryIsLocation _currn));
extern void _VS6rFunctionUnaryIsLocation ELI_ARG((_TPPrFunctionUnaryIsLocation _currn));
extern void _VS7rFunctionUnaryIsLocation ELI_ARG((_TPPrFunctionUnaryIsLocation _currn));
extern void _VS8rFunctionUnaryIsLocation ELI_ARG((_TPPrFunctionUnaryIsLocation _currn));
#define _VS1rFunctionUnaryIsNot _VS1rPTGDecl

#define _VS2rFunctionUnaryIsNot _VS2rPTGDecl

#define _VS3rFunctionUnaryIsNot _VS3rPTGDecl

extern void _VS4rFunctionUnaryIsNot ELI_ARG((_TPPrFunctionUnaryIsNot _currn));
extern void _VS5rFunctionUnaryIsNot ELI_ARG((_TPPrFunctionUnaryIsNot _currn));
extern void _VS6rFunctionUnaryIsNot ELI_ARG((_TPPrFunctionUnaryIsNot _currn));
extern void _VS7rFunctionUnaryIsNot ELI_ARG((_TPPrFunctionUnaryIsNot _currn));
extern void _VS8rFunctionUnaryIsNot ELI_ARG((_TPPrFunctionUnaryIsNot _currn));
#define _VS1rFunctionUnaryIsPtr _VS1rPTGDecl

#define _VS2rFunctionUnaryIsPtr _VS2rPTGDecl

#define _VS3rFunctionUnaryIsPtr _VS3rPTGDecl

extern void _VS4rFunctionUnaryIsPtr ELI_ARG((_TPPrFunctionUnaryIsPtr _currn));
extern void _VS5rFunctionUnaryIsPtr ELI_ARG((_TPPrFunctionUnaryIsPtr _currn));
extern void _VS6rFunctionUnaryIsPtr ELI_ARG((_TPPrFunctionUnaryIsPtr _currn));
extern void _VS7rFunctionUnaryIsPtr ELI_ARG((_TPPrFunctionUnaryIsPtr _currn));
extern void _VS8rFunctionUnaryIsPtr ELI_ARG((_TPPrFunctionUnaryIsPtr _currn));
extern void _VS1rFunctionPostfixIsPrim ELI_ARG((_TPPrFunctionPostfixIsPrim _currn));
extern void _VS2rFunctionPostfixIsPrim ELI_ARG((_TPPrFunctionPostfixIsPrim _currn));
extern void _VS3rFunctionPostfixIsPrim ELI_ARG((_TPPrFunctionPostfixIsPrim _currn));
extern void _VS4rFunctionPostfixIsPrim ELI_ARG((_TPPrFunctionPostfixIsPrim _currn));
extern void _VS5rFunctionPostfixIsPrim ELI_ARG((_TPPrFunctionPostfixIsPrim _currn));
extern void _VS6rFunctionPostfixIsPrim ELI_ARG((_TPPrFunctionPostfixIsPrim _currn,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionPostfixIsPrim ELI_ARG((_TPPrFunctionPostfixIsPrim _currn,D_typePtr* _AS0expected_type2,StringTableKeyList* _AS0_const174,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionPostfixIsPrim ELI_ARG((_TPPrFunctionPostfixIsPrim _currn,bool* _AS0is_expr_rhs,bool* _AS0is_recursive,bool* _AS0gen_return,D_typePtr* _AS0expected_type2,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionPostfixIsIndex ELI_ARG((_TPPrFunctionPostfixIsIndex _currn));
extern void _VS2rFunctionPostfixIsIndex ELI_ARG((_TPPrFunctionPostfixIsIndex _currn));
extern void _VS3rFunctionPostfixIsIndex ELI_ARG((_TPPrFunctionPostfixIsIndex _currn));
extern void _VS4rFunctionPostfixIsIndex ELI_ARG((_TPPrFunctionPostfixIsIndex _currn));
extern void _VS5rFunctionPostfixIsIndex ELI_ARG((_TPPrFunctionPostfixIsIndex _currn));
extern void _VS6rFunctionPostfixIsIndex ELI_ARG((_TPPrFunctionPostfixIsIndex _currn,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionPostfixIsIndex ELI_ARG((_TPPrFunctionPostfixIsIndex _currn,D_typePtr* _AS0expected_type2,StringTableKeyList* _AS0_const174,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionPostfixIsIndex ELI_ARG((_TPPrFunctionPostfixIsIndex _currn,bool* _AS0is_expr_rhs,bool* _AS0is_recursive,bool* _AS0gen_return,D_typePtr* _AS0expected_type2,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionPostfixIsAccess ELI_ARG((_TPPrFunctionPostfixIsAccess _currn));
extern void _VS2rFunctionPostfixIsAccess ELI_ARG((_TPPrFunctionPostfixIsAccess _currn));
extern void _VS3rFunctionPostfixIsAccess ELI_ARG((_TPPrFunctionPostfixIsAccess _currn));
extern void _VS4rFunctionPostfixIsAccess ELI_ARG((_TPPrFunctionPostfixIsAccess _currn));
extern void _VS5rFunctionPostfixIsAccess ELI_ARG((_TPPrFunctionPostfixIsAccess _currn));
extern void _VS6rFunctionPostfixIsAccess ELI_ARG((_TPPrFunctionPostfixIsAccess _currn,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionPostfixIsAccess ELI_ARG((_TPPrFunctionPostfixIsAccess _currn,D_typePtr* _AS0expected_type2,StringTableKeyList* _AS0_const174,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionPostfixIsAccess ELI_ARG((_TPPrFunctionPostfixIsAccess _currn,bool* _AS0is_expr_rhs,bool* _AS0is_recursive,bool* _AS0gen_return,D_typePtr* _AS0expected_type2,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionPostfixIsListcon ELI_ARG((_TPPrFunctionPostfixIsListcon _currn));
extern void _VS2rFunctionPostfixIsListcon ELI_ARG((_TPPrFunctionPostfixIsListcon _currn));
extern void _VS3rFunctionPostfixIsListcon ELI_ARG((_TPPrFunctionPostfixIsListcon _currn));
extern void _VS4rFunctionPostfixIsListcon ELI_ARG((_TPPrFunctionPostfixIsListcon _currn));
extern void _VS5rFunctionPostfixIsListcon ELI_ARG((_TPPrFunctionPostfixIsListcon _currn));
extern void _VS6rFunctionPostfixIsListcon ELI_ARG((_TPPrFunctionPostfixIsListcon _currn,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionPostfixIsListcon ELI_ARG((_TPPrFunctionPostfixIsListcon _currn,D_typePtr* _AS0expected_type2,StringTableKeyList* _AS0_const174,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionPostfixIsListcon ELI_ARG((_TPPrFunctionPostfixIsListcon _currn,bool* _AS0is_expr_rhs,bool* _AS0is_recursive,bool* _AS0gen_return,D_typePtr* _AS0expected_type2,PTGNode* _AS0fn_expr_code,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
#define _VS1rFunctionPrimaryIsConstant _VS0Empty

#define _VS2rFunctionPrimaryIsConstant _VS0Empty

#define _VS3rFunctionPrimaryIsConstant _VS0Empty

extern void _VS4rFunctionPrimaryIsConstant ELI_ARG((_TPPrFunctionPrimaryIsConstant _currn));
extern void _VS5rFunctionPrimaryIsConstant ELI_ARG((_TPPrFunctionPrimaryIsConstant _currn));
extern void _VS6rFunctionPrimaryIsConstant ELI_ARG((_TPPrFunctionPrimaryIsConstant _currn,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionPrimaryIsConstant ELI_ARG((_TPPrFunctionPrimaryIsConstant _currn,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionPrimaryIsConstant ELI_ARG((_TPPrFunctionPrimaryIsConstant _currn,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS9rFunctionPrimaryIsConstant ELI_ARG((_TPPrFunctionPrimaryIsConstant _currn,D_typePtr* _AS0expected_type2,PTGNode* _AS0fn_expr_code,int* _AS0_const163,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionPrimaryIsExpression ELI_ARG((_TPPrFunctionPrimaryIsExpression _currn));
extern void _VS2rFunctionPrimaryIsExpression ELI_ARG((_TPPrFunctionPrimaryIsExpression _currn));
extern void _VS3rFunctionPrimaryIsExpression ELI_ARG((_TPPrFunctionPrimaryIsExpression _currn));
extern void _VS4rFunctionPrimaryIsExpression ELI_ARG((_TPPrFunctionPrimaryIsExpression _currn));
extern void _VS5rFunctionPrimaryIsExpression ELI_ARG((_TPPrFunctionPrimaryIsExpression _currn));
extern void _VS6rFunctionPrimaryIsExpression ELI_ARG((_TPPrFunctionPrimaryIsExpression _currn,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionPrimaryIsExpression ELI_ARG((_TPPrFunctionPrimaryIsExpression _currn,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionPrimaryIsExpression ELI_ARG((_TPPrFunctionPrimaryIsExpression _currn,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS9rFunctionPrimaryIsExpression ELI_ARG((_TPPrFunctionPrimaryIsExpression _currn,D_typePtr* _AS0expected_type2,PTGNode* _AS0fn_expr_code,int* _AS0_const163,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionConstant ELI_ARG((_TPPrFunctionConstant _currn));
extern void _VS2rFunctionConstant ELI_ARG((_TPPrFunctionConstant _currn));
extern void _VS3rFunctionConstant ELI_ARG((_TPPrFunctionConstant _currn));
extern void _VS1rConstantString ELI_ARG((_TPPrConstantString _currn));
extern void _VS2rConstantString ELI_ARG((_TPPrConstantString _currn));
extern void _VS3rConstantString ELI_ARG((_TPPrConstantString _currn));
extern void _VS4rConstantString ELI_ARG((_TPPrConstantString _currn));
extern void _VS1rConstantNumber ELI_ARG((_TPPrConstantNumber _currn));
extern void _VS2rConstantNumber ELI_ARG((_TPPrConstantNumber _currn));
extern void _VS3rConstantNumber ELI_ARG((_TPPrConstantNumber _currn));
extern void _VS4rConstantNumber ELI_ARG((_TPPrConstantNumber _currn));
extern void _VS1rConstantTrue ELI_ARG((_TPPrConstantTrue _currn));
extern void _VS2rConstantTrue ELI_ARG((_TPPrConstantTrue _currn));
extern void _VS3rConstantTrue ELI_ARG((_TPPrConstantTrue _currn));
extern void _VS4rConstantTrue ELI_ARG((_TPPrConstantTrue _currn));
extern void _VS1rConstantFalse ELI_ARG((_TPPrConstantFalse _currn));
extern void _VS2rConstantFalse ELI_ARG((_TPPrConstantFalse _currn));
extern void _VS3rConstantFalse ELI_ARG((_TPPrConstantFalse _currn));
extern void _VS4rConstantFalse ELI_ARG((_TPPrConstantFalse _currn));
extern void _VS1rConstantPos ELI_ARG((_TPPrConstantPos _currn));
extern void _VS2rConstantPos ELI_ARG((_TPPrConstantPos _currn));
extern void _VS3rConstantPos ELI_ARG((_TPPrConstantPos _currn));
extern void _VS4rConstantPos ELI_ARG((_TPPrConstantPos _currn));
extern void _VS1rConstantEmptyList ELI_ARG((_TPPrConstantEmptyList _currn));
extern void _VS2rConstantEmptyList ELI_ARG((_TPPrConstantEmptyList _currn));
extern void _VS3rConstantEmptyList ELI_ARG((_TPPrConstantEmptyList _currn));
extern void _VS4rConstantEmptyList ELI_ARG((_TPPrConstantEmptyList _currn));
extern void _VS1rFunctionPrimaryIsCall ELI_ARG((_TPPrFunctionPrimaryIsCall _currn));
extern void _VS2rFunctionPrimaryIsCall ELI_ARG((_TPPrFunctionPrimaryIsCall _currn));
extern void _VS3rFunctionPrimaryIsCall ELI_ARG((_TPPrFunctionPrimaryIsCall _currn));
extern void _VS4rFunctionPrimaryIsCall ELI_ARG((_TPPrFunctionPrimaryIsCall _currn));
extern void _VS5rFunctionPrimaryIsCall ELI_ARG((_TPPrFunctionPrimaryIsCall _currn));
extern void _VS6rFunctionPrimaryIsCall ELI_ARG((_TPPrFunctionPrimaryIsCall _currn,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionPrimaryIsCall ELI_ARG((_TPPrFunctionPrimaryIsCall _currn,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionPrimaryIsCall ELI_ARG((_TPPrFunctionPrimaryIsCall _currn,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS9rFunctionPrimaryIsCall ELI_ARG((_TPPrFunctionPrimaryIsCall _currn,D_typePtr* _AS0expected_type2,PTGNode* _AS0fn_expr_code,int* _AS0_const163,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionCallParameters ELI_ARG((_TPPrFunctionCallParameters _currn));
extern void _VS2rFunctionCallParameters ELI_ARG((_TPPrFunctionCallParameters _currn));
extern void _VS3rFunctionCallParameters ELI_ARG((_TPPrFunctionCallParameters _currn));
extern void _VS4rFunctionCallParameters ELI_ARG((_TPPrFunctionCallParameters _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionCallParameters ELI_ARG((_TPPrFunctionCallParameters _currn,D_typePtr* _AS0expected_type));
extern void _VS6rFunctionCallParameters ELI_ARG((_TPPrFunctionCallParameters _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionCallParameters ELI_ARG((_TPPrFunctionCallParameters _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS8rFunctionCallParameters ELI_ARG((_TPPrFunctionCallParameters _currn,D_typePtr* _AS0expected_type,bool* _AS0is_expr_rhs,bool* _AS0fn_has_ret,int* _AS0fn_if_cnt,D_typePtr* _AS0expected_type2));
extern void _VS1rFunctionCallEmpty ELI_ARG((_TPPrFunctionCallEmpty _currn));
#define _VS2rFunctionCallEmpty _VS0Empty

extern void _VS3rFunctionCallEmpty ELI_ARG((_TPPrFunctionCallEmpty _currn));
extern void _VS4rFunctionCallEmpty ELI_ARG((_TPPrFunctionCallEmpty _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionCallEmpty ELI_ARG((_TPPrFunctionCallEmpty _currn,D_typePtr* _AS0expected_type));
extern void _VS6rFunctionCallEmpty ELI_ARG((_TPPrFunctionCallEmpty _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionCallEmpty ELI_ARG((_TPPrFunctionCallEmpty _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS8rFunctionCallEmpty ELI_ARG((_TPPrFunctionCallEmpty _currn,D_typePtr* _AS0expected_type,bool* _AS0is_expr_rhs,bool* _AS0fn_has_ret,int* _AS0fn_if_cnt,D_typePtr* _AS0expected_type2));
extern void _VS1rFunctionCallEmptyConstant ELI_ARG((_TPPrFunctionCallEmptyConstant _currn));
#define _VS2rFunctionCallEmptyConstant _VS0Empty

extern void _VS3rFunctionCallEmptyConstant ELI_ARG((_TPPrFunctionCallEmptyConstant _currn));
extern void _VS4rFunctionCallEmptyConstant ELI_ARG((_TPPrFunctionCallEmptyConstant _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionCallEmptyConstant ELI_ARG((_TPPrFunctionCallEmptyConstant _currn,D_typePtr* _AS0expected_type));
extern void _VS6rFunctionCallEmptyConstant ELI_ARG((_TPPrFunctionCallEmptyConstant _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionCallEmptyConstant ELI_ARG((_TPPrFunctionCallEmptyConstant _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS8rFunctionCallEmptyConstant ELI_ARG((_TPPrFunctionCallEmptyConstant _currn,D_typePtr* _AS0expected_type,bool* _AS0is_expr_rhs,bool* _AS0fn_has_ret,int* _AS0fn_if_cnt,D_typePtr* _AS0expected_type2));
extern void _VS1rCallIdentifier ELI_ARG((_TPPrCallIdentifier _currn));
extern void _VS2rCallIdentifier ELI_ARG((_TPPrCallIdentifier _currn));
extern void _VS3rCallIdentifier ELI_ARG((_TPPrCallIdentifier _currn));
extern void _VS4rCallIdentifier ELI_ARG((_TPPrCallIdentifier _currn));
extern void _VS5rCallIdentifier ELI_ARG((_TPPrCallIdentifier _currn));
extern void _VS6rCallIdentifier ELI_ARG((_TPPrCallIdentifier _currn));
extern void _VS7rCallIdentifier ELI_ARG((_TPPrCallIdentifier _currn));
extern void _VS8rCallIdentifier ELI_ARG((_TPPrCallIdentifier _currn));
extern void _VS9rCallIdentifier ELI_ARG((_TPPrCallIdentifier _currn,Binding* _AS0used_var_name,PTGNode* _AS0fn_expr_code,StringTableKey* _AS0_const10));
extern void _VS1rCallType ELI_ARG((_TPPrCallType _currn));
#define _VS2rCallType _VS0Empty

extern void _VS3rCallType ELI_ARG((_TPPrCallType _currn));
extern void _VS4rCallType ELI_ARG((_TPPrCallType _currn));
extern void _VS5rCallType ELI_ARG((_TPPrCallType _currn));
extern void _VS6rCallType ELI_ARG((_TPPrCallType _currn));
extern void _VS7rCallType ELI_ARG((_TPPrCallType _currn));
extern void _VS8rCallType ELI_ARG((_TPPrCallType _currn));
extern void _VS9rCallType ELI_ARG((_TPPrCallType _currn,Binding* _AS0used_var_name,PTGNode* _AS0fn_expr_code,StringTableKey* _AS0_const10));
extern void _VS1rCallEmpty ELI_ARG((_TPPrCallEmpty _currn));
#define _VS2rCallEmpty _VS0Empty

extern void _VS3rCallEmpty ELI_ARG((_TPPrCallEmpty _currn));
extern void _VS4rCallEmpty ELI_ARG((_TPPrCallEmpty _currn));
extern void _VS5rCallEmpty ELI_ARG((_TPPrCallEmpty _currn));
extern void _VS6rCallEmpty ELI_ARG((_TPPrCallEmpty _currn));
extern void _VS7rCallEmpty ELI_ARG((_TPPrCallEmpty _currn));
extern void _VS8rCallEmpty ELI_ARG((_TPPrCallEmpty _currn));
extern void _VS9rCallEmpty ELI_ARG((_TPPrCallEmpty _currn,Binding* _AS0used_var_name,PTGNode* _AS0fn_expr_code,StringTableKey* _AS0_const10));
extern void _VS1rFunctionCallParamList2 ELI_ARG((_TPPrFunctionCallParamList2 _currn));
extern void _VS2rFunctionCallParamList2 ELI_ARG((_TPPrFunctionCallParamList2 _currn));
extern void _VS3rFunctionCallParamList2 ELI_ARG((_TPPrFunctionCallParamList2 _currn));
extern void _VS4rFunctionCallParamList2 ELI_ARG((_TPPrFunctionCallParamList2 _currn,D_types* _AS0_const170,int* _AS0_const168));
extern void _VS5rFunctionCallParamList2 ELI_ARG((_TPPrFunctionCallParamList2 _currn,D_types* _AS0_const170,int* _AS0_const168));
extern void _VS6rFunctionCallParamList2 ELI_ARG((_TPPrFunctionCallParamList2 _currn,D_types* _AS0_const171,D_types* _AS0_const170,int* _AS0_const168));
extern void _VS7rFunctionCallParamList2 ELI_ARG((_TPPrFunctionCallParamList2 _currn,StringTableKeyList* _AS0_const174,D_types* _AS0_const171,D_types* _AS0_const170,int* _AS0_const168,bool* _AS0_const166,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionCallParamList2 ELI_ARG((_TPPrFunctionCallParamList2 _currn,BindingList* _AS0_const186,PTGNode* _AS0_const177,StringTableKeyList* _AS0_const174,D_types* _AS0_const171,D_types* _AS0_const170,int* _AS0_const168,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionCallParamList1 ELI_ARG((_TPPrFunctionCallParamList1 _currn));
extern void _VS2rFunctionCallParamList1 ELI_ARG((_TPPrFunctionCallParamList1 _currn));
extern void _VS3rFunctionCallParamList1 ELI_ARG((_TPPrFunctionCallParamList1 _currn));
extern void _VS4rFunctionCallParamList1 ELI_ARG((_TPPrFunctionCallParamList1 _currn,D_types* _AS0_const170,int* _AS0_const168));
extern void _VS5rFunctionCallParamList1 ELI_ARG((_TPPrFunctionCallParamList1 _currn,D_types* _AS0_const170,int* _AS0_const168));
extern void _VS6rFunctionCallParamList1 ELI_ARG((_TPPrFunctionCallParamList1 _currn,D_types* _AS0_const171,D_types* _AS0_const170,int* _AS0_const168));
extern void _VS7rFunctionCallParamList1 ELI_ARG((_TPPrFunctionCallParamList1 _currn,StringTableKeyList* _AS0_const174,D_types* _AS0_const171,D_types* _AS0_const170,int* _AS0_const168,bool* _AS0_const166,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionCallParamList1 ELI_ARG((_TPPrFunctionCallParamList1 _currn,BindingList* _AS0_const186,PTGNode* _AS0_const177,StringTableKeyList* _AS0_const174,D_types* _AS0_const171,D_types* _AS0_const170,int* _AS0_const168,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionCallParamIsExpr ELI_ARG((_TPPrFunctionCallParamIsExpr _currn));
extern void _VS2rFunctionCallParamIsExpr ELI_ARG((_TPPrFunctionCallParamIsExpr _currn));
extern void _VS3rFunctionCallParamIsExpr ELI_ARG((_TPPrFunctionCallParamIsExpr _currn));
extern void _VS4rFunctionCallParamIsExpr ELI_ARG((_TPPrFunctionCallParamIsExpr _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionCallParamIsExpr ELI_ARG((_TPPrFunctionCallParamIsExpr _currn,D_typePtr* _AS0expected_type));
extern void _VS6rFunctionCallParamIsExpr ELI_ARG((_TPPrFunctionCallParamIsExpr _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionCallParamIsExpr ELI_ARG((_TPPrFunctionCallParamIsExpr _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS8rFunctionCallParamIsExpr ELI_ARG((_TPPrFunctionCallParamIsExpr _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS1rFunctionExprIsError ELI_ARG((_TPPrFunctionExprIsError _currn));
extern void _VS2rFunctionExprIsError ELI_ARG((_TPPrFunctionExprIsError _currn));
extern void _VS3rFunctionExprIsError ELI_ARG((_TPPrFunctionExprIsError _currn));
extern void _VS4rFunctionExprIsError ELI_ARG((_TPPrFunctionExprIsError _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS5rFunctionExprIsError ELI_ARG((_TPPrFunctionExprIsError _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS6rFunctionExprIsError ELI_ARG((_TPPrFunctionExprIsError _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS7rFunctionExprIsError ELI_ARG((_TPPrFunctionExprIsError _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExprIsError ELI_ARG((_TPPrFunctionExprIsError _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS1rFunctionErrorExpression ELI_ARG((_TPPrFunctionErrorExpression _currn));
extern void _VS2rFunctionErrorExpression ELI_ARG((_TPPrFunctionErrorExpression _currn));
extern void _VS3rFunctionErrorExpression ELI_ARG((_TPPrFunctionErrorExpression _currn));
extern void _VS4rFunctionErrorExpression ELI_ARG((_TPPrFunctionErrorExpression _currn));
extern void _VS5rFunctionErrorExpression ELI_ARG((_TPPrFunctionErrorExpression _currn));
extern void _VS6rFunctionErrorExpression ELI_ARG((_TPPrFunctionErrorExpression _currn));
extern void _VS7rFunctionErrorExpression ELI_ARG((_TPPrFunctionErrorExpression _currn,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionErrorExpression ELI_ARG((_TPPrFunctionErrorExpression _currn,PTGNode* _AS0fn_expr_code,int* _AS0_const163,int* _AS0_const158,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionExpressionIsIf ELI_ARG((_TPPrFunctionExpressionIsIf _currn));
extern void _VS2rFunctionExpressionIsIf ELI_ARG((_TPPrFunctionExpressionIsIf _currn));
extern void _VS3rFunctionExpressionIsIf ELI_ARG((_TPPrFunctionExpressionIsIf _currn));
extern void _VS4rFunctionExpressionIsIf ELI_ARG((_TPPrFunctionExpressionIsIf _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS5rFunctionExpressionIsIf ELI_ARG((_TPPrFunctionExpressionIsIf _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS6rFunctionExpressionIsIf ELI_ARG((_TPPrFunctionExpressionIsIf _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS7rFunctionExpressionIsIf ELI_ARG((_TPPrFunctionExpressionIsIf _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionIsIf ELI_ARG((_TPPrFunctionExpressionIsIf _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS1rFunctionIfExpression ELI_ARG((_TPPrFunctionIfExpression _currn));
extern void _VS2rFunctionIfExpression ELI_ARG((_TPPrFunctionIfExpression _currn));
extern void _VS3rFunctionIfExpression ELI_ARG((_TPPrFunctionIfExpression _currn));
extern void _VS4rFunctionIfExpression ELI_ARG((_TPPrFunctionIfExpression _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionIfExpression ELI_ARG((_TPPrFunctionIfExpression _currn,D_typePtr* _AS0expected_type));
extern void _VS6rFunctionIfExpression ELI_ARG((_TPPrFunctionIfExpression _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionIfExpression ELI_ARG((_TPPrFunctionIfExpression _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS8rFunctionIfExpression ELI_ARG((_TPPrFunctionIfExpression _currn,bool* _AS0is_expr_rhs,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS1rFunctionRhsIsGuards ELI_ARG((_TPPrFunctionRhsIsGuards _currn));
extern void _VS2rFunctionRhsIsGuards ELI_ARG((_TPPrFunctionRhsIsGuards _currn));
extern void _VS3rFunctionRhsIsGuards ELI_ARG((_TPPrFunctionRhsIsGuards _currn));
extern void _VS4rFunctionRhsIsGuards ELI_ARG((_TPPrFunctionRhsIsGuards _currn));
extern void _VS5rFunctionRhsIsGuards ELI_ARG((_TPPrFunctionRhsIsGuards _currn));
extern void _VS6rFunctionRhsIsGuards ELI_ARG((_TPPrFunctionRhsIsGuards _currn));
extern void _VS7rFunctionRhsIsGuards ELI_ARG((_TPPrFunctionRhsIsGuards _currn));
extern void _VS1rFunctionGuardList2 ELI_ARG((_TPPrFunctionGuardList2 _currn));
extern void _VS2rFunctionGuardList2 ELI_ARG((_TPPrFunctionGuardList2 _currn));
extern void _VS3rFunctionGuardList2 ELI_ARG((_TPPrFunctionGuardList2 _currn));
extern void _VS4rFunctionGuardList2 ELI_ARG((_TPPrFunctionGuardList2 _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionGuardList2 ELI_ARG((_TPPrFunctionGuardList2 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,D_typePtr* _AS0result_type2,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const151,int* _AS0_const150));
extern void _VS6rFunctionGuardList2 ELI_ARG((_TPPrFunctionGuardList2 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,D_typePtr* _AS0result_type2,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const151,int* _AS0_const150));
extern void _VS7rFunctionGuardList2 ELI_ARG((_TPPrFunctionGuardList2 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,D_typePtr* _AS0result_type2,PTGNode* _AS0_const180,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const153,int* _AS0_const151,int* _AS0_const150,StringTableKey* _AS0_const10));
extern void _VS1rFunctionGuardList1 ELI_ARG((_TPPrFunctionGuardList1 _currn));
extern void _VS2rFunctionGuardList1 ELI_ARG((_TPPrFunctionGuardList1 _currn));
extern void _VS3rFunctionGuardList1 ELI_ARG((_TPPrFunctionGuardList1 _currn));
extern void _VS4rFunctionGuardList1 ELI_ARG((_TPPrFunctionGuardList1 _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionGuardList1 ELI_ARG((_TPPrFunctionGuardList1 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,D_typePtr* _AS0result_type2,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const151,int* _AS0_const150));
extern void _VS6rFunctionGuardList1 ELI_ARG((_TPPrFunctionGuardList1 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,D_typePtr* _AS0result_type2,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const151,int* _AS0_const150));
extern void _VS7rFunctionGuardList1 ELI_ARG((_TPPrFunctionGuardList1 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,D_typePtr* _AS0result_type2,PTGNode* _AS0_const180,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const153,int* _AS0_const151,int* _AS0_const150,StringTableKey* _AS0_const10));
#define _VS1rFunctionGuard _VS1rPTGDefList2

#define _VS2rFunctionGuard _VS2rPTGDefList2

#define _VS3rFunctionGuard _VS3rPTGDefList2

extern void _VS4rFunctionGuard ELI_ARG((_TPPrFunctionGuard _currn));
extern void _VS5rFunctionGuard ELI_ARG((_TPPrFunctionGuard _currn));
extern void _VS6rFunctionGuard ELI_ARG((_TPPrFunctionGuard _currn));
extern void _VS7rFunctionGuard ELI_ARG((_TPPrFunctionGuard _currn));
extern void _VS1rRhsStatements ELI_ARG((_TPPrRhsStatements _currn));
extern void _VS2rRhsStatements ELI_ARG((_TPPrRhsStatements _currn));
extern void _VS3rRhsStatements ELI_ARG((_TPPrRhsStatements _currn));
extern void _VS4rRhsStatements ELI_ARG((_TPPrRhsStatements _currn));
extern void _VS5rRhsStatements ELI_ARG((_TPPrRhsStatements _currn));
extern void _VS6rRhsStatements ELI_ARG((_TPPrRhsStatements _currn));
extern void _VS7rRhsStatements ELI_ARG((_TPPrRhsStatements _currn));
extern void _VS1rFunctionDoStatementList2 ELI_ARG((_TPPrFunctionDoStatementList2 _currn));
extern void _VS2rFunctionDoStatementList2 ELI_ARG((_TPPrFunctionDoStatementList2 _currn));
extern void _VS3rFunctionDoStatementList2 ELI_ARG((_TPPrFunctionDoStatementList2 _currn));
extern void _VS4rFunctionDoStatementList2 ELI_ARG((_TPPrFunctionDoStatementList2 _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionDoStatementList2 ELI_ARG((_TPPrFunctionDoStatementList2 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const151,int* _AS0_const150));
extern void _VS6rFunctionDoStatementList2 ELI_ARG((_TPPrFunctionDoStatementList2 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,int* _AS0_const158,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const151,int* _AS0_const150));
extern void _VS7rFunctionDoStatementList2 ELI_ARG((_TPPrFunctionDoStatementList2 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,PTGNode* _AS0_const181,int* _AS0_const163,int* _AS0_const158,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const153,int* _AS0_const151,int* _AS0_const150,StringTableKey* _AS0_const10));
#define _VS1rFunctionDoStatementListEmpty _VS0Empty

#define _VS2rFunctionDoStatementListEmpty _VS0Empty

#define _VS3rFunctionDoStatementListEmpty _VS0Empty

extern void _VS4rFunctionDoStatementListEmpty ELI_ARG((_TPPrFunctionDoStatementListEmpty _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionDoStatementListEmpty ELI_ARG((_TPPrFunctionDoStatementListEmpty _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const151,int* _AS0_const150));
extern void _VS6rFunctionDoStatementListEmpty ELI_ARG((_TPPrFunctionDoStatementListEmpty _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,int* _AS0_const158,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const151,int* _AS0_const150));
extern void _VS7rFunctionDoStatementListEmpty ELI_ARG((_TPPrFunctionDoStatementListEmpty _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2,PTGNode* _AS0_const181,int* _AS0_const163,int* _AS0_const158,bool* _AS0_const155,bool* _AS0_const154,int* _AS0_const153,int* _AS0_const151,int* _AS0_const150,StringTableKey* _AS0_const10));
extern void _VS1rFunctionDoExpression ELI_ARG((_TPPrFunctionDoExpression _currn));
extern void _VS2rFunctionDoExpression ELI_ARG((_TPPrFunctionDoExpression _currn));
extern void _VS3rFunctionDoExpression ELI_ARG((_TPPrFunctionDoExpression _currn));
extern void _VS4rFunctionDoExpression ELI_ARG((_TPPrFunctionDoExpression _currn));
extern void _VS5rFunctionDoExpression ELI_ARG((_TPPrFunctionDoExpression _currn));
extern void _VS6rFunctionDoExpression ELI_ARG((_TPPrFunctionDoExpression _currn));
extern void _VS7rFunctionDoExpression ELI_ARG((_TPPrFunctionDoExpression _currn));
extern void _VS1rFunctionDoCondition ELI_ARG((_TPPrFunctionDoCondition _currn));
extern void _VS2rFunctionDoCondition ELI_ARG((_TPPrFunctionDoCondition _currn));
extern void _VS3rFunctionDoCondition ELI_ARG((_TPPrFunctionDoCondition _currn));
extern void _VS4rFunctionDoCondition ELI_ARG((_TPPrFunctionDoCondition _currn));
extern void _VS5rFunctionDoCondition ELI_ARG((_TPPrFunctionDoCondition _currn));
extern void _VS6rFunctionDoCondition ELI_ARG((_TPPrFunctionDoCondition _currn));
extern void _VS7rFunctionDoCondition ELI_ARG((_TPPrFunctionDoCondition _currn));
extern void _VS1rFunctionDoAssign ELI_ARG((_TPPrFunctionDoAssign _currn));
extern void _VS2rFunctionDoAssign ELI_ARG((_TPPrFunctionDoAssign _currn));
extern void _VS3rFunctionDoAssign ELI_ARG((_TPPrFunctionDoAssign _currn));
extern void _VS4rFunctionDoAssign ELI_ARG((_TPPrFunctionDoAssign _currn));
extern void _VS5rFunctionDoAssign ELI_ARG((_TPPrFunctionDoAssign _currn));
extern void _VS6rFunctionDoAssign ELI_ARG((_TPPrFunctionDoAssign _currn));
extern void _VS7rFunctionDoAssign ELI_ARG((_TPPrFunctionDoAssign _currn));
extern void _VS1rFunctionDoReturn ELI_ARG((_TPPrFunctionDoReturn _currn));
extern void _VS2rFunctionDoReturn ELI_ARG((_TPPrFunctionDoReturn _currn));
extern void _VS3rFunctionDoReturn ELI_ARG((_TPPrFunctionDoReturn _currn));
extern void _VS4rFunctionDoReturn ELI_ARG((_TPPrFunctionDoReturn _currn));
extern void _VS5rFunctionDoReturn ELI_ARG((_TPPrFunctionDoReturn _currn));
extern void _VS6rFunctionDoReturn ELI_ARG((_TPPrFunctionDoReturn _currn));
extern void _VS7rFunctionDoReturn ELI_ARG((_TPPrFunctionDoReturn _currn));
#define _VS1rFunctionReturnStatement _VS1rPTGDecl

#define _VS2rFunctionReturnStatement _VS2rPTGDecl

#define _VS3rFunctionReturnStatement _VS3rPTGDecl

extern void _VS4rFunctionReturnStatement ELI_ARG((_TPPrFunctionReturnStatement _currn));
extern void _VS5rFunctionReturnStatement ELI_ARG((_TPPrFunctionReturnStatement _currn));
extern void _VS6rFunctionReturnStatement ELI_ARG((_TPPrFunctionReturnStatement _currn));
extern void _VS7rFunctionReturnStatement ELI_ARG((_TPPrFunctionReturnStatement _currn));
extern void _VS1rFunctionCondStatement ELI_ARG((_TPPrFunctionCondStatement _currn));
extern void _VS2rFunctionCondStatement ELI_ARG((_TPPrFunctionCondStatement _currn));
extern void _VS3rFunctionCondStatement ELI_ARG((_TPPrFunctionCondStatement _currn));
extern void _VS4rFunctionCondStatement ELI_ARG((_TPPrFunctionCondStatement _currn));
extern void _VS5rFunctionCondStatement ELI_ARG((_TPPrFunctionCondStatement _currn));
extern void _VS6rFunctionCondStatement ELI_ARG((_TPPrFunctionCondStatement _currn));
extern void _VS1rFunctionCondStatUnit ELI_ARG((_TPPrFunctionCondStatUnit _currn));
extern void _VS2rFunctionCondStatUnit ELI_ARG((_TPPrFunctionCondStatUnit _currn));
extern void _VS3rFunctionCondStatUnit ELI_ARG((_TPPrFunctionCondStatUnit _currn));
extern void _VS4rFunctionCondStatUnit ELI_ARG((_TPPrFunctionCondStatUnit _currn));
extern void _VS5rFunctionCondStatUnit ELI_ARG((_TPPrFunctionCondStatUnit _currn));
extern void _VS6rFunctionCondStatUnit ELI_ARG((_TPPrFunctionCondStatUnit _currn));
extern void _VS1rFunctionExpressionIsLet ELI_ARG((_TPPrFunctionExpressionIsLet _currn));
extern void _VS2rFunctionExpressionIsLet ELI_ARG((_TPPrFunctionExpressionIsLet _currn));
extern void _VS3rFunctionExpressionIsLet ELI_ARG((_TPPrFunctionExpressionIsLet _currn));
extern void _VS4rFunctionExpressionIsLet ELI_ARG((_TPPrFunctionExpressionIsLet _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS5rFunctionExpressionIsLet ELI_ARG((_TPPrFunctionExpressionIsLet _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS6rFunctionExpressionIsLet ELI_ARG((_TPPrFunctionExpressionIsLet _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS7rFunctionExpressionIsLet ELI_ARG((_TPPrFunctionExpressionIsLet _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionIsLet ELI_ARG((_TPPrFunctionExpressionIsLet _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
#define _VS1rFunctionLetExpression _VS1rPTGDefList2

#define _VS2rFunctionLetExpression _VS2rPTGDefList2

#define _VS3rFunctionLetExpression _VS3rPTGDefList2

extern void _VS4rFunctionLetExpression ELI_ARG((_TPPrFunctionLetExpression _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionLetExpression ELI_ARG((_TPPrFunctionLetExpression _currn,D_typePtr* _AS0expected_type));
extern void _VS6rFunctionLetExpression ELI_ARG((_TPPrFunctionLetExpression _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionLetExpression ELI_ARG((_TPPrFunctionLetExpression _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS8rFunctionLetExpression ELI_ARG((_TPPrFunctionLetExpression _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
#define _VS1rFunctionLetVarDefList2 _VS1rPTGDefList2

#define _VS2rFunctionLetVarDefList2 _VS2rPTGDefList2

#define _VS3rFunctionLetVarDefList2 _VS3rPTGDefList2

extern void _VS4rFunctionLetVarDefList2 ELI_ARG((_TPPrFunctionLetVarDefList2 _currn,int* _AS0_const168,int* _AS0_const167));
extern void _VS5rFunctionLetVarDefList2 ELI_ARG((_TPPrFunctionLetVarDefList2 _currn,int* _AS0_const168,int* _AS0_const167));
extern void _VS6rFunctionLetVarDefList2 ELI_ARG((_TPPrFunctionLetVarDefList2 _currn,int* _AS0_const168,int* _AS0_const167));
extern void _VS7rFunctionLetVarDefList2 ELI_ARG((_TPPrFunctionLetVarDefList2 _currn,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,bool* _AS0_const166,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionLetVarDefList2 ELI_ARG((_TPPrFunctionLetVarDefList2 _currn,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
#define _VS1rFunctionLetVarDefList1 _VS1rPTGDecl

#define _VS2rFunctionLetVarDefList1 _VS2rPTGDecl

#define _VS3rFunctionLetVarDefList1 _VS3rPTGDecl

extern void _VS4rFunctionLetVarDefList1 ELI_ARG((_TPPrFunctionLetVarDefList1 _currn,int* _AS0_const168,int* _AS0_const167));
extern void _VS5rFunctionLetVarDefList1 ELI_ARG((_TPPrFunctionLetVarDefList1 _currn,int* _AS0_const168,int* _AS0_const167));
extern void _VS6rFunctionLetVarDefList1 ELI_ARG((_TPPrFunctionLetVarDefList1 _currn,int* _AS0_const168,int* _AS0_const167));
extern void _VS7rFunctionLetVarDefList1 ELI_ARG((_TPPrFunctionLetVarDefList1 _currn,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,bool* _AS0_const166,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionLetVarDefList1 ELI_ARG((_TPPrFunctionLetVarDefList1 _currn,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionLetVarDef ELI_ARG((_TPPrFunctionLetVarDef _currn));
extern void _VS2rFunctionLetVarDef ELI_ARG((_TPPrFunctionLetVarDef _currn));
extern void _VS3rFunctionLetVarDef ELI_ARG((_TPPrFunctionLetVarDef _currn));
extern void _VS4rFunctionLetVarDef ELI_ARG((_TPPrFunctionLetVarDef _currn));
extern void _VS5rFunctionLetVarDef ELI_ARG((_TPPrFunctionLetVarDef _currn));
extern void _VS6rFunctionLetVarDef ELI_ARG((_TPPrFunctionLetVarDef _currn));
extern void _VS7rFunctionLetVarDef ELI_ARG((_TPPrFunctionLetVarDef _currn));
extern void _VS8rFunctionLetVarDef ELI_ARG((_TPPrFunctionLetVarDef _currn));
extern void _VS1rFunctionDoIsMessage ELI_ARG((_TPPrFunctionDoIsMessage _currn));
extern void _VS2rFunctionDoIsMessage ELI_ARG((_TPPrFunctionDoIsMessage _currn));
extern void _VS3rFunctionDoIsMessage ELI_ARG((_TPPrFunctionDoIsMessage _currn));
extern void _VS4rFunctionDoIsMessage ELI_ARG((_TPPrFunctionDoIsMessage _currn));
extern void _VS5rFunctionDoIsMessage ELI_ARG((_TPPrFunctionDoIsMessage _currn));
extern void _VS6rFunctionDoIsMessage ELI_ARG((_TPPrFunctionDoIsMessage _currn));
extern void _VS7rFunctionDoIsMessage ELI_ARG((_TPPrFunctionDoIsMessage _currn));
extern void _VS1rFunctionDoMessage ELI_ARG((_TPPrFunctionDoMessage _currn));
extern void _VS2rFunctionDoMessage ELI_ARG((_TPPrFunctionDoMessage _currn));
extern void _VS3rFunctionDoMessage ELI_ARG((_TPPrFunctionDoMessage _currn));
extern void _VS4rFunctionDoMessage ELI_ARG((_TPPrFunctionDoMessage _currn));
extern void _VS5rFunctionDoMessage ELI_ARG((_TPPrFunctionDoMessage _currn));
extern void _VS6rFunctionDoMessage ELI_ARG((_TPPrFunctionDoMessage _currn));
extern void _VS1rFunctionExpressionIsLambda ELI_ARG((_TPPrFunctionExpressionIsLambda _currn));
extern void _VS2rFunctionExpressionIsLambda ELI_ARG((_TPPrFunctionExpressionIsLambda _currn));
extern void _VS3rFunctionExpressionIsLambda ELI_ARG((_TPPrFunctionExpressionIsLambda _currn));
extern void _VS4rFunctionExpressionIsLambda ELI_ARG((_TPPrFunctionExpressionIsLambda _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS5rFunctionExpressionIsLambda ELI_ARG((_TPPrFunctionExpressionIsLambda _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS6rFunctionExpressionIsLambda ELI_ARG((_TPPrFunctionExpressionIsLambda _currn,int* _AS0index_num,int* _AS0_const168,int* _AS0_const167));
extern void _VS7rFunctionExpressionIsLambda ELI_ARG((_TPPrFunctionExpressionIsLambda _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionExpressionIsLambda ELI_ARG((_TPPrFunctionExpressionIsLambda _currn,int* _AS0index_num,StringTableKeyList* _AS0_const174,int* _AS0_const168,int* _AS0_const167,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS1rFunctionLambdaExpr ELI_ARG((_TPPrFunctionLambdaExpr _currn));
extern void _VS2rFunctionLambdaExpr ELI_ARG((_TPPrFunctionLambdaExpr _currn));
extern void _VS3rFunctionLambdaExpr ELI_ARG((_TPPrFunctionLambdaExpr _currn));
extern void _VS4rFunctionLambdaExpr ELI_ARG((_TPPrFunctionLambdaExpr _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS5rFunctionLambdaExpr ELI_ARG((_TPPrFunctionLambdaExpr _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS6rFunctionLambdaExpr ELI_ARG((_TPPrFunctionLambdaExpr _currn,D_typePtr* _AS0expected_type2,Environment* _AS0fn_arg_chn_pre));
extern void _VS7rFunctionLambdaExpr ELI_ARG((_TPPrFunctionLambdaExpr _currn,D_typePtr* _AS0expected_type2,Environment* _AS0fn_arg_chn_pre));
extern void _VS8rFunctionLambdaExpr ELI_ARG((_TPPrFunctionLambdaExpr _currn,D_typePtr* _AS0expected_type2,Environment* _AS0fn_arg_chn_pre));
#define _VS1rNoTyping _VS0Empty

#define _VS2rNoTyping _VS0Empty

extern void _VS3rNoTyping ELI_ARG((_TPPrNoTyping _currn));
extern void _VS4rNoTyping ELI_ARG((_TPPrNoTyping _currn));
#define _VS5rNoTyping _VS0Empty

extern void _VS6rNoTyping ELI_ARG((_TPPrNoTyping _currn,StringTableKey* _AS0_const10));
extern void _VS1rOptionalType ELI_ARG((_TPPrOptionalType _currn));
extern void _VS2rOptionalType ELI_ARG((_TPPrOptionalType _currn));
extern void _VS3rOptionalType ELI_ARG((_TPPrOptionalType _currn));
extern void _VS4rOptionalType ELI_ARG((_TPPrOptionalType _currn));
#define _VS5rOptionalType _VS0Empty

extern void _VS6rOptionalType ELI_ARG((_TPPrOptionalType _currn,StringTableKey* _AS0_const10));
extern void _VS1rFunctionDoIsAssignement ELI_ARG((_TPPrFunctionDoIsAssignement _currn));
extern void _VS2rFunctionDoIsAssignement ELI_ARG((_TPPrFunctionDoIsAssignement _currn));
extern void _VS3rFunctionDoIsAssignement ELI_ARG((_TPPrFunctionDoIsAssignement _currn));
extern void _VS4rFunctionDoIsAssignement ELI_ARG((_TPPrFunctionDoIsAssignement _currn));
extern void _VS5rFunctionDoIsAssignement ELI_ARG((_TPPrFunctionDoIsAssignement _currn));
extern void _VS6rFunctionDoIsAssignement ELI_ARG((_TPPrFunctionDoIsAssignement _currn));
extern void _VS7rFunctionDoIsAssignement ELI_ARG((_TPPrFunctionDoIsAssignement _currn));
extern void _VS1rFunctionDoIsEach ELI_ARG((_TPPrFunctionDoIsEach _currn));
extern void _VS2rFunctionDoIsEach ELI_ARG((_TPPrFunctionDoIsEach _currn));
extern void _VS3rFunctionDoIsEach ELI_ARG((_TPPrFunctionDoIsEach _currn));
extern void _VS4rFunctionDoIsEach ELI_ARG((_TPPrFunctionDoIsEach _currn));
extern void _VS5rFunctionDoIsEach ELI_ARG((_TPPrFunctionDoIsEach _currn));
extern void _VS6rFunctionDoIsEach ELI_ARG((_TPPrFunctionDoIsEach _currn));
extern void _VS7rFunctionDoIsEach ELI_ARG((_TPPrFunctionDoIsEach _currn));
extern void _VS1rFunctionStatAssign ELI_ARG((_TPPrFunctionStatAssign _currn));
extern void _VS2rFunctionStatAssign ELI_ARG((_TPPrFunctionStatAssign _currn));
extern void _VS3rFunctionStatAssign ELI_ARG((_TPPrFunctionStatAssign _currn));
extern void _VS4rFunctionStatAssign ELI_ARG((_TPPrFunctionStatAssign _currn));
extern void _VS5rFunctionStatAssign ELI_ARG((_TPPrFunctionStatAssign _currn));
extern void _VS6rFunctionStatAssign ELI_ARG((_TPPrFunctionStatAssign _currn));
extern void _VS1rFunctionEachStatSingle ELI_ARG((_TPPrFunctionEachStatSingle _currn));
extern void _VS2rFunctionEachStatSingle ELI_ARG((_TPPrFunctionEachStatSingle _currn));
extern void _VS3rFunctionEachStatSingle ELI_ARG((_TPPrFunctionEachStatSingle _currn));
extern void _VS4rFunctionEachStatSingle ELI_ARG((_TPPrFunctionEachStatSingle _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS5rFunctionEachStatSingle ELI_ARG((_TPPrFunctionEachStatSingle _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS6rFunctionEachStatSingle ELI_ARG((_TPPrFunctionEachStatSingle _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS7rFunctionEachStatSingle ELI_ARG((_TPPrFunctionEachStatSingle _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS1rFunctionEachStatMultiple ELI_ARG((_TPPrFunctionEachStatMultiple _currn));
extern void _VS2rFunctionEachStatMultiple ELI_ARG((_TPPrFunctionEachStatMultiple _currn));
extern void _VS3rFunctionEachStatMultiple ELI_ARG((_TPPrFunctionEachStatMultiple _currn));
extern void _VS4rFunctionEachStatMultiple ELI_ARG((_TPPrFunctionEachStatMultiple _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS5rFunctionEachStatMultiple ELI_ARG((_TPPrFunctionEachStatMultiple _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS6rFunctionEachStatMultiple ELI_ARG((_TPPrFunctionEachStatMultiple _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS7rFunctionEachStatMultiple ELI_ARG((_TPPrFunctionEachStatMultiple _currn,Environment* _AS0fn_arg_chn_pre));
extern void _VS1rDeclConstant ELI_ARG((_TPPrDeclConstant _currn));
extern void _VS2rDeclConstant ELI_ARG((_TPPrDeclConstant _currn));
#define _VS3rDeclConstant _VS0Empty

extern void _VS4rDeclConstant ELI_ARG((_TPPrDeclConstant _currn));
#define _VS5rDeclConstant _VS0Empty

#define _VS6rDeclConstant _VS0Empty

extern void _VS7rDeclConstant ELI_ARG((_TPPrDeclConstant _currn));
extern void _VS8rDeclConstant ELI_ARG((_TPPrDeclConstant _currn));
#define _VS9rDeclConstant _VS0Empty

extern void _VS10rDeclConstant ELI_ARG((_TPPrDeclConstant _currn));
extern void _VS1rConstantDecl ELI_ARG((_TPPrConstantDecl _currn));
extern void _VS2rConstantDecl ELI_ARG((_TPPrConstantDecl _currn));
extern void _VS3rConstantDecl ELI_ARG((_TPPrConstantDecl _currn));
extern void _VS4rConstantDecl ELI_ARG((_TPPrConstantDecl _currn));
extern void _VS5rConstantDecl ELI_ARG((_TPPrConstantDecl _currn));
extern void _VS6rConstantDecl ELI_ARG((_TPPrConstantDecl _currn));
extern void _VS1rConstantDefinition ELI_ARG((_TPPrConstantDefinition _currn));
extern void _VS2rConstantDefinition ELI_ARG((_TPPrConstantDefinition _currn));
extern void _VS3rConstantDefinition ELI_ARG((_TPPrConstantDefinition _currn));
extern void _VS4rConstantDefinition ELI_ARG((_TPPrConstantDefinition _currn));
extern void _VS5rConstantDefinition ELI_ARG((_TPPrConstantDefinition _currn));
extern void _VS6rConstantDefinition ELI_ARG((_TPPrConstantDefinition _currn));
extern void _VS1rConstantDefIdentifier ELI_ARG((_TPPrConstantDefIdentifier _currn));
extern void _VS2rConstantDefIdentifier ELI_ARG((_TPPrConstantDefIdentifier _currn));
extern void _VS1rConstantDefTypename ELI_ARG((_TPPrConstantDefTypename _currn));
extern void _VS2rConstantDefTypename ELI_ARG((_TPPrConstantDefTypename _currn));
extern void _VS1rFunctionDoIsNoreturn ELI_ARG((_TPPrFunctionDoIsNoreturn _currn));
#define _VS2rFunctionDoIsNoreturn _VS0Empty

#define _VS3rFunctionDoIsNoreturn _VS0Empty

extern void _VS4rFunctionDoIsNoreturn ELI_ARG((_TPPrFunctionDoIsNoreturn _currn));
extern void _VS5rFunctionDoIsNoreturn ELI_ARG((_TPPrFunctionDoIsNoreturn _currn));
extern void _VS6rFunctionDoIsNoreturn ELI_ARG((_TPPrFunctionDoIsNoreturn _currn));
extern void _VS7rFunctionDoIsNoreturn ELI_ARG((_TPPrFunctionDoIsNoreturn _currn));
extern void _VS1rFunctionNoReturn ELI_ARG((_TPPrFunctionNoReturn _currn));
extern void _VS1rFunctionPrimaryIsTuple ELI_ARG((_TPPrFunctionPrimaryIsTuple _currn));
extern void _VS2rFunctionPrimaryIsTuple ELI_ARG((_TPPrFunctionPrimaryIsTuple _currn));
extern void _VS3rFunctionPrimaryIsTuple ELI_ARG((_TPPrFunctionPrimaryIsTuple _currn));
extern void _VS4rFunctionPrimaryIsTuple ELI_ARG((_TPPrFunctionPrimaryIsTuple _currn));
extern void _VS5rFunctionPrimaryIsTuple ELI_ARG((_TPPrFunctionPrimaryIsTuple _currn));
extern void _VS6rFunctionPrimaryIsTuple ELI_ARG((_TPPrFunctionPrimaryIsTuple _currn,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionPrimaryIsTuple ELI_ARG((_TPPrFunctionPrimaryIsTuple _currn,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionPrimaryIsTuple ELI_ARG((_TPPrFunctionPrimaryIsTuple _currn,D_typePtr* _AS0expected_type2,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS9rFunctionPrimaryIsTuple ELI_ARG((_TPPrFunctionPrimaryIsTuple _currn,D_typePtr* _AS0expected_type2,PTGNode* _AS0fn_expr_code,int* _AS0_const163,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
extern void _VS1rFunctionTupleConstruction ELI_ARG((_TPPrFunctionTupleConstruction _currn));
extern void _VS2rFunctionTupleConstruction ELI_ARG((_TPPrFunctionTupleConstruction _currn));
extern void _VS3rFunctionTupleConstruction ELI_ARG((_TPPrFunctionTupleConstruction _currn));
extern void _VS4rFunctionTupleConstruction ELI_ARG((_TPPrFunctionTupleConstruction _currn,D_typePtr* _AS0expected_type));
extern void _VS5rFunctionTupleConstruction ELI_ARG((_TPPrFunctionTupleConstruction _currn,D_typePtr* _AS0expected_type));
extern void _VS6rFunctionTupleConstruction ELI_ARG((_TPPrFunctionTupleConstruction _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS7rFunctionTupleConstruction ELI_ARG((_TPPrFunctionTupleConstruction _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
extern void _VS8rFunctionTupleConstruction ELI_ARG((_TPPrFunctionTupleConstruction _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0expected_type2));
#define _VS1rFunctionTupleArgumentList2 _VS1rPTGDefList2

#define _VS2rFunctionTupleArgumentList2 _VS2rPTGDefList2

#define _VS3rFunctionTupleArgumentList2 _VS3rPTGDefList2

extern void _VS4rFunctionTupleArgumentList2 ELI_ARG((_TPPrFunctionTupleArgumentList2 _currn,D_types* _AS0_const193,int* _AS0_const168));
extern void _VS5rFunctionTupleArgumentList2 ELI_ARG((_TPPrFunctionTupleArgumentList2 _currn,D_types* _AS0_const193,int* _AS0_const168));
extern void _VS6rFunctionTupleArgumentList2 ELI_ARG((_TPPrFunctionTupleArgumentList2 _currn,D_types* _AS0_const193,D_types* _AS0_const192,int* _AS0_const168));
extern void _VS7rFunctionTupleArgumentList2 ELI_ARG((_TPPrFunctionTupleArgumentList2 _currn,D_types* _AS0_const193,D_types* _AS0_const192,StringTableKeyList* _AS0_const174,int* _AS0_const168,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionTupleArgumentList2 ELI_ARG((_TPPrFunctionTupleArgumentList2 _currn,D_types* _AS0_const193,D_types* _AS0_const192,PTGNode* _AS0_const191,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const168,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
#define _VS1rFunctionTupleArgumentList _VS1rPTGDecl

#define _VS2rFunctionTupleArgumentList _VS2rPTGDecl

#define _VS3rFunctionTupleArgumentList _VS3rPTGDecl

extern void _VS4rFunctionTupleArgumentList ELI_ARG((_TPPrFunctionTupleArgumentList _currn,D_types* _AS0_const193,int* _AS0_const168));
extern void _VS5rFunctionTupleArgumentList ELI_ARG((_TPPrFunctionTupleArgumentList _currn,D_types* _AS0_const193,int* _AS0_const168));
extern void _VS6rFunctionTupleArgumentList ELI_ARG((_TPPrFunctionTupleArgumentList _currn,D_types* _AS0_const193,D_types* _AS0_const192,int* _AS0_const168));
extern void _VS7rFunctionTupleArgumentList ELI_ARG((_TPPrFunctionTupleArgumentList _currn,D_types* _AS0_const193,D_types* _AS0_const192,StringTableKeyList* _AS0_const174,int* _AS0_const168,bool* _AS0_const166,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154));
extern void _VS8rFunctionTupleArgumentList ELI_ARG((_TPPrFunctionTupleArgumentList _currn,D_types* _AS0_const193,D_types* _AS0_const192,PTGNode* _AS0_const191,BindingList* _AS0_const186,StringTableKeyList* _AS0_const174,int* _AS0_const168,bool* _AS0_const166,int* _AS0_const163,int* _AS0_const158,int* _AS0_const156,bool* _AS0_const155,bool* _AS0_const154,StringTableKey* _AS0_const10));
#define _VS1rFunctionTupleArgument _VS1rPTGDecl

#define _VS2rFunctionTupleArgument _VS2rPTGDecl

#define _VS3rFunctionTupleArgument _VS3rPTGDecl

extern void _VS4rFunctionTupleArgument ELI_ARG((_TPPrFunctionTupleArgument _currn));
extern void _VS5rFunctionTupleArgument ELI_ARG((_TPPrFunctionTupleArgument _currn));
extern void _VS6rFunctionTupleArgument ELI_ARG((_TPPrFunctionTupleArgument _currn));
extern void _VS7rFunctionTupleArgument ELI_ARG((_TPPrFunctionTupleArgument _currn));
extern void _VS8rFunctionTupleArgument ELI_ARG((_TPPrFunctionTupleArgument _currn));
extern void _VS1rSymbolDefId ELI_ARG((_TPPrSymbolDefId _currn));
extern void _VS1rDeclAbstreeClass ELI_ARG((_TPPrDeclAbstreeClass _currn));
extern void _VS2rDeclAbstreeClass ELI_ARG((_TPPrDeclAbstreeClass _currn));
extern void _VS3rDeclAbstreeClass ELI_ARG((_TPPrDeclAbstreeClass _currn));
extern void _VS4rDeclAbstreeClass ELI_ARG((_TPPrDeclAbstreeClass _currn));
extern void _VS5rDeclAbstreeClass ELI_ARG((_TPPrDeclAbstreeClass _currn));
extern void _VS6rDeclAbstreeClass ELI_ARG((_TPPrDeclAbstreeClass _currn));
#define _VS7rDeclAbstreeClass _VS0Empty

extern void _VS8rDeclAbstreeClass ELI_ARG((_TPPrDeclAbstreeClass _currn));
#define _VS9rDeclAbstreeClass _VS0Empty

extern void _VS10rDeclAbstreeClass ELI_ARG((_TPPrDeclAbstreeClass _currn));
extern void _VS1rDeclAbstree ELI_ARG((_TPPrDeclAbstree _currn));
extern void _VS2rDeclAbstree ELI_ARG((_TPPrDeclAbstree _currn));
extern void _VS3rDeclAbstree ELI_ARG((_TPPrDeclAbstree _currn));
extern void _VS4rDeclAbstree ELI_ARG((_TPPrDeclAbstree _currn));
extern void _VS5rDeclAbstree ELI_ARG((_TPPrDeclAbstree _currn));
extern void _VS6rDeclAbstree ELI_ARG((_TPPrDeclAbstree _currn));
#define _VS7rDeclAbstree _VS0Empty

extern void _VS8rDeclAbstree ELI_ARG((_TPPrDeclAbstree _currn));
#define _VS9rDeclAbstree _VS0Empty

extern void _VS10rDeclAbstree ELI_ARG((_TPPrDeclAbstree _currn));
extern void _VS1rAbstreeClassDecl ELI_ARG((_TPPrAbstreeClassDecl _currn));
extern void _VS2rAbstreeClassDecl ELI_ARG((_TPPrAbstreeClassDecl _currn));
extern void _VS3rAbstreeClassDecl ELI_ARG((_TPPrAbstreeClassDecl _currn));
extern void _VS4rAbstreeClassDecl ELI_ARG((_TPPrAbstreeClassDecl _currn));
extern void _VS5rAbstreeClassDecl ELI_ARG((_TPPrAbstreeClassDecl _currn));
extern void _VS6rAbstreeClassDecl ELI_ARG((_TPPrAbstreeClassDecl _currn));
extern void _VS7rAbstreeClassDecl ELI_ARG((_TPPrAbstreeClassDecl _currn));
extern void _VS8rAbstreeClassDecl ELI_ARG((_TPPrAbstreeClassDecl _currn));
extern void _VS1rAbstreeDecl ELI_ARG((_TPPrAbstreeDecl _currn));
extern void _VS2rAbstreeDecl ELI_ARG((_TPPrAbstreeDecl _currn));
extern void _VS3rAbstreeDecl ELI_ARG((_TPPrAbstreeDecl _currn));
extern void _VS4rAbstreeDecl ELI_ARG((_TPPrAbstreeDecl _currn));
extern void _VS5rAbstreeDecl ELI_ARG((_TPPrAbstreeDecl _currn));
extern void _VS6rAbstreeDecl ELI_ARG((_TPPrAbstreeDecl _currn));
extern void _VS7rAbstreeDecl ELI_ARG((_TPPrAbstreeDecl _currn));
extern void _VS8rAbstreeDecl ELI_ARG((_TPPrAbstreeDecl _currn));
extern void _VS1rAbstreeDefIdIsTypename ELI_ARG((_TPPrAbstreeDefIdIsTypename _currn));
extern void _VS2rAbstreeDefIdIsTypename ELI_ARG((_TPPrAbstreeDefIdIsTypename _currn));
extern void _VS3rAbstreeDefIdIsTypename ELI_ARG((_TPPrAbstreeDefIdIsTypename _currn));
extern void _VS4rAbstreeDefIdIsTypename ELI_ARG((_TPPrAbstreeDefIdIsTypename _currn));
extern void _VS1rAbstreeDefIdIsIdent ELI_ARG((_TPPrAbstreeDefIdIsIdent _currn));
extern void _VS2rAbstreeDefIdIsIdent ELI_ARG((_TPPrAbstreeDefIdIsIdent _currn));
extern void _VS3rAbstreeDefIdIsIdent ELI_ARG((_TPPrAbstreeDefIdIsIdent _currn));
extern void _VS4rAbstreeDefIdIsIdent ELI_ARG((_TPPrAbstreeDefIdIsIdent _currn));
#define _VS1rNoAbstreeChanges _VS0Empty

#define _VS2rNoAbstreeChanges _VS0Empty

extern void _VS3rNoAbstreeChanges ELI_ARG((_TPPrNoAbstreeChanges _currn));
extern void _VS1rAbstreeChangeList2 ELI_ARG((_TPPrAbstreeChangeList2 _currn));
extern void _VS2rAbstreeChangeList2 ELI_ARG((_TPPrAbstreeChangeList2 _currn));
extern void _VS3rAbstreeChangeList2 ELI_ARG((_TPPrAbstreeChangeList2 _currn));
#define _VS1rAbstreeChangeIsPrefix _VS1rPTGDecl

#define _VS2rAbstreeChangeIsPrefix _VS2rPTGDecl

extern void _VS3rAbstreeChangeIsPrefix ELI_ARG((_TPPrAbstreeChangeIsPrefix _currn));
#define _VS1rAbstreeChangeIsCombination _VS1rPTGDecl

#define _VS2rAbstreeChangeIsCombination _VS2rPTGDecl

extern void _VS3rAbstreeChangeIsCombination ELI_ARG((_TPPrAbstreeChangeIsCombination _currn));
#define _VS1rAbstreeChangeIsInherit _VS1rPTGDecl

#define _VS2rAbstreeChangeIsInherit _VS2rPTGDecl

extern void _VS3rAbstreeChangeIsInherit ELI_ARG((_TPPrAbstreeChangeIsInherit _currn));
#define _VS1rAbstreeChangeIsTrafo _VS1rPTGDecl

#define _VS2rAbstreeChangeIsTrafo _VS2rPTGDecl

extern void _VS3rAbstreeChangeIsTrafo ELI_ARG((_TPPrAbstreeChangeIsTrafo _currn));
#define _VS1rAbstreeChangeIsDependency _VS1rPTGDecl

#define _VS2rAbstreeChangeIsDependency _VS2rPTGDecl

extern void _VS3rAbstreeChangeIsDependency ELI_ARG((_TPPrAbstreeChangeIsDependency _currn));
extern void _VS1rAbstreePrefix ELI_ARG((_TPPrAbstreePrefix _currn));
extern void _VS2rAbstreePrefix ELI_ARG((_TPPrAbstreePrefix _currn));
extern void _VS3rAbstreePrefix ELI_ARG((_TPPrAbstreePrefix _currn));
extern void _VS1rNewRuleprefixIsTypename ELI_ARG((_TPPrNewRuleprefixIsTypename _currn));
extern void _VS2rNewRuleprefixIsTypename ELI_ARG((_TPPrNewRuleprefixIsTypename _currn));
extern void _VS1rNewRuleprefixIsIdentifier ELI_ARG((_TPPrNewRuleprefixIsIdentifier _currn));
extern void _VS2rNewRuleprefixIsIdentifier ELI_ARG((_TPPrNewRuleprefixIsIdentifier _currn));
extern void _VS1rAbstreeCombination ELI_ARG((_TPPrAbstreeCombination _currn));
extern void _VS2rAbstreeCombination ELI_ARG((_TPPrAbstreeCombination _currn));
extern void _VS3rAbstreeCombination ELI_ARG((_TPPrAbstreeCombination _currn));
extern void _VS1rAbstreeReferenceList2 ELI_ARG((_TPPrAbstreeReferenceList2 _currn));
extern void _VS2rAbstreeReferenceList2 ELI_ARG((_TPPrAbstreeReferenceList2 _currn));
extern void _VS1rAbstreeReferenceList1 ELI_ARG((_TPPrAbstreeReferenceList1 _currn));
extern void _VS2rAbstreeReferenceList1 ELI_ARG((_TPPrAbstreeReferenceList1 _currn));
extern void _VS1rAbstreeReferenceIsIdentifier ELI_ARG((_TPPrAbstreeReferenceIsIdentifier _currn));
extern void _VS2rAbstreeReferenceIsIdentifier ELI_ARG((_TPPrAbstreeReferenceIsIdentifier _currn));
extern void _VS3rAbstreeReferenceIsIdentifier ELI_ARG((_TPPrAbstreeReferenceIsIdentifier _currn));
extern void _VS1rAbstreeReferenceIsTypename ELI_ARG((_TPPrAbstreeReferenceIsTypename _currn));
extern void _VS2rAbstreeReferenceIsTypename ELI_ARG((_TPPrAbstreeReferenceIsTypename _currn));
extern void _VS3rAbstreeReferenceIsTypename ELI_ARG((_TPPrAbstreeReferenceIsTypename _currn));
extern void _VS1rAbstreeInheritance ELI_ARG((_TPPrAbstreeInheritance _currn));
extern void _VS2rAbstreeInheritance ELI_ARG((_TPPrAbstreeInheritance _currn));
extern void _VS1rAbstreeTrafoTo ELI_ARG((_TPPrAbstreeTrafoTo _currn));
extern void _VS2rAbstreeTrafoTo ELI_ARG((_TPPrAbstreeTrafoTo _currn));
extern void _VS1rAbstreeTrafoFrom ELI_ARG((_TPPrAbstreeTrafoFrom _currn));
extern void _VS2rAbstreeTrafoFrom ELI_ARG((_TPPrAbstreeTrafoFrom _currn));
extern void _VS1rAbstreeDependency ELI_ARG((_TPPrAbstreeDependency _currn));
extern void _VS2rAbstreeDependency ELI_ARG((_TPPrAbstreeDependency _currn));
#define _VS1rNoAbstreeRules _VS0Empty

#define _VS2rNoAbstreeRules _VS0Empty

#define _VS3rNoAbstreeRules _VS0Empty

#define _VS4rNoAbstreeRules _VS0Empty

#define _VS5rNoAbstreeRules _VS0Empty

#define _VS6rNoAbstreeRules _VS0Empty

extern void _VS7rNoAbstreeRules ELI_ARG((_TPPrNoAbstreeRules _currn));
extern void _VS1rAbstreeRulesExist ELI_ARG((_TPPrAbstreeRulesExist _currn));
extern void _VS2rAbstreeRulesExist ELI_ARG((_TPPrAbstreeRulesExist _currn));
extern void _VS3rAbstreeRulesExist ELI_ARG((_TPPrAbstreeRulesExist _currn));
extern void _VS4rAbstreeRulesExist ELI_ARG((_TPPrAbstreeRulesExist _currn));
extern void _VS5rAbstreeRulesExist ELI_ARG((_TPPrAbstreeRulesExist _currn));
extern void _VS6rAbstreeRulesExist ELI_ARG((_TPPrAbstreeRulesExist _currn));
extern void _VS7rAbstreeRulesExist ELI_ARG((_TPPrAbstreeRulesExist _currn));
extern void _VS1rAbstreeRuleList2 ELI_ARG((_TPPrAbstreeRuleList2 _currn));
extern void _VS2rAbstreeRuleList2 ELI_ARG((_TPPrAbstreeRuleList2 _currn));
extern void _VS3rAbstreeRuleList2 ELI_ARG((_TPPrAbstreeRuleList2 _currn));
extern void _VS4rAbstreeRuleList2 ELI_ARG((_TPPrAbstreeRuleList2 _currn));
extern void _VS5rAbstreeRuleList2 ELI_ARG((_TPPrAbstreeRuleList2 _currn));
extern void _VS6rAbstreeRuleList2 ELI_ARG((_TPPrAbstreeRuleList2 _currn));
extern void _VS7rAbstreeRuleList2 ELI_ARG((_TPPrAbstreeRuleList2 _currn));
extern void _VS1rAbstreeRuleList1 ELI_ARG((_TPPrAbstreeRuleList1 _currn));
extern void _VS2rAbstreeRuleList1 ELI_ARG((_TPPrAbstreeRuleList1 _currn));
extern void _VS3rAbstreeRuleList1 ELI_ARG((_TPPrAbstreeRuleList1 _currn));
extern void _VS4rAbstreeRuleList1 ELI_ARG((_TPPrAbstreeRuleList1 _currn));
extern void _VS5rAbstreeRuleList1 ELI_ARG((_TPPrAbstreeRuleList1 _currn));
extern void _VS6rAbstreeRuleList1 ELI_ARG((_TPPrAbstreeRuleList1 _currn));
extern void _VS7rAbstreeRuleList1 ELI_ARG((_TPPrAbstreeRuleList1 _currn));
#define _VS1rAbstreeRuleIsTermDecl _VS1rPTGDecl

#define _VS2rAbstreeRuleIsTermDecl _VS2rPTGDecl

#define _VS3rAbstreeRuleIsTermDecl _VS3rPTGDecl

#define _VS4rAbstreeRuleIsTermDecl _VS4rPTGDecl

#define _VS5rAbstreeRuleIsTermDecl _VS0Empty

#define _VS6rAbstreeRuleIsTermDecl _VS0Empty

extern void _VS7rAbstreeRuleIsTermDecl ELI_ARG((_TPPrAbstreeRuleIsTermDecl _currn));
extern void _VS1rAbstreeTermDeclAs ELI_ARG((_TPPrAbstreeTermDeclAs _currn));
extern void _VS2rAbstreeTermDeclAs ELI_ARG((_TPPrAbstreeTermDeclAs _currn));
extern void _VS3rAbstreeTermDeclAs ELI_ARG((_TPPrAbstreeTermDeclAs _currn));
extern void _VS4rAbstreeTermDeclAs ELI_ARG((_TPPrAbstreeTermDeclAs _currn));
extern void _VS5rAbstreeTermDeclAs ELI_ARG((_TPPrAbstreeTermDeclAs _currn));
extern void _VS1rAbstreeTermDeclIs ELI_ARG((_TPPrAbstreeTermDeclIs _currn));
extern void _VS2rAbstreeTermDeclIs ELI_ARG((_TPPrAbstreeTermDeclIs _currn));
extern void _VS3rAbstreeTermDeclIs ELI_ARG((_TPPrAbstreeTermDeclIs _currn));
extern void _VS4rAbstreeTermDeclIs ELI_ARG((_TPPrAbstreeTermDeclIs _currn));
extern void _VS5rAbstreeTermDeclIs ELI_ARG((_TPPrAbstreeTermDeclIs _currn));
#define _VS1rAbstreeTermDeclName _VS1rPTGDecl

#define _VS2rAbstreeTermDeclName _VS0Empty

extern void _VS3rAbstreeTermDeclName ELI_ARG((_TPPrAbstreeTermDeclName _currn));
#define _VS4rAbstreeTermDeclName _VS3rPTGDecl

extern void _VS5rAbstreeTermDeclName ELI_ARG((_TPPrAbstreeTermDeclName _currn));
extern void _VS1rTreeSymbolDefId ELI_ARG((_TPPrTreeSymbolDefId _currn));
extern void _VS2rTreeSymbolDefId ELI_ARG((_TPPrTreeSymbolDefId _currn));
extern void _VS3rTreeSymbolDefId ELI_ARG((_TPPrTreeSymbolDefId _currn));
extern void _VS1rConversionReferenceTypename ELI_ARG((_TPPrConversionReferenceTypename _currn));
extern void _VS2rConversionReferenceTypename ELI_ARG((_TPPrConversionReferenceTypename _currn));
extern void _VS1rConversionReferenceIdentifier ELI_ARG((_TPPrConversionReferenceIdentifier _currn));
extern void _VS2rConversionReferenceIdentifier ELI_ARG((_TPPrConversionReferenceIdentifier _currn));
extern void _VS1rConversionReferenceNone ELI_ARG((_TPPrConversionReferenceNone _currn));
extern void _VS2rConversionReferenceNone ELI_ARG((_TPPrConversionReferenceNone _currn));
#define _VS1rAbstreeRuleIsProduction _VS1rPTGDecl

#define _VS2rAbstreeRuleIsProduction _VS0Empty

#define _VS3rAbstreeRuleIsProduction _VS2rPTGDecl

#define _VS4rAbstreeRuleIsProduction _VS3rPTGDecl

#define _VS5rAbstreeRuleIsProduction _VS4rPTGDecl

#define _VS6rAbstreeRuleIsProduction _VS5rPTGDecl

extern void _VS7rAbstreeRuleIsProduction ELI_ARG((_TPPrAbstreeRuleIsProduction _currn));
extern void _VS1rAbstreeProduction ELI_ARG((_TPPrAbstreeProduction _currn));
extern void _VS2rAbstreeProduction ELI_ARG((_TPPrAbstreeProduction _currn));
extern void _VS3rAbstreeProduction ELI_ARG((_TPPrAbstreeProduction _currn));
extern void _VS4rAbstreeProduction ELI_ARG((_TPPrAbstreeProduction _currn));
extern void _VS5rAbstreeProduction ELI_ARG((_TPPrAbstreeProduction _currn));
extern void _VS6rAbstreeProduction ELI_ARG((_TPPrAbstreeProduction _currn));
extern void _VS1rSymbolList2 ELI_ARG((_TPPrSymbolList2 _currn));
extern void _VS2rSymbolList2 ELI_ARG((_TPPrSymbolList2 _currn));
extern void _VS3rSymbolList2 ELI_ARG((_TPPrSymbolList2 _currn));
extern void _VS4rSymbolList2 ELI_ARG((_TPPrSymbolList2 _currn));
extern void _VS1rNoSymbols ELI_ARG((_TPPrNoSymbols _currn));
#define _VS2rNoSymbols _VS0Empty

#define _VS3rNoSymbols _VS0Empty

extern void _VS4rNoSymbols ELI_ARG((_TPPrNoSymbols _currn));
extern void _VS1rSymbolIsLiteral ELI_ARG((_TPPrSymbolIsLiteral _currn));
#define _VS2rSymbolIsLiteral _VS0Empty

#define _VS3rSymbolIsLiteral _VS0Empty

extern void _VS4rSymbolIsLiteral ELI_ARG((_TPPrSymbolIsLiteral _currn));
extern void _VS1rSymbolIsCreation ELI_ARG((_TPPrSymbolIsCreation _currn));
extern void _VS2rSymbolIsCreation ELI_ARG((_TPPrSymbolIsCreation _currn));
extern void _VS3rSymbolIsCreation ELI_ARG((_TPPrSymbolIsCreation _currn));
extern void _VS4rSymbolIsCreation ELI_ARG((_TPPrSymbolIsCreation _currn));
extern void _VS1rSymbolIsReference ELI_ARG((_TPPrSymbolIsReference _currn));
extern void _VS2rSymbolIsReference ELI_ARG((_TPPrSymbolIsReference _currn));
extern void _VS3rSymbolIsReference ELI_ARG((_TPPrSymbolIsReference _currn));
extern void _VS4rSymbolIsReference ELI_ARG((_TPPrSymbolIsReference _currn));
extern void _VS1rSymbolReferenceOptTree ELI_ARG((_TPPrSymbolReferenceOptTree _currn));
extern void _VS2rSymbolReferenceOptTree ELI_ARG((_TPPrSymbolReferenceOptTree _currn));
extern void _VS3rSymbolReferenceOptTree ELI_ARG((_TPPrSymbolReferenceOptTree _currn));
extern void _VS4rSymbolReferenceOptTree ELI_ARG((_TPPrSymbolReferenceOptTree _currn));
extern void _VS5rSymbolReferenceOptTree ELI_ARG((_TPPrSymbolReferenceOptTree _currn));
extern void _VS1rSymbolReference ELI_ARG((_TPPrSymbolReference _currn));
extern void _VS2rSymbolReference ELI_ARG((_TPPrSymbolReference _currn));
extern void _VS1rNoTreeHint ELI_ARG((_TPPrNoTreeHint _currn));
#define _VS2rNoTreeHint _VS0Empty

#define _VS3rNoTreeHint _VS0Empty

extern void _VS4rNoTreeHint ELI_ARG((_TPPrNoTreeHint _currn,StringTableKey* _AS0_const10));
extern void _VS1rOptTreeHint ELI_ARG((_TPPrOptTreeHint _currn));
extern void _VS2rOptTreeHint ELI_ARG((_TPPrOptTreeHint _currn));
#define _VS3rOptTreeHint _VS0Empty

extern void _VS4rOptTreeHint ELI_ARG((_TPPrOptTreeHint _currn,StringTableKey* _AS0_const10));
extern void _VS1rRulenameExists ELI_ARG((_TPPrRulenameExists _currn));
extern void _VS2rRulenameExists ELI_ARG((_TPPrRulenameExists _currn));
extern void _VS3rRulenameExists ELI_ARG((_TPPrRulenameExists _currn));
#define _VS1rNoRulename _VS0Empty

extern void _VS2rNoRulename ELI_ARG((_TPPrNoRulename _currn));
extern void _VS3rNoRulename ELI_ARG((_TPPrNoRulename _currn));
extern void _VS1rRulenameIsTypename ELI_ARG((_TPPrRulenameIsTypename _currn));
extern void _VS2rRulenameIsTypename ELI_ARG((_TPPrRulenameIsTypename _currn));
extern void _VS1rRulenameIsIdentifier ELI_ARG((_TPPrRulenameIsIdentifier _currn));
extern void _VS2rRulenameIsIdentifier ELI_ARG((_TPPrRulenameIsIdentifier _currn));
extern void _VS1rDeclGlobalAttribute ELI_ARG((_TPPrDeclGlobalAttribute _currn));
extern void _VS2rDeclGlobalAttribute ELI_ARG((_TPPrDeclGlobalAttribute _currn));
#define _VS3rDeclGlobalAttribute _VS0Empty

extern void _VS4rDeclGlobalAttribute ELI_ARG((_TPPrDeclGlobalAttribute _currn));
#define _VS5rDeclGlobalAttribute _VS0Empty

#define _VS6rDeclGlobalAttribute _VS0Empty

#define _VS7rDeclGlobalAttribute _VS0Empty

extern void _VS8rDeclGlobalAttribute ELI_ARG((_TPPrDeclGlobalAttribute _currn));
#define _VS9rDeclGlobalAttribute _VS0Empty

extern void _VS10rDeclGlobalAttribute ELI_ARG((_TPPrDeclGlobalAttribute _currn));
extern void _VS1rGlobalAttributeDecl ELI_ARG((_TPPrGlobalAttributeDecl _currn));
extern void _VS2rGlobalAttributeDecl ELI_ARG((_TPPrGlobalAttributeDecl _currn));
extern void _VS3rGlobalAttributeDecl ELI_ARG((_TPPrGlobalAttributeDecl _currn));
extern void _VS4rGlobalAttributeDecl ELI_ARG((_TPPrGlobalAttributeDecl _currn));
#define _VS1rAttributeClassInherited _VS0Empty

extern void _VS2rAttributeClassInherited ELI_ARG((_TPPrAttributeClassInherited _currn));
#define _VS1rAttributeClassSynthesized _VS0Empty

extern void _VS2rAttributeClassSynthesized ELI_ARG((_TPPrAttributeClassSynthesized _currn));
#define _VS1rAttributeClassThread _VS0Empty

extern void _VS2rAttributeClassThread ELI_ARG((_TPPrAttributeClassThread _currn));
#define _VS1rAttributeClassChain _VS0Empty

extern void _VS2rAttributeClassChain ELI_ARG((_TPPrAttributeClassChain _currn));
#define _VS1rAttributeClassInfer _VS0Empty

extern void _VS2rAttributeClassInfer ELI_ARG((_TPPrAttributeClassInfer _currn));
extern void _VS1rGlobalAttributeDefId ELI_ARG((_TPPrGlobalAttributeDefId _currn));
extern void _VS2rGlobalAttributeDefId ELI_ARG((_TPPrGlobalAttributeDefId _currn));
extern void _VS1rGlobalAttributeDefIdList2 ELI_ARG((_TPPrGlobalAttributeDefIdList2 _currn));
extern void _VS2rGlobalAttributeDefIdList2 ELI_ARG((_TPPrGlobalAttributeDefIdList2 _currn));
extern void _VS1rGlobalAttributeDefIdList1 ELI_ARG((_TPPrGlobalAttributeDefIdList1 _currn));
extern void _VS2rGlobalAttributeDefIdList1 ELI_ARG((_TPPrGlobalAttributeDefIdList1 _currn));
extern void _VS1rDeclSymbol ELI_ARG((_TPPrDeclSymbol _currn));
extern void _VS2rDeclSymbol ELI_ARG((_TPPrDeclSymbol _currn));
#define _VS3rDeclSymbol _VS0Empty

extern void _VS4rDeclSymbol ELI_ARG((_TPPrDeclSymbol _currn));
extern void _VS5rDeclSymbol ELI_ARG((_TPPrDeclSymbol _currn));
#define _VS6rDeclSymbol _VS0Empty

extern void _VS7rDeclSymbol ELI_ARG((_TPPrDeclSymbol _currn));
extern void _VS8rDeclSymbol ELI_ARG((_TPPrDeclSymbol _currn));
#define _VS9rDeclSymbol _VS0Empty

extern void _VS10rDeclSymbol ELI_ARG((_TPPrDeclSymbol _currn));
extern void _VS1rSymbolDecl ELI_ARG((_TPPrSymbolDecl _currn));
extern void _VS2rSymbolDecl ELI_ARG((_TPPrSymbolDecl _currn));
extern void _VS3rSymbolDecl ELI_ARG((_TPPrSymbolDecl _currn));
extern void _VS4rSymbolDecl ELI_ARG((_TPPrSymbolDecl _currn));
extern void _VS5rSymbolDecl ELI_ARG((_TPPrSymbolDecl _currn));
extern void _VS6rSymbolDecl ELI_ARG((_TPPrSymbolDecl _currn));
extern void _VS7rSymbolDecl ELI_ARG((_TPPrSymbolDecl _currn));
extern void _VS1rSymbolClassificationSymbol ELI_ARG((_TPPrSymbolClassificationSymbol _currn));
extern void _VS1rSymbolClassificationClassSymbol ELI_ARG((_TPPrSymbolClassificationClassSymbol _currn));
extern void _VS1rSymbolOptionList2 ELI_ARG((_TPPrSymbolOptionList2 _currn));
extern void _VS2rSymbolOptionList2 ELI_ARG((_TPPrSymbolOptionList2 _currn));
extern void _VS3rSymbolOptionList2 ELI_ARG((_TPPrSymbolOptionList2 _currn));
extern void _VS4rSymbolOptionList2 ELI_ARG((_TPPrSymbolOptionList2 _currn));
extern void _VS5rSymbolOptionList2 ELI_ARG((_TPPrSymbolOptionList2 _currn));
extern void _VS6rSymbolOptionList2 ELI_ARG((_TPPrSymbolOptionList2 _currn));
#define _VS1rNoSymbolOptions _VS0Empty

#define _VS2rNoSymbolOptions _VS0Empty

extern void _VS3rNoSymbolOptions ELI_ARG((_TPPrNoSymbolOptions _currn));
extern void _VS4rNoSymbolOptions ELI_ARG((_TPPrNoSymbolOptions _currn));
#define _VS5rNoSymbolOptions _VS0Empty

extern void _VS6rNoSymbolOptions ELI_ARG((_TPPrNoSymbolOptions _currn));
#define _VS1rSymbolOptionAttributes _VS1rPTGDecl

#define _VS2rSymbolOptionAttributes _VS2rPTGDecl

extern void _VS3rSymbolOptionAttributes ELI_ARG((_TPPrSymbolOptionAttributes _currn));
extern void _VS4rSymbolOptionAttributes ELI_ARG((_TPPrSymbolOptionAttributes _currn));
#define _VS5rSymbolOptionAttributes _VS0Empty

extern void _VS6rSymbolOptionAttributes ELI_ARG((_TPPrSymbolOptionAttributes _currn));
#define _VS1rSymbolOptionInheritance _VS1rPTGDecl

#define _VS2rSymbolOptionInheritance _VS0Empty

extern void _VS3rSymbolOptionInheritance ELI_ARG((_TPPrSymbolOptionInheritance _currn));
extern void _VS4rSymbolOptionInheritance ELI_ARG((_TPPrSymbolOptionInheritance _currn));
#define _VS5rSymbolOptionInheritance _VS4rPTGDecl

extern void _VS6rSymbolOptionInheritance ELI_ARG((_TPPrSymbolOptionInheritance _currn));
extern void _VS1rSymbolInheritance ELI_ARG((_TPPrSymbolInheritance _currn));
extern void _VS2rSymbolInheritance ELI_ARG((_TPPrSymbolInheritance _currn));
extern void _VS3rSymbolInheritance ELI_ARG((_TPPrSymbolInheritance _currn));
extern void _VS4rSymbolInheritance ELI_ARG((_TPPrSymbolInheritance _currn));
extern void _VS5rSymbolInheritance ELI_ARG((_TPPrSymbolInheritance _currn));
#define _VS1rSymbolReferenceOptTreeList2 _VS1rPTGDefList2

extern void _VS2rSymbolReferenceOptTreeList2 ELI_ARG((_TPPrSymbolReferenceOptTreeList2 _currn));
extern void _VS3rSymbolReferenceOptTreeList2 ELI_ARG((_TPPrSymbolReferenceOptTreeList2 _currn));
#define _VS4rSymbolReferenceOptTreeList2 _VS4rPTGDefList2

extern void _VS5rSymbolReferenceOptTreeList2 ELI_ARG((_TPPrSymbolReferenceOptTreeList2 _currn));
#define _VS1rSymbolReferenceOptTreeList1 _VS1rPTGDecl

extern void _VS2rSymbolReferenceOptTreeList1 ELI_ARG((_TPPrSymbolReferenceOptTreeList1 _currn));
extern void _VS3rSymbolReferenceOptTreeList1 ELI_ARG((_TPPrSymbolReferenceOptTreeList1 _currn));
#define _VS4rSymbolReferenceOptTreeList1 _VS4rPTGDecl

extern void _VS5rSymbolReferenceOptTreeList1 ELI_ARG((_TPPrSymbolReferenceOptTreeList1 _currn));
#define _VS1rSymbolLocalAttributes _VS1rPTGDecl

#define _VS2rSymbolLocalAttributes _VS2rPTGDecl

extern void _VS3rSymbolLocalAttributes ELI_ARG((_TPPrSymbolLocalAttributes _currn));
#define _VS4rSymbolLocalAttributes _VS4rPTGDecl

extern void _VS5rSymbolLocalAttributes ELI_ARG((_TPPrSymbolLocalAttributes _currn));
#define _VS1rSymbolLocalAttributeDeclList2 _VS1rPTGDefList2

#define _VS2rSymbolLocalAttributeDeclList2 _VS2rPTGDefList2

extern void _VS3rSymbolLocalAttributeDeclList2 ELI_ARG((_TPPrSymbolLocalAttributeDeclList2 _currn));
#define _VS4rSymbolLocalAttributeDeclList2 _VS4rPTGDefList2

extern void _VS5rSymbolLocalAttributeDeclList2 ELI_ARG((_TPPrSymbolLocalAttributeDeclList2 _currn));
#define _VS1rSymbolLocalAttributeDeclList1 _VS1rPTGDecl

#define _VS2rSymbolLocalAttributeDeclList1 _VS2rPTGDecl

extern void _VS3rSymbolLocalAttributeDeclList1 ELI_ARG((_TPPrSymbolLocalAttributeDeclList1 _currn));
#define _VS4rSymbolLocalAttributeDeclList1 _VS4rPTGDecl

extern void _VS5rSymbolLocalAttributeDeclList1 ELI_ARG((_TPPrSymbolLocalAttributeDeclList1 _currn));
extern void _VS1rSymbolLocalAttributeDeclSameType ELI_ARG((_TPPrSymbolLocalAttributeDeclSameType _currn));
extern void _VS2rSymbolLocalAttributeDeclSameType ELI_ARG((_TPPrSymbolLocalAttributeDeclSameType _currn));
extern void _VS3rSymbolLocalAttributeDeclSameType ELI_ARG((_TPPrSymbolLocalAttributeDeclSameType _currn));
extern void _VS4rSymbolLocalAttributeDeclSameType ELI_ARG((_TPPrSymbolLocalAttributeDeclSameType _currn));
extern void _VS5rSymbolLocalAttributeDeclSameType ELI_ARG((_TPPrSymbolLocalAttributeDeclSameType _currn));
extern void _VS1rSymbolLocalAttributeDeclDifferentType ELI_ARG((_TPPrSymbolLocalAttributeDeclDifferentType _currn));
extern void _VS2rSymbolLocalAttributeDeclDifferentType ELI_ARG((_TPPrSymbolLocalAttributeDeclDifferentType _currn));
extern void _VS3rSymbolLocalAttributeDeclDifferentType ELI_ARG((_TPPrSymbolLocalAttributeDeclDifferentType _currn));
extern void _VS4rSymbolLocalAttributeDeclDifferentType ELI_ARG((_TPPrSymbolLocalAttributeDeclDifferentType _currn));
extern void _VS5rSymbolLocalAttributeDeclDifferentType ELI_ARG((_TPPrSymbolLocalAttributeDeclDifferentType _currn));
#define _VS1rSymbolLocalAttributeDeclNoClassList2 _VS1rPTGDefList2

#define _VS2rSymbolLocalAttributeDeclNoClassList2 _VS2rPTGDefList2

extern void _VS3rSymbolLocalAttributeDeclNoClassList2 ELI_ARG((_TPPrSymbolLocalAttributeDeclNoClassList2 _currn));
#define _VS4rSymbolLocalAttributeDeclNoClassList2 _VS4rPTGDefList2

extern void _VS5rSymbolLocalAttributeDeclNoClassList2 ELI_ARG((_TPPrSymbolLocalAttributeDeclNoClassList2 _currn));
#define _VS1rSymbolLocalAttributeDeclNoClassList1 _VS1rPTGDecl

#define _VS2rSymbolLocalAttributeDeclNoClassList1 _VS2rPTGDecl

extern void _VS3rSymbolLocalAttributeDeclNoClassList1 ELI_ARG((_TPPrSymbolLocalAttributeDeclNoClassList1 _currn));
#define _VS4rSymbolLocalAttributeDeclNoClassList1 _VS4rPTGDecl

extern void _VS5rSymbolLocalAttributeDeclNoClassList1 ELI_ARG((_TPPrSymbolLocalAttributeDeclNoClassList1 _currn));
extern void _VS1rSymbolLocalAttributeDeclNoClass ELI_ARG((_TPPrSymbolLocalAttributeDeclNoClass _currn));
extern void _VS2rSymbolLocalAttributeDeclNoClass ELI_ARG((_TPPrSymbolLocalAttributeDeclNoClass _currn));
extern void _VS3rSymbolLocalAttributeDeclNoClass ELI_ARG((_TPPrSymbolLocalAttributeDeclNoClass _currn));
extern void _VS4rSymbolLocalAttributeDeclNoClass ELI_ARG((_TPPrSymbolLocalAttributeDeclNoClass _currn));
extern void _VS5rSymbolLocalAttributeDeclNoClass ELI_ARG((_TPPrSymbolLocalAttributeDeclNoClass _currn));
extern void _VS1rSymbolLocalAttributeDefIdList2 ELI_ARG((_TPPrSymbolLocalAttributeDefIdList2 _currn));
#define _VS2rSymbolLocalAttributeDefIdList2 _VS2rPTGDefList2

extern void _VS3rSymbolLocalAttributeDefIdList2 ELI_ARG((_TPPrSymbolLocalAttributeDefIdList2 _currn));
extern void _VS1rSymbolLocalAttributeDefIdList1 ELI_ARG((_TPPrSymbolLocalAttributeDefIdList1 _currn));
#define _VS2rSymbolLocalAttributeDefIdList1 _VS2rPTGDecl

extern void _VS3rSymbolLocalAttributeDefIdList1 ELI_ARG((_TPPrSymbolLocalAttributeDefIdList1 _currn));
extern void _VS1rSymbolLocalAttributeDefId ELI_ARG((_TPPrSymbolLocalAttributeDefId _currn));
extern void _VS2rSymbolLocalAttributeDefId ELI_ARG((_TPPrSymbolLocalAttributeDefId _currn));
extern void _VS3rSymbolLocalAttributeDefId ELI_ARG((_TPPrSymbolLocalAttributeDefId _currn));
extern void _VS1rSymbolLocalAttributeReference ELI_ARG((_TPPrSymbolLocalAttributeReference _currn));
extern void _VS2rSymbolLocalAttributeReference ELI_ARG((_TPPrSymbolLocalAttributeReference _currn));
extern void _VS3rSymbolLocalAttributeReference ELI_ARG((_TPPrSymbolLocalAttributeReference _currn));
extern void _VS4rSymbolLocalAttributeReference ELI_ARG((_TPPrSymbolLocalAttributeReference _currn,AttributeDir* _AS0dir,D_LocalAttributeClass* _AS0attr_class));
extern void _VS1rLocalAttributeClassUnknown ELI_ARG((_TPPrLocalAttributeClassUnknown _currn));
extern void _VS1rLocalAttributeClassInherited ELI_ARG((_TPPrLocalAttributeClassInherited _currn));
extern void _VS1rLocalAttributeClassSynthesized ELI_ARG((_TPPrLocalAttributeClassSynthesized _currn));
extern void _VS1rLocalAttributeClassTail ELI_ARG((_TPPrLocalAttributeClassTail _currn));
extern void _VS1rLocalAttributeClassHead ELI_ARG((_TPPrLocalAttributeClassHead _currn));
extern void _VS1rLocalAttributeReference ELI_ARG((_TPPrLocalAttributeReference _currn));
#define _VS1rNoSymbolComputations _VS0Empty

#define _VS2rNoSymbolComputations _VS0Empty

#define _VS3rNoSymbolComputations _VS0Empty

#define _VS4rNoSymbolComputations _VS0Empty

extern void _VS5rNoSymbolComputations ELI_ARG((_TPPrNoSymbolComputations _currn));
extern void _VS6rNoSymbolComputations ELI_ARG((_TPPrNoSymbolComputations _currn));
extern void _VS7rNoSymbolComputations ELI_ARG((_TPPrNoSymbolComputations _currn));
extern void _VS1rSymbolComputationsExist ELI_ARG((_TPPrSymbolComputationsExist _currn));
extern void _VS2rSymbolComputationsExist ELI_ARG((_TPPrSymbolComputationsExist _currn));
extern void _VS3rSymbolComputationsExist ELI_ARG((_TPPrSymbolComputationsExist _currn));
extern void _VS4rSymbolComputationsExist ELI_ARG((_TPPrSymbolComputationsExist _currn));
extern void _VS5rSymbolComputationsExist ELI_ARG((_TPPrSymbolComputationsExist _currn));
extern void _VS6rSymbolComputationsExist ELI_ARG((_TPPrSymbolComputationsExist _currn));
extern void _VS7rSymbolComputationsExist ELI_ARG((_TPPrSymbolComputationsExist _currn));
extern void _VS1rSymbolComputationList2 ELI_ARG((_TPPrSymbolComputationList2 _currn));
extern void _VS2rSymbolComputationList2 ELI_ARG((_TPPrSymbolComputationList2 _currn));
extern void _VS3rSymbolComputationList2 ELI_ARG((_TPPrSymbolComputationList2 _currn));
extern void _VS4rSymbolComputationList2 ELI_ARG((_TPPrSymbolComputationList2 _currn));
extern void _VS5rSymbolComputationList2 ELI_ARG((_TPPrSymbolComputationList2 _currn));
extern void _VS6rSymbolComputationList2 ELI_ARG((_TPPrSymbolComputationList2 _currn));
extern void _VS7rSymbolComputationList2 ELI_ARG((_TPPrSymbolComputationList2 _currn));
#define _VS1rSymbolComputationListEmpty _VS0Empty

extern void _VS2rSymbolComputationListEmpty ELI_ARG((_TPPrSymbolComputationListEmpty _currn));
#define _VS3rSymbolComputationListEmpty _VS0Empty

extern void _VS4rSymbolComputationListEmpty ELI_ARG((_TPPrSymbolComputationListEmpty _currn));
extern void _VS5rSymbolComputationListEmpty ELI_ARG((_TPPrSymbolComputationListEmpty _currn));
extern void _VS6rSymbolComputationListEmpty ELI_ARG((_TPPrSymbolComputationListEmpty _currn));
extern void _VS7rSymbolComputationListEmpty ELI_ARG((_TPPrSymbolComputationListEmpty _currn));
extern void _VS1rSymbolComputationIsAssign ELI_ARG((_TPPrSymbolComputationIsAssign _currn));
extern void _VS2rSymbolComputationIsAssign ELI_ARG((_TPPrSymbolComputationIsAssign _currn));
extern void _VS3rSymbolComputationIsAssign ELI_ARG((_TPPrSymbolComputationIsAssign _currn));
extern void _VS4rSymbolComputationIsAssign ELI_ARG((_TPPrSymbolComputationIsAssign _currn));
extern void _VS5rSymbolComputationIsAssign ELI_ARG((_TPPrSymbolComputationIsAssign _currn));
extern void _VS6rSymbolComputationIsAssign ELI_ARG((_TPPrSymbolComputationIsAssign _currn));
extern void _VS7rSymbolComputationIsAssign ELI_ARG((_TPPrSymbolComputationIsAssign _currn));
extern void _VS1rSymbolComputationIsChainstart ELI_ARG((_TPPrSymbolComputationIsChainstart _currn));
extern void _VS2rSymbolComputationIsChainstart ELI_ARG((_TPPrSymbolComputationIsChainstart _currn));
extern void _VS3rSymbolComputationIsChainstart ELI_ARG((_TPPrSymbolComputationIsChainstart _currn));
extern void _VS4rSymbolComputationIsChainstart ELI_ARG((_TPPrSymbolComputationIsChainstart _currn));
extern void _VS5rSymbolComputationIsChainstart ELI_ARG((_TPPrSymbolComputationIsChainstart _currn));
extern void _VS6rSymbolComputationIsChainstart ELI_ARG((_TPPrSymbolComputationIsChainstart _currn));
extern void _VS7rSymbolComputationIsChainstart ELI_ARG((_TPPrSymbolComputationIsChainstart _currn));
extern void _VS1rSymbolComputationIsExpression ELI_ARG((_TPPrSymbolComputationIsExpression _currn));
extern void _VS2rSymbolComputationIsExpression ELI_ARG((_TPPrSymbolComputationIsExpression _currn));
extern void _VS3rSymbolComputationIsExpression ELI_ARG((_TPPrSymbolComputationIsExpression _currn));
extern void _VS4rSymbolComputationIsExpression ELI_ARG((_TPPrSymbolComputationIsExpression _currn));
extern void _VS5rSymbolComputationIsExpression ELI_ARG((_TPPrSymbolComputationIsExpression _currn));
extern void _VS6rSymbolComputationIsExpression ELI_ARG((_TPPrSymbolComputationIsExpression _currn));
extern void _VS7rSymbolComputationIsExpression ELI_ARG((_TPPrSymbolComputationIsExpression _currn));
extern void _VS1rSymbolComputationIsOrder ELI_ARG((_TPPrSymbolComputationIsOrder _currn));
extern void _VS2rSymbolComputationIsOrder ELI_ARG((_TPPrSymbolComputationIsOrder _currn));
extern void _VS3rSymbolComputationIsOrder ELI_ARG((_TPPrSymbolComputationIsOrder _currn));
extern void _VS4rSymbolComputationIsOrder ELI_ARG((_TPPrSymbolComputationIsOrder _currn));
extern void _VS5rSymbolComputationIsOrder ELI_ARG((_TPPrSymbolComputationIsOrder _currn));
extern void _VS6rSymbolComputationIsOrder ELI_ARG((_TPPrSymbolComputationIsOrder _currn));
extern void _VS7rSymbolComputationIsOrder ELI_ARG((_TPPrSymbolComputationIsOrder _currn));
extern void _VS1rSymbolChainStart ELI_ARG((_TPPrSymbolChainStart _currn));
extern void _VS2rSymbolChainStart ELI_ARG((_TPPrSymbolChainStart _currn));
extern void _VS3rSymbolChainStart ELI_ARG((_TPPrSymbolChainStart _currn));
extern void _VS4rSymbolChainStart ELI_ARG((_TPPrSymbolChainStart _currn));
extern void _VS5rSymbolChainStart ELI_ARG((_TPPrSymbolChainStart _currn));
extern void _VS6rSymbolChainStart ELI_ARG((_TPPrSymbolChainStart _currn));
extern void _VS7rSymbolChainStart ELI_ARG((_TPPrSymbolChainStart _currn));
extern void _VS8rSymbolChainStart ELI_ARG((_TPPrSymbolChainStart _currn));
#define _VS1rNoSymbolDependency _VS0Empty

#define _VS2rNoSymbolDependency _VS0Empty

#define _VS3rNoSymbolDependency _VS0Empty

#define _VS4rNoSymbolDependency _VS0Empty

extern void _VS5rNoSymbolDependency ELI_ARG((_TPPrNoSymbolDependency _currn));
extern void _VS6rNoSymbolDependency ELI_ARG((_TPPrNoSymbolDependency _currn));
extern void _VS1rSymbolDependency ELI_ARG((_TPPrSymbolDependency _currn));
extern void _VS2rSymbolDependency ELI_ARG((_TPPrSymbolDependency _currn));
extern void _VS3rSymbolDependency ELI_ARG((_TPPrSymbolDependency _currn));
extern void _VS4rSymbolDependency ELI_ARG((_TPPrSymbolDependency _currn));
extern void _VS5rSymbolDependency ELI_ARG((_TPPrSymbolDependency _currn));
extern void _VS6rSymbolDependency ELI_ARG((_TPPrSymbolDependency _currn));
#define _VS1rSymbolSymbolAttributeReferencesSingle _VS1rPTGDecl

#define _VS2rSymbolSymbolAttributeReferencesSingle _VS2rPTGDecl

#define _VS3rSymbolSymbolAttributeReferencesSingle _VS3rPTGDecl

#define _VS4rSymbolSymbolAttributeReferencesSingle _VS4rPTGDecl

extern void _VS5rSymbolSymbolAttributeReferencesSingle ELI_ARG((_TPPrSymbolSymbolAttributeReferencesSingle _currn));
extern void _VS6rSymbolSymbolAttributeReferencesSingle ELI_ARG((_TPPrSymbolSymbolAttributeReferencesSingle _currn));
#define _VS1rSymbolSymbolAttributeReferencesMulti _VS1rPTGDecl

#define _VS2rSymbolSymbolAttributeReferencesMulti _VS2rPTGDecl

#define _VS3rSymbolSymbolAttributeReferencesMulti _VS3rPTGDecl

#define _VS4rSymbolSymbolAttributeReferencesMulti _VS4rPTGDecl

extern void _VS5rSymbolSymbolAttributeReferencesMulti ELI_ARG((_TPPrSymbolSymbolAttributeReferencesMulti _currn));
extern void _VS6rSymbolSymbolAttributeReferencesMulti ELI_ARG((_TPPrSymbolSymbolAttributeReferencesMulti _currn));
extern void _VS1rSymbolLocalAttributeReferenceIsRemote ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsRemote _currn));
extern void _VS2rSymbolLocalAttributeReferenceIsRemote ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsRemote _currn));
extern void _VS3rSymbolLocalAttributeReferenceIsRemote ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsRemote _currn));
extern void _VS4rSymbolLocalAttributeReferenceIsRemote ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsRemote _currn));
extern void _VS5rSymbolLocalAttributeReferenceIsRemote ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsRemote _currn));
extern void _VS6rSymbolLocalAttributeReferenceIsRemote ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsRemote _currn));
extern void _VS7rSymbolLocalAttributeReferenceIsRemote ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsRemote _currn));
#define _VS1rSymbolLocalAttributeReferenceIsLocal _VS0Empty

#define _VS2rSymbolLocalAttributeReferenceIsLocal _VS0Empty

extern void _VS3rSymbolLocalAttributeReferenceIsLocal ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsLocal _currn));
#define _VS4rSymbolLocalAttributeReferenceIsLocal _VS0Empty

extern void _VS5rSymbolLocalAttributeReferenceIsLocal ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsLocal _currn));
extern void _VS6rSymbolLocalAttributeReferenceIsLocal ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsLocal _currn));
extern void _VS7rSymbolLocalAttributeReferenceIsLocal ELI_ARG((_TPPrSymbolLocalAttributeReferenceIsLocal _currn));
#define _VS1rSymbolLocalAttributeRefList2 _VS1rPTGDefList2

#define _VS2rSymbolLocalAttributeRefList2 _VS2rPTGDefList2

#define _VS3rSymbolLocalAttributeRefList2 _VS3rPTGDefList2

#define _VS4rSymbolLocalAttributeRefList2 _VS4rPTGDefList2

extern void _VS5rSymbolLocalAttributeRefList2 ELI_ARG((_TPPrSymbolLocalAttributeRefList2 _currn));
extern void _VS6rSymbolLocalAttributeRefList2 ELI_ARG((_TPPrSymbolLocalAttributeRefList2 _currn,D_typePtr* _AS0expected_type));
#define _VS1rSymbolLocalAttributeRefList1 _VS1rPTGDecl

#define _VS2rSymbolLocalAttributeRefList1 _VS2rPTGDecl

#define _VS3rSymbolLocalAttributeRefList1 _VS3rPTGDecl

#define _VS4rSymbolLocalAttributeRefList1 _VS4rPTGDecl

extern void _VS5rSymbolLocalAttributeRefList1 ELI_ARG((_TPPrSymbolLocalAttributeRefList1 _currn));
extern void _VS6rSymbolLocalAttributeRefList1 ELI_ARG((_TPPrSymbolLocalAttributeRefList1 _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolDefOcc ELI_ARG((_TPPrSymbolDefOcc _currn));
extern void _VS2rSymbolDefOcc ELI_ARG((_TPPrSymbolDefOcc _currn));
extern void _VS3rSymbolDefOcc ELI_ARG((_TPPrSymbolDefOcc _currn));
extern void _VS4rSymbolDefOcc ELI_ARG((_TPPrSymbolDefOcc _currn));
extern void _VS5rSymbolDefOcc ELI_ARG((_TPPrSymbolDefOcc _currn));
extern void _VS6rSymbolDefOcc ELI_ARG((_TPPrSymbolDefOcc _currn));
extern void _VS7rSymbolDefOcc ELI_ARG((_TPPrSymbolDefOcc _currn));
extern void _VS8rSymbolDefOcc ELI_ARG((_TPPrSymbolDefOcc _currn));
extern void _VS1rSymbolOrderedComputation ELI_ARG((_TPPrSymbolOrderedComputation _currn));
extern void _VS2rSymbolOrderedComputation ELI_ARG((_TPPrSymbolOrderedComputation _currn));
extern void _VS3rSymbolOrderedComputation ELI_ARG((_TPPrSymbolOrderedComputation _currn));
extern void _VS4rSymbolOrderedComputation ELI_ARG((_TPPrSymbolOrderedComputation _currn));
extern void _VS5rSymbolOrderedComputation ELI_ARG((_TPPrSymbolOrderedComputation _currn));
extern void _VS6rSymbolOrderedComputation ELI_ARG((_TPPrSymbolOrderedComputation _currn));
extern void _VS7rSymbolOrderedComputation ELI_ARG((_TPPrSymbolOrderedComputation _currn));
extern void _VS1rSymbolOrderedStatementList2 ELI_ARG((_TPPrSymbolOrderedStatementList2 _currn));
extern void _VS2rSymbolOrderedStatementList2 ELI_ARG((_TPPrSymbolOrderedStatementList2 _currn));
extern void _VS3rSymbolOrderedStatementList2 ELI_ARG((_TPPrSymbolOrderedStatementList2 _currn));
extern void _VS4rSymbolOrderedStatementList2 ELI_ARG((_TPPrSymbolOrderedStatementList2 _currn));
extern void _VS5rSymbolOrderedStatementList2 ELI_ARG((_TPPrSymbolOrderedStatementList2 _currn,D_SymbolComputations* _AS0_const250,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolOrderedStatementList2 ELI_ARG((_TPPrSymbolOrderedStatementList2 _currn,D_typePtr* _AS0expected_type,int* _AS0_const261,int* _AS0_const253,int* _AS0_const251,D_SymbolComputations* _AS0_const250,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolOrderedStatementList2 ELI_ARG((_TPPrSymbolOrderedStatementList2 _currn,PTGNodes* _AS0lido_codes,D_typePtr* _AS0expected_type,int* _AS0_const261,int* _AS0_const253,int* _AS0_const251,D_SymbolComputations* _AS0_const250,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolOrderedStatementList1 ELI_ARG((_TPPrSymbolOrderedStatementList1 _currn));
extern void _VS2rSymbolOrderedStatementList1 ELI_ARG((_TPPrSymbolOrderedStatementList1 _currn));
extern void _VS3rSymbolOrderedStatementList1 ELI_ARG((_TPPrSymbolOrderedStatementList1 _currn));
extern void _VS4rSymbolOrderedStatementList1 ELI_ARG((_TPPrSymbolOrderedStatementList1 _currn));
extern void _VS5rSymbolOrderedStatementList1 ELI_ARG((_TPPrSymbolOrderedStatementList1 _currn,D_SymbolComputations* _AS0_const250,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolOrderedStatementList1 ELI_ARG((_TPPrSymbolOrderedStatementList1 _currn,D_typePtr* _AS0expected_type,int* _AS0_const261,int* _AS0_const253,int* _AS0_const251,D_SymbolComputations* _AS0_const250,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolOrderedStatementList1 ELI_ARG((_TPPrSymbolOrderedStatementList1 _currn,PTGNodes* _AS0lido_codes,D_typePtr* _AS0expected_type,int* _AS0_const261,int* _AS0_const253,int* _AS0_const251,D_SymbolComputations* _AS0_const250,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolOrderedStatementIsExpression ELI_ARG((_TPPrSymbolOrderedStatementIsExpression _currn));
extern void _VS2rSymbolOrderedStatementIsExpression ELI_ARG((_TPPrSymbolOrderedStatementIsExpression _currn));
extern void _VS3rSymbolOrderedStatementIsExpression ELI_ARG((_TPPrSymbolOrderedStatementIsExpression _currn));
extern void _VS4rSymbolOrderedStatementIsExpression ELI_ARG((_TPPrSymbolOrderedStatementIsExpression _currn));
extern void _VS5rSymbolOrderedStatementIsExpression ELI_ARG((_TPPrSymbolOrderedStatementIsExpression _currn));
extern void _VS6rSymbolOrderedStatementIsExpression ELI_ARG((_TPPrSymbolOrderedStatementIsExpression _currn));
extern void _VS7rSymbolOrderedStatementIsExpression ELI_ARG((_TPPrSymbolOrderedStatementIsExpression _currn,D_RHSKind* _AS0lido_rhs,PTGNodes* _AS0last_attrs,D_SymbolAttributes* _AS0last_vattrsIn));
extern void _VS1rSymbolOrderedStatementIsReturn ELI_ARG((_TPPrSymbolOrderedStatementIsReturn _currn));
extern void _VS2rSymbolOrderedStatementIsReturn ELI_ARG((_TPPrSymbolOrderedStatementIsReturn _currn));
extern void _VS3rSymbolOrderedStatementIsReturn ELI_ARG((_TPPrSymbolOrderedStatementIsReturn _currn));
extern void _VS4rSymbolOrderedStatementIsReturn ELI_ARG((_TPPrSymbolOrderedStatementIsReturn _currn));
extern void _VS5rSymbolOrderedStatementIsReturn ELI_ARG((_TPPrSymbolOrderedStatementIsReturn _currn));
extern void _VS6rSymbolOrderedStatementIsReturn ELI_ARG((_TPPrSymbolOrderedStatementIsReturn _currn));
extern void _VS7rSymbolOrderedStatementIsReturn ELI_ARG((_TPPrSymbolOrderedStatementIsReturn _currn,D_RHSKind* _AS0lido_rhs,PTGNodes* _AS0last_attrs,D_SymbolAttributes* _AS0last_vattrsIn));
extern void _VS1rSymbolOrderedStatementIsAssign ELI_ARG((_TPPrSymbolOrderedStatementIsAssign _currn));
extern void _VS2rSymbolOrderedStatementIsAssign ELI_ARG((_TPPrSymbolOrderedStatementIsAssign _currn));
extern void _VS3rSymbolOrderedStatementIsAssign ELI_ARG((_TPPrSymbolOrderedStatementIsAssign _currn));
extern void _VS4rSymbolOrderedStatementIsAssign ELI_ARG((_TPPrSymbolOrderedStatementIsAssign _currn));
extern void _VS5rSymbolOrderedStatementIsAssign ELI_ARG((_TPPrSymbolOrderedStatementIsAssign _currn));
extern void _VS6rSymbolOrderedStatementIsAssign ELI_ARG((_TPPrSymbolOrderedStatementIsAssign _currn));
extern void _VS7rSymbolOrderedStatementIsAssign ELI_ARG((_TPPrSymbolOrderedStatementIsAssign _currn,D_RHSKind* _AS0lido_rhs,PTGNodes* _AS0last_attrs,D_SymbolAttributes* _AS0last_vattrsIn));
extern void _VS1rSymbolRHSExpressionIsCompute ELI_ARG((_TPPrSymbolRHSExpressionIsCompute _currn));
extern void _VS2rSymbolRHSExpressionIsCompute ELI_ARG((_TPPrSymbolRHSExpressionIsCompute _currn));
extern void _VS3rSymbolRHSExpressionIsCompute ELI_ARG((_TPPrSymbolRHSExpressionIsCompute _currn));
extern void _VS4rSymbolRHSExpressionIsCompute ELI_ARG((_TPPrSymbolRHSExpressionIsCompute _currn));
extern void _VS5rSymbolRHSExpressionIsCompute ELI_ARG((_TPPrSymbolRHSExpressionIsCompute _currn));
extern void _VS6rSymbolRHSExpressionIsCompute ELI_ARG((_TPPrSymbolRHSExpressionIsCompute _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolRHSExpressionIsCompute ELI_ARG((_TPPrSymbolRHSExpressionIsCompute _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolRHSExpressionIsGuards ELI_ARG((_TPPrSymbolRHSExpressionIsGuards _currn));
extern void _VS2rSymbolRHSExpressionIsGuards ELI_ARG((_TPPrSymbolRHSExpressionIsGuards _currn));
extern void _VS3rSymbolRHSExpressionIsGuards ELI_ARG((_TPPrSymbolRHSExpressionIsGuards _currn));
extern void _VS4rSymbolRHSExpressionIsGuards ELI_ARG((_TPPrSymbolRHSExpressionIsGuards _currn));
extern void _VS5rSymbolRHSExpressionIsGuards ELI_ARG((_TPPrSymbolRHSExpressionIsGuards _currn));
extern void _VS6rSymbolRHSExpressionIsGuards ELI_ARG((_TPPrSymbolRHSExpressionIsGuards _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolRHSExpressionIsGuards ELI_ARG((_TPPrSymbolRHSExpressionIsGuards _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolGuardExpressionList2 ELI_ARG((_TPPrSymbolGuardExpressionList2 _currn));
extern void _VS2rSymbolGuardExpressionList2 ELI_ARG((_TPPrSymbolGuardExpressionList2 _currn));
extern void _VS3rSymbolGuardExpressionList2 ELI_ARG((_TPPrSymbolGuardExpressionList2 _currn));
extern void _VS4rSymbolGuardExpressionList2 ELI_ARG((_TPPrSymbolGuardExpressionList2 _currn));
extern void _VS5rSymbolGuardExpressionList2 ELI_ARG((_TPPrSymbolGuardExpressionList2 _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolGuardExpressionList2 ELI_ARG((_TPPrSymbolGuardExpressionList2 _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolGuardExpressionList2 ELI_ARG((_TPPrSymbolGuardExpressionList2 _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0gconds,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS8rSymbolGuardExpressionList2 ELI_ARG((_TPPrSymbolGuardExpressionList2 _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0gconds,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS1rSymbolGuardExpressionList1 ELI_ARG((_TPPrSymbolGuardExpressionList1 _currn));
extern void _VS2rSymbolGuardExpressionList1 ELI_ARG((_TPPrSymbolGuardExpressionList1 _currn));
extern void _VS3rSymbolGuardExpressionList1 ELI_ARG((_TPPrSymbolGuardExpressionList1 _currn));
extern void _VS4rSymbolGuardExpressionList1 ELI_ARG((_TPPrSymbolGuardExpressionList1 _currn));
extern void _VS5rSymbolGuardExpressionList1 ELI_ARG((_TPPrSymbolGuardExpressionList1 _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolGuardExpressionList1 ELI_ARG((_TPPrSymbolGuardExpressionList1 _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolGuardExpressionList1 ELI_ARG((_TPPrSymbolGuardExpressionList1 _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0gconds,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS8rSymbolGuardExpressionList1 ELI_ARG((_TPPrSymbolGuardExpressionList1 _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0gconds,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS1rSymbolGuardExpressionIsGuard ELI_ARG((_TPPrSymbolGuardExpressionIsGuard _currn));
extern void _VS2rSymbolGuardExpressionIsGuard ELI_ARG((_TPPrSymbolGuardExpressionIsGuard _currn));
extern void _VS3rSymbolGuardExpressionIsGuard ELI_ARG((_TPPrSymbolGuardExpressionIsGuard _currn));
extern void _VS4rSymbolGuardExpressionIsGuard ELI_ARG((_TPPrSymbolGuardExpressionIsGuard _currn));
extern void _VS5rSymbolGuardExpressionIsGuard ELI_ARG((_TPPrSymbolGuardExpressionIsGuard _currn,D_SymbolExpressions* _AS0sguardsIn));
extern void _VS6rSymbolGuardExpressionIsGuard ELI_ARG((_TPPrSymbolGuardExpressionIsGuard _currn,D_SymbolExpressions* _AS0sguardsIn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolGuardExpressionIsGuard ELI_ARG((_TPPrSymbolGuardExpressionIsGuard _currn,D_SymbolExpressions* _AS0sguardsIn,D_typePtr* _AS0expected_type,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol));
extern void _VS8rSymbolGuardExpressionIsGuard ELI_ARG((_TPPrSymbolGuardExpressionIsGuard _currn,D_SymbolExpressions* _AS0sguardsIn,D_typePtr* _AS0expected_type,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol,D_SymbolAttributes* _AS0_const252,D_OrderStats* _AS0_const246,int* _AS0_const245,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolGuardExpressionIsExpression ELI_ARG((_TPPrSymbolGuardExpressionIsExpression _currn));
extern void _VS2rSymbolGuardExpressionIsExpression ELI_ARG((_TPPrSymbolGuardExpressionIsExpression _currn));
extern void _VS3rSymbolGuardExpressionIsExpression ELI_ARG((_TPPrSymbolGuardExpressionIsExpression _currn));
extern void _VS4rSymbolGuardExpressionIsExpression ELI_ARG((_TPPrSymbolGuardExpressionIsExpression _currn));
extern void _VS5rSymbolGuardExpressionIsExpression ELI_ARG((_TPPrSymbolGuardExpressionIsExpression _currn,D_SymbolExpressions* _AS0sguardsIn));
extern void _VS6rSymbolGuardExpressionIsExpression ELI_ARG((_TPPrSymbolGuardExpressionIsExpression _currn,D_SymbolExpressions* _AS0sguardsIn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolGuardExpressionIsExpression ELI_ARG((_TPPrSymbolGuardExpressionIsExpression _currn,D_SymbolExpressions* _AS0sguardsIn,D_typePtr* _AS0expected_type,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol));
extern void _VS8rSymbolGuardExpressionIsExpression ELI_ARG((_TPPrSymbolGuardExpressionIsExpression _currn,D_SymbolExpressions* _AS0sguardsIn,D_typePtr* _AS0expected_type,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol,D_SymbolAttributes* _AS0_const252,D_OrderStats* _AS0_const246,int* _AS0_const245,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolGuardExpressionIsDefault ELI_ARG((_TPPrSymbolGuardExpressionIsDefault _currn));
extern void _VS2rSymbolGuardExpressionIsDefault ELI_ARG((_TPPrSymbolGuardExpressionIsDefault _currn));
extern void _VS3rSymbolGuardExpressionIsDefault ELI_ARG((_TPPrSymbolGuardExpressionIsDefault _currn));
extern void _VS4rSymbolGuardExpressionIsDefault ELI_ARG((_TPPrSymbolGuardExpressionIsDefault _currn));
extern void _VS5rSymbolGuardExpressionIsDefault ELI_ARG((_TPPrSymbolGuardExpressionIsDefault _currn,D_SymbolExpressions* _AS0sguardsIn));
extern void _VS6rSymbolGuardExpressionIsDefault ELI_ARG((_TPPrSymbolGuardExpressionIsDefault _currn,D_SymbolExpressions* _AS0sguardsIn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolGuardExpressionIsDefault ELI_ARG((_TPPrSymbolGuardExpressionIsDefault _currn,D_SymbolExpressions* _AS0sguardsIn,D_typePtr* _AS0expected_type,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol));
extern void _VS8rSymbolGuardExpressionIsDefault ELI_ARG((_TPPrSymbolGuardExpressionIsDefault _currn,D_SymbolExpressions* _AS0sguardsIn,D_typePtr* _AS0expected_type,D_LocalAttributeClass* _AS0remote_direction_context,StringTableKey* _AS0remote_inh_symbol,D_SymbolAttributes* _AS0_const252,D_OrderStats* _AS0_const246,int* _AS0_const245,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionChain ELI_ARG((_TPPrSymbolExpressionChain _currn));
extern void _VS2rSymbolExpressionChain ELI_ARG((_TPPrSymbolExpressionChain _currn));
extern void _VS3rSymbolExpressionChain ELI_ARG((_TPPrSymbolExpressionChain _currn));
extern void _VS4rSymbolExpressionChain ELI_ARG((_TPPrSymbolExpressionChain _currn));
extern void _VS5rSymbolExpressionChain ELI_ARG((_TPPrSymbolExpressionChain _currn,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionChain ELI_ARG((_TPPrSymbolExpressionChain _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolExpressionChain ELI_ARG((_TPPrSymbolExpressionChain _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS1rSymbolExpressionIsWhen ELI_ARG((_TPPrSymbolExpressionIsWhen _currn));
extern void _VS2rSymbolExpressionIsWhen ELI_ARG((_TPPrSymbolExpressionIsWhen _currn));
extern void _VS3rSymbolExpressionIsWhen ELI_ARG((_TPPrSymbolExpressionIsWhen _currn));
extern void _VS4rSymbolExpressionIsWhen ELI_ARG((_TPPrSymbolExpressionIsWhen _currn));
extern void _VS5rSymbolExpressionIsWhen ELI_ARG((_TPPrSymbolExpressionIsWhen _currn,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionIsWhen ELI_ARG((_TPPrSymbolExpressionIsWhen _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolExpressionIsWhen ELI_ARG((_TPPrSymbolExpressionIsWhen _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS1rSymbolExpressionIsError ELI_ARG((_TPPrSymbolExpressionIsError _currn));
extern void _VS2rSymbolExpressionIsError ELI_ARG((_TPPrSymbolExpressionIsError _currn));
extern void _VS3rSymbolExpressionIsError ELI_ARG((_TPPrSymbolExpressionIsError _currn));
extern void _VS4rSymbolExpressionIsError ELI_ARG((_TPPrSymbolExpressionIsError _currn));
extern void _VS5rSymbolExpressionIsError ELI_ARG((_TPPrSymbolExpressionIsError _currn,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionIsError ELI_ARG((_TPPrSymbolExpressionIsError _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolExpressionIsError ELI_ARG((_TPPrSymbolExpressionIsError _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS1rSymbolExpressionIsIf ELI_ARG((_TPPrSymbolExpressionIsIf _currn));
extern void _VS2rSymbolExpressionIsIf ELI_ARG((_TPPrSymbolExpressionIsIf _currn));
extern void _VS3rSymbolExpressionIsIf ELI_ARG((_TPPrSymbolExpressionIsIf _currn));
extern void _VS4rSymbolExpressionIsIf ELI_ARG((_TPPrSymbolExpressionIsIf _currn));
extern void _VS5rSymbolExpressionIsIf ELI_ARG((_TPPrSymbolExpressionIsIf _currn,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionIsIf ELI_ARG((_TPPrSymbolExpressionIsIf _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolExpressionIsIf ELI_ARG((_TPPrSymbolExpressionIsIf _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS1rSymbolExpressionIsLet ELI_ARG((_TPPrSymbolExpressionIsLet _currn));
extern void _VS2rSymbolExpressionIsLet ELI_ARG((_TPPrSymbolExpressionIsLet _currn));
extern void _VS3rSymbolExpressionIsLet ELI_ARG((_TPPrSymbolExpressionIsLet _currn));
extern void _VS4rSymbolExpressionIsLet ELI_ARG((_TPPrSymbolExpressionIsLet _currn));
extern void _VS5rSymbolExpressionIsLet ELI_ARG((_TPPrSymbolExpressionIsLet _currn,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionIsLet ELI_ARG((_TPPrSymbolExpressionIsLet _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolExpressionIsLet ELI_ARG((_TPPrSymbolExpressionIsLet _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS1rSymbolExpressionIsLambda ELI_ARG((_TPPrSymbolExpressionIsLambda _currn));
extern void _VS2rSymbolExpressionIsLambda ELI_ARG((_TPPrSymbolExpressionIsLambda _currn));
extern void _VS3rSymbolExpressionIsLambda ELI_ARG((_TPPrSymbolExpressionIsLambda _currn));
extern void _VS4rSymbolExpressionIsLambda ELI_ARG((_TPPrSymbolExpressionIsLambda _currn));
extern void _VS5rSymbolExpressionIsLambda ELI_ARG((_TPPrSymbolExpressionIsLambda _currn,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionIsLambda ELI_ARG((_TPPrSymbolExpressionIsLambda _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolExpressionIsLambda ELI_ARG((_TPPrSymbolExpressionIsLambda _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS1rSymbolExpressionIsOrder ELI_ARG((_TPPrSymbolExpressionIsOrder _currn));
extern void _VS2rSymbolExpressionIsOrder ELI_ARG((_TPPrSymbolExpressionIsOrder _currn));
extern void _VS3rSymbolExpressionIsOrder ELI_ARG((_TPPrSymbolExpressionIsOrder _currn));
extern void _VS4rSymbolExpressionIsOrder ELI_ARG((_TPPrSymbolExpressionIsOrder _currn));
extern void _VS5rSymbolExpressionIsOrder ELI_ARG((_TPPrSymbolExpressionIsOrder _currn,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionIsOrder ELI_ARG((_TPPrSymbolExpressionIsOrder _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolExpressionIsOrder ELI_ARG((_TPPrSymbolExpressionIsOrder _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS1rSymbolLambda ELI_ARG((_TPPrSymbolLambda _currn));
extern void _VS2rSymbolLambda ELI_ARG((_TPPrSymbolLambda _currn));
extern void _VS3rSymbolLambda ELI_ARG((_TPPrSymbolLambda _currn));
extern void _VS4rSymbolLambda ELI_ARG((_TPPrSymbolLambda _currn));
extern void _VS5rSymbolLambda ELI_ARG((_TPPrSymbolLambda _currn,Environment* _AS0rule_var_chn_pre));
extern void _VS6rSymbolLambda ELI_ARG((_TPPrSymbolLambda _currn,D_typePtr* _AS0expected_type,Environment* _AS0rule_var_chn_pre));
extern void _VS7rSymbolLambda ELI_ARG((_TPPrSymbolLambda _currn,D_typePtr* _AS0expected_type,Environment* _AS0rule_var_chn_pre,Environment* _AS0lng_generated_defs_chn_pre));
extern void _VS1rSymbolLambdaVarDefList2 ELI_ARG((_TPPrSymbolLambdaVarDefList2 _currn));
extern void _VS2rSymbolLambdaVarDefList2 ELI_ARG((_TPPrSymbolLambdaVarDefList2 _currn));
extern void _VS3rSymbolLambdaVarDefList2 ELI_ARG((_TPPrSymbolLambdaVarDefList2 _currn));
extern void _VS4rSymbolLambdaVarDefList2 ELI_ARG((_TPPrSymbolLambdaVarDefList2 _currn));
extern void _VS5rSymbolLambdaVarDefList2 ELI_ARG((_TPPrSymbolLambdaVarDefList2 _currn,D_types* _AS0_const255,StringTableKeyList* _AS0_const254));
extern void _VS6rSymbolLambdaVarDefList2 ELI_ARG((_TPPrSymbolLambdaVarDefList2 _currn,D_types* _AS0_const255,StringTableKeyList* _AS0_const254,StringTableKey* _AS0_const10));
extern void _VS1rSymbolLambdaVarDefList1 ELI_ARG((_TPPrSymbolLambdaVarDefList1 _currn));
extern void _VS2rSymbolLambdaVarDefList1 ELI_ARG((_TPPrSymbolLambdaVarDefList1 _currn));
extern void _VS3rSymbolLambdaVarDefList1 ELI_ARG((_TPPrSymbolLambdaVarDefList1 _currn));
extern void _VS4rSymbolLambdaVarDefList1 ELI_ARG((_TPPrSymbolLambdaVarDefList1 _currn));
extern void _VS5rSymbolLambdaVarDefList1 ELI_ARG((_TPPrSymbolLambdaVarDefList1 _currn,D_types* _AS0_const255,StringTableKeyList* _AS0_const254));
extern void _VS6rSymbolLambdaVarDefList1 ELI_ARG((_TPPrSymbolLambdaVarDefList1 _currn,D_types* _AS0_const255,StringTableKeyList* _AS0_const254,StringTableKey* _AS0_const10));
extern void _VS1rSymbolLambdaVarDef ELI_ARG((_TPPrSymbolLambdaVarDef _currn));
#define _VS2rSymbolLambdaVarDef _VS2rPTGCall

extern void _VS3rSymbolLambdaVarDef ELI_ARG((_TPPrSymbolLambdaVarDef _currn));
#define _VS4rSymbolLambdaVarDef _VS5rPTGCall

extern void _VS5rSymbolLambdaVarDef ELI_ARG((_TPPrSymbolLambdaVarDef _currn));
extern void _VS6rSymbolLambdaVarDef ELI_ARG((_TPPrSymbolLambdaVarDef _currn));
extern void _VS1rSymbolVariableDefId ELI_ARG((_TPPrSymbolVariableDefId _currn));
extern void _VS2rSymbolVariableDefId ELI_ARG((_TPPrSymbolVariableDefId _currn));
extern void _VS3rSymbolVariableDefId ELI_ARG((_TPPrSymbolVariableDefId _currn));
extern void _VS1rSymbolExpressionLet ELI_ARG((_TPPrSymbolExpressionLet _currn));
extern void _VS2rSymbolExpressionLet ELI_ARG((_TPPrSymbolExpressionLet _currn));
extern void _VS3rSymbolExpressionLet ELI_ARG((_TPPrSymbolExpressionLet _currn));
extern void _VS4rSymbolExpressionLet ELI_ARG((_TPPrSymbolExpressionLet _currn));
extern void _VS5rSymbolExpressionLet ELI_ARG((_TPPrSymbolExpressionLet _currn));
extern void _VS6rSymbolExpressionLet ELI_ARG((_TPPrSymbolExpressionLet _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionLet ELI_ARG((_TPPrSymbolExpressionLet _currn,D_typePtr* _AS0expected_type));
#define _VS1rSymbolLetVarDefList2 _VS1rPTGDefList2

#define _VS2rSymbolLetVarDefList2 _VS2rPTGDefList2

#define _VS3rSymbolLetVarDefList2 _VS3rPTGDefList2

#define _VS4rSymbolLetVarDefList2 _VS4rPTGDefList2

extern void _VS5rSymbolLetVarDefList2 ELI_ARG((_TPPrSymbolLetVarDefList2 _currn,D_SymbolLetVarDefs* _AS0_const260,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolLetVarDefList2 ELI_ARG((_TPPrSymbolLetVarDefList2 _currn,int* _AS0_const261,D_SymbolLetVarDefs* _AS0_const260,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolLetVarDefList2 ELI_ARG((_TPPrSymbolLetVarDefList2 _currn,int* _AS0_const261,D_SymbolLetVarDefs* _AS0_const260,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
#define _VS1rSymbolLetVarDefList1 _VS1rPTGDecl

#define _VS2rSymbolLetVarDefList1 _VS2rPTGDecl

#define _VS3rSymbolLetVarDefList1 _VS3rPTGDecl

#define _VS4rSymbolLetVarDefList1 _VS4rPTGDecl

extern void _VS5rSymbolLetVarDefList1 ELI_ARG((_TPPrSymbolLetVarDefList1 _currn,D_SymbolLetVarDefs* _AS0_const260,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolLetVarDefList1 ELI_ARG((_TPPrSymbolLetVarDefList1 _currn,int* _AS0_const261,D_SymbolLetVarDefs* _AS0_const260,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolLetVarDefList1 ELI_ARG((_TPPrSymbolLetVarDefList1 _currn,int* _AS0_const261,D_SymbolLetVarDefs* _AS0_const260,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolLetVarDef ELI_ARG((_TPPrSymbolLetVarDef _currn));
extern void _VS2rSymbolLetVarDef ELI_ARG((_TPPrSymbolLetVarDef _currn));
extern void _VS3rSymbolLetVarDef ELI_ARG((_TPPrSymbolLetVarDef _currn));
extern void _VS4rSymbolLetVarDef ELI_ARG((_TPPrSymbolLetVarDef _currn));
extern void _VS5rSymbolLetVarDef ELI_ARG((_TPPrSymbolLetVarDef _currn));
extern void _VS6rSymbolLetVarDef ELI_ARG((_TPPrSymbolLetVarDef _currn));
extern void _VS7rSymbolLetVarDef ELI_ARG((_TPPrSymbolLetVarDef _currn));
extern void _VS1rSymbolExpressionIf ELI_ARG((_TPPrSymbolExpressionIf _currn));
extern void _VS2rSymbolExpressionIf ELI_ARG((_TPPrSymbolExpressionIf _currn));
extern void _VS3rSymbolExpressionIf ELI_ARG((_TPPrSymbolExpressionIf _currn));
extern void _VS4rSymbolExpressionIf ELI_ARG((_TPPrSymbolExpressionIf _currn));
extern void _VS5rSymbolExpressionIf ELI_ARG((_TPPrSymbolExpressionIf _currn));
extern void _VS6rSymbolExpressionIf ELI_ARG((_TPPrSymbolExpressionIf _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionIf ELI_ARG((_TPPrSymbolExpressionIf _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionError ELI_ARG((_TPPrSymbolExpressionError _currn));
extern void _VS2rSymbolExpressionError ELI_ARG((_TPPrSymbolExpressionError _currn));
extern void _VS3rSymbolExpressionError ELI_ARG((_TPPrSymbolExpressionError _currn));
extern void _VS4rSymbolExpressionError ELI_ARG((_TPPrSymbolExpressionError _currn));
extern void _VS5rSymbolExpressionError ELI_ARG((_TPPrSymbolExpressionError _currn));
extern void _VS6rSymbolExpressionError ELI_ARG((_TPPrSymbolExpressionError _currn));
extern void _VS7rSymbolExpressionError ELI_ARG((_TPPrSymbolExpressionError _currn));
extern void _VS1rSymbolExpressionWhenCond ELI_ARG((_TPPrSymbolExpressionWhenCond _currn));
extern void _VS2rSymbolExpressionWhenCond ELI_ARG((_TPPrSymbolExpressionWhenCond _currn));
extern void _VS3rSymbolExpressionWhenCond ELI_ARG((_TPPrSymbolExpressionWhenCond _currn));
extern void _VS4rSymbolExpressionWhenCond ELI_ARG((_TPPrSymbolExpressionWhenCond _currn));
extern void _VS5rSymbolExpressionWhenCond ELI_ARG((_TPPrSymbolExpressionWhenCond _currn));
extern void _VS6rSymbolExpressionWhenCond ELI_ARG((_TPPrSymbolExpressionWhenCond _currn));
extern void _VS7rSymbolExpressionWhenCond ELI_ARG((_TPPrSymbolExpressionWhenCond _currn));
extern void _VS1rSymbolExpressionWhen ELI_ARG((_TPPrSymbolExpressionWhen _currn));
extern void _VS2rSymbolExpressionWhen ELI_ARG((_TPPrSymbolExpressionWhen _currn));
extern void _VS3rSymbolExpressionWhen ELI_ARG((_TPPrSymbolExpressionWhen _currn));
extern void _VS4rSymbolExpressionWhen ELI_ARG((_TPPrSymbolExpressionWhen _currn));
extern void _VS5rSymbolExpressionWhen ELI_ARG((_TPPrSymbolExpressionWhen _currn));
extern void _VS6rSymbolExpressionWhen ELI_ARG((_TPPrSymbolExpressionWhen _currn));
extern void _VS7rSymbolExpressionWhen ELI_ARG((_TPPrSymbolExpressionWhen _currn));
extern void _VS1rSymbolExpressionIsBinary ELI_ARG((_TPPrSymbolExpressionIsBinary _currn));
extern void _VS2rSymbolExpressionIsBinary ELI_ARG((_TPPrSymbolExpressionIsBinary _currn));
extern void _VS3rSymbolExpressionIsBinary ELI_ARG((_TPPrSymbolExpressionIsBinary _currn));
extern void _VS4rSymbolExpressionIsBinary ELI_ARG((_TPPrSymbolExpressionIsBinary _currn));
extern void _VS5rSymbolExpressionIsBinary ELI_ARG((_TPPrSymbolExpressionIsBinary _currn,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionIsBinary ELI_ARG((_TPPrSymbolExpressionIsBinary _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolExpressionIsBinary ELI_ARG((_TPPrSymbolExpressionIsBinary _currn,int* _AS0index_num,int* _AS0_const261,int* _AS0_const253,D_SymbolAttributes* _AS0_const248));
extern void _VS1rSymbolExpressionBinaryChain ELI_ARG((_TPPrSymbolExpressionBinaryChain _currn));
extern void _VS2rSymbolExpressionBinaryChain ELI_ARG((_TPPrSymbolExpressionBinaryChain _currn));
extern void _VS3rSymbolExpressionBinaryChain ELI_ARG((_TPPrSymbolExpressionBinaryChain _currn));
extern void _VS4rSymbolExpressionBinaryChain ELI_ARG((_TPPrSymbolExpressionBinaryChain _currn));
extern void _VS5rSymbolExpressionBinaryChain ELI_ARG((_TPPrSymbolExpressionBinaryChain _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryChain ELI_ARG((_TPPrSymbolExpressionBinaryChain _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryChain ELI_ARG((_TPPrSymbolExpressionBinaryChain _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryOR ELI_ARG((_TPPrSymbolExpressionBinaryOR _currn));
extern void _VS2rSymbolExpressionBinaryOR ELI_ARG((_TPPrSymbolExpressionBinaryOR _currn));
extern void _VS3rSymbolExpressionBinaryOR ELI_ARG((_TPPrSymbolExpressionBinaryOR _currn));
extern void _VS4rSymbolExpressionBinaryOR ELI_ARG((_TPPrSymbolExpressionBinaryOR _currn));
extern void _VS5rSymbolExpressionBinaryOR ELI_ARG((_TPPrSymbolExpressionBinaryOR _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryOR ELI_ARG((_TPPrSymbolExpressionBinaryOR _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryOR ELI_ARG((_TPPrSymbolExpressionBinaryOR _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryAND ELI_ARG((_TPPrSymbolExpressionBinaryAND _currn));
extern void _VS2rSymbolExpressionBinaryAND ELI_ARG((_TPPrSymbolExpressionBinaryAND _currn));
extern void _VS3rSymbolExpressionBinaryAND ELI_ARG((_TPPrSymbolExpressionBinaryAND _currn));
extern void _VS4rSymbolExpressionBinaryAND ELI_ARG((_TPPrSymbolExpressionBinaryAND _currn));
extern void _VS5rSymbolExpressionBinaryAND ELI_ARG((_TPPrSymbolExpressionBinaryAND _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryAND ELI_ARG((_TPPrSymbolExpressionBinaryAND _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryAND ELI_ARG((_TPPrSymbolExpressionBinaryAND _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryEQ ELI_ARG((_TPPrSymbolExpressionBinaryEQ _currn));
extern void _VS2rSymbolExpressionBinaryEQ ELI_ARG((_TPPrSymbolExpressionBinaryEQ _currn));
extern void _VS3rSymbolExpressionBinaryEQ ELI_ARG((_TPPrSymbolExpressionBinaryEQ _currn));
extern void _VS4rSymbolExpressionBinaryEQ ELI_ARG((_TPPrSymbolExpressionBinaryEQ _currn));
extern void _VS5rSymbolExpressionBinaryEQ ELI_ARG((_TPPrSymbolExpressionBinaryEQ _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryEQ ELI_ARG((_TPPrSymbolExpressionBinaryEQ _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryEQ ELI_ARG((_TPPrSymbolExpressionBinaryEQ _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryNE ELI_ARG((_TPPrSymbolExpressionBinaryNE _currn));
extern void _VS2rSymbolExpressionBinaryNE ELI_ARG((_TPPrSymbolExpressionBinaryNE _currn));
extern void _VS3rSymbolExpressionBinaryNE ELI_ARG((_TPPrSymbolExpressionBinaryNE _currn));
extern void _VS4rSymbolExpressionBinaryNE ELI_ARG((_TPPrSymbolExpressionBinaryNE _currn));
extern void _VS5rSymbolExpressionBinaryNE ELI_ARG((_TPPrSymbolExpressionBinaryNE _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryNE ELI_ARG((_TPPrSymbolExpressionBinaryNE _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryNE ELI_ARG((_TPPrSymbolExpressionBinaryNE _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryLT ELI_ARG((_TPPrSymbolExpressionBinaryLT _currn));
extern void _VS2rSymbolExpressionBinaryLT ELI_ARG((_TPPrSymbolExpressionBinaryLT _currn));
extern void _VS3rSymbolExpressionBinaryLT ELI_ARG((_TPPrSymbolExpressionBinaryLT _currn));
extern void _VS4rSymbolExpressionBinaryLT ELI_ARG((_TPPrSymbolExpressionBinaryLT _currn));
extern void _VS5rSymbolExpressionBinaryLT ELI_ARG((_TPPrSymbolExpressionBinaryLT _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryLT ELI_ARG((_TPPrSymbolExpressionBinaryLT _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryLT ELI_ARG((_TPPrSymbolExpressionBinaryLT _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryGT ELI_ARG((_TPPrSymbolExpressionBinaryGT _currn));
extern void _VS2rSymbolExpressionBinaryGT ELI_ARG((_TPPrSymbolExpressionBinaryGT _currn));
extern void _VS3rSymbolExpressionBinaryGT ELI_ARG((_TPPrSymbolExpressionBinaryGT _currn));
extern void _VS4rSymbolExpressionBinaryGT ELI_ARG((_TPPrSymbolExpressionBinaryGT _currn));
extern void _VS5rSymbolExpressionBinaryGT ELI_ARG((_TPPrSymbolExpressionBinaryGT _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryGT ELI_ARG((_TPPrSymbolExpressionBinaryGT _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryGT ELI_ARG((_TPPrSymbolExpressionBinaryGT _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryLE ELI_ARG((_TPPrSymbolExpressionBinaryLE _currn));
extern void _VS2rSymbolExpressionBinaryLE ELI_ARG((_TPPrSymbolExpressionBinaryLE _currn));
extern void _VS3rSymbolExpressionBinaryLE ELI_ARG((_TPPrSymbolExpressionBinaryLE _currn));
extern void _VS4rSymbolExpressionBinaryLE ELI_ARG((_TPPrSymbolExpressionBinaryLE _currn));
extern void _VS5rSymbolExpressionBinaryLE ELI_ARG((_TPPrSymbolExpressionBinaryLE _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryLE ELI_ARG((_TPPrSymbolExpressionBinaryLE _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryLE ELI_ARG((_TPPrSymbolExpressionBinaryLE _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryGE ELI_ARG((_TPPrSymbolExpressionBinaryGE _currn));
extern void _VS2rSymbolExpressionBinaryGE ELI_ARG((_TPPrSymbolExpressionBinaryGE _currn));
extern void _VS3rSymbolExpressionBinaryGE ELI_ARG((_TPPrSymbolExpressionBinaryGE _currn));
extern void _VS4rSymbolExpressionBinaryGE ELI_ARG((_TPPrSymbolExpressionBinaryGE _currn));
extern void _VS5rSymbolExpressionBinaryGE ELI_ARG((_TPPrSymbolExpressionBinaryGE _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryGE ELI_ARG((_TPPrSymbolExpressionBinaryGE _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryGE ELI_ARG((_TPPrSymbolExpressionBinaryGE _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryConcat ELI_ARG((_TPPrSymbolExpressionBinaryConcat _currn));
extern void _VS2rSymbolExpressionBinaryConcat ELI_ARG((_TPPrSymbolExpressionBinaryConcat _currn));
extern void _VS3rSymbolExpressionBinaryConcat ELI_ARG((_TPPrSymbolExpressionBinaryConcat _currn));
extern void _VS4rSymbolExpressionBinaryConcat ELI_ARG((_TPPrSymbolExpressionBinaryConcat _currn));
extern void _VS5rSymbolExpressionBinaryConcat ELI_ARG((_TPPrSymbolExpressionBinaryConcat _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryConcat ELI_ARG((_TPPrSymbolExpressionBinaryConcat _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryConcat ELI_ARG((_TPPrSymbolExpressionBinaryConcat _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryADD ELI_ARG((_TPPrSymbolExpressionBinaryADD _currn));
extern void _VS2rSymbolExpressionBinaryADD ELI_ARG((_TPPrSymbolExpressionBinaryADD _currn));
extern void _VS3rSymbolExpressionBinaryADD ELI_ARG((_TPPrSymbolExpressionBinaryADD _currn));
extern void _VS4rSymbolExpressionBinaryADD ELI_ARG((_TPPrSymbolExpressionBinaryADD _currn));
extern void _VS5rSymbolExpressionBinaryADD ELI_ARG((_TPPrSymbolExpressionBinaryADD _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryADD ELI_ARG((_TPPrSymbolExpressionBinaryADD _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryADD ELI_ARG((_TPPrSymbolExpressionBinaryADD _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinarySUB ELI_ARG((_TPPrSymbolExpressionBinarySUB _currn));
extern void _VS2rSymbolExpressionBinarySUB ELI_ARG((_TPPrSymbolExpressionBinarySUB _currn));
extern void _VS3rSymbolExpressionBinarySUB ELI_ARG((_TPPrSymbolExpressionBinarySUB _currn));
extern void _VS4rSymbolExpressionBinarySUB ELI_ARG((_TPPrSymbolExpressionBinarySUB _currn));
extern void _VS5rSymbolExpressionBinarySUB ELI_ARG((_TPPrSymbolExpressionBinarySUB _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinarySUB ELI_ARG((_TPPrSymbolExpressionBinarySUB _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinarySUB ELI_ARG((_TPPrSymbolExpressionBinarySUB _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryMUL ELI_ARG((_TPPrSymbolExpressionBinaryMUL _currn));
extern void _VS2rSymbolExpressionBinaryMUL ELI_ARG((_TPPrSymbolExpressionBinaryMUL _currn));
extern void _VS3rSymbolExpressionBinaryMUL ELI_ARG((_TPPrSymbolExpressionBinaryMUL _currn));
extern void _VS4rSymbolExpressionBinaryMUL ELI_ARG((_TPPrSymbolExpressionBinaryMUL _currn));
extern void _VS5rSymbolExpressionBinaryMUL ELI_ARG((_TPPrSymbolExpressionBinaryMUL _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryMUL ELI_ARG((_TPPrSymbolExpressionBinaryMUL _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryMUL ELI_ARG((_TPPrSymbolExpressionBinaryMUL _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryDIV ELI_ARG((_TPPrSymbolExpressionBinaryDIV _currn));
extern void _VS2rSymbolExpressionBinaryDIV ELI_ARG((_TPPrSymbolExpressionBinaryDIV _currn));
extern void _VS3rSymbolExpressionBinaryDIV ELI_ARG((_TPPrSymbolExpressionBinaryDIV _currn));
extern void _VS4rSymbolExpressionBinaryDIV ELI_ARG((_TPPrSymbolExpressionBinaryDIV _currn));
extern void _VS5rSymbolExpressionBinaryDIV ELI_ARG((_TPPrSymbolExpressionBinaryDIV _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryDIV ELI_ARG((_TPPrSymbolExpressionBinaryDIV _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryDIV ELI_ARG((_TPPrSymbolExpressionBinaryDIV _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryMOD ELI_ARG((_TPPrSymbolExpressionBinaryMOD _currn));
extern void _VS2rSymbolExpressionBinaryMOD ELI_ARG((_TPPrSymbolExpressionBinaryMOD _currn));
extern void _VS3rSymbolExpressionBinaryMOD ELI_ARG((_TPPrSymbolExpressionBinaryMOD _currn));
extern void _VS4rSymbolExpressionBinaryMOD ELI_ARG((_TPPrSymbolExpressionBinaryMOD _currn));
extern void _VS5rSymbolExpressionBinaryMOD ELI_ARG((_TPPrSymbolExpressionBinaryMOD _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryMOD ELI_ARG((_TPPrSymbolExpressionBinaryMOD _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryMOD ELI_ARG((_TPPrSymbolExpressionBinaryMOD _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionBinaryIsUnary ELI_ARG((_TPPrSymbolExpressionBinaryIsUnary _currn));
extern void _VS2rSymbolExpressionBinaryIsUnary ELI_ARG((_TPPrSymbolExpressionBinaryIsUnary _currn));
extern void _VS3rSymbolExpressionBinaryIsUnary ELI_ARG((_TPPrSymbolExpressionBinaryIsUnary _currn));
extern void _VS4rSymbolExpressionBinaryIsUnary ELI_ARG((_TPPrSymbolExpressionBinaryIsUnary _currn));
extern void _VS5rSymbolExpressionBinaryIsUnary ELI_ARG((_TPPrSymbolExpressionBinaryIsUnary _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionBinaryIsUnary ELI_ARG((_TPPrSymbolExpressionBinaryIsUnary _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionBinaryIsUnary ELI_ARG((_TPPrSymbolExpressionBinaryIsUnary _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,int* _AS0_const253,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionUnaryIsPostfix ELI_ARG((_TPPrSymbolExpressionUnaryIsPostfix _currn));
extern void _VS2rSymbolExpressionUnaryIsPostfix ELI_ARG((_TPPrSymbolExpressionUnaryIsPostfix _currn));
extern void _VS3rSymbolExpressionUnaryIsPostfix ELI_ARG((_TPPrSymbolExpressionUnaryIsPostfix _currn));
extern void _VS4rSymbolExpressionUnaryIsPostfix ELI_ARG((_TPPrSymbolExpressionUnaryIsPostfix _currn));
extern void _VS5rSymbolExpressionUnaryIsPostfix ELI_ARG((_TPPrSymbolExpressionUnaryIsPostfix _currn));
extern void _VS6rSymbolExpressionUnaryIsPostfix ELI_ARG((_TPPrSymbolExpressionUnaryIsPostfix _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionUnaryIsPostfix ELI_ARG((_TPPrSymbolExpressionUnaryIsPostfix _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionUnaryIncr ELI_ARG((_TPPrSymbolExpressionUnaryIncr _currn));
extern void _VS2rSymbolExpressionUnaryIncr ELI_ARG((_TPPrSymbolExpressionUnaryIncr _currn));
extern void _VS3rSymbolExpressionUnaryIncr ELI_ARG((_TPPrSymbolExpressionUnaryIncr _currn));
extern void _VS4rSymbolExpressionUnaryIncr ELI_ARG((_TPPrSymbolExpressionUnaryIncr _currn));
extern void _VS5rSymbolExpressionUnaryIncr ELI_ARG((_TPPrSymbolExpressionUnaryIncr _currn));
extern void _VS6rSymbolExpressionUnaryIncr ELI_ARG((_TPPrSymbolExpressionUnaryIncr _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionUnaryIncr ELI_ARG((_TPPrSymbolExpressionUnaryIncr _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionUnaryNEG ELI_ARG((_TPPrSymbolExpressionUnaryNEG _currn));
extern void _VS2rSymbolExpressionUnaryNEG ELI_ARG((_TPPrSymbolExpressionUnaryNEG _currn));
extern void _VS3rSymbolExpressionUnaryNEG ELI_ARG((_TPPrSymbolExpressionUnaryNEG _currn));
extern void _VS4rSymbolExpressionUnaryNEG ELI_ARG((_TPPrSymbolExpressionUnaryNEG _currn));
extern void _VS5rSymbolExpressionUnaryNEG ELI_ARG((_TPPrSymbolExpressionUnaryNEG _currn));
extern void _VS6rSymbolExpressionUnaryNEG ELI_ARG((_TPPrSymbolExpressionUnaryNEG _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionUnaryNEG ELI_ARG((_TPPrSymbolExpressionUnaryNEG _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionUnaryNOT ELI_ARG((_TPPrSymbolExpressionUnaryNOT _currn));
extern void _VS2rSymbolExpressionUnaryNOT ELI_ARG((_TPPrSymbolExpressionUnaryNOT _currn));
extern void _VS3rSymbolExpressionUnaryNOT ELI_ARG((_TPPrSymbolExpressionUnaryNOT _currn));
extern void _VS4rSymbolExpressionUnaryNOT ELI_ARG((_TPPrSymbolExpressionUnaryNOT _currn));
extern void _VS5rSymbolExpressionUnaryNOT ELI_ARG((_TPPrSymbolExpressionUnaryNOT _currn));
extern void _VS6rSymbolExpressionUnaryNOT ELI_ARG((_TPPrSymbolExpressionUnaryNOT _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionUnaryNOT ELI_ARG((_TPPrSymbolExpressionUnaryNOT _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionUnaryAddress ELI_ARG((_TPPrSymbolExpressionUnaryAddress _currn));
extern void _VS2rSymbolExpressionUnaryAddress ELI_ARG((_TPPrSymbolExpressionUnaryAddress _currn));
extern void _VS3rSymbolExpressionUnaryAddress ELI_ARG((_TPPrSymbolExpressionUnaryAddress _currn));
extern void _VS4rSymbolExpressionUnaryAddress ELI_ARG((_TPPrSymbolExpressionUnaryAddress _currn));
extern void _VS5rSymbolExpressionUnaryAddress ELI_ARG((_TPPrSymbolExpressionUnaryAddress _currn));
extern void _VS6rSymbolExpressionUnaryAddress ELI_ARG((_TPPrSymbolExpressionUnaryAddress _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionUnaryAddress ELI_ARG((_TPPrSymbolExpressionUnaryAddress _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionUnaryReference ELI_ARG((_TPPrSymbolExpressionUnaryReference _currn));
extern void _VS2rSymbolExpressionUnaryReference ELI_ARG((_TPPrSymbolExpressionUnaryReference _currn));
extern void _VS3rSymbolExpressionUnaryReference ELI_ARG((_TPPrSymbolExpressionUnaryReference _currn));
extern void _VS4rSymbolExpressionUnaryReference ELI_ARG((_TPPrSymbolExpressionUnaryReference _currn));
extern void _VS5rSymbolExpressionUnaryReference ELI_ARG((_TPPrSymbolExpressionUnaryReference _currn));
extern void _VS6rSymbolExpressionUnaryReference ELI_ARG((_TPPrSymbolExpressionUnaryReference _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionUnaryReference ELI_ARG((_TPPrSymbolExpressionUnaryReference _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionPostfixIsPrimary ELI_ARG((_TPPrSymbolExpressionPostfixIsPrimary _currn));
extern void _VS2rSymbolExpressionPostfixIsPrimary ELI_ARG((_TPPrSymbolExpressionPostfixIsPrimary _currn));
extern void _VS3rSymbolExpressionPostfixIsPrimary ELI_ARG((_TPPrSymbolExpressionPostfixIsPrimary _currn));
extern void _VS4rSymbolExpressionPostfixIsPrimary ELI_ARG((_TPPrSymbolExpressionPostfixIsPrimary _currn));
extern void _VS5rSymbolExpressionPostfixIsPrimary ELI_ARG((_TPPrSymbolExpressionPostfixIsPrimary _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionPostfixIsPrimary ELI_ARG((_TPPrSymbolExpressionPostfixIsPrimary _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionPostfixIsPrimary ELI_ARG((_TPPrSymbolExpressionPostfixIsPrimary _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0lido_codes,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionPostfixIsIndex ELI_ARG((_TPPrSymbolExpressionPostfixIsIndex _currn));
extern void _VS2rSymbolExpressionPostfixIsIndex ELI_ARG((_TPPrSymbolExpressionPostfixIsIndex _currn));
extern void _VS3rSymbolExpressionPostfixIsIndex ELI_ARG((_TPPrSymbolExpressionPostfixIsIndex _currn));
extern void _VS4rSymbolExpressionPostfixIsIndex ELI_ARG((_TPPrSymbolExpressionPostfixIsIndex _currn));
extern void _VS5rSymbolExpressionPostfixIsIndex ELI_ARG((_TPPrSymbolExpressionPostfixIsIndex _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionPostfixIsIndex ELI_ARG((_TPPrSymbolExpressionPostfixIsIndex _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionPostfixIsIndex ELI_ARG((_TPPrSymbolExpressionPostfixIsIndex _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0lido_codes,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionPostfixIsAccess ELI_ARG((_TPPrSymbolExpressionPostfixIsAccess _currn));
extern void _VS2rSymbolExpressionPostfixIsAccess ELI_ARG((_TPPrSymbolExpressionPostfixIsAccess _currn));
extern void _VS3rSymbolExpressionPostfixIsAccess ELI_ARG((_TPPrSymbolExpressionPostfixIsAccess _currn));
extern void _VS4rSymbolExpressionPostfixIsAccess ELI_ARG((_TPPrSymbolExpressionPostfixIsAccess _currn));
extern void _VS5rSymbolExpressionPostfixIsAccess ELI_ARG((_TPPrSymbolExpressionPostfixIsAccess _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionPostfixIsAccess ELI_ARG((_TPPrSymbolExpressionPostfixIsAccess _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionPostfixIsAccess ELI_ARG((_TPPrSymbolExpressionPostfixIsAccess _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0lido_codes,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolExpressionPostfixIsListcon ELI_ARG((_TPPrSymbolExpressionPostfixIsListcon _currn));
extern void _VS2rSymbolExpressionPostfixIsListcon ELI_ARG((_TPPrSymbolExpressionPostfixIsListcon _currn));
extern void _VS3rSymbolExpressionPostfixIsListcon ELI_ARG((_TPPrSymbolExpressionPostfixIsListcon _currn));
extern void _VS4rSymbolExpressionPostfixIsListcon ELI_ARG((_TPPrSymbolExpressionPostfixIsListcon _currn));
extern void _VS5rSymbolExpressionPostfixIsListcon ELI_ARG((_TPPrSymbolExpressionPostfixIsListcon _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolExpressionPostfixIsListcon ELI_ARG((_TPPrSymbolExpressionPostfixIsListcon _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolExpressionPostfixIsListcon ELI_ARG((_TPPrSymbolExpressionPostfixIsListcon _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0lido_codes,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
#define _VS1rSymbolExpressionPrimaryIsConstant _VS0Empty

#define _VS2rSymbolExpressionPrimaryIsConstant _VS0Empty

#define _VS3rSymbolExpressionPrimaryIsConstant _VS0Empty

#define _VS4rSymbolExpressionPrimaryIsConstant _VS0Empty

extern void _VS5rSymbolExpressionPrimaryIsConstant ELI_ARG((_TPPrSymbolExpressionPrimaryIsConstant _currn));
extern void _VS6rSymbolExpressionPrimaryIsConstant ELI_ARG((_TPPrSymbolExpressionPrimaryIsConstant _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionPrimaryIsConstant ELI_ARG((_TPPrSymbolExpressionPrimaryIsConstant _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionPrimaryIsCall ELI_ARG((_TPPrSymbolExpressionPrimaryIsCall _currn));
extern void _VS2rSymbolExpressionPrimaryIsCall ELI_ARG((_TPPrSymbolExpressionPrimaryIsCall _currn));
extern void _VS3rSymbolExpressionPrimaryIsCall ELI_ARG((_TPPrSymbolExpressionPrimaryIsCall _currn));
extern void _VS4rSymbolExpressionPrimaryIsCall ELI_ARG((_TPPrSymbolExpressionPrimaryIsCall _currn));
extern void _VS5rSymbolExpressionPrimaryIsCall ELI_ARG((_TPPrSymbolExpressionPrimaryIsCall _currn));
extern void _VS6rSymbolExpressionPrimaryIsCall ELI_ARG((_TPPrSymbolExpressionPrimaryIsCall _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionPrimaryIsCall ELI_ARG((_TPPrSymbolExpressionPrimaryIsCall _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionPrimaryIsTuple ELI_ARG((_TPPrSymbolExpressionPrimaryIsTuple _currn));
extern void _VS2rSymbolExpressionPrimaryIsTuple ELI_ARG((_TPPrSymbolExpressionPrimaryIsTuple _currn));
extern void _VS3rSymbolExpressionPrimaryIsTuple ELI_ARG((_TPPrSymbolExpressionPrimaryIsTuple _currn));
extern void _VS4rSymbolExpressionPrimaryIsTuple ELI_ARG((_TPPrSymbolExpressionPrimaryIsTuple _currn));
extern void _VS5rSymbolExpressionPrimaryIsTuple ELI_ARG((_TPPrSymbolExpressionPrimaryIsTuple _currn));
extern void _VS6rSymbolExpressionPrimaryIsTuple ELI_ARG((_TPPrSymbolExpressionPrimaryIsTuple _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionPrimaryIsTuple ELI_ARG((_TPPrSymbolExpressionPrimaryIsTuple _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionPrimaryIsWrap ELI_ARG((_TPPrSymbolExpressionPrimaryIsWrap _currn));
extern void _VS2rSymbolExpressionPrimaryIsWrap ELI_ARG((_TPPrSymbolExpressionPrimaryIsWrap _currn));
extern void _VS3rSymbolExpressionPrimaryIsWrap ELI_ARG((_TPPrSymbolExpressionPrimaryIsWrap _currn));
extern void _VS4rSymbolExpressionPrimaryIsWrap ELI_ARG((_TPPrSymbolExpressionPrimaryIsWrap _currn));
extern void _VS5rSymbolExpressionPrimaryIsWrap ELI_ARG((_TPPrSymbolExpressionPrimaryIsWrap _currn));
extern void _VS6rSymbolExpressionPrimaryIsWrap ELI_ARG((_TPPrSymbolExpressionPrimaryIsWrap _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionPrimaryIsWrap ELI_ARG((_TPPrSymbolExpressionPrimaryIsWrap _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolTupleConstruction ELI_ARG((_TPPrSymbolTupleConstruction _currn));
extern void _VS2rSymbolTupleConstruction ELI_ARG((_TPPrSymbolTupleConstruction _currn));
extern void _VS3rSymbolTupleConstruction ELI_ARG((_TPPrSymbolTupleConstruction _currn));
extern void _VS4rSymbolTupleConstruction ELI_ARG((_TPPrSymbolTupleConstruction _currn));
extern void _VS5rSymbolTupleConstruction ELI_ARG((_TPPrSymbolTupleConstruction _currn));
extern void _VS6rSymbolTupleConstruction ELI_ARG((_TPPrSymbolTupleConstruction _currn));
extern void _VS7rSymbolTupleConstruction ELI_ARG((_TPPrSymbolTupleConstruction _currn));
#define _VS1rSymbolTupleArgumentList2 _VS1rPTGDefList2

#define _VS2rSymbolTupleArgumentList2 _VS2rPTGDefList2

#define _VS3rSymbolTupleArgumentList2 _VS3rPTGDefList2

#define _VS4rSymbolTupleArgumentList2 _VS4rPTGDefList2

extern void _VS5rSymbolTupleArgumentList2 ELI_ARG((_TPPrSymbolTupleArgumentList2 _currn,D_SymbolExpressions* _AS0_const263,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolTupleArgumentList2 ELI_ARG((_TPPrSymbolTupleArgumentList2 _currn,D_SymbolExpressions* _AS0_const263,D_types* _AS0_const262,int* _AS0_const261,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolTupleArgumentList2 ELI_ARG((_TPPrSymbolTupleArgumentList2 _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressions* _AS0_const263,D_types* _AS0_const262,int* _AS0_const261,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
#define _VS1rSymbolTupleArgumentList1 _VS1rPTGDecl

#define _VS2rSymbolTupleArgumentList1 _VS2rPTGDecl

#define _VS3rSymbolTupleArgumentList1 _VS3rPTGDecl

#define _VS4rSymbolTupleArgumentList1 _VS4rPTGDecl

extern void _VS5rSymbolTupleArgumentList1 ELI_ARG((_TPPrSymbolTupleArgumentList1 _currn,D_SymbolExpressions* _AS0_const263,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolTupleArgumentList1 ELI_ARG((_TPPrSymbolTupleArgumentList1 _currn,D_SymbolExpressions* _AS0_const263,D_types* _AS0_const262,int* _AS0_const261,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolTupleArgumentList1 ELI_ARG((_TPPrSymbolTupleArgumentList1 _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressions* _AS0_const263,D_types* _AS0_const262,int* _AS0_const261,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
#define _VS1rSymbolTupleArgument _VS1rPTGDecl

#define _VS2rSymbolTupleArgument _VS2rPTGDecl

#define _VS3rSymbolTupleArgument _VS3rPTGDecl

#define _VS4rSymbolTupleArgument _VS4rPTGDecl

extern void _VS5rSymbolTupleArgument ELI_ARG((_TPPrSymbolTupleArgument _currn));
extern void _VS6rSymbolTupleArgument ELI_ARG((_TPPrSymbolTupleArgument _currn));
extern void _VS7rSymbolTupleArgument ELI_ARG((_TPPrSymbolTupleArgument _currn));
extern void _VS1rSymbolExpressionCallApplied ELI_ARG((_TPPrSymbolExpressionCallApplied _currn));
extern void _VS2rSymbolExpressionCallApplied ELI_ARG((_TPPrSymbolExpressionCallApplied _currn));
extern void _VS3rSymbolExpressionCallApplied ELI_ARG((_TPPrSymbolExpressionCallApplied _currn));
extern void _VS4rSymbolExpressionCallApplied ELI_ARG((_TPPrSymbolExpressionCallApplied _currn));
extern void _VS5rSymbolExpressionCallApplied ELI_ARG((_TPPrSymbolExpressionCallApplied _currn));
extern void _VS6rSymbolExpressionCallApplied ELI_ARG((_TPPrSymbolExpressionCallApplied _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionCallApplied ELI_ARG((_TPPrSymbolExpressionCallApplied _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionCallEmpty ELI_ARG((_TPPrSymbolExpressionCallEmpty _currn));
extern void _VS2rSymbolExpressionCallEmpty ELI_ARG((_TPPrSymbolExpressionCallEmpty _currn));
extern void _VS3rSymbolExpressionCallEmpty ELI_ARG((_TPPrSymbolExpressionCallEmpty _currn));
extern void _VS4rSymbolExpressionCallEmpty ELI_ARG((_TPPrSymbolExpressionCallEmpty _currn));
extern void _VS5rSymbolExpressionCallEmpty ELI_ARG((_TPPrSymbolExpressionCallEmpty _currn));
extern void _VS6rSymbolExpressionCallEmpty ELI_ARG((_TPPrSymbolExpressionCallEmpty _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionCallEmpty ELI_ARG((_TPPrSymbolExpressionCallEmpty _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolExpressionCallVariable ELI_ARG((_TPPrSymbolExpressionCallVariable _currn));
extern void _VS2rSymbolExpressionCallVariable ELI_ARG((_TPPrSymbolExpressionCallVariable _currn));
extern void _VS3rSymbolExpressionCallVariable ELI_ARG((_TPPrSymbolExpressionCallVariable _currn));
extern void _VS4rSymbolExpressionCallVariable ELI_ARG((_TPPrSymbolExpressionCallVariable _currn));
extern void _VS5rSymbolExpressionCallVariable ELI_ARG((_TPPrSymbolExpressionCallVariable _currn));
extern void _VS6rSymbolExpressionCallVariable ELI_ARG((_TPPrSymbolExpressionCallVariable _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolExpressionCallVariable ELI_ARG((_TPPrSymbolExpressionCallVariable _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolCallableReferenceIsIdentifier ELI_ARG((_TPPrSymbolCallableReferenceIsIdentifier _currn));
#define _VS2rSymbolCallableReferenceIsIdentifier _VS0Empty

extern void _VS3rSymbolCallableReferenceIsIdentifier ELI_ARG((_TPPrSymbolCallableReferenceIsIdentifier _currn));
#define _VS4rSymbolCallableReferenceIsIdentifier _VS0Empty

extern void _VS5rSymbolCallableReferenceIsIdentifier ELI_ARG((_TPPrSymbolCallableReferenceIsIdentifier _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolCallableReferenceIsIdentifier ELI_ARG((_TPPrSymbolCallableReferenceIsIdentifier _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolCallableReferenceIsIdentifier ELI_ARG((_TPPrSymbolCallableReferenceIsIdentifier _currn,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const248));
extern void _VS8rSymbolCallableReferenceIsIdentifier ELI_ARG((_TPPrSymbolCallableReferenceIsIdentifier _currn,PTGNodes* _AS0lido_codes,int* _AS0nonattr_call_count,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,StringTableKey* _AS0_const243,BindingList* _AS0_const242,StringTableKeyList* _AS0_const226,StringTableKey* _AS0_const10));
extern void _VS1rSymbolCallableReferenceIsTypename ELI_ARG((_TPPrSymbolCallableReferenceIsTypename _currn));
#define _VS2rSymbolCallableReferenceIsTypename _VS0Empty

#define _VS3rSymbolCallableReferenceIsTypename _VS0Empty

#define _VS4rSymbolCallableReferenceIsTypename _VS0Empty

extern void _VS5rSymbolCallableReferenceIsTypename ELI_ARG((_TPPrSymbolCallableReferenceIsTypename _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolCallableReferenceIsTypename ELI_ARG((_TPPrSymbolCallableReferenceIsTypename _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolCallableReferenceIsTypename ELI_ARG((_TPPrSymbolCallableReferenceIsTypename _currn,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const248));
extern void _VS8rSymbolCallableReferenceIsTypename ELI_ARG((_TPPrSymbolCallableReferenceIsTypename _currn,PTGNodes* _AS0lido_codes,int* _AS0nonattr_call_count,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,StringTableKey* _AS0_const243,BindingList* _AS0_const242,StringTableKeyList* _AS0_const226,StringTableKey* _AS0_const10));
extern void _VS1rSymbolCallableReferenceIsTreeCon ELI_ARG((_TPPrSymbolCallableReferenceIsTreeCon _currn));
#define _VS2rSymbolCallableReferenceIsTreeCon _VS0Empty

extern void _VS3rSymbolCallableReferenceIsTreeCon ELI_ARG((_TPPrSymbolCallableReferenceIsTreeCon _currn));
#define _VS4rSymbolCallableReferenceIsTreeCon _VS0Empty

extern void _VS5rSymbolCallableReferenceIsTreeCon ELI_ARG((_TPPrSymbolCallableReferenceIsTreeCon _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolCallableReferenceIsTreeCon ELI_ARG((_TPPrSymbolCallableReferenceIsTreeCon _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolCallableReferenceIsTreeCon ELI_ARG((_TPPrSymbolCallableReferenceIsTreeCon _currn,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const248));
extern void _VS8rSymbolCallableReferenceIsTreeCon ELI_ARG((_TPPrSymbolCallableReferenceIsTreeCon _currn,PTGNodes* _AS0lido_codes,int* _AS0nonattr_call_count,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,StringTableKey* _AS0_const243,BindingList* _AS0_const242,StringTableKeyList* _AS0_const226,StringTableKey* _AS0_const10));
extern void _VS1rSymbolCallableReferenceIsEnd ELI_ARG((_TPPrSymbolCallableReferenceIsEnd _currn));
#define _VS2rSymbolCallableReferenceIsEnd _VS0Empty

#define _VS3rSymbolCallableReferenceIsEnd _VS0Empty

#define _VS4rSymbolCallableReferenceIsEnd _VS0Empty

extern void _VS5rSymbolCallableReferenceIsEnd ELI_ARG((_TPPrSymbolCallableReferenceIsEnd _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolCallableReferenceIsEnd ELI_ARG((_TPPrSymbolCallableReferenceIsEnd _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolCallableReferenceIsEnd ELI_ARG((_TPPrSymbolCallableReferenceIsEnd _currn,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const248));
extern void _VS8rSymbolCallableReferenceIsEnd ELI_ARG((_TPPrSymbolCallableReferenceIsEnd _currn,PTGNodes* _AS0lido_codes,int* _AS0nonattr_call_count,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,StringTableKey* _AS0_const243,BindingList* _AS0_const242,StringTableKeyList* _AS0_const226,StringTableKey* _AS0_const10));
extern void _VS1rSymbolCallableReferenceIsAttribute ELI_ARG((_TPPrSymbolCallableReferenceIsAttribute _currn));
extern void _VS2rSymbolCallableReferenceIsAttribute ELI_ARG((_TPPrSymbolCallableReferenceIsAttribute _currn));
extern void _VS3rSymbolCallableReferenceIsAttribute ELI_ARG((_TPPrSymbolCallableReferenceIsAttribute _currn));
extern void _VS4rSymbolCallableReferenceIsAttribute ELI_ARG((_TPPrSymbolCallableReferenceIsAttribute _currn));
extern void _VS5rSymbolCallableReferenceIsAttribute ELI_ARG((_TPPrSymbolCallableReferenceIsAttribute _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolCallableReferenceIsAttribute ELI_ARG((_TPPrSymbolCallableReferenceIsAttribute _currn,D_SymbolExpressionPtr* _AS0sexpr_code,D_SymbolAttributes* _AS0_const248));
extern void _VS7rSymbolCallableReferenceIsAttribute ELI_ARG((_TPPrSymbolCallableReferenceIsAttribute _currn,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const248));
extern void _VS8rSymbolCallableReferenceIsAttribute ELI_ARG((_TPPrSymbolCallableReferenceIsAttribute _currn,PTGNodes* _AS0lido_codes,int* _AS0nonattr_call_count,D_SymbolExpressionPtr* _AS0sexpr_code,int* _AS0_const261,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,StringTableKey* _AS0_const243,BindingList* _AS0_const242,StringTableKeyList* _AS0_const226,StringTableKey* _AS0_const10));
extern void _VS1rSymbolCallParameterList2 ELI_ARG((_TPPrSymbolCallParameterList2 _currn));
extern void _VS2rSymbolCallParameterList2 ELI_ARG((_TPPrSymbolCallParameterList2 _currn));
extern void _VS3rSymbolCallParameterList2 ELI_ARG((_TPPrSymbolCallParameterList2 _currn));
extern void _VS4rSymbolCallParameterList2 ELI_ARG((_TPPrSymbolCallParameterList2 _currn));
extern void _VS5rSymbolCallParameterList2 ELI_ARG((_TPPrSymbolCallParameterList2 _currn,D_SymbolExpressions* _AS0_const267,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolCallParameterList2 ELI_ARG((_TPPrSymbolCallParameterList2 _currn,D_SymbolExpressions* _AS0_const267,D_types* _AS0_const265,int* _AS0_const261,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolCallParameterList2 ELI_ARG((_TPPrSymbolCallParameterList2 _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressions* _AS0_const267,D_types* _AS0_const265,int* _AS0_const261,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolCallParameterList1 ELI_ARG((_TPPrSymbolCallParameterList1 _currn));
extern void _VS2rSymbolCallParameterList1 ELI_ARG((_TPPrSymbolCallParameterList1 _currn));
extern void _VS3rSymbolCallParameterList1 ELI_ARG((_TPPrSymbolCallParameterList1 _currn));
extern void _VS4rSymbolCallParameterList1 ELI_ARG((_TPPrSymbolCallParameterList1 _currn));
extern void _VS5rSymbolCallParameterList1 ELI_ARG((_TPPrSymbolCallParameterList1 _currn,D_SymbolExpressions* _AS0_const267,D_SymbolAttributes* _AS0_const248));
extern void _VS6rSymbolCallParameterList1 ELI_ARG((_TPPrSymbolCallParameterList1 _currn,D_SymbolExpressions* _AS0_const267,D_types* _AS0_const265,int* _AS0_const261,D_SymbolAttributes* _AS0_const248,int* _AS0_const244));
extern void _VS7rSymbolCallParameterList1 ELI_ARG((_TPPrSymbolCallParameterList1 _currn,PTGNodes* _AS0lido_codes,D_SymbolExpressions* _AS0_const267,D_types* _AS0_const265,int* _AS0_const261,D_SymbolAttributes* _AS0_const252,D_SymbolAttributes* _AS0_const248,D_OrderStats* _AS0_const246,int* _AS0_const245,int* _AS0_const244,StringTableKey* _AS0_const243,BindingList* _AS0_const242,int* _AS0_const240,StringTableKeyList* _AS0_const226,D_VirtAttrs* _AS0_const220,StringTableKey* _AS0_const10));
extern void _VS1rSymbolCallParameter ELI_ARG((_TPPrSymbolCallParameter _currn));
extern void _VS2rSymbolCallParameter ELI_ARG((_TPPrSymbolCallParameter _currn));
extern void _VS3rSymbolCallParameter ELI_ARG((_TPPrSymbolCallParameter _currn));
extern void _VS4rSymbolCallParameter ELI_ARG((_TPPrSymbolCallParameter _currn));
extern void _VS5rSymbolCallParameter ELI_ARG((_TPPrSymbolCallParameter _currn));
extern void _VS6rSymbolCallParameter ELI_ARG((_TPPrSymbolCallParameter _currn,D_typePtr* _AS0expected_type));
extern void _VS7rSymbolCallParameter ELI_ARG((_TPPrSymbolCallParameter _currn,D_typePtr* _AS0expected_type));
extern void _VS1rSymbolConstant ELI_ARG((_TPPrSymbolConstant _currn));
extern void _VS2rSymbolConstant ELI_ARG((_TPPrSymbolConstant _currn));
extern void _VS3rSymbolConstant ELI_ARG((_TPPrSymbolConstant _currn));
extern void _VS1rDeclRuleSpecification ELI_ARG((_TPPrDeclRuleSpecification _currn));
extern void _VS2rDeclRuleSpecification ELI_ARG((_TPPrDeclRuleSpecification _currn));
#define _VS3rDeclRuleSpecification _VS0Empty

extern void _VS4rDeclRuleSpecification ELI_ARG((_TPPrDeclRuleSpecification _currn));
#define _VS5rDeclRuleSpecification _VS0Empty

#define _VS6rDeclRuleSpecification _VS0Empty

extern void _VS7rDeclRuleSpecification ELI_ARG((_TPPrDeclRuleSpecification _currn));
extern void _VS8rDeclRuleSpecification ELI_ARG((_TPPrDeclRuleSpecification _currn));
extern void _VS9rDeclRuleSpecification ELI_ARG((_TPPrDeclRuleSpecification _currn));
extern void _VS10rDeclRuleSpecification ELI_ARG((_TPPrDeclRuleSpecification _currn));
extern void _VS1rRuleSpecification ELI_ARG((_TPPrRuleSpecification _currn));
extern void _VS2rRuleSpecification ELI_ARG((_TPPrRuleSpecification _currn));
extern void _VS3rRuleSpecification ELI_ARG((_TPPrRuleSpecification _currn));
extern void _VS4rRuleSpecification ELI_ARG((_TPPrRuleSpecification _currn));
extern void _VS5rRuleSpecification ELI_ARG((_TPPrRuleSpecification _currn));
extern void _VS6rRuleSpecification ELI_ARG((_TPPrRuleSpecification _currn));
extern void _VS7rRuleSpecification ELI_ARG((_TPPrRuleSpecification _currn));
extern void _VS1rRuleGuardCondition ELI_ARG((_TPPrRuleGuardCondition _currn));
extern void _VS2rRuleGuardCondition ELI_ARG((_TPPrRuleGuardCondition _currn));
extern void _VS3rRuleGuardCondition ELI_ARG((_TPPrRuleGuardCondition _currn));
extern void _VS4rRuleGuardCondition ELI_ARG((_TPPrRuleGuardCondition _currn));
extern void _VS5rRuleGuardCondition ELI_ARG((_TPPrRuleGuardCondition _currn));
extern void _VS6rRuleGuardCondition ELI_ARG((_TPPrRuleGuardCondition _currn));
extern void _VS7rRuleGuardCondition ELI_ARG((_TPPrRuleGuardCondition _currn));
extern void _VS8rRuleGuardCondition ELI_ARG((_TPPrRuleGuardCondition _currn));
extern void _VS1rRuleGuardRhsGuards ELI_ARG((_TPPrRuleGuardRhsGuards _currn));
extern void _VS2rRuleGuardRhsGuards ELI_ARG((_TPPrRuleGuardRhsGuards _currn));
extern void _VS3rRuleGuardRhsGuards ELI_ARG((_TPPrRuleGuardRhsGuards _currn));
extern void _VS4rRuleGuardRhsGuards ELI_ARG((_TPPrRuleGuardRhsGuards _currn));
extern void _VS5rRuleGuardRhsGuards ELI_ARG((_TPPrRuleGuardRhsGuards _currn));
extern void _VS6rRuleGuardRhsGuards ELI_ARG((_TPPrRuleGuardRhsGuards _currn));
extern void _VS7rRuleGuardRhsGuards ELI_ARG((_TPPrRuleGuardRhsGuards _currn));
extern void _VS8rRuleGuardRhsGuards ELI_ARG((_TPPrRuleGuardRhsGuards _currn));
extern void _VS9rRuleGuardRhsGuards ELI_ARG((_TPPrRuleGuardRhsGuards _currn));
extern void _VS1rRuleGuardRhsComputes ELI_ARG((_TPPrRuleGuardRhsComputes _currn));
extern void _VS2rRuleGuardRhsComputes ELI_ARG((_TPPrRuleGuardRhsComputes _currn));
extern void _VS3rRuleGuardRhsComputes ELI_ARG((_TPPrRuleGuardRhsComputes _currn));
extern void _VS4rRuleGuardRhsComputes ELI_ARG((_TPPrRuleGuardRhsComputes _currn));
extern void _VS5rRuleGuardRhsComputes ELI_ARG((_TPPrRuleGuardRhsComputes _currn));
extern void _VS6rRuleGuardRhsComputes ELI_ARG((_TPPrRuleGuardRhsComputes _currn));
extern void _VS7rRuleGuardRhsComputes ELI_ARG((_TPPrRuleGuardRhsComputes _currn));
extern void _VS8rRuleGuardRhsComputes ELI_ARG((_TPPrRuleGuardRhsComputes _currn));
extern void _VS9rRuleGuardRhsComputes ELI_ARG((_TPPrRuleGuardRhsComputes _currn));
extern void _VS1rRuleClassificationSingle ELI_ARG((_TPPrRuleClassificationSingle _currn));
extern void _VS2rRuleClassificationSingle ELI_ARG((_TPPrRuleClassificationSingle _currn));
extern void _VS1rRuleClassificationMultiple ELI_ARG((_TPPrRuleClassificationMultiple _currn));
extern void _VS2rRuleClassificationMultiple ELI_ARG((_TPPrRuleClassificationMultiple _currn));
extern void _VS1rRuleClassificationSingleClass ELI_ARG((_TPPrRuleClassificationSingleClass _currn));
extern void _VS2rRuleClassificationSingleClass ELI_ARG((_TPPrRuleClassificationSingleClass _currn));
extern void _VS1rRuleClassificationMultipleClass ELI_ARG((_TPPrRuleClassificationMultipleClass _currn));
extern void _VS2rRuleClassificationMultipleClass ELI_ARG((_TPPrRuleClassificationMultipleClass _currn));
extern void _VS1rRulePatternMultiple ELI_ARG((_TPPrRulePatternMultiple _currn));
extern void _VS2rRulePatternMultiple ELI_ARG((_TPPrRulePatternMultiple _currn));
extern void _VS3rRulePatternMultiple ELI_ARG((_TPPrRulePatternMultiple _currn));
extern void _VS4rRulePatternMultiple ELI_ARG((_TPPrRulePatternMultiple _currn));
extern void _VS1rRulePatternSingle ELI_ARG((_TPPrRulePatternSingle _currn));
extern void _VS2rRulePatternSingle ELI_ARG((_TPPrRulePatternSingle _currn));
extern void _VS3rRulePatternSingle ELI_ARG((_TPPrRulePatternSingle _currn));
extern void _VS4rRulePatternSingle ELI_ARG((_TPPrRulePatternSingle _currn));
extern void _VS1rRulePatternVarious ELI_ARG((_TPPrRulePatternVarious _currn));
extern void _VS2rRulePatternVarious ELI_ARG((_TPPrRulePatternVarious _currn));
extern void _VS3rRulePatternVarious ELI_ARG((_TPPrRulePatternVarious _currn));
extern void _VS4rRulePatternVarious ELI_ARG((_TPPrRulePatternVarious _currn));
extern void _VS1rRuleReferenceIsTypename ELI_ARG((_TPPrRuleReferenceIsTypename _currn));
extern void _VS2rRuleReferenceIsTypename ELI_ARG((_TPPrRuleReferenceIsTypename _currn));
extern void _VS1rRuleReferenceIsIdentifier ELI_ARG((_TPPrRuleReferenceIsIdentifier _currn));
extern void _VS2rRuleReferenceIsIdentifier ELI_ARG((_TPPrRuleReferenceIsIdentifier _currn));
extern void _VS1rRuleReferenceList2 ELI_ARG((_TPPrRuleReferenceList2 _currn));
extern void _VS2rRuleReferenceList2 ELI_ARG((_TPPrRuleReferenceList2 _currn));
extern void _VS1rRuleReferenceList1 ELI_ARG((_TPPrRuleReferenceList1 _currn));
extern void _VS2rRuleReferenceList1 ELI_ARG((_TPPrRuleReferenceList1 _currn));
#define _VS1rRuleLhsPatternSpecific _VS1rFunctionLetVarDef

extern void _VS2rRuleLhsPatternSpecific ELI_ARG((_TPPrRuleLhsPatternSpecific _currn));
extern void _VS3rRuleLhsPatternSpecific ELI_ARG((_TPPrRuleLhsPatternSpecific _currn));
extern void _VS4rRuleLhsPatternSpecific ELI_ARG((_TPPrRuleLhsPatternSpecific _currn));
extern void _VS1rRuleLhsPatternDontCare ELI_ARG((_TPPrRuleLhsPatternDontCare _currn));
extern void _VS2rRuleLhsPatternDontCare ELI_ARG((_TPPrRuleLhsPatternDontCare _currn));
extern void _VS3rRuleLhsPatternDontCare ELI_ARG((_TPPrRuleLhsPatternDontCare _currn));
extern void _VS4rRuleLhsPatternDontCare ELI_ARG((_TPPrRuleLhsPatternDontCare _currn));
extern void _VS1rNoRuleVariableDef ELI_ARG((_TPPrNoRuleVariableDef _currn,StringTableKey* _AS0sym));
extern void _VS2rNoRuleVariableDef ELI_ARG((_TPPrNoRuleVariableDef _currn,StringTableKey* _AS0sym));
extern void _VS1rCreateRuleVariable ELI_ARG((_TPPrCreateRuleVariable _currn,StringTableKey* _AS0sym));
extern void _VS2rCreateRuleVariable ELI_ARG((_TPPrCreateRuleVariable _currn,StringTableKey* _AS0sym));
extern void _VS1rOptProduction ELI_ARG((_TPPrOptProduction _currn));
extern void _VS2rOptProduction ELI_ARG((_TPPrOptProduction _currn));
extern void _VS3rOptProduction ELI_ARG((_TPPrOptProduction _currn));
extern void _VS4rOptProduction ELI_ARG((_TPPrOptProduction _currn));
#define _VS1rNoProduction _VS0Empty

extern void _VS2rNoProduction ELI_ARG((_TPPrNoProduction _currn));
#define _VS3rNoProduction _VS0Empty

extern void _VS4rNoProduction ELI_ARG((_TPPrNoProduction _currn));
#define _VS1rRuleRHSPattern _VS1rPTGDecl

extern void _VS2rRuleRHSPattern ELI_ARG((_TPPrRuleRHSPattern _currn));
#define _VS3rRuleRHSPattern _VS3rPTGDecl

extern void _VS4rRuleRHSPattern ELI_ARG((_TPPrRuleRHSPattern _currn));
extern void _VS1rRuleRHSPatternSymbolList2 ELI_ARG((_TPPrRuleRHSPatternSymbolList2 _currn));
extern void _VS2rRuleRHSPatternSymbolList2 ELI_ARG((_TPPrRuleRHSPatternSymbolList2 _currn));
extern void _VS3rRuleRHSPatternSymbolList2 ELI_ARG((_TPPrRuleRHSPatternSymbolList2 _currn));
extern void _VS4rRuleRHSPatternSymbolList2 ELI_ARG((_TPPrRuleRHSPatternSymbolList2 _currn));
#define _VS1rNoRuleRHSPatternSymbols _VS0Empty

extern void _VS2rNoRuleRHSPatternSymbols ELI_ARG((_TPPrNoRuleRHSPatternSymbols _currn));
extern void _VS3rNoRuleRHSPatternSymbols ELI_ARG((_TPPrNoRuleRHSPatternSymbols _currn));
extern void _VS4rNoRuleRHSPatternSymbols ELI_ARG((_TPPrNoRuleRHSPatternSymbols _currn));
#define _VS1rRulePatternSymbolAllRest _VS0Empty

extern void _VS2rRulePatternSymbolAllRest ELI_ARG((_TPPrRulePatternSymbolAllRest _currn));
extern void _VS3rRulePatternSymbolAllRest ELI_ARG((_TPPrRulePatternSymbolAllRest _currn));
extern void _VS4rRulePatternSymbolAllRest ELI_ARG((_TPPrRulePatternSymbolAllRest _currn));
#define _VS1rRulePatternSymbolDontCare _VS0Empty

extern void _VS2rRulePatternSymbolDontCare ELI_ARG((_TPPrRulePatternSymbolDontCare _currn));
extern void _VS3rRulePatternSymbolDontCare ELI_ARG((_TPPrRulePatternSymbolDontCare _currn));
extern void _VS4rRulePatternSymbolDontCare ELI_ARG((_TPPrRulePatternSymbolDontCare _currn));
extern void _VS1rRulePatternSymbolConstructor ELI_ARG((_TPPrRulePatternSymbolConstructor _currn));
extern void _VS2rRulePatternSymbolConstructor ELI_ARG((_TPPrRulePatternSymbolConstructor _currn));
extern void _VS3rRulePatternSymbolConstructor ELI_ARG((_TPPrRulePatternSymbolConstructor _currn));
extern void _VS4rRulePatternSymbolConstructor ELI_ARG((_TPPrRulePatternSymbolConstructor _currn));
extern void _VS1rRulePatternSymbolMatchTermString ELI_ARG((_TPPrRulePatternSymbolMatchTermString _currn));
extern void _VS2rRulePatternSymbolMatchTermString ELI_ARG((_TPPrRulePatternSymbolMatchTermString _currn));
extern void _VS3rRulePatternSymbolMatchTermString ELI_ARG((_TPPrRulePatternSymbolMatchTermString _currn));
extern void _VS4rRulePatternSymbolMatchTermString ELI_ARG((_TPPrRulePatternSymbolMatchTermString _currn));
extern void _VS1rRulePatternSymbolMatchTermInt ELI_ARG((_TPPrRulePatternSymbolMatchTermInt _currn));
extern void _VS2rRulePatternSymbolMatchTermInt ELI_ARG((_TPPrRulePatternSymbolMatchTermInt _currn));
extern void _VS3rRulePatternSymbolMatchTermInt ELI_ARG((_TPPrRulePatternSymbolMatchTermInt _currn));
extern void _VS4rRulePatternSymbolMatchTermInt ELI_ARG((_TPPrRulePatternSymbolMatchTermInt _currn));
#define _VS1rNoDeeperMatching _VS0Empty

extern void _VS2rNoDeeperMatching ELI_ARG((_TPPrNoDeeperMatching _currn));
extern void _VS3rNoDeeperMatching ELI_ARG((_TPPrNoDeeperMatching _currn));
extern void _VS1rOptDeeperRulePattern ELI_ARG((_TPPrOptDeeperRulePattern _currn));
extern void _VS2rOptDeeperRulePattern ELI_ARG((_TPPrOptDeeperRulePattern _currn));
extern void _VS3rOptDeeperRulePattern ELI_ARG((_TPPrOptDeeperRulePattern _currn));
#define _VS1rDeeperRulePatternSymbolList2 _VS1rPTGDefList2

extern void _VS2rDeeperRulePatternSymbolList2 ELI_ARG((_TPPrDeeperRulePatternSymbolList2 _currn));
extern void _VS3rDeeperRulePatternSymbolList2 ELI_ARG((_TPPrDeeperRulePatternSymbolList2 _currn));
#define _VS1rNoDeeperRulePatternSymbols _VS0Empty

extern void _VS2rNoDeeperRulePatternSymbols ELI_ARG((_TPPrNoDeeperRulePatternSymbols _currn));
extern void _VS3rNoDeeperRulePatternSymbols ELI_ARG((_TPPrNoDeeperRulePatternSymbols _currn));
#define _VS1rDeeperRulePatternSymbolDontCare _VS0Empty

extern void _VS2rDeeperRulePatternSymbolDontCare ELI_ARG((_TPPrDeeperRulePatternSymbolDontCare _currn));
extern void _VS3rDeeperRulePatternSymbolDontCare ELI_ARG((_TPPrDeeperRulePatternSymbolDontCare _currn));
#define _VS1rDeeperRulePatternSymbolDontCareRest _VS0Empty

extern void _VS2rDeeperRulePatternSymbolDontCareRest ELI_ARG((_TPPrDeeperRulePatternSymbolDontCareRest _currn));
extern void _VS3rDeeperRulePatternSymbolDontCareRest ELI_ARG((_TPPrDeeperRulePatternSymbolDontCareRest _currn));
#define _VS1rDeeperRulePatternSymbolMatchTermString _VS1rPTGDecl

extern void _VS2rDeeperRulePatternSymbolMatchTermString ELI_ARG((_TPPrDeeperRulePatternSymbolMatchTermString _currn));
extern void _VS3rDeeperRulePatternSymbolMatchTermString ELI_ARG((_TPPrDeeperRulePatternSymbolMatchTermString _currn));
#define _VS1rDeeperRulePatternSymbolMatchTermInt _VS1rPTGDecl

extern void _VS2rDeeperRulePatternSymbolMatchTermInt ELI_ARG((_TPPrDeeperRulePatternSymbolMatchTermInt _currn));
extern void _VS3rDeeperRulePatternSymbolMatchTermInt ELI_ARG((_TPPrDeeperRulePatternSymbolMatchTermInt _currn));
extern void _VS1rDeeperMatchConstructor ELI_ARG((_TPPrDeeperMatchConstructor _currn));
extern void _VS2rDeeperMatchConstructor ELI_ARG((_TPPrDeeperMatchConstructor _currn));
extern void _VS3rDeeperMatchConstructor ELI_ARG((_TPPrDeeperMatchConstructor _currn));
extern void _VS1rRuleComputationList2 ELI_ARG((_TPPrRuleComputationList2 _currn));
extern void _VS2rRuleComputationList2 ELI_ARG((_TPPrRuleComputationList2 _currn));
extern void _VS3rRuleComputationList2 ELI_ARG((_TPPrRuleComputationList2 _currn));
extern void _VS4rRuleComputationList2 ELI_ARG((_TPPrRuleComputationList2 _currn));
extern void _VS5rRuleComputationList2 ELI_ARG((_TPPrRuleComputationList2 _currn));
extern void _VS6rRuleComputationList2 ELI_ARG((_TPPrRuleComputationList2 _currn));
extern void _VS7rRuleComputationList2 ELI_ARG((_TPPrRuleComputationList2 _currn));
extern void _VS8rRuleComputationList2 ELI_ARG((_TPPrRuleComputationList2 _currn));
extern void _VS9rRuleComputationList2 ELI_ARG((_TPPrRuleComputationList2 _currn));
#define _VS1rNoRuleComputations _VS0Empty

extern void _VS2rNoRuleComputations ELI_ARG((_TPPrNoRuleComputations _currn));
#define _VS3rNoRuleComputations _VS0Empty

extern void _VS4rNoRuleComputations ELI_ARG((_TPPrNoRuleComputations _currn));
#define _VS5rNoRuleComputations _VS0Empty

#define _VS6rNoRuleComputations _VS0Empty

extern void _VS7rNoRuleComputations ELI_ARG((_TPPrNoRuleComputations _currn));
extern void _VS8rNoRuleComputations ELI_ARG((_TPPrNoRuleComputations _currn));
extern void _VS9rNoRuleComputations ELI_ARG((_TPPrNoRuleComputations _currn));
extern void _VS1rRuleComputeAssign ELI_ARG((_TPPrRuleComputeAssign _currn));
extern void _VS2rRuleComputeAssign ELI_ARG((_TPPrRuleComputeAssign _currn));
extern void _VS3rRuleComputeAssign ELI_ARG((_TPPrRuleComputeAssign _currn));
extern void _VS4rRuleComputeAssign ELI_ARG((_TPPrRuleComputeAssign _currn));
extern void _VS5rRuleComputeAssign ELI_ARG((_TPPrRuleComputeAssign _currn));
extern void _VS6rRuleComputeAssign ELI_ARG((_TPPrRuleComputeAssign _currn));
extern void _VS7rRuleComputeAssign ELI_ARG((_TPPrRuleComputeAssign _currn));
extern void _VS8rRuleComputeAssign ELI_ARG((_TPPrRuleComputeAssign _currn));
extern void _VS9rRuleComputeAssign ELI_ARG((_TPPrRuleComputeAssign _currn));
extern void _VS1rRuleComputeExpr ELI_ARG((_TPPrRuleComputeExpr _currn));
extern void _VS2rRuleComputeExpr ELI_ARG((_TPPrRuleComputeExpr _currn));
extern void _VS3rRuleComputeExpr ELI_ARG((_TPPrRuleComputeExpr _currn));
extern void _VS4rRuleComputeExpr ELI_ARG((_TPPrRuleComputeExpr _currn));
extern void _VS5rRuleComputeExpr ELI_ARG((_TPPrRuleComputeExpr _currn));
extern void _VS6rRuleComputeExpr ELI_ARG((_TPPrRuleComputeExpr _currn));
extern void _VS7rRuleComputeExpr ELI_ARG((_TPPrRuleComputeExpr _currn));
extern void _VS8rRuleComputeExpr ELI_ARG((_TPPrRuleComputeExpr _currn));
extern void _VS9rRuleComputeExpr ELI_ARG((_TPPrRuleComputeExpr _currn));
extern void _VS1rRuleComputeChainStart ELI_ARG((_TPPrRuleComputeChainStart _currn));
extern void _VS2rRuleComputeChainStart ELI_ARG((_TPPrRuleComputeChainStart _currn));
extern void _VS3rRuleComputeChainStart ELI_ARG((_TPPrRuleComputeChainStart _currn));
extern void _VS4rRuleComputeChainStart ELI_ARG((_TPPrRuleComputeChainStart _currn));
extern void _VS5rRuleComputeChainStart ELI_ARG((_TPPrRuleComputeChainStart _currn));
extern void _VS6rRuleComputeChainStart ELI_ARG((_TPPrRuleComputeChainStart _currn));
extern void _VS7rRuleComputeChainStart ELI_ARG((_TPPrRuleComputeChainStart _currn));
extern void _VS8rRuleComputeChainStart ELI_ARG((_TPPrRuleComputeChainStart _currn));
extern void _VS9rRuleComputeChainStart ELI_ARG((_TPPrRuleComputeChainStart _currn));
extern void _VS1rRuleComputeOrder ELI_ARG((_TPPrRuleComputeOrder _currn));
extern void _VS2rRuleComputeOrder ELI_ARG((_TPPrRuleComputeOrder _currn));
extern void _VS3rRuleComputeOrder ELI_ARG((_TPPrRuleComputeOrder _currn));
extern void _VS4rRuleComputeOrder ELI_ARG((_TPPrRuleComputeOrder _currn));
extern void _VS5rRuleComputeOrder ELI_ARG((_TPPrRuleComputeOrder _currn));
extern void _VS6rRuleComputeOrder ELI_ARG((_TPPrRuleComputeOrder _currn));
extern void _VS7rRuleComputeOrder ELI_ARG((_TPPrRuleComputeOrder _currn));
extern void _VS8rRuleComputeOrder ELI_ARG((_TPPrRuleComputeOrder _currn));
extern void _VS9rRuleComputeOrder ELI_ARG((_TPPrRuleComputeOrder _currn));
extern void _VS1rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS2rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS3rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS4rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS5rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS6rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS7rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS8rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS9rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS10rRuleChainStart ELI_ARG((_TPPrRuleChainStart _currn));
extern void _VS1rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
extern void _VS2rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
extern void _VS3rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
extern void _VS4rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
extern void _VS5rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
extern void _VS6rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
extern void _VS7rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
extern void _VS8rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
extern void _VS9rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
extern void _VS10rRuleDefOcc ELI_ARG((_TPPrRuleDefOcc _currn));
#define _VS1rNoRuleDependency _VS0Empty

#define _VS2rNoRuleDependency _VS0Empty

#define _VS3rNoRuleDependency _VS0Empty

#define _VS4rNoRuleDependency _VS0Empty

#define _VS5rNoRuleDependency _VS0Empty

#define _VS6rNoRuleDependency _VS0Empty

extern void _VS7rNoRuleDependency ELI_ARG((_TPPrNoRuleDependency _currn));
extern void _VS8rNoRuleDependency ELI_ARG((_TPPrNoRuleDependency _currn));
extern void _VS9rNoRuleDependency ELI_ARG((_TPPrNoRuleDependency _currn));
extern void _VS1rRuleDependencies ELI_ARG((_TPPrRuleDependencies _currn));
extern void _VS2rRuleDependencies ELI_ARG((_TPPrRuleDependencies _currn));
extern void _VS3rRuleDependencies ELI_ARG((_TPPrRuleDependencies _currn));
extern void _VS4rRuleDependencies ELI_ARG((_TPPrRuleDependencies _currn));
extern void _VS5rRuleDependencies ELI_ARG((_TPPrRuleDependencies _currn));
extern void _VS6rRuleDependencies ELI_ARG((_TPPrRuleDependencies _currn));
extern void _VS7rRuleDependencies ELI_ARG((_TPPrRuleDependencies _currn));
extern void _VS8rRuleDependencies ELI_ARG((_TPPrRuleDependencies _currn));
extern void _VS9rRuleDependencies ELI_ARG((_TPPrRuleDependencies _currn));
#define _VS1rRuleAttributeReferenceSingle _VS1rPTGDecl

#define _VS2rRuleAttributeReferenceSingle _VS2rPTGDecl

#define _VS3rRuleAttributeReferenceSingle _VS3rPTGDecl

#define _VS4rRuleAttributeReferenceSingle _VS4rPTGDecl

#define _VS5rRuleAttributeReferenceSingle _VS5rPTGDecl

extern void _VS6rRuleAttributeReferenceSingle ELI_ARG((_TPPrRuleAttributeReferenceSingle _currn));
extern void _VS7rRuleAttributeReferenceSingle ELI_ARG((_TPPrRuleAttributeReferenceSingle _currn));
extern void _VS8rRuleAttributeReferenceSingle ELI_ARG((_TPPrRuleAttributeReferenceSingle _currn));
extern void _VS9rRuleAttributeReferenceSingle ELI_ARG((_TPPrRuleAttributeReferenceSingle _currn));
#define _VS1rRuleAttributeReferenceMulti _VS1rPTGDecl

#define _VS2rRuleAttributeReferenceMulti _VS2rPTGDecl

#define _VS3rRuleAttributeReferenceMulti _VS3rPTGDecl

#define _VS4rRuleAttributeReferenceMulti _VS4rPTGDecl

#define _VS5rRuleAttributeReferenceMulti _VS5rPTGDecl

extern void _VS6rRuleAttributeReferenceMulti ELI_ARG((_TPPrRuleAttributeReferenceMulti _currn));
extern void _VS7rRuleAttributeReferenceMulti ELI_ARG((_TPPrRuleAttributeReferenceMulti _currn));
extern void _VS8rRuleAttributeReferenceMulti ELI_ARG((_TPPrRuleAttributeReferenceMulti _currn));
extern void _VS9rRuleAttributeReferenceMulti ELI_ARG((_TPPrRuleAttributeReferenceMulti _currn));
#define _VS1rRuleAttributeReferenceList2 _VS1rPTGDefList2

#define _VS2rRuleAttributeReferenceList2 _VS2rPTGDefList2

#define _VS3rRuleAttributeReferenceList2 _VS3rPTGDefList2

#define _VS4rRuleAttributeReferenceList2 _VS4rPTGDefList2

#define _VS5rRuleAttributeReferenceList2 _VS5rPTGDefList2

#define _VS6rRuleAttributeReferenceList2 _VS6rFunctionPatternList2

extern void _VS7rRuleAttributeReferenceList2 ELI_ARG((_TPPrRuleAttributeReferenceList2 _currn));
extern void _VS8rRuleAttributeReferenceList2 ELI_ARG((_TPPrRuleAttributeReferenceList2 _currn));
extern void _VS9rRuleAttributeReferenceList2 ELI_ARG((_TPPrRuleAttributeReferenceList2 _currn,D_typePtr* _AS0expected_type));
#define _VS1rRuleAttributeReferenceList1 _VS1rPTGDecl

#define _VS2rRuleAttributeReferenceList1 _VS2rPTGDecl

#define _VS3rRuleAttributeReferenceList1 _VS3rPTGDecl

#define _VS4rRuleAttributeReferenceList1 _VS4rPTGDecl

#define _VS5rRuleAttributeReferenceList1 _VS5rPTGDecl

extern void _VS6rRuleAttributeReferenceList1 ELI_ARG((_TPPrRuleAttributeReferenceList1 _currn));
extern void _VS7rRuleAttributeReferenceList1 ELI_ARG((_TPPrRuleAttributeReferenceList1 _currn));
extern void _VS8rRuleAttributeReferenceList1 ELI_ARG((_TPPrRuleAttributeReferenceList1 _currn));
extern void _VS9rRuleAttributeReferenceList1 ELI_ARG((_TPPrRuleAttributeReferenceList1 _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleAttributeReferenceIsSymbolAttrRef ELI_ARG((_TPPrRuleAttributeReferenceIsSymbolAttrRef _currn));
#define _VS2rRuleAttributeReferenceIsSymbolAttrRef _VS0Empty

extern void _VS3rRuleAttributeReferenceIsSymbolAttrRef ELI_ARG((_TPPrRuleAttributeReferenceIsSymbolAttrRef _currn));
#define _VS4rRuleAttributeReferenceIsSymbolAttrRef _VS0Empty

#define _VS5rRuleAttributeReferenceIsSymbolAttrRef _VS0Empty

#define _VS6rRuleAttributeReferenceIsSymbolAttrRef _VS0Empty

extern void _VS7rRuleAttributeReferenceIsSymbolAttrRef ELI_ARG((_TPPrRuleAttributeReferenceIsSymbolAttrRef _currn));
extern void _VS8rRuleAttributeReferenceIsSymbolAttrRef ELI_ARG((_TPPrRuleAttributeReferenceIsSymbolAttrRef _currn));
extern void _VS9rRuleAttributeReferenceIsSymbolAttrRef ELI_ARG((_TPPrRuleAttributeReferenceIsSymbolAttrRef _currn));
extern void _VS1rRuleAttributeReferenceIsRemote ELI_ARG((_TPPrRuleAttributeReferenceIsRemote _currn));
extern void _VS2rRuleAttributeReferenceIsRemote ELI_ARG((_TPPrRuleAttributeReferenceIsRemote _currn));
extern void _VS3rRuleAttributeReferenceIsRemote ELI_ARG((_TPPrRuleAttributeReferenceIsRemote _currn));
extern void _VS4rRuleAttributeReferenceIsRemote ELI_ARG((_TPPrRuleAttributeReferenceIsRemote _currn));
extern void _VS5rRuleAttributeReferenceIsRemote ELI_ARG((_TPPrRuleAttributeReferenceIsRemote _currn));
extern void _VS6rRuleAttributeReferenceIsRemote ELI_ARG((_TPPrRuleAttributeReferenceIsRemote _currn));
extern void _VS7rRuleAttributeReferenceIsRemote ELI_ARG((_TPPrRuleAttributeReferenceIsRemote _currn));
extern void _VS8rRuleAttributeReferenceIsRemote ELI_ARG((_TPPrRuleAttributeReferenceIsRemote _currn));
extern void _VS9rRuleAttributeReferenceIsRemote ELI_ARG((_TPPrRuleAttributeReferenceIsRemote _currn));
extern void _VS1rRuleSymbolAttrRefIsLidoStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoStyle _currn));
extern void _VS2rRuleSymbolAttrRefIsLidoStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoStyle _currn));
extern void _VS3rRuleSymbolAttrRefIsLidoStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoStyle _currn));
extern void _VS4rRuleSymbolAttrRefIsLidoStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoStyle _currn,D_RuleSymbolAttributes* _AS0rattr_deps));
extern void _VS5rRuleSymbolAttrRefIsLidoStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoStyle _currn,D_RuleSymbolAttributes* _AS0rattr_deps));
extern void _VS6rRuleSymbolAttrRefIsLidoStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoStyle _currn,D_RuleSymbolAttributes* _AS0rattr_deps,int* _AS0_const310,int* _AS0_const299,int* _AS0_const261));
extern void _VS7rRuleSymbolAttrRefIsLidoStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoStyle _currn,D_RuleSymbolAttributes* _AS0rattr_deps,StringTableKeyList* _AS0_const330,int* _AS0_const310,int* _AS0_const299,int* _AS0_const261));
extern void _VS1rRuleSymbolAttrRefIsLidoListOfStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoListOfStyle _currn));
extern void _VS2rRuleSymbolAttrRefIsLidoListOfStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoListOfStyle _currn));
extern void _VS3rRuleSymbolAttrRefIsLidoListOfStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoListOfStyle _currn));
extern void _VS4rRuleSymbolAttrRefIsLidoListOfStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoListOfStyle _currn,D_RuleSymbolAttributes* _AS0rattr_deps));
extern void _VS5rRuleSymbolAttrRefIsLidoListOfStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoListOfStyle _currn,D_RuleSymbolAttributes* _AS0rattr_deps));
extern void _VS6rRuleSymbolAttrRefIsLidoListOfStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoListOfStyle _currn,D_RuleSymbolAttributes* _AS0rattr_deps,int* _AS0_const310,int* _AS0_const299,int* _AS0_const261));
extern void _VS7rRuleSymbolAttrRefIsLidoListOfStyle ELI_ARG((_TPPrRuleSymbolAttrRefIsLidoListOfStyle _currn,D_RuleSymbolAttributes* _AS0rattr_deps,StringTableKeyList* _AS0_const330,int* _AS0_const310,int* _AS0_const299,int* _AS0_const261));
extern void _VS1rRuleSymbolAttrRefIsVarReference ELI_ARG((_TPPrRuleSymbolAttrRefIsVarReference _currn));
extern void _VS2rRuleSymbolAttrRefIsVarReference ELI_ARG((_TPPrRuleSymbolAttrRefIsVarReference _currn));
extern void _VS3rRuleSymbolAttrRefIsVarReference ELI_ARG((_TPPrRuleSymbolAttrRefIsVarReference _currn));
extern void _VS4rRuleSymbolAttrRefIsVarReference ELI_ARG((_TPPrRuleSymbolAttrRefIsVarReference _currn,D_RuleSymbolAttributes* _AS0rattr_deps));
extern void _VS5rRuleSymbolAttrRefIsVarReference ELI_ARG((_TPPrRuleSymbolAttrRefIsVarReference _currn,D_RuleSymbolAttributes* _AS0rattr_deps));
extern void _VS6rRuleSymbolAttrRefIsVarReference ELI_ARG((_TPPrRuleSymbolAttrRefIsVarReference _currn,D_RuleSymbolAttributes* _AS0rattr_deps,int* _AS0_const310,int* _AS0_const299,int* _AS0_const261));
extern void _VS7rRuleSymbolAttrRefIsVarReference ELI_ARG((_TPPrRuleSymbolAttrRefIsVarReference _currn,D_RuleSymbolAttributes* _AS0rattr_deps,StringTableKeyList* _AS0_const330,int* _AS0_const310,int* _AS0_const299,int* _AS0_const261));
extern void _VS1rRuleAttrClassIsTail ELI_ARG((_TPPrRuleAttrClassIsTail _currn));
extern void _VS1rRuleAttrClassIsHead ELI_ARG((_TPPrRuleAttrClassIsHead _currn));
extern void _VS1rRuleVariableReference ELI_ARG((_TPPrRuleVariableReference _currn));
extern void _VS2rRuleVariableReference ELI_ARG((_TPPrRuleVariableReference _currn));
extern void _VS1rNoSymbolIndex ELI_ARG((_TPPrNoSymbolIndex _currn));
extern void _VS1rSymbolIndex ELI_ARG((_TPPrSymbolIndex _currn));
extern void _VS1rSymbolAttributeReference ELI_ARG((_TPPrSymbolAttributeReference _currn));
extern void _VS1rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS2rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS3rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS4rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS5rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS6rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS7rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS8rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS9rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS10rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS11rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
extern void _VS12rRuleOrderedComputation ELI_ARG((_TPPrRuleOrderedComputation _currn));
#define _VS1rRuleOrderedStatementList2 _VS1rPTGDefList2

#define _VS2rRuleOrderedStatementList2 _VS2rPTGDefList2

#define _VS3rRuleOrderedStatementList2 _VS3rPTGDefList2

#define _VS4rRuleOrderedStatementList2 _VS4rPTGDefList2

#define _VS5rRuleOrderedStatementList2 _VS5rPTGDefList2

#define _VS6rRuleOrderedStatementList2 _VS6rFunctionPatternList2

extern void _VS7rRuleOrderedStatementList2 ELI_ARG((_TPPrRuleOrderedStatementList2 _currn));
extern void _VS8rRuleOrderedStatementList2 ELI_ARG((_TPPrRuleOrderedStatementList2 _currn));
extern void _VS9rRuleOrderedStatementList2 ELI_ARG((_TPPrRuleOrderedStatementList2 _currn,D_RuleSymbolAttributes* _AS0_const298,D_RuleComputations* _AS0_const288));
extern void _VS10rRuleOrderedStatementList2 ELI_ARG((_TPPrRuleOrderedStatementList2 _currn,D_RuleSymbolAttributes* _AS0_const298,D_RuleComputations* _AS0_const288,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleOrderedStatementList2 ELI_ARG((_TPPrRuleOrderedStatementList2 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0result_type,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const296,D_RuleComputations* _AS0_const288,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleOrderedStatementList2 ELI_ARG((_TPPrRuleOrderedStatementList2 _currn,PTGNodes* _AS0lido_codes,D_typePtr* _AS0expected_type,D_typePtr* _AS0result_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const296,D_RuleComputations* _AS0_const288,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
#define _VS1rRuleOrderedStatementList1 _VS1rPTGDecl

#define _VS2rRuleOrderedStatementList1 _VS2rPTGDecl

#define _VS3rRuleOrderedStatementList1 _VS3rPTGDecl

#define _VS4rRuleOrderedStatementList1 _VS4rPTGDecl

#define _VS5rRuleOrderedStatementList1 _VS5rPTGDecl

extern void _VS6rRuleOrderedStatementList1 ELI_ARG((_TPPrRuleOrderedStatementList1 _currn));
extern void _VS7rRuleOrderedStatementList1 ELI_ARG((_TPPrRuleOrderedStatementList1 _currn));
extern void _VS8rRuleOrderedStatementList1 ELI_ARG((_TPPrRuleOrderedStatementList1 _currn));
extern void _VS9rRuleOrderedStatementList1 ELI_ARG((_TPPrRuleOrderedStatementList1 _currn,D_RuleSymbolAttributes* _AS0_const298,D_RuleComputations* _AS0_const288));
extern void _VS10rRuleOrderedStatementList1 ELI_ARG((_TPPrRuleOrderedStatementList1 _currn,D_RuleSymbolAttributes* _AS0_const298,D_RuleComputations* _AS0_const288,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleOrderedStatementList1 ELI_ARG((_TPPrRuleOrderedStatementList1 _currn,D_typePtr* _AS0expected_type,D_typePtr* _AS0result_type,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const296,D_RuleComputations* _AS0_const288,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleOrderedStatementList1 ELI_ARG((_TPPrRuleOrderedStatementList1 _currn,PTGNodes* _AS0lido_codes,D_typePtr* _AS0expected_type,D_typePtr* _AS0result_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const296,D_RuleComputations* _AS0_const288,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS2rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS3rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS4rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS5rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS6rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS7rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS8rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS9rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS10rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS11rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn));
extern void _VS12rRuleOrderedStatementIsExpression ELI_ARG((_TPPrRuleOrderedStatementIsExpression _currn,D_RHSKind* _AS0lido_rhs,PTGNodes* _AS0last_attrs,D_RuleSymbolAttributes* _AS0last_rattrsIn,int* _AS0ordered_cnt_chn_pre));
extern void _VS1rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS2rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS3rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS4rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS5rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS6rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS7rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS8rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS9rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS10rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS11rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn));
extern void _VS12rRuleOrderedStatementIsAssign ELI_ARG((_TPPrRuleOrderedStatementIsAssign _currn,D_RHSKind* _AS0lido_rhs,PTGNodes* _AS0last_attrs,D_RuleSymbolAttributes* _AS0last_rattrsIn,int* _AS0ordered_cnt_chn_pre));
extern void _VS1rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS2rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS3rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS4rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS5rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS6rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS7rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS8rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS9rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS10rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS11rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn));
extern void _VS12rRuleOrderedStatementIsReturn ELI_ARG((_TPPrRuleOrderedStatementIsReturn _currn,D_RHSKind* _AS0lido_rhs,PTGNodes* _AS0last_attrs,D_RuleSymbolAttributes* _AS0last_rattrsIn,int* _AS0ordered_cnt_chn_pre));
extern void _VS1rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS2rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS3rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS4rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS5rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS6rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS7rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS8rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS9rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS10rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn));
extern void _VS11rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleRhsExprIsExpr ELI_ARG((_TPPrRuleRhsExprIsExpr _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS2rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS3rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS4rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS5rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS6rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS7rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS8rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS9rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS10rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn));
extern void _VS11rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleRhsExprIsGuards ELI_ARG((_TPPrRuleRhsExprIsGuards _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn));
extern void _VS2rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn));
extern void _VS3rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn));
extern void _VS4rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn));
extern void _VS5rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn));
extern void _VS6rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn));
extern void _VS7rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn));
extern void _VS8rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn));
extern void _VS9rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn,D_RuleExpressionPtr* _AS0rexpr_code,PTGNodes* _AS0gconds,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS13rRuleGuardExpressionList2 ELI_ARG((_TPPrRuleGuardExpressionList2 _currn,D_RuleExpressionPtr* _AS0rexpr_code,PTGNodes* _AS0gconds,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS1rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn));
extern void _VS2rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn));
extern void _VS3rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn));
extern void _VS4rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn));
extern void _VS5rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn));
extern void _VS6rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn));
extern void _VS7rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn));
extern void _VS8rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn));
extern void _VS9rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn,D_RuleExpressionPtr* _AS0rexpr_code,PTGNodes* _AS0gconds,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS13rRuleGuardExpressionList1 ELI_ARG((_TPPrRuleGuardExpressionList1 _currn,D_RuleExpressionPtr* _AS0rexpr_code,PTGNodes* _AS0gconds,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS1rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn));
extern void _VS2rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn));
extern void _VS3rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn));
extern void _VS4rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn));
extern void _VS5rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn));
extern void _VS6rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn));
extern void _VS7rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn));
extern void _VS8rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn));
extern void _VS9rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn,D_RuleExpressions* _AS0rguardsIn));
extern void _VS10rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn,D_RuleExpressions* _AS0rguardsIn));
extern void _VS11rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn,D_RuleExpressions* _AS0rguardsIn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn,D_RuleExpressions* _AS0rguardsIn,D_typePtr* _AS0expected_type));
extern void _VS13rRuleGuardIsOr ELI_ARG((_TPPrRuleGuardIsOr _currn,int* _AS0default_count,D_RuleExpressions* _AS0rguardsIn,D_typePtr* _AS0expected_type,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const297,D_OrderStatRs* _AS0_const287,int* _AS0_const286,D_VirtAttrs* _AS0_const274,StringTableKey* _AS0_const10));
extern void _VS1rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn));
extern void _VS2rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn));
extern void _VS3rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn));
extern void _VS4rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn));
extern void _VS5rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn));
extern void _VS6rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn));
extern void _VS7rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn));
extern void _VS8rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn));
extern void _VS9rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn,D_RuleExpressions* _AS0rguardsIn));
extern void _VS10rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn,D_RuleExpressions* _AS0rguardsIn));
extern void _VS11rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn,D_RuleExpressions* _AS0rguardsIn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn,D_RuleExpressions* _AS0rguardsIn,D_typePtr* _AS0expected_type));
extern void _VS13rRuleGuardIsCompute ELI_ARG((_TPPrRuleGuardIsCompute _currn,int* _AS0default_count,D_RuleExpressions* _AS0rguardsIn,D_typePtr* _AS0expected_type,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const297,D_OrderStatRs* _AS0_const287,int* _AS0_const286,D_VirtAttrs* _AS0_const274,StringTableKey* _AS0_const10));
extern void _VS1rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn));
extern void _VS2rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn));
extern void _VS3rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn));
extern void _VS4rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn));
extern void _VS5rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn));
extern void _VS6rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn));
extern void _VS7rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn));
extern void _VS8rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn));
extern void _VS9rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn,D_RuleExpressions* _AS0rguardsIn));
extern void _VS10rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn,D_RuleExpressions* _AS0rguardsIn));
extern void _VS11rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn,D_RuleExpressions* _AS0rguardsIn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn,D_RuleExpressions* _AS0rguardsIn,D_typePtr* _AS0expected_type));
extern void _VS13rRuleGuardIsDefault ELI_ARG((_TPPrRuleGuardIsDefault _currn,int* _AS0default_count,D_RuleExpressions* _AS0rguardsIn,D_typePtr* _AS0expected_type,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const297,D_OrderStatRs* _AS0_const287,int* _AS0_const286,D_VirtAttrs* _AS0_const274,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn));
extern void _VS2rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn));
extern void _VS3rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn));
extern void _VS4rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn));
extern void _VS5rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn));
extern void _VS6rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn));
extern void _VS7rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn));
extern void _VS8rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn));
extern void _VS9rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS11rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS12rRuleExpressionChain ELI_ARG((_TPPrRuleExpressionChain _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,Environment* _AS0lng_generated_defs_chn_post,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS1rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn));
extern void _VS2rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn));
extern void _VS3rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn));
extern void _VS4rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn));
extern void _VS5rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn));
extern void _VS6rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn));
extern void _VS7rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn));
extern void _VS8rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn));
extern void _VS9rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS11rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS12rRuleExpressionIsWhen ELI_ARG((_TPPrRuleExpressionIsWhen _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,Environment* _AS0lng_generated_defs_chn_post,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS1rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn));
extern void _VS2rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn));
extern void _VS3rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn));
extern void _VS4rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn));
extern void _VS5rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn));
extern void _VS6rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn));
extern void _VS7rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn));
extern void _VS8rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn));
extern void _VS9rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS11rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS12rRuleExpressionIsError ELI_ARG((_TPPrRuleExpressionIsError _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,Environment* _AS0lng_generated_defs_chn_post,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS1rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn));
extern void _VS2rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn));
extern void _VS3rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn));
extern void _VS4rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn));
extern void _VS5rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn));
extern void _VS6rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn));
extern void _VS7rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn));
extern void _VS8rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn));
extern void _VS9rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS11rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS12rRuleExpressionIsIf ELI_ARG((_TPPrRuleExpressionIsIf _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,Environment* _AS0lng_generated_defs_chn_post,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS1rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn));
extern void _VS2rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn));
extern void _VS3rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn));
extern void _VS4rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn));
extern void _VS5rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn));
extern void _VS6rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn));
extern void _VS7rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn));
extern void _VS8rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn));
extern void _VS9rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS11rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS12rRuleExpressionIsLet ELI_ARG((_TPPrRuleExpressionIsLet _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,Environment* _AS0lng_generated_defs_chn_post,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS1rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn));
extern void _VS2rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn));
extern void _VS3rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn));
extern void _VS4rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn));
extern void _VS5rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn));
extern void _VS6rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn));
extern void _VS7rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn));
extern void _VS8rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn));
extern void _VS9rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS11rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS12rRuleExpressionIsLambda ELI_ARG((_TPPrRuleExpressionIsLambda _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,Environment* _AS0lng_generated_defs_chn_post,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS1rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn));
extern void _VS2rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn));
extern void _VS3rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn));
extern void _VS4rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn));
extern void _VS5rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn));
extern void _VS6rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn));
extern void _VS7rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn));
extern void _VS8rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn));
extern void _VS9rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS11rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS12rRuleExpressionIsOrder ELI_ARG((_TPPrRuleExpressionIsOrder _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,Environment* _AS0lng_generated_defs_chn_post,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS1rRuleLambda ELI_ARG((_TPPrRuleLambda _currn));
extern void _VS2rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,int* _AS0arg_type_indx_chn_pre));
extern void _VS3rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,int* _AS0arg_type_indx_chn_pre));
extern void _VS4rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,int* _AS0arg_type_indx_chn_pre));
extern void _VS5rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,int* _AS0arg_type_indx_chn_pre));
extern void _VS6rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,int* _AS0arg_type_indx_chn_pre));
extern void _VS7rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,int* _AS0arg_type_indx_chn_pre,Environment* _AS0rule_var_chn_pre));
extern void _VS8rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,int* _AS0arg_type_indx_chn_pre,Environment* _AS0rule_var_chn_pre));
extern void _VS9rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,int* _AS0arg_type_indx_chn_pre,Environment* _AS0rule_var_chn_pre));
extern void _VS10rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,D_typePtr* _AS0expected_type,int* _AS0arg_type_indx_chn_pre,Environment* _AS0rule_var_chn_pre));
extern void _VS11rRuleLambda ELI_ARG((_TPPrRuleLambda _currn,D_typePtr* _AS0expected_type,int* _AS0arg_type_indx_chn_pre,Environment* _AS0rule_var_chn_pre,Environment* _AS0lng_generated_defs_chn_pre));
extern void _VS1rRuleLambdaVarDefList2 ELI_ARG((_TPPrRuleLambdaVarDefList2 _currn));
extern void _VS2rRuleLambdaVarDefList2 ELI_ARG((_TPPrRuleLambdaVarDefList2 _currn));
extern void _VS3rRuleLambdaVarDefList2 ELI_ARG((_TPPrRuleLambdaVarDefList2 _currn));
extern void _VS4rRuleLambdaVarDefList2 ELI_ARG((_TPPrRuleLambdaVarDefList2 _currn));
extern void _VS5rRuleLambdaVarDefList2 ELI_ARG((_TPPrRuleLambdaVarDefList2 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post));
extern void _VS6rRuleLambdaVarDefList2 ELI_ARG((_TPPrRuleLambdaVarDefList2 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,StringTableKey* _AS0_const10));
extern void _VS1rRuleLambdaVarDefList1 ELI_ARG((_TPPrRuleLambdaVarDefList1 _currn));
extern void _VS2rRuleLambdaVarDefList1 ELI_ARG((_TPPrRuleLambdaVarDefList1 _currn));
extern void _VS3rRuleLambdaVarDefList1 ELI_ARG((_TPPrRuleLambdaVarDefList1 _currn));
extern void _VS4rRuleLambdaVarDefList1 ELI_ARG((_TPPrRuleLambdaVarDefList1 _currn));
extern void _VS5rRuleLambdaVarDefList1 ELI_ARG((_TPPrRuleLambdaVarDefList1 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post));
extern void _VS6rRuleLambdaVarDefList1 ELI_ARG((_TPPrRuleLambdaVarDefList1 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,StringTableKey* _AS0_const10));
extern void _VS1rRuleLambdaVarDef ELI_ARG((_TPPrRuleLambdaVarDef _currn));
#define _VS2rRuleLambdaVarDef _VS2rPTGCall

extern void _VS3rRuleLambdaVarDef ELI_ARG((_TPPrRuleLambdaVarDef _currn));
#define _VS4rRuleLambdaVarDef _VS5rPTGCall

extern void _VS5rRuleLambdaVarDef ELI_ARG((_TPPrRuleLambdaVarDef _currn));
extern void _VS6rRuleLambdaVarDef ELI_ARG((_TPPrRuleLambdaVarDef _currn));
extern void _VS1rRuleVariableDefId ELI_ARG((_TPPrRuleVariableDefId _currn));
extern void _VS2rRuleVariableDefId ELI_ARG((_TPPrRuleVariableDefId _currn));
extern void _VS3rRuleVariableDefId ELI_ARG((_TPPrRuleVariableDefId _currn));
extern void _VS1rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn));
extern void _VS2rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn));
extern void _VS3rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn));
extern void _VS4rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn));
extern void _VS5rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn));
extern void _VS6rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn));
extern void _VS7rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn));
extern void _VS8rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn));
extern void _VS9rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn));
extern void _VS10rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn,D_typePtr* _AS0expected_type));
extern void _VS11rRuleExpressionLet ELI_ARG((_TPPrRuleExpressionLet _currn,D_typePtr* _AS0expected_type));
#define _VS1rRuleLetVarDefList2 _VS1rPTGDefList2

#define _VS2rRuleLetVarDefList2 _VS2rPTGDefList2

#define _VS3rRuleLetVarDefList2 _VS3rPTGDefList2

#define _VS4rRuleLetVarDefList2 _VS4rPTGDefList2

#define _VS5rRuleLetVarDefList2 _VS5rPTGDefList2

#define _VS6rRuleLetVarDefList2 _VS6rFunctionPatternList2

extern void _VS7rRuleLetVarDefList2 ELI_ARG((_TPPrRuleLetVarDefList2 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post));
extern void _VS8rRuleLetVarDefList2 ELI_ARG((_TPPrRuleLetVarDefList2 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,D_RuleLetvarDefs* _AS0_const308,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS9rRuleLetVarDefList2 ELI_ARG((_TPPrRuleLetVarDefList2 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,D_RuleLetvarDefs* _AS0_const308,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS10rRuleLetVarDefList2 ELI_ARG((_TPPrRuleLetVarDefList2 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,D_types* _AS0_const336,int* _AS0_const310,D_RuleLetvarDefs* _AS0_const308,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS11rRuleLetVarDefList2 ELI_ARG((_TPPrRuleLetVarDefList2 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleLetvarDefs* _AS0_const308,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
#define _VS1rRuleLetVarDefList1 _VS1rPTGDecl

#define _VS2rRuleLetVarDefList1 _VS2rPTGDecl

#define _VS3rRuleLetVarDefList1 _VS3rPTGDecl

#define _VS4rRuleLetVarDefList1 _VS4rPTGDecl

#define _VS5rRuleLetVarDefList1 _VS5rPTGDecl

extern void _VS6rRuleLetVarDefList1 ELI_ARG((_TPPrRuleLetVarDefList1 _currn));
extern void _VS7rRuleLetVarDefList1 ELI_ARG((_TPPrRuleLetVarDefList1 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post));
extern void _VS8rRuleLetVarDefList1 ELI_ARG((_TPPrRuleLetVarDefList1 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,D_RuleLetvarDefs* _AS0_const308,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS9rRuleLetVarDefList1 ELI_ARG((_TPPrRuleLetVarDefList1 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,D_RuleLetvarDefs* _AS0_const308,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS10rRuleLetVarDefList1 ELI_ARG((_TPPrRuleLetVarDefList1 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,D_types* _AS0_const336,int* _AS0_const310,D_RuleLetvarDefs* _AS0_const308,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS11rRuleLetVarDefList1 ELI_ARG((_TPPrRuleLetVarDefList1 _currn,int* _AS0rule_var_idx_post,int* _AS0rule_subvar_idx_post,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleLetvarDefs* _AS0_const308,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
#define _VS1rRuleLetVarDef _VS1rFunctionLetVarDef

#define _VS2rRuleLetVarDef _VS2rFunctionLetVarDef

extern void _VS3rRuleLetVarDef ELI_ARG((_TPPrRuleLetVarDef _currn));
extern void _VS4rRuleLetVarDef ELI_ARG((_TPPrRuleLetVarDef _currn));
extern void _VS5rRuleLetVarDef ELI_ARG((_TPPrRuleLetVarDef _currn));
extern void _VS6rRuleLetVarDef ELI_ARG((_TPPrRuleLetVarDef _currn));
extern void _VS7rRuleLetVarDef ELI_ARG((_TPPrRuleLetVarDef _currn));
extern void _VS8rRuleLetVarDef ELI_ARG((_TPPrRuleLetVarDef _currn));
extern void _VS9rRuleLetVarDef ELI_ARG((_TPPrRuleLetVarDef _currn));
extern void _VS10rRuleLetVarDef ELI_ARG((_TPPrRuleLetVarDef _currn));
extern void _VS11rRuleLetVarDef ELI_ARG((_TPPrRuleLetVarDef _currn));
extern void _VS1rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS2rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS3rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS4rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS5rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS6rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS7rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS8rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS9rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS10rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn));
extern void _VS11rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionIf ELI_ARG((_TPPrRuleExpressionIf _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS2rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS3rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS4rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS5rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS6rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS7rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS8rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS9rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS10rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS11rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS12rRuleExpressionError ELI_ARG((_TPPrRuleExpressionError _currn));
extern void _VS1rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS2rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS3rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS4rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS5rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS6rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS7rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS8rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS9rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS10rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS11rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS12rRuleExpressionWhenCond ELI_ARG((_TPPrRuleExpressionWhenCond _currn));
extern void _VS1rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS2rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS3rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS4rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS5rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS6rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS7rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS8rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS9rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS10rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS11rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS12rRuleExpressionWhen ELI_ARG((_TPPrRuleExpressionWhen _currn));
extern void _VS1rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn));
extern void _VS2rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn));
extern void _VS3rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn));
extern void _VS4rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn));
extern void _VS5rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn));
extern void _VS6rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn));
extern void _VS7rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn));
extern void _VS8rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn));
extern void _VS9rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS11rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS12rRuleExpressionIsBinary ELI_ARG((_TPPrRuleExpressionIsBinary _currn,int* _AS0index_num,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0rattr_deps,Environment* _AS0lng_generated_defs_chn_post,D_types* _AS0_const336,int* _AS0_const310,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const261));
extern void _VS1rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn));
extern void _VS2rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn));
extern void _VS3rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn));
extern void _VS4rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn));
extern void _VS5rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn));
extern void _VS6rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn));
extern void _VS7rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn));
extern void _VS8rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn));
extern void _VS9rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryChain ELI_ARG((_TPPrRuleExpressionBinaryChain _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn));
extern void _VS2rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn));
extern void _VS3rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn));
extern void _VS4rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn));
extern void _VS5rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn));
extern void _VS6rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn));
extern void _VS7rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn));
extern void _VS8rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn));
extern void _VS9rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryOR ELI_ARG((_TPPrRuleExpressionBinaryOR _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn));
extern void _VS2rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn));
extern void _VS3rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn));
extern void _VS4rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn));
extern void _VS5rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn));
extern void _VS6rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn));
extern void _VS7rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn));
extern void _VS8rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn));
extern void _VS9rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryAND ELI_ARG((_TPPrRuleExpressionBinaryAND _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn));
extern void _VS2rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn));
extern void _VS3rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn));
extern void _VS4rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn));
extern void _VS5rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn));
extern void _VS6rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn));
extern void _VS7rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn));
extern void _VS8rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn));
extern void _VS9rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryEQ ELI_ARG((_TPPrRuleExpressionBinaryEQ _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn));
extern void _VS2rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn));
extern void _VS3rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn));
extern void _VS4rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn));
extern void _VS5rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn));
extern void _VS6rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn));
extern void _VS7rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn));
extern void _VS8rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn));
extern void _VS9rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryNE ELI_ARG((_TPPrRuleExpressionBinaryNE _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn));
extern void _VS2rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn));
extern void _VS3rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn));
extern void _VS4rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn));
extern void _VS5rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn));
extern void _VS6rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn));
extern void _VS7rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn));
extern void _VS8rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn));
extern void _VS9rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryLT ELI_ARG((_TPPrRuleExpressionBinaryLT _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn));
extern void _VS2rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn));
extern void _VS3rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn));
extern void _VS4rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn));
extern void _VS5rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn));
extern void _VS6rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn));
extern void _VS7rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn));
extern void _VS8rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn));
extern void _VS9rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryGT ELI_ARG((_TPPrRuleExpressionBinaryGT _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn));
extern void _VS2rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn));
extern void _VS3rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn));
extern void _VS4rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn));
extern void _VS5rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn));
extern void _VS6rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn));
extern void _VS7rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn));
extern void _VS8rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn));
extern void _VS9rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryLE ELI_ARG((_TPPrRuleExpressionBinaryLE _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn));
extern void _VS2rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn));
extern void _VS3rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn));
extern void _VS4rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn));
extern void _VS5rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn));
extern void _VS6rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn));
extern void _VS7rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn));
extern void _VS8rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn));
extern void _VS9rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryGE ELI_ARG((_TPPrRuleExpressionBinaryGE _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn));
extern void _VS2rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn));
extern void _VS3rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn));
extern void _VS4rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn));
extern void _VS5rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn));
extern void _VS6rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn));
extern void _VS7rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn));
extern void _VS8rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn));
extern void _VS9rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryConcat ELI_ARG((_TPPrRuleExpressionBinaryConcat _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn));
extern void _VS2rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn));
extern void _VS3rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn));
extern void _VS4rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn));
extern void _VS5rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn));
extern void _VS6rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn));
extern void _VS7rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn));
extern void _VS8rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn));
extern void _VS9rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryADD ELI_ARG((_TPPrRuleExpressionBinaryADD _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn));
extern void _VS2rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn));
extern void _VS3rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn));
extern void _VS4rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn));
extern void _VS5rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn));
extern void _VS6rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn));
extern void _VS7rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn));
extern void _VS8rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn));
extern void _VS9rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinarySUB ELI_ARG((_TPPrRuleExpressionBinarySUB _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn));
extern void _VS2rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn));
extern void _VS3rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn));
extern void _VS4rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn));
extern void _VS5rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn));
extern void _VS6rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn));
extern void _VS7rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn));
extern void _VS8rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn));
extern void _VS9rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryMUL ELI_ARG((_TPPrRuleExpressionBinaryMUL _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn));
extern void _VS2rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn));
extern void _VS3rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn));
extern void _VS4rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn));
extern void _VS5rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn));
extern void _VS6rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn));
extern void _VS7rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn));
extern void _VS8rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn));
extern void _VS9rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryDIV ELI_ARG((_TPPrRuleExpressionBinaryDIV _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn));
extern void _VS2rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn));
extern void _VS3rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn));
extern void _VS4rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn));
extern void _VS5rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn));
extern void _VS6rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn));
extern void _VS7rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn));
extern void _VS8rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn));
extern void _VS9rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryMOD ELI_ARG((_TPPrRuleExpressionBinaryMOD _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn));
extern void _VS2rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn));
extern void _VS3rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn));
extern void _VS4rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn));
extern void _VS5rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn));
extern void _VS6rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn));
extern void _VS7rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn));
extern void _VS8rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn));
extern void _VS9rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionBinaryIsUnary ELI_ARG((_TPPrRuleExpressionBinaryIsUnary _currn,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const299,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS2rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS3rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS4rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS5rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS6rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS7rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS8rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS9rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS10rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn));
extern void _VS11rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionUnaryIsPostfix ELI_ARG((_TPPrRuleExpressionUnaryIsPostfix _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS2rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS3rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS4rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS5rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS6rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS7rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS8rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS9rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS10rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn));
extern void _VS11rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionUnaryIncr ELI_ARG((_TPPrRuleExpressionUnaryIncr _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS2rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS3rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS4rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS5rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS6rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS7rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS8rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS9rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS10rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn));
extern void _VS11rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionUnaryNEG ELI_ARG((_TPPrRuleExpressionUnaryNEG _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS2rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS3rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS4rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS5rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS6rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS7rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS8rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS9rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS10rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn));
extern void _VS11rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionUnaryNOT ELI_ARG((_TPPrRuleExpressionUnaryNOT _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS2rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS3rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS4rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS5rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS6rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS7rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS8rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS9rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS10rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn));
extern void _VS11rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionUnaryAddress ELI_ARG((_TPPrRuleExpressionUnaryAddress _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS2rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS3rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS4rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS5rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS6rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS7rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS8rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS9rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS10rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn));
extern void _VS11rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionUnaryReference ELI_ARG((_TPPrRuleExpressionUnaryReference _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn));
extern void _VS2rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn));
extern void _VS3rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn));
extern void _VS4rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn));
extern void _VS5rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn));
extern void _VS6rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn));
extern void _VS7rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn));
extern void _VS8rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn));
extern void _VS9rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionPostfixIsPrimary ELI_ARG((_TPPrRuleExpressionPostfixIsPrimary _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn));
extern void _VS2rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn));
extern void _VS3rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn));
extern void _VS4rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn));
extern void _VS5rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn));
extern void _VS6rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn));
extern void _VS7rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn));
extern void _VS8rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn));
extern void _VS9rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionPostfixIsIndex ELI_ARG((_TPPrRuleExpressionPostfixIsIndex _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn));
extern void _VS2rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn));
extern void _VS3rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn));
extern void _VS4rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn));
extern void _VS5rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn));
extern void _VS6rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn));
extern void _VS7rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn));
extern void _VS8rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn));
extern void _VS9rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionPostfixIsAccess ELI_ARG((_TPPrRuleExpressionPostfixIsAccess _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn));
extern void _VS2rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn));
extern void _VS3rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn));
extern void _VS4rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn));
extern void _VS5rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn));
extern void _VS6rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn));
extern void _VS7rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn));
extern void _VS8rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn));
extern void _VS9rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,D_types* _AS0_const336,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleExpressionPostfixIsListcon ELI_ARG((_TPPrRuleExpressionPostfixIsListcon _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_typePtr* _AS0expected_type,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
#define _VS1rRuleExpressionPrimaryIsConstant _VS0Empty

#define _VS2rRuleExpressionPrimaryIsConstant _VS0Empty

#define _VS3rRuleExpressionPrimaryIsConstant _VS0Empty

#define _VS4rRuleExpressionPrimaryIsConstant _VS0Empty

#define _VS5rRuleExpressionPrimaryIsConstant _VS0Empty

extern void _VS6rRuleExpressionPrimaryIsConstant ELI_ARG((_TPPrRuleExpressionPrimaryIsConstant _currn));
#define _VS7rRuleExpressionPrimaryIsConstant _VS0Empty

extern void _VS8rRuleExpressionPrimaryIsConstant ELI_ARG((_TPPrRuleExpressionPrimaryIsConstant _currn));
extern void _VS9rRuleExpressionPrimaryIsConstant ELI_ARG((_TPPrRuleExpressionPrimaryIsConstant _currn));
extern void _VS10rRuleExpressionPrimaryIsConstant ELI_ARG((_TPPrRuleExpressionPrimaryIsConstant _currn));
extern void _VS11rRuleExpressionPrimaryIsConstant ELI_ARG((_TPPrRuleExpressionPrimaryIsConstant _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionPrimaryIsConstant ELI_ARG((_TPPrRuleExpressionPrimaryIsConstant _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS2rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS3rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS4rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS5rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS6rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS7rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS8rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS9rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS10rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn));
extern void _VS11rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionPrimaryIsCall ELI_ARG((_TPPrRuleExpressionPrimaryIsCall _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS2rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS3rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS4rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS5rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS6rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS7rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS8rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS9rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS10rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn));
extern void _VS11rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionPrimaryIsTuple ELI_ARG((_TPPrRuleExpressionPrimaryIsTuple _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS2rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS3rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS4rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS5rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS6rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS7rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS8rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS9rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS10rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn));
extern void _VS11rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionPrimaryIsRemoteAttribute ELI_ARG((_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS2rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS3rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS4rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS5rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS6rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS7rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS8rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS9rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS10rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn));
extern void _VS11rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionPrimaryIsWrap ELI_ARG((_TPPrRuleExpressionPrimaryIsWrap _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS2rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS3rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS4rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS5rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS6rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS7rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS8rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS9rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS10rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS11rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
extern void _VS12rRuleTupleConstruction ELI_ARG((_TPPrRuleTupleConstruction _currn));
#define _VS1rRuleTupleArgumentList2 _VS1rPTGDefList2

#define _VS2rRuleTupleArgumentList2 _VS2rPTGDefList2

#define _VS3rRuleTupleArgumentList2 _VS3rPTGDefList2

#define _VS4rRuleTupleArgumentList2 _VS4rPTGDefList2

#define _VS5rRuleTupleArgumentList2 _VS5rPTGDefList2

#define _VS6rRuleTupleArgumentList2 _VS6rFunctionPatternList2

extern void _VS7rRuleTupleArgumentList2 ELI_ARG((_TPPrRuleTupleArgumentList2 _currn));
extern void _VS8rRuleTupleArgumentList2 ELI_ARG((_TPPrRuleTupleArgumentList2 _currn));
extern void _VS9rRuleTupleArgumentList2 ELI_ARG((_TPPrRuleTupleArgumentList2 _currn,D_RuleExpressions* _AS0_const313,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleTupleArgumentList2 ELI_ARG((_TPPrRuleTupleArgumentList2 _currn,D_RuleExpressions* _AS0_const313,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleTupleArgumentList2 ELI_ARG((_TPPrRuleTupleArgumentList2 _currn,D_types* _AS0_const336,D_RuleExpressions* _AS0_const313,D_types* _AS0_const312,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleTupleArgumentList2 ELI_ARG((_TPPrRuleTupleArgumentList2 _currn,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,D_RuleExpressions* _AS0_const313,D_types* _AS0_const312,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
#define _VS1rRuleTupleArgumentList1 _VS1rPTGDecl

#define _VS2rRuleTupleArgumentList1 _VS2rPTGDecl

#define _VS3rRuleTupleArgumentList1 _VS3rPTGDecl

#define _VS4rRuleTupleArgumentList1 _VS4rPTGDecl

#define _VS5rRuleTupleArgumentList1 _VS5rPTGDecl

extern void _VS6rRuleTupleArgumentList1 ELI_ARG((_TPPrRuleTupleArgumentList1 _currn));
extern void _VS7rRuleTupleArgumentList1 ELI_ARG((_TPPrRuleTupleArgumentList1 _currn));
extern void _VS8rRuleTupleArgumentList1 ELI_ARG((_TPPrRuleTupleArgumentList1 _currn));
extern void _VS9rRuleTupleArgumentList1 ELI_ARG((_TPPrRuleTupleArgumentList1 _currn,D_RuleExpressions* _AS0_const313,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleTupleArgumentList1 ELI_ARG((_TPPrRuleTupleArgumentList1 _currn,D_RuleExpressions* _AS0_const313,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleTupleArgumentList1 ELI_ARG((_TPPrRuleTupleArgumentList1 _currn,D_types* _AS0_const336,D_RuleExpressions* _AS0_const313,D_types* _AS0_const312,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleTupleArgumentList1 ELI_ARG((_TPPrRuleTupleArgumentList1 _currn,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,D_RuleExpressions* _AS0_const313,D_types* _AS0_const312,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
#define _VS1rRuleTupleArgument _VS1rPTGDecl

#define _VS2rRuleTupleArgument _VS2rPTGDecl

#define _VS3rRuleTupleArgument _VS3rPTGDecl

extern void _VS4rRuleTupleArgument ELI_ARG((_TPPrRuleTupleArgument _currn));
#define _VS5rRuleTupleArgument _VS5rPTGDecl

extern void _VS6rRuleTupleArgument ELI_ARG((_TPPrRuleTupleArgument _currn));
extern void _VS7rRuleTupleArgument ELI_ARG((_TPPrRuleTupleArgument _currn));
extern void _VS8rRuleTupleArgument ELI_ARG((_TPPrRuleTupleArgument _currn));
extern void _VS9rRuleTupleArgument ELI_ARG((_TPPrRuleTupleArgument _currn));
extern void _VS10rRuleTupleArgument ELI_ARG((_TPPrRuleTupleArgument _currn));
extern void _VS11rRuleTupleArgument ELI_ARG((_TPPrRuleTupleArgument _currn));
extern void _VS12rRuleTupleArgument ELI_ARG((_TPPrRuleTupleArgument _currn));
extern void _VS1rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS2rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS3rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS4rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS5rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS6rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS7rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS8rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS9rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS10rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn));
extern void _VS11rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionCallApplied ELI_ARG((_TPPrRuleExpressionCallApplied _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionCallEmpty ELI_ARG((_TPPrRuleExpressionCallEmpty _currn));
#define _VS2rRuleExpressionCallEmpty _VS0Empty

extern void _VS3rRuleExpressionCallEmpty ELI_ARG((_TPPrRuleExpressionCallEmpty _currn));
#define _VS4rRuleExpressionCallEmpty _VS0Empty

#define _VS5rRuleExpressionCallEmpty _VS0Empty

#define _VS6rRuleExpressionCallEmpty _VS0Empty

extern void _VS7rRuleExpressionCallEmpty ELI_ARG((_TPPrRuleExpressionCallEmpty _currn));
extern void _VS8rRuleExpressionCallEmpty ELI_ARG((_TPPrRuleExpressionCallEmpty _currn));
extern void _VS9rRuleExpressionCallEmpty ELI_ARG((_TPPrRuleExpressionCallEmpty _currn));
extern void _VS10rRuleExpressionCallEmpty ELI_ARG((_TPPrRuleExpressionCallEmpty _currn));
extern void _VS11rRuleExpressionCallEmpty ELI_ARG((_TPPrRuleExpressionCallEmpty _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionCallEmpty ELI_ARG((_TPPrRuleExpressionCallEmpty _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleExpressionCallVariable ELI_ARG((_TPPrRuleExpressionCallVariable _currn));
#define _VS2rRuleExpressionCallVariable _VS0Empty

extern void _VS3rRuleExpressionCallVariable ELI_ARG((_TPPrRuleExpressionCallVariable _currn));
#define _VS4rRuleExpressionCallVariable _VS0Empty

#define _VS5rRuleExpressionCallVariable _VS0Empty

#define _VS6rRuleExpressionCallVariable _VS0Empty

extern void _VS7rRuleExpressionCallVariable ELI_ARG((_TPPrRuleExpressionCallVariable _currn));
extern void _VS8rRuleExpressionCallVariable ELI_ARG((_TPPrRuleExpressionCallVariable _currn));
extern void _VS9rRuleExpressionCallVariable ELI_ARG((_TPPrRuleExpressionCallVariable _currn));
extern void _VS10rRuleExpressionCallVariable ELI_ARG((_TPPrRuleExpressionCallVariable _currn));
extern void _VS11rRuleExpressionCallVariable ELI_ARG((_TPPrRuleExpressionCallVariable _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleExpressionCallVariable ELI_ARG((_TPPrRuleExpressionCallVariable _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleCallableReferenceIsIdentifier ELI_ARG((_TPPrRuleCallableReferenceIsIdentifier _currn));
extern void _VS2rRuleCallableReferenceIsIdentifier ELI_ARG((_TPPrRuleCallableReferenceIsIdentifier _currn));
extern void _VS3rRuleCallableReferenceIsIdentifier ELI_ARG((_TPPrRuleCallableReferenceIsIdentifier _currn));
#define _VS4rRuleCallableReferenceIsIdentifier _VS0Empty

extern void _VS5rRuleCallableReferenceIsIdentifier ELI_ARG((_TPPrRuleCallableReferenceIsIdentifier _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS6rRuleCallableReferenceIsIdentifier ELI_ARG((_TPPrRuleCallableReferenceIsIdentifier _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS7rRuleCallableReferenceIsIdentifier ELI_ARG((_TPPrRuleCallableReferenceIsIdentifier _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS8rRuleCallableReferenceIsIdentifier ELI_ARG((_TPPrRuleCallableReferenceIsIdentifier _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261));
extern void _VS9rRuleCallableReferenceIsIdentifier ELI_ARG((_TPPrRuleCallableReferenceIsIdentifier _currn,int* _AS0nonattr_call_count,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleCallableReferenceIsTypename ELI_ARG((_TPPrRuleCallableReferenceIsTypename _currn));
#define _VS2rRuleCallableReferenceIsTypename _VS0Empty

extern void _VS3rRuleCallableReferenceIsTypename ELI_ARG((_TPPrRuleCallableReferenceIsTypename _currn));
#define _VS4rRuleCallableReferenceIsTypename _VS0Empty

extern void _VS5rRuleCallableReferenceIsTypename ELI_ARG((_TPPrRuleCallableReferenceIsTypename _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS6rRuleCallableReferenceIsTypename ELI_ARG((_TPPrRuleCallableReferenceIsTypename _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS7rRuleCallableReferenceIsTypename ELI_ARG((_TPPrRuleCallableReferenceIsTypename _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS8rRuleCallableReferenceIsTypename ELI_ARG((_TPPrRuleCallableReferenceIsTypename _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261));
extern void _VS9rRuleCallableReferenceIsTypename ELI_ARG((_TPPrRuleCallableReferenceIsTypename _currn,int* _AS0nonattr_call_count,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleCallableReferenceIsEnd ELI_ARG((_TPPrRuleCallableReferenceIsEnd _currn));
#define _VS2rRuleCallableReferenceIsEnd _VS0Empty

extern void _VS3rRuleCallableReferenceIsEnd ELI_ARG((_TPPrRuleCallableReferenceIsEnd _currn));
#define _VS4rRuleCallableReferenceIsEnd _VS0Empty

extern void _VS5rRuleCallableReferenceIsEnd ELI_ARG((_TPPrRuleCallableReferenceIsEnd _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS6rRuleCallableReferenceIsEnd ELI_ARG((_TPPrRuleCallableReferenceIsEnd _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS7rRuleCallableReferenceIsEnd ELI_ARG((_TPPrRuleCallableReferenceIsEnd _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS8rRuleCallableReferenceIsEnd ELI_ARG((_TPPrRuleCallableReferenceIsEnd _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261));
extern void _VS9rRuleCallableReferenceIsEnd ELI_ARG((_TPPrRuleCallableReferenceIsEnd _currn,int* _AS0nonattr_call_count,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleCallableReferenceIsAttribute ELI_ARG((_TPPrRuleCallableReferenceIsAttribute _currn));
extern void _VS2rRuleCallableReferenceIsAttribute ELI_ARG((_TPPrRuleCallableReferenceIsAttribute _currn));
extern void _VS3rRuleCallableReferenceIsAttribute ELI_ARG((_TPPrRuleCallableReferenceIsAttribute _currn));
extern void _VS4rRuleCallableReferenceIsAttribute ELI_ARG((_TPPrRuleCallableReferenceIsAttribute _currn));
extern void _VS5rRuleCallableReferenceIsAttribute ELI_ARG((_TPPrRuleCallableReferenceIsAttribute _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS6rRuleCallableReferenceIsAttribute ELI_ARG((_TPPrRuleCallableReferenceIsAttribute _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS7rRuleCallableReferenceIsAttribute ELI_ARG((_TPPrRuleCallableReferenceIsAttribute _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS8rRuleCallableReferenceIsAttribute ELI_ARG((_TPPrRuleCallableReferenceIsAttribute _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261));
extern void _VS9rRuleCallableReferenceIsAttribute ELI_ARG((_TPPrRuleCallableReferenceIsAttribute _currn,int* _AS0nonattr_call_count,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleCallableReferenceIsRule ELI_ARG((_TPPrRuleCallableReferenceIsRule _currn));
extern void _VS2rRuleCallableReferenceIsRule ELI_ARG((_TPPrRuleCallableReferenceIsRule _currn));
extern void _VS3rRuleCallableReferenceIsRule ELI_ARG((_TPPrRuleCallableReferenceIsRule _currn));
extern void _VS4rRuleCallableReferenceIsRule ELI_ARG((_TPPrRuleCallableReferenceIsRule _currn));
extern void _VS5rRuleCallableReferenceIsRule ELI_ARG((_TPPrRuleCallableReferenceIsRule _currn,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS6rRuleCallableReferenceIsRule ELI_ARG((_TPPrRuleCallableReferenceIsRule _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS7rRuleCallableReferenceIsRule ELI_ARG((_TPPrRuleCallableReferenceIsRule _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282));
extern void _VS8rRuleCallableReferenceIsRule ELI_ARG((_TPPrRuleCallableReferenceIsRule _currn,int* _AS0nonattr_call_count,D_RuleExpressionPtr* _AS0rexpr_code,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261));
extern void _VS9rRuleCallableReferenceIsRule ELI_ARG((_TPPrRuleCallableReferenceIsRule _currn,int* _AS0nonattr_call_count,PTGNodes* _AS0lido_codes,D_RuleExpressionPtr* _AS0rexpr_code,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn));
extern void _VS2rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn));
extern void _VS3rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn));
extern void _VS4rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn));
extern void _VS5rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn));
extern void _VS6rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn));
extern void _VS7rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn));
extern void _VS8rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn));
extern void _VS9rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn,D_RuleExpressions* _AS0_const317,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn,D_RuleExpressions* _AS0_const317,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn,D_types* _AS0_const336,D_RuleExpressions* _AS0_const317,D_types* _AS0_const315,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleCallParameterList2 ELI_ARG((_TPPrRuleCallParameterList2 _currn,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,D_RuleExpressions* _AS0_const317,D_types* _AS0_const315,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn));
extern void _VS2rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn));
extern void _VS3rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn));
extern void _VS4rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn));
extern void _VS5rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn));
extern void _VS6rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn));
extern void _VS7rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn));
extern void _VS8rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn));
extern void _VS9rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn,D_RuleExpressions* _AS0_const317,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS10rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn,D_RuleExpressions* _AS0_const317,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS11rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn,D_types* _AS0_const336,D_RuleExpressions* _AS0_const317,D_types* _AS0_const315,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS12rRuleCallParameterList1 ELI_ARG((_TPPrRuleCallParameterList1 _currn,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,D_RuleExpressions* _AS0_const317,D_types* _AS0_const315,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS2rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS3rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS4rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS5rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS6rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS7rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS8rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS9rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS10rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn));
extern void _VS11rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn,D_typePtr* _AS0expected_type));
extern void _VS12rRuleCallParameter ELI_ARG((_TPPrRuleCallParameter _currn,D_typePtr* _AS0expected_type));
extern void _VS1rRuleConstant ELI_ARG((_TPPrRuleConstant _currn));
extern void _VS2rRuleConstant ELI_ARG((_TPPrRuleConstant _currn));
extern void _VS3rRuleConstant ELI_ARG((_TPPrRuleConstant _currn));
extern void _VS4rRuleConstant ELI_ARG((_TPPrRuleConstant _currn));
extern void _VS1rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
#define _VS2rRemoteUp _VS0Empty

extern void _VS3rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
extern void _VS4rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
#define _VS5rRemoteUp _VS0Empty

extern void _VS6rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
extern void _VS7rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
extern void _VS8rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
extern void _VS9rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
extern void _VS10rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
extern void _VS11rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
extern void _VS12rRemoteUp ELI_ARG((_TPPrRemoteUp _currn));
extern void _VS1rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS2rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS3rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS4rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS5rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS6rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS7rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS8rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS9rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS10rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS11rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS12rRemoteDown ELI_ARG((_TPPrRemoteDown _currn));
extern void _VS1rRemoteIncluding ELI_ARG((_TPPrRemoteIncluding _currn));
extern void _VS2rRemoteIncluding ELI_ARG((_TPPrRemoteIncluding _currn));
extern void _VS3rRemoteIncluding ELI_ARG((_TPPrRemoteIncluding _currn));
extern void _VS4rRemoteIncluding ELI_ARG((_TPPrRemoteIncluding _currn));
extern void _VS5rRemoteIncluding ELI_ARG((_TPPrRemoteIncluding _currn));
extern void _VS6rRemoteIncluding ELI_ARG((_TPPrRemoteIncluding _currn));
extern void _VS1rSymbolAttributeReferencesSingle ELI_ARG((_TPPrSymbolAttributeReferencesSingle _currn));
extern void _VS2rSymbolAttributeReferencesSingle ELI_ARG((_TPPrSymbolAttributeReferencesSingle _currn));
extern void _VS3rSymbolAttributeReferencesSingle ELI_ARG((_TPPrSymbolAttributeReferencesSingle _currn));
extern void _VS4rSymbolAttributeReferencesSingle ELI_ARG((_TPPrSymbolAttributeReferencesSingle _currn,D_SymbolLocalAttributes* _AS0_const319));
extern void _VS5rSymbolAttributeReferencesSingle ELI_ARG((_TPPrSymbolAttributeReferencesSingle _currn,D_types* _AS0_const336,D_SymbolLocalAttributes* _AS0_const319));
extern void _VS1rSymbolAttributeReferencesMultiple ELI_ARG((_TPPrSymbolAttributeReferencesMultiple _currn));
extern void _VS2rSymbolAttributeReferencesMultiple ELI_ARG((_TPPrSymbolAttributeReferencesMultiple _currn));
extern void _VS3rSymbolAttributeReferencesMultiple ELI_ARG((_TPPrSymbolAttributeReferencesMultiple _currn));
extern void _VS4rSymbolAttributeReferencesMultiple ELI_ARG((_TPPrSymbolAttributeReferencesMultiple _currn,D_SymbolLocalAttributes* _AS0_const319));
extern void _VS5rSymbolAttributeReferencesMultiple ELI_ARG((_TPPrSymbolAttributeReferencesMultiple _currn,D_types* _AS0_const336,D_SymbolLocalAttributes* _AS0_const319));
#define _VS1rSymbolAttributeReferenceList2 _VS1rPTGDefList2

#define _VS2rSymbolAttributeReferenceList2 _VS2rPTGDefList2

#define _VS3rSymbolAttributeReferenceList2 _VS3rPTGDefList2

extern void _VS4rSymbolAttributeReferenceList2 ELI_ARG((_TPPrSymbolAttributeReferenceList2 _currn));
extern void _VS5rSymbolAttributeReferenceList2 ELI_ARG((_TPPrSymbolAttributeReferenceList2 _currn));
#define _VS1rSymbolAttributeReferenceList1 _VS1rPTGDecl

#define _VS2rSymbolAttributeReferenceList1 _VS2rPTGDecl

#define _VS3rSymbolAttributeReferenceList1 _VS3rPTGDecl

extern void _VS4rSymbolAttributeReferenceList1 ELI_ARG((_TPPrSymbolAttributeReferenceList1 _currn));
extern void _VS5rSymbolAttributeReferenceList1 ELI_ARG((_TPPrSymbolAttributeReferenceList1 _currn));
extern void _VS1rSymbolAttributeRef ELI_ARG((_TPPrSymbolAttributeRef _currn));
extern void _VS2rSymbolAttributeRef ELI_ARG((_TPPrSymbolAttributeRef _currn));
extern void _VS3rSymbolAttributeRef ELI_ARG((_TPPrSymbolAttributeRef _currn));
extern void _VS4rSymbolAttributeRef ELI_ARG((_TPPrSymbolAttributeRef _currn));
extern void _VS5rSymbolAttributeRef ELI_ARG((_TPPrSymbolAttributeRef _currn));
extern void _VS1rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS2rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS3rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS4rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS5rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS6rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS7rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS8rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS9rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS10rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS11rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS12rRemoteConstituent ELI_ARG((_TPPrRemoteConstituent _currn));
extern void _VS1rOptSymbolIsSymbol ELI_ARG((_TPPrOptSymbolIsSymbol _currn));
extern void _VS2rOptSymbolIsSymbol ELI_ARG((_TPPrOptSymbolIsSymbol _currn));
extern void _VS3rOptSymbolIsSymbol ELI_ARG((_TPPrOptSymbolIsSymbol _currn,StringTableKey* _AS0sym,PTGNodes* _AS0lido_codes,StringTableKey* _AS0_const10));
#define _VS1rNoSymbolReference _VS0Empty

extern void _VS2rNoSymbolReference ELI_ARG((_TPPrNoSymbolReference _currn));
extern void _VS3rNoSymbolReference ELI_ARG((_TPPrNoSymbolReference _currn,StringTableKey* _AS0sym,PTGNodes* _AS0lido_codes,StringTableKey* _AS0_const10));
extern void _VS1rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS2rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS3rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS4rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS5rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS6rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS7rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS8rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS9rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS10rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS11rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
extern void _VS12rRemoteOptionsWith ELI_ARG((_TPPrRemoteOptionsWith _currn));
#define _VS1rNoRemoteOptions _VS0Empty

#define _VS2rNoRemoteOptions _VS0Empty

#define _VS3rNoRemoteOptions _VS0Empty

#define _VS4rNoRemoteOptions _VS0Empty

#define _VS5rNoRemoteOptions _VS0Empty

#define _VS6rNoRemoteOptions _VS0Empty

#define _VS7rNoRemoteOptions _VS0Empty

extern void _VS8rNoRemoteOptions ELI_ARG((_TPPrNoRemoteOptions _currn));
extern void _VS9rNoRemoteOptions ELI_ARG((_TPPrNoRemoteOptions _currn));
extern void _VS10rNoRemoteOptions ELI_ARG((_TPPrNoRemoteOptions _currn));
extern void _VS11rNoRemoteOptions ELI_ARG((_TPPrNoRemoteOptions _currn));
extern void _VS12rNoRemoteOptions ELI_ARG((_TPPrNoRemoteOptions _currn));
extern void _VS1rRemoteOptionsShield ELI_ARG((_TPPrRemoteOptionsShield _currn));
#define _VS2rRemoteOptionsShield _VS0Empty

#define _VS3rRemoteOptionsShield _VS0Empty

#define _VS4rRemoteOptionsShield _VS0Empty

#define _VS5rRemoteOptionsShield _VS0Empty

#define _VS6rRemoteOptionsShield _VS0Empty

#define _VS7rRemoteOptionsShield _VS0Empty

extern void _VS8rRemoteOptionsShield ELI_ARG((_TPPrRemoteOptionsShield _currn));
extern void _VS9rRemoteOptionsShield ELI_ARG((_TPPrRemoteOptionsShield _currn));
extern void _VS10rRemoteOptionsShield ELI_ARG((_TPPrRemoteOptionsShield _currn));
extern void _VS11rRemoteOptionsShield ELI_ARG((_TPPrRemoteOptionsShield _currn));
extern void _VS12rRemoteOptionsShield ELI_ARG((_TPPrRemoteOptionsShield _currn));
#define _VS1rNoRemoteShield _VS0Empty

extern void _VS2rNoRemoteShield ELI_ARG((_TPPrNoRemoteShield _currn));
extern void _VS1rOptRemoteShield ELI_ARG((_TPPrOptRemoteShield _currn));
extern void _VS2rOptRemoteShield ELI_ARG((_TPPrOptRemoteShield _currn));
extern void _VS1rRemoteShieldMultiple ELI_ARG((_TPPrRemoteShieldMultiple _currn));
extern void _VS2rRemoteShieldMultiple ELI_ARG((_TPPrRemoteShieldMultiple _currn));
extern void _VS1rRemoteShieldSingle ELI_ARG((_TPPrRemoteShieldSingle _currn));
extern void _VS2rRemoteShieldSingle ELI_ARG((_TPPrRemoteShieldSingle _currn));
#define _VS1rRemoteUnshieldSelf _VS0Empty

extern void _VS2rRemoteUnshieldSelf ELI_ARG((_TPPrRemoteUnshieldSelf _currn));
#define _VS1rSymbolReferenceList2 _VS1rPTGDefList2

extern void _VS2rSymbolReferenceList2 ELI_ARG((_TPPrSymbolReferenceList2 _currn));
#define _VS1rSymbolReferenceList1 _VS1rPTGDecl

extern void _VS2rSymbolReferenceList1 ELI_ARG((_TPPrSymbolReferenceList1 _currn));
extern void _VS1rRemoteAppendCall ELI_ARG((_TPPrRemoteAppendCall _currn));
#define _VS2rRemoteAppendCall _VS0Empty

extern void _VS3rRemoteAppendCall ELI_ARG((_TPPrRemoteAppendCall _currn));
extern void _VS4rRemoteAppendCall ELI_ARG((_TPPrRemoteAppendCall _currn));
extern void _VS5rRemoteAppendCall ELI_ARG((_TPPrRemoteAppendCall _currn));
extern void _VS6rRemoteAppendCall ELI_ARG((_TPPrRemoteAppendCall _currn));
#define _VS7rRemoteAppendCall _VS0Empty

extern void _VS8rRemoteAppendCall ELI_ARG((_TPPrRemoteAppendCall _currn,D_RemoteCombinePtr* _AS0append,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS9rRemoteAppendCall ELI_ARG((_TPPrRemoteAppendCall _currn,D_RemoteCombinePtr* _AS0append,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS10rRemoteAppendCall ELI_ARG((_TPPrRemoteAppendCall _currn,D_RemoteCombinePtr* _AS0append,D_typePtr* _AS0result_type,D_types* _AS0_const336,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS11rRemoteAppendCall ELI_ARG((_TPPrRemoteAppendCall _currn,D_RemoteCombinePtr* _AS0append,D_typePtr* _AS0result_type,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn));
extern void _VS2rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn));
extern void _VS3rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn));
extern void _VS4rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn));
extern void _VS5rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn));
extern void _VS6rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn));
extern void _VS7rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn));
extern void _VS8rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn,D_RemoteCombinePtr* _AS0append,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS9rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn,D_RemoteCombinePtr* _AS0append,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS10rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn,D_RemoteCombinePtr* _AS0append,D_typePtr* _AS0result_type,D_types* _AS0_const336,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS11rRemoteAppendLambda ELI_ARG((_TPPrRemoteAppendLambda _currn,D_RemoteCombinePtr* _AS0append,D_typePtr* _AS0result_type,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
#define _VS1rRemoteAppendIsBinary _VS0Empty

#define _VS2rRemoteAppendIsBinary _VS0Empty

#define _VS3rRemoteAppendIsBinary _VS0Empty

#define _VS4rRemoteAppendIsBinary _VS0Empty

extern void _VS5rRemoteAppendIsBinary ELI_ARG((_TPPrRemoteAppendIsBinary _currn));
#define _VS6rRemoteAppendIsBinary _VS0Empty

#define _VS7rRemoteAppendIsBinary _VS0Empty

extern void _VS8rRemoteAppendIsBinary ELI_ARG((_TPPrRemoteAppendIsBinary _currn,D_RemoteCombinePtr* _AS0append,D_RuleSymbolAttributes* _AS0_const298));
extern void _VS9rRemoteAppendIsBinary ELI_ARG((_TPPrRemoteAppendIsBinary _currn,D_RemoteCombinePtr* _AS0append,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281));
extern void _VS10rRemoteAppendIsBinary ELI_ARG((_TPPrRemoteAppendIsBinary _currn,D_RemoteCombinePtr* _AS0append,D_typePtr* _AS0result_type,D_types* _AS0_const336,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,int* _AS0_const261));
extern void _VS11rRemoteAppendIsBinary ELI_ARG((_TPPrRemoteAppendIsBinary _currn,D_RemoteCombinePtr* _AS0append,D_typePtr* _AS0result_type,PTGNodes* _AS0lido_codes,D_types* _AS0_const336,PTGNode* _AS0_const333,StringTableKeyList* _AS0_const330,int* _AS0_const310,D_RuleSymbolAttributes* _AS0_const298,D_OrderStatRs* _AS0_const287,int* _AS0_const286,int* _AS0_const285,StringTableKey* _AS0_const284,bool* _AS0_const283,bool* _AS0_const282,int* _AS0_const281,D_VirtAttrs* _AS0_const274,int* _AS0_const261,StringTableKey* _AS0_const10));
extern void _VS1rRemoteAppendADD ELI_ARG((_TPPrRemoteAppendADD _currn));
extern void _VS2rRemoteAppendADD ELI_ARG((_TPPrRemoteAppendADD _currn));
extern void _VS1rRemoteAppendSUB ELI_ARG((_TPPrRemoteAppendSUB _currn));
extern void _VS2rRemoteAppendSUB ELI_ARG((_TPPrRemoteAppendSUB _currn));
extern void _VS1rRemoteAppendDIV ELI_ARG((_TPPrRemoteAppendDIV _currn));
extern void _VS2rRemoteAppendDIV ELI_ARG((_TPPrRemoteAppendDIV _currn));
extern void _VS1rRemoteAppendMUL ELI_ARG((_TPPrRemoteAppendMUL _currn));
extern void _VS2rRemoteAppendMUL ELI_ARG((_TPPrRemoteAppendMUL _currn));
extern void _VS1rRemoteAppendConcat ELI_ARG((_TPPrRemoteAppendConcat _currn));
extern void _VS2rRemoteAppendConcat ELI_ARG((_TPPrRemoteAppendConcat _currn));
extern void _VS1rRemoteAppendEQ ELI_ARG((_TPPrRemoteAppendEQ _currn));
extern void _VS2rRemoteAppendEQ ELI_ARG((_TPPrRemoteAppendEQ _currn));
extern void _VS1rRemoteAppendNE ELI_ARG((_TPPrRemoteAppendNE _currn));
extern void _VS2rRemoteAppendNE ELI_ARG((_TPPrRemoteAppendNE _currn));
extern void _VS1rRemoteAppendLT ELI_ARG((_TPPrRemoteAppendLT _currn));
extern void _VS2rRemoteAppendLT ELI_ARG((_TPPrRemoteAppendLT _currn));
extern void _VS1rRemoteAppendGT ELI_ARG((_TPPrRemoteAppendGT _currn));
extern void _VS2rRemoteAppendGT ELI_ARG((_TPPrRemoteAppendGT _currn));
extern void _VS1rRemoteAppendLE ELI_ARG((_TPPrRemoteAppendLE _currn));
extern void _VS2rRemoteAppendLE ELI_ARG((_TPPrRemoteAppendLE _currn));
extern void _VS1rRemoteAppendGE ELI_ARG((_TPPrRemoteAppendGE _currn));
extern void _VS2rRemoteAppendGE ELI_ARG((_TPPrRemoteAppendGE _currn));
extern void _VS1rRemoteAppendOR ELI_ARG((_TPPrRemoteAppendOR _currn));
extern void _VS2rRemoteAppendOR ELI_ARG((_TPPrRemoteAppendOR _currn));
extern void _VS1rRemoteAppendAND ELI_ARG((_TPPrRemoteAppendAND _currn));
extern void _VS2rRemoteAppendAND ELI_ARG((_TPPrRemoteAppendAND _currn));
extern void _VS1rRemoteAppendMOD ELI_ARG((_TPPrRemoteAppendMOD _currn));
extern void _VS2rRemoteAppendMOD ELI_ARG((_TPPrRemoteAppendMOD _currn));
extern void _VS1rRemoteSingleCall ELI_ARG((_TPPrRemoteSingleCall _currn));
#define _VS2rRemoteSingleCall _VS0Empty

extern void _VS3rRemoteSingleCall ELI_ARG((_TPPrRemoteSingleCall _currn));
extern void _VS4rRemoteSingleCall ELI_ARG((_TPPrRemoteSingleCall _currn));
extern void _VS5rRemoteSingleCall ELI_ARG((_TPPrRemoteSingleCall _currn));
extern void _VS6rRemoteSingleCall ELI_ARG((_TPPrRemoteSingleCall _currn));
#define _VS7rRemoteSingleCall _VS0Empty

extern void _VS8rRemoteSingleCall ELI_ARG((_TPPrRemoteSingleCall _currn));
extern void _VS9rRemoteSingleCall ELI_ARG((_TPPrRemoteSingleCall _currn));
extern void _VS10rRemoteSingleCall ELI_ARG((_TPPrRemoteSingleCall _currn));
extern void _VS11rRemoteSingleCall ELI_ARG((_TPPrRemoteSingleCall _currn));
extern void _VS1rRemoteSingleInfer ELI_ARG((_TPPrRemoteSingleInfer _currn));
#define _VS2rRemoteSingleInfer _VS0Empty

#define _VS3rRemoteSingleInfer _VS0Empty

#define _VS4rRemoteSingleInfer _VS0Empty

#define _VS5rRemoteSingleInfer _VS0Empty

#define _VS6rRemoteSingleInfer _VS0Empty

#define _VS7rRemoteSingleInfer _VS0Empty

extern void _VS8rRemoteSingleInfer ELI_ARG((_TPPrRemoteSingleInfer _currn));
extern void _VS9rRemoteSingleInfer ELI_ARG((_TPPrRemoteSingleInfer _currn));
extern void _VS10rRemoteSingleInfer ELI_ARG((_TPPrRemoteSingleInfer _currn));
extern void _VS11rRemoteSingleInfer ELI_ARG((_TPPrRemoteSingleInfer _currn));
#define _VS1rRemoteSingleCreateList _VS0Empty

#define _VS2rRemoteSingleCreateList _VS0Empty

#define _VS3rRemoteSingleCreateList _VS0Empty

#define _VS4rRemoteSingleCreateList _VS0Empty

#define _VS5rRemoteSingleCreateList _VS0Empty

#define _VS6rRemoteSingleCreateList _VS0Empty

#define _VS7rRemoteSingleCreateList _VS0Empty

extern void _VS8rRemoteSingleCreateList ELI_ARG((_TPPrRemoteSingleCreateList _currn));
extern void _VS9rRemoteSingleCreateList ELI_ARG((_TPPrRemoteSingleCreateList _currn));
extern void _VS10rRemoteSingleCreateList ELI_ARG((_TPPrRemoteSingleCreateList _currn));
extern void _VS11rRemoteSingleCreateList ELI_ARG((_TPPrRemoteSingleCreateList _currn));
extern void _VS1rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS2rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS3rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS4rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS5rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS6rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS7rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS8rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS9rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS10rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS11rRemoteSingleLambda ELI_ARG((_TPPrRemoteSingleLambda _currn));
extern void _VS1rRemoteEmptyCall ELI_ARG((_TPPrRemoteEmptyCall _currn));
extern void _VS2rRemoteEmptyCall ELI_ARG((_TPPrRemoteEmptyCall _currn));
extern void _VS3rRemoteEmptyCall ELI_ARG((_TPPrRemoteEmptyCall _currn));
extern void _VS4rRemoteEmptyCall ELI_ARG((_TPPrRemoteEmptyCall _currn,D_typePtr* _AS0result_type));
extern void _VS5rRemoteEmptyCall ELI_ARG((_TPPrRemoteEmptyCall _currn,D_typePtr* _AS0result_type));
extern void _VS6rRemoteEmptyCall ELI_ARG((_TPPrRemoteEmptyCall _currn,D_typePtr* _AS0result_type));
extern void _VS7rRemoteEmptyCall ELI_ARG((_TPPrRemoteEmptyCall _currn,D_typePtr* _AS0result_type));
extern void _VS8rRemoteEmptyCall ELI_ARG((_TPPrRemoteEmptyCall _currn,D_typePtr* _AS0result_type));
extern void _VS1rRemoteEmptyConstant ELI_ARG((_TPPrRemoteEmptyConstant _currn));
#define _VS2rRemoteEmptyConstant _VS0Empty

#define _VS3rRemoteEmptyConstant _VS0Empty

extern void _VS4rRemoteEmptyConstant ELI_ARG((_TPPrRemoteEmptyConstant _currn,D_typePtr* _AS0result_type));
extern void _VS5rRemoteEmptyConstant ELI_ARG((_TPPrRemoteEmptyConstant _currn,D_typePtr* _AS0result_type));
extern void _VS6rRemoteEmptyConstant ELI_ARG((_TPPrRemoteEmptyConstant _currn,D_typePtr* _AS0result_type));
extern void _VS7rRemoteEmptyConstant ELI_ARG((_TPPrRemoteEmptyConstant _currn,D_typePtr* _AS0result_type));
extern void _VS8rRemoteEmptyConstant ELI_ARG((_TPPrRemoteEmptyConstant _currn,D_typePtr* _AS0result_type));
extern void _VS1rRemoteEmptyInfer ELI_ARG((_TPPrRemoteEmptyInfer _currn));
#define _VS2rRemoteEmptyInfer _VS0Empty

#define _VS3rRemoteEmptyInfer _VS0Empty

extern void _VS4rRemoteEmptyInfer ELI_ARG((_TPPrRemoteEmptyInfer _currn,D_typePtr* _AS0result_type));
extern void _VS5rRemoteEmptyInfer ELI_ARG((_TPPrRemoteEmptyInfer _currn,D_typePtr* _AS0result_type));
extern void _VS6rRemoteEmptyInfer ELI_ARG((_TPPrRemoteEmptyInfer _currn,D_typePtr* _AS0result_type));
extern void _VS7rRemoteEmptyInfer ELI_ARG((_TPPrRemoteEmptyInfer _currn,D_typePtr* _AS0result_type));
extern void _VS8rRemoteEmptyInfer ELI_ARG((_TPPrRemoteEmptyInfer _currn,D_typePtr* _AS0result_type));
extern void _VS1rRemoteWithInfer ELI_ARG((_TPPrRemoteWithInfer _currn));
#define _VS2rRemoteWithInfer _VS0Empty

#define _VS3rRemoteWithInfer _VS0Empty

#define _VS4rRemoteWithInfer _VS0Empty

#define _VS5rRemoteWithInfer _VS0Empty

#define _VS6rRemoteWithInfer _VS0Empty

#define _VS7rRemoteWithInfer _VS0Empty

extern void _VS8rRemoteWithInfer ELI_ARG((_TPPrRemoteWithInfer _currn));
extern void _VS9rRemoteWithInfer ELI_ARG((_TPPrRemoteWithInfer _currn));
extern void _VS10rRemoteWithInfer ELI_ARG((_TPPrRemoteWithInfer _currn));
extern void _VS11rRemoteWithInfer ELI_ARG((_TPPrRemoteWithInfer _currn));
extern void _VS12rRemoteWithInfer ELI_ARG((_TPPrRemoteWithInfer _currn));
extern void _VS1rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS2rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS3rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS4rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS5rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS6rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS7rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS8rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS9rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS10rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS11rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS12rRemoteWithLido ELI_ARG((_TPPrRemoteWithLido _currn));
extern void _VS1rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS2rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS3rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS4rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS5rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS6rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS7rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS8rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS9rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS10rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS11rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
extern void _VS12rRemoteWithInferTypes ELI_ARG((_TPPrRemoteWithInferTypes _currn));
#define _VS1rSymbolOptionUsing _VS1rPTGDecl

#define _VS2rSymbolOptionUsing _VS2rPTGDecl

extern void _VS3rSymbolOptionUsing ELI_ARG((_TPPrSymbolOptionUsing _currn));
extern void _VS4rSymbolOptionUsing ELI_ARG((_TPPrSymbolOptionUsing _currn));
#define _VS5rSymbolOptionUsing _VS0Empty

extern void _VS6rSymbolOptionUsing ELI_ARG((_TPPrSymbolOptionUsing _currn));
#define _VS1rSymbolUsingAttribute _VS1rPTGDecl

#define _VS2rSymbolUsingAttribute _VS2rPTGDecl

#define _VS3rSymbolUsingAttribute _VS3rPTGDecl

#define _VS4rSymbolUsingAttribute _VS4rPTGDecl

extern void _VS5rSymbolUsingAttribute ELI_ARG((_TPPrSymbolUsingAttribute _currn));
#define _VS1rSymbolUsingDeclList2 _VS1rPTGDefList2

#define _VS2rSymbolUsingDeclList2 _VS2rPTGDefList2

#define _VS3rSymbolUsingDeclList2 _VS3rPTGDefList2

#define _VS4rSymbolUsingDeclList2 _VS4rPTGDefList2

extern void _VS5rSymbolUsingDeclList2 ELI_ARG((_TPPrSymbolUsingDeclList2 _currn));
#define _VS1rSymbolUsingDeclList1 _VS1rPTGDecl

#define _VS2rSymbolUsingDeclList1 _VS2rPTGDecl

#define _VS3rSymbolUsingDeclList1 _VS3rPTGDecl

#define _VS4rSymbolUsingDeclList1 _VS4rPTGDecl

extern void _VS5rSymbolUsingDeclList1 ELI_ARG((_TPPrSymbolUsingDeclList1 _currn));
extern void _VS1rSymbolUsingDecl ELI_ARG((_TPPrSymbolUsingDecl _currn));
extern void _VS2rSymbolUsingDecl ELI_ARG((_TPPrSymbolUsingDecl _currn));
extern void _VS3rSymbolUsingDecl ELI_ARG((_TPPrSymbolUsingDecl _currn));
extern void _VS4rSymbolUsingDecl ELI_ARG((_TPPrSymbolUsingDecl _currn));
extern void _VS5rSymbolUsingDecl ELI_ARG((_TPPrSymbolUsingDecl _currn));
extern void _VS1rSymbolUsingDeclWithoutClass ELI_ARG((_TPPrSymbolUsingDeclWithoutClass _currn));
extern void _VS2rSymbolUsingDeclWithoutClass ELI_ARG((_TPPrSymbolUsingDeclWithoutClass _currn));
extern void _VS3rSymbolUsingDeclWithoutClass ELI_ARG((_TPPrSymbolUsingDeclWithoutClass _currn));
extern void _VS4rSymbolUsingDeclWithoutClass ELI_ARG((_TPPrSymbolUsingDeclWithoutClass _currn));
extern void _VS5rSymbolUsingDeclWithoutClass ELI_ARG((_TPPrSymbolUsingDeclWithoutClass _currn));
#define _VS1rSymbolUsingDeclsNoClassList2 _VS1rPTGDefList2

#define _VS2rSymbolUsingDeclsNoClassList2 _VS2rPTGDefList2

#define _VS3rSymbolUsingDeclsNoClassList2 _VS3rPTGDefList2

extern void _VS4rSymbolUsingDeclsNoClassList2 ELI_ARG((_TPPrSymbolUsingDeclsNoClassList2 _currn));
#define _VS1rSymbolUsingDeclsNoClassList1 _VS1rPTGDecl

#define _VS2rSymbolUsingDeclsNoClassList1 _VS2rPTGDecl

#define _VS3rSymbolUsingDeclsNoClassList1 _VS3rPTGDecl

extern void _VS4rSymbolUsingDeclsNoClassList1 ELI_ARG((_TPPrSymbolUsingDeclsNoClassList1 _currn));
extern void _VS1rSymbolUsingDeclNoClass ELI_ARG((_TPPrSymbolUsingDeclNoClass _currn));
extern void _VS2rSymbolUsingDeclNoClass ELI_ARG((_TPPrSymbolUsingDeclNoClass _currn));
extern void _VS3rSymbolUsingDeclNoClass ELI_ARG((_TPPrSymbolUsingDeclNoClass _currn));
extern void _VS4rSymbolUsingDeclNoClass ELI_ARG((_TPPrSymbolUsingDeclNoClass _currn));
#define _VS1rSymbolLocalAttributeReferenceList2 _VS1rPTGDefList2

extern void _VS2rSymbolLocalAttributeReferenceList2 ELI_ARG((_TPPrSymbolLocalAttributeReferenceList2 _currn));
#define _VS1rSymbolLocalAttributeReferenceList1 _VS1rPTGDecl

extern void _VS2rSymbolLocalAttributeReferenceList1 ELI_ARG((_TPPrSymbolLocalAttributeReferenceList1 _currn));
extern void _VS1rLocalUsingReference ELI_ARG((_TPPrLocalUsingReference _currn));
extern void _VS2rLocalUsingReference ELI_ARG((_TPPrLocalUsingReference _currn));
extern void _VS1rSymbolAttrReferenceIsTree ELI_ARG((_TPPrSymbolAttrReferenceIsTree _currn));
#endif
