#ifndef lng_gen_HPP
#define lng_gen_HPP

#include <string> 
#include <memory> 
#include <algorithm> 
#include <vector>
#include <map>
#include <utility>
#include <functional>
#include <fstream>
#include <unordered_map>
#include <sstream>
#include <iostream>



typedef std::string tStringType;
typedef int Int;
typedef int StringTableKey;
#ifdef DO_TEST_CODE 
typedef int* DefTableKey;
typedef int* Environment;
typedef int* Binding;
typedef int* CoordPtr;
#define NoIdn 0
#define NoEnv nullptr
#define NoBinding nullptr
#define NoKey nullptr
#define FATAL 0
#define DEBUG 10
#define ERROR 0
#define NoPosition nullptr
extern StringTableKey ensure_idn(tStringType name);
extern std::string StringTable(StringTableKey x);
extern StringTableKey IdnOf(Binding b);

#else 
#include "envmod.h"
#include "messaging.h"
#include "idn.h"
#include "csm.h"
typedef struct NODEPTR_struct *NODEPTR; 

#define GetClpValueI GetClpValue 
#define GetClpValueS GetClpValue 
#endif // DO_TEST_CODE 

template<typename T>
std::vector<T> single_list(T const& obj) { 
  std::vector<T> ret = std::vector<T>(); 
  ret.push_back(obj);
  return ret; 
}

template<typename T>
std::vector<T> append_list(std::vector<T> const& in1, std::vector<T> const& in2) { 
  auto ret = in1; 
  for(auto x : in2) { 
    ret.push_back(x); 
  }
  return ret; 
}

extern DefTableKey NewKey(); 
extern StringTableKey ensure_idn(std::string name);
extern Binding BindKey(Environment env, StringTableKey idn, DefTableKey key);
class PTG_BaseNode;
typedef std::shared_ptr<PTG_BaseNode> PTGNode;


typedef std::vector<StringTableKey> StringTableKeyList;
enum class IndexType { Unknown, HasIndex, NoIndex, Invalid, IT_Unknown};
extern std::ostream& operator<<(std::ostream& os, IndexType arg);
//extern bool operator<(IndexType const& lhs, IndexType const& rhs);

typedef std::tuple<int, tStringType, StringTableKey> D_ptg_insert;
typedef std::vector<D_ptg_insert> D_ptg_inserts;
typedef std::tuple<Binding, Binding> Edge;
typedef std::vector<Edge> Edges;
typedef std::vector<Binding> BindingList;
enum class TypingKind { Unknown, Predefined, DataSimple, DataAbstract, DataRecord, Alias, Constructor, DataConstructor, Constructed, TreeType, DataComplex, TK_Unknown};
extern std::ostream& operator<<(std::ostream& os, TypingKind arg);
//extern bool operator<(TypingKind const& lhs, TypingKind const& rhs);

enum class D_pdl_gfun { Unknown, DP_Reset, DP_Get, DP_Has, DP_Is, DP_Set, DP_Unique};
extern std::ostream& operator<<(std::ostream& os, D_pdl_gfun arg);
//extern bool operator<(D_pdl_gfun const& lhs, D_pdl_gfun const& rhs);

class D_constant;
typedef std::shared_ptr<D_constant> D_constantPtr;

class D_pattern;
typedef std::shared_ptr<D_pattern> D_patternPtr;

typedef std::vector<D_patternPtr> D_patterns;
typedef std::vector<std::vector<D_patternPtr>> D_patternLists;
class D_type;
typedef std::shared_ptr<D_type> D_typePtr;

typedef std::vector<D_typePtr> D_types;
typedef std::vector<std::vector<D_typePtr>> D_typesList;
typedef std::map<D_typePtr, D_typePtr> D_scheme;
class D_patternType;
typedef std::shared_ptr<D_patternType> D_patternTypePtr;

typedef std::vector<D_patternTypePtr> D_patternTypes;
typedef std::map<StringTableKey, std::vector<D_typePtr>> D_candidates;
class D_PatternCode;
typedef std::shared_ptr<D_PatternCode> D_PatternCodePtr;

typedef std::vector<D_PatternCodePtr> D_PatternCodes;
class D_funCode;
typedef std::shared_ptr<D_funCode> D_funCodePtr;

typedef std::vector<D_funCodePtr> D_funCodes;
class D_ConArg;
typedef std::shared_ptr<D_ConArg> D_ConArgPtr;

typedef std::vector<D_ConArgPtr> D_ConArgs;
enum class SymbolType { Unknown, ST_Term, ST_Symbol, ST_Unknown};
extern std::ostream& operator<<(std::ostream& os, SymbolType arg);
//extern bool operator<(SymbolType const& lhs, SymbolType const& rhs);

enum class TermType { Unknown, TT_Name, TT_IsTerm, TT_AsTerm, TT_Symbol, TT_Unknown};
extern std::ostream& operator<<(std::ostream& os, TermType arg);
//extern bool operator<(TermType const& lhs, TermType const& rhs);

class D_ProdSymbol;
typedef std::shared_ptr<D_ProdSymbol> D_ProdSymbolPtr;

typedef std::vector<D_ProdSymbolPtr> D_ProdSymbols;
typedef std::map<StringTableKey, int> D_SkeyToIndex;
enum class AttributeDir { Unknown, AT_Synt, AT_Inh, AT_Chain, AT_Thread, AT_Infer, AT_Unknown};
extern std::ostream& operator<<(std::ostream& os, AttributeDir arg);
//extern bool operator<(AttributeDir const& lhs, AttributeDir const& rhs);

class TreeSymb;
typedef std::shared_ptr<TreeSymb> TreeSymbPtr;

typedef std::vector<TreeSymbPtr> TreeSymbs;
class D_SymbolAttribute;
typedef std::shared_ptr<D_SymbolAttribute> D_SymbolAttributePtr;

typedef std::tuple<std::vector<D_SymbolAttributePtr>, std::vector<PTGNode>, std::vector<PTGNode>, std::vector<PTGNode>> D_OrderStat;
typedef std::vector<D_OrderStat> D_OrderStats;
typedef std::vector<D_SymbolAttributePtr> D_SymbolAttributes;
class D_SymbolComputation;
typedef std::shared_ptr<D_SymbolComputation> D_SymbolComputationPtr;

typedef std::vector<D_SymbolComputationPtr> D_SymbolComputations;
enum class D_BinOp { Unknown, BO_Add, BO_Sub, BO_Div, BO_Mod, BO_Mul, BO_Eq, BO_Ne, BO_Gt, BO_Lt, BO_Ge, BO_Le, BO_Concat, BO_Or, BO_And, BO_Unknown};
extern std::ostream& operator<<(std::ostream& os, D_BinOp arg);
//extern bool operator<(D_BinOp const& lhs, D_BinOp const& rhs);

enum class D_PostOp { Unknown, PO_Index, PO_Access, PO_ListCon};
extern std::ostream& operator<<(std::ostream& os, D_PostOp arg);
//extern bool operator<(D_PostOp const& lhs, D_PostOp const& rhs);

enum class D_UnaryOp { Unknown, UO_Incr, UO_Neg, UO_Not, UO_Address, UO_Refernce};
extern std::ostream& operator<<(std::ostream& os, D_UnaryOp arg);
//extern bool operator<(D_UnaryOp const& lhs, D_UnaryOp const& rhs);

class D_SymbolLetVarDef;
typedef std::shared_ptr<D_SymbolLetVarDef> D_SymbolLetVarDefPtr;

typedef std::vector<D_SymbolLetVarDefPtr> D_SymbolLetVarDefs;
class D_SymbolExpression;
typedef std::shared_ptr<D_SymbolExpression> D_SymbolExpressionPtr;

typedef std::vector<D_SymbolExpressionPtr> D_SymbolExpressions;
class D_ProdMatch;
typedef std::shared_ptr<D_ProdMatch> D_ProdMatchPtr;

typedef std::vector<D_ProdMatchPtr> D_ProdMatches;
enum class MatchType { Unknown, MT_Error, MT_Match, MT_Done};
extern std::ostream& operator<<(std::ostream& os, MatchType arg);
//extern bool operator<(MatchType const& lhs, MatchType const& rhs);

class D_RuleSymbolAttribute;
typedef std::shared_ptr<D_RuleSymbolAttribute> D_RuleSymbolAttributePtr;

typedef std::tuple<std::vector<D_RuleSymbolAttributePtr>, std::vector<PTGNode>, std::vector<PTGNode>, std::vector<PTGNode>> D_OrderStatR;
typedef std::vector<D_OrderStatR> D_OrderStatRs;
typedef std::vector<D_RuleSymbolAttributePtr> D_RuleSymbolAttributes;
class D_RuleComputation;
typedef std::shared_ptr<D_RuleComputation> D_RuleComputationPtr;

typedef std::vector<D_RuleComputationPtr> D_RuleComputations;
class D_RuleExpression;
typedef std::shared_ptr<D_RuleExpression> D_RuleExpressionPtr;

typedef std::vector<D_RuleExpressionPtr> D_RuleExpressions;
class D_RuleLetvarDef;
typedef std::shared_ptr<D_RuleLetvarDef> D_RuleLetvarDefPtr;

typedef std::vector<D_RuleLetvarDefPtr> D_RuleLetvarDefs;
enum class D_LocalAttributeClass { Unknown, LA_Inh, LA_Synt, LA_Tail, LA_Head, LA_Unknown};
extern std::ostream& operator<<(std::ostream& os, D_LocalAttributeClass arg);
//extern bool operator<(D_LocalAttributeClass const& lhs, D_LocalAttributeClass const& rhs);

class D_SymbolLocalAttribute;
typedef std::shared_ptr<D_SymbolLocalAttribute> D_SymbolLocalAttributePtr;

typedef std::vector<D_SymbolLocalAttributePtr> D_SymbolLocalAttributes;
class D_RemoteCombine;
typedef std::shared_ptr<D_RemoteCombine> D_RemoteCombinePtr;

class D_RemoteSingle;
typedef std::shared_ptr<D_RemoteSingle> D_RemoteSinglePtr;

class D_RemoteEmpty;
typedef std::shared_ptr<D_RemoteEmpty> D_RemoteEmptyPtr;

class D_RemoteShield;
typedef std::shared_ptr<D_RemoteShield> D_RemoteShieldPtr;

class D_ConstituentOptions;
typedef std::shared_ptr<D_ConstituentOptions> D_ConstituentOptionsPtr;

class D_RemoteAttribute;
typedef std::shared_ptr<D_RemoteAttribute> D_RemoteAttributePtr;

typedef std::vector<D_RemoteAttributePtr> D_RemoteAttributes;
typedef std::map<StringTableKey, std::vector<StringTableKey>> D_ReachableSymbols;
typedef std::map<Binding, std::vector<D_RuleSymbolAttributePtr>> D_RuleAttrMap;
typedef std::map<StringTableKey, std::map<int, std::vector<StringTableKey>>> RSymbMap;
typedef std::map<StringTableKey, std::map<StringTableKey, std::map<int, std::vector<StringTableKey>>>> SymbAttrMap;
typedef std::map<StringTableKey, std::map<StringTableKey, std::map<int, std::vector<CoordPtr>>>> SAttrPosMap;
typedef std::vector<int> IntList;
class D_VirtAttr;
typedef std::shared_ptr<D_VirtAttr> D_VirtAttrPtr;

typedef std::vector<D_VirtAttrPtr> D_VirtAttrs;
typedef std::vector<std::vector<D_VirtAttrPtr>> D_VirtAttrList;
enum class D_RHSKind { Unknown, RK_Left, RK_Cond, RK_HardLeft, RK_Right, RK_Dep};
extern std::ostream& operator<<(std::ostream& os, D_RHSKind arg);
//extern bool operator<(D_RHSKind const& lhs, D_RHSKind const& rhs);

typedef std::vector<PTGNode> PTGNodes;
typedef std::map<StringTableKey, std::vector<StringTableKey>> D_VAttrMap;
typedef std::function<PTGNode(PTGNode, PTGNode)> genFun_4;
typedef std::function<PTGNode(D_ptg_insert)> genFun_5;
typedef std::function<PTGNode(Binding)> genFun_6;
typedef std::function<tStringType(tStringType, tStringType)> genFun_7;
typedef std::tuple<D_typePtr, D_typePtr> genTuple_5;
typedef std::vector<D_patterns> genList_17;
typedef std::tuple<StringTableKey, int> genTuple_6;
typedef std::map<int, std::vector<StringTableKey>> genMap_6;
typedef std::vector<CoordPtr> genList_33;
typedef std::map<int, std::vector<CoordPtr>> genMap_9;
typedef std::map<StringTableKey, std::map<int, std::vector<CoordPtr>>> genMap_10;
typedef std::function<PTGNode(PTGNode, PTGNode, PTGNode)> genFun_8;
typedef std::function<PTGNode(PTGNode, PTGNode, PTGNode, PTGNode)> genFun_9;
typedef std::function<PTGNode(PTGNode)> genFun_10;
typedef std::function<PTGNode(StringTableKey)> genFun_11;

 
  #define stdfind std::find
  #define stdcount std::count
  #define stdunique std::unique 
  #define stdsort std::sort 

 
  extern int idn_2_int(StringTableKey idn); 

 

#include "clp.h" 
#include <unordered_set>

extern void CheckNewInput(char* name); 
extern char* remove_trailing_symbols(char* y);
extern char* remove_trailing_symbols(StringTableKey sym); 


 
  #include <algorithm>
  #define stdsort std::sort
  #define stdunique std::unique 

 
#include <boost/algorithm/string.hpp>
#define bacontains boost::algorithm::contains 

 
/// @brief checks if the given token is really Ctext and if so stores it, 
///        otherwise sets the token code to Opening bracket 
/// @param start points to the start, which should be '{' 
/// @param length the length of the token from lexing, should be 1
/// @param syncode a reference to the variable which stores the token code 
/// @param intrinsic the intrinsic information of this token, stores the text 
///                  if it is a Ctext
/// @side_effects a new string containing the CText might have been stored in 
///               the StringTable 
extern void CTextOrNot(char const* start, int length, int* syncode, int* intrinsic);

/// @brief switches if the upcoming token can be Ctext or not
/// @post if a Ctext was possible before, a Ctext now is not returned from 
///       CTextOrNot, the inverse also holds 
/// @side_effects changes the behaviour of CTextOrNot
extern void switch_ctext(); 


/// @brief next_list sets a property and increments a global variable 
/// @pre none 
/// @post k != NoKey => global counter incremented 
extern void next_list(DefTableKey k); 
extern void next_tuple(DefTableKey k); 
extern void next_map(DefTableKey k); 
extern void next_fun(DefTableKey k); 

 
  extern D_typePtr newTyVar(); 


  #define NoType nullptr

 
  extern void reset_patterns(Environment types, Environment vars, Binding fun);
  extern D_typePtr mk_invalid_type(); 


#define append_str(x, y) x += y 

 
  extern std::pair<D_typePtr, D_typePtr> make_pair(D_typePtr a, D_typePtr b); 

 
  extern D_typePtr newTyVarX(std::string prefixed);
  extern void reset_ty_vars(); 

 
  #define do_seq(x, y) x; y

 
  extern D_patterns filled_list(D_patterns in, int curr, int max); 

 
  extern void print_version(); 

 
  extern StringTableKey int_2_idn(int i); 

 
  #define seq3(a, b, c) a, b, c

 
  
  #define mkvoid(x) (void) x
  

 
extern int gen_def_num; 


  extern std::string get_filename(CoordPtr p);
  extern std::string map_position(CoordPtr p); 


  #include "clp.h" 

 
  extern int pathcount_from_to(StringTableKey context, 
                               StringTableKeyList targets, 
                               StringTableKeyList shielded, 
                               D_ReachableSymbols reachables); 

 
  extern int transreach_pathcount(StringTableKeyList from, 
                                  D_ReachableSymbols reachables, 
                                  StringTableKeyList shielded, 
                                  StringTableKeyList targets); 


class D_constant { 
public:
  enum class D_constant_kind { Unknown, DCon_String, DCon_Number, DCon_Extern, DCon_Bool, DCon_EmptyList, DCon_CoordRef}; 
protected: 
  D_constant_kind kind; 

D_constant(); 
public: 
  virtual ~D_constant();
  D_constant(D_constant const& other);
  D_constant(D_constant::D_constant_kind kind);
  D_constant(D_constant&& other);
  
  D_constant_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_constant* arg); 
extern bool operator<(D_constantPtr lhs, D_constantPtr rhs);
extern bool operator==(D_constantPtr lhs, D_constantPtr rhs);

class sub_D_constant_DCon_String : public D_constant { 
protected: 
  sub_D_constant_DCon_String();
  StringTableKey arg_0;
public:
  sub_D_constant_DCon_String(StringTableKey arg_0);
  sub_D_constant_DCon_String(sub_D_constant_DCon_String const& other); 
  sub_D_constant_DCon_String(sub_D_constant_DCon_String&& other);
  virtual ~sub_D_constant_DCon_String(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_constant_DCon_String& arg);
extern bool operator==(sub_D_constant_DCon_String& lhs, sub_D_constant_DCon_String& rhs);
extern bool operator<(sub_D_constant_DCon_String& lhs, sub_D_constant_DCon_String& rhs);
extern D_constantPtr create_sub_D_constant_DCon_String(StringTableKey arg_0);


class sub_D_constant_DCon_Number : public D_constant { 
protected: 
  sub_D_constant_DCon_Number();
  int arg_0;
public:
  sub_D_constant_DCon_Number(int arg_0);
  sub_D_constant_DCon_Number(sub_D_constant_DCon_Number const& other); 
  sub_D_constant_DCon_Number(sub_D_constant_DCon_Number&& other);
  virtual ~sub_D_constant_DCon_Number(); 
  
  int get_arg_0();
  void set_arg_0(int arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_constant_DCon_Number& arg);
extern bool operator==(sub_D_constant_DCon_Number& lhs, sub_D_constant_DCon_Number& rhs);
extern bool operator<(sub_D_constant_DCon_Number& lhs, sub_D_constant_DCon_Number& rhs);
extern D_constantPtr create_sub_D_constant_DCon_Number(int arg_0);


class sub_D_constant_DCon_Extern : public D_constant { 
protected: 
  sub_D_constant_DCon_Extern();
  StringTableKey arg_0;
  D_typePtr arg_1;
public:
  sub_D_constant_DCon_Extern(StringTableKey arg_0, D_typePtr arg_1);
  sub_D_constant_DCon_Extern(sub_D_constant_DCon_Extern const& other); 
  sub_D_constant_DCon_Extern(sub_D_constant_DCon_Extern&& other);
  virtual ~sub_D_constant_DCon_Extern(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  D_typePtr get_arg_1();
  void set_arg_1(D_typePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_constant_DCon_Extern& arg);
extern bool operator==(sub_D_constant_DCon_Extern& lhs, sub_D_constant_DCon_Extern& rhs);
extern bool operator<(sub_D_constant_DCon_Extern& lhs, sub_D_constant_DCon_Extern& rhs);
extern D_constantPtr create_sub_D_constant_DCon_Extern(StringTableKey arg_0, D_typePtr arg_1);


class sub_D_constant_DCon_Bool : public D_constant { 
protected: 
  sub_D_constant_DCon_Bool();
  bool arg_0;
public:
  sub_D_constant_DCon_Bool(bool arg_0);
  sub_D_constant_DCon_Bool(sub_D_constant_DCon_Bool const& other); 
  sub_D_constant_DCon_Bool(sub_D_constant_DCon_Bool&& other);
  virtual ~sub_D_constant_DCon_Bool(); 
  
  bool get_arg_0();
  void set_arg_0(bool arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_constant_DCon_Bool& arg);
extern bool operator==(sub_D_constant_DCon_Bool& lhs, sub_D_constant_DCon_Bool& rhs);
extern bool operator<(sub_D_constant_DCon_Bool& lhs, sub_D_constant_DCon_Bool& rhs);
extern D_constantPtr create_sub_D_constant_DCon_Bool(bool arg_0);


class sub_D_constant_DCon_EmptyList : public D_constant { 
public:
  sub_D_constant_DCon_EmptyList();
  sub_D_constant_DCon_EmptyList(sub_D_constant_DCon_EmptyList const& other);
  sub_D_constant_DCon_EmptyList(sub_D_constant_DCon_EmptyList&& other); 
  virtual ~sub_D_constant_DCon_EmptyList();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_constant_DCon_EmptyList& arg);
extern D_constantPtr create_sub_D_constant_DCon_EmptyList();


class sub_D_constant_DCon_CoordRef : public D_constant { 
public:
  sub_D_constant_DCon_CoordRef();
  sub_D_constant_DCon_CoordRef(sub_D_constant_DCon_CoordRef const& other);
  sub_D_constant_DCon_CoordRef(sub_D_constant_DCon_CoordRef&& other); 
  virtual ~sub_D_constant_DCon_CoordRef();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_constant_DCon_CoordRef& arg);
extern D_constantPtr create_sub_D_constant_DCon_CoordRef();


class D_pattern { 
public:
  enum class D_pattern_kind { Unknown, DP_Constant, DP_Ignore, DP_VarDef, DP_ListPattern, DP_TuplePattern, DP_Tree, DP_Constructor}; 
protected: 
  D_pattern_kind kind; 

D_pattern(); 
public: 
  virtual ~D_pattern();
  D_pattern(D_pattern const& other);
  D_pattern(D_pattern::D_pattern_kind kind);
  D_pattern(D_pattern&& other);
  
  D_pattern_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_pattern* arg); 
extern bool operator<(D_patternPtr lhs, D_patternPtr rhs);
extern bool operator==(D_patternPtr lhs, D_patternPtr rhs);

class sub_D_pattern_DP_Constant : public D_pattern { 
protected: 
  sub_D_pattern_DP_Constant();
  D_constantPtr arg_0;
public:
  sub_D_pattern_DP_Constant(D_constantPtr arg_0);
  sub_D_pattern_DP_Constant(sub_D_pattern_DP_Constant const& other); 
  sub_D_pattern_DP_Constant(sub_D_pattern_DP_Constant&& other);
  virtual ~sub_D_pattern_DP_Constant(); 
  
  D_constantPtr get_arg_0();
  void set_arg_0(D_constantPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_pattern_DP_Constant& arg);
extern bool operator==(sub_D_pattern_DP_Constant& lhs, sub_D_pattern_DP_Constant& rhs);
extern bool operator<(sub_D_pattern_DP_Constant& lhs, sub_D_pattern_DP_Constant& rhs);
extern D_patternPtr create_sub_D_pattern_DP_Constant(D_constantPtr arg_0);


class sub_D_pattern_DP_Ignore : public D_pattern { 
public:
  sub_D_pattern_DP_Ignore();
  sub_D_pattern_DP_Ignore(sub_D_pattern_DP_Ignore const& other);
  sub_D_pattern_DP_Ignore(sub_D_pattern_DP_Ignore&& other); 
  virtual ~sub_D_pattern_DP_Ignore();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_pattern_DP_Ignore& arg);
extern D_patternPtr create_sub_D_pattern_DP_Ignore();


class sub_D_pattern_DP_VarDef : public D_pattern { 
protected: 
  sub_D_pattern_DP_VarDef();
  StringTableKey arg_0;
public:
  sub_D_pattern_DP_VarDef(StringTableKey arg_0);
  sub_D_pattern_DP_VarDef(sub_D_pattern_DP_VarDef const& other); 
  sub_D_pattern_DP_VarDef(sub_D_pattern_DP_VarDef&& other);
  virtual ~sub_D_pattern_DP_VarDef(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_pattern_DP_VarDef& arg);
extern bool operator==(sub_D_pattern_DP_VarDef& lhs, sub_D_pattern_DP_VarDef& rhs);
extern bool operator<(sub_D_pattern_DP_VarDef& lhs, sub_D_pattern_DP_VarDef& rhs);
extern D_patternPtr create_sub_D_pattern_DP_VarDef(StringTableKey arg_0);


class sub_D_pattern_DP_ListPattern : public D_pattern { 
protected: 
  sub_D_pattern_DP_ListPattern();
  StringTableKey arg_0;
  StringTableKey arg_1;
public:
  sub_D_pattern_DP_ListPattern(StringTableKey arg_0, StringTableKey arg_1);
  sub_D_pattern_DP_ListPattern(sub_D_pattern_DP_ListPattern const& other); 
  sub_D_pattern_DP_ListPattern(sub_D_pattern_DP_ListPattern&& other);
  virtual ~sub_D_pattern_DP_ListPattern(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_pattern_DP_ListPattern& arg);
extern bool operator==(sub_D_pattern_DP_ListPattern& lhs, sub_D_pattern_DP_ListPattern& rhs);
extern bool operator<(sub_D_pattern_DP_ListPattern& lhs, sub_D_pattern_DP_ListPattern& rhs);
extern D_patternPtr create_sub_D_pattern_DP_ListPattern(StringTableKey arg_0, StringTableKey arg_1);


class sub_D_pattern_DP_TuplePattern : public D_pattern { 
protected: 
  sub_D_pattern_DP_TuplePattern();
  StringTableKeyList arg_0;
public:
  sub_D_pattern_DP_TuplePattern(StringTableKeyList arg_0);
  sub_D_pattern_DP_TuplePattern(sub_D_pattern_DP_TuplePattern const& other); 
  sub_D_pattern_DP_TuplePattern(sub_D_pattern_DP_TuplePattern&& other);
  virtual ~sub_D_pattern_DP_TuplePattern(); 
  
  StringTableKeyList get_arg_0();
  void set_arg_0(StringTableKeyList arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_pattern_DP_TuplePattern& arg);
extern bool operator==(sub_D_pattern_DP_TuplePattern& lhs, sub_D_pattern_DP_TuplePattern& rhs);
extern bool operator<(sub_D_pattern_DP_TuplePattern& lhs, sub_D_pattern_DP_TuplePattern& rhs);
extern D_patternPtr create_sub_D_pattern_DP_TuplePattern(StringTableKeyList arg_0);


class sub_D_pattern_DP_Tree : public D_pattern { 
protected: 
  sub_D_pattern_DP_Tree();
  StringTableKey arg_0;
  StringTableKey arg_1;
public:
  sub_D_pattern_DP_Tree(StringTableKey arg_0, StringTableKey arg_1);
  sub_D_pattern_DP_Tree(sub_D_pattern_DP_Tree const& other); 
  sub_D_pattern_DP_Tree(sub_D_pattern_DP_Tree&& other);
  virtual ~sub_D_pattern_DP_Tree(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_pattern_DP_Tree& arg);
extern bool operator==(sub_D_pattern_DP_Tree& lhs, sub_D_pattern_DP_Tree& rhs);
extern bool operator<(sub_D_pattern_DP_Tree& lhs, sub_D_pattern_DP_Tree& rhs);
extern D_patternPtr create_sub_D_pattern_DP_Tree(StringTableKey arg_0, StringTableKey arg_1);


class sub_D_pattern_DP_Constructor : public D_pattern { 
protected: 
  sub_D_pattern_DP_Constructor();
  StringTableKey arg_0;
  StringTableKeyList arg_1;
public:
  sub_D_pattern_DP_Constructor(StringTableKey arg_0, StringTableKeyList arg_1);
  sub_D_pattern_DP_Constructor(sub_D_pattern_DP_Constructor const& other); 
  sub_D_pattern_DP_Constructor(sub_D_pattern_DP_Constructor&& other);
  virtual ~sub_D_pattern_DP_Constructor(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  StringTableKeyList get_arg_1();
  void set_arg_1(StringTableKeyList arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_pattern_DP_Constructor& arg);
extern bool operator==(sub_D_pattern_DP_Constructor& lhs, sub_D_pattern_DP_Constructor& rhs);
extern bool operator<(sub_D_pattern_DP_Constructor& lhs, sub_D_pattern_DP_Constructor& rhs);
extern D_patternPtr create_sub_D_pattern_DP_Constructor(StringTableKey arg_0, StringTableKeyList arg_1);


class D_type { 
public:
  enum class D_type_kind { Unknown, DT_Var, DT_List, DT_Tuple, DT_Type, DT_Fun, DT_Map, DT_Alternative, DT_Record, DT_Unknown, DT_Unit, DT_Invalid}; 
protected: 
  D_type_kind kind; 

D_type(); 
public: 
  virtual ~D_type();
  D_type(D_type const& other);
  D_type(D_type::D_type_kind kind);
  D_type(D_type&& other);
  
  D_type_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_type* arg); 
extern bool operator<(D_typePtr lhs, D_typePtr rhs);
extern bool operator==(D_typePtr lhs, D_typePtr rhs);

class sub_D_type_DT_Var : public D_type { 
protected: 
  sub_D_type_DT_Var();
  StringTableKey arg_0;
public:
  sub_D_type_DT_Var(StringTableKey arg_0);
  sub_D_type_DT_Var(sub_D_type_DT_Var const& other); 
  sub_D_type_DT_Var(sub_D_type_DT_Var&& other);
  virtual ~sub_D_type_DT_Var(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Var& arg);
extern bool operator==(sub_D_type_DT_Var& lhs, sub_D_type_DT_Var& rhs);
extern bool operator<(sub_D_type_DT_Var& lhs, sub_D_type_DT_Var& rhs);
extern D_typePtr create_sub_D_type_DT_Var(StringTableKey arg_0);


class sub_D_type_DT_List : public D_type { 
protected: 
  sub_D_type_DT_List();
  D_typePtr arg_0;
public:
  sub_D_type_DT_List(D_typePtr arg_0);
  sub_D_type_DT_List(sub_D_type_DT_List const& other); 
  sub_D_type_DT_List(sub_D_type_DT_List&& other);
  virtual ~sub_D_type_DT_List(); 
  
  D_typePtr get_arg_0();
  void set_arg_0(D_typePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_List& arg);
extern bool operator==(sub_D_type_DT_List& lhs, sub_D_type_DT_List& rhs);
extern bool operator<(sub_D_type_DT_List& lhs, sub_D_type_DT_List& rhs);
extern D_typePtr create_sub_D_type_DT_List(D_typePtr arg_0);


class sub_D_type_DT_Tuple : public D_type { 
protected: 
  sub_D_type_DT_Tuple();
  D_types arg_0;
public:
  sub_D_type_DT_Tuple(D_types arg_0);
  sub_D_type_DT_Tuple(sub_D_type_DT_Tuple const& other); 
  sub_D_type_DT_Tuple(sub_D_type_DT_Tuple&& other);
  virtual ~sub_D_type_DT_Tuple(); 
  
  D_types get_arg_0();
  void set_arg_0(D_types arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Tuple& arg);
extern bool operator==(sub_D_type_DT_Tuple& lhs, sub_D_type_DT_Tuple& rhs);
extern bool operator<(sub_D_type_DT_Tuple& lhs, sub_D_type_DT_Tuple& rhs);
extern D_typePtr create_sub_D_type_DT_Tuple(D_types arg_0);


class sub_D_type_DT_Type : public D_type { 
protected: 
  sub_D_type_DT_Type();
  StringTableKey arg_0;
public:
  sub_D_type_DT_Type(StringTableKey arg_0);
  sub_D_type_DT_Type(sub_D_type_DT_Type const& other); 
  sub_D_type_DT_Type(sub_D_type_DT_Type&& other);
  virtual ~sub_D_type_DT_Type(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Type& arg);
extern bool operator==(sub_D_type_DT_Type& lhs, sub_D_type_DT_Type& rhs);
extern bool operator<(sub_D_type_DT_Type& lhs, sub_D_type_DT_Type& rhs);
extern D_typePtr create_sub_D_type_DT_Type(StringTableKey arg_0);


class sub_D_type_DT_Fun : public D_type { 
protected: 
  sub_D_type_DT_Fun();
  D_typePtr arg_0;
  D_typePtr arg_1;
public:
  sub_D_type_DT_Fun(D_typePtr arg_0, D_typePtr arg_1);
  sub_D_type_DT_Fun(sub_D_type_DT_Fun const& other); 
  sub_D_type_DT_Fun(sub_D_type_DT_Fun&& other);
  virtual ~sub_D_type_DT_Fun(); 
  
  D_typePtr get_arg_0();
  void set_arg_0(D_typePtr arg);
  D_typePtr get_arg_1();
  void set_arg_1(D_typePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Fun& arg);
extern bool operator==(sub_D_type_DT_Fun& lhs, sub_D_type_DT_Fun& rhs);
extern bool operator<(sub_D_type_DT_Fun& lhs, sub_D_type_DT_Fun& rhs);
extern D_typePtr create_sub_D_type_DT_Fun(D_typePtr arg_0, D_typePtr arg_1);


class sub_D_type_DT_Map : public D_type { 
protected: 
  sub_D_type_DT_Map();
  D_typePtr arg_0;
  D_typePtr arg_1;
public:
  sub_D_type_DT_Map(D_typePtr arg_0, D_typePtr arg_1);
  sub_D_type_DT_Map(sub_D_type_DT_Map const& other); 
  sub_D_type_DT_Map(sub_D_type_DT_Map&& other);
  virtual ~sub_D_type_DT_Map(); 
  
  D_typePtr get_arg_0();
  void set_arg_0(D_typePtr arg);
  D_typePtr get_arg_1();
  void set_arg_1(D_typePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Map& arg);
extern bool operator==(sub_D_type_DT_Map& lhs, sub_D_type_DT_Map& rhs);
extern bool operator<(sub_D_type_DT_Map& lhs, sub_D_type_DT_Map& rhs);
extern D_typePtr create_sub_D_type_DT_Map(D_typePtr arg_0, D_typePtr arg_1);


class sub_D_type_DT_Alternative : public D_type { 
protected: 
  sub_D_type_DT_Alternative();
  D_typePtr arg_0;
  D_typePtr arg_1;
public:
  sub_D_type_DT_Alternative(D_typePtr arg_0, D_typePtr arg_1);
  sub_D_type_DT_Alternative(sub_D_type_DT_Alternative const& other); 
  sub_D_type_DT_Alternative(sub_D_type_DT_Alternative&& other);
  virtual ~sub_D_type_DT_Alternative(); 
  
  D_typePtr get_arg_0();
  void set_arg_0(D_typePtr arg);
  D_typePtr get_arg_1();
  void set_arg_1(D_typePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Alternative& arg);
extern bool operator==(sub_D_type_DT_Alternative& lhs, sub_D_type_DT_Alternative& rhs);
extern bool operator<(sub_D_type_DT_Alternative& lhs, sub_D_type_DT_Alternative& rhs);
extern D_typePtr create_sub_D_type_DT_Alternative(D_typePtr arg_0, D_typePtr arg_1);


class sub_D_type_DT_Record : public D_type { 
protected: 
  sub_D_type_DT_Record();
  D_typePtr arg_0;
public:
  sub_D_type_DT_Record(D_typePtr arg_0);
  sub_D_type_DT_Record(sub_D_type_DT_Record const& other); 
  sub_D_type_DT_Record(sub_D_type_DT_Record&& other);
  virtual ~sub_D_type_DT_Record(); 
  
  D_typePtr get_arg_0();
  void set_arg_0(D_typePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Record& arg);
extern bool operator==(sub_D_type_DT_Record& lhs, sub_D_type_DT_Record& rhs);
extern bool operator<(sub_D_type_DT_Record& lhs, sub_D_type_DT_Record& rhs);
extern D_typePtr create_sub_D_type_DT_Record(D_typePtr arg_0);


class sub_D_type_DT_Unknown : public D_type { 
public:
  sub_D_type_DT_Unknown();
  sub_D_type_DT_Unknown(sub_D_type_DT_Unknown const& other);
  sub_D_type_DT_Unknown(sub_D_type_DT_Unknown&& other); 
  virtual ~sub_D_type_DT_Unknown();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Unknown& arg);
extern D_typePtr create_sub_D_type_DT_Unknown();


class sub_D_type_DT_Unit : public D_type { 
public:
  sub_D_type_DT_Unit();
  sub_D_type_DT_Unit(sub_D_type_DT_Unit const& other);
  sub_D_type_DT_Unit(sub_D_type_DT_Unit&& other); 
  virtual ~sub_D_type_DT_Unit();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Unit& arg);
extern D_typePtr create_sub_D_type_DT_Unit();


class sub_D_type_DT_Invalid : public D_type { 
public:
  sub_D_type_DT_Invalid();
  sub_D_type_DT_Invalid(sub_D_type_DT_Invalid const& other);
  sub_D_type_DT_Invalid(sub_D_type_DT_Invalid&& other); 
  virtual ~sub_D_type_DT_Invalid();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_type_DT_Invalid& arg);
extern D_typePtr create_sub_D_type_DT_Invalid();


class D_patternType { 
public:
  enum class D_patternType_kind { Unknown, DPT_Ignore, DPT_Type, DPT_Cons, DPT_Nil, DPT_AllList, DPT_Unknown, DPT_Invalid}; 
protected: 
  D_patternType_kind kind; 

D_patternType(); 
public: 
  virtual ~D_patternType();
  D_patternType(D_patternType const& other);
  D_patternType(D_patternType::D_patternType_kind kind);
  D_patternType(D_patternType&& other);
  
  D_patternType_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_patternType* arg); 
extern bool operator<(D_patternTypePtr lhs, D_patternTypePtr rhs);
extern bool operator==(D_patternTypePtr lhs, D_patternTypePtr rhs);

class sub_D_patternType_DPT_Ignore : public D_patternType { 
public:
  sub_D_patternType_DPT_Ignore();
  sub_D_patternType_DPT_Ignore(sub_D_patternType_DPT_Ignore const& other);
  sub_D_patternType_DPT_Ignore(sub_D_patternType_DPT_Ignore&& other); 
  virtual ~sub_D_patternType_DPT_Ignore();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_patternType_DPT_Ignore& arg);
extern D_patternTypePtr create_sub_D_patternType_DPT_Ignore();


class sub_D_patternType_DPT_Type : public D_patternType { 
protected: 
  sub_D_patternType_DPT_Type();
  StringTableKey arg_0;
  StringTableKeyList arg_1;
  StringTableKeyList arg_2;
public:
  sub_D_patternType_DPT_Type(StringTableKey arg_0, StringTableKeyList arg_1, StringTableKeyList arg_2);
  sub_D_patternType_DPT_Type(sub_D_patternType_DPT_Type const& other); 
  sub_D_patternType_DPT_Type(sub_D_patternType_DPT_Type&& other);
  virtual ~sub_D_patternType_DPT_Type(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  StringTableKeyList get_arg_1();
  void set_arg_1(StringTableKeyList arg);
  StringTableKeyList get_arg_2();
  void set_arg_2(StringTableKeyList arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_patternType_DPT_Type& arg);
extern bool operator==(sub_D_patternType_DPT_Type& lhs, sub_D_patternType_DPT_Type& rhs);
extern bool operator<(sub_D_patternType_DPT_Type& lhs, sub_D_patternType_DPT_Type& rhs);
extern D_patternTypePtr create_sub_D_patternType_DPT_Type(StringTableKey arg_0, StringTableKeyList arg_1, StringTableKeyList arg_2);


class sub_D_patternType_DPT_Cons : public D_patternType { 
public:
  sub_D_patternType_DPT_Cons();
  sub_D_patternType_DPT_Cons(sub_D_patternType_DPT_Cons const& other);
  sub_D_patternType_DPT_Cons(sub_D_patternType_DPT_Cons&& other); 
  virtual ~sub_D_patternType_DPT_Cons();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_patternType_DPT_Cons& arg);
extern D_patternTypePtr create_sub_D_patternType_DPT_Cons();


class sub_D_patternType_DPT_Nil : public D_patternType { 
public:
  sub_D_patternType_DPT_Nil();
  sub_D_patternType_DPT_Nil(sub_D_patternType_DPT_Nil const& other);
  sub_D_patternType_DPT_Nil(sub_D_patternType_DPT_Nil&& other); 
  virtual ~sub_D_patternType_DPT_Nil();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_patternType_DPT_Nil& arg);
extern D_patternTypePtr create_sub_D_patternType_DPT_Nil();


class sub_D_patternType_DPT_AllList : public D_patternType { 
public:
  sub_D_patternType_DPT_AllList();
  sub_D_patternType_DPT_AllList(sub_D_patternType_DPT_AllList const& other);
  sub_D_patternType_DPT_AllList(sub_D_patternType_DPT_AllList&& other); 
  virtual ~sub_D_patternType_DPT_AllList();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_patternType_DPT_AllList& arg);
extern D_patternTypePtr create_sub_D_patternType_DPT_AllList();


class sub_D_patternType_DPT_Unknown : public D_patternType { 
public:
  sub_D_patternType_DPT_Unknown();
  sub_D_patternType_DPT_Unknown(sub_D_patternType_DPT_Unknown const& other);
  sub_D_patternType_DPT_Unknown(sub_D_patternType_DPT_Unknown&& other); 
  virtual ~sub_D_patternType_DPT_Unknown();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_patternType_DPT_Unknown& arg);
extern D_patternTypePtr create_sub_D_patternType_DPT_Unknown();


class sub_D_patternType_DPT_Invalid : public D_patternType { 
public:
  sub_D_patternType_DPT_Invalid();
  sub_D_patternType_DPT_Invalid(sub_D_patternType_DPT_Invalid const& other);
  sub_D_patternType_DPT_Invalid(sub_D_patternType_DPT_Invalid&& other); 
  virtual ~sub_D_patternType_DPT_Invalid();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_patternType_DPT_Invalid& arg);
extern D_patternTypePtr create_sub_D_patternType_DPT_Invalid();


class D_PatternCode { 
public:
  enum class D_PatternCode_kind { Unknown, D_PatternCode}; 
protected: 
  D_PatternCode_kind kind; 

D_PatternCode(); 
public: 
  virtual ~D_PatternCode();
  D_PatternCode(D_PatternCode const& other);
  D_PatternCode(D_PatternCode::D_PatternCode_kind kind);
  D_PatternCode(D_PatternCode&& other);
  
  D_PatternCode_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_PatternCode* arg); 
extern bool operator<(D_PatternCodePtr lhs, D_PatternCodePtr rhs);
extern bool operator==(D_PatternCodePtr lhs, D_PatternCodePtr rhs);

class sub_D_PatternCode_D_PatternCode : public D_PatternCode { 
protected: 
  sub_D_PatternCode_D_PatternCode();
  D_patterns arg_0;
  PTGNode arg_1;
  PTGNode arg_2;
  bool arg_3;
  bool arg_4;
  bool arg_5;
  bool arg_6;
public:
  sub_D_PatternCode_D_PatternCode(D_patterns arg_0, PTGNode arg_1, PTGNode arg_2, bool arg_3, bool arg_4, bool arg_5, bool arg_6);
  sub_D_PatternCode_D_PatternCode(sub_D_PatternCode_D_PatternCode const& other); 
  sub_D_PatternCode_D_PatternCode(sub_D_PatternCode_D_PatternCode&& other);
  virtual ~sub_D_PatternCode_D_PatternCode(); 
  
  D_patterns get_arg_0();
  void set_arg_0(D_patterns arg);
  PTGNode get_arg_1();
  void set_arg_1(PTGNode arg);
  PTGNode get_arg_2();
  void set_arg_2(PTGNode arg);
  bool get_arg_3();
  void set_arg_3(bool arg);
  bool get_arg_4();
  void set_arg_4(bool arg);
  bool get_arg_5();
  void set_arg_5(bool arg);
  bool get_arg_6();
  void set_arg_6(bool arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_PatternCode_D_PatternCode& arg);
extern bool operator==(sub_D_PatternCode_D_PatternCode& lhs, sub_D_PatternCode_D_PatternCode& rhs);
extern bool operator<(sub_D_PatternCode_D_PatternCode& lhs, sub_D_PatternCode_D_PatternCode& rhs);
extern D_PatternCodePtr create_sub_D_PatternCode_D_PatternCode(D_patterns arg_0, PTGNode arg_1, PTGNode arg_2, bool arg_3, bool arg_4, bool arg_5, bool arg_6);


class D_funCode { 
public:
  enum class D_funCode_kind { Unknown, D_funCode}; 
protected: 
  D_funCode_kind kind; 

D_funCode(); 
public: 
  virtual ~D_funCode();
  D_funCode(D_funCode const& other);
  D_funCode(D_funCode::D_funCode_kind kind);
  D_funCode(D_funCode&& other);
  
  D_funCode_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_funCode* arg); 
extern bool operator<(D_funCodePtr lhs, D_funCodePtr rhs);
extern bool operator==(D_funCodePtr lhs, D_funCodePtr rhs);

class sub_D_funCode_D_funCode : public D_funCode { 
protected: 
  sub_D_funCode_D_funCode();
  PTGNode arg_0;
  PTGNode arg_1;
  bool arg_2;
public:
  sub_D_funCode_D_funCode(PTGNode arg_0, PTGNode arg_1, bool arg_2);
  sub_D_funCode_D_funCode(sub_D_funCode_D_funCode const& other); 
  sub_D_funCode_D_funCode(sub_D_funCode_D_funCode&& other);
  virtual ~sub_D_funCode_D_funCode(); 
  
  PTGNode get_arg_0();
  void set_arg_0(PTGNode arg);
  PTGNode get_arg_1();
  void set_arg_1(PTGNode arg);
  bool get_arg_2();
  void set_arg_2(bool arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_funCode_D_funCode& arg);
extern bool operator==(sub_D_funCode_D_funCode& lhs, sub_D_funCode_D_funCode& rhs);
extern bool operator<(sub_D_funCode_D_funCode& lhs, sub_D_funCode_D_funCode& rhs);
extern D_funCodePtr create_sub_D_funCode_D_funCode(PTGNode arg_0, PTGNode arg_1, bool arg_2);


class D_ConArg { 
public:
  enum class D_ConArg_kind { Unknown, D_ConArg}; 
protected: 
  D_ConArg_kind kind; 

D_ConArg(); 
public: 
  virtual ~D_ConArg();
  D_ConArg(D_ConArg const& other);
  D_ConArg(D_ConArg::D_ConArg_kind kind);
  D_ConArg(D_ConArg&& other);
  
  D_ConArg_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_ConArg* arg); 
extern bool operator<(D_ConArgPtr lhs, D_ConArgPtr rhs);
extern bool operator==(D_ConArgPtr lhs, D_ConArgPtr rhs);

class sub_D_ConArg_D_ConArg : public D_ConArg { 
protected: 
  sub_D_ConArg_D_ConArg();
  StringTableKey arg_0;
  PTGNode arg_1;
  Binding arg_2;
  Binding arg_3;
  Environment arg_4;
  StringTableKey arg_5;
public:
  sub_D_ConArg_D_ConArg(StringTableKey arg_0, PTGNode arg_1, Binding arg_2, Binding arg_3, Environment arg_4, StringTableKey arg_5);
  sub_D_ConArg_D_ConArg(sub_D_ConArg_D_ConArg const& other); 
  sub_D_ConArg_D_ConArg(sub_D_ConArg_D_ConArg&& other);
  virtual ~sub_D_ConArg_D_ConArg(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  PTGNode get_arg_1();
  void set_arg_1(PTGNode arg);
  Binding get_arg_2();
  void set_arg_2(Binding arg);
  Binding get_arg_3();
  void set_arg_3(Binding arg);
  Environment get_arg_4();
  void set_arg_4(Environment arg);
  StringTableKey get_arg_5();
  void set_arg_5(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ConArg_D_ConArg& arg);
extern bool operator==(sub_D_ConArg_D_ConArg& lhs, sub_D_ConArg_D_ConArg& rhs);
extern bool operator<(sub_D_ConArg_D_ConArg& lhs, sub_D_ConArg_D_ConArg& rhs);
extern D_ConArgPtr create_sub_D_ConArg_D_ConArg(StringTableKey arg_0, PTGNode arg_1, Binding arg_2, Binding arg_3, Environment arg_4, StringTableKey arg_5);


class D_ProdSymbol { 
public:
  enum class D_ProdSymbol_kind { Unknown, TreeSymbol, LiteralSymbol, GenSymbol}; 
protected: 
  D_ProdSymbol_kind kind; 

D_ProdSymbol(); 
public: 
  virtual ~D_ProdSymbol();
  D_ProdSymbol(D_ProdSymbol const& other);
  D_ProdSymbol(D_ProdSymbol::D_ProdSymbol_kind kind);
  D_ProdSymbol(D_ProdSymbol&& other);
  
  D_ProdSymbol_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_ProdSymbol* arg); 
extern bool operator<(D_ProdSymbolPtr lhs, D_ProdSymbolPtr rhs);
extern bool operator==(D_ProdSymbolPtr lhs, D_ProdSymbolPtr rhs);

class sub_D_ProdSymbol_TreeSymbol : public D_ProdSymbol { 
protected: 
  sub_D_ProdSymbol_TreeSymbol();
  StringTableKey arg_0;
  StringTableKey arg_1;
  int arg_2;
  int arg_3;
public:
  sub_D_ProdSymbol_TreeSymbol(StringTableKey arg_0, StringTableKey arg_1, int arg_2, int arg_3);
  sub_D_ProdSymbol_TreeSymbol(sub_D_ProdSymbol_TreeSymbol const& other); 
  sub_D_ProdSymbol_TreeSymbol(sub_D_ProdSymbol_TreeSymbol&& other);
  virtual ~sub_D_ProdSymbol_TreeSymbol(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
  int get_arg_2();
  void set_arg_2(int arg);
  int get_arg_3();
  void set_arg_3(int arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdSymbol_TreeSymbol& arg);
extern bool operator==(sub_D_ProdSymbol_TreeSymbol& lhs, sub_D_ProdSymbol_TreeSymbol& rhs);
extern bool operator<(sub_D_ProdSymbol_TreeSymbol& lhs, sub_D_ProdSymbol_TreeSymbol& rhs);
extern D_ProdSymbolPtr create_sub_D_ProdSymbol_TreeSymbol(StringTableKey arg_0, StringTableKey arg_1, int arg_2, int arg_3);


class sub_D_ProdSymbol_LiteralSymbol : public D_ProdSymbol { 
protected: 
  sub_D_ProdSymbol_LiteralSymbol();
  StringTableKey arg_0;
  int arg_1;
public:
  sub_D_ProdSymbol_LiteralSymbol(StringTableKey arg_0, int arg_1);
  sub_D_ProdSymbol_LiteralSymbol(sub_D_ProdSymbol_LiteralSymbol const& other); 
  sub_D_ProdSymbol_LiteralSymbol(sub_D_ProdSymbol_LiteralSymbol&& other);
  virtual ~sub_D_ProdSymbol_LiteralSymbol(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  int get_arg_1();
  void set_arg_1(int arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdSymbol_LiteralSymbol& arg);
extern bool operator==(sub_D_ProdSymbol_LiteralSymbol& lhs, sub_D_ProdSymbol_LiteralSymbol& rhs);
extern bool operator<(sub_D_ProdSymbol_LiteralSymbol& lhs, sub_D_ProdSymbol_LiteralSymbol& rhs);
extern D_ProdSymbolPtr create_sub_D_ProdSymbol_LiteralSymbol(StringTableKey arg_0, int arg_1);


class sub_D_ProdSymbol_GenSymbol : public D_ProdSymbol { 
protected: 
  sub_D_ProdSymbol_GenSymbol();
  StringTableKey arg_0;
  StringTableKey arg_1;
  int arg_2;
  int arg_3;
public:
  sub_D_ProdSymbol_GenSymbol(StringTableKey arg_0, StringTableKey arg_1, int arg_2, int arg_3);
  sub_D_ProdSymbol_GenSymbol(sub_D_ProdSymbol_GenSymbol const& other); 
  sub_D_ProdSymbol_GenSymbol(sub_D_ProdSymbol_GenSymbol&& other);
  virtual ~sub_D_ProdSymbol_GenSymbol(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
  int get_arg_2();
  void set_arg_2(int arg);
  int get_arg_3();
  void set_arg_3(int arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdSymbol_GenSymbol& arg);
extern bool operator==(sub_D_ProdSymbol_GenSymbol& lhs, sub_D_ProdSymbol_GenSymbol& rhs);
extern bool operator<(sub_D_ProdSymbol_GenSymbol& lhs, sub_D_ProdSymbol_GenSymbol& rhs);
extern D_ProdSymbolPtr create_sub_D_ProdSymbol_GenSymbol(StringTableKey arg_0, StringTableKey arg_1, int arg_2, int arg_3);


class TreeSymb { 
public:
  enum class TreeSymb_kind { Unknown, TreeSymb}; 
protected: 
  TreeSymb_kind kind; 

TreeSymb(); 
public: 
  virtual ~TreeSymb();
  TreeSymb(TreeSymb const& other);
  TreeSymb(TreeSymb::TreeSymb_kind kind);
  TreeSymb(TreeSymb&& other);
  
  TreeSymb_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, TreeSymb* arg); 
extern bool operator<(TreeSymbPtr lhs, TreeSymbPtr rhs);
extern bool operator==(TreeSymbPtr lhs, TreeSymbPtr rhs);

class sub_TreeSymb_TreeSymb : public TreeSymb { 
protected: 
  sub_TreeSymb_TreeSymb();
  Binding arg_0;
  StringTableKey arg_1;
public:
  sub_TreeSymb_TreeSymb(Binding arg_0, StringTableKey arg_1);
  sub_TreeSymb_TreeSymb(sub_TreeSymb_TreeSymb const& other); 
  sub_TreeSymb_TreeSymb(sub_TreeSymb_TreeSymb&& other);
  virtual ~sub_TreeSymb_TreeSymb(); 
  
  Binding get_arg_0();
  void set_arg_0(Binding arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_TreeSymb_TreeSymb& arg);
extern bool operator==(sub_TreeSymb_TreeSymb& lhs, sub_TreeSymb_TreeSymb& rhs);
extern bool operator<(sub_TreeSymb_TreeSymb& lhs, sub_TreeSymb_TreeSymb& rhs);
extern TreeSymbPtr create_sub_TreeSymb_TreeSymb(Binding arg_0, StringTableKey arg_1);


class D_SymbolAttribute { 
public:
  enum class D_SymbolAttribute_kind { Unknown, SA_Local, SA_Remote}; 
protected: 
  D_SymbolAttribute_kind kind; 

D_SymbolAttribute(); 
public: 
  virtual ~D_SymbolAttribute();
  D_SymbolAttribute(D_SymbolAttribute const& other);
  D_SymbolAttribute(D_SymbolAttribute::D_SymbolAttribute_kind kind);
  D_SymbolAttribute(D_SymbolAttribute&& other);
  
  D_SymbolAttribute_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_SymbolAttribute* arg); 
extern bool operator<(D_SymbolAttributePtr lhs, D_SymbolAttributePtr rhs);
extern bool operator==(D_SymbolAttributePtr lhs, D_SymbolAttributePtr rhs);

class sub_D_SymbolAttribute_SA_Local : public D_SymbolAttribute { 
protected: 
  sub_D_SymbolAttribute_SA_Local();
  D_SymbolLocalAttributePtr arg_0;
public:
  sub_D_SymbolAttribute_SA_Local(D_SymbolLocalAttributePtr arg_0);
  sub_D_SymbolAttribute_SA_Local(sub_D_SymbolAttribute_SA_Local const& other); 
  sub_D_SymbolAttribute_SA_Local(sub_D_SymbolAttribute_SA_Local&& other);
  virtual ~sub_D_SymbolAttribute_SA_Local(); 
  
  D_SymbolLocalAttributePtr get_arg_0();
  void set_arg_0(D_SymbolLocalAttributePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolAttribute_SA_Local& arg);
extern bool operator==(sub_D_SymbolAttribute_SA_Local& lhs, sub_D_SymbolAttribute_SA_Local& rhs);
extern bool operator<(sub_D_SymbolAttribute_SA_Local& lhs, sub_D_SymbolAttribute_SA_Local& rhs);
extern D_SymbolAttributePtr create_sub_D_SymbolAttribute_SA_Local(D_SymbolLocalAttributePtr arg_0);


class sub_D_SymbolAttribute_SA_Remote : public D_SymbolAttribute { 
protected: 
  sub_D_SymbolAttribute_SA_Remote();
  D_RemoteAttributePtr arg_0;
public:
  sub_D_SymbolAttribute_SA_Remote(D_RemoteAttributePtr arg_0);
  sub_D_SymbolAttribute_SA_Remote(sub_D_SymbolAttribute_SA_Remote const& other); 
  sub_D_SymbolAttribute_SA_Remote(sub_D_SymbolAttribute_SA_Remote&& other);
  virtual ~sub_D_SymbolAttribute_SA_Remote(); 
  
  D_RemoteAttributePtr get_arg_0();
  void set_arg_0(D_RemoteAttributePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolAttribute_SA_Remote& arg);
extern bool operator==(sub_D_SymbolAttribute_SA_Remote& lhs, sub_D_SymbolAttribute_SA_Remote& rhs);
extern bool operator<(sub_D_SymbolAttribute_SA_Remote& lhs, sub_D_SymbolAttribute_SA_Remote& rhs);
extern D_SymbolAttributePtr create_sub_D_SymbolAttribute_SA_Remote(D_RemoteAttributePtr arg_0);


class D_SymbolComputation { 
public:
  enum class D_SymbolComputation_kind { Unknown, SC_DefOcc, SC_ChainStart, SC_Expression, SC_Ordered, SC_Return}; 
protected: 
  D_SymbolComputation_kind kind; 

D_SymbolComputation(); 
public: 
  virtual ~D_SymbolComputation();
  D_SymbolComputation(D_SymbolComputation const& other);
  D_SymbolComputation(D_SymbolComputation::D_SymbolComputation_kind kind);
  D_SymbolComputation(D_SymbolComputation&& other);
  
  D_SymbolComputation_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_SymbolComputation* arg); 
extern bool operator<(D_SymbolComputationPtr lhs, D_SymbolComputationPtr rhs);
extern bool operator==(D_SymbolComputationPtr lhs, D_SymbolComputationPtr rhs);

class sub_D_SymbolComputation_SC_DefOcc : public D_SymbolComputation { 
protected: 
  sub_D_SymbolComputation_SC_DefOcc();
  CoordPtr arg_0;
  D_SymbolAttributePtr arg_1;
  D_SymbolAttributes arg_2;
  D_SymbolExpressionPtr arg_3;
public:
  sub_D_SymbolComputation_SC_DefOcc(CoordPtr arg_0, D_SymbolAttributePtr arg_1, D_SymbolAttributes arg_2, D_SymbolExpressionPtr arg_3);
  sub_D_SymbolComputation_SC_DefOcc(sub_D_SymbolComputation_SC_DefOcc const& other); 
  sub_D_SymbolComputation_SC_DefOcc(sub_D_SymbolComputation_SC_DefOcc&& other);
  virtual ~sub_D_SymbolComputation_SC_DefOcc(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_SymbolAttributePtr get_arg_1();
  void set_arg_1(D_SymbolAttributePtr arg);
  D_SymbolAttributes get_arg_2();
  void set_arg_2(D_SymbolAttributes arg);
  D_SymbolExpressionPtr get_arg_3();
  void set_arg_3(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolComputation_SC_DefOcc& arg);
extern bool operator==(sub_D_SymbolComputation_SC_DefOcc& lhs, sub_D_SymbolComputation_SC_DefOcc& rhs);
extern bool operator<(sub_D_SymbolComputation_SC_DefOcc& lhs, sub_D_SymbolComputation_SC_DefOcc& rhs);
extern D_SymbolComputationPtr create_sub_D_SymbolComputation_SC_DefOcc(CoordPtr arg_0, D_SymbolAttributePtr arg_1, D_SymbolAttributes arg_2, D_SymbolExpressionPtr arg_3);


class sub_D_SymbolComputation_SC_ChainStart : public D_SymbolComputation { 
protected: 
  sub_D_SymbolComputation_SC_ChainStart();
  CoordPtr arg_0;
  D_SymbolAttributePtr arg_1;
  D_SymbolAttributes arg_2;
  D_SymbolExpressionPtr arg_3;
public:
  sub_D_SymbolComputation_SC_ChainStart(CoordPtr arg_0, D_SymbolAttributePtr arg_1, D_SymbolAttributes arg_2, D_SymbolExpressionPtr arg_3);
  sub_D_SymbolComputation_SC_ChainStart(sub_D_SymbolComputation_SC_ChainStart const& other); 
  sub_D_SymbolComputation_SC_ChainStart(sub_D_SymbolComputation_SC_ChainStart&& other);
  virtual ~sub_D_SymbolComputation_SC_ChainStart(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_SymbolAttributePtr get_arg_1();
  void set_arg_1(D_SymbolAttributePtr arg);
  D_SymbolAttributes get_arg_2();
  void set_arg_2(D_SymbolAttributes arg);
  D_SymbolExpressionPtr get_arg_3();
  void set_arg_3(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolComputation_SC_ChainStart& arg);
extern bool operator==(sub_D_SymbolComputation_SC_ChainStart& lhs, sub_D_SymbolComputation_SC_ChainStart& rhs);
extern bool operator<(sub_D_SymbolComputation_SC_ChainStart& lhs, sub_D_SymbolComputation_SC_ChainStart& rhs);
extern D_SymbolComputationPtr create_sub_D_SymbolComputation_SC_ChainStart(CoordPtr arg_0, D_SymbolAttributePtr arg_1, D_SymbolAttributes arg_2, D_SymbolExpressionPtr arg_3);


class sub_D_SymbolComputation_SC_Expression : public D_SymbolComputation { 
protected: 
  sub_D_SymbolComputation_SC_Expression();
  CoordPtr arg_0;
  D_SymbolExpressionPtr arg_1;
  D_SymbolAttributes arg_2;
public:
  sub_D_SymbolComputation_SC_Expression(CoordPtr arg_0, D_SymbolExpressionPtr arg_1, D_SymbolAttributes arg_2);
  sub_D_SymbolComputation_SC_Expression(sub_D_SymbolComputation_SC_Expression const& other); 
  sub_D_SymbolComputation_SC_Expression(sub_D_SymbolComputation_SC_Expression&& other);
  virtual ~sub_D_SymbolComputation_SC_Expression(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_SymbolExpressionPtr get_arg_1();
  void set_arg_1(D_SymbolExpressionPtr arg);
  D_SymbolAttributes get_arg_2();
  void set_arg_2(D_SymbolAttributes arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolComputation_SC_Expression& arg);
extern bool operator==(sub_D_SymbolComputation_SC_Expression& lhs, sub_D_SymbolComputation_SC_Expression& rhs);
extern bool operator<(sub_D_SymbolComputation_SC_Expression& lhs, sub_D_SymbolComputation_SC_Expression& rhs);
extern D_SymbolComputationPtr create_sub_D_SymbolComputation_SC_Expression(CoordPtr arg_0, D_SymbolExpressionPtr arg_1, D_SymbolAttributes arg_2);


class sub_D_SymbolComputation_SC_Ordered : public D_SymbolComputation { 
protected: 
  sub_D_SymbolComputation_SC_Ordered();
  CoordPtr arg_0;
  D_SymbolComputations arg_1;
public:
  sub_D_SymbolComputation_SC_Ordered(CoordPtr arg_0, D_SymbolComputations arg_1);
  sub_D_SymbolComputation_SC_Ordered(sub_D_SymbolComputation_SC_Ordered const& other); 
  sub_D_SymbolComputation_SC_Ordered(sub_D_SymbolComputation_SC_Ordered&& other);
  virtual ~sub_D_SymbolComputation_SC_Ordered(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_SymbolComputations get_arg_1();
  void set_arg_1(D_SymbolComputations arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolComputation_SC_Ordered& arg);
extern bool operator==(sub_D_SymbolComputation_SC_Ordered& lhs, sub_D_SymbolComputation_SC_Ordered& rhs);
extern bool operator<(sub_D_SymbolComputation_SC_Ordered& lhs, sub_D_SymbolComputation_SC_Ordered& rhs);
extern D_SymbolComputationPtr create_sub_D_SymbolComputation_SC_Ordered(CoordPtr arg_0, D_SymbolComputations arg_1);


class sub_D_SymbolComputation_SC_Return : public D_SymbolComputation { 
protected: 
  sub_D_SymbolComputation_SC_Return();
  CoordPtr arg_0;
  D_SymbolExpressionPtr arg_1;
  D_SymbolAttributes arg_2;
public:
  sub_D_SymbolComputation_SC_Return(CoordPtr arg_0, D_SymbolExpressionPtr arg_1, D_SymbolAttributes arg_2);
  sub_D_SymbolComputation_SC_Return(sub_D_SymbolComputation_SC_Return const& other); 
  sub_D_SymbolComputation_SC_Return(sub_D_SymbolComputation_SC_Return&& other);
  virtual ~sub_D_SymbolComputation_SC_Return(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_SymbolExpressionPtr get_arg_1();
  void set_arg_1(D_SymbolExpressionPtr arg);
  D_SymbolAttributes get_arg_2();
  void set_arg_2(D_SymbolAttributes arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolComputation_SC_Return& arg);
extern bool operator==(sub_D_SymbolComputation_SC_Return& lhs, sub_D_SymbolComputation_SC_Return& rhs);
extern bool operator<(sub_D_SymbolComputation_SC_Return& lhs, sub_D_SymbolComputation_SC_Return& rhs);
extern D_SymbolComputationPtr create_sub_D_SymbolComputation_SC_Return(CoordPtr arg_0, D_SymbolExpressionPtr arg_1, D_SymbolAttributes arg_2);


class D_SymbolLetVarDef { 
public:
  enum class D_SymbolLetVarDef_kind { Unknown, D_SymbolLetVarDef}; 
protected: 
  D_SymbolLetVarDef_kind kind; 

D_SymbolLetVarDef(); 
public: 
  virtual ~D_SymbolLetVarDef();
  D_SymbolLetVarDef(D_SymbolLetVarDef const& other);
  D_SymbolLetVarDef(D_SymbolLetVarDef::D_SymbolLetVarDef_kind kind);
  D_SymbolLetVarDef(D_SymbolLetVarDef&& other);
  
  D_SymbolLetVarDef_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_SymbolLetVarDef* arg); 
extern bool operator<(D_SymbolLetVarDefPtr lhs, D_SymbolLetVarDefPtr rhs);
extern bool operator==(D_SymbolLetVarDefPtr lhs, D_SymbolLetVarDefPtr rhs);

class sub_D_SymbolLetVarDef_D_SymbolLetVarDef : public D_SymbolLetVarDef { 
protected: 
  sub_D_SymbolLetVarDef_D_SymbolLetVarDef();
  StringTableKey arg_0;
  D_SymbolExpressionPtr arg_1;
  D_typePtr arg_2;
public:
  sub_D_SymbolLetVarDef_D_SymbolLetVarDef(StringTableKey arg_0, D_SymbolExpressionPtr arg_1, D_typePtr arg_2);
  sub_D_SymbolLetVarDef_D_SymbolLetVarDef(sub_D_SymbolLetVarDef_D_SymbolLetVarDef const& other); 
  sub_D_SymbolLetVarDef_D_SymbolLetVarDef(sub_D_SymbolLetVarDef_D_SymbolLetVarDef&& other);
  virtual ~sub_D_SymbolLetVarDef_D_SymbolLetVarDef(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  D_SymbolExpressionPtr get_arg_1();
  void set_arg_1(D_SymbolExpressionPtr arg);
  D_typePtr get_arg_2();
  void set_arg_2(D_typePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolLetVarDef_D_SymbolLetVarDef& arg);
extern bool operator==(sub_D_SymbolLetVarDef_D_SymbolLetVarDef& lhs, sub_D_SymbolLetVarDef_D_SymbolLetVarDef& rhs);
extern bool operator<(sub_D_SymbolLetVarDef_D_SymbolLetVarDef& lhs, sub_D_SymbolLetVarDef_D_SymbolLetVarDef& rhs);
extern D_SymbolLetVarDefPtr create_sub_D_SymbolLetVarDef_D_SymbolLetVarDef(StringTableKey arg_0, D_SymbolExpressionPtr arg_1, D_typePtr arg_2);


class D_SymbolExpression { 
public:
  enum class D_SymbolExpression_kind { Unknown, SE_Return, SE_IfExpr, SE_BinExpr, SE_UnaryExpr, SE_PostFix, SE_Wrap, SE_Call, SE_Tuple, SE_Cond, SE_Lambda, SE_Let, SE_Guards, SE_Error, SE_GuardExpr, SE_DefaultGuard, SE_Order, SE_Number, SE_String, SE_Name, SE_Attribute, SE_Bool, SE_CoordRef, SE_EmptyList, SE_Invalid}; 
protected: 
  D_SymbolExpression_kind kind; 

D_SymbolExpression(); 
public: 
  virtual ~D_SymbolExpression();
  D_SymbolExpression(D_SymbolExpression const& other);
  D_SymbolExpression(D_SymbolExpression::D_SymbolExpression_kind kind);
  D_SymbolExpression(D_SymbolExpression&& other);
  
  D_SymbolExpression_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_SymbolExpression* arg); 
extern bool operator<(D_SymbolExpressionPtr lhs, D_SymbolExpressionPtr rhs);
extern bool operator==(D_SymbolExpressionPtr lhs, D_SymbolExpressionPtr rhs);

class sub_D_SymbolExpression_SE_Return : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Return();
  D_SymbolExpressionPtr arg_0;
public:
  sub_D_SymbolExpression_SE_Return(D_SymbolExpressionPtr arg_0);
  sub_D_SymbolExpression_SE_Return(sub_D_SymbolExpression_SE_Return const& other); 
  sub_D_SymbolExpression_SE_Return(sub_D_SymbolExpression_SE_Return&& other);
  virtual ~sub_D_SymbolExpression_SE_Return(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Return& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Return& lhs, sub_D_SymbolExpression_SE_Return& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Return& lhs, sub_D_SymbolExpression_SE_Return& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Return(D_SymbolExpressionPtr arg_0);


class sub_D_SymbolExpression_SE_IfExpr : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_IfExpr();
  D_SymbolExpressionPtr arg_0;
  D_SymbolExpressionPtr arg_1;
  D_SymbolExpressionPtr arg_2;
public:
  sub_D_SymbolExpression_SE_IfExpr(D_SymbolExpressionPtr arg_0, D_SymbolExpressionPtr arg_1, D_SymbolExpressionPtr arg_2);
  sub_D_SymbolExpression_SE_IfExpr(sub_D_SymbolExpression_SE_IfExpr const& other); 
  sub_D_SymbolExpression_SE_IfExpr(sub_D_SymbolExpression_SE_IfExpr&& other);
  virtual ~sub_D_SymbolExpression_SE_IfExpr(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
  D_SymbolExpressionPtr get_arg_1();
  void set_arg_1(D_SymbolExpressionPtr arg);
  D_SymbolExpressionPtr get_arg_2();
  void set_arg_2(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_IfExpr& arg);
extern bool operator==(sub_D_SymbolExpression_SE_IfExpr& lhs, sub_D_SymbolExpression_SE_IfExpr& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_IfExpr& lhs, sub_D_SymbolExpression_SE_IfExpr& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_IfExpr(D_SymbolExpressionPtr arg_0, D_SymbolExpressionPtr arg_1, D_SymbolExpressionPtr arg_2);


class sub_D_SymbolExpression_SE_BinExpr : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_BinExpr();
  D_SymbolExpressionPtr arg_0;
  D_SymbolExpressionPtr arg_1;
  D_BinOp arg_2;
public:
  sub_D_SymbolExpression_SE_BinExpr(D_SymbolExpressionPtr arg_0, D_SymbolExpressionPtr arg_1, D_BinOp arg_2);
  sub_D_SymbolExpression_SE_BinExpr(sub_D_SymbolExpression_SE_BinExpr const& other); 
  sub_D_SymbolExpression_SE_BinExpr(sub_D_SymbolExpression_SE_BinExpr&& other);
  virtual ~sub_D_SymbolExpression_SE_BinExpr(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
  D_SymbolExpressionPtr get_arg_1();
  void set_arg_1(D_SymbolExpressionPtr arg);
  D_BinOp get_arg_2();
  void set_arg_2(D_BinOp arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_BinExpr& arg);
extern bool operator==(sub_D_SymbolExpression_SE_BinExpr& lhs, sub_D_SymbolExpression_SE_BinExpr& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_BinExpr& lhs, sub_D_SymbolExpression_SE_BinExpr& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_BinExpr(D_SymbolExpressionPtr arg_0, D_SymbolExpressionPtr arg_1, D_BinOp arg_2);


class sub_D_SymbolExpression_SE_UnaryExpr : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_UnaryExpr();
  D_SymbolExpressionPtr arg_0;
  D_UnaryOp arg_1;
public:
  sub_D_SymbolExpression_SE_UnaryExpr(D_SymbolExpressionPtr arg_0, D_UnaryOp arg_1);
  sub_D_SymbolExpression_SE_UnaryExpr(sub_D_SymbolExpression_SE_UnaryExpr const& other); 
  sub_D_SymbolExpression_SE_UnaryExpr(sub_D_SymbolExpression_SE_UnaryExpr&& other);
  virtual ~sub_D_SymbolExpression_SE_UnaryExpr(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
  D_UnaryOp get_arg_1();
  void set_arg_1(D_UnaryOp arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_UnaryExpr& arg);
extern bool operator==(sub_D_SymbolExpression_SE_UnaryExpr& lhs, sub_D_SymbolExpression_SE_UnaryExpr& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_UnaryExpr& lhs, sub_D_SymbolExpression_SE_UnaryExpr& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_UnaryExpr(D_SymbolExpressionPtr arg_0, D_UnaryOp arg_1);


class sub_D_SymbolExpression_SE_PostFix : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_PostFix();
  D_SymbolExpressionPtr arg_0;
  D_SymbolExpressionPtr arg_1;
  D_PostOp arg_2;
public:
  sub_D_SymbolExpression_SE_PostFix(D_SymbolExpressionPtr arg_0, D_SymbolExpressionPtr arg_1, D_PostOp arg_2);
  sub_D_SymbolExpression_SE_PostFix(sub_D_SymbolExpression_SE_PostFix const& other); 
  sub_D_SymbolExpression_SE_PostFix(sub_D_SymbolExpression_SE_PostFix&& other);
  virtual ~sub_D_SymbolExpression_SE_PostFix(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
  D_SymbolExpressionPtr get_arg_1();
  void set_arg_1(D_SymbolExpressionPtr arg);
  D_PostOp get_arg_2();
  void set_arg_2(D_PostOp arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_PostFix& arg);
extern bool operator==(sub_D_SymbolExpression_SE_PostFix& lhs, sub_D_SymbolExpression_SE_PostFix& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_PostFix& lhs, sub_D_SymbolExpression_SE_PostFix& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_PostFix(D_SymbolExpressionPtr arg_0, D_SymbolExpressionPtr arg_1, D_PostOp arg_2);


class sub_D_SymbolExpression_SE_Wrap : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Wrap();
  D_SymbolExpressionPtr arg_0;
public:
  sub_D_SymbolExpression_SE_Wrap(D_SymbolExpressionPtr arg_0);
  sub_D_SymbolExpression_SE_Wrap(sub_D_SymbolExpression_SE_Wrap const& other); 
  sub_D_SymbolExpression_SE_Wrap(sub_D_SymbolExpression_SE_Wrap&& other);
  virtual ~sub_D_SymbolExpression_SE_Wrap(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Wrap& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Wrap& lhs, sub_D_SymbolExpression_SE_Wrap& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Wrap& lhs, sub_D_SymbolExpression_SE_Wrap& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Wrap(D_SymbolExpressionPtr arg_0);


class sub_D_SymbolExpression_SE_Call : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Call();
  D_SymbolExpressionPtr arg_0;
  D_SymbolExpressions arg_1;
public:
  sub_D_SymbolExpression_SE_Call(D_SymbolExpressionPtr arg_0, D_SymbolExpressions arg_1);
  sub_D_SymbolExpression_SE_Call(sub_D_SymbolExpression_SE_Call const& other); 
  sub_D_SymbolExpression_SE_Call(sub_D_SymbolExpression_SE_Call&& other);
  virtual ~sub_D_SymbolExpression_SE_Call(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
  D_SymbolExpressions get_arg_1();
  void set_arg_1(D_SymbolExpressions arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Call& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Call& lhs, sub_D_SymbolExpression_SE_Call& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Call& lhs, sub_D_SymbolExpression_SE_Call& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Call(D_SymbolExpressionPtr arg_0, D_SymbolExpressions arg_1);


class sub_D_SymbolExpression_SE_Tuple : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Tuple();
  D_SymbolExpressions arg_0;
public:
  sub_D_SymbolExpression_SE_Tuple(D_SymbolExpressions arg_0);
  sub_D_SymbolExpression_SE_Tuple(sub_D_SymbolExpression_SE_Tuple const& other); 
  sub_D_SymbolExpression_SE_Tuple(sub_D_SymbolExpression_SE_Tuple&& other);
  virtual ~sub_D_SymbolExpression_SE_Tuple(); 
  
  D_SymbolExpressions get_arg_0();
  void set_arg_0(D_SymbolExpressions arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Tuple& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Tuple& lhs, sub_D_SymbolExpression_SE_Tuple& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Tuple& lhs, sub_D_SymbolExpression_SE_Tuple& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Tuple(D_SymbolExpressions arg_0);


class sub_D_SymbolExpression_SE_Cond : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Cond();
  D_SymbolExpressionPtr arg_0;
  D_SymbolExpressionPtr arg_1;
  bool arg_2;
public:
  sub_D_SymbolExpression_SE_Cond(D_SymbolExpressionPtr arg_0, D_SymbolExpressionPtr arg_1, bool arg_2);
  sub_D_SymbolExpression_SE_Cond(sub_D_SymbolExpression_SE_Cond const& other); 
  sub_D_SymbolExpression_SE_Cond(sub_D_SymbolExpression_SE_Cond&& other);
  virtual ~sub_D_SymbolExpression_SE_Cond(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
  D_SymbolExpressionPtr get_arg_1();
  void set_arg_1(D_SymbolExpressionPtr arg);
  bool get_arg_2();
  void set_arg_2(bool arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Cond& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Cond& lhs, sub_D_SymbolExpression_SE_Cond& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Cond& lhs, sub_D_SymbolExpression_SE_Cond& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Cond(D_SymbolExpressionPtr arg_0, D_SymbolExpressionPtr arg_1, bool arg_2);


class sub_D_SymbolExpression_SE_Lambda : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Lambda();
  StringTableKeyList arg_0;
  D_types arg_1;
  D_SymbolExpressionPtr arg_2;
public:
  sub_D_SymbolExpression_SE_Lambda(StringTableKeyList arg_0, D_types arg_1, D_SymbolExpressionPtr arg_2);
  sub_D_SymbolExpression_SE_Lambda(sub_D_SymbolExpression_SE_Lambda const& other); 
  sub_D_SymbolExpression_SE_Lambda(sub_D_SymbolExpression_SE_Lambda&& other);
  virtual ~sub_D_SymbolExpression_SE_Lambda(); 
  
  StringTableKeyList get_arg_0();
  void set_arg_0(StringTableKeyList arg);
  D_types get_arg_1();
  void set_arg_1(D_types arg);
  D_SymbolExpressionPtr get_arg_2();
  void set_arg_2(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Lambda& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Lambda& lhs, sub_D_SymbolExpression_SE_Lambda& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Lambda& lhs, sub_D_SymbolExpression_SE_Lambda& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Lambda(StringTableKeyList arg_0, D_types arg_1, D_SymbolExpressionPtr arg_2);


class sub_D_SymbolExpression_SE_Let : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Let();
  D_SymbolLetVarDefs arg_0;
  D_SymbolExpressionPtr arg_1;
public:
  sub_D_SymbolExpression_SE_Let(D_SymbolLetVarDefs arg_0, D_SymbolExpressionPtr arg_1);
  sub_D_SymbolExpression_SE_Let(sub_D_SymbolExpression_SE_Let const& other); 
  sub_D_SymbolExpression_SE_Let(sub_D_SymbolExpression_SE_Let&& other);
  virtual ~sub_D_SymbolExpression_SE_Let(); 
  
  D_SymbolLetVarDefs get_arg_0();
  void set_arg_0(D_SymbolLetVarDefs arg);
  D_SymbolExpressionPtr get_arg_1();
  void set_arg_1(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Let& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Let& lhs, sub_D_SymbolExpression_SE_Let& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Let& lhs, sub_D_SymbolExpression_SE_Let& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Let(D_SymbolLetVarDefs arg_0, D_SymbolExpressionPtr arg_1);


class sub_D_SymbolExpression_SE_Guards : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Guards();
  D_SymbolExpressions arg_0;
public:
  sub_D_SymbolExpression_SE_Guards(D_SymbolExpressions arg_0);
  sub_D_SymbolExpression_SE_Guards(sub_D_SymbolExpression_SE_Guards const& other); 
  sub_D_SymbolExpression_SE_Guards(sub_D_SymbolExpression_SE_Guards&& other);
  virtual ~sub_D_SymbolExpression_SE_Guards(); 
  
  D_SymbolExpressions get_arg_0();
  void set_arg_0(D_SymbolExpressions arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Guards& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Guards& lhs, sub_D_SymbolExpression_SE_Guards& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Guards& lhs, sub_D_SymbolExpression_SE_Guards& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Guards(D_SymbolExpressions arg_0);


class sub_D_SymbolExpression_SE_Error : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Error();
  D_SymbolExpressionPtr arg_0;
public:
  sub_D_SymbolExpression_SE_Error(D_SymbolExpressionPtr arg_0);
  sub_D_SymbolExpression_SE_Error(sub_D_SymbolExpression_SE_Error const& other); 
  sub_D_SymbolExpression_SE_Error(sub_D_SymbolExpression_SE_Error&& other);
  virtual ~sub_D_SymbolExpression_SE_Error(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Error& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Error& lhs, sub_D_SymbolExpression_SE_Error& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Error& lhs, sub_D_SymbolExpression_SE_Error& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Error(D_SymbolExpressionPtr arg_0);


class sub_D_SymbolExpression_SE_GuardExpr : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_GuardExpr();
  D_SymbolExpressions arg_0;
  D_SymbolExpressionPtr arg_1;
public:
  sub_D_SymbolExpression_SE_GuardExpr(D_SymbolExpressions arg_0, D_SymbolExpressionPtr arg_1);
  sub_D_SymbolExpression_SE_GuardExpr(sub_D_SymbolExpression_SE_GuardExpr const& other); 
  sub_D_SymbolExpression_SE_GuardExpr(sub_D_SymbolExpression_SE_GuardExpr&& other);
  virtual ~sub_D_SymbolExpression_SE_GuardExpr(); 
  
  D_SymbolExpressions get_arg_0();
  void set_arg_0(D_SymbolExpressions arg);
  D_SymbolExpressionPtr get_arg_1();
  void set_arg_1(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_GuardExpr& arg);
extern bool operator==(sub_D_SymbolExpression_SE_GuardExpr& lhs, sub_D_SymbolExpression_SE_GuardExpr& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_GuardExpr& lhs, sub_D_SymbolExpression_SE_GuardExpr& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_GuardExpr(D_SymbolExpressions arg_0, D_SymbolExpressionPtr arg_1);


class sub_D_SymbolExpression_SE_DefaultGuard : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_DefaultGuard();
  D_SymbolExpressionPtr arg_0;
public:
  sub_D_SymbolExpression_SE_DefaultGuard(D_SymbolExpressionPtr arg_0);
  sub_D_SymbolExpression_SE_DefaultGuard(sub_D_SymbolExpression_SE_DefaultGuard const& other); 
  sub_D_SymbolExpression_SE_DefaultGuard(sub_D_SymbolExpression_SE_DefaultGuard&& other);
  virtual ~sub_D_SymbolExpression_SE_DefaultGuard(); 
  
  D_SymbolExpressionPtr get_arg_0();
  void set_arg_0(D_SymbolExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_DefaultGuard& arg);
extern bool operator==(sub_D_SymbolExpression_SE_DefaultGuard& lhs, sub_D_SymbolExpression_SE_DefaultGuard& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_DefaultGuard& lhs, sub_D_SymbolExpression_SE_DefaultGuard& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_DefaultGuard(D_SymbolExpressionPtr arg_0);


class sub_D_SymbolExpression_SE_Order : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Order();
  D_SymbolComputations arg_0;
public:
  sub_D_SymbolExpression_SE_Order(D_SymbolComputations arg_0);
  sub_D_SymbolExpression_SE_Order(sub_D_SymbolExpression_SE_Order const& other); 
  sub_D_SymbolExpression_SE_Order(sub_D_SymbolExpression_SE_Order&& other);
  virtual ~sub_D_SymbolExpression_SE_Order(); 
  
  D_SymbolComputations get_arg_0();
  void set_arg_0(D_SymbolComputations arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Order& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Order& lhs, sub_D_SymbolExpression_SE_Order& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Order& lhs, sub_D_SymbolExpression_SE_Order& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Order(D_SymbolComputations arg_0);


class sub_D_SymbolExpression_SE_Number : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Number();
  StringTableKey arg_0;
public:
  sub_D_SymbolExpression_SE_Number(StringTableKey arg_0);
  sub_D_SymbolExpression_SE_Number(sub_D_SymbolExpression_SE_Number const& other); 
  sub_D_SymbolExpression_SE_Number(sub_D_SymbolExpression_SE_Number&& other);
  virtual ~sub_D_SymbolExpression_SE_Number(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Number& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Number& lhs, sub_D_SymbolExpression_SE_Number& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Number& lhs, sub_D_SymbolExpression_SE_Number& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Number(StringTableKey arg_0);


class sub_D_SymbolExpression_SE_String : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_String();
  StringTableKey arg_0;
public:
  sub_D_SymbolExpression_SE_String(StringTableKey arg_0);
  sub_D_SymbolExpression_SE_String(sub_D_SymbolExpression_SE_String const& other); 
  sub_D_SymbolExpression_SE_String(sub_D_SymbolExpression_SE_String&& other);
  virtual ~sub_D_SymbolExpression_SE_String(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_String& arg);
extern bool operator==(sub_D_SymbolExpression_SE_String& lhs, sub_D_SymbolExpression_SE_String& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_String& lhs, sub_D_SymbolExpression_SE_String& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_String(StringTableKey arg_0);


class sub_D_SymbolExpression_SE_Name : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Name();
  StringTableKey arg_0;
public:
  sub_D_SymbolExpression_SE_Name(StringTableKey arg_0);
  sub_D_SymbolExpression_SE_Name(sub_D_SymbolExpression_SE_Name const& other); 
  sub_D_SymbolExpression_SE_Name(sub_D_SymbolExpression_SE_Name&& other);
  virtual ~sub_D_SymbolExpression_SE_Name(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Name& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Name& lhs, sub_D_SymbolExpression_SE_Name& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Name& lhs, sub_D_SymbolExpression_SE_Name& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Name(StringTableKey arg_0);


class sub_D_SymbolExpression_SE_Attribute : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Attribute();
  D_SymbolAttributePtr arg_0;
public:
  sub_D_SymbolExpression_SE_Attribute(D_SymbolAttributePtr arg_0);
  sub_D_SymbolExpression_SE_Attribute(sub_D_SymbolExpression_SE_Attribute const& other); 
  sub_D_SymbolExpression_SE_Attribute(sub_D_SymbolExpression_SE_Attribute&& other);
  virtual ~sub_D_SymbolExpression_SE_Attribute(); 
  
  D_SymbolAttributePtr get_arg_0();
  void set_arg_0(D_SymbolAttributePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Attribute& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Attribute& lhs, sub_D_SymbolExpression_SE_Attribute& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Attribute& lhs, sub_D_SymbolExpression_SE_Attribute& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Attribute(D_SymbolAttributePtr arg_0);


class sub_D_SymbolExpression_SE_Bool : public D_SymbolExpression { 
protected: 
  sub_D_SymbolExpression_SE_Bool();
  bool arg_0;
public:
  sub_D_SymbolExpression_SE_Bool(bool arg_0);
  sub_D_SymbolExpression_SE_Bool(sub_D_SymbolExpression_SE_Bool const& other); 
  sub_D_SymbolExpression_SE_Bool(sub_D_SymbolExpression_SE_Bool&& other);
  virtual ~sub_D_SymbolExpression_SE_Bool(); 
  
  bool get_arg_0();
  void set_arg_0(bool arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Bool& arg);
extern bool operator==(sub_D_SymbolExpression_SE_Bool& lhs, sub_D_SymbolExpression_SE_Bool& rhs);
extern bool operator<(sub_D_SymbolExpression_SE_Bool& lhs, sub_D_SymbolExpression_SE_Bool& rhs);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Bool(bool arg_0);


class sub_D_SymbolExpression_SE_CoordRef : public D_SymbolExpression { 
public:
  sub_D_SymbolExpression_SE_CoordRef();
  sub_D_SymbolExpression_SE_CoordRef(sub_D_SymbolExpression_SE_CoordRef const& other);
  sub_D_SymbolExpression_SE_CoordRef(sub_D_SymbolExpression_SE_CoordRef&& other); 
  virtual ~sub_D_SymbolExpression_SE_CoordRef();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_CoordRef& arg);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_CoordRef();


class sub_D_SymbolExpression_SE_EmptyList : public D_SymbolExpression { 
public:
  sub_D_SymbolExpression_SE_EmptyList();
  sub_D_SymbolExpression_SE_EmptyList(sub_D_SymbolExpression_SE_EmptyList const& other);
  sub_D_SymbolExpression_SE_EmptyList(sub_D_SymbolExpression_SE_EmptyList&& other); 
  virtual ~sub_D_SymbolExpression_SE_EmptyList();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_EmptyList& arg);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_EmptyList();


class sub_D_SymbolExpression_SE_Invalid : public D_SymbolExpression { 
public:
  sub_D_SymbolExpression_SE_Invalid();
  sub_D_SymbolExpression_SE_Invalid(sub_D_SymbolExpression_SE_Invalid const& other);
  sub_D_SymbolExpression_SE_Invalid(sub_D_SymbolExpression_SE_Invalid&& other); 
  virtual ~sub_D_SymbolExpression_SE_Invalid();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolExpression_SE_Invalid& arg);
extern D_SymbolExpressionPtr create_sub_D_SymbolExpression_SE_Invalid();


class D_ProdMatch { 
public:
  enum class D_ProdMatch_kind { Unknown, PM_IgnoreRest, PM_NoMatch, PM_MatchAny, PM_MatchConcrete, PM_MatchNumber, PM_MatchString, PM_MatchLiteral, PM_SubMatch}; 
protected: 
  D_ProdMatch_kind kind; 

D_ProdMatch(); 
public: 
  virtual ~D_ProdMatch();
  D_ProdMatch(D_ProdMatch const& other);
  D_ProdMatch(D_ProdMatch::D_ProdMatch_kind kind);
  D_ProdMatch(D_ProdMatch&& other);
  
  D_ProdMatch_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_ProdMatch* arg); 
extern bool operator<(D_ProdMatchPtr lhs, D_ProdMatchPtr rhs);
extern bool operator==(D_ProdMatchPtr lhs, D_ProdMatchPtr rhs);

class sub_D_ProdMatch_PM_IgnoreRest : public D_ProdMatch { 
public:
  sub_D_ProdMatch_PM_IgnoreRest();
  sub_D_ProdMatch_PM_IgnoreRest(sub_D_ProdMatch_PM_IgnoreRest const& other);
  sub_D_ProdMatch_PM_IgnoreRest(sub_D_ProdMatch_PM_IgnoreRest&& other); 
  virtual ~sub_D_ProdMatch_PM_IgnoreRest();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdMatch_PM_IgnoreRest& arg);
extern D_ProdMatchPtr create_sub_D_ProdMatch_PM_IgnoreRest();


class sub_D_ProdMatch_PM_NoMatch : public D_ProdMatch { 
public:
  sub_D_ProdMatch_PM_NoMatch();
  sub_D_ProdMatch_PM_NoMatch(sub_D_ProdMatch_PM_NoMatch const& other);
  sub_D_ProdMatch_PM_NoMatch(sub_D_ProdMatch_PM_NoMatch&& other); 
  virtual ~sub_D_ProdMatch_PM_NoMatch();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdMatch_PM_NoMatch& arg);
extern D_ProdMatchPtr create_sub_D_ProdMatch_PM_NoMatch();


class sub_D_ProdMatch_PM_MatchAny : public D_ProdMatch { 
protected: 
  sub_D_ProdMatch_PM_MatchAny();
  StringTableKey arg_0;
  StringTableKey arg_1;
public:
  sub_D_ProdMatch_PM_MatchAny(StringTableKey arg_0, StringTableKey arg_1);
  sub_D_ProdMatch_PM_MatchAny(sub_D_ProdMatch_PM_MatchAny const& other); 
  sub_D_ProdMatch_PM_MatchAny(sub_D_ProdMatch_PM_MatchAny&& other);
  virtual ~sub_D_ProdMatch_PM_MatchAny(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdMatch_PM_MatchAny& arg);
extern bool operator==(sub_D_ProdMatch_PM_MatchAny& lhs, sub_D_ProdMatch_PM_MatchAny& rhs);
extern bool operator<(sub_D_ProdMatch_PM_MatchAny& lhs, sub_D_ProdMatch_PM_MatchAny& rhs);
extern D_ProdMatchPtr create_sub_D_ProdMatch_PM_MatchAny(StringTableKey arg_0, StringTableKey arg_1);


class sub_D_ProdMatch_PM_MatchConcrete : public D_ProdMatch { 
protected: 
  sub_D_ProdMatch_PM_MatchConcrete();
  StringTableKey arg_0;
  StringTableKey arg_1;
  StringTableKey arg_2;
public:
  sub_D_ProdMatch_PM_MatchConcrete(StringTableKey arg_0, StringTableKey arg_1, StringTableKey arg_2);
  sub_D_ProdMatch_PM_MatchConcrete(sub_D_ProdMatch_PM_MatchConcrete const& other); 
  sub_D_ProdMatch_PM_MatchConcrete(sub_D_ProdMatch_PM_MatchConcrete&& other);
  virtual ~sub_D_ProdMatch_PM_MatchConcrete(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
  StringTableKey get_arg_2();
  void set_arg_2(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdMatch_PM_MatchConcrete& arg);
extern bool operator==(sub_D_ProdMatch_PM_MatchConcrete& lhs, sub_D_ProdMatch_PM_MatchConcrete& rhs);
extern bool operator<(sub_D_ProdMatch_PM_MatchConcrete& lhs, sub_D_ProdMatch_PM_MatchConcrete& rhs);
extern D_ProdMatchPtr create_sub_D_ProdMatch_PM_MatchConcrete(StringTableKey arg_0, StringTableKey arg_1, StringTableKey arg_2);


class sub_D_ProdMatch_PM_MatchNumber : public D_ProdMatch { 
protected: 
  sub_D_ProdMatch_PM_MatchNumber();
  StringTableKey arg_0;
public:
  sub_D_ProdMatch_PM_MatchNumber(StringTableKey arg_0);
  sub_D_ProdMatch_PM_MatchNumber(sub_D_ProdMatch_PM_MatchNumber const& other); 
  sub_D_ProdMatch_PM_MatchNumber(sub_D_ProdMatch_PM_MatchNumber&& other);
  virtual ~sub_D_ProdMatch_PM_MatchNumber(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdMatch_PM_MatchNumber& arg);
extern bool operator==(sub_D_ProdMatch_PM_MatchNumber& lhs, sub_D_ProdMatch_PM_MatchNumber& rhs);
extern bool operator<(sub_D_ProdMatch_PM_MatchNumber& lhs, sub_D_ProdMatch_PM_MatchNumber& rhs);
extern D_ProdMatchPtr create_sub_D_ProdMatch_PM_MatchNumber(StringTableKey arg_0);


class sub_D_ProdMatch_PM_MatchString : public D_ProdMatch { 
protected: 
  sub_D_ProdMatch_PM_MatchString();
  StringTableKey arg_0;
public:
  sub_D_ProdMatch_PM_MatchString(StringTableKey arg_0);
  sub_D_ProdMatch_PM_MatchString(sub_D_ProdMatch_PM_MatchString const& other); 
  sub_D_ProdMatch_PM_MatchString(sub_D_ProdMatch_PM_MatchString&& other);
  virtual ~sub_D_ProdMatch_PM_MatchString(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdMatch_PM_MatchString& arg);
extern bool operator==(sub_D_ProdMatch_PM_MatchString& lhs, sub_D_ProdMatch_PM_MatchString& rhs);
extern bool operator<(sub_D_ProdMatch_PM_MatchString& lhs, sub_D_ProdMatch_PM_MatchString& rhs);
extern D_ProdMatchPtr create_sub_D_ProdMatch_PM_MatchString(StringTableKey arg_0);


class sub_D_ProdMatch_PM_MatchLiteral : public D_ProdMatch { 
protected: 
  sub_D_ProdMatch_PM_MatchLiteral();
  StringTableKey arg_0;
public:
  sub_D_ProdMatch_PM_MatchLiteral(StringTableKey arg_0);
  sub_D_ProdMatch_PM_MatchLiteral(sub_D_ProdMatch_PM_MatchLiteral const& other); 
  sub_D_ProdMatch_PM_MatchLiteral(sub_D_ProdMatch_PM_MatchLiteral&& other);
  virtual ~sub_D_ProdMatch_PM_MatchLiteral(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdMatch_PM_MatchLiteral& arg);
extern bool operator==(sub_D_ProdMatch_PM_MatchLiteral& lhs, sub_D_ProdMatch_PM_MatchLiteral& rhs);
extern bool operator<(sub_D_ProdMatch_PM_MatchLiteral& lhs, sub_D_ProdMatch_PM_MatchLiteral& rhs);
extern D_ProdMatchPtr create_sub_D_ProdMatch_PM_MatchLiteral(StringTableKey arg_0);


class sub_D_ProdMatch_PM_SubMatch : public D_ProdMatch { 
protected: 
  sub_D_ProdMatch_PM_SubMatch();
  D_ProdMatchPtr arg_0;
  D_ProdMatches arg_1;
public:
  sub_D_ProdMatch_PM_SubMatch(D_ProdMatchPtr arg_0, D_ProdMatches arg_1);
  sub_D_ProdMatch_PM_SubMatch(sub_D_ProdMatch_PM_SubMatch const& other); 
  sub_D_ProdMatch_PM_SubMatch(sub_D_ProdMatch_PM_SubMatch&& other);
  virtual ~sub_D_ProdMatch_PM_SubMatch(); 
  
  D_ProdMatchPtr get_arg_0();
  void set_arg_0(D_ProdMatchPtr arg);
  D_ProdMatches get_arg_1();
  void set_arg_1(D_ProdMatches arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ProdMatch_PM_SubMatch& arg);
extern bool operator==(sub_D_ProdMatch_PM_SubMatch& lhs, sub_D_ProdMatch_PM_SubMatch& rhs);
extern bool operator<(sub_D_ProdMatch_PM_SubMatch& lhs, sub_D_ProdMatch_PM_SubMatch& rhs);
extern D_ProdMatchPtr create_sub_D_ProdMatch_PM_SubMatch(D_ProdMatchPtr arg_0, D_ProdMatches arg_1);


class D_RuleSymbolAttribute { 
public:
  enum class D_RuleSymbolAttribute_kind { Unknown, RSA_Var, RSA_Concrete, RSA_Remote, RSA_Class}; 
protected: 
  D_RuleSymbolAttribute_kind kind; 

D_RuleSymbolAttribute(); 
public: 
  virtual ~D_RuleSymbolAttribute();
  D_RuleSymbolAttribute(D_RuleSymbolAttribute const& other);
  D_RuleSymbolAttribute(D_RuleSymbolAttribute::D_RuleSymbolAttribute_kind kind);
  D_RuleSymbolAttribute(D_RuleSymbolAttribute&& other);
  
  D_RuleSymbolAttribute_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_RuleSymbolAttribute* arg); 
extern bool operator<(D_RuleSymbolAttributePtr lhs, D_RuleSymbolAttributePtr rhs);
extern bool operator==(D_RuleSymbolAttributePtr lhs, D_RuleSymbolAttributePtr rhs);

class sub_D_RuleSymbolAttribute_RSA_Var : public D_RuleSymbolAttribute { 
protected: 
  sub_D_RuleSymbolAttribute_RSA_Var();
  CoordPtr arg_0;
  Binding arg_1;
  StringTableKey arg_2;
public:
  sub_D_RuleSymbolAttribute_RSA_Var(CoordPtr arg_0, Binding arg_1, StringTableKey arg_2);
  sub_D_RuleSymbolAttribute_RSA_Var(sub_D_RuleSymbolAttribute_RSA_Var const& other); 
  sub_D_RuleSymbolAttribute_RSA_Var(sub_D_RuleSymbolAttribute_RSA_Var&& other);
  virtual ~sub_D_RuleSymbolAttribute_RSA_Var(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  Binding get_arg_1();
  void set_arg_1(Binding arg);
  StringTableKey get_arg_2();
  void set_arg_2(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleSymbolAttribute_RSA_Var& arg);
extern bool operator==(sub_D_RuleSymbolAttribute_RSA_Var& lhs, sub_D_RuleSymbolAttribute_RSA_Var& rhs);
extern bool operator<(sub_D_RuleSymbolAttribute_RSA_Var& lhs, sub_D_RuleSymbolAttribute_RSA_Var& rhs);
extern D_RuleSymbolAttributePtr create_sub_D_RuleSymbolAttribute_RSA_Var(CoordPtr arg_0, Binding arg_1, StringTableKey arg_2);


class sub_D_RuleSymbolAttribute_RSA_Concrete : public D_RuleSymbolAttribute { 
protected: 
  sub_D_RuleSymbolAttribute_RSA_Concrete();
  CoordPtr arg_0;
  StringTableKey arg_1;
  int arg_2;
  StringTableKey arg_3;
  StringTableKey arg_4;
public:
  sub_D_RuleSymbolAttribute_RSA_Concrete(CoordPtr arg_0, StringTableKey arg_1, int arg_2, StringTableKey arg_3, StringTableKey arg_4);
  sub_D_RuleSymbolAttribute_RSA_Concrete(sub_D_RuleSymbolAttribute_RSA_Concrete const& other); 
  sub_D_RuleSymbolAttribute_RSA_Concrete(sub_D_RuleSymbolAttribute_RSA_Concrete&& other);
  virtual ~sub_D_RuleSymbolAttribute_RSA_Concrete(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
  int get_arg_2();
  void set_arg_2(int arg);
  StringTableKey get_arg_3();
  void set_arg_3(StringTableKey arg);
  StringTableKey get_arg_4();
  void set_arg_4(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleSymbolAttribute_RSA_Concrete& arg);
extern bool operator==(sub_D_RuleSymbolAttribute_RSA_Concrete& lhs, sub_D_RuleSymbolAttribute_RSA_Concrete& rhs);
extern bool operator<(sub_D_RuleSymbolAttribute_RSA_Concrete& lhs, sub_D_RuleSymbolAttribute_RSA_Concrete& rhs);
extern D_RuleSymbolAttributePtr create_sub_D_RuleSymbolAttribute_RSA_Concrete(CoordPtr arg_0, StringTableKey arg_1, int arg_2, StringTableKey arg_3, StringTableKey arg_4);


class sub_D_RuleSymbolAttribute_RSA_Remote : public D_RuleSymbolAttribute { 
protected: 
  sub_D_RuleSymbolAttribute_RSA_Remote();
  D_RemoteAttributePtr arg_0;
public:
  sub_D_RuleSymbolAttribute_RSA_Remote(D_RemoteAttributePtr arg_0);
  sub_D_RuleSymbolAttribute_RSA_Remote(sub_D_RuleSymbolAttribute_RSA_Remote const& other); 
  sub_D_RuleSymbolAttribute_RSA_Remote(sub_D_RuleSymbolAttribute_RSA_Remote&& other);
  virtual ~sub_D_RuleSymbolAttribute_RSA_Remote(); 
  
  D_RemoteAttributePtr get_arg_0();
  void set_arg_0(D_RemoteAttributePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleSymbolAttribute_RSA_Remote& arg);
extern bool operator==(sub_D_RuleSymbolAttribute_RSA_Remote& lhs, sub_D_RuleSymbolAttribute_RSA_Remote& rhs);
extern bool operator<(sub_D_RuleSymbolAttribute_RSA_Remote& lhs, sub_D_RuleSymbolAttribute_RSA_Remote& rhs);
extern D_RuleSymbolAttributePtr create_sub_D_RuleSymbolAttribute_RSA_Remote(D_RemoteAttributePtr arg_0);


class sub_D_RuleSymbolAttribute_RSA_Class : public D_RuleSymbolAttribute { 
protected: 
  sub_D_RuleSymbolAttribute_RSA_Class();
  CoordPtr arg_0;
  bool arg_1;
  StringTableKey arg_2;
public:
  sub_D_RuleSymbolAttribute_RSA_Class(CoordPtr arg_0, bool arg_1, StringTableKey arg_2);
  sub_D_RuleSymbolAttribute_RSA_Class(sub_D_RuleSymbolAttribute_RSA_Class const& other); 
  sub_D_RuleSymbolAttribute_RSA_Class(sub_D_RuleSymbolAttribute_RSA_Class&& other);
  virtual ~sub_D_RuleSymbolAttribute_RSA_Class(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  bool get_arg_1();
  void set_arg_1(bool arg);
  StringTableKey get_arg_2();
  void set_arg_2(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleSymbolAttribute_RSA_Class& arg);
extern bool operator==(sub_D_RuleSymbolAttribute_RSA_Class& lhs, sub_D_RuleSymbolAttribute_RSA_Class& rhs);
extern bool operator<(sub_D_RuleSymbolAttribute_RSA_Class& lhs, sub_D_RuleSymbolAttribute_RSA_Class& rhs);
extern D_RuleSymbolAttributePtr create_sub_D_RuleSymbolAttribute_RSA_Class(CoordPtr arg_0, bool arg_1, StringTableKey arg_2);


class D_RuleComputation { 
public:
  enum class D_RuleComputation_kind { Unknown, RC_DefOcc, RC_ChainStart, RC_Expression, RC_Ordered, RC_NoGuard, RC_GuardComp, RC_Return}; 
protected: 
  D_RuleComputation_kind kind; 

D_RuleComputation(); 
public: 
  virtual ~D_RuleComputation();
  D_RuleComputation(D_RuleComputation const& other);
  D_RuleComputation(D_RuleComputation::D_RuleComputation_kind kind);
  D_RuleComputation(D_RuleComputation&& other);
  
  D_RuleComputation_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_RuleComputation* arg); 
extern bool operator<(D_RuleComputationPtr lhs, D_RuleComputationPtr rhs);
extern bool operator==(D_RuleComputationPtr lhs, D_RuleComputationPtr rhs);

class sub_D_RuleComputation_RC_DefOcc : public D_RuleComputation { 
protected: 
  sub_D_RuleComputation_RC_DefOcc();
  CoordPtr arg_0;
  D_RuleSymbolAttributePtr arg_1;
  D_RuleSymbolAttributes arg_2;
  D_RuleExpressionPtr arg_3;
public:
  sub_D_RuleComputation_RC_DefOcc(CoordPtr arg_0, D_RuleSymbolAttributePtr arg_1, D_RuleSymbolAttributes arg_2, D_RuleExpressionPtr arg_3);
  sub_D_RuleComputation_RC_DefOcc(sub_D_RuleComputation_RC_DefOcc const& other); 
  sub_D_RuleComputation_RC_DefOcc(sub_D_RuleComputation_RC_DefOcc&& other);
  virtual ~sub_D_RuleComputation_RC_DefOcc(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_RuleSymbolAttributePtr get_arg_1();
  void set_arg_1(D_RuleSymbolAttributePtr arg);
  D_RuleSymbolAttributes get_arg_2();
  void set_arg_2(D_RuleSymbolAttributes arg);
  D_RuleExpressionPtr get_arg_3();
  void set_arg_3(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleComputation_RC_DefOcc& arg);
extern bool operator==(sub_D_RuleComputation_RC_DefOcc& lhs, sub_D_RuleComputation_RC_DefOcc& rhs);
extern bool operator<(sub_D_RuleComputation_RC_DefOcc& lhs, sub_D_RuleComputation_RC_DefOcc& rhs);
extern D_RuleComputationPtr create_sub_D_RuleComputation_RC_DefOcc(CoordPtr arg_0, D_RuleSymbolAttributePtr arg_1, D_RuleSymbolAttributes arg_2, D_RuleExpressionPtr arg_3);


class sub_D_RuleComputation_RC_ChainStart : public D_RuleComputation { 
protected: 
  sub_D_RuleComputation_RC_ChainStart();
  CoordPtr arg_0;
  D_RuleSymbolAttributePtr arg_1;
  D_RuleSymbolAttributes arg_2;
  D_RuleExpressionPtr arg_3;
public:
  sub_D_RuleComputation_RC_ChainStart(CoordPtr arg_0, D_RuleSymbolAttributePtr arg_1, D_RuleSymbolAttributes arg_2, D_RuleExpressionPtr arg_3);
  sub_D_RuleComputation_RC_ChainStart(sub_D_RuleComputation_RC_ChainStart const& other); 
  sub_D_RuleComputation_RC_ChainStart(sub_D_RuleComputation_RC_ChainStart&& other);
  virtual ~sub_D_RuleComputation_RC_ChainStart(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_RuleSymbolAttributePtr get_arg_1();
  void set_arg_1(D_RuleSymbolAttributePtr arg);
  D_RuleSymbolAttributes get_arg_2();
  void set_arg_2(D_RuleSymbolAttributes arg);
  D_RuleExpressionPtr get_arg_3();
  void set_arg_3(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleComputation_RC_ChainStart& arg);
extern bool operator==(sub_D_RuleComputation_RC_ChainStart& lhs, sub_D_RuleComputation_RC_ChainStart& rhs);
extern bool operator<(sub_D_RuleComputation_RC_ChainStart& lhs, sub_D_RuleComputation_RC_ChainStart& rhs);
extern D_RuleComputationPtr create_sub_D_RuleComputation_RC_ChainStart(CoordPtr arg_0, D_RuleSymbolAttributePtr arg_1, D_RuleSymbolAttributes arg_2, D_RuleExpressionPtr arg_3);


class sub_D_RuleComputation_RC_Expression : public D_RuleComputation { 
protected: 
  sub_D_RuleComputation_RC_Expression();
  CoordPtr arg_0;
  D_RuleExpressionPtr arg_1;
  D_RuleSymbolAttributes arg_2;
public:
  sub_D_RuleComputation_RC_Expression(CoordPtr arg_0, D_RuleExpressionPtr arg_1, D_RuleSymbolAttributes arg_2);
  sub_D_RuleComputation_RC_Expression(sub_D_RuleComputation_RC_Expression const& other); 
  sub_D_RuleComputation_RC_Expression(sub_D_RuleComputation_RC_Expression&& other);
  virtual ~sub_D_RuleComputation_RC_Expression(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_RuleExpressionPtr get_arg_1();
  void set_arg_1(D_RuleExpressionPtr arg);
  D_RuleSymbolAttributes get_arg_2();
  void set_arg_2(D_RuleSymbolAttributes arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleComputation_RC_Expression& arg);
extern bool operator==(sub_D_RuleComputation_RC_Expression& lhs, sub_D_RuleComputation_RC_Expression& rhs);
extern bool operator<(sub_D_RuleComputation_RC_Expression& lhs, sub_D_RuleComputation_RC_Expression& rhs);
extern D_RuleComputationPtr create_sub_D_RuleComputation_RC_Expression(CoordPtr arg_0, D_RuleExpressionPtr arg_1, D_RuleSymbolAttributes arg_2);


class sub_D_RuleComputation_RC_Ordered : public D_RuleComputation { 
protected: 
  sub_D_RuleComputation_RC_Ordered();
  CoordPtr arg_0;
  D_RuleComputations arg_1;
public:
  sub_D_RuleComputation_RC_Ordered(CoordPtr arg_0, D_RuleComputations arg_1);
  sub_D_RuleComputation_RC_Ordered(sub_D_RuleComputation_RC_Ordered const& other); 
  sub_D_RuleComputation_RC_Ordered(sub_D_RuleComputation_RC_Ordered&& other);
  virtual ~sub_D_RuleComputation_RC_Ordered(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_RuleComputations get_arg_1();
  void set_arg_1(D_RuleComputations arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleComputation_RC_Ordered& arg);
extern bool operator==(sub_D_RuleComputation_RC_Ordered& lhs, sub_D_RuleComputation_RC_Ordered& rhs);
extern bool operator<(sub_D_RuleComputation_RC_Ordered& lhs, sub_D_RuleComputation_RC_Ordered& rhs);
extern D_RuleComputationPtr create_sub_D_RuleComputation_RC_Ordered(CoordPtr arg_0, D_RuleComputations arg_1);


class sub_D_RuleComputation_RC_NoGuard : public D_RuleComputation { 
protected: 
  sub_D_RuleComputation_RC_NoGuard();
  CoordPtr arg_0;
  D_RuleComputations arg_1;
public:
  sub_D_RuleComputation_RC_NoGuard(CoordPtr arg_0, D_RuleComputations arg_1);
  sub_D_RuleComputation_RC_NoGuard(sub_D_RuleComputation_RC_NoGuard const& other); 
  sub_D_RuleComputation_RC_NoGuard(sub_D_RuleComputation_RC_NoGuard&& other);
  virtual ~sub_D_RuleComputation_RC_NoGuard(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_RuleComputations get_arg_1();
  void set_arg_1(D_RuleComputations arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleComputation_RC_NoGuard& arg);
extern bool operator==(sub_D_RuleComputation_RC_NoGuard& lhs, sub_D_RuleComputation_RC_NoGuard& rhs);
extern bool operator<(sub_D_RuleComputation_RC_NoGuard& lhs, sub_D_RuleComputation_RC_NoGuard& rhs);
extern D_RuleComputationPtr create_sub_D_RuleComputation_RC_NoGuard(CoordPtr arg_0, D_RuleComputations arg_1);


class sub_D_RuleComputation_RC_GuardComp : public D_RuleComputation { 
protected: 
  sub_D_RuleComputation_RC_GuardComp();
  CoordPtr arg_0;
  D_RuleExpressions arg_1;
  D_RuleComputations arg_2;
public:
  sub_D_RuleComputation_RC_GuardComp(CoordPtr arg_0, D_RuleExpressions arg_1, D_RuleComputations arg_2);
  sub_D_RuleComputation_RC_GuardComp(sub_D_RuleComputation_RC_GuardComp const& other); 
  sub_D_RuleComputation_RC_GuardComp(sub_D_RuleComputation_RC_GuardComp&& other);
  virtual ~sub_D_RuleComputation_RC_GuardComp(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_RuleExpressions get_arg_1();
  void set_arg_1(D_RuleExpressions arg);
  D_RuleComputations get_arg_2();
  void set_arg_2(D_RuleComputations arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleComputation_RC_GuardComp& arg);
extern bool operator==(sub_D_RuleComputation_RC_GuardComp& lhs, sub_D_RuleComputation_RC_GuardComp& rhs);
extern bool operator<(sub_D_RuleComputation_RC_GuardComp& lhs, sub_D_RuleComputation_RC_GuardComp& rhs);
extern D_RuleComputationPtr create_sub_D_RuleComputation_RC_GuardComp(CoordPtr arg_0, D_RuleExpressions arg_1, D_RuleComputations arg_2);


class sub_D_RuleComputation_RC_Return : public D_RuleComputation { 
protected: 
  sub_D_RuleComputation_RC_Return();
  CoordPtr arg_0;
  D_RuleExpressionPtr arg_1;
  D_RuleSymbolAttributes arg_2;
public:
  sub_D_RuleComputation_RC_Return(CoordPtr arg_0, D_RuleExpressionPtr arg_1, D_RuleSymbolAttributes arg_2);
  sub_D_RuleComputation_RC_Return(sub_D_RuleComputation_RC_Return const& other); 
  sub_D_RuleComputation_RC_Return(sub_D_RuleComputation_RC_Return&& other);
  virtual ~sub_D_RuleComputation_RC_Return(); 
  
  CoordPtr get_arg_0();
  void set_arg_0(CoordPtr arg);
  D_RuleExpressionPtr get_arg_1();
  void set_arg_1(D_RuleExpressionPtr arg);
  D_RuleSymbolAttributes get_arg_2();
  void set_arg_2(D_RuleSymbolAttributes arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleComputation_RC_Return& arg);
extern bool operator==(sub_D_RuleComputation_RC_Return& lhs, sub_D_RuleComputation_RC_Return& rhs);
extern bool operator<(sub_D_RuleComputation_RC_Return& lhs, sub_D_RuleComputation_RC_Return& rhs);
extern D_RuleComputationPtr create_sub_D_RuleComputation_RC_Return(CoordPtr arg_0, D_RuleExpressionPtr arg_1, D_RuleSymbolAttributes arg_2);


class D_RuleExpression { 
public:
  enum class D_RuleExpression_kind { Unknown, RE_Return, RE_IfExpr, RE_BinExpr, RE_CondExpr, RE_ErrExpr, RE_Let, RE_Lambda, RE_Order, RE_PostFix, RE_Unary, RE_Tuple, RE_Attribute, RE_Call, RE_Guards, RE_DefaultGuard, RE_GuardExpr, RE_Wrap, RE_Number, RE_String, RE_Name, RE_Rule, RE_Bool, RE_CoordRef, RE_EmptyList, RE_Invalid}; 
protected: 
  D_RuleExpression_kind kind; 

D_RuleExpression(); 
public: 
  virtual ~D_RuleExpression();
  D_RuleExpression(D_RuleExpression const& other);
  D_RuleExpression(D_RuleExpression::D_RuleExpression_kind kind);
  D_RuleExpression(D_RuleExpression&& other);
  
  D_RuleExpression_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_RuleExpression* arg); 
extern bool operator<(D_RuleExpressionPtr lhs, D_RuleExpressionPtr rhs);
extern bool operator==(D_RuleExpressionPtr lhs, D_RuleExpressionPtr rhs);

class sub_D_RuleExpression_RE_Return : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Return();
  D_RuleExpressionPtr arg_0;
public:
  sub_D_RuleExpression_RE_Return(D_RuleExpressionPtr arg_0);
  sub_D_RuleExpression_RE_Return(sub_D_RuleExpression_RE_Return const& other); 
  sub_D_RuleExpression_RE_Return(sub_D_RuleExpression_RE_Return&& other);
  virtual ~sub_D_RuleExpression_RE_Return(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Return& arg);
extern bool operator==(sub_D_RuleExpression_RE_Return& lhs, sub_D_RuleExpression_RE_Return& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Return& lhs, sub_D_RuleExpression_RE_Return& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Return(D_RuleExpressionPtr arg_0);


class sub_D_RuleExpression_RE_IfExpr : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_IfExpr();
  D_RuleExpressionPtr arg_0;
  D_RuleExpressionPtr arg_1;
  D_RuleExpressionPtr arg_2;
public:
  sub_D_RuleExpression_RE_IfExpr(D_RuleExpressionPtr arg_0, D_RuleExpressionPtr arg_1, D_RuleExpressionPtr arg_2);
  sub_D_RuleExpression_RE_IfExpr(sub_D_RuleExpression_RE_IfExpr const& other); 
  sub_D_RuleExpression_RE_IfExpr(sub_D_RuleExpression_RE_IfExpr&& other);
  virtual ~sub_D_RuleExpression_RE_IfExpr(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
  D_RuleExpressionPtr get_arg_1();
  void set_arg_1(D_RuleExpressionPtr arg);
  D_RuleExpressionPtr get_arg_2();
  void set_arg_2(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_IfExpr& arg);
extern bool operator==(sub_D_RuleExpression_RE_IfExpr& lhs, sub_D_RuleExpression_RE_IfExpr& rhs);
extern bool operator<(sub_D_RuleExpression_RE_IfExpr& lhs, sub_D_RuleExpression_RE_IfExpr& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_IfExpr(D_RuleExpressionPtr arg_0, D_RuleExpressionPtr arg_1, D_RuleExpressionPtr arg_2);


class sub_D_RuleExpression_RE_BinExpr : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_BinExpr();
  D_RuleExpressionPtr arg_0;
  D_RuleExpressionPtr arg_1;
  D_BinOp arg_2;
public:
  sub_D_RuleExpression_RE_BinExpr(D_RuleExpressionPtr arg_0, D_RuleExpressionPtr arg_1, D_BinOp arg_2);
  sub_D_RuleExpression_RE_BinExpr(sub_D_RuleExpression_RE_BinExpr const& other); 
  sub_D_RuleExpression_RE_BinExpr(sub_D_RuleExpression_RE_BinExpr&& other);
  virtual ~sub_D_RuleExpression_RE_BinExpr(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
  D_RuleExpressionPtr get_arg_1();
  void set_arg_1(D_RuleExpressionPtr arg);
  D_BinOp get_arg_2();
  void set_arg_2(D_BinOp arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_BinExpr& arg);
extern bool operator==(sub_D_RuleExpression_RE_BinExpr& lhs, sub_D_RuleExpression_RE_BinExpr& rhs);
extern bool operator<(sub_D_RuleExpression_RE_BinExpr& lhs, sub_D_RuleExpression_RE_BinExpr& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_BinExpr(D_RuleExpressionPtr arg_0, D_RuleExpressionPtr arg_1, D_BinOp arg_2);


class sub_D_RuleExpression_RE_CondExpr : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_CondExpr();
  D_RuleExpressionPtr arg_0;
  D_RuleExpressionPtr arg_1;
  bool arg_2;
public:
  sub_D_RuleExpression_RE_CondExpr(D_RuleExpressionPtr arg_0, D_RuleExpressionPtr arg_1, bool arg_2);
  sub_D_RuleExpression_RE_CondExpr(sub_D_RuleExpression_RE_CondExpr const& other); 
  sub_D_RuleExpression_RE_CondExpr(sub_D_RuleExpression_RE_CondExpr&& other);
  virtual ~sub_D_RuleExpression_RE_CondExpr(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
  D_RuleExpressionPtr get_arg_1();
  void set_arg_1(D_RuleExpressionPtr arg);
  bool get_arg_2();
  void set_arg_2(bool arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_CondExpr& arg);
extern bool operator==(sub_D_RuleExpression_RE_CondExpr& lhs, sub_D_RuleExpression_RE_CondExpr& rhs);
extern bool operator<(sub_D_RuleExpression_RE_CondExpr& lhs, sub_D_RuleExpression_RE_CondExpr& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_CondExpr(D_RuleExpressionPtr arg_0, D_RuleExpressionPtr arg_1, bool arg_2);


class sub_D_RuleExpression_RE_ErrExpr : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_ErrExpr();
  D_RuleExpressionPtr arg_0;
public:
  sub_D_RuleExpression_RE_ErrExpr(D_RuleExpressionPtr arg_0);
  sub_D_RuleExpression_RE_ErrExpr(sub_D_RuleExpression_RE_ErrExpr const& other); 
  sub_D_RuleExpression_RE_ErrExpr(sub_D_RuleExpression_RE_ErrExpr&& other);
  virtual ~sub_D_RuleExpression_RE_ErrExpr(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_ErrExpr& arg);
extern bool operator==(sub_D_RuleExpression_RE_ErrExpr& lhs, sub_D_RuleExpression_RE_ErrExpr& rhs);
extern bool operator<(sub_D_RuleExpression_RE_ErrExpr& lhs, sub_D_RuleExpression_RE_ErrExpr& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_ErrExpr(D_RuleExpressionPtr arg_0);


class sub_D_RuleExpression_RE_Let : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Let();
  D_RuleLetvarDefs arg_0;
  D_RuleExpressionPtr arg_1;
public:
  sub_D_RuleExpression_RE_Let(D_RuleLetvarDefs arg_0, D_RuleExpressionPtr arg_1);
  sub_D_RuleExpression_RE_Let(sub_D_RuleExpression_RE_Let const& other); 
  sub_D_RuleExpression_RE_Let(sub_D_RuleExpression_RE_Let&& other);
  virtual ~sub_D_RuleExpression_RE_Let(); 
  
  D_RuleLetvarDefs get_arg_0();
  void set_arg_0(D_RuleLetvarDefs arg);
  D_RuleExpressionPtr get_arg_1();
  void set_arg_1(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Let& arg);
extern bool operator==(sub_D_RuleExpression_RE_Let& lhs, sub_D_RuleExpression_RE_Let& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Let& lhs, sub_D_RuleExpression_RE_Let& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Let(D_RuleLetvarDefs arg_0, D_RuleExpressionPtr arg_1);


class sub_D_RuleExpression_RE_Lambda : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Lambda();
  StringTableKeyList arg_0;
  D_types arg_1;
  D_RuleExpressionPtr arg_2;
public:
  sub_D_RuleExpression_RE_Lambda(StringTableKeyList arg_0, D_types arg_1, D_RuleExpressionPtr arg_2);
  sub_D_RuleExpression_RE_Lambda(sub_D_RuleExpression_RE_Lambda const& other); 
  sub_D_RuleExpression_RE_Lambda(sub_D_RuleExpression_RE_Lambda&& other);
  virtual ~sub_D_RuleExpression_RE_Lambda(); 
  
  StringTableKeyList get_arg_0();
  void set_arg_0(StringTableKeyList arg);
  D_types get_arg_1();
  void set_arg_1(D_types arg);
  D_RuleExpressionPtr get_arg_2();
  void set_arg_2(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Lambda& arg);
extern bool operator==(sub_D_RuleExpression_RE_Lambda& lhs, sub_D_RuleExpression_RE_Lambda& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Lambda& lhs, sub_D_RuleExpression_RE_Lambda& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Lambda(StringTableKeyList arg_0, D_types arg_1, D_RuleExpressionPtr arg_2);


class sub_D_RuleExpression_RE_Order : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Order();
  D_RuleComputations arg_0;
public:
  sub_D_RuleExpression_RE_Order(D_RuleComputations arg_0);
  sub_D_RuleExpression_RE_Order(sub_D_RuleExpression_RE_Order const& other); 
  sub_D_RuleExpression_RE_Order(sub_D_RuleExpression_RE_Order&& other);
  virtual ~sub_D_RuleExpression_RE_Order(); 
  
  D_RuleComputations get_arg_0();
  void set_arg_0(D_RuleComputations arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Order& arg);
extern bool operator==(sub_D_RuleExpression_RE_Order& lhs, sub_D_RuleExpression_RE_Order& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Order& lhs, sub_D_RuleExpression_RE_Order& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Order(D_RuleComputations arg_0);


class sub_D_RuleExpression_RE_PostFix : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_PostFix();
  D_RuleExpressionPtr arg_0;
  D_RuleExpressionPtr arg_1;
  D_PostOp arg_2;
public:
  sub_D_RuleExpression_RE_PostFix(D_RuleExpressionPtr arg_0, D_RuleExpressionPtr arg_1, D_PostOp arg_2);
  sub_D_RuleExpression_RE_PostFix(sub_D_RuleExpression_RE_PostFix const& other); 
  sub_D_RuleExpression_RE_PostFix(sub_D_RuleExpression_RE_PostFix&& other);
  virtual ~sub_D_RuleExpression_RE_PostFix(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
  D_RuleExpressionPtr get_arg_1();
  void set_arg_1(D_RuleExpressionPtr arg);
  D_PostOp get_arg_2();
  void set_arg_2(D_PostOp arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_PostFix& arg);
extern bool operator==(sub_D_RuleExpression_RE_PostFix& lhs, sub_D_RuleExpression_RE_PostFix& rhs);
extern bool operator<(sub_D_RuleExpression_RE_PostFix& lhs, sub_D_RuleExpression_RE_PostFix& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_PostFix(D_RuleExpressionPtr arg_0, D_RuleExpressionPtr arg_1, D_PostOp arg_2);


class sub_D_RuleExpression_RE_Unary : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Unary();
  D_RuleExpressionPtr arg_0;
  D_UnaryOp arg_1;
public:
  sub_D_RuleExpression_RE_Unary(D_RuleExpressionPtr arg_0, D_UnaryOp arg_1);
  sub_D_RuleExpression_RE_Unary(sub_D_RuleExpression_RE_Unary const& other); 
  sub_D_RuleExpression_RE_Unary(sub_D_RuleExpression_RE_Unary&& other);
  virtual ~sub_D_RuleExpression_RE_Unary(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
  D_UnaryOp get_arg_1();
  void set_arg_1(D_UnaryOp arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Unary& arg);
extern bool operator==(sub_D_RuleExpression_RE_Unary& lhs, sub_D_RuleExpression_RE_Unary& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Unary& lhs, sub_D_RuleExpression_RE_Unary& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Unary(D_RuleExpressionPtr arg_0, D_UnaryOp arg_1);


class sub_D_RuleExpression_RE_Tuple : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Tuple();
  D_RuleExpressions arg_0;
public:
  sub_D_RuleExpression_RE_Tuple(D_RuleExpressions arg_0);
  sub_D_RuleExpression_RE_Tuple(sub_D_RuleExpression_RE_Tuple const& other); 
  sub_D_RuleExpression_RE_Tuple(sub_D_RuleExpression_RE_Tuple&& other);
  virtual ~sub_D_RuleExpression_RE_Tuple(); 
  
  D_RuleExpressions get_arg_0();
  void set_arg_0(D_RuleExpressions arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Tuple& arg);
extern bool operator==(sub_D_RuleExpression_RE_Tuple& lhs, sub_D_RuleExpression_RE_Tuple& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Tuple& lhs, sub_D_RuleExpression_RE_Tuple& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Tuple(D_RuleExpressions arg_0);


class sub_D_RuleExpression_RE_Attribute : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Attribute();
  D_RuleSymbolAttributePtr arg_0;
public:
  sub_D_RuleExpression_RE_Attribute(D_RuleSymbolAttributePtr arg_0);
  sub_D_RuleExpression_RE_Attribute(sub_D_RuleExpression_RE_Attribute const& other); 
  sub_D_RuleExpression_RE_Attribute(sub_D_RuleExpression_RE_Attribute&& other);
  virtual ~sub_D_RuleExpression_RE_Attribute(); 
  
  D_RuleSymbolAttributePtr get_arg_0();
  void set_arg_0(D_RuleSymbolAttributePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Attribute& arg);
extern bool operator==(sub_D_RuleExpression_RE_Attribute& lhs, sub_D_RuleExpression_RE_Attribute& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Attribute& lhs, sub_D_RuleExpression_RE_Attribute& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Attribute(D_RuleSymbolAttributePtr arg_0);


class sub_D_RuleExpression_RE_Call : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Call();
  D_RuleExpressionPtr arg_0;
  D_RuleExpressions arg_1;
public:
  sub_D_RuleExpression_RE_Call(D_RuleExpressionPtr arg_0, D_RuleExpressions arg_1);
  sub_D_RuleExpression_RE_Call(sub_D_RuleExpression_RE_Call const& other); 
  sub_D_RuleExpression_RE_Call(sub_D_RuleExpression_RE_Call&& other);
  virtual ~sub_D_RuleExpression_RE_Call(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
  D_RuleExpressions get_arg_1();
  void set_arg_1(D_RuleExpressions arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Call& arg);
extern bool operator==(sub_D_RuleExpression_RE_Call& lhs, sub_D_RuleExpression_RE_Call& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Call& lhs, sub_D_RuleExpression_RE_Call& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Call(D_RuleExpressionPtr arg_0, D_RuleExpressions arg_1);


class sub_D_RuleExpression_RE_Guards : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Guards();
  D_RuleExpressions arg_0;
public:
  sub_D_RuleExpression_RE_Guards(D_RuleExpressions arg_0);
  sub_D_RuleExpression_RE_Guards(sub_D_RuleExpression_RE_Guards const& other); 
  sub_D_RuleExpression_RE_Guards(sub_D_RuleExpression_RE_Guards&& other);
  virtual ~sub_D_RuleExpression_RE_Guards(); 
  
  D_RuleExpressions get_arg_0();
  void set_arg_0(D_RuleExpressions arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Guards& arg);
extern bool operator==(sub_D_RuleExpression_RE_Guards& lhs, sub_D_RuleExpression_RE_Guards& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Guards& lhs, sub_D_RuleExpression_RE_Guards& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Guards(D_RuleExpressions arg_0);


class sub_D_RuleExpression_RE_DefaultGuard : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_DefaultGuard();
  D_RuleExpressionPtr arg_0;
public:
  sub_D_RuleExpression_RE_DefaultGuard(D_RuleExpressionPtr arg_0);
  sub_D_RuleExpression_RE_DefaultGuard(sub_D_RuleExpression_RE_DefaultGuard const& other); 
  sub_D_RuleExpression_RE_DefaultGuard(sub_D_RuleExpression_RE_DefaultGuard&& other);
  virtual ~sub_D_RuleExpression_RE_DefaultGuard(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_DefaultGuard& arg);
extern bool operator==(sub_D_RuleExpression_RE_DefaultGuard& lhs, sub_D_RuleExpression_RE_DefaultGuard& rhs);
extern bool operator<(sub_D_RuleExpression_RE_DefaultGuard& lhs, sub_D_RuleExpression_RE_DefaultGuard& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_DefaultGuard(D_RuleExpressionPtr arg_0);


class sub_D_RuleExpression_RE_GuardExpr : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_GuardExpr();
  D_RuleExpressions arg_0;
  D_RuleExpressionPtr arg_1;
public:
  sub_D_RuleExpression_RE_GuardExpr(D_RuleExpressions arg_0, D_RuleExpressionPtr arg_1);
  sub_D_RuleExpression_RE_GuardExpr(sub_D_RuleExpression_RE_GuardExpr const& other); 
  sub_D_RuleExpression_RE_GuardExpr(sub_D_RuleExpression_RE_GuardExpr&& other);
  virtual ~sub_D_RuleExpression_RE_GuardExpr(); 
  
  D_RuleExpressions get_arg_0();
  void set_arg_0(D_RuleExpressions arg);
  D_RuleExpressionPtr get_arg_1();
  void set_arg_1(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_GuardExpr& arg);
extern bool operator==(sub_D_RuleExpression_RE_GuardExpr& lhs, sub_D_RuleExpression_RE_GuardExpr& rhs);
extern bool operator<(sub_D_RuleExpression_RE_GuardExpr& lhs, sub_D_RuleExpression_RE_GuardExpr& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_GuardExpr(D_RuleExpressions arg_0, D_RuleExpressionPtr arg_1);


class sub_D_RuleExpression_RE_Wrap : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Wrap();
  D_RuleExpressionPtr arg_0;
public:
  sub_D_RuleExpression_RE_Wrap(D_RuleExpressionPtr arg_0);
  sub_D_RuleExpression_RE_Wrap(sub_D_RuleExpression_RE_Wrap const& other); 
  sub_D_RuleExpression_RE_Wrap(sub_D_RuleExpression_RE_Wrap&& other);
  virtual ~sub_D_RuleExpression_RE_Wrap(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Wrap& arg);
extern bool operator==(sub_D_RuleExpression_RE_Wrap& lhs, sub_D_RuleExpression_RE_Wrap& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Wrap& lhs, sub_D_RuleExpression_RE_Wrap& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Wrap(D_RuleExpressionPtr arg_0);


class sub_D_RuleExpression_RE_Number : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Number();
  StringTableKey arg_0;
public:
  sub_D_RuleExpression_RE_Number(StringTableKey arg_0);
  sub_D_RuleExpression_RE_Number(sub_D_RuleExpression_RE_Number const& other); 
  sub_D_RuleExpression_RE_Number(sub_D_RuleExpression_RE_Number&& other);
  virtual ~sub_D_RuleExpression_RE_Number(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Number& arg);
extern bool operator==(sub_D_RuleExpression_RE_Number& lhs, sub_D_RuleExpression_RE_Number& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Number& lhs, sub_D_RuleExpression_RE_Number& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Number(StringTableKey arg_0);


class sub_D_RuleExpression_RE_String : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_String();
  StringTableKey arg_0;
public:
  sub_D_RuleExpression_RE_String(StringTableKey arg_0);
  sub_D_RuleExpression_RE_String(sub_D_RuleExpression_RE_String const& other); 
  sub_D_RuleExpression_RE_String(sub_D_RuleExpression_RE_String&& other);
  virtual ~sub_D_RuleExpression_RE_String(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_String& arg);
extern bool operator==(sub_D_RuleExpression_RE_String& lhs, sub_D_RuleExpression_RE_String& rhs);
extern bool operator<(sub_D_RuleExpression_RE_String& lhs, sub_D_RuleExpression_RE_String& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_String(StringTableKey arg_0);


class sub_D_RuleExpression_RE_Name : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Name();
  StringTableKey arg_0;
public:
  sub_D_RuleExpression_RE_Name(StringTableKey arg_0);
  sub_D_RuleExpression_RE_Name(sub_D_RuleExpression_RE_Name const& other); 
  sub_D_RuleExpression_RE_Name(sub_D_RuleExpression_RE_Name&& other);
  virtual ~sub_D_RuleExpression_RE_Name(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Name& arg);
extern bool operator==(sub_D_RuleExpression_RE_Name& lhs, sub_D_RuleExpression_RE_Name& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Name& lhs, sub_D_RuleExpression_RE_Name& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Name(StringTableKey arg_0);


class sub_D_RuleExpression_RE_Rule : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Rule();
  StringTableKey arg_0;
  StringTableKey arg_1;
public:
  sub_D_RuleExpression_RE_Rule(StringTableKey arg_0, StringTableKey arg_1);
  sub_D_RuleExpression_RE_Rule(sub_D_RuleExpression_RE_Rule const& other); 
  sub_D_RuleExpression_RE_Rule(sub_D_RuleExpression_RE_Rule&& other);
  virtual ~sub_D_RuleExpression_RE_Rule(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Rule& arg);
extern bool operator==(sub_D_RuleExpression_RE_Rule& lhs, sub_D_RuleExpression_RE_Rule& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Rule& lhs, sub_D_RuleExpression_RE_Rule& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Rule(StringTableKey arg_0, StringTableKey arg_1);


class sub_D_RuleExpression_RE_Bool : public D_RuleExpression { 
protected: 
  sub_D_RuleExpression_RE_Bool();
  bool arg_0;
public:
  sub_D_RuleExpression_RE_Bool(bool arg_0);
  sub_D_RuleExpression_RE_Bool(sub_D_RuleExpression_RE_Bool const& other); 
  sub_D_RuleExpression_RE_Bool(sub_D_RuleExpression_RE_Bool&& other);
  virtual ~sub_D_RuleExpression_RE_Bool(); 
  
  bool get_arg_0();
  void set_arg_0(bool arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Bool& arg);
extern bool operator==(sub_D_RuleExpression_RE_Bool& lhs, sub_D_RuleExpression_RE_Bool& rhs);
extern bool operator<(sub_D_RuleExpression_RE_Bool& lhs, sub_D_RuleExpression_RE_Bool& rhs);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Bool(bool arg_0);


class sub_D_RuleExpression_RE_CoordRef : public D_RuleExpression { 
public:
  sub_D_RuleExpression_RE_CoordRef();
  sub_D_RuleExpression_RE_CoordRef(sub_D_RuleExpression_RE_CoordRef const& other);
  sub_D_RuleExpression_RE_CoordRef(sub_D_RuleExpression_RE_CoordRef&& other); 
  virtual ~sub_D_RuleExpression_RE_CoordRef();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_CoordRef& arg);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_CoordRef();


class sub_D_RuleExpression_RE_EmptyList : public D_RuleExpression { 
public:
  sub_D_RuleExpression_RE_EmptyList();
  sub_D_RuleExpression_RE_EmptyList(sub_D_RuleExpression_RE_EmptyList const& other);
  sub_D_RuleExpression_RE_EmptyList(sub_D_RuleExpression_RE_EmptyList&& other); 
  virtual ~sub_D_RuleExpression_RE_EmptyList();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_EmptyList& arg);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_EmptyList();


class sub_D_RuleExpression_RE_Invalid : public D_RuleExpression { 
public:
  sub_D_RuleExpression_RE_Invalid();
  sub_D_RuleExpression_RE_Invalid(sub_D_RuleExpression_RE_Invalid const& other);
  sub_D_RuleExpression_RE_Invalid(sub_D_RuleExpression_RE_Invalid&& other); 
  virtual ~sub_D_RuleExpression_RE_Invalid();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleExpression_RE_Invalid& arg);
extern D_RuleExpressionPtr create_sub_D_RuleExpression_RE_Invalid();


class D_RuleLetvarDef { 
public:
  enum class D_RuleLetvarDef_kind { Unknown, D_RuleLetvarDef}; 
protected: 
  D_RuleLetvarDef_kind kind; 

D_RuleLetvarDef(); 
public: 
  virtual ~D_RuleLetvarDef();
  D_RuleLetvarDef(D_RuleLetvarDef const& other);
  D_RuleLetvarDef(D_RuleLetvarDef::D_RuleLetvarDef_kind kind);
  D_RuleLetvarDef(D_RuleLetvarDef&& other);
  
  D_RuleLetvarDef_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_RuleLetvarDef* arg); 
extern bool operator<(D_RuleLetvarDefPtr lhs, D_RuleLetvarDefPtr rhs);
extern bool operator==(D_RuleLetvarDefPtr lhs, D_RuleLetvarDefPtr rhs);

class sub_D_RuleLetvarDef_D_RuleLetvarDef : public D_RuleLetvarDef { 
protected: 
  sub_D_RuleLetvarDef_D_RuleLetvarDef();
  StringTableKey arg_0;
  D_RuleExpressionPtr arg_1;
  D_typePtr arg_2;
public:
  sub_D_RuleLetvarDef_D_RuleLetvarDef(StringTableKey arg_0, D_RuleExpressionPtr arg_1, D_typePtr arg_2);
  sub_D_RuleLetvarDef_D_RuleLetvarDef(sub_D_RuleLetvarDef_D_RuleLetvarDef const& other); 
  sub_D_RuleLetvarDef_D_RuleLetvarDef(sub_D_RuleLetvarDef_D_RuleLetvarDef&& other);
  virtual ~sub_D_RuleLetvarDef_D_RuleLetvarDef(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
  D_RuleExpressionPtr get_arg_1();
  void set_arg_1(D_RuleExpressionPtr arg);
  D_typePtr get_arg_2();
  void set_arg_2(D_typePtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RuleLetvarDef_D_RuleLetvarDef& arg);
extern bool operator==(sub_D_RuleLetvarDef_D_RuleLetvarDef& lhs, sub_D_RuleLetvarDef_D_RuleLetvarDef& rhs);
extern bool operator<(sub_D_RuleLetvarDef_D_RuleLetvarDef& lhs, sub_D_RuleLetvarDef_D_RuleLetvarDef& rhs);
extern D_RuleLetvarDefPtr create_sub_D_RuleLetvarDef_D_RuleLetvarDef(StringTableKey arg_0, D_RuleExpressionPtr arg_1, D_typePtr arg_2);


class D_SymbolLocalAttribute { 
public:
  enum class D_SymbolLocalAttribute_kind { Unknown, D_SymbolLocalAttribute}; 
protected: 
  D_SymbolLocalAttribute_kind kind; 

D_SymbolLocalAttribute(); 
public: 
  virtual ~D_SymbolLocalAttribute();
  D_SymbolLocalAttribute(D_SymbolLocalAttribute const& other);
  D_SymbolLocalAttribute(D_SymbolLocalAttribute::D_SymbolLocalAttribute_kind kind);
  D_SymbolLocalAttribute(D_SymbolLocalAttribute&& other);
  
  D_SymbolLocalAttribute_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_SymbolLocalAttribute* arg); 
extern bool operator<(D_SymbolLocalAttributePtr lhs, D_SymbolLocalAttributePtr rhs);
extern bool operator==(D_SymbolLocalAttributePtr lhs, D_SymbolLocalAttributePtr rhs);

class sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute : public D_SymbolLocalAttribute { 
protected: 
  sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute();
  D_LocalAttributeClass arg_0;
  StringTableKey arg_1;
  StringTableKey arg_2;
public:
  sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute(D_LocalAttributeClass arg_0, StringTableKey arg_1, StringTableKey arg_2);
  sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute(sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute const& other); 
  sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute(sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute&& other);
  virtual ~sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute(); 
  
  D_LocalAttributeClass get_arg_0();
  void set_arg_0(D_LocalAttributeClass arg);
  StringTableKey get_arg_1();
  void set_arg_1(StringTableKey arg);
  StringTableKey get_arg_2();
  void set_arg_2(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute& arg);
extern bool operator==(sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute& lhs, sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute& rhs);
extern bool operator<(sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute& lhs, sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute& rhs);
extern D_SymbolLocalAttributePtr create_sub_D_SymbolLocalAttribute_D_SymbolLocalAttribute(D_LocalAttributeClass arg_0, StringTableKey arg_1, StringTableKey arg_2);


class D_RemoteCombine { 
public:
  enum class D_RemoteCombine_kind { Unknown, RC_BinOp, RC_Call, RC_Infer, RC_Lambda}; 
protected: 
  D_RemoteCombine_kind kind; 

D_RemoteCombine(); 
public: 
  virtual ~D_RemoteCombine();
  D_RemoteCombine(D_RemoteCombine const& other);
  D_RemoteCombine(D_RemoteCombine::D_RemoteCombine_kind kind);
  D_RemoteCombine(D_RemoteCombine&& other);
  
  D_RemoteCombine_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_RemoteCombine* arg); 
extern bool operator<(D_RemoteCombinePtr lhs, D_RemoteCombinePtr rhs);
extern bool operator==(D_RemoteCombinePtr lhs, D_RemoteCombinePtr rhs);

class sub_D_RemoteCombine_RC_BinOp : public D_RemoteCombine { 
protected: 
  sub_D_RemoteCombine_RC_BinOp();
  D_BinOp arg_0;
public:
  sub_D_RemoteCombine_RC_BinOp(D_BinOp arg_0);
  sub_D_RemoteCombine_RC_BinOp(sub_D_RemoteCombine_RC_BinOp const& other); 
  sub_D_RemoteCombine_RC_BinOp(sub_D_RemoteCombine_RC_BinOp&& other);
  virtual ~sub_D_RemoteCombine_RC_BinOp(); 
  
  D_BinOp get_arg_0();
  void set_arg_0(D_BinOp arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteCombine_RC_BinOp& arg);
extern bool operator==(sub_D_RemoteCombine_RC_BinOp& lhs, sub_D_RemoteCombine_RC_BinOp& rhs);
extern bool operator<(sub_D_RemoteCombine_RC_BinOp& lhs, sub_D_RemoteCombine_RC_BinOp& rhs);
extern D_RemoteCombinePtr create_sub_D_RemoteCombine_RC_BinOp(D_BinOp arg_0);


class sub_D_RemoteCombine_RC_Call : public D_RemoteCombine { 
protected: 
  sub_D_RemoteCombine_RC_Call();
  Binding arg_0;
public:
  sub_D_RemoteCombine_RC_Call(Binding arg_0);
  sub_D_RemoteCombine_RC_Call(sub_D_RemoteCombine_RC_Call const& other); 
  sub_D_RemoteCombine_RC_Call(sub_D_RemoteCombine_RC_Call&& other);
  virtual ~sub_D_RemoteCombine_RC_Call(); 
  
  Binding get_arg_0();
  void set_arg_0(Binding arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteCombine_RC_Call& arg);
extern bool operator==(sub_D_RemoteCombine_RC_Call& lhs, sub_D_RemoteCombine_RC_Call& rhs);
extern bool operator<(sub_D_RemoteCombine_RC_Call& lhs, sub_D_RemoteCombine_RC_Call& rhs);
extern D_RemoteCombinePtr create_sub_D_RemoteCombine_RC_Call(Binding arg_0);


class sub_D_RemoteCombine_RC_Infer : public D_RemoteCombine { 
public:
  sub_D_RemoteCombine_RC_Infer();
  sub_D_RemoteCombine_RC_Infer(sub_D_RemoteCombine_RC_Infer const& other);
  sub_D_RemoteCombine_RC_Infer(sub_D_RemoteCombine_RC_Infer&& other); 
  virtual ~sub_D_RemoteCombine_RC_Infer();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteCombine_RC_Infer& arg);
extern D_RemoteCombinePtr create_sub_D_RemoteCombine_RC_Infer();


class sub_D_RemoteCombine_RC_Lambda : public D_RemoteCombine { 
protected: 
  sub_D_RemoteCombine_RC_Lambda();
  D_RuleExpressionPtr arg_0;
public:
  sub_D_RemoteCombine_RC_Lambda(D_RuleExpressionPtr arg_0);
  sub_D_RemoteCombine_RC_Lambda(sub_D_RemoteCombine_RC_Lambda const& other); 
  sub_D_RemoteCombine_RC_Lambda(sub_D_RemoteCombine_RC_Lambda&& other);
  virtual ~sub_D_RemoteCombine_RC_Lambda(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteCombine_RC_Lambda& arg);
extern bool operator==(sub_D_RemoteCombine_RC_Lambda& lhs, sub_D_RemoteCombine_RC_Lambda& rhs);
extern bool operator<(sub_D_RemoteCombine_RC_Lambda& lhs, sub_D_RemoteCombine_RC_Lambda& rhs);
extern D_RemoteCombinePtr create_sub_D_RemoteCombine_RC_Lambda(D_RuleExpressionPtr arg_0);


class D_RemoteSingle { 
public:
  enum class D_RemoteSingle_kind { Unknown, RSi_List, RSi_Infer, RSi_Lambda, RSi_Call}; 
protected: 
  D_RemoteSingle_kind kind; 

D_RemoteSingle(); 
public: 
  virtual ~D_RemoteSingle();
  D_RemoteSingle(D_RemoteSingle const& other);
  D_RemoteSingle(D_RemoteSingle::D_RemoteSingle_kind kind);
  D_RemoteSingle(D_RemoteSingle&& other);
  
  D_RemoteSingle_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_RemoteSingle* arg); 
extern bool operator<(D_RemoteSinglePtr lhs, D_RemoteSinglePtr rhs);
extern bool operator==(D_RemoteSinglePtr lhs, D_RemoteSinglePtr rhs);

class sub_D_RemoteSingle_RSi_List : public D_RemoteSingle { 
public:
  sub_D_RemoteSingle_RSi_List();
  sub_D_RemoteSingle_RSi_List(sub_D_RemoteSingle_RSi_List const& other);
  sub_D_RemoteSingle_RSi_List(sub_D_RemoteSingle_RSi_List&& other); 
  virtual ~sub_D_RemoteSingle_RSi_List();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteSingle_RSi_List& arg);
extern D_RemoteSinglePtr create_sub_D_RemoteSingle_RSi_List();


class sub_D_RemoteSingle_RSi_Infer : public D_RemoteSingle { 
public:
  sub_D_RemoteSingle_RSi_Infer();
  sub_D_RemoteSingle_RSi_Infer(sub_D_RemoteSingle_RSi_Infer const& other);
  sub_D_RemoteSingle_RSi_Infer(sub_D_RemoteSingle_RSi_Infer&& other); 
  virtual ~sub_D_RemoteSingle_RSi_Infer();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteSingle_RSi_Infer& arg);
extern D_RemoteSinglePtr create_sub_D_RemoteSingle_RSi_Infer();


class sub_D_RemoteSingle_RSi_Lambda : public D_RemoteSingle { 
protected: 
  sub_D_RemoteSingle_RSi_Lambda();
  D_RuleExpressionPtr arg_0;
public:
  sub_D_RemoteSingle_RSi_Lambda(D_RuleExpressionPtr arg_0);
  sub_D_RemoteSingle_RSi_Lambda(sub_D_RemoteSingle_RSi_Lambda const& other); 
  sub_D_RemoteSingle_RSi_Lambda(sub_D_RemoteSingle_RSi_Lambda&& other);
  virtual ~sub_D_RemoteSingle_RSi_Lambda(); 
  
  D_RuleExpressionPtr get_arg_0();
  void set_arg_0(D_RuleExpressionPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteSingle_RSi_Lambda& arg);
extern bool operator==(sub_D_RemoteSingle_RSi_Lambda& lhs, sub_D_RemoteSingle_RSi_Lambda& rhs);
extern bool operator<(sub_D_RemoteSingle_RSi_Lambda& lhs, sub_D_RemoteSingle_RSi_Lambda& rhs);
extern D_RemoteSinglePtr create_sub_D_RemoteSingle_RSi_Lambda(D_RuleExpressionPtr arg_0);


class sub_D_RemoteSingle_RSi_Call : public D_RemoteSingle { 
protected: 
  sub_D_RemoteSingle_RSi_Call();
  Binding arg_0;
public:
  sub_D_RemoteSingle_RSi_Call(Binding arg_0);
  sub_D_RemoteSingle_RSi_Call(sub_D_RemoteSingle_RSi_Call const& other); 
  sub_D_RemoteSingle_RSi_Call(sub_D_RemoteSingle_RSi_Call&& other);
  virtual ~sub_D_RemoteSingle_RSi_Call(); 
  
  Binding get_arg_0();
  void set_arg_0(Binding arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteSingle_RSi_Call& arg);
extern bool operator==(sub_D_RemoteSingle_RSi_Call& lhs, sub_D_RemoteSingle_RSi_Call& rhs);
extern bool operator<(sub_D_RemoteSingle_RSi_Call& lhs, sub_D_RemoteSingle_RSi_Call& rhs);
extern D_RemoteSinglePtr create_sub_D_RemoteSingle_RSi_Call(Binding arg_0);


class D_RemoteEmpty { 
public:
  enum class D_RemoteEmpty_kind { Unknown, REm_Constant, REm_Call, REm_Infer}; 
protected: 
  D_RemoteEmpty_kind kind; 

D_RemoteEmpty(); 
public: 
  virtual ~D_RemoteEmpty();
  D_RemoteEmpty(D_RemoteEmpty const& other);
  D_RemoteEmpty(D_RemoteEmpty::D_RemoteEmpty_kind kind);
  D_RemoteEmpty(D_RemoteEmpty&& other);
  
  D_RemoteEmpty_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_RemoteEmpty* arg); 
extern bool operator<(D_RemoteEmptyPtr lhs, D_RemoteEmptyPtr rhs);
extern bool operator==(D_RemoteEmptyPtr lhs, D_RemoteEmptyPtr rhs);

class sub_D_RemoteEmpty_REm_Constant : public D_RemoteEmpty { 
protected: 
  sub_D_RemoteEmpty_REm_Constant();
  D_constantPtr arg_0;
public:
  sub_D_RemoteEmpty_REm_Constant(D_constantPtr arg_0);
  sub_D_RemoteEmpty_REm_Constant(sub_D_RemoteEmpty_REm_Constant const& other); 
  sub_D_RemoteEmpty_REm_Constant(sub_D_RemoteEmpty_REm_Constant&& other);
  virtual ~sub_D_RemoteEmpty_REm_Constant(); 
  
  D_constantPtr get_arg_0();
  void set_arg_0(D_constantPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteEmpty_REm_Constant& arg);
extern bool operator==(sub_D_RemoteEmpty_REm_Constant& lhs, sub_D_RemoteEmpty_REm_Constant& rhs);
extern bool operator<(sub_D_RemoteEmpty_REm_Constant& lhs, sub_D_RemoteEmpty_REm_Constant& rhs);
extern D_RemoteEmptyPtr create_sub_D_RemoteEmpty_REm_Constant(D_constantPtr arg_0);


class sub_D_RemoteEmpty_REm_Call : public D_RemoteEmpty { 
protected: 
  sub_D_RemoteEmpty_REm_Call();
  Binding arg_0;
public:
  sub_D_RemoteEmpty_REm_Call(Binding arg_0);
  sub_D_RemoteEmpty_REm_Call(sub_D_RemoteEmpty_REm_Call const& other); 
  sub_D_RemoteEmpty_REm_Call(sub_D_RemoteEmpty_REm_Call&& other);
  virtual ~sub_D_RemoteEmpty_REm_Call(); 
  
  Binding get_arg_0();
  void set_arg_0(Binding arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteEmpty_REm_Call& arg);
extern bool operator==(sub_D_RemoteEmpty_REm_Call& lhs, sub_D_RemoteEmpty_REm_Call& rhs);
extern bool operator<(sub_D_RemoteEmpty_REm_Call& lhs, sub_D_RemoteEmpty_REm_Call& rhs);
extern D_RemoteEmptyPtr create_sub_D_RemoteEmpty_REm_Call(Binding arg_0);


class sub_D_RemoteEmpty_REm_Infer : public D_RemoteEmpty { 
public:
  sub_D_RemoteEmpty_REm_Infer();
  sub_D_RemoteEmpty_REm_Infer(sub_D_RemoteEmpty_REm_Infer const& other);
  sub_D_RemoteEmpty_REm_Infer(sub_D_RemoteEmpty_REm_Infer&& other); 
  virtual ~sub_D_RemoteEmpty_REm_Infer();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteEmpty_REm_Infer& arg);
extern D_RemoteEmptyPtr create_sub_D_RemoteEmpty_REm_Infer();


class D_RemoteShield { 
public:
  enum class D_RemoteShield_kind { Unknown, RS_None, RS_Unshield, RS_Shield}; 
protected: 
  D_RemoteShield_kind kind; 

D_RemoteShield(); 
public: 
  virtual ~D_RemoteShield();
  D_RemoteShield(D_RemoteShield const& other);
  D_RemoteShield(D_RemoteShield::D_RemoteShield_kind kind);
  D_RemoteShield(D_RemoteShield&& other);
  
  D_RemoteShield_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_RemoteShield* arg); 
extern bool operator<(D_RemoteShieldPtr lhs, D_RemoteShieldPtr rhs);
extern bool operator==(D_RemoteShieldPtr lhs, D_RemoteShieldPtr rhs);

class sub_D_RemoteShield_RS_None : public D_RemoteShield { 
public:
  sub_D_RemoteShield_RS_None();
  sub_D_RemoteShield_RS_None(sub_D_RemoteShield_RS_None const& other);
  sub_D_RemoteShield_RS_None(sub_D_RemoteShield_RS_None&& other); 
  virtual ~sub_D_RemoteShield_RS_None();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteShield_RS_None& arg);
extern D_RemoteShieldPtr create_sub_D_RemoteShield_RS_None();


class sub_D_RemoteShield_RS_Unshield : public D_RemoteShield { 
public:
  sub_D_RemoteShield_RS_Unshield();
  sub_D_RemoteShield_RS_Unshield(sub_D_RemoteShield_RS_Unshield const& other);
  sub_D_RemoteShield_RS_Unshield(sub_D_RemoteShield_RS_Unshield&& other); 
  virtual ~sub_D_RemoteShield_RS_Unshield();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteShield_RS_Unshield& arg);
extern D_RemoteShieldPtr create_sub_D_RemoteShield_RS_Unshield();


class sub_D_RemoteShield_RS_Shield : public D_RemoteShield { 
protected: 
  sub_D_RemoteShield_RS_Shield();
  StringTableKeyList arg_0;
public:
  sub_D_RemoteShield_RS_Shield(StringTableKeyList arg_0);
  sub_D_RemoteShield_RS_Shield(sub_D_RemoteShield_RS_Shield const& other); 
  sub_D_RemoteShield_RS_Shield(sub_D_RemoteShield_RS_Shield&& other);
  virtual ~sub_D_RemoteShield_RS_Shield(); 
  
  StringTableKeyList get_arg_0();
  void set_arg_0(StringTableKeyList arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteShield_RS_Shield& arg);
extern bool operator==(sub_D_RemoteShield_RS_Shield& lhs, sub_D_RemoteShield_RS_Shield& rhs);
extern bool operator<(sub_D_RemoteShield_RS_Shield& lhs, sub_D_RemoteShield_RS_Shield& rhs);
extern D_RemoteShieldPtr create_sub_D_RemoteShield_RS_Shield(StringTableKeyList arg_0);


class D_ConstituentOptions { 
public:
  enum class D_ConstituentOptions_kind { Unknown, CO_None, CO_OnlyShield, CO_Infer, CO_Functions}; 
protected: 
  D_ConstituentOptions_kind kind; 

D_ConstituentOptions(); 
public: 
  virtual ~D_ConstituentOptions();
  D_ConstituentOptions(D_ConstituentOptions const& other);
  D_ConstituentOptions(D_ConstituentOptions::D_ConstituentOptions_kind kind);
  D_ConstituentOptions(D_ConstituentOptions&& other);
  
  D_ConstituentOptions_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_ConstituentOptions* arg); 
extern bool operator<(D_ConstituentOptionsPtr lhs, D_ConstituentOptionsPtr rhs);
extern bool operator==(D_ConstituentOptionsPtr lhs, D_ConstituentOptionsPtr rhs);

class sub_D_ConstituentOptions_CO_None : public D_ConstituentOptions { 
public:
  sub_D_ConstituentOptions_CO_None();
  sub_D_ConstituentOptions_CO_None(sub_D_ConstituentOptions_CO_None const& other);
  sub_D_ConstituentOptions_CO_None(sub_D_ConstituentOptions_CO_None&& other); 
  virtual ~sub_D_ConstituentOptions_CO_None();
};

extern std::ostream& operator<<(std::ostream& os, sub_D_ConstituentOptions_CO_None& arg);
extern D_ConstituentOptionsPtr create_sub_D_ConstituentOptions_CO_None();


class sub_D_ConstituentOptions_CO_OnlyShield : public D_ConstituentOptions { 
protected: 
  sub_D_ConstituentOptions_CO_OnlyShield();
  D_RemoteShieldPtr arg_0;
public:
  sub_D_ConstituentOptions_CO_OnlyShield(D_RemoteShieldPtr arg_0);
  sub_D_ConstituentOptions_CO_OnlyShield(sub_D_ConstituentOptions_CO_OnlyShield const& other); 
  sub_D_ConstituentOptions_CO_OnlyShield(sub_D_ConstituentOptions_CO_OnlyShield&& other);
  virtual ~sub_D_ConstituentOptions_CO_OnlyShield(); 
  
  D_RemoteShieldPtr get_arg_0();
  void set_arg_0(D_RemoteShieldPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ConstituentOptions_CO_OnlyShield& arg);
extern bool operator==(sub_D_ConstituentOptions_CO_OnlyShield& lhs, sub_D_ConstituentOptions_CO_OnlyShield& rhs);
extern bool operator<(sub_D_ConstituentOptions_CO_OnlyShield& lhs, sub_D_ConstituentOptions_CO_OnlyShield& rhs);
extern D_ConstituentOptionsPtr create_sub_D_ConstituentOptions_CO_OnlyShield(D_RemoteShieldPtr arg_0);


class sub_D_ConstituentOptions_CO_Infer : public D_ConstituentOptions { 
protected: 
  sub_D_ConstituentOptions_CO_Infer();
  D_RemoteShieldPtr arg_0;
public:
  sub_D_ConstituentOptions_CO_Infer(D_RemoteShieldPtr arg_0);
  sub_D_ConstituentOptions_CO_Infer(sub_D_ConstituentOptions_CO_Infer const& other); 
  sub_D_ConstituentOptions_CO_Infer(sub_D_ConstituentOptions_CO_Infer&& other);
  virtual ~sub_D_ConstituentOptions_CO_Infer(); 
  
  D_RemoteShieldPtr get_arg_0();
  void set_arg_0(D_RemoteShieldPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ConstituentOptions_CO_Infer& arg);
extern bool operator==(sub_D_ConstituentOptions_CO_Infer& lhs, sub_D_ConstituentOptions_CO_Infer& rhs);
extern bool operator<(sub_D_ConstituentOptions_CO_Infer& lhs, sub_D_ConstituentOptions_CO_Infer& rhs);
extern D_ConstituentOptionsPtr create_sub_D_ConstituentOptions_CO_Infer(D_RemoteShieldPtr arg_0);


class sub_D_ConstituentOptions_CO_Functions : public D_ConstituentOptions { 
protected: 
  sub_D_ConstituentOptions_CO_Functions();
  D_RemoteShieldPtr arg_0;
  D_typePtr arg_1;
  D_RemoteCombinePtr arg_2;
  D_RemoteSinglePtr arg_3;
  D_RemoteEmptyPtr arg_4;
public:
  sub_D_ConstituentOptions_CO_Functions(D_RemoteShieldPtr arg_0, D_typePtr arg_1, D_RemoteCombinePtr arg_2, D_RemoteSinglePtr arg_3, D_RemoteEmptyPtr arg_4);
  sub_D_ConstituentOptions_CO_Functions(sub_D_ConstituentOptions_CO_Functions const& other); 
  sub_D_ConstituentOptions_CO_Functions(sub_D_ConstituentOptions_CO_Functions&& other);
  virtual ~sub_D_ConstituentOptions_CO_Functions(); 
  
  D_RemoteShieldPtr get_arg_0();
  void set_arg_0(D_RemoteShieldPtr arg);
  D_typePtr get_arg_1();
  void set_arg_1(D_typePtr arg);
  D_RemoteCombinePtr get_arg_2();
  void set_arg_2(D_RemoteCombinePtr arg);
  D_RemoteSinglePtr get_arg_3();
  void set_arg_3(D_RemoteSinglePtr arg);
  D_RemoteEmptyPtr get_arg_4();
  void set_arg_4(D_RemoteEmptyPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_ConstituentOptions_CO_Functions& arg);
extern bool operator==(sub_D_ConstituentOptions_CO_Functions& lhs, sub_D_ConstituentOptions_CO_Functions& rhs);
extern bool operator<(sub_D_ConstituentOptions_CO_Functions& lhs, sub_D_ConstituentOptions_CO_Functions& rhs);
extern D_ConstituentOptionsPtr create_sub_D_ConstituentOptions_CO_Functions(D_RemoteShieldPtr arg_0, D_typePtr arg_1, D_RemoteCombinePtr arg_2, D_RemoteSinglePtr arg_3, D_RemoteEmptyPtr arg_4);


class D_RemoteAttribute { 
public:
  enum class D_RemoteAttribute_kind { Unknown, D_RemoteIncluding, D_RemoteConstituents}; 
protected: 
  D_RemoteAttribute_kind kind; 

D_RemoteAttribute(); 
public: 
  virtual ~D_RemoteAttribute();
  D_RemoteAttribute(D_RemoteAttribute const& other);
  D_RemoteAttribute(D_RemoteAttribute::D_RemoteAttribute_kind kind);
  D_RemoteAttribute(D_RemoteAttribute&& other);
  
  D_RemoteAttribute_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_RemoteAttribute* arg); 
extern bool operator<(D_RemoteAttributePtr lhs, D_RemoteAttributePtr rhs);
extern bool operator==(D_RemoteAttributePtr lhs, D_RemoteAttributePtr rhs);

class sub_D_RemoteAttribute_D_RemoteIncluding : public D_RemoteAttribute { 
protected: 
  sub_D_RemoteAttribute_D_RemoteIncluding();
  D_SymbolLocalAttributes arg_0;
public:
  sub_D_RemoteAttribute_D_RemoteIncluding(D_SymbolLocalAttributes arg_0);
  sub_D_RemoteAttribute_D_RemoteIncluding(sub_D_RemoteAttribute_D_RemoteIncluding const& other); 
  sub_D_RemoteAttribute_D_RemoteIncluding(sub_D_RemoteAttribute_D_RemoteIncluding&& other);
  virtual ~sub_D_RemoteAttribute_D_RemoteIncluding(); 
  
  D_SymbolLocalAttributes get_arg_0();
  void set_arg_0(D_SymbolLocalAttributes arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteAttribute_D_RemoteIncluding& arg);
extern bool operator==(sub_D_RemoteAttribute_D_RemoteIncluding& lhs, sub_D_RemoteAttribute_D_RemoteIncluding& rhs);
extern bool operator<(sub_D_RemoteAttribute_D_RemoteIncluding& lhs, sub_D_RemoteAttribute_D_RemoteIncluding& rhs);
extern D_RemoteAttributePtr create_sub_D_RemoteAttribute_D_RemoteIncluding(D_SymbolLocalAttributes arg_0);


class sub_D_RemoteAttribute_D_RemoteConstituents : public D_RemoteAttribute { 
protected: 
  sub_D_RemoteAttribute_D_RemoteConstituents();
  D_SymbolLocalAttributes arg_0;
  D_ConstituentOptionsPtr arg_1;
public:
  sub_D_RemoteAttribute_D_RemoteConstituents(D_SymbolLocalAttributes arg_0, D_ConstituentOptionsPtr arg_1);
  sub_D_RemoteAttribute_D_RemoteConstituents(sub_D_RemoteAttribute_D_RemoteConstituents const& other); 
  sub_D_RemoteAttribute_D_RemoteConstituents(sub_D_RemoteAttribute_D_RemoteConstituents&& other);
  virtual ~sub_D_RemoteAttribute_D_RemoteConstituents(); 
  
  D_SymbolLocalAttributes get_arg_0();
  void set_arg_0(D_SymbolLocalAttributes arg);
  D_ConstituentOptionsPtr get_arg_1();
  void set_arg_1(D_ConstituentOptionsPtr arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_RemoteAttribute_D_RemoteConstituents& arg);
extern bool operator==(sub_D_RemoteAttribute_D_RemoteConstituents& lhs, sub_D_RemoteAttribute_D_RemoteConstituents& rhs);
extern bool operator<(sub_D_RemoteAttribute_D_RemoteConstituents& lhs, sub_D_RemoteAttribute_D_RemoteConstituents& rhs);
extern D_RemoteAttributePtr create_sub_D_RemoteAttribute_D_RemoteConstituents(D_SymbolLocalAttributes arg_0, D_ConstituentOptionsPtr arg_1);


class D_VirtAttr { 
public:
  enum class D_VirtAttr_kind { Unknown, D_VirtAttr}; 
protected: 
  D_VirtAttr_kind kind; 

D_VirtAttr(); 
public: 
  virtual ~D_VirtAttr();
  D_VirtAttr(D_VirtAttr const& other);
  D_VirtAttr(D_VirtAttr::D_VirtAttr_kind kind);
  D_VirtAttr(D_VirtAttr&& other);
  
  D_VirtAttr_kind get_kind() const; 
}; 

extern std::ostream& operator<<(std::ostream& os, D_VirtAttr* arg); 
extern bool operator<(D_VirtAttrPtr lhs, D_VirtAttrPtr rhs);
extern bool operator==(D_VirtAttrPtr lhs, D_VirtAttrPtr rhs);

class sub_D_VirtAttr_D_VirtAttr : public D_VirtAttr { 
protected: 
  sub_D_VirtAttr_D_VirtAttr();
  StringTableKey arg_0;
public:
  sub_D_VirtAttr_D_VirtAttr(StringTableKey arg_0);
  sub_D_VirtAttr_D_VirtAttr(sub_D_VirtAttr_D_VirtAttr const& other); 
  sub_D_VirtAttr_D_VirtAttr(sub_D_VirtAttr_D_VirtAttr&& other);
  virtual ~sub_D_VirtAttr_D_VirtAttr(); 
  
  StringTableKey get_arg_0();
  void set_arg_0(StringTableKey arg);
}; 

extern std::ostream& operator<<(std::ostream& os, sub_D_VirtAttr_D_VirtAttr& arg);
extern bool operator==(sub_D_VirtAttr_D_VirtAttr& lhs, sub_D_VirtAttr_D_VirtAttr& rhs);
extern bool operator<(sub_D_VirtAttr_D_VirtAttr& lhs, sub_D_VirtAttr_D_VirtAttr& rhs);
extern D_VirtAttrPtr create_sub_D_VirtAttr_D_VirtAttr(StringTableKey arg_0);



// PTG Base class 
class PTG_BaseNode { 
public:
  enum class Kind { Unknown, String, Int, Seq, SeqComma, SeqNL, SeqAnd, BasePTGHead, FunPTGHead, InsertDecl, InsertCallarg, ImplBase, PrintKind, ImplPTGFunEmpty, StringPattern, OptionalPattern, RefArg, InsertionRefArg, CallPattern, InsertionPattern, InsertionPatternStringTableKey, InsertionPatternBinding, ImplPTGFunFull, DefaultInitStr, DefaultInitInt, DefaultInitPTG, DefaultInitBinding, DefaultInitStringTableKey, DefaultInitOther, ArgInit, CheckOptStr, CheckOptInt, CheckOptIntEq, CheckOptStrEq, CheckOptPTG, CheckOptPTGEq, CheckOptBinding, CheckOptBindingEq, CheckOptStringTableKey, CheckOptStringTableKeyEq, HeadPTG, ImplPTG, ArgumentPDLDecl, ArgumentPDLFunsInDecl, ArgumentPDLFunsOutDecl, IncludeStat, HeaderPDL, ImplPDLIn, ImplPDLOut, SourcePDL, ImplPDLClone, Ptr, GenList, GenTuple, GenMap, GenFun, MapName, ListName, FunName, VecOf, TypeDef, EClass, ComplexDecl, SimplePrintCase, SimplePrintComplex, SimplePrint, ComplexPrint, RecordPrint, SeqPComma, TupleOf, TupleName, MapType, FunArg, FunDecl, Header, BaseForwardPTG, SourceFile, AstFile, LidoFile, HeadFile, ArgRef, ArgKindRef, Equals, StringVal, EmptyListCheck, SingleListCheck, EnumAccess, EnumAccessKind, SeqAndSelect, CallTemplates, CastAccess, SeqWS, AccessArg, AccessTupleArg, ConstructType, EachBind, Each, DoCreateVar, Auto, AssignStat, Template, MapOf, CaptureLambda, LambdaExpr, DeclType, WrapReturn2, ReportCondNoPos, DoReturn, DoReturn1, DoReturn2, DoReturn3, DoReturn4, DoReturn5, DoReturn6, DoReturn7, DoReturn8, DoReturn9, DoReturn10, DoReturn11, DoReturn12, DoReturn13, DoReturn14, CondErrorStat, GuardExpr, UnknownIfCond, WrapSemi, IfStat, IfExpr, Apply, Empty, VarVec, InitVec, FunctionTypeDecl, ListIndex, PtrExpr, Not, Loc, Neg, Incr, StringTableOf, ConcatObj, ConcatInd, ConcatInt, ConcatBool, IdnOf, BinOp, ConcatExpr, ConcatInv, ListConcatExpr, MakeTuple, ErrorICE, Error, SeqOr, TupleAccess, RecordAccess, ListAccess, WrapBrackets, ListCon, RetPushback, ImplFun, TemplateClass, WrapTemplates, ArgDecl, TemplateFunctionSignature, FunctionSignature, MapTypeName, SimpleCond, SimpleCondWarn, SimpleImple, RetDecl, IterDecl, Iteration, ListInitRec, TypePtr, ListInitRecNoTail, RetAssign, ColonPre, DataBaseDecl, DataInhSimple, ArgDef, ArgName, ArgFuns, ArgFunsImplT, ConstructorName, DataInhComplex, DataRecordNormal, TemplateRecord, TemplateArgDef, TemplateArgName, TemplateDecl, RecordFuns, CommaPre, DataArgInit, DataArgInitIn, DefaultTypeInit, DefaultECAInit, PrefixOther, RecordArgPrint, TODO, ConvertComplex, ReturnFalse, ReturnTrue, ConargLess, ConargLessP, DataBaseImpl, DataInhSimpleI, DataInhComplexI, RecordImpl, DataArgMove, DataArgMoveIn, DataFunImpl, RecordFunImpl, PrintVariable, PrintList, GetPrintArg, GetPrintArgP, GetTupleNode, RecArg, PrintTuple, PrintFunctionSignature, AppendFirst, AppendSecond, PrintMapElem, PrintAddress, PrintVoid, PrintDefault, CreateTupleRef, TupleRef, GetArg, GetArgP, PrintSkey, PrintBinding, PrintBool, PrintKey, PrintEnv, SimpleEqCase, ComplexEqCase, SimpleLtCase, ComplexLtCase, ConstantDecl, ConstantImpl, TreeFun, Inh, Synt, InhName, SyntName, AttrDef, AttrDefG, SeqCommaNL, SeqSemiNL, CoordInfo, Tail, Head, Attribute, Including, Includings, ConstituentDep, ConstituentDeps, LocalAttr, Assign, If, Or, LidoLambda, ErrorReportLido, CondLido, WhenLido, AddExpr, AddLido, SubExpr, SubLido, DivExpr, DivLido, MulExpr, MulLido, ModExpr, ModLido, EqExpr, EqLido, NeExpr, NeLido, GtExpr, GtLido, GeExpr, GeLido, LtExpr, LtLido, LeExpr, LeLido, OrExpr, OrLido, AndExpr, AndLido, NegLido, NegExpr, NotLido, NotExpr, LocLido, LocExpr, RefLido, RefExpr, ListIndexLido, ListAccessLido, TupleAccessLido, RecordAccessLido, ListConLido, ListConP, MakeTupleLido, CallLido, CallLidoEmpty, Call, CallEmpty, EnumAccessLido, RuleCall, EmptyListCall, SeqDot, Constituents, Constituent, ConstituentOpt, ConstituentsOpt, Unshield, ShieldOne, Shield, WithDef, ExprWithDeps, ChainStart, Order, AssignStatLido, EmptyDef, SingleDef, AppendDef, SymbolAttrDecls, SymbolInheritStat, SymbolComputations, SymbolInheritCompute, ChainStartAttr, SymbolIndex, RuleAttr, GAttrDef, GChain, DefineLNG, DefaultVattrRuleLhs, TreeComps, RuleComputations, VirtAttrDecls, VirtAttrDeclsClass, DefaultVAttr, VirtAttrCompute, TermDecl, IsTerm, TermSymb, TermSymbConv, GenSymbol, RuleDecl, AbstreeInfo}; 
protected: 
  Kind kind;

public:
  PTG_BaseNode(); 
  PTG_BaseNode(Kind kind);
  virtual ~PTG_BaseNode();
  
  virtual void set_kind(Kind arg);
  virtual const Kind& get_kind() const; 
  virtual bool print_optional() const;
  friend std::ostream& operator<<(std::ostream& os, PTG_BaseNode* node);
};

//typedef std::shared_ptr<PTG_BaseNode> PTGNode; 
extern std::ostream& operator<<(std::ostream& os, PTGNode node);
extern PTGNode PTGOut(PTGNode node);
extern PTGNode PTGOutFile(std::string file, PTGNode node);
extern PTGNode PTGNull();
#define PTGNULL nullptr
extern std::ostream& IndentNewLine(std::ostream& os);
extern std::ostream& IndentIncr(std::ostream& os);
extern std::ostream& IndentDecr(std::ostream& os);
extern std::ostream& INL(std::ostream& os);
extern std::ostream& II(std::ostream& os);
extern std::ostream& ID(std::ostream& os);
extern void IndentSetStep(unsigned int n);


// PTG constructors 
class PTG_StringNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_StringNode();
  PTG_StringNode(tStringType arg1);
  virtual ~PTG_StringNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_StringNode& node);
};

extern PTGNode PTGString(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_StringNode& node);


class PTG_IntNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_IntNode();
  PTG_IntNode(int arg1);
  virtual ~PTG_IntNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IntNode& node);
};

extern PTGNode PTGInt(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_IntNode& node);


class PTG_SeqNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqNode();
  PTG_SeqNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqNode& node);
};

extern PTGNode PTGSeq(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqNode& node);


class PTG_SeqCommaNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqCommaNode();
  PTG_SeqCommaNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqCommaNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqCommaNode& node);
};

extern PTGNode PTGSeqComma(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqCommaNode& node);


class PTG_SeqNLNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqNLNode();
  PTG_SeqNLNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqNLNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqNLNode& node);
};

extern PTGNode PTGSeqNL(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqNLNode& node);


class PTG_SeqAndNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqAndNode();
  PTG_SeqAndNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqAndNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqAndNode& node);
};

extern PTGNode PTGSeqAnd(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqAndNode& node);


class PTG_BasePTGHeadNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_BasePTGHeadNode();
  PTG_BasePTGHeadNode(PTGNode arg1);
  virtual ~PTG_BasePTGHeadNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_BasePTGHeadNode& node);
};

extern PTGNode PTGBasePTGHead(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_BasePTGHeadNode& node);


class PTG_FunPTGHeadNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_FunPTGHeadNode();
  PTG_FunPTGHeadNode(tStringType arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_FunPTGHeadNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_FunPTGHeadNode& node);
};

extern PTGNode PTGFunPTGHead(tStringType arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_FunPTGHeadNode& node);


class PTG_InsertDeclNode : public PTG_BaseNode { 
protected: 
  int arg1;
  tStringType arg2;
public: 
  PTG_InsertDeclNode();
  PTG_InsertDeclNode(int arg1, tStringType arg2);
  virtual ~PTG_InsertDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_InsertDeclNode& node);
};

extern PTGNode PTGInsertDecl(int arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_InsertDeclNode& node);


class PTG_InsertCallargNode : public PTG_BaseNode { 
protected: 
  int arg1;
  tStringType arg2;
public: 
  PTG_InsertCallargNode();
  PTG_InsertCallargNode(int arg1, tStringType arg2);
  virtual ~PTG_InsertCallargNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_InsertCallargNode& node);
};

extern PTGNode PTGInsertCallarg(int arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_InsertCallargNode& node);


class PTG_ImplBaseNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ImplBaseNode();
  PTG_ImplBaseNode(PTGNode arg1);
  virtual ~PTG_ImplBaseNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ImplBaseNode& node);
};

extern PTGNode PTGImplBase(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ImplBaseNode& node);


class PTG_PrintKindNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_PrintKindNode();
  PTG_PrintKindNode(tStringType arg1);
  virtual ~PTG_PrintKindNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintKindNode& node);
};

extern PTGNode PTGPrintKind(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintKindNode& node);


class PTG_ImplPTGFunEmptyNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_ImplPTGFunEmptyNode();
  PTG_ImplPTGFunEmptyNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_ImplPTGFunEmptyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ImplPTGFunEmptyNode& node);
};

extern PTGNode PTGImplPTGFunEmpty(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ImplPTGFunEmptyNode& node);


class PTG_StringPatternNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_StringPatternNode();
  PTG_StringPatternNode(tStringType arg1);
  virtual ~PTG_StringPatternNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_StringPatternNode& node);
};

extern PTGNode PTGStringPattern(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_StringPatternNode& node);


class PTG_OptionalPatternNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_OptionalPatternNode();
  PTG_OptionalPatternNode(PTGNode arg1);
  virtual ~PTG_OptionalPatternNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_OptionalPatternNode& node);
};

extern PTGNode PTGOptionalPattern(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_OptionalPatternNode& node);


class PTG_RefArgNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_RefArgNode();
  PTG_RefArgNode(int arg1);
  virtual ~PTG_RefArgNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RefArgNode& node);
};

extern PTGNode PTGRefArg(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_RefArgNode& node);


class PTG_InsertionRefArgNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_InsertionRefArgNode();
  PTG_InsertionRefArgNode(int arg1);
  virtual ~PTG_InsertionRefArgNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_InsertionRefArgNode& node);
};

extern PTGNode PTGInsertionRefArg(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_InsertionRefArgNode& node);


class PTG_CallPatternNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_CallPatternNode();
  PTG_CallPatternNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_CallPatternNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CallPatternNode& node);
};

extern PTGNode PTGCallPattern(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_CallPatternNode& node);


class PTG_InsertionPatternNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_InsertionPatternNode();
  PTG_InsertionPatternNode(int arg1);
  virtual ~PTG_InsertionPatternNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_InsertionPatternNode& node);
};

extern PTGNode PTGInsertionPattern(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_InsertionPatternNode& node);


class PTG_InsertionPatternStringTableKeyNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_InsertionPatternStringTableKeyNode();
  PTG_InsertionPatternStringTableKeyNode(int arg1);
  virtual ~PTG_InsertionPatternStringTableKeyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_InsertionPatternStringTableKeyNode& node);
};

extern PTGNode PTGInsertionPatternStringTableKey(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_InsertionPatternStringTableKeyNode& node);


class PTG_InsertionPatternBindingNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_InsertionPatternBindingNode();
  PTG_InsertionPatternBindingNode(int arg1);
  virtual ~PTG_InsertionPatternBindingNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_InsertionPatternBindingNode& node);
};

extern PTGNode PTGInsertionPatternBinding(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_InsertionPatternBindingNode& node);


class PTG_ImplPTGFunFullNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
  PTGNode arg5;
  PTGNode arg6;
  PTGNode arg7;
  PTGNode arg8;
public: 
  PTG_ImplPTGFunFullNode();
  PTG_ImplPTGFunFullNode(tStringType arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8);
  virtual ~PTG_ImplPTGFunFullNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ImplPTGFunFullNode& node);
};

extern PTGNode PTGImplPTGFunFull(tStringType arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8); 
extern std::ostream& operator<<(std::ostream& os, PTG_ImplPTGFunFullNode& node);


class PTG_DefaultInitStrNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_DefaultInitStrNode();
  PTG_DefaultInitStrNode(int arg1);
  virtual ~PTG_DefaultInitStrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultInitStrNode& node);
};

extern PTGNode PTGDefaultInitStr(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultInitStrNode& node);


class PTG_DefaultInitIntNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_DefaultInitIntNode();
  PTG_DefaultInitIntNode(int arg1);
  virtual ~PTG_DefaultInitIntNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultInitIntNode& node);
};

extern PTGNode PTGDefaultInitInt(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultInitIntNode& node);


class PTG_DefaultInitPTGNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_DefaultInitPTGNode();
  PTG_DefaultInitPTGNode(int arg1);
  virtual ~PTG_DefaultInitPTGNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultInitPTGNode& node);
};

extern PTGNode PTGDefaultInitPTG(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultInitPTGNode& node);


class PTG_DefaultInitBindingNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_DefaultInitBindingNode();
  PTG_DefaultInitBindingNode(int arg1);
  virtual ~PTG_DefaultInitBindingNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultInitBindingNode& node);
};

extern PTGNode PTGDefaultInitBinding(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultInitBindingNode& node);


class PTG_DefaultInitStringTableKeyNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_DefaultInitStringTableKeyNode();
  PTG_DefaultInitStringTableKeyNode(int arg1);
  virtual ~PTG_DefaultInitStringTableKeyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultInitStringTableKeyNode& node);
};

extern PTGNode PTGDefaultInitStringTableKey(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultInitStringTableKeyNode& node);


class PTG_DefaultInitOtherNode : public PTG_BaseNode { 
protected: 
  int arg1;
  tStringType arg2;
public: 
  PTG_DefaultInitOtherNode();
  PTG_DefaultInitOtherNode(int arg1, tStringType arg2);
  virtual ~PTG_DefaultInitOtherNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultInitOtherNode& node);
};

extern PTGNode PTGDefaultInitOther(int arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultInitOtherNode& node);


class PTG_ArgInitNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_ArgInitNode();
  PTG_ArgInitNode(int arg1);
  virtual ~PTG_ArgInitNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgInitNode& node);
};

extern PTGNode PTGArgInit(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgInitNode& node);


class PTG_CheckOptStrNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptStrNode();
  PTG_CheckOptStrNode(int arg1);
  virtual ~PTG_CheckOptStrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptStrNode& node);
};

extern PTGNode PTGCheckOptStr(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptStrNode& node);


class PTG_CheckOptIntNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptIntNode();
  PTG_CheckOptIntNode(int arg1);
  virtual ~PTG_CheckOptIntNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptIntNode& node);
};

extern PTGNode PTGCheckOptInt(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptIntNode& node);


class PTG_CheckOptIntEqNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptIntEqNode();
  PTG_CheckOptIntEqNode(int arg1);
  virtual ~PTG_CheckOptIntEqNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptIntEqNode& node);
};

extern PTGNode PTGCheckOptIntEq(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptIntEqNode& node);


class PTG_CheckOptStrEqNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptStrEqNode();
  PTG_CheckOptStrEqNode(int arg1);
  virtual ~PTG_CheckOptStrEqNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptStrEqNode& node);
};

extern PTGNode PTGCheckOptStrEq(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptStrEqNode& node);


class PTG_CheckOptPTGNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptPTGNode();
  PTG_CheckOptPTGNode(int arg1);
  virtual ~PTG_CheckOptPTGNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptPTGNode& node);
};

extern PTGNode PTGCheckOptPTG(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptPTGNode& node);


class PTG_CheckOptPTGEqNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptPTGEqNode();
  PTG_CheckOptPTGEqNode(int arg1);
  virtual ~PTG_CheckOptPTGEqNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptPTGEqNode& node);
};

extern PTGNode PTGCheckOptPTGEq(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptPTGEqNode& node);


class PTG_CheckOptBindingNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptBindingNode();
  PTG_CheckOptBindingNode(int arg1);
  virtual ~PTG_CheckOptBindingNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptBindingNode& node);
};

extern PTGNode PTGCheckOptBinding(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptBindingNode& node);


class PTG_CheckOptBindingEqNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptBindingEqNode();
  PTG_CheckOptBindingEqNode(int arg1);
  virtual ~PTG_CheckOptBindingEqNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptBindingEqNode& node);
};

extern PTGNode PTGCheckOptBindingEq(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptBindingEqNode& node);


class PTG_CheckOptStringTableKeyNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptStringTableKeyNode();
  PTG_CheckOptStringTableKeyNode(int arg1);
  virtual ~PTG_CheckOptStringTableKeyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptStringTableKeyNode& node);
};

extern PTGNode PTGCheckOptStringTableKey(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptStringTableKeyNode& node);


class PTG_CheckOptStringTableKeyEqNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_CheckOptStringTableKeyEqNode();
  PTG_CheckOptStringTableKeyEqNode(int arg1);
  virtual ~PTG_CheckOptStringTableKeyEqNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CheckOptStringTableKeyEqNode& node);
};

extern PTGNode PTGCheckOptStringTableKeyEq(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CheckOptStringTableKeyEqNode& node);


class PTG_HeadPTGNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_HeadPTGNode();
  PTG_HeadPTGNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_HeadPTGNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_HeadPTGNode& node);
};

extern PTGNode PTGHeadPTG(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_HeadPTGNode& node);


class PTG_ImplPTGNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ImplPTGNode();
  PTG_ImplPTGNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ImplPTGNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ImplPTGNode& node);
};

extern PTGNode PTGImplPTG(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ImplPTGNode& node);


class PTG_ArgumentPDLDeclNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  int arg2;
public: 
  PTG_ArgumentPDLDeclNode();
  PTG_ArgumentPDLDeclNode(PTGNode arg1, int arg2);
  virtual ~PTG_ArgumentPDLDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgumentPDLDeclNode& node);
};

extern PTGNode PTGArgumentPDLDecl(PTGNode arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgumentPDLDeclNode& node);


class PTG_ArgumentPDLFunsInDeclNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  int arg2;
public: 
  PTG_ArgumentPDLFunsInDeclNode();
  PTG_ArgumentPDLFunsInDeclNode(PTGNode arg1, int arg2);
  virtual ~PTG_ArgumentPDLFunsInDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgumentPDLFunsInDeclNode& node);
};

extern PTGNode PTGArgumentPDLFunsInDecl(PTGNode arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgumentPDLFunsInDeclNode& node);


class PTG_ArgumentPDLFunsOutDeclNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_ArgumentPDLFunsOutDeclNode();
  PTG_ArgumentPDLFunsOutDeclNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_ArgumentPDLFunsOutDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgumentPDLFunsOutDeclNode& node);
};

extern PTGNode PTGArgumentPDLFunsOutDecl(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgumentPDLFunsOutDeclNode& node);


class PTG_IncludeStatNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_IncludeStatNode();
  PTG_IncludeStatNode(tStringType arg1);
  virtual ~PTG_IncludeStatNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IncludeStatNode& node);
};

extern PTGNode PTGIncludeStat(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_IncludeStatNode& node);


class PTG_HeaderPDLNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_HeaderPDLNode();
  PTG_HeaderPDLNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_HeaderPDLNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_HeaderPDLNode& node);
};

extern PTGNode PTGHeaderPDL(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_HeaderPDLNode& node);


class PTG_ImplPDLInNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  int arg2;
public: 
  PTG_ImplPDLInNode();
  PTG_ImplPDLInNode(PTGNode arg1, int arg2);
  virtual ~PTG_ImplPDLInNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ImplPDLInNode& node);
};

extern PTGNode PTGImplPDLIn(PTGNode arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ImplPDLInNode& node);


class PTG_ImplPDLOutNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  int arg3;
public: 
  PTG_ImplPDLOutNode();
  PTG_ImplPDLOutNode(tStringType arg1, PTGNode arg2, int arg3);
  virtual ~PTG_ImplPDLOutNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ImplPDLOutNode& node);
};

extern PTGNode PTGImplPDLOut(tStringType arg1, PTGNode arg2, int arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_ImplPDLOutNode& node);


class PTG_SourcePDLNode : public PTG_BaseNode { 
protected: 
  int arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
public: 
  PTG_SourcePDLNode();
  PTG_SourcePDLNode(int arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4);
  virtual ~PTG_SourcePDLNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SourcePDLNode& node);
};

extern PTGNode PTGSourcePDL(int arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_SourcePDLNode& node);


class PTG_ImplPDLCloneNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  int arg2;
public: 
  PTG_ImplPDLCloneNode();
  PTG_ImplPDLCloneNode(tStringType arg1, int arg2);
  virtual ~PTG_ImplPDLCloneNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ImplPDLCloneNode& node);
};

extern PTGNode PTGImplPDLClone(tStringType arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ImplPDLCloneNode& node);


class PTG_PtrNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_PtrNode();
  PTG_PtrNode(tStringType arg1);
  virtual ~PTG_PtrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PtrNode& node);
};

extern PTGNode PTGPtr(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PtrNode& node);


class PTG_GenListNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  int arg2;
public: 
  PTG_GenListNode();
  PTG_GenListNode(PTGNode arg1, int arg2);
  virtual ~PTG_GenListNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GenListNode& node);
};

extern PTGNode PTGGenList(PTGNode arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GenListNode& node);


class PTG_GenTupleNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  int arg2;
public: 
  PTG_GenTupleNode();
  PTG_GenTupleNode(PTGNode arg1, int arg2);
  virtual ~PTG_GenTupleNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GenTupleNode& node);
};

extern PTGNode PTGGenTuple(PTGNode arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GenTupleNode& node);


class PTG_GenMapNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  int arg2;
public: 
  PTG_GenMapNode();
  PTG_GenMapNode(PTGNode arg1, int arg2);
  virtual ~PTG_GenMapNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GenMapNode& node);
};

extern PTGNode PTGGenMap(PTGNode arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GenMapNode& node);


class PTG_GenFunNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  int arg2;
public: 
  PTG_GenFunNode();
  PTG_GenFunNode(tStringType arg1, int arg2);
  virtual ~PTG_GenFunNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GenFunNode& node);
};

extern PTGNode PTGGenFun(tStringType arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GenFunNode& node);


class PTG_MapNameNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_MapNameNode();
  PTG_MapNameNode(int arg1);
  virtual ~PTG_MapNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_MapNameNode& node);
};

extern PTGNode PTGMapName(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_MapNameNode& node);


class PTG_ListNameNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_ListNameNode();
  PTG_ListNameNode(int arg1);
  virtual ~PTG_ListNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListNameNode& node);
};

extern PTGNode PTGListName(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListNameNode& node);


class PTG_FunNameNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_FunNameNode();
  PTG_FunNameNode(int arg1);
  virtual ~PTG_FunNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_FunNameNode& node);
};

extern PTGNode PTGFunName(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_FunNameNode& node);


class PTG_VecOfNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_VecOfNode();
  PTG_VecOfNode(PTGNode arg1);
  virtual ~PTG_VecOfNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_VecOfNode& node);
};

extern PTGNode PTGVecOf(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_VecOfNode& node);


class PTG_TypeDefNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_TypeDefNode();
  PTG_TypeDefNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_TypeDefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TypeDefNode& node);
};

extern PTGNode PTGTypeDef(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_TypeDefNode& node);


class PTG_EClassNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_EClassNode();
  PTG_EClassNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_EClassNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EClassNode& node);
};

extern PTGNode PTGEClass(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_EClassNode& node);


class PTG_ComplexDeclNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_ComplexDeclNode();
  PTG_ComplexDeclNode(tStringType arg1);
  virtual ~PTG_ComplexDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ComplexDeclNode& node);
};

extern PTGNode PTGComplexDecl(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ComplexDeclNode& node);


class PTG_SimplePrintCaseNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_SimplePrintCaseNode();
  PTG_SimplePrintCaseNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_SimplePrintCaseNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SimplePrintCaseNode& node);
};

extern PTGNode PTGSimplePrintCase(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SimplePrintCaseNode& node);


class PTG_SimplePrintComplexNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_SimplePrintComplexNode();
  PTG_SimplePrintComplexNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_SimplePrintComplexNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SimplePrintComplexNode& node);
};

extern PTGNode PTGSimplePrintComplex(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SimplePrintComplexNode& node);


class PTG_SimplePrintNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_SimplePrintNode();
  PTG_SimplePrintNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_SimplePrintNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SimplePrintNode& node);
};

extern PTGNode PTGSimplePrint(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SimplePrintNode& node);


class PTG_ComplexPrintNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_ComplexPrintNode();
  PTG_ComplexPrintNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_ComplexPrintNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ComplexPrintNode& node);
};

extern PTGNode PTGComplexPrint(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ComplexPrintNode& node);


class PTG_RecordPrintNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_RecordPrintNode();
  PTG_RecordPrintNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_RecordPrintNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RecordPrintNode& node);
};

extern PTGNode PTGRecordPrint(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_RecordPrintNode& node);


class PTG_SeqPCommaNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqPCommaNode();
  PTG_SeqPCommaNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqPCommaNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqPCommaNode& node);
};

extern PTGNode PTGSeqPComma(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqPCommaNode& node);


class PTG_TupleOfNode : public PTG_BaseNode { 
protected: 
  PTGNode arg2;
public: 
  PTG_TupleOfNode();
  PTG_TupleOfNode(PTGNode arg2);
  virtual ~PTG_TupleOfNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TupleOfNode& node);
};

extern PTGNode PTGTupleOf(PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_TupleOfNode& node);


class PTG_TupleNameNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_TupleNameNode();
  PTG_TupleNameNode(int arg1);
  virtual ~PTG_TupleNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TupleNameNode& node);
};

extern PTGNode PTGTupleName(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_TupleNameNode& node);


class PTG_MapTypeNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_MapTypeNode();
  PTG_MapTypeNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_MapTypeNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_MapTypeNode& node);
};

extern PTGNode PTGMapType(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_MapTypeNode& node);


class PTG_FunArgNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  int arg2;
public: 
  PTG_FunArgNode();
  PTG_FunArgNode(PTGNode arg1, int arg2);
  virtual ~PTG_FunArgNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_FunArgNode& node);
};

extern PTGNode PTGFunArg(PTGNode arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_FunArgNode& node);


class PTG_FunDeclNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_FunDeclNode();
  PTG_FunDeclNode(PTGNode arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_FunDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_FunDeclNode& node);
};

extern PTGNode PTGFunDecl(PTGNode arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_FunDeclNode& node);


class PTG_HeaderNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
  PTGNode arg5;
  PTGNode arg6;
  PTGNode arg7;
  PTGNode arg8;
  PTGNode arg9;
  PTGNode arg10;
  PTGNode arg11;
  PTGNode arg12;
  PTGNode arg13;
  PTGNode arg14;
public: 
  PTG_HeaderNode();
  PTG_HeaderNode(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9, PTGNode arg10, PTGNode arg11, PTGNode arg12, PTGNode arg13, PTGNode arg14);
  virtual ~PTG_HeaderNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_HeaderNode& node);
};

extern PTGNode PTGHeader(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9, PTGNode arg10, PTGNode arg11, PTGNode arg12, PTGNode arg13, PTGNode arg14); 
extern std::ostream& operator<<(std::ostream& os, PTG_HeaderNode& node);


class PTG_BaseForwardPTGNode : public PTG_BaseNode { 
public: 
  PTG_BaseForwardPTGNode();
  virtual ~PTG_BaseForwardPTGNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_BaseForwardPTGNode& node);
};

extern PTGNode PTGBaseForwardPTG(); 
extern std::ostream& operator<<(std::ostream& os, PTG_BaseForwardPTGNode& node);


class PTG_SourceFileNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
  PTGNode arg5;
  PTGNode arg6;
  PTGNode arg7;
  PTGNode arg8;
  PTGNode arg9;
public: 
  PTG_SourceFileNode();
  PTG_SourceFileNode(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9);
  virtual ~PTG_SourceFileNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SourceFileNode& node);
};

extern PTGNode PTGSourceFile(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9); 
extern std::ostream& operator<<(std::ostream& os, PTG_SourceFileNode& node);


class PTG_AstFileNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_AstFileNode();
  PTG_AstFileNode(PTGNode arg1);
  virtual ~PTG_AstFileNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AstFileNode& node);
};

extern PTGNode PTGAstFile(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_AstFileNode& node);


class PTG_LidoFileNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_LidoFileNode();
  PTG_LidoFileNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_LidoFileNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LidoFileNode& node);
};

extern PTGNode PTGLidoFile(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_LidoFileNode& node);


class PTG_HeadFileNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_HeadFileNode();
  PTG_HeadFileNode(tStringType arg1);
  virtual ~PTG_HeadFileNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_HeadFileNode& node);
};

extern PTGNode PTGHeadFile(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_HeadFileNode& node);


class PTG_ArgRefNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_ArgRefNode();
  PTG_ArgRefNode(int arg1);
  virtual ~PTG_ArgRefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgRefNode& node);
};

extern PTGNode PTGArgRef(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgRefNode& node);


class PTG_ArgKindRefNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_ArgKindRefNode();
  PTG_ArgKindRefNode(int arg1);
  virtual ~PTG_ArgKindRefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgKindRefNode& node);
};

extern PTGNode PTGArgKindRef(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgKindRefNode& node);


class PTG_EqualsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_EqualsNode();
  PTG_EqualsNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_EqualsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EqualsNode& node);
};

extern PTGNode PTGEquals(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_EqualsNode& node);


class PTG_StringValNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_StringValNode();
  PTG_StringValNode(tStringType arg1);
  virtual ~PTG_StringValNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_StringValNode& node);
};

extern PTGNode PTGStringVal(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_StringValNode& node);


class PTG_EmptyListCheckNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_EmptyListCheckNode();
  PTG_EmptyListCheckNode(int arg1);
  virtual ~PTG_EmptyListCheckNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EmptyListCheckNode& node);
};

extern PTGNode PTGEmptyListCheck(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_EmptyListCheckNode& node);


class PTG_SingleListCheckNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_SingleListCheckNode();
  PTG_SingleListCheckNode(int arg1);
  virtual ~PTG_SingleListCheckNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SingleListCheckNode& node);
};

extern PTGNode PTGSingleListCheck(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_SingleListCheckNode& node);


class PTG_EnumAccessNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_EnumAccessNode();
  PTG_EnumAccessNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_EnumAccessNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EnumAccessNode& node);
};

extern PTGNode PTGEnumAccess(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_EnumAccessNode& node);


class PTG_EnumAccessKindNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_EnumAccessKindNode();
  PTG_EnumAccessKindNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_EnumAccessKindNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EnumAccessKindNode& node);
};

extern PTGNode PTGEnumAccessKind(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_EnumAccessKindNode& node);


class PTG_SeqAndSelectNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqAndSelectNode();
  PTG_SeqAndSelectNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqAndSelectNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqAndSelectNode& node);
};

extern PTGNode PTGSeqAndSelect(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqAndSelectNode& node);


class PTG_CallTemplatesNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_CallTemplatesNode();
  PTG_CallTemplatesNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_CallTemplatesNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CallTemplatesNode& node);
};

extern PTGNode PTGCallTemplates(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_CallTemplatesNode& node);


class PTG_CastAccessNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  int arg3;
  int arg4;
public: 
  PTG_CastAccessNode();
  PTG_CastAccessNode(PTGNode arg1, int arg3, int arg4);
  virtual ~PTG_CastAccessNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CastAccessNode& node);
};

extern PTGNode PTGCastAccess(PTGNode arg1, int arg3, int arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_CastAccessNode& node);


class PTG_SeqWSNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqWSNode();
  PTG_SeqWSNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqWSNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqWSNode& node);
};

extern PTGNode PTGSeqWS(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqWSNode& node);


class PTG_AccessArgNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_AccessArgNode();
  PTG_AccessArgNode(int arg1);
  virtual ~PTG_AccessArgNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AccessArgNode& node);
};

extern PTGNode PTGAccessArg(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_AccessArgNode& node);


class PTG_AccessTupleArgNode : public PTG_BaseNode { 
protected: 
  int arg1;
  int arg2;
public: 
  PTG_AccessTupleArgNode();
  PTG_AccessTupleArgNode(int arg1, int arg2);
  virtual ~PTG_AccessTupleArgNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AccessTupleArgNode& node);
};

extern PTGNode PTGAccessTupleArg(int arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_AccessTupleArgNode& node);


class PTG_ConstructTypeNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ConstructTypeNode();
  PTG_ConstructTypeNode(PTGNode arg1);
  virtual ~PTG_ConstructTypeNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstructTypeNode& node);
};

extern PTGNode PTGConstructType(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstructTypeNode& node);


class PTG_EachBindNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_EachBindNode();
  PTG_EachBindNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_EachBindNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EachBindNode& node);
};

extern PTGNode PTGEachBind(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_EachBindNode& node);


class PTG_EachNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_EachNode();
  PTG_EachNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_EachNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EachNode& node);
};

extern PTGNode PTGEach(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_EachNode& node);


class PTG_DoCreateVarNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_DoCreateVarNode();
  PTG_DoCreateVarNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_DoCreateVarNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoCreateVarNode& node);
};

extern PTGNode PTGDoCreateVar(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoCreateVarNode& node);


class PTG_AutoNode : public PTG_BaseNode { 
public: 
  PTG_AutoNode();
  virtual ~PTG_AutoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AutoNode& node);
};

extern PTGNode PTGAuto(); 
extern std::ostream& operator<<(std::ostream& os, PTG_AutoNode& node);


class PTG_AssignStatNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_AssignStatNode();
  PTG_AssignStatNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_AssignStatNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AssignStatNode& node);
};

extern PTGNode PTGAssignStat(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_AssignStatNode& node);


class PTG_TemplateNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_TemplateNode();
  PTG_TemplateNode(tStringType arg1);
  virtual ~PTG_TemplateNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TemplateNode& node);
};

extern PTGNode PTGTemplate(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_TemplateNode& node);


class PTG_MapOfNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_MapOfNode();
  PTG_MapOfNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_MapOfNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_MapOfNode& node);
};

extern PTGNode PTGMapOf(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_MapOfNode& node);


class PTG_CaptureLambdaNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_CaptureLambdaNode();
  PTG_CaptureLambdaNode(tStringType arg1);
  virtual ~PTG_CaptureLambdaNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CaptureLambdaNode& node);
};

extern PTGNode PTGCaptureLambda(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CaptureLambdaNode& node);


class PTG_LambdaExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
public: 
  PTG_LambdaExprNode();
  PTG_LambdaExprNode(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4);
  virtual ~PTG_LambdaExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LambdaExprNode& node);
};

extern PTGNode PTGLambdaExpr(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_LambdaExprNode& node);


class PTG_DeclTypeNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DeclTypeNode();
  PTG_DeclTypeNode(PTGNode arg1);
  virtual ~PTG_DeclTypeNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DeclTypeNode& node);
};

extern PTGNode PTGDeclType(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DeclTypeNode& node);


class PTG_WrapReturn2Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_WrapReturn2Node();
  PTG_WrapReturn2Node(PTGNode arg1);
  virtual ~PTG_WrapReturn2Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_WrapReturn2Node& node);
};

extern PTGNode PTGWrapReturn2(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_WrapReturn2Node& node);


class PTG_ReportCondNoPosNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ReportCondNoPosNode();
  PTG_ReportCondNoPosNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ReportCondNoPosNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ReportCondNoPosNode& node);
};

extern PTGNode PTGReportCondNoPos(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ReportCondNoPosNode& node);


class PTG_DoReturnNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturnNode();
  PTG_DoReturnNode(PTGNode arg1);
  virtual ~PTG_DoReturnNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturnNode& node);
};

extern PTGNode PTGDoReturn(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturnNode& node);


class PTG_DoReturn1Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn1Node();
  PTG_DoReturn1Node(PTGNode arg1);
  virtual ~PTG_DoReturn1Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn1Node& node);
};

extern PTGNode PTGDoReturn1(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn1Node& node);


class PTG_DoReturn2Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn2Node();
  PTG_DoReturn2Node(PTGNode arg1);
  virtual ~PTG_DoReturn2Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn2Node& node);
};

extern PTGNode PTGDoReturn2(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn2Node& node);


class PTG_DoReturn3Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn3Node();
  PTG_DoReturn3Node(PTGNode arg1);
  virtual ~PTG_DoReturn3Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn3Node& node);
};

extern PTGNode PTGDoReturn3(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn3Node& node);


class PTG_DoReturn4Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn4Node();
  PTG_DoReturn4Node(PTGNode arg1);
  virtual ~PTG_DoReturn4Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn4Node& node);
};

extern PTGNode PTGDoReturn4(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn4Node& node);


class PTG_DoReturn5Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn5Node();
  PTG_DoReturn5Node(PTGNode arg1);
  virtual ~PTG_DoReturn5Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn5Node& node);
};

extern PTGNode PTGDoReturn5(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn5Node& node);


class PTG_DoReturn6Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn6Node();
  PTG_DoReturn6Node(PTGNode arg1);
  virtual ~PTG_DoReturn6Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn6Node& node);
};

extern PTGNode PTGDoReturn6(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn6Node& node);


class PTG_DoReturn7Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn7Node();
  PTG_DoReturn7Node(PTGNode arg1);
  virtual ~PTG_DoReturn7Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn7Node& node);
};

extern PTGNode PTGDoReturn7(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn7Node& node);


class PTG_DoReturn8Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn8Node();
  PTG_DoReturn8Node(PTGNode arg1);
  virtual ~PTG_DoReturn8Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn8Node& node);
};

extern PTGNode PTGDoReturn8(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn8Node& node);


class PTG_DoReturn9Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn9Node();
  PTG_DoReturn9Node(PTGNode arg1);
  virtual ~PTG_DoReturn9Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn9Node& node);
};

extern PTGNode PTGDoReturn9(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn9Node& node);


class PTG_DoReturn10Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn10Node();
  PTG_DoReturn10Node(PTGNode arg1);
  virtual ~PTG_DoReturn10Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn10Node& node);
};

extern PTGNode PTGDoReturn10(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn10Node& node);


class PTG_DoReturn11Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn11Node();
  PTG_DoReturn11Node(PTGNode arg1);
  virtual ~PTG_DoReturn11Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn11Node& node);
};

extern PTGNode PTGDoReturn11(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn11Node& node);


class PTG_DoReturn12Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn12Node();
  PTG_DoReturn12Node(PTGNode arg1);
  virtual ~PTG_DoReturn12Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn12Node& node);
};

extern PTGNode PTGDoReturn12(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn12Node& node);


class PTG_DoReturn13Node : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DoReturn13Node();
  PTG_DoReturn13Node(PTGNode arg1);
  virtual ~PTG_DoReturn13Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn13Node& node);
};

extern PTGNode PTGDoReturn13(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn13Node& node);


class PTG_DoReturn14Node : public PTG_BaseNode { 
public: 
  PTG_DoReturn14Node();
  virtual ~PTG_DoReturn14Node();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DoReturn14Node& node);
};

extern PTGNode PTGDoReturn14(); 
extern std::ostream& operator<<(std::ostream& os, PTG_DoReturn14Node& node);


class PTG_CondErrorStatNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_CondErrorStatNode();
  PTG_CondErrorStatNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_CondErrorStatNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CondErrorStatNode& node);
};

extern PTGNode PTGCondErrorStat(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_CondErrorStatNode& node);


class PTG_GuardExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_GuardExprNode();
  PTG_GuardExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_GuardExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GuardExprNode& node);
};

extern PTGNode PTGGuardExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GuardExprNode& node);


class PTG_UnknownIfCondNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
public: 
  PTG_UnknownIfCondNode();
  PTG_UnknownIfCondNode(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4);
  virtual ~PTG_UnknownIfCondNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_UnknownIfCondNode& node);
};

extern PTGNode PTGUnknownIfCond(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_UnknownIfCondNode& node);


class PTG_WrapSemiNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_WrapSemiNode();
  PTG_WrapSemiNode(PTGNode arg1);
  virtual ~PTG_WrapSemiNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_WrapSemiNode& node);
};

extern PTGNode PTGWrapSemi(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_WrapSemiNode& node);


class PTG_IfStatNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_IfStatNode();
  PTG_IfStatNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_IfStatNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IfStatNode& node);
};

extern PTGNode PTGIfStat(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_IfStatNode& node);


class PTG_IfExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_IfExprNode();
  PTG_IfExprNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_IfExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IfExprNode& node);
};

extern PTGNode PTGIfExpr(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_IfExprNode& node);


class PTG_ApplyNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ApplyNode();
  PTG_ApplyNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ApplyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ApplyNode& node);
};

extern PTGNode PTGApply(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ApplyNode& node);


class PTG_EmptyNode : public PTG_BaseNode { 
public: 
  PTG_EmptyNode();
  virtual ~PTG_EmptyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EmptyNode& node);
};

extern PTGNode PTGEmpty(); 
extern std::ostream& operator<<(std::ostream& os, PTG_EmptyNode& node);


class PTG_VarVecNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_VarVecNode();
  PTG_VarVecNode(PTGNode arg1);
  virtual ~PTG_VarVecNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_VarVecNode& node);
};

extern PTGNode PTGVarVec(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_VarVecNode& node);


class PTG_InitVecNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_InitVecNode();
  PTG_InitVecNode(PTGNode arg1);
  virtual ~PTG_InitVecNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_InitVecNode& node);
};

extern PTGNode PTGInitVec(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_InitVecNode& node);


class PTG_FunctionTypeDeclNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_FunctionTypeDeclNode();
  PTG_FunctionTypeDeclNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_FunctionTypeDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_FunctionTypeDeclNode& node);
};

extern PTGNode PTGFunctionTypeDecl(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_FunctionTypeDeclNode& node);


class PTG_ListIndexNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ListIndexNode();
  PTG_ListIndexNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ListIndexNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListIndexNode& node);
};

extern PTGNode PTGListIndex(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListIndexNode& node);


class PTG_PtrExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PtrExprNode();
  PTG_PtrExprNode(PTGNode arg1);
  virtual ~PTG_PtrExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PtrExprNode& node);
};

extern PTGNode PTGPtrExpr(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PtrExprNode& node);


class PTG_NotNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_NotNode();
  PTG_NotNode(PTGNode arg1);
  virtual ~PTG_NotNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_NotNode& node);
};

extern PTGNode PTGNot(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_NotNode& node);


class PTG_LocNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_LocNode();
  PTG_LocNode(PTGNode arg1);
  virtual ~PTG_LocNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LocNode& node);
};

extern PTGNode PTGLoc(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_LocNode& node);


class PTG_NegNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_NegNode();
  PTG_NegNode(PTGNode arg1);
  virtual ~PTG_NegNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_NegNode& node);
};

extern PTGNode PTGNeg(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_NegNode& node);


class PTG_IncrNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_IncrNode();
  PTG_IncrNode(PTGNode arg1);
  virtual ~PTG_IncrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IncrNode& node);
};

extern PTGNode PTGIncr(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_IncrNode& node);


class PTG_StringTableOfNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_StringTableOfNode();
  PTG_StringTableOfNode(PTGNode arg1);
  virtual ~PTG_StringTableOfNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_StringTableOfNode& node);
};

extern PTGNode PTGStringTableOf(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_StringTableOfNode& node);


class PTG_ConcatObjNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ConcatObjNode();
  PTG_ConcatObjNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ConcatObjNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConcatObjNode& node);
};

extern PTGNode PTGConcatObj(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConcatObjNode& node);


class PTG_ConcatIndNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ConcatIndNode();
  PTG_ConcatIndNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ConcatIndNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConcatIndNode& node);
};

extern PTGNode PTGConcatInd(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConcatIndNode& node);


class PTG_ConcatIntNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ConcatIntNode();
  PTG_ConcatIntNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ConcatIntNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConcatIntNode& node);
};

extern PTGNode PTGConcatInt(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConcatIntNode& node);


class PTG_ConcatBoolNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ConcatBoolNode();
  PTG_ConcatBoolNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ConcatBoolNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConcatBoolNode& node);
};

extern PTGNode PTGConcatBool(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConcatBoolNode& node);


class PTG_IdnOfNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_IdnOfNode();
  PTG_IdnOfNode(PTGNode arg1);
  virtual ~PTG_IdnOfNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IdnOfNode& node);
};

extern PTGNode PTGIdnOf(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_IdnOfNode& node);


class PTG_BinOpNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_BinOpNode();
  PTG_BinOpNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_BinOpNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_BinOpNode& node);
};

extern PTGNode PTGBinOp(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_BinOpNode& node);


class PTG_ConcatExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ConcatExprNode();
  PTG_ConcatExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ConcatExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConcatExprNode& node);
};

extern PTGNode PTGConcatExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConcatExprNode& node);


class PTG_ConcatInvNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ConcatInvNode();
  PTG_ConcatInvNode(PTGNode arg1);
  virtual ~PTG_ConcatInvNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConcatInvNode& node);
};

extern PTGNode PTGConcatInv(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConcatInvNode& node);


class PTG_ListConcatExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ListConcatExprNode();
  PTG_ListConcatExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ListConcatExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListConcatExprNode& node);
};

extern PTGNode PTGListConcatExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListConcatExprNode& node);


class PTG_MakeTupleNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_MakeTupleNode();
  PTG_MakeTupleNode(PTGNode arg1);
  virtual ~PTG_MakeTupleNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_MakeTupleNode& node);
};

extern PTGNode PTGMakeTuple(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_MakeTupleNode& node);


class PTG_ErrorICENode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ErrorICENode();
  PTG_ErrorICENode(PTGNode arg1);
  virtual ~PTG_ErrorICENode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ErrorICENode& node);
};

extern PTGNode PTGErrorICE(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ErrorICENode& node);


class PTG_ErrorNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ErrorNode();
  PTG_ErrorNode(PTGNode arg1);
  virtual ~PTG_ErrorNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ErrorNode& node);
};

extern PTGNode PTGError(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ErrorNode& node);


class PTG_SeqOrNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqOrNode();
  PTG_SeqOrNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqOrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqOrNode& node);
};

extern PTGNode PTGSeqOr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqOrNode& node);


class PTG_TupleAccessNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_TupleAccessNode();
  PTG_TupleAccessNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_TupleAccessNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TupleAccessNode& node);
};

extern PTGNode PTGTupleAccess(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_TupleAccessNode& node);


class PTG_RecordAccessNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_RecordAccessNode();
  PTG_RecordAccessNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_RecordAccessNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RecordAccessNode& node);
};

extern PTGNode PTGRecordAccess(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_RecordAccessNode& node);


class PTG_ListAccessNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ListAccessNode();
  PTG_ListAccessNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ListAccessNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListAccessNode& node);
};

extern PTGNode PTGListAccess(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListAccessNode& node);


class PTG_WrapBracketsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_WrapBracketsNode();
  PTG_WrapBracketsNode(PTGNode arg1);
  virtual ~PTG_WrapBracketsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_WrapBracketsNode& node);
};

extern PTGNode PTGWrapBrackets(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_WrapBracketsNode& node);


class PTG_ListConNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ListConNode();
  PTG_ListConNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ListConNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListConNode& node);
};

extern PTGNode PTGListCon(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListConNode& node);


class PTG_RetPushbackNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_RetPushbackNode();
  PTG_RetPushbackNode(PTGNode arg1);
  virtual ~PTG_RetPushbackNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RetPushbackNode& node);
};

extern PTGNode PTGRetPushback(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_RetPushbackNode& node);


class PTG_ImplFunNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ImplFunNode();
  PTG_ImplFunNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ImplFunNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ImplFunNode& node);
};

extern PTGNode PTGImplFun(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ImplFunNode& node);


class PTG_TemplateClassNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_TemplateClassNode();
  PTG_TemplateClassNode(tStringType arg1);
  virtual ~PTG_TemplateClassNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TemplateClassNode& node);
};

extern PTGNode PTGTemplateClass(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_TemplateClassNode& node);


class PTG_WrapTemplatesNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_WrapTemplatesNode();
  PTG_WrapTemplatesNode(PTGNode arg1);
  virtual ~PTG_WrapTemplatesNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_WrapTemplatesNode& node);
};

extern PTGNode PTGWrapTemplates(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_WrapTemplatesNode& node);


class PTG_ArgDeclNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  int arg2;
public: 
  PTG_ArgDeclNode();
  PTG_ArgDeclNode(PTGNode arg1, int arg2);
  virtual ~PTG_ArgDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgDeclNode& node);
};

extern PTGNode PTGArgDecl(PTGNode arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgDeclNode& node);


class PTG_TemplateFunctionSignatureNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  PTGNode arg3;
  PTGNode arg4;
public: 
  PTG_TemplateFunctionSignatureNode();
  PTG_TemplateFunctionSignatureNode(PTGNode arg1, tStringType arg2, PTGNode arg3, PTGNode arg4);
  virtual ~PTG_TemplateFunctionSignatureNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TemplateFunctionSignatureNode& node);
};

extern PTGNode PTGTemplateFunctionSignature(PTGNode arg1, tStringType arg2, PTGNode arg3, PTGNode arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_TemplateFunctionSignatureNode& node);


class PTG_FunctionSignatureNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  PTGNode arg4;
public: 
  PTG_FunctionSignatureNode();
  PTG_FunctionSignatureNode(tStringType arg1, PTGNode arg2, PTGNode arg4);
  virtual ~PTG_FunctionSignatureNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_FunctionSignatureNode& node);
};

extern PTGNode PTGFunctionSignature(tStringType arg1, PTGNode arg2, PTGNode arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_FunctionSignatureNode& node);


class PTG_MapTypeNameNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_MapTypeNameNode();
  PTG_MapTypeNameNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_MapTypeNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_MapTypeNameNode& node);
};

extern PTGNode PTGMapTypeName(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_MapTypeNameNode& node);


class PTG_SimpleCondNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SimpleCondNode();
  PTG_SimpleCondNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SimpleCondNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SimpleCondNode& node);
};

extern PTGNode PTGSimpleCond(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SimpleCondNode& node);


class PTG_SimpleCondWarnNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SimpleCondWarnNode();
  PTG_SimpleCondWarnNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SimpleCondWarnNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SimpleCondWarnNode& node);
};

extern PTGNode PTGSimpleCondWarn(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SimpleCondWarnNode& node);


class PTG_SimpleImpleNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SimpleImpleNode();
  PTG_SimpleImpleNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SimpleImpleNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SimpleImpleNode& node);
};

extern PTGNode PTGSimpleImple(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SimpleImpleNode& node);


class PTG_RetDeclNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_RetDeclNode();
  PTG_RetDeclNode(PTGNode arg1);
  virtual ~PTG_RetDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RetDeclNode& node);
};

extern PTGNode PTGRetDecl(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_RetDeclNode& node);


class PTG_IterDeclNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  int arg2;
public: 
  PTG_IterDeclNode();
  PTG_IterDeclNode(tStringType arg1, int arg2);
  virtual ~PTG_IterDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IterDeclNode& node);
};

extern PTGNode PTGIterDecl(tStringType arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_IterDeclNode& node);


class PTG_IterationNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_IterationNode();
  PTG_IterationNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_IterationNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IterationNode& node);
};

extern PTGNode PTGIteration(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_IterationNode& node);


class PTG_ListInitRecNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
  int arg3;
public: 
  PTG_ListInitRecNode();
  PTG_ListInitRecNode(tStringType arg1, tStringType arg2, int arg3);
  virtual ~PTG_ListInitRecNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListInitRecNode& node);
};

extern PTGNode PTGListInitRec(tStringType arg1, tStringType arg2, int arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListInitRecNode& node);


class PTG_TypePtrNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_TypePtrNode();
  PTG_TypePtrNode(PTGNode arg1);
  virtual ~PTG_TypePtrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TypePtrNode& node);
};

extern PTGNode PTGTypePtr(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_TypePtrNode& node);


class PTG_ListInitRecNoTailNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  int arg3;
public: 
  PTG_ListInitRecNoTailNode();
  PTG_ListInitRecNoTailNode(tStringType arg1, int arg3);
  virtual ~PTG_ListInitRecNoTailNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListInitRecNoTailNode& node);
};

extern PTGNode PTGListInitRecNoTail(tStringType arg1, int arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListInitRecNoTailNode& node);


class PTG_RetAssignNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_RetAssignNode();
  PTG_RetAssignNode(PTGNode arg1);
  virtual ~PTG_RetAssignNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RetAssignNode& node);
};

extern PTGNode PTGRetAssign(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_RetAssignNode& node);


class PTG_ColonPreNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ColonPreNode();
  PTG_ColonPreNode(PTGNode arg1);
  virtual ~PTG_ColonPreNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ColonPreNode& node);
};

extern PTGNode PTGColonPre(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ColonPreNode& node);


class PTG_DataBaseDeclNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_DataBaseDeclNode();
  PTG_DataBaseDeclNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_DataBaseDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataBaseDeclNode& node);
};

extern PTGNode PTGDataBaseDecl(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataBaseDeclNode& node);


class PTG_DataInhSimpleNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
public: 
  PTG_DataInhSimpleNode();
  PTG_DataInhSimpleNode(PTGNode arg1, tStringType arg2);
  virtual ~PTG_DataInhSimpleNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataInhSimpleNode& node);
};

extern PTGNode PTGDataInhSimple(PTGNode arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataInhSimpleNode& node);


class PTG_ArgDefNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
public: 
  PTG_ArgDefNode();
  PTG_ArgDefNode(PTGNode arg1, tStringType arg2);
  virtual ~PTG_ArgDefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgDefNode& node);
};

extern PTGNode PTGArgDef(PTGNode arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgDefNode& node);


class PTG_ArgNameNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
public: 
  PTG_ArgNameNode();
  PTG_ArgNameNode(PTGNode arg1, tStringType arg2);
  virtual ~PTG_ArgNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgNameNode& node);
};

extern PTGNode PTGArgName(PTGNode arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgNameNode& node);


class PTG_ArgFunsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
public: 
  PTG_ArgFunsNode();
  PTG_ArgFunsNode(PTGNode arg1, tStringType arg2);
  virtual ~PTG_ArgFunsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgFunsNode& node);
};

extern PTGNode PTGArgFuns(PTGNode arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgFunsNode& node);


class PTG_ArgFunsImplTNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_ArgFunsImplTNode();
  PTG_ArgFunsImplTNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_ArgFunsImplTNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ArgFunsImplTNode& node);
};

extern PTGNode PTGArgFunsImplT(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ArgFunsImplTNode& node);


class PTG_ConstructorNameNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_ConstructorNameNode();
  PTG_ConstructorNameNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_ConstructorNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstructorNameNode& node);
};

extern PTGNode PTGConstructorName(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstructorNameNode& node);


class PTG_DataInhComplexNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  PTGNode arg3;
  PTGNode arg4;
  PTGNode arg5;
public: 
  PTG_DataInhComplexNode();
  PTG_DataInhComplexNode(PTGNode arg1, tStringType arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5);
  virtual ~PTG_DataInhComplexNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataInhComplexNode& node);
};

extern PTGNode PTGDataInhComplex(PTGNode arg1, tStringType arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataInhComplexNode& node);


class PTG_DataRecordNormalNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
public: 
  PTG_DataRecordNormalNode();
  PTG_DataRecordNormalNode(tStringType arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4);
  virtual ~PTG_DataRecordNormalNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataRecordNormalNode& node);
};

extern PTGNode PTGDataRecordNormal(tStringType arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataRecordNormalNode& node);


class PTG_TemplateRecordNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
  PTGNode arg5;
  PTGNode arg6;
  PTGNode arg7;
  PTGNode arg8;
  PTGNode arg9;
  PTGNode arg10;
  PTGNode arg11;
  PTGNode arg12;
  PTGNode arg13;
  PTGNode arg14;
  PTGNode arg15;
public: 
  PTG_TemplateRecordNode();
  PTG_TemplateRecordNode(tStringType arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9, PTGNode arg10, PTGNode arg11, PTGNode arg12, PTGNode arg13, PTGNode arg14, PTGNode arg15);
  virtual ~PTG_TemplateRecordNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TemplateRecordNode& node);
};

extern PTGNode PTGTemplateRecord(tStringType arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9, PTGNode arg10, PTGNode arg11, PTGNode arg12, PTGNode arg13, PTGNode arg14, PTGNode arg15); 
extern std::ostream& operator<<(std::ostream& os, PTG_TemplateRecordNode& node);


class PTG_TemplateArgDefNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_TemplateArgDefNode();
  PTG_TemplateArgDefNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_TemplateArgDefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TemplateArgDefNode& node);
};

extern PTGNode PTGTemplateArgDef(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_TemplateArgDefNode& node);


class PTG_TemplateArgNameNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_TemplateArgNameNode();
  PTG_TemplateArgNameNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_TemplateArgNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TemplateArgNameNode& node);
};

extern PTGNode PTGTemplateArgName(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_TemplateArgNameNode& node);


class PTG_TemplateDeclNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_TemplateDeclNode();
  PTG_TemplateDeclNode(tStringType arg1);
  virtual ~PTG_TemplateDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TemplateDeclNode& node);
};

extern PTGNode PTGTemplateDecl(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_TemplateDeclNode& node);


class PTG_RecordFunsNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_RecordFunsNode();
  PTG_RecordFunsNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_RecordFunsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RecordFunsNode& node);
};

extern PTGNode PTGRecordFuns(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_RecordFunsNode& node);


class PTG_CommaPreNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_CommaPreNode();
  PTG_CommaPreNode(PTGNode arg1);
  virtual ~PTG_CommaPreNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CommaPreNode& node);
};

extern PTGNode PTGCommaPre(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CommaPreNode& node);


class PTG_DataArgInitNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_DataArgInitNode();
  PTG_DataArgInitNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_DataArgInitNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataArgInitNode& node);
};

extern PTGNode PTGDataArgInit(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataArgInitNode& node);


class PTG_DataArgInitInNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_DataArgInitInNode();
  PTG_DataArgInitInNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_DataArgInitInNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataArgInitInNode& node);
};

extern PTGNode PTGDataArgInitIn(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataArgInitInNode& node);


class PTG_DefaultTypeInitNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_DefaultTypeInitNode();
  PTG_DefaultTypeInitNode(PTGNode arg1);
  virtual ~PTG_DefaultTypeInitNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultTypeInitNode& node);
};

extern PTGNode PTGDefaultTypeInit(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultTypeInitNode& node);


class PTG_DefaultECAInitNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_DefaultECAInitNode();
  PTG_DefaultECAInitNode(tStringType arg1);
  virtual ~PTG_DefaultECAInitNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultECAInitNode& node);
};

extern PTGNode PTGDefaultECAInit(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultECAInitNode& node);


class PTG_PrefixOtherNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_PrefixOtherNode();
  PTG_PrefixOtherNode(tStringType arg1);
  virtual ~PTG_PrefixOtherNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrefixOtherNode& node);
};

extern PTGNode PTGPrefixOther(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrefixOtherNode& node);


class PTG_RecordArgPrintNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_RecordArgPrintNode();
  PTG_RecordArgPrintNode(tStringType arg1);
  virtual ~PTG_RecordArgPrintNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RecordArgPrintNode& node);
};

extern PTGNode PTGRecordArgPrint(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_RecordArgPrintNode& node);


class PTG_TODONode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_TODONode();
  PTG_TODONode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_TODONode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TODONode& node);
};

extern PTGNode PTGTODO(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_TODONode& node);


class PTG_ConvertComplexNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_ConvertComplexNode();
  PTG_ConvertComplexNode(tStringType arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_ConvertComplexNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConvertComplexNode& node);
};

extern PTGNode PTGConvertComplex(tStringType arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConvertComplexNode& node);


class PTG_ReturnFalseNode : public PTG_BaseNode { 
public: 
  PTG_ReturnFalseNode();
  virtual ~PTG_ReturnFalseNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ReturnFalseNode& node);
};

extern PTGNode PTGReturnFalse(); 
extern std::ostream& operator<<(std::ostream& os, PTG_ReturnFalseNode& node);


class PTG_ReturnTrueNode : public PTG_BaseNode { 
public: 
  PTG_ReturnTrueNode();
  virtual ~PTG_ReturnTrueNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ReturnTrueNode& node);
};

extern PTGNode PTGReturnTrue(); 
extern std::ostream& operator<<(std::ostream& os, PTG_ReturnTrueNode& node);


class PTG_ConargLessNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ConargLessNode();
  PTG_ConargLessNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ConargLessNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConargLessNode& node);
};

extern PTGNode PTGConargLess(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConargLessNode& node);


class PTG_ConargLessPNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ConargLessPNode();
  PTG_ConargLessPNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ConargLessPNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConargLessPNode& node);
};

extern PTGNode PTGConargLessP(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConargLessPNode& node);


class PTG_DataBaseImplNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_DataBaseImplNode();
  PTG_DataBaseImplNode(tStringType arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_DataBaseImplNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataBaseImplNode& node);
};

extern PTGNode PTGDataBaseImpl(tStringType arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataBaseImplNode& node);


class PTG_DataInhSimpleINode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  tStringType arg3;
public: 
  PTG_DataInhSimpleINode();
  PTG_DataInhSimpleINode(PTGNode arg1, tStringType arg2, tStringType arg3);
  virtual ~PTG_DataInhSimpleINode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataInhSimpleINode& node);
};

extern PTGNode PTGDataInhSimpleI(PTGNode arg1, tStringType arg2, tStringType arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataInhSimpleINode& node);


class PTG_DataInhComplexINode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  tStringType arg3;
  PTGNode arg4;
  PTGNode arg5;
  PTGNode arg6;
  PTGNode arg7;
  PTGNode arg8;
  PTGNode arg9;
  PTGNode arg10;
  PTGNode arg11;
  PTGNode arg12;
  PTGNode arg13;
  PTGNode arg14;
  PTGNode arg15;
  PTGNode arg16;
  PTGNode arg17;
public: 
  PTG_DataInhComplexINode();
  PTG_DataInhComplexINode(PTGNode arg1, tStringType arg2, tStringType arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9, PTGNode arg10, PTGNode arg11, PTGNode arg12, PTGNode arg13, PTGNode arg14, PTGNode arg15, PTGNode arg16, PTGNode arg17);
  virtual ~PTG_DataInhComplexINode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataInhComplexINode& node);
};

extern PTGNode PTGDataInhComplexI(PTGNode arg1, tStringType arg2, tStringType arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9, PTGNode arg10, PTGNode arg11, PTGNode arg12, PTGNode arg13, PTGNode arg14, PTGNode arg15, PTGNode arg16, PTGNode arg17); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataInhComplexINode& node);


class PTG_RecordImplNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
  PTGNode arg5;
  PTGNode arg6;
  PTGNode arg7;
  PTGNode arg8;
  PTGNode arg9;
  PTGNode arg10;
  PTGNode arg11;
  PTGNode arg12;
  PTGNode arg13;
  PTGNode arg14;
  PTGNode arg15;
public: 
  PTG_RecordImplNode();
  PTG_RecordImplNode(tStringType arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9, PTGNode arg10, PTGNode arg11, PTGNode arg12, PTGNode arg13, PTGNode arg14, PTGNode arg15);
  virtual ~PTG_RecordImplNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RecordImplNode& node);
};

extern PTGNode PTGRecordImpl(tStringType arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6, PTGNode arg7, PTGNode arg8, PTGNode arg9, PTGNode arg10, PTGNode arg11, PTGNode arg12, PTGNode arg13, PTGNode arg14, PTGNode arg15); 
extern std::ostream& operator<<(std::ostream& os, PTG_RecordImplNode& node);


class PTG_DataArgMoveNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_DataArgMoveNode();
  PTG_DataArgMoveNode(tStringType arg1);
  virtual ~PTG_DataArgMoveNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataArgMoveNode& node);
};

extern PTGNode PTGDataArgMove(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataArgMoveNode& node);


class PTG_DataArgMoveInNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_DataArgMoveInNode();
  PTG_DataArgMoveInNode(tStringType arg1);
  virtual ~PTG_DataArgMoveInNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataArgMoveInNode& node);
};

extern PTGNode PTGDataArgMoveIn(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataArgMoveInNode& node);


class PTG_DataFunImplNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_DataFunImplNode();
  PTG_DataFunImplNode(PTGNode arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_DataFunImplNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DataFunImplNode& node);
};

extern PTGNode PTGDataFunImpl(PTGNode arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_DataFunImplNode& node);


class PTG_RecordFunImplNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_RecordFunImplNode();
  PTG_RecordFunImplNode(PTGNode arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_RecordFunImplNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RecordFunImplNode& node);
};

extern PTGNode PTGRecordFunImpl(PTGNode arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_RecordFunImplNode& node);


class PTG_PrintVariableNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintVariableNode();
  PTG_PrintVariableNode(PTGNode arg1);
  virtual ~PTG_PrintVariableNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintVariableNode& node);
};

extern PTGNode PTGPrintVariable(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintVariableNode& node);


class PTG_PrintListNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_PrintListNode();
  PTG_PrintListNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_PrintListNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintListNode& node);
};

extern PTGNode PTGPrintList(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintListNode& node);


class PTG_GetPrintArgNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_GetPrintArgNode();
  PTG_GetPrintArgNode(PTGNode arg1);
  virtual ~PTG_GetPrintArgNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GetPrintArgNode& node);
};

extern PTGNode PTGGetPrintArg(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_GetPrintArgNode& node);


class PTG_GetPrintArgPNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_GetPrintArgPNode();
  PTG_GetPrintArgPNode(PTGNode arg1);
  virtual ~PTG_GetPrintArgPNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GetPrintArgPNode& node);
};

extern PTGNode PTGGetPrintArgP(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_GetPrintArgPNode& node);


class PTG_GetTupleNodeNode : public PTG_BaseNode { 
protected: 
  int arg1;
  PTGNode arg2;
public: 
  PTG_GetTupleNodeNode();
  PTG_GetTupleNodeNode(int arg1, PTGNode arg2);
  virtual ~PTG_GetTupleNodeNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GetTupleNodeNode& node);
};

extern PTGNode PTGGetTupleNode(int arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GetTupleNodeNode& node);


class PTG_RecArgNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_RecArgNode();
  PTG_RecArgNode(PTGNode arg1);
  virtual ~PTG_RecArgNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RecArgNode& node);
};

extern PTGNode PTGRecArg(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_RecArgNode& node);


class PTG_PrintTupleNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintTupleNode();
  PTG_PrintTupleNode(PTGNode arg1);
  virtual ~PTG_PrintTupleNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintTupleNode& node);
};

extern PTGNode PTGPrintTuple(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintTupleNode& node);


class PTG_PrintFunctionSignatureNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintFunctionSignatureNode();
  PTG_PrintFunctionSignatureNode(PTGNode arg1);
  virtual ~PTG_PrintFunctionSignatureNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintFunctionSignatureNode& node);
};

extern PTGNode PTGPrintFunctionSignature(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintFunctionSignatureNode& node);


class PTG_AppendFirstNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_AppendFirstNode();
  PTG_AppendFirstNode(PTGNode arg1);
  virtual ~PTG_AppendFirstNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AppendFirstNode& node);
};

extern PTGNode PTGAppendFirst(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_AppendFirstNode& node);


class PTG_AppendSecondNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_AppendSecondNode();
  PTG_AppendSecondNode(PTGNode arg1);
  virtual ~PTG_AppendSecondNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AppendSecondNode& node);
};

extern PTGNode PTGAppendSecond(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_AppendSecondNode& node);


class PTG_PrintMapElemNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_PrintMapElemNode();
  PTG_PrintMapElemNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_PrintMapElemNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintMapElemNode& node);
};

extern PTGNode PTGPrintMapElem(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintMapElemNode& node);


class PTG_PrintAddressNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintAddressNode();
  PTG_PrintAddressNode(PTGNode arg1);
  virtual ~PTG_PrintAddressNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintAddressNode& node);
};

extern PTGNode PTGPrintAddress(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintAddressNode& node);


class PTG_PrintVoidNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintVoidNode();
  PTG_PrintVoidNode(PTGNode arg1);
  virtual ~PTG_PrintVoidNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintVoidNode& node);
};

extern PTGNode PTGPrintVoid(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintVoidNode& node);


class PTG_PrintDefaultNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintDefaultNode();
  PTG_PrintDefaultNode(PTGNode arg1);
  virtual ~PTG_PrintDefaultNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintDefaultNode& node);
};

extern PTGNode PTGPrintDefault(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintDefaultNode& node);


class PTG_CreateTupleRefNode : public PTG_BaseNode { 
protected: 
  int arg1;
  int arg2;
  PTGNode arg3;
public: 
  PTG_CreateTupleRefNode();
  PTG_CreateTupleRefNode(int arg1, int arg2, PTGNode arg3);
  virtual ~PTG_CreateTupleRefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CreateTupleRefNode& node);
};

extern PTGNode PTGCreateTupleRef(int arg1, int arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_CreateTupleRefNode& node);


class PTG_TupleRefNode : public PTG_BaseNode { 
protected: 
  int arg1;
  int arg2;
public: 
  PTG_TupleRefNode();
  PTG_TupleRefNode(int arg1, int arg2);
  virtual ~PTG_TupleRefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TupleRefNode& node);
};

extern PTGNode PTGTupleRef(int arg1, int arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_TupleRefNode& node);


class PTG_GetArgNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
public: 
  PTG_GetArgNode();
  PTG_GetArgNode(PTGNode arg1, tStringType arg2);
  virtual ~PTG_GetArgNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GetArgNode& node);
};

extern PTGNode PTGGetArg(PTGNode arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GetArgNode& node);


class PTG_GetArgPNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
public: 
  PTG_GetArgPNode();
  PTG_GetArgPNode(PTGNode arg1, tStringType arg2);
  virtual ~PTG_GetArgPNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GetArgPNode& node);
};

extern PTGNode PTGGetArgP(PTGNode arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GetArgPNode& node);


class PTG_PrintSkeyNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintSkeyNode();
  PTG_PrintSkeyNode(PTGNode arg1);
  virtual ~PTG_PrintSkeyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintSkeyNode& node);
};

extern PTGNode PTGPrintSkey(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintSkeyNode& node);


class PTG_PrintBindingNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintBindingNode();
  PTG_PrintBindingNode(PTGNode arg1);
  virtual ~PTG_PrintBindingNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintBindingNode& node);
};

extern PTGNode PTGPrintBinding(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintBindingNode& node);


class PTG_PrintBoolNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintBoolNode();
  PTG_PrintBoolNode(PTGNode arg1);
  virtual ~PTG_PrintBoolNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintBoolNode& node);
};

extern PTGNode PTGPrintBool(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintBoolNode& node);


class PTG_PrintKeyNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintKeyNode();
  PTG_PrintKeyNode(PTGNode arg1);
  virtual ~PTG_PrintKeyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintKeyNode& node);
};

extern PTGNode PTGPrintKey(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintKeyNode& node);


class PTG_PrintEnvNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_PrintEnvNode();
  PTG_PrintEnvNode(PTGNode arg1);
  virtual ~PTG_PrintEnvNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_PrintEnvNode& node);
};

extern PTGNode PTGPrintEnv(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_PrintEnvNode& node);


class PTG_SimpleEqCaseNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_SimpleEqCaseNode();
  PTG_SimpleEqCaseNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_SimpleEqCaseNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SimpleEqCaseNode& node);
};

extern PTGNode PTGSimpleEqCase(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SimpleEqCaseNode& node);


class PTG_ComplexEqCaseNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_ComplexEqCaseNode();
  PTG_ComplexEqCaseNode(tStringType arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_ComplexEqCaseNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ComplexEqCaseNode& node);
};

extern PTGNode PTGComplexEqCase(tStringType arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_ComplexEqCaseNode& node);


class PTG_SimpleLtCaseNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_SimpleLtCaseNode();
  PTG_SimpleLtCaseNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_SimpleLtCaseNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SimpleLtCaseNode& node);
};

extern PTGNode PTGSimpleLtCase(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SimpleLtCaseNode& node);


class PTG_ComplexLtCaseNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_ComplexLtCaseNode();
  PTG_ComplexLtCaseNode(tStringType arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_ComplexLtCaseNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ComplexLtCaseNode& node);
};

extern PTGNode PTGComplexLtCase(tStringType arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_ComplexLtCaseNode& node);


class PTG_ConstantDeclNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_ConstantDeclNode();
  PTG_ConstantDeclNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_ConstantDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstantDeclNode& node);
};

extern PTGNode PTGConstantDecl(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstantDeclNode& node);


class PTG_ConstantImplNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_ConstantImplNode();
  PTG_ConstantImplNode(tStringType arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_ConstantImplNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstantImplNode& node);
};

extern PTGNode PTGConstantImpl(tStringType arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstantImplNode& node);


class PTG_TreeFunNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_TreeFunNode();
  PTG_TreeFunNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_TreeFunNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TreeFunNode& node);
};

extern PTGNode PTGTreeFun(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_TreeFunNode& node);


class PTG_InhNode : public PTG_BaseNode { 
public: 
  PTG_InhNode();
  virtual ~PTG_InhNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_InhNode& node);
};

extern PTGNode PTGInh(); 
extern std::ostream& operator<<(std::ostream& os, PTG_InhNode& node);


class PTG_SyntNode : public PTG_BaseNode { 
public: 
  PTG_SyntNode();
  virtual ~PTG_SyntNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SyntNode& node);
};

extern PTGNode PTGSynt(); 
extern std::ostream& operator<<(std::ostream& os, PTG_SyntNode& node);


class PTG_InhNameNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_InhNameNode();
  PTG_InhNameNode(tStringType arg1);
  virtual ~PTG_InhNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_InhNameNode& node);
};

extern PTGNode PTGInhName(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_InhNameNode& node);


class PTG_SyntNameNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_SyntNameNode();
  PTG_SyntNameNode(tStringType arg1);
  virtual ~PTG_SyntNameNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SyntNameNode& node);
};

extern PTGNode PTGSyntName(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_SyntNameNode& node);


class PTG_AttrDefNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_AttrDefNode();
  PTG_AttrDefNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_AttrDefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AttrDefNode& node);
};

extern PTGNode PTGAttrDef(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_AttrDefNode& node);


class PTG_AttrDefGNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_AttrDefGNode();
  PTG_AttrDefGNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_AttrDefGNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AttrDefGNode& node);
};

extern PTGNode PTGAttrDefG(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_AttrDefGNode& node);


class PTG_SeqCommaNLNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqCommaNLNode();
  PTG_SeqCommaNLNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqCommaNLNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqCommaNLNode& node);
};

extern PTGNode PTGSeqCommaNL(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqCommaNLNode& node);


class PTG_SeqSemiNLNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqSemiNLNode();
  PTG_SeqSemiNLNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqSemiNLNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqSemiNLNode& node);
};

extern PTGNode PTGSeqSemiNL(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqSemiNLNode& node);


class PTG_CoordInfoNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_CoordInfoNode();
  PTG_CoordInfoNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_CoordInfoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CoordInfoNode& node);
};

extern PTGNode PTGCoordInfo(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_CoordInfoNode& node);


class PTG_TailNode : public PTG_BaseNode { 
public: 
  PTG_TailNode();
  virtual ~PTG_TailNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TailNode& node);
};

extern PTGNode PTGTail(); 
extern std::ostream& operator<<(std::ostream& os, PTG_TailNode& node);


class PTG_HeadNode : public PTG_BaseNode { 
public: 
  PTG_HeadNode();
  virtual ~PTG_HeadNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_HeadNode& node);
};

extern PTGNode PTGHead(); 
extern std::ostream& operator<<(std::ostream& os, PTG_HeadNode& node);


class PTG_AttributeNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_AttributeNode();
  PTG_AttributeNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_AttributeNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AttributeNode& node);
};

extern PTGNode PTGAttribute(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_AttributeNode& node);


class PTG_IncludingNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_IncludingNode();
  PTG_IncludingNode(PTGNode arg1);
  virtual ~PTG_IncludingNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IncludingNode& node);
};

extern PTGNode PTGIncluding(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_IncludingNode& node);


class PTG_IncludingsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_IncludingsNode();
  PTG_IncludingsNode(PTGNode arg1);
  virtual ~PTG_IncludingsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IncludingsNode& node);
};

extern PTGNode PTGIncludings(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_IncludingsNode& node);


class PTG_ConstituentDepNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ConstituentDepNode();
  PTG_ConstituentDepNode(PTGNode arg1);
  virtual ~PTG_ConstituentDepNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstituentDepNode& node);
};

extern PTGNode PTGConstituentDep(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstituentDepNode& node);


class PTG_ConstituentDepsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ConstituentDepsNode();
  PTG_ConstituentDepsNode(PTGNode arg1);
  virtual ~PTG_ConstituentDepsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstituentDepsNode& node);
};

extern PTGNode PTGConstituentDeps(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstituentDepsNode& node);


class PTG_LocalAttrNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_LocalAttrNode();
  PTG_LocalAttrNode(tStringType arg1);
  virtual ~PTG_LocalAttrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LocalAttrNode& node);
};

extern PTGNode PTGLocalAttr(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_LocalAttrNode& node);


class PTG_AssignNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_AssignNode();
  PTG_AssignNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_AssignNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AssignNode& node);
};

extern PTGNode PTGAssign(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_AssignNode& node);


class PTG_IfNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_IfNode();
  PTG_IfNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_IfNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IfNode& node);
};

extern PTGNode PTGIf(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_IfNode& node);


class PTG_OrNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_OrNode();
  PTG_OrNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_OrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_OrNode& node);
};

extern PTGNode PTGOr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_OrNode& node);


class PTG_LidoLambdaNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_LidoLambdaNode();
  PTG_LidoLambdaNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_LidoLambdaNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LidoLambdaNode& node);
};

extern PTGNode PTGLidoLambda(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_LidoLambdaNode& node);


class PTG_ErrorReportLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ErrorReportLidoNode();
  PTG_ErrorReportLidoNode(PTGNode arg1);
  virtual ~PTG_ErrorReportLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ErrorReportLidoNode& node);
};

extern PTGNode PTGErrorReportLido(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ErrorReportLidoNode& node);


class PTG_CondLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_CondLidoNode();
  PTG_CondLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_CondLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CondLidoNode& node);
};

extern PTGNode PTGCondLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_CondLidoNode& node);


class PTG_WhenLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_WhenLidoNode();
  PTG_WhenLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_WhenLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_WhenLidoNode& node);
};

extern PTGNode PTGWhenLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_WhenLidoNode& node);


class PTG_AddExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_AddExprNode();
  PTG_AddExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_AddExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AddExprNode& node);
};

extern PTGNode PTGAddExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_AddExprNode& node);


class PTG_AddLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_AddLidoNode();
  PTG_AddLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_AddLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AddLidoNode& node);
};

extern PTGNode PTGAddLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_AddLidoNode& node);


class PTG_SubExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SubExprNode();
  PTG_SubExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SubExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SubExprNode& node);
};

extern PTGNode PTGSubExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SubExprNode& node);


class PTG_SubLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SubLidoNode();
  PTG_SubLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SubLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SubLidoNode& node);
};

extern PTGNode PTGSubLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SubLidoNode& node);


class PTG_DivExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_DivExprNode();
  PTG_DivExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_DivExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DivExprNode& node);
};

extern PTGNode PTGDivExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_DivExprNode& node);


class PTG_DivLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_DivLidoNode();
  PTG_DivLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_DivLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DivLidoNode& node);
};

extern PTGNode PTGDivLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_DivLidoNode& node);


class PTG_MulExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_MulExprNode();
  PTG_MulExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_MulExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_MulExprNode& node);
};

extern PTGNode PTGMulExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_MulExprNode& node);


class PTG_MulLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_MulLidoNode();
  PTG_MulLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_MulLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_MulLidoNode& node);
};

extern PTGNode PTGMulLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_MulLidoNode& node);


class PTG_ModExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ModExprNode();
  PTG_ModExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ModExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ModExprNode& node);
};

extern PTGNode PTGModExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ModExprNode& node);


class PTG_ModLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ModLidoNode();
  PTG_ModLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ModLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ModLidoNode& node);
};

extern PTGNode PTGModLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ModLidoNode& node);


class PTG_EqExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_EqExprNode();
  PTG_EqExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_EqExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EqExprNode& node);
};

extern PTGNode PTGEqExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_EqExprNode& node);


class PTG_EqLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_EqLidoNode();
  PTG_EqLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_EqLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EqLidoNode& node);
};

extern PTGNode PTGEqLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_EqLidoNode& node);


class PTG_NeExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_NeExprNode();
  PTG_NeExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_NeExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_NeExprNode& node);
};

extern PTGNode PTGNeExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_NeExprNode& node);


class PTG_NeLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_NeLidoNode();
  PTG_NeLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_NeLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_NeLidoNode& node);
};

extern PTGNode PTGNeLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_NeLidoNode& node);


class PTG_GtExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_GtExprNode();
  PTG_GtExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_GtExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GtExprNode& node);
};

extern PTGNode PTGGtExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GtExprNode& node);


class PTG_GtLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_GtLidoNode();
  PTG_GtLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_GtLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GtLidoNode& node);
};

extern PTGNode PTGGtLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GtLidoNode& node);


class PTG_GeExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_GeExprNode();
  PTG_GeExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_GeExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GeExprNode& node);
};

extern PTGNode PTGGeExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GeExprNode& node);


class PTG_GeLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_GeLidoNode();
  PTG_GeLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_GeLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GeLidoNode& node);
};

extern PTGNode PTGGeLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GeLidoNode& node);


class PTG_LtExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_LtExprNode();
  PTG_LtExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_LtExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LtExprNode& node);
};

extern PTGNode PTGLtExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_LtExprNode& node);


class PTG_LtLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_LtLidoNode();
  PTG_LtLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_LtLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LtLidoNode& node);
};

extern PTGNode PTGLtLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_LtLidoNode& node);


class PTG_LeExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_LeExprNode();
  PTG_LeExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_LeExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LeExprNode& node);
};

extern PTGNode PTGLeExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_LeExprNode& node);


class PTG_LeLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_LeLidoNode();
  PTG_LeLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_LeLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LeLidoNode& node);
};

extern PTGNode PTGLeLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_LeLidoNode& node);


class PTG_OrExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_OrExprNode();
  PTG_OrExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_OrExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_OrExprNode& node);
};

extern PTGNode PTGOrExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_OrExprNode& node);


class PTG_OrLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_OrLidoNode();
  PTG_OrLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_OrLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_OrLidoNode& node);
};

extern PTGNode PTGOrLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_OrLidoNode& node);


class PTG_AndExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_AndExprNode();
  PTG_AndExprNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_AndExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AndExprNode& node);
};

extern PTGNode PTGAndExpr(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_AndExprNode& node);


class PTG_AndLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_AndLidoNode();
  PTG_AndLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_AndLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AndLidoNode& node);
};

extern PTGNode PTGAndLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_AndLidoNode& node);


class PTG_NegLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_NegLidoNode();
  PTG_NegLidoNode(PTGNode arg1);
  virtual ~PTG_NegLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_NegLidoNode& node);
};

extern PTGNode PTGNegLido(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_NegLidoNode& node);


class PTG_NegExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_NegExprNode();
  PTG_NegExprNode(PTGNode arg1);
  virtual ~PTG_NegExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_NegExprNode& node);
};

extern PTGNode PTGNegExpr(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_NegExprNode& node);


class PTG_NotLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_NotLidoNode();
  PTG_NotLidoNode(PTGNode arg1);
  virtual ~PTG_NotLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_NotLidoNode& node);
};

extern PTGNode PTGNotLido(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_NotLidoNode& node);


class PTG_NotExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_NotExprNode();
  PTG_NotExprNode(PTGNode arg1);
  virtual ~PTG_NotExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_NotExprNode& node);
};

extern PTGNode PTGNotExpr(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_NotExprNode& node);


class PTG_LocLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_LocLidoNode();
  PTG_LocLidoNode(PTGNode arg1);
  virtual ~PTG_LocLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LocLidoNode& node);
};

extern PTGNode PTGLocLido(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_LocLidoNode& node);


class PTG_LocExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_LocExprNode();
  PTG_LocExprNode(PTGNode arg1);
  virtual ~PTG_LocExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_LocExprNode& node);
};

extern PTGNode PTGLocExpr(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_LocExprNode& node);


class PTG_RefLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_RefLidoNode();
  PTG_RefLidoNode(PTGNode arg1);
  virtual ~PTG_RefLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RefLidoNode& node);
};

extern PTGNode PTGRefLido(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_RefLidoNode& node);


class PTG_RefExprNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_RefExprNode();
  PTG_RefExprNode(PTGNode arg1);
  virtual ~PTG_RefExprNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RefExprNode& node);
};

extern PTGNode PTGRefExpr(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_RefExprNode& node);


class PTG_ListIndexLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ListIndexLidoNode();
  PTG_ListIndexLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ListIndexLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListIndexLidoNode& node);
};

extern PTGNode PTGListIndexLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListIndexLidoNode& node);


class PTG_ListAccessLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ListAccessLidoNode();
  PTG_ListAccessLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ListAccessLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListAccessLidoNode& node);
};

extern PTGNode PTGListAccessLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListAccessLidoNode& node);


class PTG_TupleAccessLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_TupleAccessLidoNode();
  PTG_TupleAccessLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_TupleAccessLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TupleAccessLidoNode& node);
};

extern PTGNode PTGTupleAccessLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_TupleAccessLidoNode& node);


class PTG_RecordAccessLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_RecordAccessLidoNode();
  PTG_RecordAccessLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_RecordAccessLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RecordAccessLidoNode& node);
};

extern PTGNode PTGRecordAccessLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_RecordAccessLidoNode& node);


class PTG_ListConLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ListConLidoNode();
  PTG_ListConLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ListConLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListConLidoNode& node);
};

extern PTGNode PTGListConLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListConLidoNode& node);


class PTG_ListConPNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ListConPNode();
  PTG_ListConPNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ListConPNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ListConPNode& node);
};

extern PTGNode PTGListConP(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ListConPNode& node);


class PTG_MakeTupleLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_MakeTupleLidoNode();
  PTG_MakeTupleLidoNode(PTGNode arg1);
  virtual ~PTG_MakeTupleLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_MakeTupleLidoNode& node);
};

extern PTGNode PTGMakeTupleLido(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_MakeTupleLidoNode& node);


class PTG_CallLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_CallLidoNode();
  PTG_CallLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_CallLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CallLidoNode& node);
};

extern PTGNode PTGCallLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_CallLidoNode& node);


class PTG_CallLidoEmptyNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_CallLidoEmptyNode();
  PTG_CallLidoEmptyNode(PTGNode arg1);
  virtual ~PTG_CallLidoEmptyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CallLidoEmptyNode& node);
};

extern PTGNode PTGCallLidoEmpty(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CallLidoEmptyNode& node);


class PTG_CallNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_CallNode();
  PTG_CallNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_CallNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CallNode& node);
};

extern PTGNode PTGCall(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_CallNode& node);


class PTG_CallEmptyNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_CallEmptyNode();
  PTG_CallEmptyNode(PTGNode arg1);
  virtual ~PTG_CallEmptyNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_CallEmptyNode& node);
};

extern PTGNode PTGCallEmpty(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_CallEmptyNode& node);


class PTG_EnumAccessLidoNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
public: 
  PTG_EnumAccessLidoNode();
  PTG_EnumAccessLidoNode(tStringType arg1, tStringType arg2);
  virtual ~PTG_EnumAccessLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EnumAccessLidoNode& node);
};

extern PTGNode PTGEnumAccessLido(tStringType arg1, tStringType arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_EnumAccessLidoNode& node);


class PTG_RuleCallNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_RuleCallNode();
  PTG_RuleCallNode(tStringType arg1);
  virtual ~PTG_RuleCallNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RuleCallNode& node);
};

extern PTGNode PTGRuleCall(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_RuleCallNode& node);


class PTG_EmptyListCallNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_EmptyListCallNode();
  PTG_EmptyListCallNode(PTGNode arg1);
  virtual ~PTG_EmptyListCallNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EmptyListCallNode& node);
};

extern PTGNode PTGEmptyListCall(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_EmptyListCallNode& node);


class PTG_SeqDotNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_SeqDotNode();
  PTG_SeqDotNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_SeqDotNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SeqDotNode& node);
};

extern PTGNode PTGSeqDot(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_SeqDotNode& node);


class PTG_ConstituentsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ConstituentsNode();
  PTG_ConstituentsNode(PTGNode arg1);
  virtual ~PTG_ConstituentsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstituentsNode& node);
};

extern PTGNode PTGConstituents(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstituentsNode& node);


class PTG_ConstituentNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ConstituentNode();
  PTG_ConstituentNode(PTGNode arg1);
  virtual ~PTG_ConstituentNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstituentNode& node);
};

extern PTGNode PTGConstituent(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstituentNode& node);


class PTG_ConstituentOptNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ConstituentOptNode();
  PTG_ConstituentOptNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ConstituentOptNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstituentOptNode& node);
};

extern PTGNode PTGConstituentOpt(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstituentOptNode& node);


class PTG_ConstituentsOptNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ConstituentsOptNode();
  PTG_ConstituentsOptNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ConstituentsOptNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ConstituentsOptNode& node);
};

extern PTGNode PTGConstituentsOpt(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ConstituentsOptNode& node);


class PTG_UnshieldNode : public PTG_BaseNode { 
public: 
  PTG_UnshieldNode();
  virtual ~PTG_UnshieldNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_UnshieldNode& node);
};

extern PTGNode PTGUnshield(); 
extern std::ostream& operator<<(std::ostream& os, PTG_UnshieldNode& node);


class PTG_ShieldOneNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ShieldOneNode();
  PTG_ShieldOneNode(PTGNode arg1);
  virtual ~PTG_ShieldOneNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ShieldOneNode& node);
};

extern PTGNode PTGShieldOne(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ShieldOneNode& node);


class PTG_ShieldNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ShieldNode();
  PTG_ShieldNode(PTGNode arg1);
  virtual ~PTG_ShieldNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ShieldNode& node);
};

extern PTGNode PTGShield(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ShieldNode& node);


class PTG_WithDefNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
  PTGNode arg4;
public: 
  PTG_WithDefNode();
  PTG_WithDefNode(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4);
  virtual ~PTG_WithDefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_WithDefNode& node);
};

extern PTGNode PTGWithDef(PTGNode arg1, PTGNode arg2, PTGNode arg3, PTGNode arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_WithDefNode& node);


class PTG_ExprWithDepsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ExprWithDepsNode();
  PTG_ExprWithDepsNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ExprWithDepsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ExprWithDepsNode& node);
};

extern PTGNode PTGExprWithDeps(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ExprWithDepsNode& node);


class PTG_ChainStartNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_ChainStartNode();
  PTG_ChainStartNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_ChainStartNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ChainStartNode& node);
};

extern PTGNode PTGChainStart(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_ChainStartNode& node);


class PTG_OrderNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_OrderNode();
  PTG_OrderNode(PTGNode arg1);
  virtual ~PTG_OrderNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_OrderNode& node);
};

extern PTGNode PTGOrder(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_OrderNode& node);


class PTG_AssignStatLidoNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_AssignStatLidoNode();
  PTG_AssignStatLidoNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_AssignStatLidoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AssignStatLidoNode& node);
};

extern PTGNode PTGAssignStatLido(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_AssignStatLidoNode& node);


class PTG_EmptyDefNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_EmptyDefNode();
  PTG_EmptyDefNode(tStringType arg1);
  virtual ~PTG_EmptyDefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_EmptyDefNode& node);
};

extern PTGNode PTGEmptyDef(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_EmptyDefNode& node);


class PTG_SingleDefNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_SingleDefNode();
  PTG_SingleDefNode(tStringType arg1);
  virtual ~PTG_SingleDefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SingleDefNode& node);
};

extern PTGNode PTGSingleDef(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_SingleDefNode& node);


class PTG_AppendDefNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_AppendDefNode();
  PTG_AppendDefNode(tStringType arg1);
  virtual ~PTG_AppendDefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AppendDefNode& node);
};

extern PTGNode PTGAppendDef(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_AppendDefNode& node);


class PTG_SymbolAttrDeclsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_SymbolAttrDeclsNode();
  PTG_SymbolAttrDeclsNode(PTGNode arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_SymbolAttrDeclsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SymbolAttrDeclsNode& node);
};

extern PTGNode PTGSymbolAttrDecls(PTGNode arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_SymbolAttrDeclsNode& node);


class PTG_SymbolInheritStatNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_SymbolInheritStatNode();
  PTG_SymbolInheritStatNode(PTGNode arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_SymbolInheritStatNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SymbolInheritStatNode& node);
};

extern PTGNode PTGSymbolInheritStat(PTGNode arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_SymbolInheritStatNode& node);


class PTG_SymbolComputationsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_SymbolComputationsNode();
  PTG_SymbolComputationsNode(PTGNode arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_SymbolComputationsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SymbolComputationsNode& node);
};

extern PTGNode PTGSymbolComputations(PTGNode arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_SymbolComputationsNode& node);


class PTG_SymbolInheritComputeNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  tStringType arg2;
  PTGNode arg3;
  PTGNode arg4;
public: 
  PTG_SymbolInheritComputeNode();
  PTG_SymbolInheritComputeNode(PTGNode arg1, tStringType arg2, PTGNode arg3, PTGNode arg4);
  virtual ~PTG_SymbolInheritComputeNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SymbolInheritComputeNode& node);
};

extern PTGNode PTGSymbolInheritCompute(PTGNode arg1, tStringType arg2, PTGNode arg3, PTGNode arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_SymbolInheritComputeNode& node);


class PTG_ChainStartAttrNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
public: 
  PTG_ChainStartAttrNode();
  PTG_ChainStartAttrNode(PTGNode arg1);
  virtual ~PTG_ChainStartAttrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_ChainStartAttrNode& node);
};

extern PTGNode PTGChainStartAttr(PTGNode arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_ChainStartAttrNode& node);


class PTG_SymbolIndexNode : public PTG_BaseNode { 
protected: 
  int arg1;
public: 
  PTG_SymbolIndexNode();
  PTG_SymbolIndexNode(int arg1);
  virtual ~PTG_SymbolIndexNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_SymbolIndexNode& node);
};

extern PTGNode PTGSymbolIndex(int arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_SymbolIndexNode& node);


class PTG_RuleAttrNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_RuleAttrNode();
  PTG_RuleAttrNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_RuleAttrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RuleAttrNode& node);
};

extern PTGNode PTGRuleAttr(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_RuleAttrNode& node);


class PTG_GAttrDefNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_GAttrDefNode();
  PTG_GAttrDefNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_GAttrDefNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GAttrDefNode& node);
};

extern PTGNode PTGGAttrDef(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_GAttrDefNode& node);


class PTG_GChainNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_GChainNode();
  PTG_GChainNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_GChainNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GChainNode& node);
};

extern PTGNode PTGGChain(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_GChainNode& node);


class PTG_DefineLNGNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
public: 
  PTG_DefineLNGNode();
  PTG_DefineLNGNode(PTGNode arg1, PTGNode arg2);
  virtual ~PTG_DefineLNGNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefineLNGNode& node);
};

extern PTGNode PTGDefineLNG(PTGNode arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefineLNGNode& node);


class PTG_DefaultVattrRuleLhsNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_DefaultVattrRuleLhsNode();
  PTG_DefaultVattrRuleLhsNode(tStringType arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_DefaultVattrRuleLhsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultVattrRuleLhsNode& node);
};

extern PTGNode PTGDefaultVattrRuleLhs(tStringType arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultVattrRuleLhsNode& node);


class PTG_TreeCompsNode : public PTG_BaseNode { 
protected: 
  PTGNode arg1;
  PTGNode arg2;
  PTGNode arg3;
public: 
  PTG_TreeCompsNode();
  PTG_TreeCompsNode(PTGNode arg1, PTGNode arg2, PTGNode arg3);
  virtual ~PTG_TreeCompsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TreeCompsNode& node);
};

extern PTGNode PTGTreeComps(PTGNode arg1, PTGNode arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_TreeCompsNode& node);


class PTG_RuleComputationsNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
  PTGNode arg3;
  PTGNode arg4;
public: 
  PTG_RuleComputationsNode();
  PTG_RuleComputationsNode(tStringType arg1, tStringType arg2, PTGNode arg3, PTGNode arg4);
  virtual ~PTG_RuleComputationsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RuleComputationsNode& node);
};

extern PTGNode PTGRuleComputations(tStringType arg1, tStringType arg2, PTGNode arg3, PTGNode arg4); 
extern std::ostream& operator<<(std::ostream& os, PTG_RuleComputationsNode& node);


class PTG_VirtAttrDeclsNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_VirtAttrDeclsNode();
  PTG_VirtAttrDeclsNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_VirtAttrDeclsNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_VirtAttrDeclsNode& node);
};

extern PTGNode PTGVirtAttrDecls(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_VirtAttrDeclsNode& node);


class PTG_VirtAttrDeclsClassNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_VirtAttrDeclsClassNode();
  PTG_VirtAttrDeclsClassNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_VirtAttrDeclsClassNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_VirtAttrDeclsClassNode& node);
};

extern PTGNode PTGVirtAttrDeclsClass(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_VirtAttrDeclsClassNode& node);


class PTG_DefaultVAttrNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_DefaultVAttrNode();
  PTG_DefaultVAttrNode(tStringType arg1);
  virtual ~PTG_DefaultVAttrNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_DefaultVAttrNode& node);
};

extern PTGNode PTGDefaultVAttr(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_DefaultVAttrNode& node);


class PTG_VirtAttrComputeNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_VirtAttrComputeNode();
  PTG_VirtAttrComputeNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_VirtAttrComputeNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_VirtAttrComputeNode& node);
};

extern PTGNode PTGVirtAttrCompute(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_VirtAttrComputeNode& node);


class PTG_TermDeclNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_TermDeclNode();
  PTG_TermDeclNode(tStringType arg1);
  virtual ~PTG_TermDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TermDeclNode& node);
};

extern PTGNode PTGTermDecl(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_TermDeclNode& node);


class PTG_IsTermNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_IsTermNode();
  PTG_IsTermNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_IsTermNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_IsTermNode& node);
};

extern PTGNode PTGIsTerm(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_IsTermNode& node);


class PTG_TermSymbNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
public: 
  PTG_TermSymbNode();
  PTG_TermSymbNode(tStringType arg1, PTGNode arg2);
  virtual ~PTG_TermSymbNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TermSymbNode& node);
};

extern PTGNode PTGTermSymb(tStringType arg1, PTGNode arg2); 
extern std::ostream& operator<<(std::ostream& os, PTG_TermSymbNode& node);


class PTG_TermSymbConvNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  PTGNode arg2;
  tStringType arg3;
public: 
  PTG_TermSymbConvNode();
  PTG_TermSymbConvNode(tStringType arg1, PTGNode arg2, tStringType arg3);
  virtual ~PTG_TermSymbConvNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_TermSymbConvNode& node);
};

extern PTGNode PTGTermSymbConv(tStringType arg1, PTGNode arg2, tStringType arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_TermSymbConvNode& node);


class PTG_GenSymbolNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_GenSymbolNode();
  PTG_GenSymbolNode(tStringType arg1);
  virtual ~PTG_GenSymbolNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_GenSymbolNode& node);
};

extern PTGNode PTGGenSymbol(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_GenSymbolNode& node);


class PTG_RuleDeclNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
  tStringType arg2;
  PTGNode arg3;
public: 
  PTG_RuleDeclNode();
  PTG_RuleDeclNode(tStringType arg1, tStringType arg2, PTGNode arg3);
  virtual ~PTG_RuleDeclNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_RuleDeclNode& node);
};

extern PTGNode PTGRuleDecl(tStringType arg1, tStringType arg2, PTGNode arg3); 
extern std::ostream& operator<<(std::ostream& os, PTG_RuleDeclNode& node);


class PTG_AbstreeInfoNode : public PTG_BaseNode { 
protected: 
  tStringType arg1;
public: 
  PTG_AbstreeInfoNode();
  PTG_AbstreeInfoNode(tStringType arg1);
  virtual ~PTG_AbstreeInfoNode();
  virtual bool print_optional() const; 
  friend std::ostream& operator<<(std::ostream& os, PTG_AbstreeInfoNode& node);
};

extern PTGNode PTGAbstreeInfo(tStringType arg1); 
extern std::ostream& operator<<(std::ostream& os, PTG_AbstreeInfoNode& node);





class DefTable { 
private: 
  struct DefTableEntry { 
    std::vector<bool> has_args; 
    CoordPtr arg_0;
    PTGNode arg_1;
    int arg_2;
    TypingKind arg_3;
    Binding arg_4;
    StringTableKeyList arg_5;
    int arg_6;
    PTGNode arg_7;
    PTGNode arg_8;
    int arg_9;
    StringTableKey arg_10;
    StringTableKey arg_11;
    StringTableKeyList arg_12;
    bool arg_13;
    StringTableKeyList arg_14;
    int arg_15;
    StringTableKey arg_16;
    int arg_17;
    StringTableKey arg_18;
    StringTableKey arg_19;
    Environment arg_20;
    bool arg_21;
    int arg_22;
    StringTableKey arg_23;
    bool arg_24;
    StringTableKey arg_25;
    StringTableKeyList arg_26;
    D_patternLists arg_27;
    bool arg_28;
    D_types arg_29;
    Environment arg_30;
    D_typePtr arg_31;
    Binding arg_32;
    bool arg_33;
    D_scheme arg_34;
    int arg_35;
    int arg_36;
    int arg_37;
    PTGNode arg_38;
    D_PatternCodes arg_39;
    bool arg_40;
    int arg_41;
    Environment arg_42;
    Environment arg_43;
    Binding arg_44;
    StringTableKey arg_45;
    D_ProdSymbols arg_46;
    BindingList arg_47;
    SymbolType arg_48;
    TermType arg_49;
    StringTableKey arg_50;
    StringTableKey arg_51;
    BindingList arg_52;
    BindingList arg_53;
    BindingList arg_54;
    AttributeDir arg_55;
    D_typePtr arg_56;
    StringTableKeyList arg_57;
    D_SymbolComputations arg_58;
    int arg_59;
    int arg_60;
    D_RuleComputations arg_61;
    D_VirtAttrs arg_62;
    PTGNode arg_63;
    PTGNodes arg_64;
    BindingList arg_65;
    DefTableEntry(); 
    ~DefTableEntry() = default; 
  };
  std::unordered_map<DefTableKey, DefTableEntry> entries; 
public: 
  DefTable() = default; 
  ~DefTable() = default; 
  friend DefTableKey LNGCloneKey(DefTableKey k); 
  void set_arg_0(DefTableKey k, CoordPtr arg);
  bool has_arg_0(DefTableKey k); 
  CoordPtr get_arg_0(DefTableKey k, CoordPtr value); 
  
  void set_arg_1(DefTableKey k, PTGNode arg);
  bool has_arg_1(DefTableKey k); 
  PTGNode get_arg_1(DefTableKey k, PTGNode value); 
  
  void set_arg_2(DefTableKey k, int arg);
  bool has_arg_2(DefTableKey k); 
  int get_arg_2(DefTableKey k, int value); 
  
  void set_arg_3(DefTableKey k, TypingKind arg);
  bool has_arg_3(DefTableKey k); 
  TypingKind get_arg_3(DefTableKey k, TypingKind value); 
  
  void set_arg_4(DefTableKey k, Binding arg);
  bool has_arg_4(DefTableKey k); 
  Binding get_arg_4(DefTableKey k, Binding value); 
  
  void set_arg_5(DefTableKey k, StringTableKeyList arg);
  bool has_arg_5(DefTableKey k); 
  StringTableKeyList get_arg_5(DefTableKey k, StringTableKeyList value); 
  
  void set_arg_6(DefTableKey k, int arg);
  bool has_arg_6(DefTableKey k); 
  int get_arg_6(DefTableKey k, int value); 
  
  void set_arg_7(DefTableKey k, PTGNode arg);
  bool has_arg_7(DefTableKey k); 
  PTGNode get_arg_7(DefTableKey k, PTGNode value); 
  
  void set_arg_8(DefTableKey k, PTGNode arg);
  bool has_arg_8(DefTableKey k); 
  PTGNode get_arg_8(DefTableKey k, PTGNode value); 
  
  void set_arg_9(DefTableKey k, int arg);
  bool has_arg_9(DefTableKey k); 
  int get_arg_9(DefTableKey k, int value); 
  
  void set_arg_10(DefTableKey k, StringTableKey arg);
  bool has_arg_10(DefTableKey k); 
  StringTableKey get_arg_10(DefTableKey k, StringTableKey value); 
  
  void set_arg_11(DefTableKey k, StringTableKey arg);
  bool has_arg_11(DefTableKey k); 
  StringTableKey get_arg_11(DefTableKey k, StringTableKey value); 
  
  void set_arg_12(DefTableKey k, StringTableKeyList arg);
  bool has_arg_12(DefTableKey k); 
  StringTableKeyList get_arg_12(DefTableKey k, StringTableKeyList value); 
  
  void set_arg_13(DefTableKey k, bool arg);
  bool has_arg_13(DefTableKey k); 
  bool get_arg_13(DefTableKey k, bool value); 
  
  void set_arg_14(DefTableKey k, StringTableKeyList arg);
  bool has_arg_14(DefTableKey k); 
  StringTableKeyList get_arg_14(DefTableKey k, StringTableKeyList value); 
  
  void set_arg_15(DefTableKey k, int arg);
  bool has_arg_15(DefTableKey k); 
  int get_arg_15(DefTableKey k, int value); 
  
  void set_arg_16(DefTableKey k, StringTableKey arg);
  bool has_arg_16(DefTableKey k); 
  StringTableKey get_arg_16(DefTableKey k, StringTableKey value); 
  
  void set_arg_17(DefTableKey k, int arg);
  bool has_arg_17(DefTableKey k); 
  int get_arg_17(DefTableKey k, int value); 
  
  void set_arg_18(DefTableKey k, StringTableKey arg);
  bool has_arg_18(DefTableKey k); 
  StringTableKey get_arg_18(DefTableKey k, StringTableKey value); 
  
  void set_arg_19(DefTableKey k, StringTableKey arg);
  bool has_arg_19(DefTableKey k); 
  StringTableKey get_arg_19(DefTableKey k, StringTableKey value); 
  
  void set_arg_20(DefTableKey k, Environment arg);
  bool has_arg_20(DefTableKey k); 
  Environment get_arg_20(DefTableKey k, Environment value); 
  
  void set_arg_21(DefTableKey k, bool arg);
  bool has_arg_21(DefTableKey k); 
  bool get_arg_21(DefTableKey k, bool value); 
  
  void set_arg_22(DefTableKey k, int arg);
  bool has_arg_22(DefTableKey k); 
  int get_arg_22(DefTableKey k, int value); 
  
  void set_arg_23(DefTableKey k, StringTableKey arg);
  bool has_arg_23(DefTableKey k); 
  StringTableKey get_arg_23(DefTableKey k, StringTableKey value); 
  
  void set_arg_24(DefTableKey k, bool arg);
  bool has_arg_24(DefTableKey k); 
  bool get_arg_24(DefTableKey k, bool value); 
  
  void set_arg_25(DefTableKey k, StringTableKey arg);
  bool has_arg_25(DefTableKey k); 
  StringTableKey get_arg_25(DefTableKey k, StringTableKey value); 
  
  void set_arg_26(DefTableKey k, StringTableKeyList arg);
  bool has_arg_26(DefTableKey k); 
  StringTableKeyList get_arg_26(DefTableKey k, StringTableKeyList value); 
  
  void set_arg_27(DefTableKey k, D_patternLists arg);
  bool has_arg_27(DefTableKey k); 
  D_patternLists get_arg_27(DefTableKey k, D_patternLists value); 
  
  void set_arg_28(DefTableKey k, bool arg);
  bool has_arg_28(DefTableKey k); 
  bool get_arg_28(DefTableKey k, bool value); 
  
  void set_arg_29(DefTableKey k, D_types arg);
  bool has_arg_29(DefTableKey k); 
  D_types get_arg_29(DefTableKey k, D_types value); 
  
  void set_arg_30(DefTableKey k, Environment arg);
  bool has_arg_30(DefTableKey k); 
  Environment get_arg_30(DefTableKey k, Environment value); 
  
  void set_arg_31(DefTableKey k, D_typePtr arg);
  bool has_arg_31(DefTableKey k); 
  D_typePtr get_arg_31(DefTableKey k, D_typePtr value); 
  
  void set_arg_32(DefTableKey k, Binding arg);
  bool has_arg_32(DefTableKey k); 
  Binding get_arg_32(DefTableKey k, Binding value); 
  
  void set_arg_33(DefTableKey k, bool arg);
  bool has_arg_33(DefTableKey k); 
  bool get_arg_33(DefTableKey k, bool value); 
  
  void set_arg_34(DefTableKey k, D_scheme arg);
  bool has_arg_34(DefTableKey k); 
  D_scheme get_arg_34(DefTableKey k, D_scheme value); 
  
  void set_arg_35(DefTableKey k, int arg);
  bool has_arg_35(DefTableKey k); 
  int get_arg_35(DefTableKey k, int value); 
  
  void set_arg_36(DefTableKey k, int arg);
  bool has_arg_36(DefTableKey k); 
  int get_arg_36(DefTableKey k, int value); 
  
  void set_arg_37(DefTableKey k, int arg);
  bool has_arg_37(DefTableKey k); 
  int get_arg_37(DefTableKey k, int value); 
  
  void set_arg_38(DefTableKey k, PTGNode arg);
  bool has_arg_38(DefTableKey k); 
  PTGNode get_arg_38(DefTableKey k, PTGNode value); 
  
  void set_arg_39(DefTableKey k, D_PatternCodes arg);
  bool has_arg_39(DefTableKey k); 
  D_PatternCodes get_arg_39(DefTableKey k, D_PatternCodes value); 
  
  void set_arg_40(DefTableKey k, bool arg);
  bool has_arg_40(DefTableKey k); 
  bool get_arg_40(DefTableKey k, bool value); 
  
  void set_arg_41(DefTableKey k, int arg);
  bool has_arg_41(DefTableKey k); 
  int get_arg_41(DefTableKey k, int value); 
  
  void set_arg_42(DefTableKey k, Environment arg);
  bool has_arg_42(DefTableKey k); 
  Environment get_arg_42(DefTableKey k, Environment value); 
  
  void set_arg_43(DefTableKey k, Environment arg);
  bool has_arg_43(DefTableKey k); 
  Environment get_arg_43(DefTableKey k, Environment value); 
  
  void set_arg_44(DefTableKey k, Binding arg);
  bool has_arg_44(DefTableKey k); 
  Binding get_arg_44(DefTableKey k, Binding value); 
  
  void set_arg_45(DefTableKey k, StringTableKey arg);
  bool has_arg_45(DefTableKey k); 
  StringTableKey get_arg_45(DefTableKey k, StringTableKey value); 
  
  void set_arg_46(DefTableKey k, D_ProdSymbols arg);
  bool has_arg_46(DefTableKey k); 
  D_ProdSymbols get_arg_46(DefTableKey k, D_ProdSymbols value); 
  
  void set_arg_47(DefTableKey k, BindingList arg);
  bool has_arg_47(DefTableKey k); 
  BindingList get_arg_47(DefTableKey k, BindingList value); 
  
  void set_arg_48(DefTableKey k, SymbolType arg);
  bool has_arg_48(DefTableKey k); 
  SymbolType get_arg_48(DefTableKey k, SymbolType value); 
  
  void set_arg_49(DefTableKey k, TermType arg);
  bool has_arg_49(DefTableKey k); 
  TermType get_arg_49(DefTableKey k, TermType value); 
  
  void set_arg_50(DefTableKey k, StringTableKey arg);
  bool has_arg_50(DefTableKey k); 
  StringTableKey get_arg_50(DefTableKey k, StringTableKey value); 
  
  void set_arg_51(DefTableKey k, StringTableKey arg);
  bool has_arg_51(DefTableKey k); 
  StringTableKey get_arg_51(DefTableKey k, StringTableKey value); 
  
  void set_arg_52(DefTableKey k, BindingList arg);
  bool has_arg_52(DefTableKey k); 
  BindingList get_arg_52(DefTableKey k, BindingList value); 
  
  void set_arg_53(DefTableKey k, BindingList arg);
  bool has_arg_53(DefTableKey k); 
  BindingList get_arg_53(DefTableKey k, BindingList value); 
  
  void set_arg_54(DefTableKey k, BindingList arg);
  bool has_arg_54(DefTableKey k); 
  BindingList get_arg_54(DefTableKey k, BindingList value); 
  
  void set_arg_55(DefTableKey k, AttributeDir arg);
  bool has_arg_55(DefTableKey k); 
  AttributeDir get_arg_55(DefTableKey k, AttributeDir value); 
  
  void set_arg_56(DefTableKey k, D_typePtr arg);
  bool has_arg_56(DefTableKey k); 
  D_typePtr get_arg_56(DefTableKey k, D_typePtr value); 
  
  void set_arg_57(DefTableKey k, StringTableKeyList arg);
  bool has_arg_57(DefTableKey k); 
  StringTableKeyList get_arg_57(DefTableKey k, StringTableKeyList value); 
  
  void set_arg_58(DefTableKey k, D_SymbolComputations arg);
  bool has_arg_58(DefTableKey k); 
  D_SymbolComputations get_arg_58(DefTableKey k, D_SymbolComputations value); 
  
  void set_arg_59(DefTableKey k, int arg);
  bool has_arg_59(DefTableKey k); 
  int get_arg_59(DefTableKey k, int value); 
  
  void set_arg_60(DefTableKey k, int arg);
  bool has_arg_60(DefTableKey k); 
  int get_arg_60(DefTableKey k, int value); 
  
  void set_arg_61(DefTableKey k, D_RuleComputations arg);
  bool has_arg_61(DefTableKey k); 
  D_RuleComputations get_arg_61(DefTableKey k, D_RuleComputations value); 
  
  void set_arg_62(DefTableKey k, D_VirtAttrs arg);
  bool has_arg_62(DefTableKey k); 
  D_VirtAttrs get_arg_62(DefTableKey k, D_VirtAttrs value); 
  
  void set_arg_63(DefTableKey k, PTGNode arg);
  bool has_arg_63(DefTableKey k); 
  PTGNode get_arg_63(DefTableKey k, PTGNode value); 
  
  void set_arg_64(DefTableKey k, PTGNodes arg);
  bool has_arg_64(DefTableKey k); 
  PTGNodes get_arg_64(DefTableKey k, PTGNodes value); 
  
  void set_arg_65(DefTableKey k, BindingList arg);
  bool has_arg_65(DefTableKey k); 
  BindingList get_arg_65(DefTableKey k, BindingList value); 
  
}; 
extern DefTable definitions;
extern void ResetPos(DefTableKey k, CoordPtr v);
extern CoordPtr GetPos(DefTableKey k, CoordPtr v);
extern bool HasPos(DefTableKey k);
extern void IsPos(DefTableKey k, CoordPtr which, CoordPtr err);
extern void SetPos(DefTableKey k, CoordPtr add, CoordPtr replace);
extern void UniquePos(DefTableKey k, std::function<CoordPtr(void)> next);

extern void ResetPDL_Type(DefTableKey k, PTGNode v);
extern PTGNode GetPDL_Type(DefTableKey k, PTGNode v);
extern bool HasPDL_Type(DefTableKey k);
extern void IsPDL_Type(DefTableKey k, PTGNode which, PTGNode err);
extern void SetPDL_Type(DefTableKey k, PTGNode add, PTGNode replace);
extern void UniquePDL_Type(DefTableKey k, std::function<PTGNode(void)> next);

extern void ResetPDL_PropertyNumber(DefTableKey k, int v);
extern int GetPDL_PropertyNumber(DefTableKey k, int v);
extern bool HasPDL_PropertyNumber(DefTableKey k);
extern void IsPDL_PropertyNumber(DefTableKey k, int which, int err);
extern void SetPDL_PropertyNumber(DefTableKey k, int add, int replace);
extern void UniquePDL_PropertyNumber(DefTableKey k, std::function<int(void)> next);

extern void ResetT_Type(DefTableKey k, TypingKind v);
extern TypingKind GetT_Type(DefTableKey k, TypingKind v);
extern bool HasT_Type(DefTableKey k);
extern void IsT_Type(DefTableKey k, TypingKind which, TypingKind err);
extern void SetT_Type(DefTableKey k, TypingKind add, TypingKind replace);
extern void UniqueT_Type(DefTableKey k, std::function<TypingKind(void)> next);

extern void ResetD_Parent(DefTableKey k, Binding v);
extern Binding GetD_Parent(DefTableKey k, Binding v);
extern bool HasD_Parent(DefTableKey k);
extern void IsD_Parent(DefTableKey k, Binding which, Binding err);
extern void SetD_Parent(DefTableKey k, Binding add, Binding replace);
extern void UniqueD_Parent(DefTableKey k, std::function<Binding(void)> next);

extern void ResetD_ConNames(DefTableKey k, StringTableKeyList v);
extern StringTableKeyList GetD_ConNames(DefTableKey k, StringTableKeyList v);
extern bool HasD_ConNames(DefTableKey k);
extern void IsD_ConNames(DefTableKey k, StringTableKeyList which, StringTableKeyList err);
extern void SetD_ConNames(DefTableKey k, StringTableKeyList add, StringTableKeyList replace);
extern void UniqueD_ConNames(DefTableKey k, std::function<StringTableKeyList(void)> next);

extern void ResetT_Listindex(DefTableKey k, int v);
extern int GetT_Listindex(DefTableKey k, int v);
extern bool HasT_Listindex(DefTableKey k);
extern void IsT_Listindex(DefTableKey k, int which, int err);
extern void SetT_Listindex(DefTableKey k, int add, int replace);
extern void UniqueT_Listindex(DefTableKey k, std::function<int(void)> next);

extern void ResetT_DeclCode(DefTableKey k, PTGNode v);
extern PTGNode GetT_DeclCode(DefTableKey k, PTGNode v);
extern bool HasT_DeclCode(DefTableKey k);
extern void IsT_DeclCode(DefTableKey k, PTGNode which, PTGNode err);
extern void SetT_DeclCode(DefTableKey k, PTGNode add, PTGNode replace);
extern void UniqueT_DeclCode(DefTableKey k, std::function<PTGNode(void)> next);

extern void ResetT_NameCode(DefTableKey k, PTGNode v);
extern PTGNode GetT_NameCode(DefTableKey k, PTGNode v);
extern bool HasT_NameCode(DefTableKey k);
extern void IsT_NameCode(DefTableKey k, PTGNode which, PTGNode err);
extern void SetT_NameCode(DefTableKey k, PTGNode add, PTGNode replace);
extern void UniqueT_NameCode(DefTableKey k, std::function<PTGNode(void)> next);

extern void ResetT_MapNumber(DefTableKey k, int v);
extern int GetT_MapNumber(DefTableKey k, int v);
extern bool HasT_MapNumber(DefTableKey k);
extern void IsT_MapNumber(DefTableKey k, int which, int err);
extern void SetT_MapNumber(DefTableKey k, int add, int replace);
extern void UniqueT_MapNumber(DefTableKey k, std::function<int(void)> next);

extern void ResetNT_Inverse(DefTableKey k, StringTableKey v);
extern StringTableKey GetNT_Inverse(DefTableKey k, StringTableKey v);
extern bool HasNT_Inverse(DefTableKey k);
extern void IsNT_Inverse(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetNT_Inverse(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueNT_Inverse(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetNT_BaseType(DefTableKey k, StringTableKey v);
extern StringTableKey GetNT_BaseType(DefTableKey k, StringTableKey v);
extern bool HasNT_BaseType(DefTableKey k);
extern void IsNT_BaseType(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetNT_BaseType(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueNT_BaseType(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetDC_ArgumentTypes(DefTableKey k, StringTableKeyList v);
extern StringTableKeyList GetDC_ArgumentTypes(DefTableKey k, StringTableKeyList v);
extern bool HasDC_ArgumentTypes(DefTableKey k);
extern void IsDC_ArgumentTypes(DefTableKey k, StringTableKeyList which, StringTableKeyList err);
extern void SetDC_ArgumentTypes(DefTableKey k, StringTableKeyList add, StringTableKeyList replace);
extern void UniqueDC_ArgumentTypes(DefTableKey k, std::function<StringTableKeyList(void)> next);

extern void ResetCode_DoGen(DefTableKey k, bool v);
extern bool GetCode_DoGen(DefTableKey k, bool v);
extern bool HasCode_DoGen(DefTableKey k);
extern void IsCode_DoGen(DefTableKey k, bool which, bool err);
extern void SetCode_DoGen(DefTableKey k, bool add, bool replace);
extern void UniqueCode_DoGen(DefTableKey k, std::function<bool(void)> next);

extern void ResetTU_Args(DefTableKey k, StringTableKeyList v);
extern StringTableKeyList GetTU_Args(DefTableKey k, StringTableKeyList v);
extern bool HasTU_Args(DefTableKey k);
extern void IsTU_Args(DefTableKey k, StringTableKeyList which, StringTableKeyList err);
extern void SetTU_Args(DefTableKey k, StringTableKeyList add, StringTableKeyList replace);
extern void UniqueTU_Args(DefTableKey k, std::function<StringTableKeyList(void)> next);

extern void ResetT_FunIndex(DefTableKey k, int v);
extern int GetT_FunIndex(DefTableKey k, int v);
extern bool HasT_FunIndex(DefTableKey k);
extern void IsT_FunIndex(DefTableKey k, int which, int err);
extern void SetT_FunIndex(DefTableKey k, int add, int replace);
extern void UniqueT_FunIndex(DefTableKey k, std::function<int(void)> next);

extern void ResetL_Type(DefTableKey k, StringTableKey v);
extern StringTableKey GetL_Type(DefTableKey k, StringTableKey v);
extern bool HasL_Type(DefTableKey k);
extern void IsL_Type(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetL_Type(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueL_Type(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetT_TupleNumber(DefTableKey k, int v);
extern int GetT_TupleNumber(DefTableKey k, int v);
extern bool HasT_TupleNumber(DefTableKey k);
extern void IsT_TupleNumber(DefTableKey k, int which, int err);
extern void SetT_TupleNumber(DefTableKey k, int add, int replace);
extern void UniqueT_TupleNumber(DefTableKey k, std::function<int(void)> next);

extern void ResetM_Key(DefTableKey k, StringTableKey v);
extern StringTableKey GetM_Key(DefTableKey k, StringTableKey v);
extern bool HasM_Key(DefTableKey k);
extern void IsM_Key(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetM_Key(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueM_Key(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetM_Value(DefTableKey k, StringTableKey v);
extern StringTableKey GetM_Value(DefTableKey k, StringTableKey v);
extern bool HasM_Value(DefTableKey k);
extern void IsM_Value(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetM_Value(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueM_Value(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetD_RecordArgs(DefTableKey k, Environment v);
extern Environment GetD_RecordArgs(DefTableKey k, Environment v);
extern bool HasD_RecordArgs(DefTableKey k);
extern void IsD_RecordArgs(DefTableKey k, Environment which, Environment err);
extern void SetD_RecordArgs(DefTableKey k, Environment add, Environment replace);
extern void UniqueD_RecordArgs(DefTableKey k, std::function<Environment(void)> next);

extern void ResetD_Templated(DefTableKey k, bool v);
extern bool GetD_Templated(DefTableKey k, bool v);
extern bool HasD_Templated(DefTableKey k);
extern void IsD_Templated(DefTableKey k, bool which, bool err);
extern void SetD_Templated(DefTableKey k, bool add, bool replace);
extern void UniqueD_Templated(DefTableKey k, std::function<bool(void)> next);

extern void ResetRA_Index(DefTableKey k, int v);
extern int GetRA_Index(DefTableKey k, int v);
extern bool HasRA_Index(DefTableKey k);
extern void IsRA_Index(DefTableKey k, int which, int err);
extern void SetRA_Index(DefTableKey k, int add, int replace);
extern void UniqueRA_Index(DefTableKey k, std::function<int(void)> next);

extern void ResetRA_Type(DefTableKey k, StringTableKey v);
extern StringTableKey GetRA_Type(DefTableKey k, StringTableKey v);
extern bool HasRA_Type(DefTableKey k);
extern void IsRA_Type(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetRA_Type(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueRA_Type(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetF_DoGen(DefTableKey k, bool v);
extern bool GetF_DoGen(DefTableKey k, bool v);
extern bool HasF_DoGen(DefTableKey k);
extern void IsF_DoGen(DefTableKey k, bool which, bool err);
extern void SetF_DoGen(DefTableKey k, bool add, bool replace);
extern void UniqueF_DoGen(DefTableKey k, std::function<bool(void)> next);

extern void ResetF_ReturnType(DefTableKey k, StringTableKey v);
extern StringTableKey GetF_ReturnType(DefTableKey k, StringTableKey v);
extern bool HasF_ReturnType(DefTableKey k);
extern void IsF_ReturnType(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetF_ReturnType(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueF_ReturnType(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetF_Arguments(DefTableKey k, StringTableKeyList v);
extern StringTableKeyList GetF_Arguments(DefTableKey k, StringTableKeyList v);
extern bool HasF_Arguments(DefTableKey k);
extern void IsF_Arguments(DefTableKey k, StringTableKeyList which, StringTableKeyList err);
extern void SetF_Arguments(DefTableKey k, StringTableKeyList add, StringTableKeyList replace);
extern void UniqueF_Arguments(DefTableKey k, std::function<StringTableKeyList(void)> next);

extern void ResetF_Patterns(DefTableKey k, D_patternLists v);
extern D_patternLists GetF_Patterns(DefTableKey k, D_patternLists v);
extern bool HasF_Patterns(DefTableKey k);
extern void IsF_Patterns(DefTableKey k, D_patternLists which, D_patternLists err);
extern void SetF_Patterns(DefTableKey k, D_patternLists add, D_patternLists replace);
extern void UniqueF_Patterns(DefTableKey k, std::function<D_patternLists(void)> next);

extern void ResetF_HasErrs(DefTableKey k, bool v);
extern bool GetF_HasErrs(DefTableKey k, bool v);
extern bool HasF_HasErrs(DefTableKey k);
extern void IsF_HasErrs(DefTableKey k, bool which, bool err);
extern void SetF_HasErrs(DefTableKey k, bool add, bool replace);
extern void UniqueF_HasErrs(DefTableKey k, std::function<bool(void)> next);

extern void ResetF_PatternTypes(DefTableKey k, D_types v);
extern D_types GetF_PatternTypes(DefTableKey k, D_types v);
extern bool HasF_PatternTypes(DefTableKey k);
extern void IsF_PatternTypes(DefTableKey k, D_types which, D_types err);
extern void SetF_PatternTypes(DefTableKey k, D_types add, D_types replace);
extern void UniqueF_PatternTypes(DefTableKey k, std::function<D_types(void)> next);

extern void ResetF_TypeEnv(DefTableKey k, Environment v);
extern Environment GetF_TypeEnv(DefTableKey k, Environment v);
extern bool HasF_TypeEnv(DefTableKey k);
extern void IsF_TypeEnv(DefTableKey k, Environment which, Environment err);
extern void SetF_TypeEnv(DefTableKey k, Environment add, Environment replace);
extern void UniqueF_TypeEnv(DefTableKey k, std::function<Environment(void)> next);

extern void ResetFA_Type(DefTableKey k, D_typePtr v);
extern D_typePtr GetFA_Type(DefTableKey k, D_typePtr v);
extern bool HasFA_Type(DefTableKey k);
extern void IsFA_Type(DefTableKey k, D_typePtr which, D_typePtr err);
extern void SetFA_Type(DefTableKey k, D_typePtr add, D_typePtr replace);
extern void UniqueFA_Type(DefTableKey k, std::function<D_typePtr(void)> next);

extern void ResetFV_Argument(DefTableKey k, Binding v);
extern Binding GetFV_Argument(DefTableKey k, Binding v);
extern bool HasFV_Argument(DefTableKey k);
extern void IsFV_Argument(DefTableKey k, Binding which, Binding err);
extern void SetFV_Argument(DefTableKey k, Binding add, Binding replace);
extern void UniqueFV_Argument(DefTableKey k, std::function<Binding(void)> next);

extern void ResetFV_Head(DefTableKey k, bool v);
extern bool GetFV_Head(DefTableKey k, bool v);
extern bool HasFV_Head(DefTableKey k);
extern void IsFV_Head(DefTableKey k, bool which, bool err);
extern void SetFV_Head(DefTableKey k, bool add, bool replace);
extern void UniqueFV_Head(DefTableKey k, std::function<bool(void)> next);

extern void ResetFV_Replacements(DefTableKey k, D_scheme v);
extern D_scheme GetFV_Replacements(DefTableKey k, D_scheme v);
extern bool HasFV_Replacements(DefTableKey k);
extern void IsFV_Replacements(DefTableKey k, D_scheme which, D_scheme err);
extern void SetFV_Replacements(DefTableKey k, D_scheme add, D_scheme replace);
extern void UniqueFV_Replacements(DefTableKey k, std::function<D_scheme(void)> next);

extern void ResetFV_CurrInd(DefTableKey k, int v);
extern int GetFV_CurrInd(DefTableKey k, int v);
extern bool HasFV_CurrInd(DefTableKey k);
extern void IsFV_CurrInd(DefTableKey k, int which, int err);
extern void SetFV_CurrInd(DefTableKey k, int add, int replace);
extern void UniqueFV_CurrInd(DefTableKey k, std::function<int(void)> next);

extern void ResetFV_TuInd(DefTableKey k, int v);
extern int GetFV_TuInd(DefTableKey k, int v);
extern bool HasFV_TuInd(DefTableKey k);
extern void IsFV_TuInd(DefTableKey k, int which, int err);
extern void SetFV_TuInd(DefTableKey k, int add, int replace);
extern void UniqueFV_TuInd(DefTableKey k, std::function<int(void)> next);

extern void ResetF_PatternCount(DefTableKey k, int v);
extern int GetF_PatternCount(DefTableKey k, int v);
extern bool HasF_PatternCount(DefTableKey k);
extern void IsF_PatternCount(DefTableKey k, int which, int err);
extern void SetF_PatternCount(DefTableKey k, int add, int replace);
extern void UniqueF_PatternCount(DefTableKey k, std::function<int(void)> next);

extern void ResetFV_ExprCode(DefTableKey k, PTGNode v);
extern PTGNode GetFV_ExprCode(DefTableKey k, PTGNode v);
extern bool HasFV_ExprCode(DefTableKey k);
extern void IsFV_ExprCode(DefTableKey k, PTGNode which, PTGNode err);
extern void SetFV_ExprCode(DefTableKey k, PTGNode add, PTGNode replace);
extern void UniqueFV_ExprCode(DefTableKey k, std::function<PTGNode(void)> next);

extern void ResetF_PatternCodes(DefTableKey k, D_PatternCodes v);
extern D_PatternCodes GetF_PatternCodes(DefTableKey k, D_PatternCodes v);
extern bool HasF_PatternCodes(DefTableKey k);
extern void IsF_PatternCodes(DefTableKey k, D_PatternCodes which, D_PatternCodes err);
extern void SetF_PatternCodes(DefTableKey k, D_PatternCodes add, D_PatternCodes replace);
extern void UniqueF_PatternCodes(DefTableKey k, std::function<D_PatternCodes(void)> next);

extern void ResetIsClass(DefTableKey k, bool v);
extern bool GetIsClass(DefTableKey k, bool v);
extern bool HasIsClass(DefTableKey k);
extern void IsIsClass(DefTableKey k, bool which, bool err);
extern void SetIsClass(DefTableKey k, bool add, bool replace);
extern void UniqueIsClass(DefTableKey k, std::function<bool(void)> next);

extern void ResetA_GenRules(DefTableKey k, int v);
extern int GetA_GenRules(DefTableKey k, int v);
extern bool HasA_GenRules(DefTableKey k);
extern void IsA_GenRules(DefTableKey k, int which, int err);
extern void SetA_GenRules(DefTableKey k, int add, int replace);
extern void UniqueA_GenRules(DefTableKey k, std::function<int(void)> next);

extern void ResetA_Rules(DefTableKey k, Environment v);
extern Environment GetA_Rules(DefTableKey k, Environment v);
extern bool HasA_Rules(DefTableKey k);
extern void IsA_Rules(DefTableKey k, Environment which, Environment err);
extern void SetA_Rules(DefTableKey k, Environment add, Environment replace);
extern void UniqueA_Rules(DefTableKey k, std::function<Environment(void)> next);

extern void ResetA_Symbols(DefTableKey k, Environment v);
extern Environment GetA_Symbols(DefTableKey k, Environment v);
extern bool HasA_Symbols(DefTableKey k);
extern void IsA_Symbols(DefTableKey k, Environment which, Environment err);
extern void SetA_Symbols(DefTableKey k, Environment add, Environment replace);
extern void UniqueA_Symbols(DefTableKey k, std::function<Environment(void)> next);

extern void ResetP_TreeOf(DefTableKey k, Binding v);
extern Binding GetP_TreeOf(DefTableKey k, Binding v);
extern bool HasP_TreeOf(DefTableKey k);
extern void IsP_TreeOf(DefTableKey k, Binding which, Binding err);
extern void SetP_TreeOf(DefTableKey k, Binding add, Binding replace);
extern void UniqueP_TreeOf(DefTableKey k, std::function<Binding(void)> next);

extern void ResetP_LHS(DefTableKey k, StringTableKey v);
extern StringTableKey GetP_LHS(DefTableKey k, StringTableKey v);
extern bool HasP_LHS(DefTableKey k);
extern void IsP_LHS(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetP_LHS(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueP_LHS(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetP_RHS(DefTableKey k, D_ProdSymbols v);
extern D_ProdSymbols GetP_RHS(DefTableKey k, D_ProdSymbols v);
extern bool HasP_RHS(DefTableKey k);
extern void IsP_RHS(DefTableKey k, D_ProdSymbols which, D_ProdSymbols err);
extern void SetP_RHS(DefTableKey k, D_ProdSymbols add, D_ProdSymbols replace);
extern void UniqueP_RHS(DefTableKey k, std::function<D_ProdSymbols(void)> next);

extern void ResetS_Trees(DefTableKey k, BindingList v);
extern BindingList GetS_Trees(DefTableKey k, BindingList v);
extern bool HasS_Trees(DefTableKey k);
extern void IsS_Trees(DefTableKey k, BindingList which, BindingList err);
extern void SetS_Trees(DefTableKey k, BindingList add, BindingList replace);
extern void UniqueS_Trees(DefTableKey k, std::function<BindingList(void)> next);

extern void ResetS_Type(DefTableKey k, SymbolType v);
extern SymbolType GetS_Type(DefTableKey k, SymbolType v);
extern bool HasS_Type(DefTableKey k);
extern void IsS_Type(DefTableKey k, SymbolType which, SymbolType err);
extern void SetS_Type(DefTableKey k, SymbolType add, SymbolType replace);
extern void UniqueS_Type(DefTableKey k, std::function<SymbolType(void)> next);

extern void ResetS_Term(DefTableKey k, TermType v);
extern TermType GetS_Term(DefTableKey k, TermType v);
extern bool HasS_Term(DefTableKey k);
extern void IsS_Term(DefTableKey k, TermType which, TermType err);
extern void SetS_Term(DefTableKey k, TermType add, TermType replace);
extern void UniqueS_Term(DefTableKey k, std::function<TermType(void)> next);

extern void ResetTermType(DefTableKey k, StringTableKey v);
extern StringTableKey GetTermType(DefTableKey k, StringTableKey v);
extern bool HasTermType(DefTableKey k);
extern void IsTermType(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetTermType(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueTermType(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetTermConversion(DefTableKey k, StringTableKey v);
extern StringTableKey GetTermConversion(DefTableKey k, StringTableKey v);
extern bool HasTermConversion(DefTableKey k);
extern void IsTermConversion(DefTableKey k, StringTableKey which, StringTableKey err);
extern void SetTermConversion(DefTableKey k, StringTableKey add, StringTableKey replace);
extern void UniqueTermConversion(DefTableKey k, std::function<StringTableKey(void)> next);

extern void ResetS_Rules(DefTableKey k, BindingList v);
extern BindingList GetS_Rules(DefTableKey k, BindingList v);
extern bool HasS_Rules(DefTableKey k);
extern void IsS_Rules(DefTableKey k, BindingList which, BindingList err);
extern void SetS_Rules(DefTableKey k, BindingList add, BindingList replace);
extern void UniqueS_Rules(DefTableKey k, std::function<BindingList(void)> next);

extern void ResetA_Inherited(DefTableKey k, BindingList v);
extern BindingList GetA_Inherited(DefTableKey k, BindingList v);
extern bool HasA_Inherited(DefTableKey k);
extern void IsA_Inherited(DefTableKey k, BindingList which, BindingList err);
extern void SetA_Inherited(DefTableKey k, BindingList add, BindingList replace);
extern void UniqueA_Inherited(DefTableKey k, std::function<BindingList(void)> next);

extern void ResetA_Inheriting(DefTableKey k, BindingList v);
extern BindingList GetA_Inheriting(DefTableKey k, BindingList v);
extern bool HasA_Inheriting(DefTableKey k);
extern void IsA_Inheriting(DefTableKey k, BindingList which, BindingList err);
extern void SetA_Inheriting(DefTableKey k, BindingList add, BindingList replace);
extern void UniqueA_Inheriting(DefTableKey k, std::function<BindingList(void)> next);

extern void ResetAT_Direction(DefTableKey k, AttributeDir v);
extern AttributeDir GetAT_Direction(DefTableKey k, AttributeDir v);
extern bool HasAT_Direction(DefTableKey k);
extern void IsAT_Direction(DefTableKey k, AttributeDir which, AttributeDir err);
extern void SetAT_Direction(DefTableKey k, AttributeDir add, AttributeDir replace);
extern void UniqueAT_Direction(DefTableKey k, std::function<AttributeDir(void)> next);

extern void ResetAT_Type(DefTableKey k, D_typePtr v);
extern D_typePtr GetAT_Type(DefTableKey k, D_typePtr v);
extern bool HasAT_Type(DefTableKey k);
extern void IsAT_Type(DefTableKey k, D_typePtr which, D_typePtr err);
extern void SetAT_Type(DefTableKey k, D_typePtr add, D_typePtr replace);
extern void UniqueAT_Type(DefTableKey k, std::function<D_typePtr(void)> next);

extern void ResetS_LocalAttributes(DefTableKey k, StringTableKeyList v);
extern StringTableKeyList GetS_LocalAttributes(DefTableKey k, StringTableKeyList v);
extern bool HasS_LocalAttributes(DefTableKey k);
extern void IsS_LocalAttributes(DefTableKey k, StringTableKeyList which, StringTableKeyList err);
extern void SetS_LocalAttributes(DefTableKey k, StringTableKeyList add, StringTableKeyList replace);
extern void UniqueS_LocalAttributes(DefTableKey k, std::function<StringTableKeyList(void)> next);

extern void ResetS_Computations(DefTableKey k, D_SymbolComputations v);
extern D_SymbolComputations GetS_Computations(DefTableKey k, D_SymbolComputations v);
extern bool HasS_Computations(DefTableKey k);
extern void IsS_Computations(DefTableKey k, D_SymbolComputations which, D_SymbolComputations err);
extern void SetS_Computations(DefTableKey k, D_SymbolComputations add, D_SymbolComputations replace);
extern void UniqueS_Computations(DefTableKey k, std::function<D_SymbolComputations(void)> next);

extern void ResetV_Index(DefTableKey k, int v);
extern int GetV_Index(DefTableKey k, int v);
extern bool HasV_Index(DefTableKey k);
extern void IsV_Index(DefTableKey k, int which, int err);
extern void SetV_Index(DefTableKey k, int add, int replace);
extern void UniqueV_Index(DefTableKey k, std::function<int(void)> next);

extern void ResetV_SubIndex(DefTableKey k, int v);
extern int GetV_SubIndex(DefTableKey k, int v);
extern bool HasV_SubIndex(DefTableKey k);
extern void IsV_SubIndex(DefTableKey k, int which, int err);
extern void SetV_SubIndex(DefTableKey k, int add, int replace);
extern void UniqueV_SubIndex(DefTableKey k, std::function<int(void)> next);

extern void ResetR_Computations(DefTableKey k, D_RuleComputations v);
extern D_RuleComputations GetR_Computations(DefTableKey k, D_RuleComputations v);
extern bool HasR_Computations(DefTableKey k);
extern void IsR_Computations(DefTableKey k, D_RuleComputations which, D_RuleComputations err);
extern void SetR_Computations(DefTableKey k, D_RuleComputations add, D_RuleComputations replace);
extern void UniqueR_Computations(DefTableKey k, std::function<D_RuleComputations(void)> next);

extern void ResetS_GenVoids(DefTableKey k, D_VirtAttrs v);
extern D_VirtAttrs GetS_GenVoids(DefTableKey k, D_VirtAttrs v);
extern bool HasS_GenVoids(DefTableKey k);
extern void IsS_GenVoids(DefTableKey k, D_VirtAttrs which, D_VirtAttrs err);
extern void SetS_GenVoids(DefTableKey k, D_VirtAttrs add, D_VirtAttrs replace);
extern void UniqueS_GenVoids(DefTableKey k, std::function<D_VirtAttrs(void)> next);

extern void ResetSP_DeclCode(DefTableKey k, PTGNode v);
extern PTGNode GetSP_DeclCode(DefTableKey k, PTGNode v);
extern bool HasSP_DeclCode(DefTableKey k);
extern void IsSP_DeclCode(DefTableKey k, PTGNode which, PTGNode err);
extern void SetSP_DeclCode(DefTableKey k, PTGNode add, PTGNode replace);
extern void UniqueSP_DeclCode(DefTableKey k, std::function<PTGNode(void)> next);

extern void ResetAT_VarCodes(DefTableKey k, PTGNodes v);
extern PTGNodes GetAT_VarCodes(DefTableKey k, PTGNodes v);
extern bool HasAT_VarCodes(DefTableKey k);
extern void IsAT_VarCodes(DefTableKey k, PTGNodes which, PTGNodes err);
extern void SetAT_VarCodes(DefTableKey k, PTGNodes add, PTGNodes replace);
extern void UniqueAT_VarCodes(DefTableKey k, std::function<PTGNodes(void)> next);

extern void ResetS_InheritingSymbols(DefTableKey k, BindingList v);
extern BindingList GetS_InheritingSymbols(DefTableKey k, BindingList v);
extern bool HasS_InheritingSymbols(DefTableKey k);
extern void IsS_InheritingSymbols(DefTableKey k, BindingList which, BindingList err);
extern void SetS_InheritingSymbols(DefTableKey k, BindingList add, BindingList replace);
extern void UniqueS_InheritingSymbols(DefTableKey k, std::function<BindingList(void)> next);

extern DefTableKey LNGCloneKey(DefTableKey k); 




 
/// @brief dosort gives a topological order of the argument l according to the 
///        dependencies specified via in 
/// @return the sortable part of the input parameter l 
/// @pre l and in may not contain pure name definitions 
/// @post may report an error if the input is not a dag 
/// @param in the dependency relationship between type definitions 
/// @param l all type definitions 
extern BindingList dosort(Edges in, BindingList l); 


D_RuleSymbolAttributes compute_last_symbols(BindingList arg0, D_RuleSymbolAttributePtr arg1, Environment arg2);
D_RuleSymbolAttributes compute_last_rattrs(BindingList arg0, StringTableKey arg1);
void store_rule_computations(BindingList arg0, PTGNodes arg1);
PTGNodes compute_deps_codes(PTGNodes arg0, D_RuleSymbolAttributes arg1, D_RuleSymbolAttributePtr arg2, BindingList arg3, CoordPtr arg4, Environment arg5, PTGNodes arg6, Environment arg7, Environment arg8);
PTGNodes compute_ordered_expr_code_r(D_RHSKind arg0, PTGNodes arg1, BindingList arg2, StringTableKey arg3, PTGNodes arg4, CoordPtr arg5);
PTGNodes compute_ordered_expr_code(D_RHSKind arg0, PTGNodes arg1, StringTableKey arg2, PTGNodes arg3, CoordPtr arg4);
StringTableKey get_last_virtattr(D_VirtAttrs arg0);
PTGNodes compute_ordered_lastout_attr(StringTableKey arg0, AttributeDir arg1, D_LocalAttributeClass arg2, Binding arg3, PTGNodes arg4);
PTGNodes compute_deps_codes_s(D_SymbolLocalAttributePtr arg0, PTGNodes arg1, BindingList arg2, CoordPtr arg3, Environment arg4, D_SymbolAttributes arg5);
PTGNodes compute_expr_order_compute_r(BindingList arg0, PTGNodes arg1, std::vector<std::tuple<std::vector<D_RuleSymbolAttributePtr>, std::vector<PTGNode>, std::vector<PTGNode>, std::vector<PTGNode>>> arg2, Environment arg3);
PTGNodes compute_expr_order_compute(BindingList arg0, PTGNodes arg1, std::vector<std::tuple<std::vector<D_SymbolAttributePtr>, std::vector<PTGNode>, std::vector<PTGNode>, std::vector<PTGNode>>> arg2, Environment arg3);
PTGNode convert_symbolattr_to_code(D_SymbolAttributePtr arg0, Environment arg1);
PTGNode convert_symblocal_to_code(D_SymbolLocalAttributePtr arg0, Environment arg1);
PTGNodes compute_ordstat_init(PTGNodes arg0, PTGNodes arg1, PTGNodes arg2);
PTGNode convert_rulesymb_to_code(D_RuleSymbolAttributePtr arg0);
template<typename templ_a>
int atoone(templ_a arg0);
template<typename templ_a>
templ_a id(templ_a arg0);
IndexType it_combine_indices(IndexType arg0, IndexType arg1);
tStringType ptgins_type_of(std::tuple<int, tStringType, StringTableKey> arg0);
int ptgins_index_of(std::tuple<int, tStringType, StringTableKey> arg0);
bool ptgins_eq_index(std::tuple<int, tStringType, StringTableKey> arg0, std::tuple<int, tStringType, StringTableKey> arg1);
template<typename templ_a>
std::vector<templ_a> filter(std::function<bool(templ_a)> arg0, std::vector<templ_a> arg1);
template<typename templ_b, typename templ_a>
std::vector<templ_b> map(std::function<templ_b(templ_a)> arg0, std::vector<templ_a> arg1);
template<typename templ_b, typename templ_a>
templ_b fold(std::function<templ_b(templ_a, templ_b)> arg0, std::vector<templ_a> arg1, templ_b arg2);
std::vector<std::tuple<int, tStringType, StringTableKey>> ptgins_with_index(std::vector<std::tuple<int, tStringType, StringTableKey>> arg0, int arg1);
bool ptgins_types_valid(std::vector<std::tuple<int, tStringType, StringTableKey>> arg0);
bool ptgins_types_eq(std::vector<std::tuple<int, tStringType, StringTableKey>> arg0, tStringType arg1);
bool ptgins_cmp(std::tuple<int, tStringType, StringTableKey> arg0, std::tuple<int, tStringType, StringTableKey> arg1);
std::vector<std::tuple<int, tStringType, StringTableKey>> ptgins_uniques(std::vector<std::tuple<int, tStringType, StringTableKey>> arg0);
PTGNode ptgins_2_decl(std::tuple<int, tStringType, StringTableKey> arg0);
PTGNode ptgins_2_callarg(std::tuple<int, tStringType, StringTableKey> arg0);
PTGNode ptgins_fold(std::vector<std::tuple<int, tStringType, StringTableKey>> arg0, std::function<PTGNode(PTGNode, PTGNode)> arg1, std::function<PTGNode(std::tuple<int, tStringType, StringTableKey>)> arg2, PTGNode arg3);
int ptgins_size(std::vector<std::tuple<int, tStringType, StringTableKey>> arg0);
PTGNode ptgins_2_definit(std::tuple<int, tStringType, StringTableKey> arg0);
PTGNode ptgins_2_init(std::tuple<int, tStringType, StringTableKey> arg0);
PTGNode ptgins_2_check(std::tuple<int, tStringType, StringTableKey> arg0);
PTGNode ptgins_2_check_eq(std::tuple<int, tStringType, StringTableKey> arg0);
PTGNode ptgins_2_callref(std::tuple<int, tStringType, StringTableKey> arg0);
tStringType do_erase(tStringType arg0, bool arg1);
PTGNode compute_copy_code(StringTableKey arg0);
void predefine_severity(Environment arg0);
Environment predefined_types();
void set_type(tStringType arg0, DefTableKey arg1);
void predefine_type(Environment arg0, tStringType arg1);
std::vector<std::tuple<Binding, Binding>> nt_make_edges(BindingList arg0, Binding arg1);
BindingList typeenv_to_blist(Environment arg0);
bool do_ins(Binding arg0, BindingList arg1);
void empty_stat();
BindingList binds_to_list(Binding arg0);
PTGNode bindlist_2_ptg(BindingList arg0, std::function<PTGNode(Binding)> arg1, std::function<PTGNode(PTGNode, PTGNode)> arg2, PTGNode arg3);
PTGNode decl_code_of(Binding arg0);
CoordPtr defpos(StringTableKey arg0, Environment arg1);
bool doTrue(StringTableKey arg0);
bool doFalse();
bool constructed_type_is_fn(StringTableKey arg0);
void predef_fun(DefTableKey arg0, StringTableKeyList arg1, StringTableKey arg2);
void predef_fun_noargs(tStringType arg0, tStringType arg1, Environment arg2);
void predef_fun_newscope(Environment arg0);
void predef_fun_inherit(tStringType arg0, Environment arg1);
void predef_fun_bindkey(tStringType arg0, Environment arg1);
void predef_fun_bindlook(tStringType arg0, Environment arg1);
void predef_fun_keylook(tStringType arg0, Environment arg1);
void predef_fun_addidn(Environment arg0);
void predef_fun_bindidn(Environment arg0);
void predef_fun_defkey(tStringType arg0, Environment arg1);
void predef_next_inh_key(Environment arg0);
void predef_next_inh_bind(Environment arg0);
void predef_fun_bind_arg(tStringType arg0, tStringType arg1, Environment arg2);
void predef_fun_type_arg(tStringType arg0, tStringType arg1, tStringType arg2, Environment arg3);
void predef_fun_setkeyenv(Environment arg0);
void predef_ptgfun_null(Environment arg0);
void predef_ptgfun_out(Environment arg0);
void predef_skeyfuns(Environment arg0);
Environment predefined_functions();
void predef_clpfuns(Environment arg0);
void predef_report(Environment arg0);
void predef_report_helpers(Environment arg0, tStringType arg1, tStringType arg2);
void assert_no_voidargs(Environment arg0, StringTableKeyList arg1, CoordPtr arg2, StringTableKey arg3, int arg4);
void invalid_arg(bool arg0, CoordPtr arg1, StringTableKey arg2);
void assert_eq_types_l(Environment arg0, StringTableKeyList arg1, StringTableKeyList arg2, CoordPtr arg3, StringTableKey arg4);
void assert_eq_msg(bool arg0, bool arg1, bool arg2, StringTableKey arg3, StringTableKey arg4, StringTableKey arg5, StringTableKey arg6, CoordPtr arg7, StringTableKey arg8);
void assert_eq_types(Environment arg0, StringTableKey arg1, StringTableKey arg2, CoordPtr arg3, StringTableKey arg4);
void mk_pdl_fun(tStringType arg0, StringTableKey arg1, StringTableKey arg2, StringTableKeyList arg3, Environment arg4);
StringTableKey mk_pdl_rettype(D_pdl_gfun arg0, StringTableKey arg1);
StringTableKeyList mk_pdl_default_arg();
StringTableKeyList mk_pdl_arguments(D_pdl_gfun arg0, StringTableKey arg1);
tStringType pdl_prefix_of(D_pdl_gfun arg0);
void pdl_wrapped(D_pdl_gfun arg0, StringTableKey arg1, StringTableKey arg2, Environment arg3);
Environment mk_pdl_funs(StringTableKey arg0, StringTableKey arg1, Environment arg2);
StringTableKey ptg_ins_to_argtype(std::tuple<int, tStringType, StringTableKey> arg0);
StringTableKeyList ptg_uniques_to_args(std::vector<std::tuple<int, tStringType, StringTableKey>> arg0);
Environment mk_ptg_fun(StringTableKey arg0, StringTableKeyList arg1, Environment arg2);
PTGNode funs_2_head(BindingList arg0, Environment arg1, CoordPtr arg2);
PTGNode fun_arg_to_decl(StringTableKey arg0, Environment arg1, int arg2, CoordPtr arg3);
PTGNode fun_args_to_decl(StringTableKeyList arg0, Environment arg1, CoordPtr arg2);
PTGNode fun_2_head(Binding arg0, Environment arg1, CoordPtr arg2);
BindingList get_gen_funs(Environment arg0);
bool is_constructed(Binding arg0);
bool is_type_var(StringTableKey arg0);
bool is_eli_typeref(tStringType arg0);
void assert_equality(bool arg0, CoordPtr arg1, StringTableKey arg2, StringTableKey arg3);
bool fn_args_are_equal(StringTableKeyList arg0, StringTableKeyList arg1, CoordPtr arg2);
D_typePtr unify_alternatives(D_typePtr arg0, D_typePtr arg1, Environment arg2, CoordPtr arg3);
D_typePtr unify_dtype(D_typePtr arg0, D_typePtr arg1, Environment arg2, CoordPtr arg3);
D_typePtr invalid_on_inequal(bool arg0, CoordPtr arg1, StringTableKey arg2, StringTableKey arg3);
D_typePtr unify_dtype_noerr(D_typePtr arg0, D_typePtr arg1, Environment arg2, CoordPtr arg3);
void do_report_inequal_type_names(bool arg0, CoordPtr arg1, StringTableKey arg2, StringTableKey arg3);
D_typePtr report_inequal_type_names(bool arg0, CoordPtr arg1, StringTableKey arg2, StringTableKey arg3);
D_typePtr report_invalid(CoordPtr arg0, D_typePtr arg1, D_typePtr arg2);
D_types unify_dtypes(D_types arg0, D_types arg1, Environment arg2, CoordPtr arg3);
D_types unify_dtypes_noerr(D_types arg0, D_types arg1, Environment arg2, CoordPtr arg3);
D_types unify_dtypes_list(D_typesList arg0, D_types arg1, Environment arg2, CoordPtr arg3);
D_typePtr simple_type_to_tvar(StringTableKey arg0, Environment arg1, CoordPtr arg2);
int stdmax(int arg0, int arg1);
D_typePtr fun_to_dtype(bool arg0, Binding arg1, TypingKind arg2, Environment arg3, CoordPtr arg4);
D_typePtr flatten_dtypes_for_fn(D_types arg0, D_typePtr arg1);
D_typePtr tuple_to_dtype(bool arg0, Binding arg1, TypingKind arg2, Environment arg3, CoordPtr arg4);
D_types simple_types_to_tvars(StringTableKeyList arg0, Environment arg1, CoordPtr arg2);
D_typePtr map_to_dtype(bool arg0, Binding arg1, TypingKind arg2, Environment arg3, CoordPtr arg4);
D_typePtr list_to_dtype(bool arg0, Binding arg1, TypingKind arg2, Environment arg3, CoordPtr arg4);
D_typePtr predefined_skey(bool arg0, StringTableKey arg1);
void bind_tuple_args(D_types arg0, int arg1, int arg2, CoordPtr arg3, Environment arg4);
void assert_binding(Binding arg0, CoordPtr arg1, StringTableKey arg2);
void bind_arg_indexed(D_typePtr arg0, int arg1, CoordPtr arg2, Environment arg3);
Environment flattened_to_env(D_types arg0, int arg1, CoordPtr arg2);
StringTableKey skeys_hd(StringTableKeyList arg0);
StringTableKeyList skeys_tl(StringTableKeyList arg0);
D_typePtr basic_type_of_list(D_typePtr arg0);
D_typePtr make_concat_expected();
bool is_var_type(D_typePtr arg0);
D_typePtr make_alternatives(D_typePtr arg0, D_typePtr arg1, D_types arg2);
D_types fatypes_of(Environment arg0);
Environment create_list_hint_env(D_typePtr arg0);
Environment create_record_hint_env(D_typePtr arg0, Environment arg1);
bool is_record_type(D_typePtr arg0, Environment arg1);
D_typePtr compute_access_type(D_typePtr arg0, D_typePtr arg1, Environment arg2);
D_typePtr compute_access_type2(D_typePtr arg0, D_typePtr arg1, Environment arg2);
bool dtypes_includes(D_types arg0, D_typePtr arg1);
D_typePtr compute_access_type_possible_tree(StringTableKey arg0, StringTableKey arg1, Environment arg2);
D_typePtr compute_access_type_possible_tree2(StringTableKey arg0, StringTableKey arg1, Environment arg2);
bool expected_are_compatible(D_types arg0, D_types arg1);
bool function_as_expected(D_typePtr arg0, D_typePtr arg1);
bool expected_is_compatible(D_typePtr arg0, D_typePtr arg1);
bool check_equal_types(StringTableKey arg0, StringTableKey arg1);
D_typePtr compute_constructor_call(Binding arg0, Binding arg1, Environment arg2, CoordPtr arg3);
D_typePtr compute_record_call(Binding arg0, Environment arg1, CoordPtr arg2);
template<typename templ_a>
std::vector<templ_a> append_unique(std::vector<templ_a> arg0, std::vector<templ_a> arg1);
D_typePtr compute_call_type(Binding arg0, Binding arg1, Binding arg2, Environment arg3, StringTableKey arg4, CoordPtr arg5, Binding arg6);
D_typePtr compute_call_type_typedef(Binding arg0, Environment arg1, StringTableKey arg2, CoordPtr arg3);
bool dtype_is_function(D_typePtr arg0);
D_typePtr get_return_or_other(D_typePtr arg0);
D_typePtr get_arg_or_other(D_typePtr arg0);
bool is_invalid_type(D_typePtr arg0);
bool or_fn(bool arg0, bool arg1);
bool is_unit_type(D_typePtr arg0);
D_typePtr get_return_type(D_typePtr arg0);
D_typePtr get_arg_types(D_typePtr arg0);
bool is_unknown_dtype(D_typePtr arg0);
D_typePtr apply_type(D_typePtr arg0, D_typePtr arg1);
D_types apply_tuples(D_types arg0, D_types arg1);
D_typePtr report_apply_unknown(D_typePtr arg0, D_typePtr arg1);
void reset_fatype(Binding arg0, D_typePtr arg1, D_typePtr arg2, CoordPtr arg3);
D_typePtr base_var_of(D_typePtr arg0);
D_typePtr underlying_dtype(D_typePtr arg0);
bool is_var_type_recs(D_types arg0);
bool is_var_type_rec(D_typePtr arg0);
BindingList some_env_to_blist(Environment arg0);
void store_type_in_env(Binding arg0);
void do_store_type_in_arg(Binding arg0, D_typePtr arg1);
tStringType tuple_args_to_str(D_types arg0);
tStringType dtype_to_str(D_typePtr arg0);
StringTableKey dtype_to_idn(D_typePtr arg0);
StringTableKey flatten_skeylist(std::function<tStringType(tStringType, tStringType)> arg0, StringTableKeyList arg1);
tStringType seq_comma_str(tStringType arg0, tStringType arg1);
tStringType dtypes_to_str(D_types arg0);
bool is_list_type(D_typePtr arg0);
bool is_map_type(D_typePtr arg0);
StringTableKey get_type_var(D_typePtr arg0);
D_typePtr apply_subst(D_typePtr arg0, D_scheme arg1);
void store_application_eq(bool arg0, Binding arg1, D_typePtr arg2, D_typePtr arg3);
void store_application(bool arg0, Binding arg1, D_typePtr arg2, D_typePtr arg3);
D_typePtr apply_equal_functions(D_typePtr arg0, D_typePtr arg1, D_typePtr arg2, Binding arg3, CoordPtr arg4);
bool is_invalid_funarg(D_typePtr arg0);
D_typePtr apply_argtype(D_typePtr arg0, D_typePtr arg1, D_typePtr arg2, Binding arg3, CoordPtr arg4);
D_typePtr apply_argtypes(D_typePtr arg0, D_typePtr arg1, D_types arg2, D_typePtr arg3, Binding arg4, CoordPtr arg5, bool arg6);
bool is_free_var(D_typePtr arg0, D_scheme arg1);
D_typePtr message_higher_order(D_typePtr arg0, Binding arg1, CoordPtr arg2, bool arg3);
bool dtypes_equals_check(D_types arg0, D_types arg1);
bool typevar_is_generated(StringTableKey arg0);
bool dtype_equals_check(D_typePtr arg0, D_typePtr arg1);
bool dtype_equals_concat(D_typePtr arg0, D_typePtr arg1);
bool dtypes_equals_concat(D_types arg0, D_types arg1);
D_typePtr store_rewrite_binop(D_typePtr arg0, D_typePtr arg1, D_typePtr arg2, D_typePtr arg3, Binding arg4, CoordPtr arg5);
D_typePtr basetype_of_list(D_typePtr arg0);
bool only_misses_braces(D_typePtr arg0, D_typePtr arg1, Binding arg2);
bool is_mapping_tuple(D_typePtr arg0);
Environment create_map_hint_env(D_typePtr arg0);
Environment create_mapping_tuple_hint_env(D_typePtr arg0);
bool is_defined_data(D_typePtr arg0, Environment arg1, CoordPtr arg2);
void replace_all_step(Binding arg0, CoordPtr arg1, D_typePtr arg2, D_typePtr arg3);
bool is_alternative_type(D_typePtr arg0);
D_scheme assign_scheme(bool arg0, D_scheme arg1, D_typePtr arg2, D_typePtr arg3);
void replace_all(Binding arg0, CoordPtr arg1, Environment arg2, D_typePtr arg3, Binding arg4);
void do_reset_fa_type(DefTableKey arg0, D_typePtr arg1);
void print_funs_env(Environment arg0, Environment arg1);
void print_funs_binds(Binding arg0, Environment arg1);
void convert_types_to_patterntypes(Binding arg0, Environment arg1);
StringTableKeyList ftv_dtype(D_typePtr arg0);
StringTableKeyList ftv_dtypes(D_types arg0);
D_typePtr typevar_of_int(int arg0);
D_typePtr newvar(StringTableKey arg0, int arg1, int arg2);
void ftv_fun(Binding arg0);
D_typePtr basetype_of_list_or_tuple(D_typePtr arg0, Environment arg1);
void rewrite_vars(D_types arg0, D_types arg1, Binding arg2, CoordPtr arg3);
void report_wrong_calling(StringTableKey arg0, D_typePtr arg1, D_typePtr arg2, D_types arg3, D_scheme arg4, D_scheme arg5, CoordPtr arg6);
tStringType argtypes_to_str(D_types arg0, D_scheme arg1);
tStringType append_strs(tStringType arg0, tStringType arg1);
D_typePtr make_expected_type_call(D_types arg0, D_typePtr arg1);
Environment create_extern_constant_env();
int get_patterns_size(D_patternLists arg0, D_patterns arg1, CoordPtr arg2);
D_types patterns_to_dtypes(D_patterns arg0, Environment arg1, Environment arg2);
D_typesList patternlists_to_dtypes(D_patternLists arg0, Environment arg1, Environment arg2);
D_typePtr constant_to_dtype(D_constantPtr arg0);
D_types string_keys_to_typevars(StringTableKeyList arg0);
D_typePtr pattern_to_dtype(D_patternPtr arg0, Environment arg1, Environment arg2);
D_typePtr constructor_to_dtype(StringTableKey arg0, Environment arg1, Environment arg2);
StringTableKey ptg_node_idn_add(StringTableKey arg0, StringTableKey arg1);
void emit_file(PTGNode arg0, tStringType arg1);
PTGNode bool_to_ptgcheck(bool arg0);
PTGNode dconstant_to_select_code(D_constantPtr arg0, int arg1);
void analyze_functions(Environment arg0, Environment arg1, Environment arg2);
D_patternTypes patterns_to_ptypes(D_patterns arg0, Environment arg1, Environment arg2);
void analyze_pattern_lr(D_patterns arg0, int arg1, int arg2, Environment arg3, Environment arg4, CoordPtr arg5, StringTableKey arg6, bool arg7);
void analyze_pattern_td(D_patterns arg0, int arg1, int arg2, Environment arg3, Environment arg4, CoordPtr arg5, StringTableKey arg6);
D_patternTypePtr unify_ptypes_lr(D_patternTypePtr arg0, D_patternTypes arg1);
D_patternTypePtr unify_ptypes_td(D_patternTypePtr arg0, D_patternTypes arg1);
bool is_nonfull_ptype(D_patternTypePtr arg0);
bool is_full_ptype(D_patternTypePtr arg0);
bool equal_names(StringTableKeyList arg0, StringTableKeyList arg1);
bool contains_name(StringTableKeyList arg0, StringTableKey arg1);
D_patternTypePtr unify_ptype_td(D_patternTypePtr arg0, D_patternTypePtr arg1);
D_patternTypePtr unify_ptype_lr(D_patternTypePtr arg0, D_patternTypePtr arg1);
void analyze_functions_bind(Binding arg0, Environment arg1, Environment arg2);
void analyze_patterns_td(D_patternLists arg0, Environment arg1, Environment arg2, CoordPtr arg3, StringTableKey arg4);
void analyze_patterns_lr(D_PatternCodes arg0, Environment arg1, Environment arg2, CoordPtr arg3, StringTableKey arg4);
D_patterns get_pattern_from_code(D_PatternCodePtr arg0);
bool get_fallthrough_from_code(D_PatternCodePtr arg0);
D_patterns pattern_filter_indexed(D_patternLists arg0, int arg1);
D_patternLists pattern_filter_cols(D_patternLists arg0);
PTGNode convert_patterns_to_select(D_patterns arg0, int arg1, Environment arg2);
PTGNode dpattern_to_select_code(D_patternPtr arg0, int arg1, Environment arg2);
PTGNode dconstructor_to_select_code(StringTableKey arg0, int arg1, Environment arg2);
D_patternTypePtr pattern_to_ptype(D_patternPtr arg0, Environment arg1, Environment arg2);
D_patternTypePtr constant_to_ptype(D_constantPtr arg0, Environment arg1, Environment arg2);
StringTableKey bool_to_con(bool arg0);
StringTableKeyList bool_constructor(bool arg0);
StringTableKeyList bool_constructors();
D_patternTypePtr somename_to_ptype(StringTableKey arg0, Environment arg1, Environment arg2);
D_typePtr unify_to_flat(D_types arg0, D_typePtr arg1);
bool dtype_equals_calltemp(D_typePtr arg0, D_typePtr arg1);
bool dtypes_equals_calltemp(D_types arg0, D_types arg1);
PTGNode compute_calltemplate_code(StringTableKey arg0, StringTableKeyList arg1, D_scheme arg2, D_scheme arg3, Environment arg4, CoordPtr arg5, Binding arg6, bool arg7);
StringTableKey get_type_var_r(D_typePtr arg0);
StringTableKey get_type_vars_r(D_types arg0);
bool contains_dtype(D_types arg0, D_typePtr arg1);
D_typePtr apply_subst_r(D_typePtr arg0, D_scheme arg1);
PTGNode callref_to_code(StringTableKey arg0, Binding arg1, Binding arg2, Binding arg3, D_typePtr arg4, D_patterns arg5, CoordPtr arg6, Environment arg7, int arg8, int arg9, bool arg10, D_scheme arg11);
bool is_list_head(D_patternPtr arg0, StringTableKey arg1);
bool is_listpattern(D_patternPtr arg0);
Binding base_type_of(Binding arg0, Environment arg1, TypingKind arg2);
Binding dtype_to_type_bind(D_typePtr arg0, Environment arg1);
Binding pattern_to_conname(D_patternPtr arg0, Environment arg1);
Binding pattern_to_typebind(D_patternPtr arg0, Environment arg1);
int argnum_of_conarg(D_patternPtr arg0, StringTableKey arg1);
int argnum_of_conargs(StringTableKeyList arg0, StringTableKey arg1, int arg2);
PTGNode compute_each_code(D_typePtr arg0, PTGNode arg1, PTGNode arg2, PTGNode arg3);
PTGNode compute_assign_type(D_typePtr arg0, Environment arg1, bool arg2);
PTGNode compute_assign_types(D_types arg0, Environment arg1, bool arg2);
void warn_lambda_variable(BindingList arg0, CoordPtr arg1, D_scheme arg2);
PTGNode compute_lambda_return_type(D_typePtr arg0, Environment arg1, PTGNode arg2);
PTGNode compute_lambda_var_decls(BindingList arg0, Environment arg1, D_scheme arg2);
BindingList compute_lambda_unique_vars(BindingList arg0);
StringTableKeyList compute_lambda_caputures(BindingList arg0, StringTableKeyList arg1);
PTGNode convert_lambda_captures(StringTableKeyList arg0);
bool compute_if_subreturns(bool arg0, bool arg1, bool arg2);
PTGNode compute_if_code(bool arg0, bool arg1, bool arg2, bool arg3, PTGNode arg4, PTGNode arg5, PTGNode arg6);
PTGNode bool_to_ptg(bool arg0);
bool contains_fulllist_pattern(StringTableKeyList arg0, D_patterns arg1);
StringTableKeyList append_list_pattern(StringTableKeyList arg0, D_patternPtr arg1);
PTGNode compute_constant_initvec(CoordPtr arg0, D_typePtr arg1, Environment arg2, D_scheme arg3);
PTGNode compute_constant_emptylist(D_typePtr arg0, bool arg1, Environment arg2, D_scheme arg3);
PTGNode compute_binary_concat_op_code(D_typePtr arg0, D_typePtr arg1, D_typePtr arg2, PTGNode arg3, PTGNode arg4, CoordPtr arg5);
PTGNode compute_access_code(D_typePtr arg0, PTGNode arg1, D_typePtr arg2, PTGNode arg3, Environment arg4);
void print_patterncode(StringTableKey arg0, D_PatternCodePtr arg1);
PTGNode signature_of(D_funCodePtr arg0);
PTGNode generate_function_signatures(D_funCodes arg0);
PTGNode generate_function_implementations(D_funCodes arg0, bool arg1);
PTGNode signature_and_impl(D_funCodePtr arg0, bool arg1);
D_funCodes fun_codegen(Environment arg0, Environment arg1);
bool has_some_lists(D_patternLists arg0, D_types arg1);
bool has_some_return(D_PatternCodes arg0);
bool patcode_has_return(D_PatternCodePtr arg0);
D_funCodes fun_codegen_bind(Binding arg0, Environment arg1);
PTGNode fun_signature(Binding arg0, D_types arg1, D_typePtr arg2, bool arg3, StringTableKeyList arg4, Environment arg5);
PTGNode funargs_codegen(D_types arg0, Environment arg1);
PTGNode funarg_codegen(D_typePtr arg0, int arg1, Environment arg2);
PTGNode report_invalid_varname(StringTableKey arg0, PTGNode arg1);
PTGNode compute_dtype_code_force(D_typePtr arg0, Environment arg1, bool arg2);
PTGNode compute_record_code(D_typePtr arg0, Environment arg1);
PTGNode compute_typename_code(StringTableKey arg0, Environment arg1);
PTGNode tuple_args_to_code(D_types arg0, Environment arg1, bool arg2);
PTGNode compute_dtype_code(D_typePtr arg0, Environment arg1, bool arg2);
bool is_tail_recursive(D_PatternCodes arg0, int arg1);
bool is_recursive(D_PatternCodePtr arg0);
PTGNode fun_impl_code(Binding arg0, D_PatternCodes arg1, D_patternLists arg2, D_types arg3, bool arg4, Environment arg5);
bool patcode_has_errexpr(D_PatternCodePtr arg0);
bool none_has_return(D_PatternCodes arg0);
bool and_fn(bool arg0, bool arg1);
bool any_has_return(D_PatternCodes arg0);
D_PatternCodePtr add_return_to_code(D_PatternCodePtr arg0);
D_PatternCodePtr remove_select_code(D_PatternCodePtr arg0);
int list_iteration_pos(D_patterns arg0, int arg1);
PTGNode compute_list_iteration_code(D_patterns arg0, PTGNode arg1, PTGNode arg2);
PTGNode compute_list_iter_code(D_patternPtr arg0, int arg1);
PTGNode compute_recursive_list_init(D_patternPtr arg0, int arg1);
PTGNode compute_list_recursion_code(D_patterns arg0, PTGNode arg1, PTGNode arg2);
PTGNode fun_impl_code_single(D_PatternCodePtr arg0, D_patterns arg1, D_types arg2, bool arg3, Environment arg4, D_typePtr arg5, Binding arg6, bool arg7);
bool has_list_init(D_patterns arg0, int arg1);
bool is_list_init(D_patternPtr arg0);
int cnt_listpats(D_patterns arg0, D_types arg1);
bool is_list_constant(D_patternPtr arg0);
bool constant_is_emptylist(D_constantPtr arg0);
PTGNode do_opt_colon(PTGNode arg0);
PTGNode do_opt_comma(PTGNode arg0);
PTGNode skey_to_typevar(StringTableKey arg0);
PTGNode genarg_to_decl(D_ConArgPtr arg0);
PTGNode genarg_to_arg(D_ConArgPtr arg0);
PTGNode genarg_to_funs(D_ConArgPtr arg0);
bool type_is_initializable(StringTableKey arg0, Environment arg1);
bool dtype_is_initializable(D_typePtr arg0, Environment arg1);
bool is_record(TypingKind arg0);
bool is_constructing(TypingKind arg0);
PTGNode compute_default_init(StringTableKey arg0, StringTableKey arg1, Environment arg2);
PTGNode compute_default_init_dtype(StringTableKey arg0, D_typePtr arg1, Environment arg2);
PTGNode genarg_to_default(D_ConArgPtr arg0);
PTGNode genarg_to_default_in(D_ConArgPtr arg0);
PTGNode genarg_to_fun_impls_record(D_ConArgPtr arg0);
PTGNode genarg_to_fun_impls_r(D_ConArgPtr arg0);
PTGNode genarg_to_init(D_ConArgPtr arg0);
PTGNode genarg_to_init_in(D_ConArgPtr arg0);
PTGNode genarg_to_init_other(D_ConArgPtr arg0);
PTGNode genarg_to_init_other_in(D_ConArgPtr arg0);
PTGNode genarg_to_print_code(D_ConArgPtr arg0);
PTGNode compute_default_init_in(StringTableKey arg0, StringTableKey arg1, Environment arg2);
PTGNode compute_default_init_in_dtype(StringTableKey arg0, D_typePtr arg1, Environment arg2);
PTGNode transform_conargs_less(bool arg0, D_ConArgs arg1);
PTGNode conarg_name_of(D_ConArgPtr arg0);
PTGNode genarg_to_eq_r(D_ConArgPtr arg0);
PTGNode genarg_to_eq(D_ConArgPtr arg0);
PTGNode genarg_to_init_move(D_ConArgPtr arg0);
PTGNode genarg_to_init_move_in(D_ConArgPtr arg0);
PTGNode genarg_to_fun_impls(D_ConArgPtr arg0);
PTGNode genarg_to_argref(D_ConArgPtr arg0);
PTGNode genarg_to_print_impl(D_ConArgPtr arg0);
PTGNode compute_print_impl(D_typePtr arg0, PTGNode arg1, Environment arg2, bool arg3, int arg4);
PTGNode compute_predefined_print(StringTableKey arg0, PTGNode arg1);
bool is_tuple_type(D_typePtr arg0);
D_typePtr access_tuple_type(D_typePtr arg0, D_typePtr arg1, int arg2, CoordPtr arg3);
StringTableKeyList dtype_to_skeylist(D_typePtr arg0);
bool is_string_type(D_typePtr arg0);
Environment create_string_hint_env();
bool is_tree_type(D_typePtr arg0);
bool contains_bind(BindingList arg0, Binding arg1);
D_typePtr make_index_alternatives(D_typePtr arg0);
void check_term_symbol(bool arg0, SymbolType arg1, TermType arg2, CoordPtr arg3, Binding arg4, StringTableKey arg5, StringTableKey arg6, StringTableKey arg7);
PTGNode resulting_tree_fun(Binding arg0, StringTableKeyList arg1, StringTableKey arg2);
Environment mk_initial_mktree_fun(Binding arg0, StringTableKeyList arg1, StringTableKey arg2, Environment arg3);
StringTableKeyList map_term_types(StringTableKeyList arg0, Environment arg1);
StringTableKey map_term_type(StringTableKey arg0, Environment arg1);
bool rules_are_equal(bool arg0, CoordPtr arg1, StringTableKey arg2, StringTableKey arg3, D_ProdSymbols arg4, D_ProdSymbols arg5);
bool prod_symbol_equals(D_ProdSymbolPtr arg0, D_ProdSymbolPtr arg1);
D_ProdSymbols gen_prodsymbol_indices(StringTableKey arg0, int arg1, D_SkeyToIndex arg2, D_ProdSymbols arg3);
D_SkeyToIndex increment_map(D_ProdSymbolPtr arg0, StringTableKey arg1, D_SkeyToIndex arg2);
D_ProdSymbolPtr apply_mapping(D_ProdSymbolPtr arg0, int arg1, D_SkeyToIndex arg2);
void insert_rules_from(BindingList arg0, Binding arg1, StringTableKey arg2, StringTableKey arg3, CoordPtr arg4);
void insert_rules_from_tree(Binding arg0, Binding arg1, StringTableKey arg2, StringTableKey arg3, CoordPtr arg4);
void insert_rule_to_tree(Binding arg0, Environment arg1, Binding arg2, Binding arg3, StringTableKey arg4, StringTableKey arg5, CoordPtr arg6);
D_ProdSymbolPtr apply_rename_rule(D_ProdSymbolPtr arg0, StringTableKey arg1, StringTableKey arg2, StringTableKey arg3, Environment arg4);
void insert_symbols_from(BindingList arg0, Binding arg1, StringTableKey arg2, StringTableKey arg3, CoordPtr arg4);
void insert_symbols_from_tree(Binding arg0, Binding arg1, StringTableKey arg2, StringTableKey arg3, CoordPtr arg4);
void insert_symbol_to_tree(Binding arg0, Environment arg1, Binding arg2, StringTableKey arg3, StringTableKey arg4, CoordPtr arg5);
void insert_symbol_attributes_from_trees(BindingList arg0, Binding arg1, StringTableKey arg2, Environment arg3, CoordPtr arg4);
void insert_symbol_attributes_to_tree(Binding arg0, Binding arg1, Binding arg2, StringTableKey arg3, Environment arg4, CoordPtr arg5);
void copy_attr_to_env(Environment arg0, StringTableKey arg1, tStringType arg2, tStringType arg3, CoordPtr arg4);
void copy_symbols_from(BindingList arg0, Binding arg1, CoordPtr arg2);
void copy_symbol_to_tree(Binding arg0, Environment arg1, Binding arg2, Binding arg3, CoordPtr arg4);
void copy_rules_from(BindingList arg0, Binding arg1, CoordPtr arg2);
void copy_rule_to_tree(Binding arg0, Environment arg1, Binding arg2, Binding arg3, CoordPtr arg4);
D_ProdSymbolPtr apply_rule_treechange(D_ProdSymbolPtr arg0, StringTableKey arg1, StringTableKey arg2, CoordPtr arg3);
void insert_thread_attributes(Environment arg0, StringTableKey arg1, D_typePtr arg2, CoordPtr arg3);
Environment create_gentree_attr(Environment arg0, StringTableKey arg1, StringTableKey arg2, StringTableKey arg3, CoordPtr arg4, Environment arg5);
Binding lookup_or_insert(Environment arg0, StringTableKey arg1, StringTableKey arg2, bool arg3, StringTableKeyList arg4);
Binding insert_new_symbol(Environment arg0, StringTableKey arg1, Binding arg2, bool arg3);
StringTableKeyList remove_declared(StringTableKeyList arg0, StringTableKeyList arg1);
StringTableKey lookup_symb_tree(StringTableKey arg0, StringTableKey arg1, Environment arg2, StringTableKeyList arg3);
Binding create_local_attribute(Binding arg0, StringTableKey arg1, StringTableKey arg2, StringTableKey arg3, Environment arg4, CoordPtr arg5);
Binding bind_of_treesymb(TreeSymbPtr arg0);
StringTableKey tree_of_treesymb(TreeSymbPtr arg0);
TreeSymbPtr lookup_symbol(StringTableKey arg0, StringTableKey arg1, Binding arg2, Environment arg3);
void insert_inherited_computations_trees(Binding arg0, StringTableKey arg1, TreeSymbs arg2);
void insert_inherited_computations(Binding arg0, StringTableKey arg1, TreeSymbPtr arg2);
D_SymbolComputations replace_comps_symbol(StringTableKey arg0, StringTableKey arg1, D_SymbolComputations arg2);
D_SymbolComputationPtr replace_comp_symbol(D_SymbolComputationPtr arg0, StringTableKey arg1, StringTableKey arg2);
D_SymbolAttributePtr replace_attr_symbol(D_SymbolAttributePtr arg0, StringTableKey arg1, StringTableKey arg2);
D_SymbolLocalAttributePtr replace_locattr_symbol(D_SymbolLocalAttributePtr arg0, StringTableKey arg1, StringTableKey arg2);
D_SymbolAttributes replace_attrs_symbol(D_SymbolAttributes arg0, StringTableKey arg1, StringTableKey arg2);
D_SymbolExpressionPtr replace_expr_symbol(D_SymbolExpressionPtr arg0, StringTableKey arg1, StringTableKey arg2);
D_SymbolExpressions replace_exprs_symbol(D_SymbolExpressions arg0, StringTableKey arg1, StringTableKey arg2);
void insert_inherited_attributes_trees(Binding arg0, StringTableKey arg1, TreeSymbs arg2, Environment arg3, CoordPtr arg4);
void insert_inherited_attributes(Binding arg0, StringTableKey arg1, TreeSymbPtr arg2, Environment arg3, CoordPtr arg4);
void insert_inherited_attribute(Environment arg0, StringTableKey arg1, TreeSymbPtr arg2, Binding arg3, StringTableKey arg4, CoordPtr arg5);
StringTableKey lookup_symbol_attr_name(StringTableKey arg0, StringTableKey arg1, StringTableKey arg2, StringTableKeyList arg3, Environment arg4);
D_LocalAttributeClass map_dirs(AttributeDir arg0);
bool compatible_dirs(AttributeDir arg0, D_LocalAttributeClass arg1);
D_SymbolExpressionPtr append_guards(D_SymbolExpressionPtr arg0, D_SymbolExpressionPtr arg1, CoordPtr arg2);
D_SymbolExpressionPtr message_wrong_guards(CoordPtr arg0, D_SymbolExpressionPtr arg1, D_SymbolExpressionPtr arg2, tStringType arg3, D_SymbolExpressionPtr arg4);
bool symb_attrs_eq(D_SymbolAttributePtr arg0, D_SymbolAttributePtr arg1);
bool local_attrs_eq(D_SymbolLocalAttributePtr arg0, D_SymbolLocalAttributePtr arg1);
bool breaks_chain(D_LocalAttributeClass arg0, D_LocalAttributeClass arg1);
bool local_dirs_compatible(D_LocalAttributeClass arg0, D_LocalAttributeClass arg1);
bool remote_attrs_eq(D_RemoteAttributePtr arg0, D_RemoteAttributePtr arg1);
bool contains_loc_attr(D_SymbolLocalAttributes arg0, D_SymbolLocalAttributePtr arg1);
bool contains_symb_attr(D_SymbolAttributes arg0, D_SymbolAttributePtr arg1);
D_SymbolAttributes append_symb_attrs(D_SymbolAttributes arg0, D_SymbolAttributes arg1);
bool def_occ_eq(D_SymbolComputationPtr arg0, D_SymbolComputationPtr arg1);
bool contains_defocc(D_SymbolComputations arg0, D_SymbolComputationPtr arg1);
D_SymbolComputationPtr find_defocc(D_SymbolComputations arg0, D_SymbolComputationPtr arg1);
D_SymbolComputations append_computations(bool arg0, D_SymbolComputations arg1, D_SymbolComputations arg2);
CoordPtr get_comp_pos(D_SymbolComputationPtr arg0);
StringTableKey get_def_attr_id(D_SymbolComputationPtr arg0);
StringTableKey get_symbattr_attrid(D_SymbolAttributePtr arg0);
StringTableKey get_symbattr_symbid(D_SymbolAttributePtr arg0);
StringTableKey get_symblocal_symbid(D_SymbolLocalAttributePtr arg0);
StringTableKey get_symblocal_attrid(D_SymbolLocalAttributePtr arg0);
StringTableKey get_def_symb_id(D_SymbolComputationPtr arg0);
void report_double_comp(D_SymbolComputations arg0, D_SymbolComputationPtr arg1);
D_ProdMatchPtr compute_match_from_prodsymb(D_ProdSymbolPtr arg0, StringTableKey arg1, CoordPtr arg2);
bool doesnt_match(D_ProdMatchPtr arg0);
BindingList matching_rules(bool arg0, StringTableKeyList arg1, D_ProdMatches arg2, StringTableKey arg3, Environment arg4, CoordPtr arg5);
void report_multi_match(BindingList arg0, CoordPtr arg1);
bool rule_does_match(D_ProdMatches arg0, Binding arg1, StringTableKey arg2);
bool matches_name(StringTableKey arg0, D_ProdMatchPtr arg1, StringTableKey arg2);
bool match_symbols(D_ProdSymbols arg0, D_ProdMatches arg1, StringTableKey arg2);
MatchType match_trees(StringTableKey arg0, StringTableKey arg1, StringTableKey arg2);
MatchType match_symbol(D_ProdSymbolPtr arg0, D_ProdMatchPtr arg1, StringTableKey arg2);
D_RuleExpressionPtr append_rule_guards(D_RuleExpressionPtr arg0, D_RuleExpressionPtr arg1, CoordPtr arg2);
D_RuleExpressionPtr message_wrong_guards_rule(CoordPtr arg0, D_RuleExpressionPtr arg1, D_RuleExpressionPtr arg2, tStringType arg3, D_RuleExpressionPtr arg4);
void store_rule_computations_rules(bool arg0, BindingList arg1, D_RuleComputationPtr arg2, Environment arg3, Environment arg4, StringTableKey arg5, CoordPtr arg6, Environment arg7);
D_RuleComputationPtr replace_vars_with_concrete(D_RuleComputationPtr arg0, Environment arg1, StringTableKey arg2, D_ProdSymbols arg3);
template<typename templ_b, typename templ_a>
std::vector<templ_b> map2(std::function<templ_b(templ_a)> arg0, std::vector<templ_a> arg1);
D_RuleSymbolAttributePtr replace_ruleattr_with_concrete(D_RuleSymbolAttributePtr arg0, Environment arg1, StringTableKey arg2, D_ProdSymbols arg3);
D_RuleSymbolAttributePtr convert_treesymbol_to_rule_attribute(D_ProdSymbolPtr arg0, StringTableKey arg1, Binding arg2, CoordPtr arg3);
D_ProdSymbols symb_in_prodsymb(D_ProdSymbols arg0, StringTableKey arg1);
bool prodsymb_has_treesymb(D_ProdSymbolPtr arg0, StringTableKey arg1);
D_RuleExpressionPtr replace_expr_attrs_with_concrete(D_RuleExpressionPtr arg0, Environment arg1, StringTableKey arg2, D_ProdSymbols arg3);
D_RuleExpressions replace_exprs_attrs_with_concrete(D_RuleExpressions arg0, Environment arg1, StringTableKey arg2, D_ProdSymbols arg3);
D_RuleComputations replace_computations_to_concrete(D_RuleComputations arg0, Environment arg1, StringTableKey arg2, D_ProdSymbols arg3);
D_RuleLetvarDefPtr replace_letvarexpr_to_concrete(D_RuleLetvarDefPtr arg0, Environment arg1, StringTableKey arg2, D_ProdSymbols arg3);
StringTableKey name_of_letvar(D_RuleLetvarDefPtr arg0);
D_RuleExpressionPtr unfold_letvars_concrete(D_RuleLetvarDefs arg0, D_RuleExpressionPtr arg1, Environment arg2, StringTableKey arg3, D_ProdSymbols arg4);
D_RuleExpressionPtr unfold_letvars(D_RuleLetvarDefs arg0, D_RuleExpressionPtr arg1);
D_RuleExpressions unfold_letvars_list(D_RuleLetvarDefs arg0, D_RuleExpressions arg1);
D_RuleComputations unfold_letvars_in_computes(D_RuleLetvarDefs arg0, D_RuleComputations arg1);
D_RuleComputationPtr unfold_letvars_in_compute(D_RuleLetvarDefs arg0, D_RuleComputationPtr arg1);
D_RuleExpressionPtr substitute_var(StringTableKey arg0, D_RuleLetvarDefs arg1);
D_RuleExpressionPtr get_letvar_expr(D_RuleLetvarDefPtr arg0);
void store_and_check_rule_computations(bool arg0, Binding arg1, D_RuleComputationPtr arg2, Environment arg3, Environment arg4, StringTableKey arg5, CoordPtr arg6, Environment arg7);
D_RuleComputations symbol_computations_to_rule_computations(bool arg0, Binding arg1, StringTableKey arg2, D_ProdSymbols arg3, Environment arg4, StringTableKey arg5, CoordPtr arg6, Environment arg7);
BindingList lookup_inheritance_trees(Binding arg0, BindingList arg1);
Binding lookup_symbol_for_rule(StringTableKey arg0, StringTableKey arg1, StringTableKey arg2, Binding arg3, Binding arg4, Environment arg5);
D_RuleComputations load_symbol_computations_for_rule(bool arg0, D_ProdSymbolPtr arg1, D_LocalAttributeClass arg2, Binding arg3, D_ProdSymbolPtr arg4, D_ProdSymbols arg5, Environment arg6, StringTableKey arg7, CoordPtr arg8, Environment arg9);
D_RuleComputationPtr convert_symbol_computation(D_SymbolComputationPtr arg0, D_LocalAttributeClass arg1, D_ProdSymbolPtr arg2, D_ProdSymbols arg3, StringTableKey arg4, int arg5);
D_RuleSymbolAttributes convert_symbol_attributes(D_SymbolAttributes arg0, D_LocalAttributeClass arg1, D_ProdSymbolPtr arg2, D_ProdSymbols arg3, StringTableKey arg4, int arg5);
D_RuleComputations convert_symbol_computations(D_SymbolComputations arg0, D_LocalAttributeClass arg1, D_ProdSymbolPtr arg2, D_ProdSymbols arg3, StringTableKey arg4, int arg5);
D_RuleSymbolAttributePtr convert_symbol_attribute(D_SymbolAttributePtr arg0, D_LocalAttributeClass arg1, D_ProdSymbolPtr arg2, D_ProdSymbols arg3, StringTableKey arg4, int arg5);
bool dir_is_remote(D_LocalAttributeClass arg0);
D_RuleSymbolAttributePtr convert_local_attribute(D_SymbolLocalAttributePtr arg0, D_LocalAttributeClass arg1, D_ProdSymbolPtr arg2, D_ProdSymbols arg3, StringTableKey arg4, int arg5);
bool scomputation_defines(D_SymbolComputationPtr arg0);
bool any_computation_defines(D_SymbolComputations arg0);
bool sdefocc_matches_dir(D_SymbolComputationPtr arg0, D_LocalAttributeClass arg1, Environment arg2, D_ProdSymbolPtr arg3, D_ProdSymbols arg4);
bool symbattr_matches_dir(D_SymbolAttributePtr arg0, D_LocalAttributeClass arg1, Environment arg2, D_ProdSymbolPtr arg3, D_ProdSymbols arg4);
bool localattr_matches_dir(D_SymbolLocalAttributePtr arg0, D_LocalAttributeClass arg1, Environment arg2, D_ProdSymbolPtr arg3, D_ProdSymbols arg4);
bool match_symbattr_to_rule_dir(D_LocalAttributeClass arg0, D_LocalAttributeClass arg1, StringTableKey arg2, StringTableKey arg3, StringTableKey arg4, D_ProdSymbols arg5, Environment arg6, Binding arg7);
bool dirs_are_compatible(D_LocalAttributeClass arg0, D_LocalAttributeClass arg1);
D_RuleComputations get_computations(D_RuleComputationPtr arg0);
bool def_occ_eq_r(D_RuleComputationPtr arg0, D_RuleComputationPtr arg1);
bool rule_attrs_eq(D_RuleSymbolAttributePtr arg0, D_RuleSymbolAttributePtr arg1);
bool contains_defocc_r(D_RuleComputations arg0, D_RuleComputationPtr arg1);
D_RuleComputationPtr find_defocc_r(D_RuleComputations arg0, D_RuleComputationPtr arg1);
D_RuleComputations append_computations_r(bool arg0, D_RuleComputations arg1, D_RuleComputations arg2);
CoordPtr get_comp_pos_r(D_RuleComputationPtr arg0);
tStringType get_def_attr_id_r(D_RuleComputationPtr arg0);
tStringType get_def_attr_id_ra(D_RuleSymbolAttributePtr arg0);
void report_double_comp_r(D_RuleComputations arg0, D_RuleComputationPtr arg1);
D_RuleExpressionPtr convert_symbol_expression(D_SymbolExpressionPtr arg0, D_LocalAttributeClass arg1, D_ProdSymbolPtr arg2, D_ProdSymbols arg3, StringTableKey arg4, int arg5);
D_RuleExpressions convert_symbol_expressions(D_SymbolExpressions arg0, D_LocalAttributeClass arg1, D_ProdSymbolPtr arg2, D_ProdSymbols arg3, StringTableKey arg4, int arg5);
D_RuleExpressionPtr convert_symbol_let(D_SymbolLetVarDefs arg0, D_SymbolExpressionPtr arg1, D_LocalAttributeClass arg2, D_ProdSymbolPtr arg3, D_ProdSymbols arg4, StringTableKey arg5, int arg6);
D_SymbolExpressionPtr get_letvar_expr_s(D_SymbolLetVarDefPtr arg0);
StringTableKey get_letvar_name_s(D_SymbolLetVarDefPtr arg0);
D_typePtr get_letvar_type_s(D_SymbolLetVarDefPtr arg0);
Environment add_symbol_computations_in_tree(Environment arg0, Environment arg1);
void add_symbol_computations_in_rules(Environment arg0, Environment arg1, Environment arg2);
void add_symbol_computations_in_rule(Binding arg0, Environment arg1, Environment arg2);
D_RemoteShieldPtr combine_shields(D_RemoteShieldPtr arg0, D_RemoteShieldPtr arg1, CoordPtr arg2);
D_RemoteShieldPtr report_invalid_shields(D_RemoteShieldPtr arg0, D_RemoteShieldPtr arg1, CoordPtr arg2);
bool is_ag_ok(Environment arg0, Environment arg1, Environment arg2);
bool tree_is_ordered(Binding arg0, Environment arg1);
bool remote_computations_are_valid(Binding arg0, Environment arg1, bool arg2);
void check_valid_attr(D_RuleSymbolAttributePtr arg0, Binding arg1, Environment arg2, bool arg3);
void check_attr_list_existing(D_SymbolLocalAttributes arg0, Binding arg1, Environment arg2);
Binding attr_is_valid(D_SymbolLocalAttributePtr arg0, Binding arg1, Environment arg2);
D_RuleAttrMap compute_referenced_attrs(BindingList arg0);
D_RuleSymbolAttributes get_referenced_rattrs(D_RuleComputationPtr arg0);
D_RuleSymbolAttributes get_referenced_rattrs_in_expr(D_RuleExpressionPtr arg0);
bool contains_rattr(D_RuleSymbolAttributes arg0, D_RuleSymbolAttributePtr arg1);
int get_prod_symbindex(D_ProdSymbolPtr arg0);
StringTableKey get_prod_symbol(D_ProdSymbolPtr arg0);
bool is_literal_prodsymb(D_ProdSymbolPtr arg0);
bool computations_are_complete(Binding arg0, Environment arg1);
SAttrPosMap convert_defocc_pos(SAttrPosMap arg0, D_RuleComputationPtr arg1);
SAttrPosMap convert_defocc_pos_attr(SAttrPosMap arg0, D_RuleSymbolAttributePtr arg1);
tStringType compute_missing_attr_message(Binding arg0, StringTableKey arg1, StringTableKey arg2, StringTableKey arg3, int arg4);
tStringType compute_existing_attr_message(Binding arg0, StringTableKeyList arg1, StringTableKey arg2, StringTableKey arg3, StringTableKey arg4, int arg5);
SymbAttrMap convert_def_occ(SymbAttrMap arg0, D_RuleComputationPtr arg1, Binding arg2, RSymbMap arg3, RSymbMap arg4);
SymbAttrMap rules_per_class(SymbAttrMap arg0, D_RuleComputationPtr arg1, Binding arg2, StringTableKey arg3, RSymbMap arg4, RSymbMap arg5, Environment arg6);
SymbAttrMap convert_rule_attr(SymbAttrMap arg0, D_RuleSymbolAttributePtr arg1, Binding arg2, RSymbMap arg3, RSymbMap arg4);
SymbAttrMap convert_rule_attr_class(SymbAttrMap arg0, D_RuleSymbolAttributePtr arg1, Binding arg2, StringTableKey arg3, RSymbMap arg4, RSymbMap arg5, Environment arg6);
SymbAttrMap insert_rules_in_smap(SymbAttrMap arg0, StringTableKey arg1, StringTableKey arg2, int arg3, RSymbMap arg4);
D_typePtr unify_constituent_args(D_types arg0, D_typePtr arg1, D_typePtr arg2);
D_types mk_append_results(D_typePtr arg0, D_typePtr arg1);
D_typePtr make_append_binary_result_type(tStringType arg0, tStringType arg1, tStringType arg2);
D_typePtr compute_concat_result_in_append(D_typePtr arg0, D_typePtr arg1);
StringTableKeyList compute_ruleclass_symbols(BindingList arg0, bool arg1);
IntList compute_ruleclass_indices(BindingList arg0, bool arg1);
IntList compute_rulevar_indices(BindingList arg0, Binding arg1);
StringTableKeyList compute_rulevar_symbols(BindingList arg0, Binding arg1);
D_types compute_tmp_types(StringTableKeyList arg0, IntList arg1, StringTableKey arg2, BindingList arg3, CoordPtr arg4, Environment arg5, Environment arg6);
D_typePtr compute_lambda_type(BindingList arg0, D_typePtr arg1, BindingList arg2);
D_typePtr store_rewrites_binop(D_typePtr arg0, D_typePtr arg1, D_typePtr arg2, D_typePtr arg3, BindingList arg4, CoordPtr arg5);
void replace_all_vars(bool arg0, D_typePtr arg1, D_typePtr arg2, Binding arg3, BindingList arg4, Environment arg5);
D_typePtr compute_call_types_for_rule(Binding arg0, Binding arg1, Binding arg2, Binding arg3, Environment arg4, BindingList arg5, CoordPtr arg6, StringTableKey arg7);
D_typePtr compute_result_type_rule(D_typePtr arg0, D_typePtr arg1, bool arg2, bool arg3, StringTableKey arg4, BindingList arg5, CoordPtr arg6);
Binding lookup_rule(StringTableKey arg0, StringTableKey arg1, Environment arg2, BindingList arg3);
D_typePtr apply_substs(D_typePtr arg0, BindingList arg1);
D_typePtr apply_argtypes_binds(D_typePtr arg0, D_typePtr arg1, D_types arg2, D_typePtr arg3, BindingList arg4, CoordPtr arg5, bool arg6);
D_typePtr compute_rule_calltype(Binding arg0);
bool prodsymb_is_treesymb(D_ProdSymbolPtr arg0);
void replace_all_binds(Binding arg0, CoordPtr arg1, Environment arg2, D_typePtr arg3, BindingList arg4);
PTGNode attrdir_to_ptg(AttributeDir arg0, DefTableKey arg1, CoordPtr arg2, StringTableKey arg3);
PTGNode report_noinfer(CoordPtr arg0, StringTableKey arg1);
PTGNodes create_empty_ptgnodes(BindingList arg0);
PTGNodes compute_assign_code(D_SymbolLocalAttributePtr arg0, D_SymbolAttributes arg1, BindingList arg2, PTGNodes arg3, Environment arg4, CoordPtr arg5, PTGNodes arg6);
void check_constituent_symbol_deps(D_SymbolAttributes arg0, CoordPtr arg1);
void check_including_symbol_deps(D_SymbolAttributes arg0, CoordPtr arg1);
StringTableKeyList get_including_symbols(D_SymbolAttributePtr arg0);
StringTableKeyList get_constituent_symbols(D_SymbolAttributePtr arg0);
StringTableKeyList get_remote_constituent_symbols(D_RemoteAttributePtr arg0);
StringTableKeyList get_remote_including_symbols(D_RemoteAttributePtr arg0);
PTGNode compute_dependency_code(D_SymbolAttributePtr arg0, StringTableKey arg1, Environment arg2, Binding arg3, CoordPtr arg4);
PTGNode compute_remote_dependency(D_RemoteAttributePtr arg0, StringTableKey arg1, Environment arg2, Binding arg3, CoordPtr arg4);
PTGNode compute_local_dependency(D_SymbolLocalAttributePtr arg0, StringTableKey arg1, Environment arg2, bool arg3, Binding arg4, CoordPtr arg5);
StringTableKey map_name_attr_dir(StringTableKey arg0, bool arg1, bool arg2, D_LocalAttributeClass arg3, StringTableKey arg4, bool arg5, CoordPtr arg6);
PTGNode aclass_to_ptg(D_LocalAttributeClass arg0, int arg1);
D_SymbolAttributes compute_additional_deps(D_SymbolLocalAttributePtr arg0, D_SymbolAttributes arg1, Binding arg2);
D_SymbolAttributes append_deps(D_SymbolAttributes arg0, D_SymbolAttributes arg1);
bool sets_dependency(StringTableKey arg0, D_SymbolComputationPtr arg1);
bool expr_is_attr(D_SymbolExpressionPtr arg0, StringTableKey arg1);
bool attribute_is_local_attr(D_SymbolAttributePtr arg0, StringTableKey arg1);
bool local_attr_name_equals(D_SymbolLocalAttributePtr arg0, StringTableKey arg1);
bool contains_dep(D_SymbolAttributes arg0, D_SymbolAttributePtr arg1);
D_SymbolAttributes get_dependencies(D_SymbolComputationPtr arg0);
PTGNodes apply_nodes(std::function<PTGNode(PTGNode, PTGNode)> arg0, PTGNodes arg1, PTGNodes arg2);
PTGNodes apply_nodes_3(std::function<PTGNode(PTGNode, PTGNode, PTGNode)> arg0, PTGNodes arg1, PTGNodes arg2, PTGNodes arg3);
PTGNodes apply_nodes_4(std::function<PTGNode(PTGNode, PTGNode, PTGNode, PTGNode)> arg0, PTGNodes arg1, PTGNodes arg2, PTGNodes arg3, PTGNodes arg4);
PTGNodes create_nonempty_ptgnodes(BindingList arg0);
Binding new_gendef(Environment arg0);
PTGNodes map_generated_names(PTGNodes arg0, PTGNode arg1);
PTGNode compute_symbol_lambda_code(BindingList arg0, PTGNodes arg1, Environment arg2, BindingList arg3, D_typePtr arg4);
PTGNode lambda_var_decl_code(Binding arg0, Environment arg1, Binding arg2);
PTGNodes map_nodes(std::function<PTGNode(PTGNode)> arg0, PTGNodes arg1);
PTGNodes compute_binop_code(PTGNodes arg0, PTGNodes arg1, D_BinOp arg2, D_typePtr arg3, D_typePtr arg4, D_typePtr arg5, BindingList arg6, D_RHSKind arg7, CoordPtr arg8);
PTGNode compute_binexpr_code(PTGNode arg0, PTGNode arg1, D_BinOp arg2, D_typePtr arg3, D_typePtr arg4, D_typePtr arg5, Binding arg6, D_RHSKind arg7, CoordPtr arg8);
std::function<PTGNode(PTGNode, PTGNode)> get_op_code(D_BinOp arg0, D_RHSKind arg1);
PTGNodes compute_access_codes(D_typePtr arg0, PTGNodes arg1, D_typePtr arg2, PTGNodes arg3, Environment arg4, BindingList arg5, D_RHSKind arg6, CoordPtr arg7);
PTGNode compute_access_code_lido(D_typePtr arg0, PTGNode arg1, D_typePtr arg2, PTGNode arg3, Environment arg4);
PTGNodes callref_to_codes(StringTableKey arg0, bool arg1, Binding arg2, Binding arg3, Binding arg4, D_typePtr arg5, Environment arg6, BindingList arg7, Environment arg8, D_RHSKind arg9, CoordPtr arg10);
PTGNodes compute_calltemplate_codes_lido(Binding arg0, StringTableKeyList arg1, BindingList arg2, D_typePtr arg3, Environment arg4, CoordPtr arg5, StringTableKey arg6, Environment arg7);
PTGNode templated_call_lido_code(Binding arg0, StringTableKeyList arg1, Environment arg2, Environment arg3, StringTableKey arg4, CoordPtr arg5, Binding arg6);
PTGNodes compute_emptylist_init(D_typePtr arg0, D_typePtr arg1, BindingList arg2, Environment arg3, CoordPtr arg4, bool arg5);
PTGNodes compute_emptylist_inits(D_types arg0, BindingList arg1, Environment arg2, CoordPtr arg3);
StringTableKey dtype_to_baseidn(D_typePtr arg0, Environment arg1);
StringTableKey dtype_to_baseidn_reverse(D_typePtr arg0, Environment arg1);
StringTableKey get_base_idn(Binding arg0, StringTableKey arg1, Environment arg2);
PTGNode emptylist_init_code(D_typePtr arg0, Environment arg1, CoordPtr arg2);
PTGNode access_class_to_ptg(D_LocalAttributeClass arg0, bool arg1);
PTGNode fold_skey_to_ptg(std::function<PTGNode(PTGNode, PTGNode)> arg0, std::function<PTGNode(StringTableKey)> arg1, StringTableKeyList arg2, PTGNode arg3);
PTGNodes infer_empty_codes(BindingList arg0, D_typePtr arg1, D_typePtr arg2, Environment arg3, CoordPtr arg4);
PTGNodes infer_single_codes(BindingList arg0, D_typePtr arg1, D_typePtr arg2, Environment arg3, CoordPtr arg4);
PTGNode compute_single_code(int arg0, int arg1, CoordPtr arg2, StringTableKey arg3, StringTableKey arg4);
PTGNodes infer_append_codes(BindingList arg0, D_typePtr arg1, D_typePtr arg2, Environment arg3, CoordPtr arg4);
PTGNodes infer_type_codes(BindingList arg0, D_typePtr arg1, D_typePtr arg2, Environment arg3, CoordPtr arg4);
PTGNodes apply_nodes_seqcomma(PTGNodes arg0, PTGNodes arg1);
StringTableKey new_genvar();
StringTableKey get_virtattr_name(D_VirtAttrPtr arg0);
void check_return_stat(bool arg0, int arg1, int arg2, D_RHSKind arg3, CoordPtr arg4);
D_SymbolAttributes map_from_gen(PTGNodes arg0, D_SymbolAttributePtr arg1);
PTGNodes call_code_constituent_lido(BindingList arg0, D_typePtr arg1, Binding arg2, Environment arg3, int arg4, PTGNodes arg5, Environment arg6, CoordPtr arg7);
void store_generated_virtattrs(BindingList arg0, D_VirtAttrs arg1);
void store_symbol_decl_codes(BindingList arg0, PTGNodes arg1, PTGNode arg2, PTGNode arg3, CoordPtr arg4);
PTGNodes compute_ruleattr_code(StringTableKeyList arg0, IntList arg1, StringTableKey arg2, bool arg3, BindingList arg4, Environment arg5, Environment arg6, D_RHSKind arg7, CoordPtr arg8);
StringTableKeyList fill_symbols(StringTableKeyList arg0, BindingList arg1);
IntList fill_indices(IntList arg0, BindingList arg1);
PTGNodes compute_rule_assign_code(D_RuleSymbolAttributePtr arg0, D_RuleSymbolAttributes arg1, PTGNodes arg2, BindingList arg3, PTGNodes arg4, PTGNodes arg5, bool arg6, Environment arg7, Environment arg8, Environment arg9, CoordPtr arg10);
void check_constituent_symbol_deps_r(D_RuleSymbolAttributes arg0, CoordPtr arg1);
void check_including_symbol_deps_r(D_RuleSymbolAttributes arg0, CoordPtr arg1);
StringTableKeyList get_including_symbols_r(D_RuleSymbolAttributePtr arg0);
StringTableKeyList get_constituent_symbols_r(D_RuleSymbolAttributePtr arg0);
PTGNode compute_dependency_code_r(D_RuleSymbolAttributePtr arg0, Environment arg1, Environment arg2, Binding arg3, CoordPtr arg4);
D_RuleSymbolAttributes compute_additional_deps_r(D_RuleSymbolAttributePtr arg0, D_RuleSymbolAttributes arg1, Binding arg2);
D_RuleSymbolAttributes append_deps_r(D_RuleSymbolAttributes arg0, D_RuleSymbolAttributes arg1);
bool contains_dep_r(D_RuleSymbolAttributes arg0, D_RuleSymbolAttributePtr arg1);
D_RuleSymbolAttributes get_dependencies_r(D_RuleComputationPtr arg0);
bool sets_dependency_r(D_RuleSymbolAttributePtr arg0, D_RuleComputationPtr arg1);
bool expr_is_attr_r(D_RuleSymbolAttributePtr arg0, D_RuleExpressionPtr arg1);
bool attrs_are_compatible(D_RuleSymbolAttributePtr arg0, D_RuleSymbolAttributePtr arg1);
CoordPtr get_comp_pos_x(D_RuleComputationPtr arg0);
CoordPtr get_attr_pos_r(D_RuleSymbolAttributePtr arg0);
PTGNodes compute_lhs_codes(BindingList arg0);
void print_info(D_typePtr arg0, Environment arg1);
PTGNode compute_globattr_code(StringTableKey arg0, AttributeDir arg1, Binding arg2, D_typePtr arg3, Environment arg4, CoordPtr arg5);
PTGNode compute_lido_type_code(Environment arg0, D_typePtr arg1);
PTGNode compute_symblocal_attrcodes(Environment arg0);
D_VAttrMap insert_vattr_map(D_VAttrMap arg0, StringTableKey arg1, D_VirtAttrs arg2);
PTGNode compute_symblocal_tree(Binding arg0);
PTGNode compute_vattr_codes(StringTableKey arg0, StringTableKeyList arg1);
PTGNode compute_symbattr_code(Binding arg0);
PTGNode compute_ruleattr_tree_code(Binding arg0);
PTGNode vattrs_to_default(StringTableKey arg0, D_VirtAttrs arg1);
PTGNode vattrs_to_symbdecl(StringTableKey arg0, D_VirtAttrs arg1, bool arg2);
PTGNode compute_ast_code(Environment arg0, Environment arg1);
PTGNode compute_tree_ast_code(Binding arg0, Environment arg1);
PTGNode compute_rule_ast_code(Binding arg0);
PTGNode prodsymbol_to_code(D_ProdSymbolPtr arg0);
PTGNode compute_symb_ast_code(Binding arg0, Environment arg1);
PTGNode compute_symbol_decl(StringTableKey arg0, SymbolType arg1, TermType arg2, StringTableKey arg3, StringTableKey arg4, Environment arg5, CoordPtr arg6);
PTGNode create_lng_defined_codes(Environment arg0);
AttributeDir combine_attr_dirs(AttributeDir arg0, AttributeDir arg1);
D_LocalAttributeClass compute_remote_dircontext(BindingList arg0);
D_LocalAttributeClass combine_dirs(D_LocalAttributeClass arg0, D_LocalAttributeClass arg1);
bool compute_is_lhs(StringTableKey arg0, int arg1, BindingList arg2);
bool compute_is_lhs_bind(StringTableKey arg0, int arg1, Binding arg2);
void store_inherited_symbols(TreeSymbs arg0, BindingList arg1);
StringTableKeyList replace_class_with_inherited(StringTableKeyList arg0, Environment arg1);
StringTableKeyList compute_inheriting_symbols(StringTableKey arg0, Environment arg1);
StringTableKeyList get_inheriting_names(Binding arg0);
void message_invalid_includes(bool arg0, StringTableKeyList arg1, CoordPtr arg2);
bool validate_remote_including(D_LocalAttributeClass arg0, StringTableKey arg1, StringTableKeyList arg2, Environment arg3, BindingList arg4, Environment arg5, CoordPtr arg6);
bool doesnt_reach_targets(Binding arg0, bool arg1, StringTableKeyList arg2, D_LocalAttributeClass arg3, Environment arg4, Environment arg5, StringTableKey arg6, D_VAttrMap arg7, bool arg8);
Binding context_to_bind(StringTableKey arg0, Environment arg1);
D_VAttrMap compute_reachable_up(StringTableKeyList arg0, Environment arg1);
D_VAttrMap compute_reachable_down(StringTableKeyList arg0, Environment arg1);
StringTableKeyList compute_contexts(D_LocalAttributeClass arg0, BindingList arg1, Environment arg2, Environment arg3, StringTableKey arg4);
StringTableKeyList compute_contexts_single(Binding arg0, SymbolType arg1, D_LocalAttributeClass arg2, Environment arg3, Environment arg4, StringTableKey arg5);
StringTableKey lookup_var_in_rule(StringTableKey arg0, Environment arg1, Binding arg2);
BindingList find_containing_rules(Binding arg0, Environment arg1, D_LocalAttributeClass arg2);
void message_invalid_remotes(StringTableKeyList arg0, CoordPtr arg1);
bool valid_constituents(StringTableKeyList arg0, StringTableKeyList arg1, bool arg2, bool arg3, D_LocalAttributeClass arg4, StringTableKey arg5, Environment arg6, BindingList arg7, Environment arg8, CoordPtr arg9);
int get_pathcount(Binding arg0, bool arg1, StringTableKeyList arg2, D_LocalAttributeClass arg3, Environment arg4, Environment arg5, StringTableKey arg6, D_VAttrMap arg7, StringTableKeyList arg8, bool arg9);

template<typename templ_a>
int atoone(templ_a arg0) { 
  return 1 /* RETURN6 */;
} 

template<typename templ_a>
templ_a id(templ_a arg0) { 
  return arg0 /* RETURN6 */;
} 

template<typename templ_a>
std::vector<templ_a> filter(std::function<bool(templ_a)> arg0, std::vector<templ_a> arg1) { 
  if(arg1.size() == 0) { 
    return std::vector<templ_a>() /* RETURN3 */;
  }
  auto _xs = arg1;
  auto _x = _xs.back();
  _xs.pop_back();
  
  if(arg0(_x)) { 
    auto _ret = filter<templ_a>(arg0, _xs);
    _ret.push_back(_x);
    return _ret /* RETURN9 */;
  } 
  else { 
    return filter<templ_a>(arg0, _xs) /* RETURN12 */;
  }
} 

template<typename templ_b, typename templ_a>
std::vector<templ_b> map(std::function<templ_b(templ_a)> arg0, std::vector<templ_a> arg1) { 
  auto _ret = std::vector<templ_b>();
  for(auto _x : arg1) { 
    _ret.push_back(arg0(_x));
  }
  return _ret;
} 

template<typename templ_b, typename templ_a>
templ_b fold(std::function<templ_b(templ_a, templ_b)> arg0, std::vector<templ_a> arg1, templ_b arg2) { 
  auto _ret = arg2;
  for(auto _x : arg1) { 
    _ret = arg0(_x, _ret);
  }
  return _ret;
} 

template<typename templ_a>
std::vector<templ_a> append_unique(std::vector<templ_a> arg0, std::vector<templ_a> arg1) { 
  std::vector<templ_a> x = append_list(arg0, arg1);
  stdsort(x.begin(), x.end());
  auto newend = stdunique(x.begin(), x.end());
  x.erase(newend, x.end());
  return x /* RETURN2 */;
} 

template<typename templ_b, typename templ_a>
std::vector<templ_b> map2(std::function<templ_b(templ_a)> arg0, std::vector<templ_a> arg1) { 
  auto _ret = std::vector<templ_b>();
  for(auto _x : arg1) { 
    _ret.push_back(arg0(_x));
  }
  return _ret;
} 


#define LNG_Call(x) x()
#define LNG_TupleGen std::make_tuple
#define LNG_TupleAccess(x, y) std::get<x>(y)
#define ECA(x, y) x::y
#define LNG_CallFn(a, b) a.b 
typedef std::ostream& PTGFile;

#define LNG_GenName_67() 0
#define LNG_GenName_66() false
#define LNG_GenName_65() NoIdn
#define LNG_GenName_64 [] (StringTableKey x, StringTableKey y) -> StringTableKey { \
  return (((x == NoIdn))? x : y);\
} \

#define LNG_GenName_63() false
#define LNG_GenName_62() false
#define LNG_GenName_61() NoIdn
#define LNG_GenName_60 [] (StringTableKey x, StringTableKey y) -> StringTableKey { \
  return (((x == NoIdn))? y : x);\
} \

#define LNG_GenName_59() 0
#define LNG_GenName_58() 0
#define LNG_GenName_57() 0
#define LNG_GenName_56() 0
#define LNG_GenName_55 [] (StringTableKey x) -> PTGNode { \
  return PTGString(StringTable(x));\
} \

#define LNG_GenName_54() 0
#define LNG_GenName_53 [] (StringTableKey x) -> PTGNode { \
  return (((x == NoIdn))? PTGNULL : PTGString(StringTable(x)));\
} \

#define LNG_GenName_52() 0
#define LNG_GenName_51() 0
#define LNG_GenName_50() 0
#define LNG_GenName_49() 0
#define LNG_GenName_48() 0
#define LNG_GenName_47() NoIdn
#define LNG_GenName_46 [] (StringTableKey x, StringTableKey y) -> StringTableKey { \
  return (((x == NoIdn))? y : x);\
} \

#define LNG_GenName_45() NoBinding
#define LNG_GenName_44 [] (Binding x, Binding y) -> Binding { \
  return (((x == NoBinding))? y : x);\
} \

#define LNG_GenName_43() 0
#define LNG_GenName_42() 0
#define LNG_GenName_41() 0
#define LNG_GenName_40() 0
#define LNG_GenName_39 [] (StringTableKey x) -> PTGNode { \
  return PTGString(StringTable(x));\
} \

#define LNG_GenName_38() 0
#define LNG_GenName_37() 0
#define LNG_GenName_36() 0
#define LNG_GenName_35() 0
#define LNG_GenName_34() 0
#define LNG_GenName_33() 0
#define LNG_GenName_32() 0
#define LNG_GenName_31() 0
#define LNG_GenName_30() false
#define LNG_GenName_29() 0
#define LNG_GenName_28() 0
#define LNG_GenName_27() false
#define LNG_GenName_26() false
#define LNG_GenName_25() PTGNULL
#define LNG_GenName_24() NoIdn
#define LNG_GenName_23() 0
#define LNG_GenName_22() 0
#define LNG_GenName_21() NoIdn
#define LNG_GenName_20 [] (StringTableKey x, StringTableKey y) -> StringTableKey { \
  return (((x == NoIdn))? y : x);\
} \

#define LNG_GenName_19() false
#define LNG_GenName_18 [] (StringTableKey x) -> bool { \
  return true;\
} \

#define LNG_GenName_17() 0
#define LNG_GenName_16() 0
#define LNG_GenName_15() 0
#define LNG_GenName_14 [] (StringTableKey x) -> PTGNode { \
  return (((x == NoIdn))? PTGNULL : PTGString(StringTable(x)));\
} \

#define LNG_GenName_13() NoIdn
#define LNG_GenName_12 [] (StringTableKey x, StringTableKey y) -> StringTableKey { \
  return (((x == NoIdn))? y : x);\
} \

#define LNG_GenName_11() 0
#define LNG_GenName_10() 0
#define LNG_GenName_9() 0
#define LNG_GenName_8() false
#define LNG_GenName_7() ECA(IndexType, IT_Unknown)
#define LNG_GenName_6() 0
#define LNG_GenName_5() 0
#define LNG_GenName_4() 0
#define LNG_GenName_3() 0
#define LNG_GenName_2() 0
#define LNG_GenName_1() 0
#define LNG_GenName_0() 0

extern NODEPTR MkrProgramRoot(CoordPtr);
extern NODEPTR MkrProgram(CoordPtr);
extern NODEPTR MkrDeclList2(CoordPtr);
extern NODEPTR MkrNoDecls(CoordPtr);
extern NODEPTR MkrDeclLoad(CoordPtr);
extern NODEPTR MkrLoadDecl(CoordPtr);
extern NODEPTR MkrDeclPTG(CoordPtr);
extern NODEPTR MkrPTGDecl(CoordPtr);
extern NODEPTR MkrPTGDefList2(CoordPtr);
extern NODEPTR MkrNoPTGDefs(CoordPtr);
extern NODEPTR MkrPTGDefinition(CoordPtr);
extern NODEPTR MkrPTGDefIdentifier(CoordPtr);
extern NODEPTR MkrPTGDefTypename(CoordPtr);
extern NODEPTR MkrPTGPatternList2(CoordPtr);
extern NODEPTR MkrPTGPatternEmpty(CoordPtr);
extern NODEPTR MkrPTGPatternInsert(CoordPtr);
extern NODEPTR MkrPTGPatternCall(CoordPtr);
extern NODEPTR MkrPTGPatternString(CoordPtr);
extern NODEPTR MkrPTGPatternOptional(CoordPtr);
extern NODEPTR MkrPTGInsertion(CoordPtr);
extern NODEPTR MkrPTGNoIndex(CoordPtr);
extern NODEPTR MkrPTGHasIndex(CoordPtr);
extern NODEPTR MkrPTGIndex(CoordPtr);
extern NODEPTR MkrPTGTypeNode(CoordPtr);
extern NODEPTR MkrPTGTypeEli(CoordPtr);
extern NODEPTR MkrPTGTypeGeneral(CoordPtr);
extern NODEPTR MkrEliTypeInt(CoordPtr);
extern NODEPTR MkrEliTypeString(CoordPtr);
extern NODEPTR MkrEliTypePointer(CoordPtr);
extern NODEPTR MkrEliTypeChar(CoordPtr);
extern NODEPTR MkrEliTypeDouble(CoordPtr);
extern NODEPTR MkrEliTypeFloat(CoordPtr);
extern NODEPTR MkrEliTypeLong(CoordPtr);
extern NODEPTR MkrEliTypeShort(CoordPtr);
extern NODEPTR MkrTypeRefSimple(CoordPtr);
extern NODEPTR MkrPTGCall(CoordPtr);
extern NODEPTR MkrPTGFnRefIdentifier(CoordPtr);
extern NODEPTR MkrPTGFnRefTypename(CoordPtr);
extern NODEPTR MkrPTGCallParamList2(CoordPtr);
extern NODEPTR MkrPTGCallParamEmpty(CoordPtr);
extern NODEPTR MkrPTGCallInsertion(CoordPtr);
extern NODEPTR MkrPTGOptionalPatterns(CoordPtr);
extern NODEPTR MkrDeclPDL(CoordPtr);
extern NODEPTR MkrPDLDecl(CoordPtr);
extern NODEPTR MkrPDLDefinitionList2(CoordPtr);
extern NODEPTR MkrPDLDefinitionListEmpty(CoordPtr);
extern NODEPTR MkrPDLLoad(CoordPtr);
extern NODEPTR MkrPDLDefInclude(CoordPtr);
extern NODEPTR MkrPDLInclude(CoordPtr);
extern NODEPTR MkrPDLRealDef(CoordPtr);
extern NODEPTR MkrPDLDefProperties(CoordPtr);
extern NODEPTR MkrPDLDefNameList2(CoordPtr);
extern NODEPTR MkrPDLDefNameList1(CoordPtr);
extern NODEPTR MkrPDLDefTypename(CoordPtr);
extern NODEPTR MkrPDLDefIdentifier(CoordPtr);
extern NODEPTR MkrPDLReferenceType(CoordPtr);
extern NODEPTR MkrDeclCopySource(CoordPtr);
extern NODEPTR MkrDeclCopyHead(CoordPtr);
extern NODEPTR MkrDeclCopyLido(CoordPtr);
extern NODEPTR MkrCopySourcePre(CoordPtr);
extern NODEPTR MkrCopySourcePost(CoordPtr);
extern NODEPTR MkrCopyHeadPre(CoordPtr);
extern NODEPTR MkrCopyHeadPost(CoordPtr);
extern NODEPTR MkrCopyLido(CoordPtr);
extern NODEPTR MkrDeclTypedef(CoordPtr);
extern NODEPTR MkrTypedefDecl(CoordPtr);
extern NODEPTR MkrTypingReference(CoordPtr);
extern NODEPTR MkrTypingList(CoordPtr);
extern NODEPTR MkrTypingBasic(CoordPtr);
extern NODEPTR MkrTypingTuple(CoordPtr);
extern NODEPTR MkrTypingMap(CoordPtr);
extern NODEPTR MkrTupleType(CoordPtr);
extern NODEPTR MkrTypeConList2(CoordPtr);
extern NODEPTR MkrTypeConList1(CoordPtr);
extern NODEPTR MkrTypeListConstruction(CoordPtr);
extern NODEPTR MkrConstructSimple(CoordPtr);
extern NODEPTR MkrTypeMapOf(CoordPtr);
extern NODEPTR MkrDeclData(CoordPtr);
extern NODEPTR MkrDataDecl(CoordPtr);
extern NODEPTR MkrDataName(CoordPtr);
extern NODEPTR MkrDataConstructorList2(CoordPtr);
extern NODEPTR MkrDataConstructorList1(CoordPtr);
extern NODEPTR MkrDataConstructorIsSimple(CoordPtr);
extern NODEPTR MkrDataConstructorIsComplex(CoordPtr);
extern NODEPTR MkrDataConstructorComplex(CoordPtr);
extern NODEPTR MkrDataConstructorArgumentList2(CoordPtr);
extern NODEPTR MkrDataConstructorArgumentList1(CoordPtr);
extern NODEPTR MkrDataConstructorArgument(CoordPtr);
extern NODEPTR MkrDataConstructorSimple(CoordPtr);
extern NODEPTR MkrDataConstructorDefId(CoordPtr);
extern NODEPTR MkrDataDefId(CoordPtr);
extern NODEPTR MkrTypeDefId(CoordPtr);
extern NODEPTR MkrDataRecordDecl(CoordPtr);
extern NODEPTR MkrDataRecordArgumentList2(CoordPtr);
extern NODEPTR MkrDataRecordArgumentList1(CoordPtr);
extern NODEPTR MkrDataRecordArgument(CoordPtr);
extern NODEPTR MkrDataRecordArgumentDefId(CoordPtr);
extern NODEPTR MkrDeclFunctionType(CoordPtr);
extern NODEPTR MkrFunctionTypeDecl(CoordPtr);
extern NODEPTR MkrFunctionType(CoordPtr);
extern NODEPTR MkrFunctionTypeList2(CoordPtr);
extern NODEPTR MkrFunctionTypeList1(CoordPtr);
extern NODEPTR MkrReturnTypeSimple(CoordPtr);
extern NODEPTR MkrFunctionDefId(CoordPtr);
extern NODEPTR MkrTypeOfFunction(CoordPtr);
extern NODEPTR MkrVoidTyping(CoordPtr);
extern NODEPTR MkrTypeTemplate(CoordPtr);
extern NODEPTR MkrTypeVariableDefId(CoordPtr);
extern NODEPTR MkrDeclFunctionImplementation(CoordPtr);
extern NODEPTR MkrFunctionImplementationDecl(CoordPtr);
extern NODEPTR MkrFunctionPatternList2(CoordPtr);
extern NODEPTR MkrFunctionPatternListEmpty(CoordPtr);
extern NODEPTR MkrFunctionPatternVar(CoordPtr);
extern NODEPTR MkrFunctionPatternIsConstant(CoordPtr);
extern NODEPTR MkrFunctionPatternIsListPattern(CoordPtr);
extern NODEPTR MkrFunctionPatternIsTuple(CoordPtr);
extern NODEPTR MkrFunctionPatternIsIgnored(CoordPtr);
extern NODEPTR MkrFunctionPatternIsSimpleCon(CoordPtr);
extern NODEPTR MkrFunctionPatternIsComplex(CoordPtr);
extern NODEPTR MkrFunctionConstructorPatternComplex(CoordPtr);
extern NODEPTR MkrFunctionPatternArgBindList2(CoordPtr);
extern NODEPTR MkrFunctionPatternArgBindList1(CoordPtr);
extern NODEPTR MkrFunctionPatternArgBind(CoordPtr);
extern NODEPTR MkrFunctionPatternConstant(CoordPtr);
extern NODEPTR MkrFunctionConstructorPatternSimple(CoordPtr);
extern NODEPTR MkrFunctionIngorePattern(CoordPtr);
extern NODEPTR MkrFunctionTuplePattern(CoordPtr);
extern NODEPTR MkrFunctionTuplePatternElementList2(CoordPtr);
extern NODEPTR MkrFunctionTuplePatternElementList1(CoordPtr);
extern NODEPTR MkrFunctionPatternListHead(CoordPtr);
extern NODEPTR MkrFunctionPatternListHeadAndTail(CoordPtr);
extern NODEPTR MkrFunctionTuplePatternElement(CoordPtr);
extern NODEPTR MkrRhsExpression(CoordPtr);
extern NODEPTR MkrFunctionVariableDefId(CoordPtr);
extern NODEPTR MkrFunctionExpressionChain(CoordPtr);
extern NODEPTR MkrFunctionExpressionIsBinop(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopChain(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopOR(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopAND(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopEQ(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopNE(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopLE(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopLT(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopGT(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopGE(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopConcat(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopADD(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopSUB(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopMUL(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopDIV(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopMOD(CoordPtr);
extern NODEPTR MkrFunctionExpressionBinopIsUnary(CoordPtr);
extern NODEPTR MkrFunctionUnaryIsPostfix(CoordPtr);
extern NODEPTR MkrFunctionUnaryIsIncrement(CoordPtr);
extern NODEPTR MkrFunctionUnaryIsMinus(CoordPtr);
extern NODEPTR MkrFunctionUnaryIsLocation(CoordPtr);
extern NODEPTR MkrFunctionUnaryIsNot(CoordPtr);
extern NODEPTR MkrFunctionUnaryIsPtr(CoordPtr);
extern NODEPTR MkrFunctionPostfixIsPrim(CoordPtr);
extern NODEPTR MkrFunctionPostfixIsIndex(CoordPtr);
extern NODEPTR MkrFunctionPostfixIsAccess(CoordPtr);
extern NODEPTR MkrFunctionPostfixIsListcon(CoordPtr);
extern NODEPTR MkrFunctionPrimaryIsConstant(CoordPtr);
extern NODEPTR MkrFunctionPrimaryIsExpression(CoordPtr);
extern NODEPTR MkrFunctionConstant(CoordPtr);
extern NODEPTR MkrConstantString(CoordPtr);
extern NODEPTR MkrConstantNumber(CoordPtr);
extern NODEPTR MkrConstantTrue(CoordPtr);
extern NODEPTR MkrConstantFalse(CoordPtr);
extern NODEPTR MkrConstantPos(CoordPtr);
extern NODEPTR MkrConstantEmptyList(CoordPtr);
extern NODEPTR MkrFunctionPrimaryIsCall(CoordPtr);
extern NODEPTR MkrFunctionCallParameters(CoordPtr);
extern NODEPTR MkrFunctionCallEmpty(CoordPtr);
extern NODEPTR MkrFunctionCallEmptyConstant(CoordPtr);
extern NODEPTR MkrCallIdentifier(CoordPtr);
extern NODEPTR MkrCallType(CoordPtr);
extern NODEPTR MkrCallEmpty(CoordPtr);
extern NODEPTR MkrFunctionCallParamList2(CoordPtr);
extern NODEPTR MkrFunctionCallParamList1(CoordPtr);
extern NODEPTR MkrFunctionCallParamIsExpr(CoordPtr);
extern NODEPTR MkrFunctionExprIsError(CoordPtr);
extern NODEPTR MkrFunctionErrorExpression(CoordPtr);
extern NODEPTR MkrFunctionExpressionIsIf(CoordPtr);
extern NODEPTR MkrFunctionIfExpression(CoordPtr);
extern NODEPTR MkrFunctionRhsIsGuards(CoordPtr);
extern NODEPTR MkrFunctionGuardList2(CoordPtr);
extern NODEPTR MkrFunctionGuardList1(CoordPtr);
extern NODEPTR MkrFunctionGuard(CoordPtr);
extern NODEPTR MkrRhsStatements(CoordPtr);
extern NODEPTR MkrFunctionDoStatementList2(CoordPtr);
extern NODEPTR MkrFunctionDoStatementListEmpty(CoordPtr);
extern NODEPTR MkrFunctionDoExpression(CoordPtr);
extern NODEPTR MkrFunctionDoCondition(CoordPtr);
extern NODEPTR MkrFunctionDoAssign(CoordPtr);
extern NODEPTR MkrFunctionDoReturn(CoordPtr);
extern NODEPTR MkrFunctionReturnStatement(CoordPtr);
extern NODEPTR MkrFunctionCondStatement(CoordPtr);
extern NODEPTR MkrFunctionCondStatUnit(CoordPtr);
extern NODEPTR MkrFunctionExpressionIsLet(CoordPtr);
extern NODEPTR MkrFunctionLetExpression(CoordPtr);
extern NODEPTR MkrFunctionLetVarDefList2(CoordPtr);
extern NODEPTR MkrFunctionLetVarDefList1(CoordPtr);
extern NODEPTR MkrFunctionLetVarDef(CoordPtr);
extern NODEPTR MkrFunctionDoIsMessage(CoordPtr);
extern NODEPTR MkrFunctionDoMessage(CoordPtr);
extern NODEPTR MkrFunctionExpressionIsLambda(CoordPtr);
extern NODEPTR MkrFunctionLambdaExpr(CoordPtr);
extern NODEPTR MkrNoTyping(CoordPtr);
extern NODEPTR MkrOptionalType(CoordPtr);
extern NODEPTR MkrFunctionDoIsAssignement(CoordPtr);
extern NODEPTR MkrFunctionDoIsEach(CoordPtr);
extern NODEPTR MkrFunctionStatAssign(CoordPtr);
extern NODEPTR MkrFunctionEachStatSingle(CoordPtr);
extern NODEPTR MkrFunctionEachStatMultiple(CoordPtr);
extern NODEPTR MkrDeclConstant(CoordPtr);
extern NODEPTR MkrConstantDecl(CoordPtr);
extern NODEPTR MkrConstantDefinition(CoordPtr);
extern NODEPTR MkrConstantDefIdentifier(CoordPtr);
extern NODEPTR MkrConstantDefTypename(CoordPtr);
extern NODEPTR MkrFunctionDoIsNoreturn(CoordPtr);
extern NODEPTR MkrFunctionNoReturn(CoordPtr);
extern NODEPTR MkrFunctionPrimaryIsTuple(CoordPtr);
extern NODEPTR MkrFunctionTupleConstruction(CoordPtr);
extern NODEPTR MkrFunctionTupleArgumentList2(CoordPtr);
extern NODEPTR MkrFunctionTupleArgumentList(CoordPtr);
extern NODEPTR MkrFunctionTupleArgument(CoordPtr);
extern NODEPTR MkrSymbolDefId(CoordPtr);
extern NODEPTR MkrDeclAbstreeClass(CoordPtr);
extern NODEPTR MkrDeclAbstree(CoordPtr);
extern NODEPTR MkrAbstreeClassDecl(CoordPtr);
extern NODEPTR MkrAbstreeDecl(CoordPtr);
extern NODEPTR MkrAbstreeDefIdIsTypename(CoordPtr);
extern NODEPTR MkrAbstreeDefIdIsIdent(CoordPtr);
extern NODEPTR MkrNoAbstreeChanges(CoordPtr);
extern NODEPTR MkrAbstreeChangeList2(CoordPtr);
extern NODEPTR MkrAbstreeChangeIsPrefix(CoordPtr);
extern NODEPTR MkrAbstreeChangeIsCombination(CoordPtr);
extern NODEPTR MkrAbstreeChangeIsInherit(CoordPtr);
extern NODEPTR MkrAbstreeChangeIsTrafo(CoordPtr);
extern NODEPTR MkrAbstreeChangeIsDependency(CoordPtr);
extern NODEPTR MkrAbstreePrefix(CoordPtr);
extern NODEPTR MkrNewRuleprefixIsTypename(CoordPtr);
extern NODEPTR MkrNewRuleprefixIsIdentifier(CoordPtr);
extern NODEPTR MkrAbstreeCombination(CoordPtr);
extern NODEPTR MkrAbstreeReferenceList2(CoordPtr);
extern NODEPTR MkrAbstreeReferenceList1(CoordPtr);
extern NODEPTR MkrAbstreeReferenceIsIdentifier(CoordPtr);
extern NODEPTR MkrAbstreeReferenceIsTypename(CoordPtr);
extern NODEPTR MkrAbstreeInheritance(CoordPtr);
extern NODEPTR MkrAbstreeTrafoTo(CoordPtr);
extern NODEPTR MkrAbstreeTrafoFrom(CoordPtr);
extern NODEPTR MkrAbstreeDependency(CoordPtr);
extern NODEPTR MkrNoAbstreeRules(CoordPtr);
extern NODEPTR MkrAbstreeRulesExist(CoordPtr);
extern NODEPTR MkrAbstreeRuleList2(CoordPtr);
extern NODEPTR MkrAbstreeRuleList1(CoordPtr);
extern NODEPTR MkrAbstreeRuleIsTermDecl(CoordPtr);
extern NODEPTR MkrAbstreeTermDeclAs(CoordPtr);
extern NODEPTR MkrAbstreeTermDeclIs(CoordPtr);
extern NODEPTR MkrAbstreeTermDeclName(CoordPtr);
extern NODEPTR MkrTreeSymbolDefId(CoordPtr);
extern NODEPTR MkrConversionReferenceTypename(CoordPtr);
extern NODEPTR MkrConversionReferenceIdentifier(CoordPtr);
extern NODEPTR MkrConversionReferenceNone(CoordPtr);
extern NODEPTR MkrAbstreeRuleIsProduction(CoordPtr);
extern NODEPTR MkrAbstreeProduction(CoordPtr);
extern NODEPTR MkrSymbolList2(CoordPtr);
extern NODEPTR MkrNoSymbols(CoordPtr);
extern NODEPTR MkrSymbolIsLiteral(CoordPtr);
extern NODEPTR MkrSymbolIsCreation(CoordPtr);
extern NODEPTR MkrSymbolIsReference(CoordPtr);
extern NODEPTR MkrSymbolReferenceOptTree(CoordPtr);
extern NODEPTR MkrSymbolReference(CoordPtr);
extern NODEPTR MkrNoTreeHint(CoordPtr);
extern NODEPTR MkrOptTreeHint(CoordPtr);
extern NODEPTR MkrRulenameExists(CoordPtr);
extern NODEPTR MkrNoRulename(CoordPtr);
extern NODEPTR MkrRulenameIsTypename(CoordPtr);
extern NODEPTR MkrRulenameIsIdentifier(CoordPtr);
extern NODEPTR MkrDeclGlobalAttribute(CoordPtr);
extern NODEPTR MkrGlobalAttributeDecl(CoordPtr);
extern NODEPTR MkrAttributeClassInherited(CoordPtr);
extern NODEPTR MkrAttributeClassSynthesized(CoordPtr);
extern NODEPTR MkrAttributeClassThread(CoordPtr);
extern NODEPTR MkrAttributeClassChain(CoordPtr);
extern NODEPTR MkrAttributeClassInfer(CoordPtr);
extern NODEPTR MkrGlobalAttributeDefId(CoordPtr);
extern NODEPTR MkrGlobalAttributeDefIdList2(CoordPtr);
extern NODEPTR MkrGlobalAttributeDefIdList1(CoordPtr);
extern NODEPTR MkrDeclSymbol(CoordPtr);
extern NODEPTR MkrSymbolDecl(CoordPtr);
extern NODEPTR MkrSymbolClassificationSymbol(CoordPtr);
extern NODEPTR MkrSymbolClassificationClassSymbol(CoordPtr);
extern NODEPTR MkrSymbolOptionList2(CoordPtr);
extern NODEPTR MkrNoSymbolOptions(CoordPtr);
extern NODEPTR MkrSymbolOptionAttributes(CoordPtr);
extern NODEPTR MkrSymbolOptionInheritance(CoordPtr);
extern NODEPTR MkrSymbolInheritance(CoordPtr);
extern NODEPTR MkrSymbolReferenceOptTreeList2(CoordPtr);
extern NODEPTR MkrSymbolReferenceOptTreeList1(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributes(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDeclList2(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDeclList1(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDeclSameType(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDeclDifferentType(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDeclNoClassList2(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDeclNoClassList1(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDeclNoClass(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDefIdList2(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDefIdList1(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeDefId(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeReference(CoordPtr);
extern NODEPTR MkrLocalAttributeClassUnknown(CoordPtr);
extern NODEPTR MkrLocalAttributeClassInherited(CoordPtr);
extern NODEPTR MkrLocalAttributeClassSynthesized(CoordPtr);
extern NODEPTR MkrLocalAttributeClassTail(CoordPtr);
extern NODEPTR MkrLocalAttributeClassHead(CoordPtr);
extern NODEPTR MkrLocalAttributeReference(CoordPtr);
extern NODEPTR MkrNoSymbolComputations(CoordPtr);
extern NODEPTR MkrSymbolComputationsExist(CoordPtr);
extern NODEPTR MkrSymbolComputationList2(CoordPtr);
extern NODEPTR MkrSymbolComputationListEmpty(CoordPtr);
extern NODEPTR MkrSymbolComputationIsAssign(CoordPtr);
extern NODEPTR MkrSymbolComputationIsChainstart(CoordPtr);
extern NODEPTR MkrSymbolComputationIsExpression(CoordPtr);
extern NODEPTR MkrSymbolComputationIsOrder(CoordPtr);
extern NODEPTR MkrSymbolChainStart(CoordPtr);
extern NODEPTR MkrNoSymbolDependency(CoordPtr);
extern NODEPTR MkrSymbolDependency(CoordPtr);
extern NODEPTR MkrSymbolSymbolAttributeReferencesSingle(CoordPtr);
extern NODEPTR MkrSymbolSymbolAttributeReferencesMulti(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeReferenceIsRemote(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeReferenceIsLocal(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeRefList2(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeRefList1(CoordPtr);
extern NODEPTR MkrSymbolDefOcc(CoordPtr);
extern NODEPTR MkrSymbolOrderedComputation(CoordPtr);
extern NODEPTR MkrSymbolOrderedStatementList2(CoordPtr);
extern NODEPTR MkrSymbolOrderedStatementList1(CoordPtr);
extern NODEPTR MkrSymbolOrderedStatementIsExpression(CoordPtr);
extern NODEPTR MkrSymbolOrderedStatementIsReturn(CoordPtr);
extern NODEPTR MkrSymbolOrderedStatementIsAssign(CoordPtr);
extern NODEPTR MkrSymbolRHSExpressionIsCompute(CoordPtr);
extern NODEPTR MkrSymbolRHSExpressionIsGuards(CoordPtr);
extern NODEPTR MkrSymbolGuardExpressionList2(CoordPtr);
extern NODEPTR MkrSymbolGuardExpressionList1(CoordPtr);
extern NODEPTR MkrSymbolGuardExpressionIsGuard(CoordPtr);
extern NODEPTR MkrSymbolGuardExpressionIsExpression(CoordPtr);
extern NODEPTR MkrSymbolGuardExpressionIsDefault(CoordPtr);
extern NODEPTR MkrSymbolExpressionChain(CoordPtr);
extern NODEPTR MkrSymbolExpressionIsWhen(CoordPtr);
extern NODEPTR MkrSymbolExpressionIsError(CoordPtr);
extern NODEPTR MkrSymbolExpressionIsIf(CoordPtr);
extern NODEPTR MkrSymbolExpressionIsLet(CoordPtr);
extern NODEPTR MkrSymbolExpressionIsLambda(CoordPtr);
extern NODEPTR MkrSymbolExpressionIsOrder(CoordPtr);
extern NODEPTR MkrSymbolLambda(CoordPtr);
extern NODEPTR MkrSymbolLambdaVarDefList2(CoordPtr);
extern NODEPTR MkrSymbolLambdaVarDefList1(CoordPtr);
extern NODEPTR MkrSymbolLambdaVarDef(CoordPtr);
extern NODEPTR MkrSymbolVariableDefId(CoordPtr);
extern NODEPTR MkrSymbolExpressionLet(CoordPtr);
extern NODEPTR MkrSymbolLetVarDefList2(CoordPtr);
extern NODEPTR MkrSymbolLetVarDefList1(CoordPtr);
extern NODEPTR MkrSymbolLetVarDef(CoordPtr);
extern NODEPTR MkrSymbolExpressionIf(CoordPtr);
extern NODEPTR MkrSymbolExpressionError(CoordPtr);
extern NODEPTR MkrSymbolExpressionWhenCond(CoordPtr);
extern NODEPTR MkrSymbolExpressionWhen(CoordPtr);
extern NODEPTR MkrSymbolExpressionIsBinary(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryChain(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryOR(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryAND(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryEQ(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryNE(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryLT(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryGT(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryLE(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryGE(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryConcat(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryADD(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinarySUB(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryMUL(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryDIV(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryMOD(CoordPtr);
extern NODEPTR MkrSymbolExpressionBinaryIsUnary(CoordPtr);
extern NODEPTR MkrSymbolExpressionUnaryIsPostfix(CoordPtr);
extern NODEPTR MkrSymbolExpressionUnaryIncr(CoordPtr);
extern NODEPTR MkrSymbolExpressionUnaryNEG(CoordPtr);
extern NODEPTR MkrSymbolExpressionUnaryNOT(CoordPtr);
extern NODEPTR MkrSymbolExpressionUnaryAddress(CoordPtr);
extern NODEPTR MkrSymbolExpressionUnaryReference(CoordPtr);
extern NODEPTR MkrSymbolExpressionPostfixIsPrimary(CoordPtr);
extern NODEPTR MkrSymbolExpressionPostfixIsIndex(CoordPtr);
extern NODEPTR MkrSymbolExpressionPostfixIsAccess(CoordPtr);
extern NODEPTR MkrSymbolExpressionPostfixIsListcon(CoordPtr);
extern NODEPTR MkrSymbolExpressionPrimaryIsConstant(CoordPtr);
extern NODEPTR MkrSymbolExpressionPrimaryIsCall(CoordPtr);
extern NODEPTR MkrSymbolExpressionPrimaryIsTuple(CoordPtr);
extern NODEPTR MkrSymbolExpressionPrimaryIsWrap(CoordPtr);
extern NODEPTR MkrSymbolTupleConstruction(CoordPtr);
extern NODEPTR MkrSymbolTupleArgumentList2(CoordPtr);
extern NODEPTR MkrSymbolTupleArgumentList1(CoordPtr);
extern NODEPTR MkrSymbolTupleArgument(CoordPtr);
extern NODEPTR MkrSymbolExpressionCallApplied(CoordPtr);
extern NODEPTR MkrSymbolExpressionCallEmpty(CoordPtr);
extern NODEPTR MkrSymbolExpressionCallVariable(CoordPtr);
extern NODEPTR MkrSymbolCallableReferenceIsIdentifier(CoordPtr);
extern NODEPTR MkrSymbolCallableReferenceIsTypename(CoordPtr);
extern NODEPTR MkrSymbolCallableReferenceIsTreeCon(CoordPtr);
extern NODEPTR MkrSymbolCallableReferenceIsEnd(CoordPtr);
extern NODEPTR MkrSymbolCallableReferenceIsAttribute(CoordPtr);
extern NODEPTR MkrSymbolCallParameterList2(CoordPtr);
extern NODEPTR MkrSymbolCallParameterList1(CoordPtr);
extern NODEPTR MkrSymbolCallParameter(CoordPtr);
extern NODEPTR MkrSymbolConstant(CoordPtr);
extern NODEPTR MkrDeclRuleSpecification(CoordPtr);
extern NODEPTR MkrRuleSpecification(CoordPtr);
extern NODEPTR MkrRuleGuardCondition(CoordPtr);
extern NODEPTR MkrRuleGuardRhsGuards(CoordPtr);
extern NODEPTR MkrRuleGuardRhsComputes(CoordPtr);
extern NODEPTR MkrRuleClassificationSingle(CoordPtr);
extern NODEPTR MkrRuleClassificationMultiple(CoordPtr);
extern NODEPTR MkrRuleClassificationSingleClass(CoordPtr);
extern NODEPTR MkrRuleClassificationMultipleClass(CoordPtr);
extern NODEPTR MkrRulePatternMultiple(CoordPtr);
extern NODEPTR MkrRulePatternSingle(CoordPtr);
extern NODEPTR MkrRulePatternVarious(CoordPtr);
extern NODEPTR MkrRuleReferenceIsTypename(CoordPtr);
extern NODEPTR MkrRuleReferenceIsIdentifier(CoordPtr);
extern NODEPTR MkrRuleReferenceList2(CoordPtr);
extern NODEPTR MkrRuleReferenceList1(CoordPtr);
extern NODEPTR MkrRuleLhsPatternSpecific(CoordPtr);
extern NODEPTR MkrRuleLhsPatternDontCare(CoordPtr);
extern NODEPTR MkrNoRuleVariableDef(CoordPtr);
extern NODEPTR MkrCreateRuleVariable(CoordPtr);
extern NODEPTR MkrOptProduction(CoordPtr);
extern NODEPTR MkrNoProduction(CoordPtr);
extern NODEPTR MkrRuleRHSPattern(CoordPtr);
extern NODEPTR MkrRuleRHSPatternSymbolList2(CoordPtr);
extern NODEPTR MkrNoRuleRHSPatternSymbols(CoordPtr);
extern NODEPTR MkrRulePatternSymbolAllRest(CoordPtr);
extern NODEPTR MkrRulePatternSymbolDontCare(CoordPtr);
extern NODEPTR MkrRulePatternSymbolConstructor(CoordPtr);
extern NODEPTR MkrRulePatternSymbolMatchTermString(CoordPtr);
extern NODEPTR MkrRulePatternSymbolMatchTermInt(CoordPtr);
extern NODEPTR MkrNoDeeperMatching(CoordPtr);
extern NODEPTR MkrOptDeeperRulePattern(CoordPtr);
extern NODEPTR MkrDeeperRulePatternSymbolList2(CoordPtr);
extern NODEPTR MkrNoDeeperRulePatternSymbols(CoordPtr);
extern NODEPTR MkrDeeperRulePatternSymbolDontCare(CoordPtr);
extern NODEPTR MkrDeeperRulePatternSymbolDontCareRest(CoordPtr);
extern NODEPTR MkrDeeperRulePatternSymbolMatchTermString(CoordPtr);
extern NODEPTR MkrDeeperRulePatternSymbolMatchTermInt(CoordPtr);
extern NODEPTR MkrDeeperMatchConstructor(CoordPtr);
extern NODEPTR MkrRuleComputationList2(CoordPtr);
extern NODEPTR MkrNoRuleComputations(CoordPtr);
extern NODEPTR MkrRuleComputeAssign(CoordPtr);
extern NODEPTR MkrRuleComputeExpr(CoordPtr);
extern NODEPTR MkrRuleComputeChainStart(CoordPtr);
extern NODEPTR MkrRuleComputeOrder(CoordPtr);
extern NODEPTR MkrRuleChainStart(CoordPtr);
extern NODEPTR MkrRuleDefOcc(CoordPtr);
extern NODEPTR MkrNoRuleDependency(CoordPtr);
extern NODEPTR MkrRuleDependencies(CoordPtr);
extern NODEPTR MkrRuleAttributeReferenceSingle(CoordPtr);
extern NODEPTR MkrRuleAttributeReferenceMulti(CoordPtr);
extern NODEPTR MkrRuleAttributeReferenceList2(CoordPtr);
extern NODEPTR MkrRuleAttributeReferenceList1(CoordPtr);
extern NODEPTR MkrRuleAttributeReferenceIsSymbolAttrRef(CoordPtr);
extern NODEPTR MkrRuleAttributeReferenceIsRemote(CoordPtr);
extern NODEPTR MkrRuleSymbolAttrRefIsLidoStyle(CoordPtr);
extern NODEPTR MkrRuleSymbolAttrRefIsLidoListOfStyle(CoordPtr);
extern NODEPTR MkrRuleSymbolAttrRefIsVarReference(CoordPtr);
extern NODEPTR MkrRuleAttrClassIsTail(CoordPtr);
extern NODEPTR MkrRuleAttrClassIsHead(CoordPtr);
extern NODEPTR MkrRuleVariableReference(CoordPtr);
extern NODEPTR MkrNoSymbolIndex(CoordPtr);
extern NODEPTR MkrSymbolIndex(CoordPtr);
extern NODEPTR MkrSymbolAttributeReference(CoordPtr);
extern NODEPTR MkrRuleOrderedComputation(CoordPtr);
extern NODEPTR MkrRuleOrderedStatementList2(CoordPtr);
extern NODEPTR MkrRuleOrderedStatementList1(CoordPtr);
extern NODEPTR MkrRuleOrderedStatementIsExpression(CoordPtr);
extern NODEPTR MkrRuleOrderedStatementIsAssign(CoordPtr);
extern NODEPTR MkrRuleOrderedStatementIsReturn(CoordPtr);
extern NODEPTR MkrRuleRhsExprIsExpr(CoordPtr);
extern NODEPTR MkrRuleRhsExprIsGuards(CoordPtr);
extern NODEPTR MkrRuleGuardExpressionList2(CoordPtr);
extern NODEPTR MkrRuleGuardExpressionList1(CoordPtr);
extern NODEPTR MkrRuleGuardIsOr(CoordPtr);
extern NODEPTR MkrRuleGuardIsCompute(CoordPtr);
extern NODEPTR MkrRuleGuardIsDefault(CoordPtr);
extern NODEPTR MkrRuleExpressionChain(CoordPtr);
extern NODEPTR MkrRuleExpressionIsWhen(CoordPtr);
extern NODEPTR MkrRuleExpressionIsError(CoordPtr);
extern NODEPTR MkrRuleExpressionIsIf(CoordPtr);
extern NODEPTR MkrRuleExpressionIsLet(CoordPtr);
extern NODEPTR MkrRuleExpressionIsLambda(CoordPtr);
extern NODEPTR MkrRuleExpressionIsOrder(CoordPtr);
extern NODEPTR MkrRuleLambda(CoordPtr);
extern NODEPTR MkrRuleLambdaVarDefList2(CoordPtr);
extern NODEPTR MkrRuleLambdaVarDefList1(CoordPtr);
extern NODEPTR MkrRuleLambdaVarDef(CoordPtr);
extern NODEPTR MkrRuleVariableDefId(CoordPtr);
extern NODEPTR MkrRuleExpressionLet(CoordPtr);
extern NODEPTR MkrRuleLetVarDefList2(CoordPtr);
extern NODEPTR MkrRuleLetVarDefList1(CoordPtr);
extern NODEPTR MkrRuleLetVarDef(CoordPtr);
extern NODEPTR MkrRuleExpressionIf(CoordPtr);
extern NODEPTR MkrRuleExpressionError(CoordPtr);
extern NODEPTR MkrRuleExpressionWhenCond(CoordPtr);
extern NODEPTR MkrRuleExpressionWhen(CoordPtr);
extern NODEPTR MkrRuleExpressionIsBinary(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryChain(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryOR(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryAND(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryEQ(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryNE(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryLT(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryGT(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryLE(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryGE(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryConcat(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryADD(CoordPtr);
extern NODEPTR MkrRuleExpressionBinarySUB(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryMUL(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryDIV(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryMOD(CoordPtr);
extern NODEPTR MkrRuleExpressionBinaryIsUnary(CoordPtr);
extern NODEPTR MkrRuleExpressionUnaryIsPostfix(CoordPtr);
extern NODEPTR MkrRuleExpressionUnaryIncr(CoordPtr);
extern NODEPTR MkrRuleExpressionUnaryNEG(CoordPtr);
extern NODEPTR MkrRuleExpressionUnaryNOT(CoordPtr);
extern NODEPTR MkrRuleExpressionUnaryAddress(CoordPtr);
extern NODEPTR MkrRuleExpressionUnaryReference(CoordPtr);
extern NODEPTR MkrRuleExpressionPostfixIsPrimary(CoordPtr);
extern NODEPTR MkrRuleExpressionPostfixIsIndex(CoordPtr);
extern NODEPTR MkrRuleExpressionPostfixIsAccess(CoordPtr);
extern NODEPTR MkrRuleExpressionPostfixIsListcon(CoordPtr);
extern NODEPTR MkrRuleExpressionPrimaryIsConstant(CoordPtr);
extern NODEPTR MkrRuleExpressionPrimaryIsCall(CoordPtr);
extern NODEPTR MkrRuleExpressionPrimaryIsTuple(CoordPtr);
extern NODEPTR MkrRuleExpressionPrimaryIsRemoteAttribute(CoordPtr);
extern NODEPTR MkrRuleExpressionPrimaryIsWrap(CoordPtr);
extern NODEPTR MkrRuleTupleConstruction(CoordPtr);
extern NODEPTR MkrRuleTupleArgumentList2(CoordPtr);
extern NODEPTR MkrRuleTupleArgumentList1(CoordPtr);
extern NODEPTR MkrRuleTupleArgument(CoordPtr);
extern NODEPTR MkrRuleExpressionCallApplied(CoordPtr);
extern NODEPTR MkrRuleExpressionCallEmpty(CoordPtr);
extern NODEPTR MkrRuleExpressionCallVariable(CoordPtr);
extern NODEPTR MkrRuleCallableReferenceIsIdentifier(CoordPtr);
extern NODEPTR MkrRuleCallableReferenceIsTypename(CoordPtr);
extern NODEPTR MkrRuleCallableReferenceIsEnd(CoordPtr);
extern NODEPTR MkrRuleCallableReferenceIsAttribute(CoordPtr);
extern NODEPTR MkrRuleCallableReferenceIsRule(CoordPtr);
extern NODEPTR MkrRuleCallParameterList2(CoordPtr);
extern NODEPTR MkrRuleCallParameterList1(CoordPtr);
extern NODEPTR MkrRuleCallParameter(CoordPtr);
extern NODEPTR MkrRuleConstant(CoordPtr);
extern NODEPTR MkrRemoteUp(CoordPtr);
extern NODEPTR MkrRemoteDown(CoordPtr);
extern NODEPTR MkrRemoteIncluding(CoordPtr);
extern NODEPTR MkrSymbolAttributeReferencesSingle(CoordPtr);
extern NODEPTR MkrSymbolAttributeReferencesMultiple(CoordPtr);
extern NODEPTR MkrSymbolAttributeReferenceList2(CoordPtr);
extern NODEPTR MkrSymbolAttributeReferenceList1(CoordPtr);
extern NODEPTR MkrSymbolAttributeRef(CoordPtr);
extern NODEPTR MkrRemoteConstituent(CoordPtr);
extern NODEPTR MkrOptSymbolIsSymbol(CoordPtr);
extern NODEPTR MkrNoSymbolReference(CoordPtr);
extern NODEPTR MkrRemoteOptionsWith(CoordPtr);
extern NODEPTR MkrNoRemoteOptions(CoordPtr);
extern NODEPTR MkrRemoteOptionsShield(CoordPtr);
extern NODEPTR MkrNoRemoteShield(CoordPtr);
extern NODEPTR MkrOptRemoteShield(CoordPtr);
extern NODEPTR MkrRemoteShieldMultiple(CoordPtr);
extern NODEPTR MkrRemoteShieldSingle(CoordPtr);
extern NODEPTR MkrRemoteUnshieldSelf(CoordPtr);
extern NODEPTR MkrSymbolReferenceList2(CoordPtr);
extern NODEPTR MkrSymbolReferenceList1(CoordPtr);
extern NODEPTR MkrRemoteAppendCall(CoordPtr);
extern NODEPTR MkrRemoteAppendLambda(CoordPtr);
extern NODEPTR MkrRemoteAppendIsBinary(CoordPtr);
extern NODEPTR MkrRemoteAppendADD(CoordPtr);
extern NODEPTR MkrRemoteAppendSUB(CoordPtr);
extern NODEPTR MkrRemoteAppendDIV(CoordPtr);
extern NODEPTR MkrRemoteAppendMUL(CoordPtr);
extern NODEPTR MkrRemoteAppendConcat(CoordPtr);
extern NODEPTR MkrRemoteAppendEQ(CoordPtr);
extern NODEPTR MkrRemoteAppendNE(CoordPtr);
extern NODEPTR MkrRemoteAppendLT(CoordPtr);
extern NODEPTR MkrRemoteAppendGT(CoordPtr);
extern NODEPTR MkrRemoteAppendLE(CoordPtr);
extern NODEPTR MkrRemoteAppendGE(CoordPtr);
extern NODEPTR MkrRemoteAppendOR(CoordPtr);
extern NODEPTR MkrRemoteAppendAND(CoordPtr);
extern NODEPTR MkrRemoteAppendMOD(CoordPtr);
extern NODEPTR MkrRemoteSingleCall(CoordPtr);
extern NODEPTR MkrRemoteSingleInfer(CoordPtr);
extern NODEPTR MkrRemoteSingleCreateList(CoordPtr);
extern NODEPTR MkrRemoteSingleLambda(CoordPtr);
extern NODEPTR MkrRemoteEmptyCall(CoordPtr);
extern NODEPTR MkrRemoteEmptyConstant(CoordPtr);
extern NODEPTR MkrRemoteEmptyInfer(CoordPtr);
extern NODEPTR MkrRemoteWithInfer(CoordPtr);
extern NODEPTR MkrRemoteWithLido(CoordPtr);
extern NODEPTR MkrRemoteWithInferTypes(CoordPtr);
extern NODEPTR MkrSymbolOptionUsing(CoordPtr);
extern NODEPTR MkrSymbolUsingAttribute(CoordPtr);
extern NODEPTR MkrSymbolUsingDeclList2(CoordPtr);
extern NODEPTR MkrSymbolUsingDeclList1(CoordPtr);
extern NODEPTR MkrSymbolUsingDecl(CoordPtr);
extern NODEPTR MkrSymbolUsingDeclWithoutClass(CoordPtr);
extern NODEPTR MkrSymbolUsingDeclsNoClassList2(CoordPtr);
extern NODEPTR MkrSymbolUsingDeclsNoClassList1(CoordPtr);
extern NODEPTR MkrSymbolUsingDeclNoClass(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeReferenceList2(CoordPtr);
extern NODEPTR MkrSymbolLocalAttributeReferenceList1(CoordPtr);
extern NODEPTR MkrLocalUsingReference(CoordPtr);
extern NODEPTR MkrSymbolAttrReferenceIsTree(CoordPtr);

#endif //lng_gen_HPP
