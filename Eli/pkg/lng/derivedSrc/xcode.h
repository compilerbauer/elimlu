switch (CaseTbl[c = *p++]) {
case 0:	/* sentinel - probably EOB */
  if (c == '\0') {
    p = TokenStart = TokenEnd = auxNUL(TokenStart, 0);
    if (*p) extcode = NORETURN;
    else {
      p = TokenStart = TokenEnd = auxEOF(TokenStart, 0);
      if (*p) extcode = NORETURN;
      else { extcode = EOFTOKEN; EndOfText(p, 0, &extcode, v); }
    }
    goto done;
  } else {
    obstack_grow(Csm_obstk, "char '", 6);
    obstack_cchgrow(Csm_obstk, c);
    message(
      ERROR,
      (char *)obstack_copy0(Csm_obstk, "' is not a token", 16),
      0,
      &curpos);
    TokenEnd = p;
    continue;
  }
  
case 1:	/* space */
  while (scanTbl[c = *p++] & 1<<0) ;
  TokenEnd = p - 1;
  continue;
case 2:	/* tab */
  do { StartLine -= TABSIZE(p - StartLine); }
  while (scanTbl[c = *p++] & 1<<1);
  TokenEnd = p - 1;
  continue;
case 4:	/* carriage return */
  if (*p == '\n') { TokenEnd = p; continue; }
case 3:	/* newline */
  do { LineNum++; } while (scanTbl[c = *p++] & 1<<2);
  StartLine = (TokenEnd = p - 1) - 1;
  continue;

case 5:	/* Entered on:  A-E G-S U-Z */
	St_27:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_73;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 115;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 6:	/* Entered on: 1-9 */
	St_19:
		if( scanTbl[(c= *p++)+0] & 1<< 4){ /*  0-9 */
			goto St_66;}
		else if( scanTbl[c+0] & 1<< 5){ /*  U u */
			goto St_64;}
		else if( scanTbl[c+0] & 1<< 6){ /*  L l */
			goto St_63;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 95;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 7:	/* Entered on:  b g j-k q x-z */
	St_35:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 8:	/* Entered on: | */
	St_54:
		if((c= *p++) ==124) {			goto St_124;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 32;
			goto done;
			}

case 9:	/* Entered on: { */
	St_53:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 31;
			CTextOrNot(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;

case 10:	/* Entered on: w */
	St_52:
		if( scanTbl[(c= *p++)+0] & 1<< 7){ /*  0-9 A-Z _ a-g j-z */
			goto St_78;}
		else if(c ==105) {			goto St_123;}
		else if(c ==104) {			goto St_122;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 11:	/* Entered on: v */
	St_51:
		if( scanTbl[(c= *p++)+256] & 1<< 0){ /*  0-9 A-Z _ a-h j-z */
			goto St_78;}
		else if(c ==105) {			goto St_121;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 12:	/* Entered on: u */
	St_50:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_120;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 13:	/* Entered on: t */
	St_49:
		if( scanTbl[(c= *p++)+256] & 1<< 2){ /*  0-9 A-Z _ b-d f-g i-n p-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_119;}
		else if(c ==111) {			goto St_118;}
		else if(c ==104) {			goto St_117;}
		else if(c ==101) {			goto St_116;}
		else if(c ==97) {			goto St_115;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 14:	/* Entered on: s */
	St_48:
		if( scanTbl[(c= *p++)+256] & 1<< 3){ /*  0-9 A-Z _ a-g i-n p-s u-x z */
			goto St_78;}
		else if(c ==121) {			goto St_114;}
		else if(c ==116) {			goto St_113;}
		else if(c ==111) {			goto St_112;}
		else if(c ==104) {			goto St_111;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 15:	/* Entered on: r */
	St_47:
		if( scanTbl[(c= *p++)+256] & 1<< 4){ /*  0-9 A-Z _ a-d f-t v-z */
			goto St_78;}
		else if(c ==117) {			goto St_110;}
		else if(c ==101) {			goto St_109;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 16:	/* Entered on: p */
	St_46:
		if( scanTbl[(c= *p++)+256] & 1<< 5){ /*  0-9 A-Z _ a-c e-n p-q s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_108;}
		else if(c ==114) {			goto St_107;}
		else if(c ==111) {			goto St_106;}
		else if(c ==100) {			goto St_105;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 17:	/* Entered on: o */
	St_45:
		if( scanTbl[(c= *p++)+256] & 1<< 6){ /*  0-9 A-Z _ a-e g-z */
			goto St_78;}
		else if(c ==102) {			goto St_104;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 18:	/* Entered on: n */
	St_44:
		if( scanTbl[(c= *p++)+256] & 1<< 7){ /*  0-9 A-Z _ a-d f-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_103;}
		else if(c ==101) {			goto St_102;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 19:	/* Entered on: m */
	St_43:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_101;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 20:	/* Entered on: l */
	St_42:
		if( scanTbl[(c= *p++)+512] & 1<< 0){ /*  0-9 A-Z _ a-d f-h j-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_100;}
		else if(c ==105) {			goto St_99;}
		else if(c ==101) {			goto St_98;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 21:	/* Entered on: i */
	St_41:
		if( scanTbl[(c= *p++)+512] & 1<< 1){ /*  0-9 A-Z _ a-e g-m o-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_97;}
		else if(c ==110) {			goto St_96;}
		else if(c ==102) {			goto St_95;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 22:	/* Entered on: h */
	St_40:
		if( scanTbl[(c= *p++)+512] & 1<< 2){ /*  0-9 A-Z _ b-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_94;}
		else if(c ==97) {			goto St_93;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 23:	/* Entered on: f */
	St_39:
		if( scanTbl[(c= *p++)+512] & 1<< 3){ /*  0-9 A-Z _ b-k m-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_92;}
		else if(c ==108) {			goto St_91;}
		else if(c ==97) {			goto St_90;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 24:	/* Entered on: e */
	St_38:
		if( scanTbl[(c= *p++)+512] & 1<< 4){ /*  0-9 A-Z _ b-k m o-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_89;}
		else if(c ==110) {			goto St_88;}
		else if(c ==108) {			goto St_87;}
		else if(c ==97) {			goto St_86;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 25:	/* Entered on: d */
	St_37:
		if( scanTbl[(c= *p++)+512] & 1<< 5){ /*  0-9 A-Z _ b-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_85;}
		else if(c ==97) {			goto St_84;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 26:	/* Entered on: c */
	St_36:
		if( scanTbl[(c= *p++)+512] & 1<< 6){ /*  0-9 A-Z _ a-g i-k m-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_83;}
		else if(c ==108) {			goto St_82;}
		else if(c ==104) {			goto St_81;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 27:	/* Entered on: a */
	St_34:
		if( scanTbl[(c= *p++)+512] & 1<< 7){ /*  0-9 A-Z _ a c-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_80;}
		else if(c ==98) {			goto St_79;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 28:	/* Entered on: _ */
	St_33:
		if((c= *p++) ==95) {			goto St_77;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 37;
			goto done;
			}

case 29:	/* Entered on: \ */
	St_31:
		if((c= *p++) ==92) {			goto St_76;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 89;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 30:	/* Entered on: T */
	St_29:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_73;}
		else if(c ==114) {			goto St_75;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 115;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 31:	/* Entered on: F */
	St_28:
		if( scanTbl[(c= *p++)+768] & 1<< 1){ /*  0-9 A-Z _ b-z */
			goto St_73;}
		else if(c ==97) {			goto St_74;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 115;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 32:	/* Entered on: > */
	St_24:
		if((c= *p++) ==61) {			goto St_72;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 78;
			goto done;
			}

case 33:	/* Entered on: = */
	St_23:
		if((c= *p++) ==62) {			goto St_71;}
		else if(c ==61) {			goto St_70;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 26;
			goto done;
			}

case 34:	/* Entered on: < */
	St_22:
		if((c= *p++) ==61) {			goto St_69;}
		else if(c ==45) {			goto St_68;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 76;
			goto done;
			}

case 35:	/* Entered on: : */
	St_20:
		if((c= *p++) ==58) {			goto St_67;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 57;
			goto done;
			}

case 36:	/* Entered on: 0 */
	St_18:
		if( scanTbl[(c= *p++)+768] & 1<< 2){ /*  0-7 */
			goto St_62;}
		else if( scanTbl[c+768] & 1<< 3){ /*  X x */
		extcode = 95;/* remember fallback*/
		TokenEnd = p-1;

		scan = NULL;
		proc = mkidn;
			goto St_65;}
		else if( scanTbl[c+0] & 1<< 5){ /*  U u */
			goto St_64;}
		else if( scanTbl[c+0] & 1<< 6){ /*  L l */
			goto St_63;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 95;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}

case 37:	/* Entered on: / */
	St_17:
		if((c= *p++) ==42) {			goto St_61;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 70;
			goto done;
			}

case 38:	/* Entered on: - */
	St_15:
		if((c= *p++) ==62) {			goto St_60;}
		else if(c ==45) {			goto St_59;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 46;
			goto done;
			}

case 39:	/* Entered on: + */
	St_13:
		if((c= *p++) ==43) {			goto St_58;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 47;
			goto done;
			}

case 40:	/* Entered on: ' */
	St_9:
			TokenEnd=p=auxPascalString(TokenStart, p-TokenStart);
			extcode = 90;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;

case 41:	/* Entered on: & */
	St_8:
		if((c= *p++) ==38) {			goto St_57;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 82;
			goto done;
			}

case 42:	/* Entered on: " */
	St_5:
			TokenEnd=p=auxCString(TokenStart, p-TokenStart);
			extcode = 110;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;

case 43:	/* Entered on: ! */
	St_4:
		if((c= *p++) ==61) {			goto St_56;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 81;
			goto done;
			}


default: TokenEnd=p; extcode=ExtCodeTbl[c]; goto done; /*  $-% (-* , . ; ?-@ [ ] } */
}
	St_56:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 59;
			goto done;
	St_57:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 68;
			goto done;
	St_58:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 50;
			goto done;
	St_59:
			TokenEnd=p=auxEOL(TokenStart, p-TokenStart);
			extcode = 15001;
			goto done;
	St_60:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 63;
			goto done;
	St_61:
			TokenEnd=p=auxCComment(TokenStart, p-TokenStart);
			extcode = 15001;
			goto done;
	St_63:
		if( scanTbl[(c= *p++)+0] & 1<< 5){ /*  U u */
			goto St_125;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 95;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_125:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 95;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
	St_64:
		if( scanTbl[(c= *p++)+0] & 1<< 6){ /*  L l */
			goto St_126;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 95;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_126:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 95;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
	St_65:
		if( scanTbl[(c= *p++)+768] & 1<< 4){ /*  0-9 A-F a-f */
			goto St_127;}
		else {--p; goto fallback; }
	St_127:
		/*  0-9 A-F a-f*/
		while(scanTbl[(c= *p++)+768] & 1<< 4);--p;
		if( scanTbl[(c= *p++)+0] & 1<< 5){ /*  U u */
			goto St_64;}
		else if( scanTbl[c+0] & 1<< 6){ /*  L l */
			goto St_63;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 95;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_62:
		/*  0-7*/
		while(scanTbl[(c= *p++)+768] & 1<< 2);--p;
		if( scanTbl[(c= *p++)+0] & 1<< 5){ /*  U u */
			goto St_64;}
		else if( scanTbl[c+0] & 1<< 6){ /*  L l */
			goto St_63;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 95;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_67:
		if((c= *p++) ==61) {			goto St_128;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 25;
			goto done;
			}
	St_128:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 12;
			goto done;
	St_68:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 3;
			goto done;
	St_69:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 75;
			goto done;
	St_70:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 58;
			goto done;
	St_71:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 7;
			goto done;
	St_72:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 77;
			goto done;
	St_74:
		if( scanTbl[(c= *p++)+768] & 1<< 5){ /*  0-9 A-Z _ a-k m-z */
			goto St_73;}
		else if(c ==108) {			goto St_129;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 115;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_129:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_73;}
		else if(c ==115) {			goto St_183;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 115;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_183:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_73;}
		else if(c ==101) {			goto St_233;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 115;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_233:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_73;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 85;
			goto done;
			}
	St_73:
		/*  0-9 A-Z _ a-z*/
		while(scanTbl[(c= *p++)+0] & 1<< 3);--p;
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 115;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
	St_75:
		if( scanTbl[(c= *p++)+768] & 1<< 7){ /*  0-9 A-Z _ a-t v-z */
			goto St_73;}
		else if(c ==117) {			goto St_130;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 115;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_130:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_73;}
		else if(c ==101) {			goto St_184;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 115;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_184:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_73;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 87;
			goto done;
			}
	St_76:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 89;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
	St_77:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 36;
			goto done;
	St_79:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_131;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_131:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_185;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_185:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_234;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_234:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_262;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_262:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_282;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_282:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 2;
			goto done;
			}
	St_78:
		/*  0-9 A-Z _ a-z*/
		while(scanTbl[(c= *p++)+0] & 1<< 3);--p;
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
	St_80:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 14;
			goto done;
			}
	St_81:
		if( scanTbl[(c= *p++)+768] & 1<< 1){ /*  0-9 A-Z _ b-z */
			goto St_78;}
		else if(c ==97) {			goto St_132;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_132:
		if( scanTbl[(c= *p++)+1024] & 1<< 1){ /*  0-9 A-Z _ a-h j-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_187;}
		else if(c ==105) {			goto St_186;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_186:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_235;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_235:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_263;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 18;
			goto done;
			}
	St_263:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_283;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_283:
		if( scanTbl[(c= *p++)+768] & 1<< 1){ /*  0-9 A-Z _ b-z */
			goto St_78;}
		else if(c ==97) {			goto St_294;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_294:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_303;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_303:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_307;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_307:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 106;
			goto done;
			}
	St_187:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 38;
			goto done;
			}
	St_82:
		if( scanTbl[(c= *p++)+768] & 1<< 1){ /*  0-9 A-Z _ b-z */
			goto St_78;}
		else if(c ==97) {			goto St_133;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_133:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_188;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_188:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_236;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_236:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 23;
			goto done;
			}
	St_83:
		if( scanTbl[(c= *p++)+1024] & 1<< 3){ /*  0-9 A-Z _ a-m p-z */
			goto St_78;}
		else if(c ==111) {			goto St_135;}
		else if(c ==110) {			goto St_134;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_134:
		if( scanTbl[(c= *p++)+1024] & 1<< 4){ /*  0-9 A-Z _ a-c e-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_190;}
		else if(c ==100) {			goto St_189;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_189:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 54;
			goto done;
			}
	St_190:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_237;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_237:
		if( scanTbl[(c= *p++)+1024] & 1<< 5){ /*  0-9 A-Z _ b-h j-z */
			goto St_78;}
		else if(c ==105) {			goto St_265;}
		else if(c ==97) {			goto St_264;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_264:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_284;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_284:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_295;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_295:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 24;
			goto done;
			}
	St_265:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_285;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_285:
		if( scanTbl[(c= *p++)+768] & 1<< 7){ /*  0-9 A-Z _ a-t v-z */
			goto St_78;}
		else if(c ==117) {			goto St_296;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_296:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_304;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_304:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_308;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_308:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_310;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_310:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_312;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 102;
			goto done;
			}
	St_312:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 101;
			goto done;
			}
	St_135:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_191;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_191:
		if( scanTbl[(c= *p++)+1024] & 1<< 6){ /*  0-9 A-Z _ a-c e-z */
			goto St_78;}
		else if(c ==100) {			goto St_238;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_238:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_266;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_266:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_286;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_286:
		if( scanTbl[(c= *p++)+256] & 1<< 6){ /*  0-9 A-Z _ a-e g-z */
			goto St_78;}
		else if(c ==102) {			goto St_297;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_297:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 83;
			goto done;
			}
	St_84:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_136;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_136:
		if( scanTbl[(c= *p++)+768] & 1<< 1){ /*  0-9 A-Z _ b-z */
			goto St_78;}
		else if(c ==97) {			goto St_192;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_192:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 33;
			goto done;
			}
	St_85:
		if( scanTbl[(c= *p++)+768] & 1<< 7){ /*  0-9 A-Z _ a-t v-z */
			goto St_78;}
		else if(c ==117) {			goto St_137;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 80;
			goto done;
			}
	St_137:
		if( scanTbl[(c= *p++)+1024] & 1<< 7){ /*  0-9 A-Z _ a c-z */
			goto St_78;}
		else if(c ==98) {			goto St_193;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_193:
		if( scanTbl[(c= *p++)+768] & 1<< 5){ /*  0-9 A-Z _ a-k m-z */
			goto St_78;}
		else if(c ==108) {			goto St_239;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_239:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_267;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_267:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 39;
			goto done;
			}
	St_86:
		if( scanTbl[(c= *p++)+1280] & 1<< 0){ /*  0-9 A-Z _ a-b d-z */
			goto St_78;}
		else if(c ==99) {			goto St_138;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_138:
		if( scanTbl[(c= *p++)+1280] & 1<< 1){ /*  0-9 A-Z _ a-g i-z */
			goto St_78;}
		else if(c ==104) {			goto St_194;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_194:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 55;
			goto done;
			}
	St_87:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_139;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_139:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_195;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_195:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 62;
			goto done;
			}
	St_88:
		if( scanTbl[(c= *p++)+1024] & 1<< 6){ /*  0-9 A-Z _ a-c e-z */
			goto St_78;}
		else if(c ==100) {			goto St_140;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_140:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 8;
			goto done;
			}
	St_89:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_141;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_141:
		if( scanTbl[(c= *p++)+1280] & 1<< 2){ /*  0-9 A-Z _ a-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_196;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_196:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_240;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_240:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 60;
			goto done;
			}
	St_90:
		if( scanTbl[(c= *p++)+768] & 1<< 5){ /*  0-9 A-Z _ a-k m-z */
			goto St_78;}
		else if(c ==108) {			goto St_142;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_142:
		if( scanTbl[(c= *p++)+1280] & 1<< 3){ /*  0-9 A-Z _ a-k m-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_198;}
		else if(c ==108) {			goto St_197;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_197:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_241;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_241:
		if( scanTbl[(c= *p++)+1280] & 1<< 1){ /*  0-9 A-Z _ a-g i-z */
			goto St_78;}
		else if(c ==104) {			goto St_268;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_268:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_287;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_287:
		if( scanTbl[(c= *p++)+1280] & 1<< 2){ /*  0-9 A-Z _ a-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_298;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_298:
		if( scanTbl[(c= *p++)+768] & 1<< 7){ /*  0-9 A-Z _ a-t v-z */
			goto St_78;}
		else if(c ==117) {			goto St_305;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_305:
		if( scanTbl[(c= *p++)+1280] & 1<< 4){ /*  0-9 A-Z _ a-f h-z */
			goto St_78;}
		else if(c ==103) {			goto St_309;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_309:
		if( scanTbl[(c= *p++)+1280] & 1<< 1){ /*  0-9 A-Z _ a-g i-z */
			goto St_78;}
		else if(c ==104) {			goto St_311;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_311:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 73;
			goto done;
			}
	St_198:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_242;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_242:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 84;
			goto done;
			}
	St_91:
		if( scanTbl[(c= *p++)+1280] & 1<< 2){ /*  0-9 A-Z _ a-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_143;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_143:
		if( scanTbl[(c= *p++)+768] & 1<< 1){ /*  0-9 A-Z _ b-z */
			goto St_78;}
		else if(c ==97) {			goto St_199;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_199:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_243;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_243:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 40;
			goto done;
			}
	St_92:
		if( scanTbl[(c= *p++)+1280] & 1<< 2){ /*  0-9 A-Z _ a-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_144;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_144:
		if( scanTbl[(c= *p++)+1280] & 1<< 5){ /*  0-9 A-Z _ a-l n-z */
			goto St_78;}
		else if(c ==109) {			goto St_200;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_200:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 16;
			goto done;
			}
	St_93:
		if( scanTbl[(c= *p++)+1280] & 1<< 6){ /*  0-9 A-Z _ a-u w-z */
			goto St_78;}
		else if(c ==118) {			goto St_145;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_145:
		if( scanTbl[(c= *p++)+256] & 1<< 0){ /*  0-9 A-Z _ a-h j-z */
			goto St_78;}
		else if(c ==105) {			goto St_201;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_201:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_244;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_244:
		if( scanTbl[(c= *p++)+1280] & 1<< 4){ /*  0-9 A-Z _ a-f h-z */
			goto St_78;}
		else if(c ==103) {			goto St_269;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_269:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 113;
			goto done;
			}
	St_94:
		if( scanTbl[(c= *p++)+768] & 1<< 1){ /*  0-9 A-Z _ b-z */
			goto St_78;}
		else if(c ==97) {			goto St_146;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_146:
		if( scanTbl[(c= *p++)+1024] & 1<< 6){ /*  0-9 A-Z _ a-c e-z */
			goto St_78;}
		else if(c ==100) {			goto St_202;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_202:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 28;
			goto done;
			}
	St_95:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 61;
			goto done;
			}
	St_96:
		if( scanTbl[(c= *p++)+1280] & 1<< 7){ /*  0-9 A-Z _ a-b d-e g i-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_150;}
		else if(c ==104) {			goto St_149;}
		else if(c ==102) {			goto St_148;}
		else if(c ==99) {			goto St_147;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 65;
			goto done;
			}
	St_147:
		if( scanTbl[(c= *p++)+768] & 1<< 5){ /*  0-9 A-Z _ a-k m-z */
			goto St_78;}
		else if(c ==108) {			goto St_203;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_203:
		if( scanTbl[(c= *p++)+768] & 1<< 7){ /*  0-9 A-Z _ a-t v-z */
			goto St_78;}
		else if(c ==117) {			goto St_245;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_245:
		if( scanTbl[(c= *p++)+1024] & 1<< 6){ /*  0-9 A-Z _ a-c e-z */
			goto St_78;}
		else if(c ==100) {			goto St_270;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_270:
		if( scanTbl[(c= *p++)+256] & 1<< 0){ /*  0-9 A-Z _ a-h j-z */
			goto St_78;}
		else if(c ==105) {			goto St_288;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_288:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_299;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_299:
		if( scanTbl[(c= *p++)+1280] & 1<< 4){ /*  0-9 A-Z _ a-f h-z */
			goto St_78;}
		else if(c ==103) {			goto St_306;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_306:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 104;
			goto done;
			}
	St_148:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_204;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_204:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_246;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_246:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 19;
			goto done;
			}
	St_149:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_205;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 20;
			goto done;
			}
	St_205:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_247;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_247:
		if( scanTbl[(c= *p++)+256] & 1<< 0){ /*  0-9 A-Z _ a-h j-z */
			goto St_78;}
		else if(c ==105) {			goto St_271;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_271:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_289;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_289:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_300;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_300:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 4;
			goto done;
			}
	St_150:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 41;
			goto done;
			}
	St_97:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 5;
			goto done;
			}
	St_98:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_151;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_151:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 64;
			goto done;
			}
	St_99:
		if( scanTbl[(c= *p++)+1024] & 1<< 6){ /*  0-9 A-Z _ a-c e-z */
			goto St_78;}
		else if(c ==100) {			goto St_152;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_152:
		if( scanTbl[(c= *p++)+1280] & 1<< 2){ /*  0-9 A-Z _ a-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_206;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_206:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 29;
			goto done;
			}
	St_100:
		if( scanTbl[(c= *p++)+1536] & 1<< 0){ /*  0-9 A-Z _ b-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_154;}
		else if(c ==97) {			goto St_153;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_153:
		if( scanTbl[(c= *p++)+1024] & 1<< 6){ /*  0-9 A-Z _ a-c e-z */
			goto St_78;}
		else if(c ==100) {			goto St_207;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_207:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 91;
			goto done;
			}
	St_154:
		if( scanTbl[(c= *p++)+1280] & 1<< 4){ /*  0-9 A-Z _ a-f h-z */
			goto St_78;}
		else if(c ==103) {			goto St_208;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_208:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 42;
			goto done;
			}
	St_101:
		if( scanTbl[(c= *p++)+1280] & 1<< 4){ /*  0-9 A-Z _ a-f h-z */
			goto St_78;}
		else if(c ==103) {			goto St_155;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_155:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 53;
			goto done;
			}
	St_102:
		if( scanTbl[(c= *p++)+1536] & 1<< 1){ /*  0-9 A-Z _ a-v x-z */
			goto St_78;}
		else if(c ==119) {			goto St_156;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_156:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_209;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_209:
		if( scanTbl[(c= *p++)+1536] & 1<< 2){ /*  0-9 A-Z _ a-x z */
			goto St_78;}
		else if(c ==121) {			goto St_248;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_248:
		if( scanTbl[(c= *p++)+1536] & 1<< 3){ /*  0-9 A-Z _ a-o q-z */
			goto St_78;}
		else if(c ==112) {			goto St_272;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_272:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_290;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_290:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 94;
			goto done;
			}
	St_103:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_157;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_157:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_210;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_210:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_249;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_249:
		if( scanTbl[(c= *p++)+768] & 1<< 7){ /*  0-9 A-Z _ a-t v-z */
			goto St_78;}
		else if(c ==117) {			goto St_273;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_273:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_291;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_291:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_301;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_301:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 74;
			goto done;
			}
	St_104:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 56;
			goto done;
			}
	St_105:
		if( scanTbl[(c= *p++)+768] & 1<< 5){ /*  0-9 A-Z _ a-k m-z */
			goto St_78;}
		else if(c ==108) {			goto St_158;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_158:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 98;
			goto done;
			}
	St_106:
		if( scanTbl[(c= *p++)+1536] & 1<< 4){ /*  0-9 A-Z _ a-h j-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_160;}
		else if(c ==105) {			goto St_159;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_159:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_211;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_211:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_250;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_250:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_274;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_274:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_292;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_292:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 43;
			goto done;
			}
	St_160:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_212;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_212:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 27;
			goto done;
			}
	St_107:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_161;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_161:
		if( scanTbl[(c= *p++)+256] & 1<< 6){ /*  0-9 A-Z _ a-e g-z */
			goto St_78;}
		else if(c ==102) {			goto St_213;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_213:
		if( scanTbl[(c= *p++)+256] & 1<< 0){ /*  0-9 A-Z _ a-h j-z */
			goto St_78;}
		else if(c ==105) {			goto St_251;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_251:
		if( scanTbl[(c= *p++)+1536] & 1<< 5){ /*  0-9 A-Z _ a-w y-z */
			goto St_78;}
		else if(c ==120) {			goto St_275;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_275:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_293;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_293:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_302;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_302:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 9;
			goto done;
			}
	St_108:
		if( scanTbl[(c= *p++)+1280] & 1<< 4){ /*  0-9 A-Z _ a-f h-z */
			goto St_78;}
		else if(c ==103) {			goto St_162;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_162:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 99;
			goto done;
			}
	St_109:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_163;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_163:
		if( scanTbl[(c= *p++)+768] & 1<< 7){ /*  0-9 A-Z _ a-t v-z */
			goto St_78;}
		else if(c ==117) {			goto St_214;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_214:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_252;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_252:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_276;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_276:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 79;
			goto done;
			}
	St_110:
		if( scanTbl[(c= *p++)+768] & 1<< 5){ /*  0-9 A-Z _ a-k m-z */
			goto St_78;}
		else if(c ==108) {			goto St_164;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_164:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_215;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_215:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_253;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 107;
			goto done;
			}
	St_253:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 108;
			goto done;
			}
	St_111:
		if( scanTbl[(c= *p++)+1536] & 1<< 6){ /*  0-9 A-Z _ a-h j-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_166;}
		else if(c ==105) {			goto St_165;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_165:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_216;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_216:
		if( scanTbl[(c= *p++)+768] & 1<< 5){ /*  0-9 A-Z _ a-k m-z */
			goto St_78;}
		else if(c ==108) {			goto St_254;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_254:
		if( scanTbl[(c= *p++)+1024] & 1<< 6){ /*  0-9 A-Z _ a-c e-z */
			goto St_78;}
		else if(c ==100) {			goto St_277;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_277:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 105;
			goto done;
			}
	St_166:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_217;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_217:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_255;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_255:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 44;
			goto done;
			}
	St_112:
		if( scanTbl[(c= *p++)+768] & 1<< 7){ /*  0-9 A-Z _ a-t v-z */
			goto St_78;}
		else if(c ==117) {			goto St_167;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_167:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_218;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_218:
		if( scanTbl[(c= *p++)+1280] & 1<< 0){ /*  0-9 A-Z _ a-b d-z */
			goto St_78;}
		else if(c ==99) {			goto St_256;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_256:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_278;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_278:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 30;
			goto done;
			}
	St_113:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_168;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_168:
		if( scanTbl[(c= *p++)+256] & 1<< 0){ /*  0-9 A-Z _ a-h j-z */
			goto St_78;}
		else if(c ==105) {			goto St_219;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_219:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_257;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_257:
		if( scanTbl[(c= *p++)+1280] & 1<< 4){ /*  0-9 A-Z _ a-f h-z */
			goto St_78;}
		else if(c ==103) {			goto St_279;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_279:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 45;
			goto done;
			}
	St_114:
		if( scanTbl[(c= *p++)+1536] & 1<< 7){ /*  0-9 A-Z _ a-l o-z */
			goto St_78;}
		else if(c ==110) {			goto St_170;}
		else if(c ==109) {			goto St_169;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_169:
		if( scanTbl[(c= *p++)+1024] & 1<< 7){ /*  0-9 A-Z _ a c-z */
			goto St_78;}
		else if(c ==98) {			goto St_220;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_220:
		if( scanTbl[(c= *p++)+1280] & 1<< 2){ /*  0-9 A-Z _ a-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_258;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_258:
		if( scanTbl[(c= *p++)+768] & 1<< 5){ /*  0-9 A-Z _ a-k m-z */
			goto St_78;}
		else if(c ==108) {			goto St_280;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_280:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 112;
			goto done;
			}
	St_170:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_221;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_221:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 21;
			goto done;
			}
	St_115:
		if( scanTbl[(c= *p++)+256] & 1<< 0){ /*  0-9 A-Z _ a-h j-z */
			goto St_78;}
		else if(c ==105) {			goto St_171;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_171:
		if( scanTbl[(c= *p++)+768] & 1<< 5){ /*  0-9 A-Z _ a-k m-z */
			goto St_78;}
		else if(c ==108) {			goto St_222;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_222:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 92;
			goto done;
			}
	St_116:
		if( scanTbl[(c= *p++)+768] & 1<< 0){ /*  0-9 A-Z _ a-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_172;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_172:
		if( scanTbl[(c= *p++)+1280] & 1<< 5){ /*  0-9 A-Z _ a-l n-z */
			goto St_78;}
		else if(c ==109) {			goto St_223;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_223:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 13;
			goto done;
			}
	St_117:
		if( scanTbl[(c= *p++)+1792] & 1<< 0){ /*  0-9 A-Z _ a-d f-h j-q s-z */
			goto St_78;}
		else if(c ==114) {			goto St_175;}
		else if(c ==105) {			goto St_174;}
		else if(c ==101) {			goto St_173;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_173:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_224;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_224:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 52;
			goto done;
			}
	St_174:
		if( scanTbl[(c= *p++)+256] & 1<< 1){ /*  0-9 A-Z _ a-r t-z */
			goto St_78;}
		else if(c ==115) {			goto St_225;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_225:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 93;
			goto done;
			}
	St_175:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_226;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_226:
		if( scanTbl[(c= *p++)+768] & 1<< 1){ /*  0-9 A-Z _ b-z */
			goto St_78;}
		else if(c ==97) {			goto St_259;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_259:
		if( scanTbl[(c= *p++)+1024] & 1<< 6){ /*  0-9 A-Z _ a-c e-z */
			goto St_78;}
		else if(c ==100) {			goto St_281;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_281:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 22;
			goto done;
			}
	St_118:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 17;
			goto done;
			}
	St_119:
		if( scanTbl[(c= *p++)+1792] & 1<< 1){ /*  0-9 A-Z _ b-d f-t v-z */
			goto St_78;}
		else if(c ==117) {			goto St_178;}
		else if(c ==101) {			goto St_177;}
		else if(c ==97) {			goto St_176;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_176:
		if( scanTbl[(c= *p++)+256] & 1<< 6){ /*  0-9 A-Z _ a-e g-z */
			goto St_78;}
		else if(c ==102) {			goto St_227;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_227:
		if( scanTbl[(c= *p++)+1280] & 1<< 2){ /*  0-9 A-Z _ a-n p-z */
			goto St_78;}
		else if(c ==111) {			goto St_260;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_260:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 15;
			goto done;
			}
	St_177:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_228;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_228:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 111;
			goto done;
			}
	St_178:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_229;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_229:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 86;
			goto done;
			}
	St_120:
		if( scanTbl[(c= *p++)+256] & 1<< 0){ /*  0-9 A-Z _ a-h j-z */
			goto St_78;}
		else if(c ==105) {			goto St_179;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_179:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_230;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_230:
		if( scanTbl[(c= *p++)+1280] & 1<< 4){ /*  0-9 A-Z _ a-f h-z */
			goto St_78;}
		else if(c ==103) {			goto St_261;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_261:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 114;
			goto done;
			}
	St_121:
		if( scanTbl[(c= *p++)+768] & 1<< 1){ /*  0-9 A-Z _ b-z */
			goto St_78;}
		else if(c ==97) {			goto St_180;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_180:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 96;
			goto done;
			}
	St_122:
		if( scanTbl[(c= *p++)+768] & 1<< 6){ /*  0-9 A-Z _ a-d f-z */
			goto St_78;}
		else if(c ==101) {			goto St_181;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_181:
		if( scanTbl[(c= *p++)+1024] & 1<< 2){ /*  0-9 A-Z _ a-m o-z */
			goto St_78;}
		else if(c ==110) {			goto St_231;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_231:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 51;
			goto done;
			}
	St_123:
		if( scanTbl[(c= *p++)+1024] & 1<< 0){ /*  0-9 A-Z _ a-s u-z */
			goto St_78;}
		else if(c ==116) {			goto St_182;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_182:
		if( scanTbl[(c= *p++)+1280] & 1<< 1){ /*  0-9 A-Z _ a-g i-z */
			goto St_78;}
		else if(c ==104) {			goto St_232;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 88;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
	St_232:
		if( scanTbl[(c= *p++)+0] & 1<< 3){ /*  0-9 A-Z _ a-z */
			goto St_78;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 10;
			goto done;
			}
	St_124:
			TokenEnd= p; /* FINAL, no auxscan, must set */
			extcode = 69;
			goto done;
	St_66:
		/*  0-9*/
		while(scanTbl[(c= *p++)+0] & 1<< 4);--p;
		if( scanTbl[(c= *p++)+0] & 1<< 5){ /*  U u */
			goto St_64;}
		else if( scanTbl[c+0] & 1<< 6){ /*  L l */
			goto St_63;}
		else {
			TokenEnd= (--p); /* FINAL, no auxscan, must set */
			extcode = 95;
			mkidn(TokenStart, TokenEnd-TokenStart,&extcode,v);
			goto done;
			}
