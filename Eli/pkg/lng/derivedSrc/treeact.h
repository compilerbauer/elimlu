

#define _TERMACT_rule_07 \
	_currn->_ATsym=_TERM1;

#define _TERMACT_rule_06 \
	_currn->_ATsym=_TERM1;

#define _TERMACT_rule_05 \
	_currn->_ATTERM_1=_TERM1; \
	_currn->_ATforced_sym=_TERM1;

#define _TERMACT_rule_04 \
	_currn->_ATTERM_1=_TERM1;

#define _TERMACT_rule_03 \
	_currn->_ATTERM_1=_TERM1;

#define _TERMACT_rule_02 \
	_currn->_ATsym=_TERM1;

#define _TERMACT_rule_01 \
	_currn->_ATTERM_1=_TERM1;

#define _TERMACT_rProgramRoot

#define _TERMACT_rProgram \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rDeclList2

#define _TERMACT_rNoDecls

#define _TERMACT_rDeclLoad

#define _TERMACT_rLoadDecl

#define _TERMACT_rDeclPTG

#define _TERMACT_rPTGDecl

#define _TERMACT_rPTGDefList2

#define _TERMACT_rNoPTGDefs

#define _TERMACT_rPTGDefinition \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rPTGDefIdentifier \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rPTGDefTypename \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rPTGPatternList2

#define _TERMACT_rPTGPatternEmpty

#define _TERMACT_rPTGPatternInsert

#define _TERMACT_rPTGPatternCall

#define _TERMACT_rPTGPatternString

#define _TERMACT_rPTGPatternOptional

#define _TERMACT_rPTGInsertion

#define _TERMACT_rPTGNoIndex

#define _TERMACT_rPTGHasIndex

#define _TERMACT_rPTGIndex

#define _TERMACT_rPTGTypeNode

#define _TERMACT_rPTGTypeEli

#define _TERMACT_rPTGTypeGeneral

#define _TERMACT_rEliTypeInt

#define _TERMACT_rEliTypeString

#define _TERMACT_rEliTypePointer

#define _TERMACT_rEliTypeChar

#define _TERMACT_rEliTypeDouble

#define _TERMACT_rEliTypeFloat

#define _TERMACT_rEliTypeLong

#define _TERMACT_rEliTypeShort

#define _TERMACT_rTypeRefSimple \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rPTGCall

#define _TERMACT_rPTGFnRefIdentifier

#define _TERMACT_rPTGFnRefTypename

#define _TERMACT_rPTGCallParamList2

#define _TERMACT_rPTGCallParamEmpty

#define _TERMACT_rPTGCallInsertion

#define _TERMACT_rPTGOptionalPatterns

#define _TERMACT_rDeclPDL

#define _TERMACT_rPDLDecl

#define _TERMACT_rPDLDefinitionList2

#define _TERMACT_rPDLDefinitionListEmpty

#define _TERMACT_rPDLLoad

#define _TERMACT_rPDLDefInclude

#define _TERMACT_rPDLInclude \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rPDLRealDef

#define _TERMACT_rPDLDefProperties \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rPDLDefNameList2

#define _TERMACT_rPDLDefNameList1

#define _TERMACT_rPDLDefTypename \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rPDLDefIdentifier \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rPDLReferenceType

#define _TERMACT_rDeclCopySource

#define _TERMACT_rDeclCopyHead

#define _TERMACT_rDeclCopyLido

#define _TERMACT_rCopySourcePre

#define _TERMACT_rCopySourcePost

#define _TERMACT_rCopyHeadPre

#define _TERMACT_rCopyHeadPost

#define _TERMACT_rCopyLido

#define _TERMACT_rDeclTypedef

#define _TERMACT_rTypedefDecl \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTypingReference \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTypingList \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTypingBasic \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTypingTuple \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTypingMap \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTupleType

#define _TERMACT_rTypeConList2

#define _TERMACT_rTypeConList1

#define _TERMACT_rTypeListConstruction

#define _TERMACT_rConstructSimple

#define _TERMACT_rTypeMapOf \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rDeclData

#define _TERMACT_rDataDecl \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rDataName \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rDataConstructorList2

#define _TERMACT_rDataConstructorList1

#define _TERMACT_rDataConstructorIsSimple

#define _TERMACT_rDataConstructorIsComplex

#define _TERMACT_rDataConstructorComplex

#define _TERMACT_rDataConstructorArgumentList2

#define _TERMACT_rDataConstructorArgumentList1

#define _TERMACT_rDataConstructorArgument

#define _TERMACT_rDataConstructorSimple

#define _TERMACT_rDataConstructorDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rDataDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTypeDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rDataRecordDecl \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rDataRecordArgumentList2

#define _TERMACT_rDataRecordArgumentList1

#define _TERMACT_rDataRecordArgument

#define _TERMACT_rDataRecordArgumentDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rDeclFunctionType

#define _TERMACT_rFunctionTypeDecl \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionType

#define _TERMACT_rFunctionTypeList2

#define _TERMACT_rFunctionTypeList1

#define _TERMACT_rReturnTypeSimple

#define _TERMACT_rFunctionDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTypeOfFunction \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rVoidTyping \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTypeTemplate \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rTypeVariableDefId

#define _TERMACT_rDeclFunctionImplementation \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionImplementationDecl \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionPatternList2

#define _TERMACT_rFunctionPatternListEmpty

#define _TERMACT_rFunctionPatternVar

#define _TERMACT_rFunctionPatternIsConstant

#define _TERMACT_rFunctionPatternIsListPattern

#define _TERMACT_rFunctionPatternIsTuple

#define _TERMACT_rFunctionPatternIsIgnored

#define _TERMACT_rFunctionPatternIsSimpleCon

#define _TERMACT_rFunctionPatternIsComplex

#define _TERMACT_rFunctionConstructorPatternComplex

#define _TERMACT_rFunctionPatternArgBindList2

#define _TERMACT_rFunctionPatternArgBindList1

#define _TERMACT_rFunctionPatternArgBind \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionPatternConstant \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionConstructorPatternSimple

#define _TERMACT_rFunctionIngorePattern

#define _TERMACT_rFunctionTuplePattern

#define _TERMACT_rFunctionTuplePatternElementList2

#define _TERMACT_rFunctionTuplePatternElementList1

#define _TERMACT_rFunctionPatternListHead

#define _TERMACT_rFunctionPatternListHeadAndTail

#define _TERMACT_rFunctionTuplePatternElement

#define _TERMACT_rRhsExpression

#define _TERMACT_rFunctionVariableDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionChain

#define _TERMACT_rFunctionExpressionIsBinop

#define _TERMACT_rFunctionExpressionBinopChain \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopOR \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopAND \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopEQ \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopNE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopLE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopLT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopGT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopGE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopConcat \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopADD \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopSUB \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopMUL \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopDIV \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopMOD \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionBinopIsUnary \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionUnaryIsPostfix

#define _TERMACT_rFunctionUnaryIsIncrement

#define _TERMACT_rFunctionUnaryIsMinus

#define _TERMACT_rFunctionUnaryIsLocation

#define _TERMACT_rFunctionUnaryIsNot

#define _TERMACT_rFunctionUnaryIsPtr

#define _TERMACT_rFunctionPostfixIsPrim

#define _TERMACT_rFunctionPostfixIsIndex \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionPostfixIsAccess \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionPostfixIsListcon \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionPrimaryIsConstant

#define _TERMACT_rFunctionPrimaryIsExpression

#define _TERMACT_rFunctionConstant \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConstantString \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConstantNumber \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConstantTrue \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConstantFalse \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConstantPos \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConstantEmptyList \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionPrimaryIsCall

#define _TERMACT_rFunctionCallParameters \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionCallEmpty \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionCallEmptyConstant \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rCallIdentifier \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rCallType \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rCallEmpty \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionCallParamList2

#define _TERMACT_rFunctionCallParamList1

#define _TERMACT_rFunctionCallParamIsExpr \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExprIsError

#define _TERMACT_rFunctionErrorExpression \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionIsIf

#define _TERMACT_rFunctionIfExpression \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionRhsIsGuards

#define _TERMACT_rFunctionGuardList2 \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionGuardList1

#define _TERMACT_rFunctionGuard

#define _TERMACT_rRhsStatements

#define _TERMACT_rFunctionDoStatementList2

#define _TERMACT_rFunctionDoStatementListEmpty

#define _TERMACT_rFunctionDoExpression \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionDoCondition \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionDoAssign \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionDoReturn \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionReturnStatement

#define _TERMACT_rFunctionCondStatement \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionCondStatUnit \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionIsLet

#define _TERMACT_rFunctionLetExpression

#define _TERMACT_rFunctionLetVarDefList2

#define _TERMACT_rFunctionLetVarDefList1

#define _TERMACT_rFunctionLetVarDef

#define _TERMACT_rFunctionDoIsMessage \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionDoMessage \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionExpressionIsLambda

#define _TERMACT_rFunctionLambdaExpr \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rNoTyping

#define _TERMACT_rOptionalType \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionDoIsAssignement \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionDoIsEach \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionStatAssign \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionEachStatSingle

#define _TERMACT_rFunctionEachStatMultiple

#define _TERMACT_rDeclConstant

#define _TERMACT_rConstantDecl \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConstantDefinition \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConstantDefIdentifier \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConstantDefTypename \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionDoIsNoreturn \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionNoReturn

#define _TERMACT_rFunctionPrimaryIsTuple

#define _TERMACT_rFunctionTupleConstruction \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rFunctionTupleArgumentList2

#define _TERMACT_rFunctionTupleArgumentList

#define _TERMACT_rFunctionTupleArgument

#define _TERMACT_rSymbolDefId

#define _TERMACT_rDeclAbstreeClass

#define _TERMACT_rDeclAbstree

#define _TERMACT_rAbstreeClassDecl

#define _TERMACT_rAbstreeDecl

#define _TERMACT_rAbstreeDefIdIsTypename \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rAbstreeDefIdIsIdent \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rNoAbstreeChanges

#define _TERMACT_rAbstreeChangeList2

#define _TERMACT_rAbstreeChangeIsPrefix

#define _TERMACT_rAbstreeChangeIsCombination

#define _TERMACT_rAbstreeChangeIsInherit

#define _TERMACT_rAbstreeChangeIsTrafo

#define _TERMACT_rAbstreeChangeIsDependency

#define _TERMACT_rAbstreePrefix \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rNewRuleprefixIsTypename

#define _TERMACT_rNewRuleprefixIsIdentifier

#define _TERMACT_rAbstreeCombination \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rAbstreeReferenceList2

#define _TERMACT_rAbstreeReferenceList1

#define _TERMACT_rAbstreeReferenceIsIdentifier \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rAbstreeReferenceIsTypename \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rAbstreeInheritance

#define _TERMACT_rAbstreeTrafoTo

#define _TERMACT_rAbstreeTrafoFrom

#define _TERMACT_rAbstreeDependency

#define _TERMACT_rNoAbstreeRules

#define _TERMACT_rAbstreeRulesExist

#define _TERMACT_rAbstreeRuleList2

#define _TERMACT_rAbstreeRuleList1

#define _TERMACT_rAbstreeRuleIsTermDecl

#define _TERMACT_rAbstreeTermDeclAs

#define _TERMACT_rAbstreeTermDeclIs

#define _TERMACT_rAbstreeTermDeclName

#define _TERMACT_rTreeSymbolDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rConversionReferenceTypename

#define _TERMACT_rConversionReferenceIdentifier

#define _TERMACT_rConversionReferenceNone

#define _TERMACT_rAbstreeRuleIsProduction

#define _TERMACT_rAbstreeProduction \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolList2

#define _TERMACT_rNoSymbols

#define _TERMACT_rSymbolIsLiteral

#define _TERMACT_rSymbolIsCreation \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolIsReference

#define _TERMACT_rSymbolReferenceOptTree \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolReference

#define _TERMACT_rNoTreeHint

#define _TERMACT_rOptTreeHint

#define _TERMACT_rRulenameExists

#define _TERMACT_rNoRulename

#define _TERMACT_rRulenameIsTypename

#define _TERMACT_rRulenameIsIdentifier

#define _TERMACT_rDeclGlobalAttribute

#define _TERMACT_rGlobalAttributeDecl

#define _TERMACT_rAttributeClassInherited

#define _TERMACT_rAttributeClassSynthesized

#define _TERMACT_rAttributeClassThread

#define _TERMACT_rAttributeClassChain

#define _TERMACT_rAttributeClassInfer

#define _TERMACT_rGlobalAttributeDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rGlobalAttributeDefIdList2

#define _TERMACT_rGlobalAttributeDefIdList1

#define _TERMACT_rDeclSymbol

#define _TERMACT_rSymbolDecl \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolClassificationSymbol

#define _TERMACT_rSymbolClassificationClassSymbol

#define _TERMACT_rSymbolOptionList2

#define _TERMACT_rNoSymbolOptions

#define _TERMACT_rSymbolOptionAttributes

#define _TERMACT_rSymbolOptionInheritance

#define _TERMACT_rSymbolInheritance

#define _TERMACT_rSymbolReferenceOptTreeList2

#define _TERMACT_rSymbolReferenceOptTreeList1

#define _TERMACT_rSymbolLocalAttributes

#define _TERMACT_rSymbolLocalAttributeDeclList2

#define _TERMACT_rSymbolLocalAttributeDeclList1

#define _TERMACT_rSymbolLocalAttributeDeclSameType

#define _TERMACT_rSymbolLocalAttributeDeclDifferentType

#define _TERMACT_rSymbolLocalAttributeDeclNoClassList2

#define _TERMACT_rSymbolLocalAttributeDeclNoClassList1

#define _TERMACT_rSymbolLocalAttributeDeclNoClass

#define _TERMACT_rSymbolLocalAttributeDefIdList2

#define _TERMACT_rSymbolLocalAttributeDefIdList1

#define _TERMACT_rSymbolLocalAttributeDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolLocalAttributeReference \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rLocalAttributeClassUnknown

#define _TERMACT_rLocalAttributeClassInherited

#define _TERMACT_rLocalAttributeClassSynthesized

#define _TERMACT_rLocalAttributeClassTail

#define _TERMACT_rLocalAttributeClassHead

#define _TERMACT_rLocalAttributeReference

#define _TERMACT_rNoSymbolComputations

#define _TERMACT_rSymbolComputationsExist

#define _TERMACT_rSymbolComputationList2

#define _TERMACT_rSymbolComputationListEmpty

#define _TERMACT_rSymbolComputationIsAssign \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolComputationIsChainstart \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolComputationIsExpression \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolComputationIsOrder

#define _TERMACT_rSymbolChainStart \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rNoSymbolDependency

#define _TERMACT_rSymbolDependency

#define _TERMACT_rSymbolSymbolAttributeReferencesSingle

#define _TERMACT_rSymbolSymbolAttributeReferencesMulti

#define _TERMACT_rSymbolLocalAttributeReferenceIsRemote

#define _TERMACT_rSymbolLocalAttributeReferenceIsLocal

#define _TERMACT_rSymbolLocalAttributeRefList2

#define _TERMACT_rSymbolLocalAttributeRefList1

#define _TERMACT_rSymbolDefOcc \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolOrderedComputation \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolOrderedStatementList2

#define _TERMACT_rSymbolOrderedStatementList1

#define _TERMACT_rSymbolOrderedStatementIsExpression \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolOrderedStatementIsReturn \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolOrderedStatementIsAssign \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolRHSExpressionIsCompute

#define _TERMACT_rSymbolRHSExpressionIsGuards \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolGuardExpressionList2 \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolGuardExpressionList1

#define _TERMACT_rSymbolGuardExpressionIsGuard

#define _TERMACT_rSymbolGuardExpressionIsExpression \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolGuardExpressionIsDefault \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionChain

#define _TERMACT_rSymbolExpressionIsWhen

#define _TERMACT_rSymbolExpressionIsError

#define _TERMACT_rSymbolExpressionIsIf

#define _TERMACT_rSymbolExpressionIsLet

#define _TERMACT_rSymbolExpressionIsLambda

#define _TERMACT_rSymbolExpressionIsOrder

#define _TERMACT_rSymbolLambda \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolLambdaVarDefList2

#define _TERMACT_rSymbolLambdaVarDefList1

#define _TERMACT_rSymbolLambdaVarDef

#define _TERMACT_rSymbolVariableDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionLet

#define _TERMACT_rSymbolLetVarDefList2

#define _TERMACT_rSymbolLetVarDefList1

#define _TERMACT_rSymbolLetVarDef \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionIf \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionError \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionWhenCond \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionWhen \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionIsBinary

#define _TERMACT_rSymbolExpressionBinaryChain

#define _TERMACT_rSymbolExpressionBinaryOR \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryAND \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryEQ \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryNE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryLT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryGT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryLE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryGE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryConcat \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryADD \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinarySUB \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryMUL \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryDIV \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryMOD \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionBinaryIsUnary \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionUnaryIsPostfix

#define _TERMACT_rSymbolExpressionUnaryIncr \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionUnaryNEG \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionUnaryNOT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionUnaryAddress

#define _TERMACT_rSymbolExpressionUnaryReference

#define _TERMACT_rSymbolExpressionPostfixIsPrimary

#define _TERMACT_rSymbolExpressionPostfixIsIndex \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionPostfixIsAccess \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionPostfixIsListcon \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionPrimaryIsConstant

#define _TERMACT_rSymbolExpressionPrimaryIsCall

#define _TERMACT_rSymbolExpressionPrimaryIsTuple \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionPrimaryIsWrap

#define _TERMACT_rSymbolTupleConstruction

#define _TERMACT_rSymbolTupleArgumentList2

#define _TERMACT_rSymbolTupleArgumentList1

#define _TERMACT_rSymbolTupleArgument

#define _TERMACT_rSymbolExpressionCallApplied \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionCallEmpty \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolExpressionCallVariable \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolCallableReferenceIsIdentifier \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolCallableReferenceIsTypename \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolCallableReferenceIsTreeCon \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolCallableReferenceIsEnd \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolCallableReferenceIsAttribute \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolCallParameterList2

#define _TERMACT_rSymbolCallParameterList1

#define _TERMACT_rSymbolCallParameter \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolConstant \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rDeclRuleSpecification

#define _TERMACT_rRuleSpecification \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleGuardCondition \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleGuardRhsGuards \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleGuardRhsComputes \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleClassificationSingle

#define _TERMACT_rRuleClassificationMultiple

#define _TERMACT_rRuleClassificationSingleClass

#define _TERMACT_rRuleClassificationMultipleClass

#define _TERMACT_rRulePatternMultiple

#define _TERMACT_rRulePatternSingle

#define _TERMACT_rRulePatternVarious

#define _TERMACT_rRuleReferenceIsTypename

#define _TERMACT_rRuleReferenceIsIdentifier

#define _TERMACT_rRuleReferenceList2

#define _TERMACT_rRuleReferenceList1

#define _TERMACT_rRuleLhsPatternSpecific

#define _TERMACT_rRuleLhsPatternDontCare

#define _TERMACT_rNoRuleVariableDef

#define _TERMACT_rCreateRuleVariable

#define _TERMACT_rOptProduction

#define _TERMACT_rNoProduction

#define _TERMACT_rRuleRHSPattern

#define _TERMACT_rRuleRHSPatternSymbolList2

#define _TERMACT_rNoRuleRHSPatternSymbols

#define _TERMACT_rRulePatternSymbolAllRest

#define _TERMACT_rRulePatternSymbolDontCare

#define _TERMACT_rRulePatternSymbolConstructor \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRulePatternSymbolMatchTermString

#define _TERMACT_rRulePatternSymbolMatchTermInt

#define _TERMACT_rNoDeeperMatching

#define _TERMACT_rOptDeeperRulePattern

#define _TERMACT_rDeeperRulePatternSymbolList2

#define _TERMACT_rNoDeeperRulePatternSymbols

#define _TERMACT_rDeeperRulePatternSymbolDontCare

#define _TERMACT_rDeeperRulePatternSymbolDontCareRest

#define _TERMACT_rDeeperRulePatternSymbolMatchTermString

#define _TERMACT_rDeeperRulePatternSymbolMatchTermInt

#define _TERMACT_rDeeperMatchConstructor

#define _TERMACT_rRuleComputationList2

#define _TERMACT_rNoRuleComputations

#define _TERMACT_rRuleComputeAssign

#define _TERMACT_rRuleComputeExpr \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleComputeChainStart

#define _TERMACT_rRuleComputeOrder \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleChainStart \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleDefOcc \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rNoRuleDependency

#define _TERMACT_rRuleDependencies

#define _TERMACT_rRuleAttributeReferenceSingle

#define _TERMACT_rRuleAttributeReferenceMulti

#define _TERMACT_rRuleAttributeReferenceList2

#define _TERMACT_rRuleAttributeReferenceList1

#define _TERMACT_rRuleAttributeReferenceIsSymbolAttrRef

#define _TERMACT_rRuleAttributeReferenceIsRemote

#define _TERMACT_rRuleSymbolAttrRefIsLidoStyle \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleSymbolAttrRefIsLidoListOfStyle \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleSymbolAttrRefIsVarReference \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleAttrClassIsTail

#define _TERMACT_rRuleAttrClassIsHead

#define _TERMACT_rRuleVariableReference \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rNoSymbolIndex

#define _TERMACT_rSymbolIndex

#define _TERMACT_rSymbolAttributeReference

#define _TERMACT_rRuleOrderedComputation

#define _TERMACT_rRuleOrderedStatementList2

#define _TERMACT_rRuleOrderedStatementList1

#define _TERMACT_rRuleOrderedStatementIsExpression \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleOrderedStatementIsAssign \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleOrderedStatementIsReturn \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleRhsExprIsExpr \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleRhsExprIsGuards \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleGuardExpressionList2 \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleGuardExpressionList1

#define _TERMACT_rRuleGuardIsOr \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleGuardIsCompute \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleGuardIsDefault \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionChain

#define _TERMACT_rRuleExpressionIsWhen

#define _TERMACT_rRuleExpressionIsError

#define _TERMACT_rRuleExpressionIsIf

#define _TERMACT_rRuleExpressionIsLet

#define _TERMACT_rRuleExpressionIsLambda

#define _TERMACT_rRuleExpressionIsOrder

#define _TERMACT_rRuleLambda \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleLambdaVarDefList2

#define _TERMACT_rRuleLambdaVarDefList1

#define _TERMACT_rRuleLambdaVarDef

#define _TERMACT_rRuleVariableDefId \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionLet

#define _TERMACT_rRuleLetVarDefList2

#define _TERMACT_rRuleLetVarDefList1

#define _TERMACT_rRuleLetVarDef

#define _TERMACT_rRuleExpressionIf \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionError \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionWhenCond \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionWhen \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionIsBinary

#define _TERMACT_rRuleExpressionBinaryChain

#define _TERMACT_rRuleExpressionBinaryOR \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryAND \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryEQ \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryNE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryLT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryGT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryLE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryGE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryConcat \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryADD \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinarySUB \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryMUL \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryDIV \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryMOD \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionBinaryIsUnary \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionUnaryIsPostfix

#define _TERMACT_rRuleExpressionUnaryIncr \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionUnaryNEG \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionUnaryNOT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionUnaryAddress

#define _TERMACT_rRuleExpressionUnaryReference

#define _TERMACT_rRuleExpressionPostfixIsPrimary

#define _TERMACT_rRuleExpressionPostfixIsIndex \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionPostfixIsAccess \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionPostfixIsListcon \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionPrimaryIsConstant

#define _TERMACT_rRuleExpressionPrimaryIsCall

#define _TERMACT_rRuleExpressionPrimaryIsTuple \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionPrimaryIsRemoteAttribute

#define _TERMACT_rRuleExpressionPrimaryIsWrap

#define _TERMACT_rRuleTupleConstruction

#define _TERMACT_rRuleTupleArgumentList2

#define _TERMACT_rRuleTupleArgumentList1

#define _TERMACT_rRuleTupleArgument

#define _TERMACT_rRuleExpressionCallApplied \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionCallEmpty \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleExpressionCallVariable

#define _TERMACT_rRuleCallableReferenceIsIdentifier \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleCallableReferenceIsTypename \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleCallableReferenceIsEnd \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleCallableReferenceIsAttribute \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleCallableReferenceIsRule \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleCallParameterList2

#define _TERMACT_rRuleCallParameterList1

#define _TERMACT_rRuleCallParameter \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRuleConstant \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteUp

#define _TERMACT_rRemoteDown

#define _TERMACT_rRemoteIncluding \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolAttributeReferencesSingle

#define _TERMACT_rSymbolAttributeReferencesMultiple

#define _TERMACT_rSymbolAttributeReferenceList2

#define _TERMACT_rSymbolAttributeReferenceList1

#define _TERMACT_rSymbolAttributeRef \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteConstituent \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rOptSymbolIsSymbol \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rNoSymbolReference

#define _TERMACT_rRemoteOptionsWith \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rNoRemoteOptions

#define _TERMACT_rRemoteOptionsShield

#define _TERMACT_rNoRemoteShield

#define _TERMACT_rOptRemoteShield

#define _TERMACT_rRemoteShieldMultiple

#define _TERMACT_rRemoteShieldSingle

#define _TERMACT_rRemoteUnshieldSelf

#define _TERMACT_rSymbolReferenceList2

#define _TERMACT_rSymbolReferenceList1

#define _TERMACT_rRemoteAppendCall \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendLambda

#define _TERMACT_rRemoteAppendIsBinary

#define _TERMACT_rRemoteAppendADD \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendSUB \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendDIV \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendMUL \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendConcat \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendEQ \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendNE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendLT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendGT \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendLE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendGE \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendOR \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendAND \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteAppendMOD \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteSingleCall \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteSingleInfer \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteSingleCreateList

#define _TERMACT_rRemoteSingleLambda

#define _TERMACT_rRemoteEmptyCall \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteEmptyConstant \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteEmptyInfer \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteWithInfer \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteWithLido \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rRemoteWithInferTypes \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolOptionUsing

#define _TERMACT_rSymbolUsingAttribute

#define _TERMACT_rSymbolUsingDeclList2

#define _TERMACT_rSymbolUsingDeclList1

#define _TERMACT_rSymbolUsingDecl \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolUsingDeclWithoutClass \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolUsingDeclsNoClassList2

#define _TERMACT_rSymbolUsingDeclsNoClassList1

#define _TERMACT_rSymbolUsingDeclNoClass \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolLocalAttributeReferenceList2

#define _TERMACT_rSymbolLocalAttributeReferenceList1

#define _TERMACT_rLocalUsingReference \
	_currn->_AT_pos=(_coordref ? *_coordref : NoCoord);

#define _TERMACT_rSymbolAttrReferenceIsTree
