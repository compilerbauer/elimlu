

#include "messaging.h" 
#include "CoordMap.h"
#include "clp.h" 
#include <sstream>
#include <locale>
#include <iomanip> 
#include <unordered_set> 

extern DefTableKey InputFile; 

std::unordered_set<std::string> reported_for_once; 

std::unordered_map<severity, int> severity_count;

int lhsmin = 10;

namespace messaging{
  std::unordered_map<severity, sevname> severity_names;
  int severity_cutoff = -1;
  int severity_is_error = -1;
  int severity_is_deadly = -1;
  std::ostream* out = &std::cerr;
  bool flush = true;
  bool ag = true;
  int cut = 80;
  bool do_debug = false;
  int nonflush = 0;
  std::string last_file = ""; 
}

void InitializeMessagingModule(safe_bool flush, std::ostream* o, safe_bool ag, int cut, safe_bool deb){
  messaging::out = o;
  messaging::flush = (flush == safe_bool::s_true)? true : false;
  messaging::ag = (ag == safe_bool::s_true)? true : false;
  messaging::cut = cut;
  messaging::do_debug = (deb == safe_bool::s_true)? true : false;
  
  if(messaging::out == nullptr) {
    std::cerr << "DEADLY: Cannot create error messaging stream" << std::endl;
    exit(1);
  }
}


std::string get_filename(CoordPtr p) {
  std::stringstream ss;
  ss << "\"";
  if(p == NoPosition || p == nullptr) {
    int namesym = GetClpValue(InputFile, 0);
    if(namesym == 0) {
      ss << "NoFile";
    }
    else { 
      ss << StringTable(namesym);
    }
  }
  else { 
    ss << MapFile(LineOf(*p));
  }
  ss << "\"";
  return ss.str();
}


std::string map_position(CoordPtr p) {
  if(p == NoPosition || p == nullptr) return "NoPosition";
  
  std::stringstream ss;
  ss << MapLine(LineOf(*p)) << ":" << ColOf(*p);
  return ss.str();
}

int index_of_last_whitespace(std::string& text, int add, int cut) {
  int lws = 0;
  int i = 0; 
  
  for(i = 0; i < text.length(); i++) {
    if((i + add) >= cut) break;
    if(isblank(text[i])) lws = i;
  }
  if(i == text.length()) { lws = text.length(); } 
  
  
  return lws;
}

std::string map_message(std::string& lhs, std::string& msg, int start, int cut) {
  std::stringstream ss;
  int wscnt = lhs.length();
  int lws = 0;
  
  if(cut < 0 || (msg.find("\n") != msg.npos)) {
    return msg;
  }
  if(wscnt >= cut - 1){
    std::string pre = "    ";
    return map_message(pre, msg, start, cut);
  }
  
  
  int splitind = index_of_last_whitespace(msg, wscnt, cut);
  int rlen = msg.length() - (splitind + 1);
  if(start != 0) {
    for(int i = 0; i < wscnt; i++) {
      ss << " ";
    }
  }
  
 
  if(splitind == 0 || rlen <= 0) {
    ss << msg;
  }
  else { 
    std::string app = msg.substr(0, splitind);
    std::string remain = msg.substr(splitind + 1, rlen);
    
    ss << app << "\n";
    ss << map_message(lhs, remain, 1, cut);
  }
  
  return ss.str();
}

void ReportOnce(severity sev, CoordPtr p, const std::string msg, int agcond, bool nobreak) { 
  Report(sev, p, msg, agcond, nobreak, true); 
}

void Report(severity sev, CoordPtr p, const std::string msg, int agcond, bool nobreak, bool only_once) {
  severity_count[sev]++;
  
  if(sev <= messaging::severity_cutoff || (sev == DEBUG && messaging::do_debug == false && !Debug)) 
    return;
  
  std::stringstream ss;

  std::string filename = get_filename(p);
  
  if(messaging::last_file != filename && noInfile == false) { 
    *messaging::out << "FILE " << filename << ": " << "\n"; 
    messaging::last_file = filename;
  }
  else if(noInfile) { 
    ss << filename << ", "; 
  }

  ss << map_position(p) << " " << messaging::severity_names[sev] << ": ";
  
  std::string lhs = ss.str(); 
  
  std::stringstream ss2;
  ss2 << msg;
  if(messaging::ag == true && agcond > 0 && messaging::do_debug) {
    ss2 << " (AG = " << agcond << ") ";
  }
  
  std::string tmp = ss2.str();
  
  std::string nmesg = nobreak? tmp : map_message(lhs, tmp, 0, messaging::cut);
   
#ifdef MONITOR
  int lsev = sev;
  if(sev < messaging::severity_is_deadly && sev >= messaging::severity_is_error) {
    lsev = 2;
  }
  else if( sev < messaging::severity_is_deadly ) {
    lsev = 1;
  }
  else {
    lsev = 3;
  }
  
  if(only_once && reported_for_once.count(msg) == 0) { 
    message(lsev, msg.c_str(), agcond, p);  
    reported_for_once.insert(msg); 
  }
  else if (!only_once){ 
    message(lsev, msg.c_str(), agcond, p);  
  }
  
#else   
  if(lhs.length() > lhsmin) { 
    lhsmin = lhs.length() + 1;
  }
  int wlen = lhsmin;
  if(only_once) { 
    auto once_msg = lhs + nmesg; 
    if(reported_for_once.count(once_msg) == 0) { 
      reported_for_once.insert(once_msg); 
      *messaging::out << std::setw(wlen) << lhs << nmesg << "\n";
    }
  }
  else { 
    *messaging::out << std::setw(wlen) << lhs << nmesg << "\n";
  }
  
  
  if(messaging::flush || messaging::nonflush > 2000000) {
    messaging::out->flush();
    messaging::nonflush = 0;
  }
  else { 
    messaging::nonflush++;
  }
#endif // MONITOR 
  
  bool doexit = false;
  
  if(sev >= messaging::severity_is_deadly) {
    doexit = true;
  }
  
  if(sev >= messaging::severity_is_error) {
    int cnt = (severity_count[sev]);
    int ln = LineNum / 20; ln += 10;
    if(canExit && cnt > ln) {
      doexit = true;
    }
  }
  
  if(doexit) {
    *messaging::out << "Exiting due to Errors" << std::endl;
    messaging::out->flush();
    exit(1);
  }
}

void InternalMessage(const std::string msg, CoordPtr coord) {
  if(Debug)
    Report(DEBUG, coord, msg, -1, true);
}

std::string concat_str(const std::string a, std::string b) {
  std::string ret = a + b;
  return ret;
}

std::string concat_int(const std::string a, int v) {
  std::stringstream ss;
  ss << a << v;
  return ss.str();
}

std::string concat_ind(const std::string a, int ind) {
  std::stringstream ss;
  ss << a << StringTable(ind);
  return ss.str();
}

std::string concat_bool(const std::string a, bool v) {
  std::stringstream ss;
  ss << a << std::boolalpha << v;
  return ss.str(); 
}

int getErrs() { 
  int res = 0;
  for(auto t : severity_count) { 
    if(t.first >= messaging::severity_is_error) {
      res += t.second;
    }
  }
  return res;
}


void OptReport(bool val, severity sev, CoordPtr p, const std::string msg, 
               int agcond, bool nobreak) {
  if(!val) return;
  Report(sev, p, msg, agcond, nobreak);
}

