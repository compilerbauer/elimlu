
/* implementation of tree construction functions */

#include "node.h"

#include "nodecode.h"

#include "attrpredef.h"

#include "visitmap.h"

#include "treeact.h"

#ifdef MONITOR
#include "attr_mon_dapto.h"
#include "MONTblStack.h"
#endif

#include <stdlib.h>

#define _USE_OBSTACK 1

/* use of obstack: */

#if _USE_OBSTACK

#include "obstack.h"
static struct obstack TreeSpace;
static void *_TreeBase;

#ifdef __cplusplus
void* NODEPTR_struct::operator new(size_t size)
{
	return obstack_alloc(&TreeSpace, size);
}
#else
#if defined(__STDC__) || defined(__cplusplus)
char* TreeNodeAlloc(int size)
#else
char* TreeNodeAlloc(size) int size;
#endif
{
	return (char *)(obstack_alloc(&TreeSpace, size));
}
#endif

void InitTree()
{
	obstack_init(&TreeSpace);
	_TreeBase=obstack_alloc(&TreeSpace,0);
}

void FreeTree()
{
	obstack_free(&TreeSpace, _TreeBase);
	_TreeBase=obstack_alloc(&TreeSpace,0);
}

#else

#include <stdio.h>

#ifdef __cplusplus
void* NODEPTR_struct::operator new(size_t size)
{
	void *retval = malloc(size);
	if (retval) return retval;
	fprintf(stderr, "*** DEADLY: No more memory.\n");
	exit(1);
}
#else
#if defined(__STDC__) || defined(__cplusplus)
char* TreeNodeAlloc(int size)
#else
char* TreeNodeAlloc(size) int size;
#endif
{
	char *retval = (char *) malloc(size);
	if (retval) return retval;
	fprintf(stderr, "*** DEADLY: No more memory.\n");
	exit(1);
}
#endif

void InitTree() { }

void FreeTree() { }

#endif

#ifdef MONITOR
#define _SETCOORD(node) \
        node->_coord = _coordref ? *_coordref : NoCoord;
#define _COPYCOORD(node) \
        node->_coord = _currn->_desc1->_coord;
#else
#define _SETCOORD(node)
#define _COPYCOORD(node)
#endif
#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAAARoot (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAAARoot (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAAARoot)) return (_currn);
if (IsSymb (_currn, SYMBDecls)) return (MkrProgramRoot(_coordref, _currn));
if (IsSymb (_currn, SYMBProgram)) return (MkrProgramRoot(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAAARoot */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkProgram (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkProgram (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBProgram)) return (_currn);
if (IsSymb (_currn, SYMBDecls)) return (MkrProgram(_coordref, _currn));
return(NULLNODEPTR);
}/* MkProgram */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDecls (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDecls (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDecls)) return (_currn);
return(NULLNODEPTR);
}/* MkDecls */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_Decl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_Decl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_Decl)) return (_currn);
if (IsSymb (_currn, SYMBPTG_Definitions)) return (MkrPTGDecl(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_Decl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_Definitions (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_Definitions (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_Definitions)) return (_currn);
return(NULLNODEPTR);
}/* MkPTG_Definitions */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_Definition (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_Definition (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_Definition)) return (_currn);
return(NULLNODEPTR);
}/* MkPTG_Definition */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_DefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_DefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_DefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrPTGDefTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrPTGDefIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_DefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_Pattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_Pattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_Pattern)) return (_currn);
if (IsSymb (_currn, SYMBPTG_Optional)) return (MkrPTGPatternOptional(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Call)) return (MkrPTGPatternCall(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Insertion)) return (MkrPTGPatternInsert(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Pattern)) return (MkrPTGPatternOptional(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Patterns)) return (MkrPTGPatternOptional(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrPTGPatternString(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_Pattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_OptionalIndex (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_OptionalIndex (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_OptionalIndex)) return (_currn);
if (IsSymb (_currn, SYMBNumber)) return (MkrPTGHasIndex(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Index)) return (MkrPTGHasIndex(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_OptionalIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_Index (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_Index (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_Index)) return (_currn);
if (IsSymb (_currn, SYMBNumber)) return (MkrPTGIndex(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_Index */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_OptionalType (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_OptionalType (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_OptionalType)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrPTGTypeGeneral(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrPTGTypeGeneral(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrPTGTypeGeneral(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrPTGTypeGeneral(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrPTGTypeGeneral(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrPTGTypeGeneral(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrPTGTypeGeneral(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrPTGTypeGeneral(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrPTGTypeEli(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrPTGTypeGeneral(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrPTGTypeGeneral(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_OptionalType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_Call (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_Call (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_Call)) return (_currn);
return(NULLNODEPTR);
}/* MkPTG_Call */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_FunctionReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_FunctionReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_FunctionReference)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrPTGFnRefTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrPTGFnRefIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_FunctionReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_CallParams (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_CallParams (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_CallParams)) return (_currn);
return(NULLNODEPTR);
}/* MkPTG_CallParams */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_Insertion (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_Insertion (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_Insertion)) return (_currn);
return(NULLNODEPTR);
}/* MkPTG_Insertion */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_CallParam (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_CallParam (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_CallParam)) return (_currn);
if (IsSymb (_currn, SYMBPTG_Insertion)) return (MkrPTGCallInsertion(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_CallParam */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_Patterns (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_Patterns (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_Patterns)) return (_currn);
if (IsSymb (_currn, SYMBPTG_Optional)) return (MkrPTGPatternEmpty(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Call)) return (MkrPTGPatternEmpty(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Insertion)) return (MkrPTGPatternEmpty(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Pattern)) return (MkrPTGPatternEmpty(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Patterns)) return (MkrPTGPatternEmpty(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrPTGPatternEmpty(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_Patterns */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPTG_Optional (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPTG_Optional (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPTG_Optional)) return (_currn);
if (IsSymb (_currn, SYMBPTG_Optional)) return (MkrPTGOptionalPatterns(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Call)) return (MkrPTGOptionalPatterns(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Insertion)) return (MkrPTGOptionalPatterns(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Pattern)) return (MkrPTGOptionalPatterns(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Patterns)) return (MkrPTGOptionalPatterns(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrPTGOptionalPatterns(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPTG_Optional */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPDL_Decl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPDL_Decl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPDL_Decl)) return (_currn);
if (IsSymb (_currn, SYMBPDL_Definitions)) return (MkrPDLDecl(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPDL_Decl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPDL_Definitions (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPDL_Definitions (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPDL_Definitions)) return (_currn);
return(NULLNODEPTR);
}/* MkPDL_Definitions */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkLoad_Decl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkLoad_Decl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBLoad_Decl)) return (_currn);
if (IsSymb (_currn, SYMBString)) return (MkrLoadDecl(_coordref, _currn));
return(NULLNODEPTR);
}/* MkLoad_Decl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPDL_IncludeDecl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPDL_IncludeDecl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPDL_IncludeDecl)) return (_currn);
if (IsSymb (_currn, SYMBString)) return (MkrPDLInclude(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPDL_IncludeDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPDL_Definition (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPDL_Definition (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPDL_Definition)) return (_currn);
if (IsSymb (_currn, SYMBPDL_PropertyDefinition)) return (MkrPDLRealDef(_coordref, _currn));
if (IsSymb (_currn, SYMBPDL_IncludeDecl)) return (MkrPDLDefInclude(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrPDLDefInclude(_coordref, _currn));
if (IsSymb (_currn, SYMBLoad_Decl)) return (MkrPDLLoad(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPDL_Definition */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPDL_PropertyDefinition (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPDL_PropertyDefinition (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPDL_PropertyDefinition)) return (_currn);
return(NULLNODEPTR);
}/* MkPDL_PropertyDefinition */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPDL_PropertyNames (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPDL_PropertyNames (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPDL_PropertyNames)) return (_currn);
if (IsSymb (_currn, SYMBPDL_PropertyDefId)) return (MkrPDLDefNameList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrPDLDefNameList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrPDLDefNameList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPDL_PropertyNames */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPDL_PropertyDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPDL_PropertyDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPDL_PropertyDefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrPDLDefTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrPDLDefIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPDL_PropertyDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkPDL_Type (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkPDL_Type (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBPDL_Type)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrPDLReferenceType(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrPDLReferenceType(_coordref, _currn));
return(NULLNODEPTR);
}/* MkPDL_Type */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkCopy_Source (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkCopy_Source (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBCopy_Source)) return (_currn);
if (IsSymb (_currn, SYMBCText)) return (MkrCopySourcePost(_coordref, _currn));
return(NULLNODEPTR);
}/* MkCopy_Source */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkCopy_Head (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkCopy_Head (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBCopy_Head)) return (_currn);
if (IsSymb (_currn, SYMBCText)) return (MkrCopyHeadPost(_coordref, _currn));
return(NULLNODEPTR);
}/* MkCopy_Head */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkCopy_Lido (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkCopy_Lido (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBCopy_Lido)) return (_currn);
if (IsSymb (_currn, SYMBCText)) return (MkrCopyLido(_coordref, _currn));
return(NULLNODEPTR);
}/* MkCopy_Lido */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkNewType_Decl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkNewType_Decl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBNewType_Decl)) return (_currn);
return(NULLNODEPTR);
}/* MkNewType_Decl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkEliType_Reference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkEliType_Reference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBEliType_Reference)) return (_currn);
return(NULLNODEPTR);
}/* MkEliType_Reference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkType_TupleOf (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkType_TupleOf (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBType_TupleOf)) return (_currn);
return(NULLNODEPTR);
}/* MkType_TupleOf */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkType_Constructions (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkType_Constructions (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBType_Constructions)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrTypeConList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrTypeConList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkType_Constructions */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkType_Listof (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkType_Listof (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBType_Listof)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrTypeListConstruction(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrTypeListConstruction(_coordref, _currn));
return(NULLNODEPTR);
}/* MkType_Listof */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkType_Construction (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkType_Construction (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBType_Construction)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrConstructSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrConstructSimple(_coordref, _currn));
return(NULLNODEPTR);
}/* MkType_Construction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkType_MapOf (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkType_MapOf (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBType_MapOf)) return (_currn);
return(NULLNODEPTR);
}/* MkType_MapOf */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_Constructors (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_Constructors (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_Constructors)) return (_currn);
if (IsSymb (_currn, SYMBData_ConstructorDefId)) return (MkrDataConstructorList1(_coordref, _currn));
if (IsSymb (_currn, SYMBData_ComplexConstructor)) return (MkrDataConstructorList1(_coordref, _currn));
if (IsSymb (_currn, SYMBData_SimpleConstructor)) return (MkrDataConstructorList1(_coordref, _currn));
if (IsSymb (_currn, SYMBData_Constructor)) return (MkrDataConstructorList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrDataConstructorList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_Constructors */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_Constructor (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_Constructor (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_Constructor)) return (_currn);
if (IsSymb (_currn, SYMBData_ConstructorDefId)) return (MkrDataConstructorIsSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBData_ComplexConstructor)) return (MkrDataConstructorIsComplex(_coordref, _currn));
if (IsSymb (_currn, SYMBData_SimpleConstructor)) return (MkrDataConstructorIsSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrDataConstructorIsSimple(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_Constructor */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_ComplexConstructor (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_ComplexConstructor (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_ComplexConstructor)) return (_currn);
return(NULLNODEPTR);
}/* MkData_ComplexConstructor */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_ConstructorArguments (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_ConstructorArguments (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_ConstructorArguments)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBData_ConstructorArgument)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrDataConstructorArgumentList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_ConstructorArguments */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_ConstructorArgument (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_ConstructorArgument (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_ConstructorArgument)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrDataConstructorArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrDataConstructorArgument(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_ConstructorArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_SimpleConstructor (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_SimpleConstructor (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_SimpleConstructor)) return (_currn);
if (IsSymb (_currn, SYMBData_ConstructorDefId)) return (MkrDataConstructorSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrDataConstructorSimple(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_SimpleConstructor */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_ConstructorDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_ConstructorDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_ConstructorDefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrDataConstructorDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_ConstructorDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkType_DefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkType_DefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBType_DefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrTypeDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkType_DefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_DefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_DefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_DefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrDataDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_DefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_Decl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_Decl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_Decl)) return (_currn);
if (IsSymb (_currn, SYMBData_DefId)) return (MkrDataName(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrDataName(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_Decl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_RecordArguments (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_RecordArguments (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_RecordArguments)) return (_currn);
if (IsSymb (_currn, SYMBData_RecordArgument)) return (MkrDataRecordArgumentList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_RecordArguments */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_RecordArgument (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_RecordArgument (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_RecordArgument)) return (_currn);
return(NULLNODEPTR);
}/* MkData_RecordArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkData_RecordArgumentDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkData_RecordArgumentDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBData_RecordArgumentDefId)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrDataRecordArgumentDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkData_RecordArgumentDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_TypeDecl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_TypeDecl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_TypeDecl)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_TypeDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Types (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Types (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Types)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionTypeList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionTypeList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_Types */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_ReturnType (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_ReturnType (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_ReturnType)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrReturnTypeSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrReturnTypeSimple(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_ReturnType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Typing (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Typing (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Typing)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_Typing */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkType_VariableDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkType_VariableDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBType_VariableDefId)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrTypeVariableDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkType_VariableDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_DefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_DefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_DefId)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_DefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Implementation (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Implementation (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Implementation)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_Implementation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Patterns (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Patterns (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Patterns)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_Patterns */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Pattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Pattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Pattern)) return (_currn);
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionPatternIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ConstructorPatternComplex)) return (MkrFunctionPatternIsComplex(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ConstructorPatternSimple)) return (MkrFunctionPatternIsSimpleCon(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IgnorePattern)) return (MkrFunctionPatternIsIgnored(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_TuplePattern)) return (MkrFunctionPatternIsTuple(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ListPattern)) return (MkrFunctionPatternIsListPattern(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ConstantPattern)) return (MkrFunctionPatternIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_VariableDefId)) return (MkrFunctionPatternVar(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrFunctionPatternIsSimpleCon(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionPatternIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionPatternIsSimpleCon(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionPatternVar(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionPatternIsConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_Pattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_ConstructorPatternComplex (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_ConstructorPatternComplex (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_ConstructorPatternComplex)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_ConstructorPatternComplex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_PatternArgBinds (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_PatternArgBinds (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_PatternArgBinds)) return (_currn);
if (IsSymb (_currn, SYMBFunction_PatternArgBind)) return (MkrFunctionPatternArgBindList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_VariableDefId)) return (MkrFunctionPatternArgBindList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionPatternArgBindList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_PatternArgBinds */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_PatternArgBind (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_PatternArgBind (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_PatternArgBind)) return (_currn);
if (IsSymb (_currn, SYMBFunction_VariableDefId)) return (MkrFunctionPatternArgBind(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionPatternArgBind(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_PatternArgBind */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_ConstantPattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_ConstantPattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_ConstantPattern)) return (_currn);
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionPatternConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionPatternConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionPatternConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_ConstantPattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkType_Reference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkType_Reference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBType_Reference)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrTypeRefSimple(_coordref, _currn));
return(NULLNODEPTR);
}/* MkType_Reference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_ConstructorPatternSimple (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_ConstructorPatternSimple (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_ConstructorPatternSimple)) return (_currn);
if (IsSymb (_currn, SYMBType_Reference)) return (MkrFunctionConstructorPatternSimple(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionConstructorPatternSimple(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_ConstructorPatternSimple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_IgnorePattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_IgnorePattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_IgnorePattern)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_IgnorePattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_TuplePattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_TuplePattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_TuplePattern)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_TuplePattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_TuplePatternElements (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_TuplePatternElements (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_TuplePatternElements)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TuplePatternElement)) return (MkrFunctionTuplePatternElementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_VariableDefId)) return (MkrFunctionTuplePatternElementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionTuplePatternElementList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_TuplePatternElements */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_ListPattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_ListPattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_ListPattern)) return (_currn);
if (IsSymb (_currn, SYMBFunction_VariableDefId)) return (MkrFunctionPatternListHead(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionPatternListHead(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_ListPattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_TuplePatternElement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_TuplePatternElement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_TuplePatternElement)) return (_currn);
if (IsSymb (_currn, SYMBFunction_VariableDefId)) return (MkrFunctionTuplePatternElement(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionTuplePatternElement(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_TuplePatternElement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_BinaryExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_BinaryExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionExpressionBinopChain(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionExpressionBinopIsUnary(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_BinaryExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_UnaryExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_UnaryExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionUnaryIsPtr(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_UnaryExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Constant (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Constant (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Constant)) return (_currn);
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_Constant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Call (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Call (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Call)) return (_currn);
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionCallEmptyConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionCallEmptyConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionCallEmptyConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_Call */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_CallParameters (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_CallParameters (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_CallParameters)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_CallParameter)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionCallParamList1(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionCallParamList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_CallParameters */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_CallParameter (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_CallParameter (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_CallParameter)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionCallParamIsExpr(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_CallParameter */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_IfExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_IfExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_IfExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Guards (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Guards (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Guards)) return (_currn);
if (IsSymb (_currn, SYMBFunction_Guard)) return (MkrFunctionGuardList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_Guards */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Guard (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Guard (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Guard)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_Guard */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_RightHandSide (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_RightHandSide (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_RightHandSide)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_DoStatements)) return (MkrRhsStatements(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Guard)) return (MkrFunctionRhsIsGuards(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Guards)) return (MkrFunctionRhsIsGuards(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRhsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRhsExpression(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_RightHandSide */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_ReturnStatement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_ReturnStatement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_ReturnStatement)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionReturnStatement(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionReturnStatement(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_ReturnStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_ErrorExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_ErrorExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionErrorExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionErrorExpression(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_ErrorExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_CondStatement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_CondStatement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_CondStatement)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_CondStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_LetExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_LetExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_LetExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_LetVarDefs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_LetVarDefs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_LetVarDefs)) return (_currn);
if (IsSymb (_currn, SYMBFunction_LetVarDef)) return (MkrFunctionLetVarDefList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_LetVarDefs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_LetVarDef (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_LetVarDef (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_LetVarDef)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_LetVarDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_DoMessage (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_DoMessage (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_DoMessage)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_DoMessage */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_LambdaExpr (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_LambdaExpr (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_LambdaExpr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_PostfixExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_PostfixExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionPostfixIsPrim(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_PostfixExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_AssignStatement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_AssignStatement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_AssignStatement)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_AssignStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_DoStatements (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_DoStatements (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_DoStatements)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_DoStatements */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_VariableDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_VariableDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_VariableDefId)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionVariableDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_VariableDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_EachStatement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_EachStatement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_EachStatement)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_EachStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkExternConstant (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkExternConstant (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBExternConstant)) return (_currn);
return(NULLNODEPTR);
}/* MkExternConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkExternConstantDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkExternConstantDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBExternConstantDefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrConstantDefTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrConstantDefIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkExternConstantDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_DoStatement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_DoStatement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_DoStatement)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunctionNoReturn)) return (MkrFunctionDoIsNoreturn(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_EachStatement)) return (MkrFunctionDoIsEach(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_AssignStatement)) return (MkrFunctionDoIsAssignement(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_DoMessage)) return (MkrFunctionDoIsMessage(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ReturnStatement)) return (MkrFunctionDoReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_CondStatement)) return (MkrFunctionDoCondition(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionDoExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionDoExpression(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_DoStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunctionNoReturn (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunctionNoReturn (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunctionNoReturn)) return (_currn);
return(NULLNODEPTR);
}/* MkFunctionNoReturn */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_PrimaryExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_PrimaryExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionPrimaryIsTuple(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionPrimaryIsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionPrimaryIsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionPrimaryIsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionPrimaryIsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionPrimaryIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionPrimaryIsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionPrimaryIsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionPrimaryIsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionPrimaryIsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionPrimaryIsExpression(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionPrimaryIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionPrimaryIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionPrimaryIsConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_PrimaryExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_TupleConstruction (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_TupleConstruction (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (_currn);
return(NULLNODEPTR);
}/* MkFunction_TupleConstruction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_TupleArguments (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_TupleArguments (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_TupleArguments)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleArgument)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionTupleArgumentList(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_TupleArguments */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_Expression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_Expression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_Expression)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionExpressionIsLambda(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionExpressionIsLet(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionExpressionIsIf(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionExprIsError(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionExpressionChain(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionExpressionIsBinop(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_Expression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkFunction_TupleArgument (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkFunction_TupleArgument (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBFunction_TupleArgument)) return (_currn);
if (IsSymb (_currn, SYMBFunction_TupleConstruction)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LambdaExpr)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_LetExpression)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_IfExpression)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_ErrorExpression)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Call)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Constant)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PrimaryExpression)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_PostfixExpression)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_UnaryExpression)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_BinaryExpression)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Expression)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrFunctionTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrFunctionTupleArgument(_coordref, _currn));
return(NULLNODEPTR);
}/* MkFunction_TupleArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Decl_Class (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Decl_Class (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Decl_Class)) return (_currn);
return(NULLNODEPTR);
}/* MkAbstree_Decl_Class */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Decl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Decl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Decl)) return (_currn);
return(NULLNODEPTR);
}/* MkAbstree_Decl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_DefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_DefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_DefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeDefIdIsTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrAbstreeDefIdIsIdent(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_DefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Changes (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Changes (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Changes)) return (_currn);
return(NULLNODEPTR);
}/* MkAbstree_Changes */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Change (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Change (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Change)) return (_currn);
if (IsSymb (_currn, SYMBAbstree_Reference)) return (MkrAbstreeChangeIsDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_ReferenceList)) return (MkrAbstreeChangeIsDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Dependency)) return (MkrAbstreeChangeIsDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Transformation)) return (MkrAbstreeChangeIsTrafo(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Inheritance)) return (MkrAbstreeChangeIsInherit(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Combination)) return (MkrAbstreeChangeIsCombination(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Prefix)) return (MkrAbstreeChangeIsPrefix(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeChangeIsDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrAbstreeChangeIsDependency(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_Change */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Prefix (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Prefix (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Prefix)) return (_currn);
return(NULLNODEPTR);
}/* MkAbstree_Prefix */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkNew_Ruleprefix (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkNew_Ruleprefix (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBNew_Ruleprefix)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrNewRuleprefixIsTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrNewRuleprefixIsIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkNew_Ruleprefix */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Combination (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Combination (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Combination)) return (_currn);
if (IsSymb (_currn, SYMBAbstree_Reference)) return (MkrAbstreeCombination(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_ReferenceList)) return (MkrAbstreeCombination(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeCombination(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrAbstreeCombination(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_Combination */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Inheritance (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Inheritance (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Inheritance)) return (_currn);
if (IsSymb (_currn, SYMBAbstree_Reference)) return (MkrAbstreeInheritance(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_ReferenceList)) return (MkrAbstreeInheritance(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeInheritance(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrAbstreeInheritance(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_Inheritance */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Transformation (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Transformation (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Transformation)) return (_currn);
return(NULLNODEPTR);
}/* MkAbstree_Transformation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_ReferenceList (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_ReferenceList (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_ReferenceList)) return (_currn);
if (IsSymb (_currn, SYMBAbstree_Reference)) return (MkrAbstreeReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrAbstreeReferenceList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_ReferenceList */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Dependency (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Dependency (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Dependency)) return (_currn);
if (IsSymb (_currn, SYMBAbstree_Reference)) return (MkrAbstreeDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_ReferenceList)) return (MkrAbstreeDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrAbstreeDependency(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_Dependency */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_Abstree_Rules (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_Abstree_Rules (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_Abstree_Rules)) return (_currn);
if (IsSymb (_currn, SYMBAbstree_Production)) return (MkrAbstreeRulesExist(_coordref, _currn));
if (IsSymb (_currn, SYMBTreeSymbol_DefId)) return (MkrAbstreeRulesExist(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_TermDecl)) return (MkrAbstreeRulesExist(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Rule)) return (MkrAbstreeRulesExist(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Rules)) return (MkrAbstreeRulesExist(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeRulesExist(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_Abstree_Rules */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Rules (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Rules (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Rules)) return (_currn);
if (IsSymb (_currn, SYMBAbstree_Production)) return (MkrAbstreeRuleList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTreeSymbol_DefId)) return (MkrAbstreeRuleList1(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_TermDecl)) return (MkrAbstreeRuleList1(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Rule)) return (MkrAbstreeRuleList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeRuleList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_Rules */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_TermDecl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_TermDecl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_TermDecl)) return (_currn);
if (IsSymb (_currn, SYMBTreeSymbol_DefId)) return (MkrAbstreeTermDeclName(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeTermDeclName(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_TermDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_ConversionReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_ConversionReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_ConversionReference)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrConversionReferenceTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrConversionReferenceIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_ConversionReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Rule (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Rule (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Rule)) return (_currn);
if (IsSymb (_currn, SYMBAbstree_Production)) return (MkrAbstreeRuleIsProduction(_coordref, _currn));
if (IsSymb (_currn, SYMBTreeSymbol_DefId)) return (MkrAbstreeRuleIsTermDecl(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_TermDecl)) return (MkrAbstreeRuleIsTermDecl(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeRuleIsTermDecl(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_Rule */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkTreeSymbol_DefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkTreeSymbol_DefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBTreeSymbol_DefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrTreeSymbolDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkTreeSymbol_DefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Production (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Production (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Production)) return (_currn);
return(NULLNODEPTR);
}/* MkAbstree_Production */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbols (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbols (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbols)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbols */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAbstree_Reference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAbstree_Reference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAbstree_Reference)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrAbstreeReferenceIsTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrAbstreeReferenceIsIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkAbstree_Reference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_Rulename (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_Rulename (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_Rulename)) return (_currn);
if (IsSymb (_currn, SYMBRulename_DefId)) return (MkrRulenameExists(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRulenameExists(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRulenameExists(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_Rulename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRulename_DefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRulename_DefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRulename_DefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrRulenameIsTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRulenameIsIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRulename_DefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkGlobal_AttributeDecl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkGlobal_AttributeDecl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBGlobal_AttributeDecl)) return (_currn);
return(NULLNODEPTR);
}/* MkGlobal_AttributeDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkGlobal_AttributeDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkGlobal_AttributeDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBGlobal_AttributeDefId)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrGlobalAttributeDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkGlobal_AttributeDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkGlobal_AttributeDefIds (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkGlobal_AttributeDefIds (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBGlobal_AttributeDefIds)) return (_currn);
if (IsSymb (_currn, SYMBGlobal_AttributeDefId)) return (MkrGlobalAttributeDefIdList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrGlobalAttributeDefIdList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkGlobal_AttributeDefIds */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_DefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_DefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_DefId)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_DefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Decl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Decl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Decl)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Decl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Classification (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Classification (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Classification)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Classification */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Options (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Options (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Options)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Options */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Inheritance (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Inheritance (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Inheritance)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_Reference_OptTree_List)) return (MkrSymbolInheritance(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Reference_OptTree)) return (MkrSymbolInheritance(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Inheritance */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Reference_OptTree (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Reference_OptTree (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Reference_OptTree)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Reference_OptTree */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Reference_OptTree_List (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Reference_OptTree_List (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Reference_OptTree_List)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_Reference_OptTree)) return (MkrSymbolReferenceOptTreeList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Reference_OptTree_List */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LocalAttributes (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LocalAttributes (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LocalAttributes)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decl)) return (MkrSymbolLocalAttributes(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decls)) return (MkrSymbolLocalAttributes(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_LocalAttributes */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LocalAttribute_Decls (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LocalAttribute_Decls (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decls)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decl)) return (MkrSymbolLocalAttributeDeclList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_LocalAttribute_Decls */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LocalAttribute_Decl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LocalAttribute_Decl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decl)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_LocalAttribute_Decl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LocalAttribute_Decls_NoClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LocalAttribute_Decls_NoClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decls_NoClass)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decl_NoClass)) return (MkrSymbolLocalAttributeDeclNoClassList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_LocalAttribute_Decls_NoClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LocalAttribute_Decl_NoClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LocalAttribute_Decl_NoClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decl_NoClass)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_LocalAttribute_Decl_NoClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LocalAttribute_Defs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LocalAttribute_Defs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Defs)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_DefId)) return (MkrSymbolLocalAttributeDefIdList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolLocalAttributeDefIdList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_LocalAttribute_Defs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LocalAttribute_DefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LocalAttribute_DefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_DefId)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolLocalAttributeDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_LocalAttribute_DefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkLocal_AttributeClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkLocal_AttributeClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBLocal_AttributeClass)) return (_currn);
return(NULLNODEPTR);
}/* MkLocal_AttributeClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkLocal_AttributeReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkLocal_AttributeReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBLocal_AttributeReference)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrLocalAttributeReference(_coordref, _currn));
return(NULLNODEPTR);
}/* MkLocal_AttributeReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_Symbol_Computations (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_Symbol_Computations (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_Symbol_Computations)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_Computations)) return (MkrSymbolComputationsExist(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_Symbol_Computations */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Computations (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Computations (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Computations)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Computations */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Computation (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Computation (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Computation)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolComputationIsOrder(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Computation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_ChainStart (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_ChainStart (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_ChainStart)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_ChainStart */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_SymbolDependency (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_SymbolDependency (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_SymbolDependency)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Attribute_ReferenceList)) return (MkrSymbolDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolDependency(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Attribute_References)) return (MkrSymbolDependency(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_SymbolDependency */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Attribute_References (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Attribute_References (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Attribute_References)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolSymbolAttributeReferencesSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolSymbolAttributeReferencesSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolSymbolAttributeReferencesSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolSymbolAttributeReferencesSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolSymbolAttributeReferencesSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolSymbolAttributeReferencesSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Attribute_ReferenceList)) return (MkrSymbolSymbolAttributeReferencesMulti(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolSymbolAttributeReferencesSingle(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Attribute_References */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Attribute_ReferenceList (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Attribute_ReferenceList (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Attribute_ReferenceList)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolLocalAttributeRefList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolLocalAttributeRefList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolLocalAttributeRefList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolLocalAttributeRefList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolLocalAttributeRefList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolLocalAttributeRefList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolLocalAttributeRefList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Attribute_ReferenceList */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_AssignStatement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_AssignStatement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_AssignStatement)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_AssignStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_OrderedStatements (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_OrderedStatements (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolOrderedStatementList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_OrderedStatements */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LocalAttribute_Reference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LocalAttribute_Reference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_LocalAttribute_Reference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_OrderedStatement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_OrderedStatement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolOrderedStatementIsReturn(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_OrderedStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_RHS_Expression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_RHS_Expression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_RHS_Expression)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Guard_Expression)) return (MkrSymbolRHSExpressionIsGuards(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Guard_Expressions)) return (MkrSymbolRHSExpressionIsGuards(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolRHSExpressionIsCompute(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_RHS_Expression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Guard_Expressions (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Guard_Expressions (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Guard_Expressions)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Guard_Expression)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolGuardExpressionList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Guard_Expressions */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Guard_Expression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Guard_Expression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Guard_Expression)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolGuardExpressionIsDefault(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Guard_Expression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_OrderedComputation (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_OrderedComputation (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolOrderedComputation(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_OrderedComputation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Lambda (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Lambda (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Lambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Lambda_VarDefs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Lambda_VarDefs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Lambda_VarDefs)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_Lambda_VarDef)) return (MkrSymbolLambdaVarDefList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Lambda_VarDefs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Lambda_VarDef (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Lambda_VarDef (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Lambda_VarDef)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Lambda_VarDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression_Let (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression_Let (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Expression_Let */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LetVar_Defs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LetVar_Defs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LetVar_Defs)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_LetVar_Def)) return (MkrSymbolLetVarDefList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_LetVar_Defs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_VariableDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_VariableDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_VariableDefId)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolVariableDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_VariableDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LetVar_Def (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LetVar_Def (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LetVar_Def)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_LetVar_Def */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression_If (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression_If (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Expression_If */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression_Error (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression_Error (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolExpressionError(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Expression_Error */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression_When (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression_When (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_Expression_When */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression_Binary (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression_Binary (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolExpressionBinaryChain(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolExpressionBinaryIsUnary(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Expression_Binary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression_Unary (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression_Unary (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolExpressionUnaryReference(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Expression_Unary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression_Postfix (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression_Postfix (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolExpressionPostfixIsPrimary(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Expression_Postfix */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression_Primary (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression_Primary (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolExpressionPrimaryIsTuple(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolExpressionPrimaryIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolExpressionPrimaryIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolExpressionPrimaryIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolExpressionPrimaryIsConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Expression_Primary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_TupleConstruction (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_TupleConstruction (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_TupleConstruction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_TupleArguments (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_TupleArguments (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_TupleArguments)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleArgument)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolTupleArgumentList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_TupleArguments */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_TupleArgument (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_TupleArgument (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_TupleArgument)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolTupleArgument(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_TupleArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression_Call (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression_Call (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolExpressionCallVariable(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Expression_Call */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_LocalAttribute_Ref (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_LocalAttribute_Ref (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolLocalAttributeReferenceIsRemote(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolLocalAttributeReferenceIsRemote(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolLocalAttributeReferenceIsRemote(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolLocalAttributeReferenceIsRemote(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolLocalAttributeReferenceIsLocal(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolLocalAttributeReferenceIsRemote(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_LocalAttribute_Ref */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_CallableReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_CallableReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolCallableReferenceIsAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolCallableReferenceIsAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolCallableReferenceIsAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolCallableReferenceIsAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolCallableReferenceIsAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolCallableReferenceIsAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolCallableReferenceIsAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolCallableReferenceIsTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolCallableReferenceIsIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_CallableReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_CallParameters (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_CallParameters (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_CallParameters)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallParameter)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolCallParameterList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_CallParameters */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Expression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Expression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Expression)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolExpressionIsLambda(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolExpressionIsLet(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolExpressionIsIf(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolExpressionIsError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolExpressionIsWhen(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolExpressionChain(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolExpressionIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolExpressionIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolExpressionIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolExpressionIsBinary(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Expression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_CallParameter (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_CallParameter (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_CallParameter)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_CallableReference)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_TupleConstruction)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Call)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Constant)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Primary)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Postfix)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Unary)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Binary)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Lambda)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Let)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_If)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_Error)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression_When)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Expression)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatement)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedStatements)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Reference)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Ref)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_OrderedComputation)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolCallParameter(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_CallParameter */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Constant (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Constant (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Constant)) return (_currn);
if (IsSymb (_currn, SYMBConstant)) return (MkrSymbolConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrSymbolConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Constant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDecl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDecl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDecl)) return (_currn);
if (IsSymb (_currn, SYMBRule_Specification)) return (MkrDeclRuleSpecification(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Decl)) return (MkrDeclSymbol(_coordref, _currn));
if (IsSymb (_currn, SYMBGlobal_AttributeDecl)) return (MkrDeclGlobalAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Decl)) return (MkrDeclAbstree(_coordref, _currn));
if (IsSymb (_currn, SYMBAbstree_Decl_Class)) return (MkrDeclAbstreeClass(_coordref, _currn));
if (IsSymb (_currn, SYMBExternConstant)) return (MkrDeclConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Implementation)) return (MkrDeclFunctionImplementation(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_TypeDecl)) return (MkrDeclFunctionType(_coordref, _currn));
if (IsSymb (_currn, SYMBData_DefId)) return (MkrDeclData(_coordref, _currn));
if (IsSymb (_currn, SYMBData_Decl)) return (MkrDeclData(_coordref, _currn));
if (IsSymb (_currn, SYMBNewType_Decl)) return (MkrDeclTypedef(_coordref, _currn));
if (IsSymb (_currn, SYMBCText)) return (MkrDeclCopyLido(_coordref, _currn));
if (IsSymb (_currn, SYMBCopy_Lido)) return (MkrDeclCopyLido(_coordref, _currn));
if (IsSymb (_currn, SYMBCopy_Head)) return (MkrDeclCopyHead(_coordref, _currn));
if (IsSymb (_currn, SYMBCopy_Source)) return (MkrDeclCopySource(_coordref, _currn));
if (IsSymb (_currn, SYMBPDL_Definitions)) return (MkrDeclPDL(_coordref, _currn));
if (IsSymb (_currn, SYMBPDL_Decl)) return (MkrDeclPDL(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrDeclData(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Definitions)) return (MkrDeclPTG(_coordref, _currn));
if (IsSymb (_currn, SYMBPTG_Decl)) return (MkrDeclPTG(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrDeclLoad(_coordref, _currn));
if (IsSymb (_currn, SYMBLoad_Decl)) return (MkrDeclLoad(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Specification (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Specification (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Specification)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Specification */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Guard (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Guard (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Guard)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Guard */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_GuardRhs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_GuardRhs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_GuardRhs)) return (_currn);
if (IsSymb (_currn, SYMBRule_Computations)) return (MkrRuleGuardRhsComputes(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Guard)) return (MkrRuleGuardRhsGuards(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_GuardRhs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Classification (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Classification (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Classification)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Classification */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Pattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Pattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Pattern)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Pattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_References (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_References (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_References)) return (_currn);
if (IsSymb (_currn, SYMBRule_Reference)) return (MkrRuleReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleReferenceList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_References */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_LHSPattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_LHSPattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_LHSPattern)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_LHSPattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_Production (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_Production (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_Production)) return (_currn);
return(NULLNODEPTR);
}/* MkOpt_Production */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_RHSPattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_RHSPattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_RHSPattern)) return (_currn);
if (IsSymb (_currn, SYMBRule_RHSPatternSymbols)) return (MkrRuleRHSPattern(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_RHSPattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_RHSPatternSymbols (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_RHSPatternSymbols (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_RHSPatternSymbols)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_RHSPatternSymbols */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_Reference_OptTree)) return (MkrSymbolIsReference(_coordref, _currn));
if (IsSymb (_currn, SYMBLidoLiteral)) return (MkrSymbolIsLiteral(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_RHSPatternSymbol (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_RHSPatternSymbol (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_RHSPatternSymbol)) return (_currn);
if (IsSymb (_currn, SYMBRule_VariableDefId)) return (MkrRulePatternSymbolDontCare(_coordref, _currn));
if (IsSymb (_currn, SYMBOpt_RuleVariableDefId)) return (MkrRulePatternSymbolDontCare(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRulePatternSymbolMatchTermInt(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRulePatternSymbolDontCare(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRulePatternSymbolMatchTermString(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_RHSPatternSymbol */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_DeeperRulePattern (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_DeeperRulePattern (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_DeeperRulePattern)) return (_currn);
if (IsSymb (_currn, SYMBDeeper_RulePatternSymbols)) return (MkrOptDeeperRulePattern(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_DeeperRulePattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDeeper_RulePatternSymbols (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDeeper_RulePatternSymbols (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDeeper_RulePatternSymbols)) return (_currn);
return(NULLNODEPTR);
}/* MkDeeper_RulePatternSymbols */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_RuleVariableDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_RuleVariableDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_RuleVariableDefId)) return (_currn);
if (IsSymb (_currn, SYMBRule_VariableDefId)) return (MkrCreateRuleVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrCreateRuleVariable(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_RuleVariableDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkDeeper_RulePatternSymbol (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkDeeper_RulePatternSymbol (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBDeeper_RulePatternSymbol)) return (_currn);
if (IsSymb (_currn, SYMBNumber)) return (MkrDeeperRulePatternSymbolMatchTermInt(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrDeeperRulePatternSymbolMatchTermString(_coordref, _currn));
return(NULLNODEPTR);
}/* MkDeeper_RulePatternSymbol */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Computations (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Computations (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Computations)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Computations */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Computation (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Computation (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Computation)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleComputeOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleComputeOrder(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Computation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_ChainStart (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_ChainStart (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_ChainStart)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_ChainStart */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_AssignStatement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_AssignStatement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_AssignStatement)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_AssignStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_RuleDependency (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_RuleDependency (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_RuleDependency)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleDependencies(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleDependencies(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleDependencies(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleDependencies(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleDependencies(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_AttributeReferenceList)) return (MkrRuleDependencies(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_AttributeReference)) return (MkrRuleDependencies(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_AttributeReferences)) return (MkrRuleDependencies(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleDependencies(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_RuleDependency */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_AttributeReferences (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_AttributeReferences (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_AttributeReferences)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleAttributeReferenceSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleAttributeReferenceSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleAttributeReferenceSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleAttributeReferenceSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleAttributeReferenceSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_AttributeReferenceList)) return (MkrRuleAttributeReferenceMulti(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_AttributeReference)) return (MkrRuleAttributeReferenceSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleAttributeReferenceSingle(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_AttributeReferences */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_AttributeReferenceList (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_AttributeReferenceList (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_AttributeReferenceList)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleAttributeReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleAttributeReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleAttributeReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleAttributeReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleAttributeReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_AttributeReference)) return (MkrRuleAttributeReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleAttributeReferenceList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_AttributeReferenceList */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_AttributeReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_AttributeReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_AttributeReference)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleAttributeReferenceIsRemote(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleAttributeReferenceIsRemote(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleAttributeReferenceIsRemote(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleAttributeReferenceIsRemote(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleAttributeReferenceIsSymbolAttrRef(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleAttributeReferenceIsRemote(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_AttributeReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_AttributeClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_AttributeClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_AttributeClass)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_AttributeClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_VariableReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_VariableReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_VariableReference)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleVariableReference(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_VariableReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_OrderedStatements (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_OrderedStatements (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleOrderedStatementList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_OrderedStatements */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_OrderedStatement (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_OrderedStatement (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleOrderedStatementIsReturn(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_OrderedStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_RHS_Expression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_RHS_Expression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_RHS_Expression)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_GuardExpression)) return (MkrRuleRhsExprIsGuards(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_GuardExpressions)) return (MkrRuleRhsExprIsGuards(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleRhsExprIsExpr(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_RHS_Expression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_GuardExpressions (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_GuardExpressions (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_GuardExpressions)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_GuardExpression)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleGuardExpressionList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_GuardExpressions */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_GuardExpression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_GuardExpression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_GuardExpression)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleGuardIsDefault(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleGuardIsDefault(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_GuardExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_OrderedComputation (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_OrderedComputation (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleOrderedComputation(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleOrderedComputation(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_OrderedComputation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Lambda_VarDefs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Lambda_VarDefs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Lambda_VarDefs)) return (_currn);
if (IsSymb (_currn, SYMBRule_Lambda_VarDef)) return (MkrRuleLambdaVarDefList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Lambda_VarDefs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Lambda_VarDef (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Lambda_VarDef (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Lambda_VarDef)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Lambda_VarDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression_Let (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression_Let (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Expression_Let */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_LetVar_Defs (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_LetVar_Defs (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_LetVar_Defs)) return (_currn);
if (IsSymb (_currn, SYMBRule_LetVar_Def)) return (MkrRuleLetVarDefList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_LetVar_Defs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOptionalType (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOptionalType (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOptionalType)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrOptionalType(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrOptionalType(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOptionalType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_VariableDefId (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_VariableDefId (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_VariableDefId)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleVariableDefId(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_VariableDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_LetVar_Def (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_LetVar_Def (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_LetVar_Def)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_LetVar_Def */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression_If (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression_If (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression_If)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Expression_If */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression_Error (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression_Error (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleExpressionError(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleExpressionError(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Expression_Error */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression_When (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression_When (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression_When)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Expression_When */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression_Binary (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression_Binary (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleExpressionBinaryChain(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleExpressionBinaryIsUnary(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Expression_Binary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression_Unary (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression_Unary (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleExpressionUnaryReference(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Expression_Unary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression_Postfix (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression_Postfix (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleExpressionPostfixIsPrimary(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Expression_Postfix */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression_Primary (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression_Primary (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleExpressionPrimaryIsRemoteAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleExpressionPrimaryIsRemoteAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleExpressionPrimaryIsRemoteAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleExpressionPrimaryIsRemoteAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleExpressionPrimaryIsTuple(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleExpressionPrimaryIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleExpressionPrimaryIsWrap(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleExpressionPrimaryIsRemoteAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleExpressionPrimaryIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleExpressionPrimaryIsConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleExpressionPrimaryIsCall(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleExpressionPrimaryIsConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Expression_Primary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_TupleConstruction (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_TupleConstruction (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_TupleConstruction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_TupleArguments (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_TupleArguments (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_TupleArguments)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleArgument)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleTupleArgumentList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_TupleArguments */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_TupleArgument (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_TupleArgument (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_TupleArgument)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleTupleArgument(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleTupleArgument(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_TupleArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression_Call (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression_Call (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (_currn);
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleExpressionCallVariable(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleExpressionCallVariable(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Expression_Call */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_SymbolAttributeReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_SymbolAttributeReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_SymbolAttributeReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_TreeHint (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_TreeHint (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_TreeHint)) return (_currn);
if (IsSymb (_currn, SYMBAbstree_Reference)) return (MkrOptTreeHint(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrOptTreeHint(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrOptTreeHint(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_TreeHint */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Reference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Reference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Reference)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleReferenceIsTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleReferenceIsIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Reference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_CallableReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_CallableReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_CallableReference)) return (_currn);
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleCallableReferenceIsAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleCallableReferenceIsTypename(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleCallableReferenceIsIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_CallableReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_CallParameters (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_CallParameters (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_CallParameters)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallParameter)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleCallParameterList1(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleCallParameterList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_CallParameters */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Expression (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Expression (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Expression)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleExpressionIsLambda(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleExpressionIsLet(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleExpressionIsIf(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleExpressionIsError(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleExpressionIsWhen(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleExpressionChain(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleExpressionIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleExpressionIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleExpressionIsOrder(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleExpressionIsBinary(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Expression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_CallParameter (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_CallParameter (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_CallParameter)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_CallableReference)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_TupleConstruction)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Call)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Constant)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Primary)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Postfix)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Unary)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Binary)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Let)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_If)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_Error)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression_When)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Expression)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatement)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedStatements)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_SymbolAttributeReference)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_OrderedComputation)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Attribute)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRuleCallParameter(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleCallParameter(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_CallParameter */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Constant (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Constant (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Constant)) return (_currn);
if (IsSymb (_currn, SYMBConstant)) return (MkrRuleConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRuleConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRuleConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRule_Constant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_Attribute (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_Attribute (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_Attribute)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRemoteUp(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRemoteUp(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Constituent)) return (MkrRemoteDown(_coordref, _currn));
if (IsSymb (_currn, SYMBRemote_Including)) return (MkrRemoteUp(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemote_Attribute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_Including (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_Including (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_Including)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrRemoteIncluding(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (MkrRemoteIncluding(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemote_Including */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_AttributeReferenceList (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_AttributeReferenceList (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_AttributeReferenceList)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolAttributeReferenceList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_AttributeReferenceList */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbolAttribute (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbolAttribute (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbolAttribute)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbolAttribute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_AttributeReferences (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_AttributeReferences (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_AttributeReferences)) return (_currn);
if (IsSymb (_currn, SYMBSymbolAttribute)) return (MkrSymbolAttributeReferencesSingle(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_AttributeReferences */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_Constituent (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_Constituent (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_Constituent)) return (_currn);
return(NULLNODEPTR);
}/* MkRemote_Constituent */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_SymbolIndex (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_SymbolIndex (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_SymbolIndex)) return (_currn);
if (IsSymb (_currn, SYMBNumber)) return (MkrSymbolIndex(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_SymbolIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_SymbolReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_SymbolReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_SymbolReference)) return (_currn);
return(NULLNODEPTR);
}/* MkOpt_SymbolReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_Options (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_Options (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_Options)) return (_currn);
if (IsSymb (_currn, SYMBRemote_Shield)) return (MkrRemoteOptionsShield(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Reference)) return (MkrRemoteOptionsShield(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRemoteOptionsShield(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemote_Options */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkOpt_Remote_Shield (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkOpt_Remote_Shield (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBOpt_Remote_Shield)) return (_currn);
if (IsSymb (_currn, SYMBRemote_Shield)) return (MkrOptRemoteShield(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Reference)) return (MkrOptRemoteShield(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrOptRemoteShield(_coordref, _currn));
return(NULLNODEPTR);
}/* MkOpt_Remote_Shield */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_Shield (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_Shield (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_Shield)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_Reference)) return (MkrRemoteShieldSingle(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRemoteShieldSingle(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemote_Shield */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Reference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Reference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Reference)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolReference(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Reference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_References (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_References (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_References)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_Reference)) return (MkrSymbolReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrSymbolReferenceList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_References */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_AppendBinary (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_AppendBinary (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_AppendBinary)) return (_currn);
return(NULLNODEPTR);
}/* MkRemote_AppendBinary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRule_Lambda (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRule_Lambda (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRule_Lambda)) return (_currn);
return(NULLNODEPTR);
}/* MkRule_Lambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkCallable_Reference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkCallable_Reference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBCallable_Reference)) return (_currn);
if (IsSymb (_currn, SYMBTypename)) return (MkrCallType(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrCallIdentifier(_coordref, _currn));
return(NULLNODEPTR);
}/* MkCallable_Reference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkConstant (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkConstant (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBConstant)) return (_currn);
if (IsSymb (_currn, SYMBNumber)) return (MkrConstantNumber(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrConstantString(_coordref, _currn));
return(NULLNODEPTR);
}/* MkConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_Empty (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_Empty (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_Empty)) return (_currn);
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrRemoteEmptyCall(_coordref, _currn));
if (IsSymb (_currn, SYMBConstant)) return (MkrRemoteEmptyConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBNumber)) return (MkrRemoteEmptyConstant(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRemoteEmptyCall(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRemoteEmptyCall(_coordref, _currn));
if (IsSymb (_currn, SYMBString)) return (MkrRemoteEmptyConstant(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemote_Empty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_Single (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_Single (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_Single)) return (_currn);
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRemoteSingleLambda(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrRemoteSingleCall(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRemoteSingleCall(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRemoteSingleCall(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemote_Single */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_Append (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_Append (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_Append)) return (_currn);
if (IsSymb (_currn, SYMBRemote_AppendBinary)) return (MkrRemoteAppendIsBinary(_coordref, _currn));
if (IsSymb (_currn, SYMBRule_Lambda)) return (MkrRemoteAppendLambda(_coordref, _currn));
if (IsSymb (_currn, SYMBCallable_Reference)) return (MkrRemoteAppendCall(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrRemoteAppendCall(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrRemoteAppendCall(_coordref, _currn));
return(NULLNODEPTR);
}/* MkRemote_Append */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkRemote_With (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkRemote_With (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBRemote_With)) return (_currn);
return(NULLNODEPTR);
}/* MkRemote_With */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_Option (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_Option (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_Option)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_UsingDecl)) return (MkrSymbolOptionUsing(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_UsingDecls)) return (MkrSymbolOptionUsing(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_UsingAttribute)) return (MkrSymbolOptionUsing(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decl)) return (MkrSymbolOptionAttributes(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttribute_Decls)) return (MkrSymbolOptionAttributes(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Reference_OptTree_List)) return (MkrSymbolOptionInheritance(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Inheritance)) return (MkrSymbolOptionInheritance(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_LocalAttributes)) return (MkrSymbolOptionAttributes(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_Reference_OptTree)) return (MkrSymbolOptionInheritance(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_Option */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_UsingAttribute (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_UsingAttribute (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_UsingAttribute)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_UsingDecl)) return (MkrSymbolUsingAttribute(_coordref, _currn));
if (IsSymb (_currn, SYMBSymbol_UsingDecls)) return (MkrSymbolUsingAttribute(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_UsingAttribute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_UsingDecls (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_UsingDecls (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_UsingDecls)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_UsingDecl)) return (MkrSymbolUsingDeclList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_UsingDecls */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkAttributeClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkAttributeClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBAttributeClass)) return (_currn);
return(NULLNODEPTR);
}/* MkAttributeClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_UsingDecl (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_UsingDecl (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_UsingDecl)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_UsingDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_UsingDeclsNoClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_UsingDeclsNoClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_UsingDeclsNoClass)) return (_currn);
if (IsSymb (_currn, SYMBSymbol_UsingDeclNoClass)) return (MkrSymbolUsingDeclsNoClassList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_UsingDeclsNoClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSimple_Typing (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSimple_Typing (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSimple_Typing)) return (_currn);
if (IsSymb (_currn, SYMBType_VariableDefId)) return (MkrTypeTemplate(_coordref, _currn));
if (IsSymb (_currn, SYMBFunction_Typing)) return (MkrTypeOfFunction(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Construction)) return (MkrTypingList(_coordref, _currn));
if (IsSymb (_currn, SYMBType_MapOf)) return (MkrTypingMap(_coordref, _currn));
if (IsSymb (_currn, SYMBType_TupleOf)) return (MkrTypingTuple(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Listof)) return (MkrTypingList(_coordref, _currn));
if (IsSymb (_currn, SYMBType_Reference)) return (MkrTypingReference(_coordref, _currn));
if (IsSymb (_currn, SYMBSimple_Typing)) return (MkrTypingList(_coordref, _currn));
if (IsSymb (_currn, SYMBEliType_Reference)) return (MkrTypingBasic(_coordref, _currn));
if (IsSymb (_currn, SYMBTypename)) return (MkrTypingReference(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrTypeTemplate(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSimple_Typing */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_UsingDeclNoClass (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_UsingDeclNoClass (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_UsingDeclNoClass)) return (_currn);
return(NULLNODEPTR);
}/* MkSymbol_UsingDeclNoClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkLocal_UsingReferences (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkLocal_UsingReferences (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBLocal_UsingReferences)) return (_currn);
if (IsSymb (_currn, SYMBLocal_UsingReference)) return (MkrSymbolLocalAttributeReferenceList1(_coordref, _currn));
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolLocalAttributeReferenceList1(_coordref, _currn));
return(NULLNODEPTR);
}/* MkLocal_UsingReferences */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkLocal_UsingReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkLocal_UsingReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBLocal_UsingReference)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrLocalUsingReference(_coordref, _currn));
return(NULLNODEPTR);
}/* MkLocal_UsingReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkSymbol_AttributeReference (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkSymbol_AttributeReference (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBSymbol_AttributeReference)) return (_currn);
if (IsSymb (_currn, SYMBIdentifier)) return (MkrSymbolAttributeReference(_coordref, _currn));
return(NULLNODEPTR);
}/* MkSymbol_AttributeReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkString (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkString (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBString)) return (_currn);
return(NULLNODEPTR);
}/* MkString */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkIdentifier (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkIdentifier (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBIdentifier)) return (_currn);
return(NULLNODEPTR);
}/* MkIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkTypename (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkTypename (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBTypename)) return (_currn);
return(NULLNODEPTR);
}/* MkTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkNumber (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkNumber (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBNumber)) return (_currn);
return(NULLNODEPTR);
}/* MkNumber */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkCText (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkCText (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBCText)) return (_currn);
return(NULLNODEPTR);
}/* MkCText */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkLambda (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkLambda (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBLambda)) return (_currn);
return(NULLNODEPTR);
}/* MkLambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkLidoLiteral (POSITION *_coordref, NODEPTR _currn)
#else
NODEPTR MkLidoLiteral (_coordref, _currn)
	POSITION *_coordref; NODEPTR _currn;
#endif
{
if (_currn == NULLNODEPTR) return NULLNODEPTR;
if (IsSymb (_currn, SYMBLidoLiteral)) return (_currn);
return(NULLNODEPTR);
}/* MkLidoLiteral */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_07 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_07 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_07 _currn;
#ifdef __cplusplus
_currn = new _TPrule_07;
#else
_currn = (_TPPrule_07) TreeNodeAlloc (sizeof (struct _TPrule_07));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_07;
_SETCOORD(_currn)
_TERMACT_rule_07;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "glaTypename", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_07 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_06 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_06 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_06 _currn;
#ifdef __cplusplus
_currn = new _TPrule_06;
#else
_currn = (_TPPrule_06) TreeNodeAlloc (sizeof (struct _TPrule_06));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_06;
_SETCOORD(_currn)
_TERMACT_rule_06;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "glaString", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_06 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_05 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_05 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_05 _currn;
#ifdef __cplusplus
_currn = new _TPrule_05;
#else
_currn = (_TPPrule_05) TreeNodeAlloc (sizeof (struct _TPrule_05));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_05;
_SETCOORD(_currn)
_TERMACT_rule_05;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "glaNumber", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_05 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_04 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_04 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_04 _currn;
#ifdef __cplusplus
_currn = new _TPrule_04;
#else
_currn = (_TPPrule_04) TreeNodeAlloc (sizeof (struct _TPrule_04));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_04;
_SETCOORD(_currn)
_TERMACT_rule_04;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "glaLiteral", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_04 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_03 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_03 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_03 _currn;
#ifdef __cplusplus
_currn = new _TPrule_03;
#else
_currn = (_TPPrule_03) TreeNodeAlloc (sizeof (struct _TPrule_03));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_03;
_SETCOORD(_currn)
_TERMACT_rule_03;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "glaBackslash", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_03 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_02 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_02 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_02 _currn;
#ifdef __cplusplus
_currn = new _TPrule_02;
#else
_currn = (_TPPrule_02) TreeNodeAlloc (sizeof (struct _TPrule_02));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_02;
_SETCOORD(_currn)
_TERMACT_rule_02;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "glaIdentifier", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_02 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR Mkrule_01 (POSITION *_coordref, int _TERM1)
#else
NODEPTR Mkrule_01 (_coordref, _TERM1)
	POSITION *_coordref;
	int _TERM1;
#endif
{	_TPPrule_01 _currn;
#ifdef __cplusplus
_currn = new _TPrule_01;
#else
_currn = (_TPPrule_01) TreeNodeAlloc (sizeof (struct _TPrule_01));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErule_01;
_SETCOORD(_currn)
_TERMACT_rule_01;
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
_dapto_term_int((_currn)->_uid, "glaCText", _TERM1);
#endif

return ( (NODEPTR) _currn);
}/* Mkrule_01 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrProgramRoot (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrProgramRoot (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrProgramRoot _currn;
#ifdef __cplusplus
_currn = new _TPrProgramRoot;
#else
_currn = (_TPPrProgramRoot) TreeNodeAlloc (sizeof (struct _TPrProgramRoot));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErProgramRoot;
_currn->_desc1 = (_TSPProgram) MkProgram (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rProgramRoot: root of subtree no. 1 can not be made a Program node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rProgramRoot;
return ( (NODEPTR) _currn);
}/* MkrProgramRoot */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrProgram (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrProgram (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrProgram _currn;
#ifdef __cplusplus
_currn = new _TPrProgram;
#else
_currn = (_TPPrProgram) TreeNodeAlloc (sizeof (struct _TPrProgram));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErProgram;
_currn->_desc1 = (_TSPDecls) MkDecls (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rProgram: root of subtree no. 1 can not be made a Decls node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rProgram;
return ( (NODEPTR) _currn);
}/* MkrProgram */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDeclList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDeclList2 _currn;
#ifdef __cplusplus
_currn = new _TPrDeclList2;
#else
_currn = (_TPPrDeclList2) TreeNodeAlloc (sizeof (struct _TPrDeclList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclList2;
_currn->_desc1 = (_TSPDecls) MkDecls (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclList2: root of subtree no. 1 can not be made a Decls node ", 0, _coordref);
_currn->_desc2 = (_TSPDecl) MkDecl (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclList2: root of subtree no. 2 can not be made a Decl node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDeclList2;
return ( (NODEPTR) _currn);
}/* MkrDeclList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoDecls (POSITION *_coordref)
#else
NODEPTR MkrNoDecls (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoDecls _currn;
#ifdef __cplusplus
_currn = new _TPrNoDecls;
#else
_currn = (_TPPrNoDecls) TreeNodeAlloc (sizeof (struct _TPrNoDecls));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoDecls;
_SETCOORD(_currn)
_TERMACT_rNoDecls;
return ( (NODEPTR) _currn);
}/* MkrNoDecls */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclLoad (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclLoad (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclLoad _currn;
#ifdef __cplusplus
_currn = new _TPrDeclLoad;
#else
_currn = (_TPPrDeclLoad) TreeNodeAlloc (sizeof (struct _TPrDeclLoad));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclLoad;
_currn->_desc1 = (_TSPLoad_Decl) MkLoad_Decl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclLoad: root of subtree no. 1 can not be made a Load_Decl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclLoad;
return ( (NODEPTR) _currn);
}/* MkrDeclLoad */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrLoadDecl (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrLoadDecl (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrLoadDecl _currn;
#ifdef __cplusplus
_currn = new _TPrLoadDecl;
#else
_currn = (_TPPrLoadDecl) TreeNodeAlloc (sizeof (struct _TPrLoadDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErLoadDecl;
_currn->_desc1 = (_TSPString) MkString (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rLoadDecl: root of subtree no. 1 can not be made a String node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rLoadDecl;
return ( (NODEPTR) _currn);
}/* MkrLoadDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclPTG (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclPTG (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclPTG _currn;
#ifdef __cplusplus
_currn = new _TPrDeclPTG;
#else
_currn = (_TPPrDeclPTG) TreeNodeAlloc (sizeof (struct _TPrDeclPTG));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclPTG;
_currn->_desc1 = (_TSPPTG_Decl) MkPTG_Decl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclPTG: root of subtree no. 1 can not be made a PTG_Decl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclPTG;
return ( (NODEPTR) _currn);
}/* MkrDeclPTG */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGDecl (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGDecl (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGDecl _currn;
#ifdef __cplusplus
_currn = new _TPrPTGDecl;
#else
_currn = (_TPPrPTGDecl) TreeNodeAlloc (sizeof (struct _TPrPTGDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGDecl;
_currn->_desc1 = (_TSPPTG_Definitions) MkPTG_Definitions (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGDecl: root of subtree no. 1 can not be made a PTG_Definitions node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGDecl;
return ( (NODEPTR) _currn);
}/* MkrPTGDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGDefList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrPTGDefList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrPTGDefList2 _currn;
#ifdef __cplusplus
_currn = new _TPrPTGDefList2;
#else
_currn = (_TPPrPTGDefList2) TreeNodeAlloc (sizeof (struct _TPrPTGDefList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGDefList2;
_currn->_desc1 = (_TSPPTG_Definitions) MkPTG_Definitions (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGDefList2: root of subtree no. 1 can not be made a PTG_Definitions node ", 0, _coordref);
_currn->_desc2 = (_TSPPTG_Definition) MkPTG_Definition (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGDefList2: root of subtree no. 2 can not be made a PTG_Definition node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rPTGDefList2;
return ( (NODEPTR) _currn);
}/* MkrPTGDefList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoPTGDefs (POSITION *_coordref)
#else
NODEPTR MkrNoPTGDefs (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoPTGDefs _currn;
#ifdef __cplusplus
_currn = new _TPrNoPTGDefs;
#else
_currn = (_TPPrNoPTGDefs) TreeNodeAlloc (sizeof (struct _TPrNoPTGDefs));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoPTGDefs;
_SETCOORD(_currn)
_TERMACT_rNoPTGDefs;
return ( (NODEPTR) _currn);
}/* MkrNoPTGDefs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGDefinition (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrPTGDefinition (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrPTGDefinition _currn;
#ifdef __cplusplus
_currn = new _TPrPTGDefinition;
#else
_currn = (_TPPrPTGDefinition) TreeNodeAlloc (sizeof (struct _TPrPTGDefinition));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGDefinition;
_currn->_desc1 = (_TSPPTG_DefId) MkPTG_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGDefinition: root of subtree no. 1 can not be made a PTG_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPPTG_Patterns) MkPTG_Patterns (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGDefinition: root of subtree no. 2 can not be made a PTG_Patterns node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rPTGDefinition;
return ( (NODEPTR) _currn);
}/* MkrPTGDefinition */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGDefIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGDefIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGDefIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrPTGDefIdentifier;
#else
_currn = (_TPPrPTGDefIdentifier) TreeNodeAlloc (sizeof (struct _TPrPTGDefIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGDefIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGDefIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGDefIdentifier;
return ( (NODEPTR) _currn);
}/* MkrPTGDefIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGDefTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGDefTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGDefTypename _currn;
#ifdef __cplusplus
_currn = new _TPrPTGDefTypename;
#else
_currn = (_TPPrPTGDefTypename) TreeNodeAlloc (sizeof (struct _TPrPTGDefTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGDefTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGDefTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGDefTypename;
return ( (NODEPTR) _currn);
}/* MkrPTGDefTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGPatternList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrPTGPatternList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrPTGPatternList2 _currn;
#ifdef __cplusplus
_currn = new _TPrPTGPatternList2;
#else
_currn = (_TPPrPTGPatternList2) TreeNodeAlloc (sizeof (struct _TPrPTGPatternList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGPatternList2;
_currn->_desc1 = (_TSPPTG_Patterns) MkPTG_Patterns (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGPatternList2: root of subtree no. 1 can not be made a PTG_Patterns node ", 0, _coordref);
_currn->_desc2 = (_TSPPTG_Pattern) MkPTG_Pattern (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGPatternList2: root of subtree no. 2 can not be made a PTG_Pattern node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rPTGPatternList2;
return ( (NODEPTR) _currn);
}/* MkrPTGPatternList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGPatternEmpty (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGPatternEmpty (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGPatternEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrPTGPatternEmpty;
#else
_currn = (_TPPrPTGPatternEmpty) TreeNodeAlloc (sizeof (struct _TPrPTGPatternEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGPatternEmpty;
_currn->_desc1 = (_TSPPTG_Pattern) MkPTG_Pattern (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGPatternEmpty: root of subtree no. 1 can not be made a PTG_Pattern node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGPatternEmpty;
return ( (NODEPTR) _currn);
}/* MkrPTGPatternEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGPatternInsert (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGPatternInsert (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGPatternInsert _currn;
#ifdef __cplusplus
_currn = new _TPrPTGPatternInsert;
#else
_currn = (_TPPrPTGPatternInsert) TreeNodeAlloc (sizeof (struct _TPrPTGPatternInsert));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGPatternInsert;
_currn->_desc1 = (_TSPPTG_Insertion) MkPTG_Insertion (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGPatternInsert: root of subtree no. 1 can not be made a PTG_Insertion node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGPatternInsert;
return ( (NODEPTR) _currn);
}/* MkrPTGPatternInsert */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGPatternCall (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGPatternCall (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGPatternCall _currn;
#ifdef __cplusplus
_currn = new _TPrPTGPatternCall;
#else
_currn = (_TPPrPTGPatternCall) TreeNodeAlloc (sizeof (struct _TPrPTGPatternCall));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGPatternCall;
_currn->_desc1 = (_TSPPTG_Call) MkPTG_Call (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGPatternCall: root of subtree no. 1 can not be made a PTG_Call node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGPatternCall;
return ( (NODEPTR) _currn);
}/* MkrPTGPatternCall */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGPatternString (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGPatternString (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGPatternString _currn;
#ifdef __cplusplus
_currn = new _TPrPTGPatternString;
#else
_currn = (_TPPrPTGPatternString) TreeNodeAlloc (sizeof (struct _TPrPTGPatternString));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGPatternString;
_currn->_desc1 = (_TSPString) MkString (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGPatternString: root of subtree no. 1 can not be made a String node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGPatternString;
return ( (NODEPTR) _currn);
}/* MkrPTGPatternString */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGPatternOptional (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGPatternOptional (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGPatternOptional _currn;
#ifdef __cplusplus
_currn = new _TPrPTGPatternOptional;
#else
_currn = (_TPPrPTGPatternOptional) TreeNodeAlloc (sizeof (struct _TPrPTGPatternOptional));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGPatternOptional;
_currn->_desc1 = (_TSPPTG_Optional) MkPTG_Optional (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGPatternOptional: root of subtree no. 1 can not be made a PTG_Optional node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGPatternOptional;
return ( (NODEPTR) _currn);
}/* MkrPTGPatternOptional */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGInsertion (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrPTGInsertion (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrPTGInsertion _currn;
#ifdef __cplusplus
_currn = new _TPrPTGInsertion;
#else
_currn = (_TPPrPTGInsertion) TreeNodeAlloc (sizeof (struct _TPrPTGInsertion));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGInsertion;
_currn->_desc1 = (_TSPPTG_OptionalIndex) MkPTG_OptionalIndex (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGInsertion: root of subtree no. 1 can not be made a PTG_OptionalIndex node ", 0, _coordref);
_currn->_desc2 = (_TSPPTG_OptionalType) MkPTG_OptionalType (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGInsertion: root of subtree no. 2 can not be made a PTG_OptionalType node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rPTGInsertion;
return ( (NODEPTR) _currn);
}/* MkrPTGInsertion */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGNoIndex (POSITION *_coordref)
#else
NODEPTR MkrPTGNoIndex (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrPTGNoIndex _currn;
#ifdef __cplusplus
_currn = new _TPrPTGNoIndex;
#else
_currn = (_TPPrPTGNoIndex) TreeNodeAlloc (sizeof (struct _TPrPTGNoIndex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGNoIndex;
_SETCOORD(_currn)
_TERMACT_rPTGNoIndex;
return ( (NODEPTR) _currn);
}/* MkrPTGNoIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGHasIndex (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGHasIndex (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGHasIndex _currn;
#ifdef __cplusplus
_currn = new _TPrPTGHasIndex;
#else
_currn = (_TPPrPTGHasIndex) TreeNodeAlloc (sizeof (struct _TPrPTGHasIndex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGHasIndex;
_currn->_desc1 = (_TSPPTG_Index) MkPTG_Index (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGHasIndex: root of subtree no. 1 can not be made a PTG_Index node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGHasIndex;
return ( (NODEPTR) _currn);
}/* MkrPTGHasIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGIndex (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGIndex (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGIndex _currn;
#ifdef __cplusplus
_currn = new _TPrPTGIndex;
#else
_currn = (_TPPrPTGIndex) TreeNodeAlloc (sizeof (struct _TPrPTGIndex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGIndex;
_currn->_desc1 = (_TSPNumber) MkNumber (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGIndex: root of subtree no. 1 can not be made a Number node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGIndex;
return ( (NODEPTR) _currn);
}/* MkrPTGIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGTypeNode (POSITION *_coordref)
#else
NODEPTR MkrPTGTypeNode (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrPTGTypeNode _currn;
#ifdef __cplusplus
_currn = new _TPrPTGTypeNode;
#else
_currn = (_TPPrPTGTypeNode) TreeNodeAlloc (sizeof (struct _TPrPTGTypeNode));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGTypeNode;
_SETCOORD(_currn)
_TERMACT_rPTGTypeNode;
return ( (NODEPTR) _currn);
}/* MkrPTGTypeNode */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGTypeEli (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGTypeEli (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGTypeEli _currn;
#ifdef __cplusplus
_currn = new _TPrPTGTypeEli;
#else
_currn = (_TPPrPTGTypeEli) TreeNodeAlloc (sizeof (struct _TPrPTGTypeEli));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGTypeEli;
_currn->_desc1 = (_TSPEliType_Reference) MkEliType_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGTypeEli: root of subtree no. 1 can not be made a EliType_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGTypeEli;
return ( (NODEPTR) _currn);
}/* MkrPTGTypeEli */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGTypeGeneral (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGTypeGeneral (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGTypeGeneral _currn;
#ifdef __cplusplus
_currn = new _TPrPTGTypeGeneral;
#else
_currn = (_TPPrPTGTypeGeneral) TreeNodeAlloc (sizeof (struct _TPrPTGTypeGeneral));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGTypeGeneral;
_currn->_desc1 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGTypeGeneral: root of subtree no. 1 can not be made a Simple_Typing node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGTypeGeneral;
return ( (NODEPTR) _currn);
}/* MkrPTGTypeGeneral */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrEliTypeInt (POSITION *_coordref)
#else
NODEPTR MkrEliTypeInt (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrEliTypeInt _currn;
#ifdef __cplusplus
_currn = new _TPrEliTypeInt;
#else
_currn = (_TPPrEliTypeInt) TreeNodeAlloc (sizeof (struct _TPrEliTypeInt));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErEliTypeInt;
_SETCOORD(_currn)
_TERMACT_rEliTypeInt;
return ( (NODEPTR) _currn);
}/* MkrEliTypeInt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrEliTypeString (POSITION *_coordref)
#else
NODEPTR MkrEliTypeString (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrEliTypeString _currn;
#ifdef __cplusplus
_currn = new _TPrEliTypeString;
#else
_currn = (_TPPrEliTypeString) TreeNodeAlloc (sizeof (struct _TPrEliTypeString));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErEliTypeString;
_SETCOORD(_currn)
_TERMACT_rEliTypeString;
return ( (NODEPTR) _currn);
}/* MkrEliTypeString */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrEliTypePointer (POSITION *_coordref)
#else
NODEPTR MkrEliTypePointer (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrEliTypePointer _currn;
#ifdef __cplusplus
_currn = new _TPrEliTypePointer;
#else
_currn = (_TPPrEliTypePointer) TreeNodeAlloc (sizeof (struct _TPrEliTypePointer));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErEliTypePointer;
_SETCOORD(_currn)
_TERMACT_rEliTypePointer;
return ( (NODEPTR) _currn);
}/* MkrEliTypePointer */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrEliTypeChar (POSITION *_coordref)
#else
NODEPTR MkrEliTypeChar (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrEliTypeChar _currn;
#ifdef __cplusplus
_currn = new _TPrEliTypeChar;
#else
_currn = (_TPPrEliTypeChar) TreeNodeAlloc (sizeof (struct _TPrEliTypeChar));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErEliTypeChar;
_SETCOORD(_currn)
_TERMACT_rEliTypeChar;
return ( (NODEPTR) _currn);
}/* MkrEliTypeChar */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrEliTypeDouble (POSITION *_coordref)
#else
NODEPTR MkrEliTypeDouble (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrEliTypeDouble _currn;
#ifdef __cplusplus
_currn = new _TPrEliTypeDouble;
#else
_currn = (_TPPrEliTypeDouble) TreeNodeAlloc (sizeof (struct _TPrEliTypeDouble));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErEliTypeDouble;
_SETCOORD(_currn)
_TERMACT_rEliTypeDouble;
return ( (NODEPTR) _currn);
}/* MkrEliTypeDouble */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrEliTypeFloat (POSITION *_coordref)
#else
NODEPTR MkrEliTypeFloat (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrEliTypeFloat _currn;
#ifdef __cplusplus
_currn = new _TPrEliTypeFloat;
#else
_currn = (_TPPrEliTypeFloat) TreeNodeAlloc (sizeof (struct _TPrEliTypeFloat));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErEliTypeFloat;
_SETCOORD(_currn)
_TERMACT_rEliTypeFloat;
return ( (NODEPTR) _currn);
}/* MkrEliTypeFloat */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrEliTypeLong (POSITION *_coordref)
#else
NODEPTR MkrEliTypeLong (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrEliTypeLong _currn;
#ifdef __cplusplus
_currn = new _TPrEliTypeLong;
#else
_currn = (_TPPrEliTypeLong) TreeNodeAlloc (sizeof (struct _TPrEliTypeLong));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErEliTypeLong;
_SETCOORD(_currn)
_TERMACT_rEliTypeLong;
return ( (NODEPTR) _currn);
}/* MkrEliTypeLong */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrEliTypeShort (POSITION *_coordref)
#else
NODEPTR MkrEliTypeShort (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrEliTypeShort _currn;
#ifdef __cplusplus
_currn = new _TPrEliTypeShort;
#else
_currn = (_TPPrEliTypeShort) TreeNodeAlloc (sizeof (struct _TPrEliTypeShort));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErEliTypeShort;
_SETCOORD(_currn)
_TERMACT_rEliTypeShort;
return ( (NODEPTR) _currn);
}/* MkrEliTypeShort */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypeRefSimple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypeRefSimple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypeRefSimple _currn;
#ifdef __cplusplus
_currn = new _TPrTypeRefSimple;
#else
_currn = (_TPPrTypeRefSimple) TreeNodeAlloc (sizeof (struct _TPrTypeRefSimple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypeRefSimple;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeRefSimple: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypeRefSimple;
return ( (NODEPTR) _currn);
}/* MkrTypeRefSimple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGCall (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrPTGCall (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrPTGCall _currn;
#ifdef __cplusplus
_currn = new _TPrPTGCall;
#else
_currn = (_TPPrPTGCall) TreeNodeAlloc (sizeof (struct _TPrPTGCall));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGCall;
_currn->_desc1 = (_TSPPTG_FunctionReference) MkPTG_FunctionReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGCall: root of subtree no. 1 can not be made a PTG_FunctionReference node ", 0, _coordref);
_currn->_desc2 = (_TSPPTG_CallParams) MkPTG_CallParams (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGCall: root of subtree no. 2 can not be made a PTG_CallParams node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rPTGCall;
return ( (NODEPTR) _currn);
}/* MkrPTGCall */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGFnRefIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGFnRefIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGFnRefIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrPTGFnRefIdentifier;
#else
_currn = (_TPPrPTGFnRefIdentifier) TreeNodeAlloc (sizeof (struct _TPrPTGFnRefIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGFnRefIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGFnRefIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGFnRefIdentifier;
return ( (NODEPTR) _currn);
}/* MkrPTGFnRefIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGFnRefTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGFnRefTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGFnRefTypename _currn;
#ifdef __cplusplus
_currn = new _TPrPTGFnRefTypename;
#else
_currn = (_TPPrPTGFnRefTypename) TreeNodeAlloc (sizeof (struct _TPrPTGFnRefTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGFnRefTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGFnRefTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGFnRefTypename;
return ( (NODEPTR) _currn);
}/* MkrPTGFnRefTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGCallParamList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrPTGCallParamList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrPTGCallParamList2 _currn;
#ifdef __cplusplus
_currn = new _TPrPTGCallParamList2;
#else
_currn = (_TPPrPTGCallParamList2) TreeNodeAlloc (sizeof (struct _TPrPTGCallParamList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGCallParamList2;
_currn->_desc1 = (_TSPPTG_CallParams) MkPTG_CallParams (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGCallParamList2: root of subtree no. 1 can not be made a PTG_CallParams node ", 0, _coordref);
_currn->_desc2 = (_TSPPTG_CallParam) MkPTG_CallParam (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGCallParamList2: root of subtree no. 2 can not be made a PTG_CallParam node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rPTGCallParamList2;
return ( (NODEPTR) _currn);
}/* MkrPTGCallParamList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGCallParamEmpty (POSITION *_coordref)
#else
NODEPTR MkrPTGCallParamEmpty (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrPTGCallParamEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrPTGCallParamEmpty;
#else
_currn = (_TPPrPTGCallParamEmpty) TreeNodeAlloc (sizeof (struct _TPrPTGCallParamEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGCallParamEmpty;
_SETCOORD(_currn)
_TERMACT_rPTGCallParamEmpty;
return ( (NODEPTR) _currn);
}/* MkrPTGCallParamEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGCallInsertion (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGCallInsertion (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGCallInsertion _currn;
#ifdef __cplusplus
_currn = new _TPrPTGCallInsertion;
#else
_currn = (_TPPrPTGCallInsertion) TreeNodeAlloc (sizeof (struct _TPrPTGCallInsertion));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGCallInsertion;
_currn->_desc1 = (_TSPPTG_Insertion) MkPTG_Insertion (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGCallInsertion: root of subtree no. 1 can not be made a PTG_Insertion node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGCallInsertion;
return ( (NODEPTR) _currn);
}/* MkrPTGCallInsertion */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPTGOptionalPatterns (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPTGOptionalPatterns (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPTGOptionalPatterns _currn;
#ifdef __cplusplus
_currn = new _TPrPTGOptionalPatterns;
#else
_currn = (_TPPrPTGOptionalPatterns) TreeNodeAlloc (sizeof (struct _TPrPTGOptionalPatterns));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPTGOptionalPatterns;
_currn->_desc1 = (_TSPPTG_Patterns) MkPTG_Patterns (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPTGOptionalPatterns: root of subtree no. 1 can not be made a PTG_Patterns node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPTGOptionalPatterns;
return ( (NODEPTR) _currn);
}/* MkrPTGOptionalPatterns */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclPDL (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclPDL (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclPDL _currn;
#ifdef __cplusplus
_currn = new _TPrDeclPDL;
#else
_currn = (_TPPrDeclPDL) TreeNodeAlloc (sizeof (struct _TPrDeclPDL));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclPDL;
_currn->_desc1 = (_TSPPDL_Decl) MkPDL_Decl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclPDL: root of subtree no. 1 can not be made a PDL_Decl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclPDL;
return ( (NODEPTR) _currn);
}/* MkrDeclPDL */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLDecl (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPDLDecl (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPDLDecl _currn;
#ifdef __cplusplus
_currn = new _TPrPDLDecl;
#else
_currn = (_TPPrPDLDecl) TreeNodeAlloc (sizeof (struct _TPrPDLDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLDecl;
_currn->_desc1 = (_TSPPDL_Definitions) MkPDL_Definitions (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDecl: root of subtree no. 1 can not be made a PDL_Definitions node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPDLDecl;
return ( (NODEPTR) _currn);
}/* MkrPDLDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLDefinitionList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrPDLDefinitionList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrPDLDefinitionList2 _currn;
#ifdef __cplusplus
_currn = new _TPrPDLDefinitionList2;
#else
_currn = (_TPPrPDLDefinitionList2) TreeNodeAlloc (sizeof (struct _TPrPDLDefinitionList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLDefinitionList2;
_currn->_desc1 = (_TSPPDL_Definitions) MkPDL_Definitions (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefinitionList2: root of subtree no. 1 can not be made a PDL_Definitions node ", 0, _coordref);
_currn->_desc2 = (_TSPPDL_Definition) MkPDL_Definition (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefinitionList2: root of subtree no. 2 can not be made a PDL_Definition node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rPDLDefinitionList2;
return ( (NODEPTR) _currn);
}/* MkrPDLDefinitionList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLDefinitionListEmpty (POSITION *_coordref)
#else
NODEPTR MkrPDLDefinitionListEmpty (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrPDLDefinitionListEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrPDLDefinitionListEmpty;
#else
_currn = (_TPPrPDLDefinitionListEmpty) TreeNodeAlloc (sizeof (struct _TPrPDLDefinitionListEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLDefinitionListEmpty;
_SETCOORD(_currn)
_TERMACT_rPDLDefinitionListEmpty;
return ( (NODEPTR) _currn);
}/* MkrPDLDefinitionListEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLLoad (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPDLLoad (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPDLLoad _currn;
#ifdef __cplusplus
_currn = new _TPrPDLLoad;
#else
_currn = (_TPPrPDLLoad) TreeNodeAlloc (sizeof (struct _TPrPDLLoad));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLLoad;
_currn->_desc1 = (_TSPLoad_Decl) MkLoad_Decl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLLoad: root of subtree no. 1 can not be made a Load_Decl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPDLLoad;
return ( (NODEPTR) _currn);
}/* MkrPDLLoad */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLDefInclude (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPDLDefInclude (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPDLDefInclude _currn;
#ifdef __cplusplus
_currn = new _TPrPDLDefInclude;
#else
_currn = (_TPPrPDLDefInclude) TreeNodeAlloc (sizeof (struct _TPrPDLDefInclude));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLDefInclude;
_currn->_desc1 = (_TSPPDL_IncludeDecl) MkPDL_IncludeDecl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefInclude: root of subtree no. 1 can not be made a PDL_IncludeDecl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPDLDefInclude;
return ( (NODEPTR) _currn);
}/* MkrPDLDefInclude */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLInclude (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPDLInclude (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPDLInclude _currn;
#ifdef __cplusplus
_currn = new _TPrPDLInclude;
#else
_currn = (_TPPrPDLInclude) TreeNodeAlloc (sizeof (struct _TPrPDLInclude));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLInclude;
_currn->_desc1 = (_TSPString) MkString (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLInclude: root of subtree no. 1 can not be made a String node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPDLInclude;
return ( (NODEPTR) _currn);
}/* MkrPDLInclude */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLRealDef (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPDLRealDef (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPDLRealDef _currn;
#ifdef __cplusplus
_currn = new _TPrPDLRealDef;
#else
_currn = (_TPPrPDLRealDef) TreeNodeAlloc (sizeof (struct _TPrPDLRealDef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLRealDef;
_currn->_desc1 = (_TSPPDL_PropertyDefinition) MkPDL_PropertyDefinition (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLRealDef: root of subtree no. 1 can not be made a PDL_PropertyDefinition node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPDLRealDef;
return ( (NODEPTR) _currn);
}/* MkrPDLRealDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLDefProperties (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrPDLDefProperties (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrPDLDefProperties _currn;
#ifdef __cplusplus
_currn = new _TPrPDLDefProperties;
#else
_currn = (_TPPrPDLDefProperties) TreeNodeAlloc (sizeof (struct _TPrPDLDefProperties));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLDefProperties;
_currn->_desc1 = (_TSPPDL_PropertyNames) MkPDL_PropertyNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefProperties: root of subtree no. 1 can not be made a PDL_PropertyNames node ", 0, _coordref);
_currn->_desc2 = (_TSPPDL_Type) MkPDL_Type (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefProperties: root of subtree no. 2 can not be made a PDL_Type node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rPDLDefProperties;
return ( (NODEPTR) _currn);
}/* MkrPDLDefProperties */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLDefNameList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrPDLDefNameList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrPDLDefNameList2 _currn;
#ifdef __cplusplus
_currn = new _TPrPDLDefNameList2;
#else
_currn = (_TPPrPDLDefNameList2) TreeNodeAlloc (sizeof (struct _TPrPDLDefNameList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLDefNameList2;
_currn->_desc1 = (_TSPPDL_PropertyNames) MkPDL_PropertyNames (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefNameList2: root of subtree no. 1 can not be made a PDL_PropertyNames node ", 0, _coordref);
_currn->_desc2 = (_TSPPDL_PropertyDefId) MkPDL_PropertyDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefNameList2: root of subtree no. 2 can not be made a PDL_PropertyDefId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rPDLDefNameList2;
return ( (NODEPTR) _currn);
}/* MkrPDLDefNameList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLDefNameList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPDLDefNameList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPDLDefNameList1 _currn;
#ifdef __cplusplus
_currn = new _TPrPDLDefNameList1;
#else
_currn = (_TPPrPDLDefNameList1) TreeNodeAlloc (sizeof (struct _TPrPDLDefNameList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLDefNameList1;
_currn->_desc1 = (_TSPPDL_PropertyDefId) MkPDL_PropertyDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefNameList1: root of subtree no. 1 can not be made a PDL_PropertyDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPDLDefNameList1;
return ( (NODEPTR) _currn);
}/* MkrPDLDefNameList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLDefTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPDLDefTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPDLDefTypename _currn;
#ifdef __cplusplus
_currn = new _TPrPDLDefTypename;
#else
_currn = (_TPPrPDLDefTypename) TreeNodeAlloc (sizeof (struct _TPrPDLDefTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLDefTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPDLDefTypename;
return ( (NODEPTR) _currn);
}/* MkrPDLDefTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLDefIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPDLDefIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPDLDefIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrPDLDefIdentifier;
#else
_currn = (_TPPrPDLDefIdentifier) TreeNodeAlloc (sizeof (struct _TPrPDLDefIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLDefIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLDefIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPDLDefIdentifier;
return ( (NODEPTR) _currn);
}/* MkrPDLDefIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrPDLReferenceType (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrPDLReferenceType (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrPDLReferenceType _currn;
#ifdef __cplusplus
_currn = new _TPrPDLReferenceType;
#else
_currn = (_TPPrPDLReferenceType) TreeNodeAlloc (sizeof (struct _TPrPDLReferenceType));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErPDLReferenceType;
_currn->_desc1 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rPDLReferenceType: root of subtree no. 1 can not be made a Simple_Typing node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rPDLReferenceType;
return ( (NODEPTR) _currn);
}/* MkrPDLReferenceType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclCopySource (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclCopySource (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclCopySource _currn;
#ifdef __cplusplus
_currn = new _TPrDeclCopySource;
#else
_currn = (_TPPrDeclCopySource) TreeNodeAlloc (sizeof (struct _TPrDeclCopySource));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclCopySource;
_currn->_desc1 = (_TSPCopy_Source) MkCopy_Source (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclCopySource: root of subtree no. 1 can not be made a Copy_Source node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclCopySource;
return ( (NODEPTR) _currn);
}/* MkrDeclCopySource */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclCopyHead (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclCopyHead (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclCopyHead _currn;
#ifdef __cplusplus
_currn = new _TPrDeclCopyHead;
#else
_currn = (_TPPrDeclCopyHead) TreeNodeAlloc (sizeof (struct _TPrDeclCopyHead));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclCopyHead;
_currn->_desc1 = (_TSPCopy_Head) MkCopy_Head (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclCopyHead: root of subtree no. 1 can not be made a Copy_Head node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclCopyHead;
return ( (NODEPTR) _currn);
}/* MkrDeclCopyHead */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclCopyLido (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclCopyLido (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclCopyLido _currn;
#ifdef __cplusplus
_currn = new _TPrDeclCopyLido;
#else
_currn = (_TPPrDeclCopyLido) TreeNodeAlloc (sizeof (struct _TPrDeclCopyLido));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclCopyLido;
_currn->_desc1 = (_TSPCopy_Lido) MkCopy_Lido (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclCopyLido: root of subtree no. 1 can not be made a Copy_Lido node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclCopyLido;
return ( (NODEPTR) _currn);
}/* MkrDeclCopyLido */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrCopySourcePre (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrCopySourcePre (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrCopySourcePre _currn;
#ifdef __cplusplus
_currn = new _TPrCopySourcePre;
#else
_currn = (_TPPrCopySourcePre) TreeNodeAlloc (sizeof (struct _TPrCopySourcePre));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErCopySourcePre;
_currn->_desc1 = (_TSPCText) MkCText (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rCopySourcePre: root of subtree no. 1 can not be made a CText node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rCopySourcePre;
return ( (NODEPTR) _currn);
}/* MkrCopySourcePre */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrCopySourcePost (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrCopySourcePost (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrCopySourcePost _currn;
#ifdef __cplusplus
_currn = new _TPrCopySourcePost;
#else
_currn = (_TPPrCopySourcePost) TreeNodeAlloc (sizeof (struct _TPrCopySourcePost));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErCopySourcePost;
_currn->_desc1 = (_TSPCText) MkCText (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rCopySourcePost: root of subtree no. 1 can not be made a CText node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rCopySourcePost;
return ( (NODEPTR) _currn);
}/* MkrCopySourcePost */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrCopyHeadPre (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrCopyHeadPre (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrCopyHeadPre _currn;
#ifdef __cplusplus
_currn = new _TPrCopyHeadPre;
#else
_currn = (_TPPrCopyHeadPre) TreeNodeAlloc (sizeof (struct _TPrCopyHeadPre));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErCopyHeadPre;
_currn->_desc1 = (_TSPCText) MkCText (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rCopyHeadPre: root of subtree no. 1 can not be made a CText node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rCopyHeadPre;
return ( (NODEPTR) _currn);
}/* MkrCopyHeadPre */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrCopyHeadPost (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrCopyHeadPost (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrCopyHeadPost _currn;
#ifdef __cplusplus
_currn = new _TPrCopyHeadPost;
#else
_currn = (_TPPrCopyHeadPost) TreeNodeAlloc (sizeof (struct _TPrCopyHeadPost));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErCopyHeadPost;
_currn->_desc1 = (_TSPCText) MkCText (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rCopyHeadPost: root of subtree no. 1 can not be made a CText node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rCopyHeadPost;
return ( (NODEPTR) _currn);
}/* MkrCopyHeadPost */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrCopyLido (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrCopyLido (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrCopyLido _currn;
#ifdef __cplusplus
_currn = new _TPrCopyLido;
#else
_currn = (_TPPrCopyLido) TreeNodeAlloc (sizeof (struct _TPrCopyLido));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErCopyLido;
_currn->_desc1 = (_TSPCText) MkCText (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rCopyLido: root of subtree no. 1 can not be made a CText node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rCopyLido;
return ( (NODEPTR) _currn);
}/* MkrCopyLido */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclTypedef (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclTypedef (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclTypedef _currn;
#ifdef __cplusplus
_currn = new _TPrDeclTypedef;
#else
_currn = (_TPPrDeclTypedef) TreeNodeAlloc (sizeof (struct _TPrDeclTypedef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclTypedef;
_currn->_desc1 = (_TSPNewType_Decl) MkNewType_Decl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclTypedef: root of subtree no. 1 can not be made a NewType_Decl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclTypedef;
return ( (NODEPTR) _currn);
}/* MkrDeclTypedef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypedefDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrTypedefDecl (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrTypedefDecl _currn;
#ifdef __cplusplus
_currn = new _TPrTypedefDecl;
#else
_currn = (_TPPrTypedefDecl) TreeNodeAlloc (sizeof (struct _TPrTypedefDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypedefDecl;
_currn->_desc1 = (_TSPType_DefId) MkType_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypedefDecl: root of subtree no. 1 can not be made a Type_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypedefDecl: root of subtree no. 2 can not be made a Simple_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rTypedefDecl;
return ( (NODEPTR) _currn);
}/* MkrTypedefDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypingReference (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypingReference (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypingReference _currn;
#ifdef __cplusplus
_currn = new _TPrTypingReference;
#else
_currn = (_TPPrTypingReference) TreeNodeAlloc (sizeof (struct _TPrTypingReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypingReference;
_currn->_desc1 = (_TSPType_Reference) MkType_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypingReference: root of subtree no. 1 can not be made a Type_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypingReference;
return ( (NODEPTR) _currn);
}/* MkrTypingReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypingList (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypingList (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypingList _currn;
#ifdef __cplusplus
_currn = new _TPrTypingList;
#else
_currn = (_TPPrTypingList) TreeNodeAlloc (sizeof (struct _TPrTypingList));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypingList;
_currn->_desc1 = (_TSPType_Listof) MkType_Listof (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypingList: root of subtree no. 1 can not be made a Type_Listof node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypingList;
return ( (NODEPTR) _currn);
}/* MkrTypingList */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypingBasic (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypingBasic (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypingBasic _currn;
#ifdef __cplusplus
_currn = new _TPrTypingBasic;
#else
_currn = (_TPPrTypingBasic) TreeNodeAlloc (sizeof (struct _TPrTypingBasic));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypingBasic;
_currn->_desc1 = (_TSPEliType_Reference) MkEliType_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypingBasic: root of subtree no. 1 can not be made a EliType_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypingBasic;
return ( (NODEPTR) _currn);
}/* MkrTypingBasic */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypingTuple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypingTuple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypingTuple _currn;
#ifdef __cplusplus
_currn = new _TPrTypingTuple;
#else
_currn = (_TPPrTypingTuple) TreeNodeAlloc (sizeof (struct _TPrTypingTuple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypingTuple;
_currn->_desc1 = (_TSPType_TupleOf) MkType_TupleOf (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypingTuple: root of subtree no. 1 can not be made a Type_TupleOf node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypingTuple;
return ( (NODEPTR) _currn);
}/* MkrTypingTuple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypingMap (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypingMap (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypingMap _currn;
#ifdef __cplusplus
_currn = new _TPrTypingMap;
#else
_currn = (_TPPrTypingMap) TreeNodeAlloc (sizeof (struct _TPrTypingMap));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypingMap;
_currn->_desc1 = (_TSPType_MapOf) MkType_MapOf (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypingMap: root of subtree no. 1 can not be made a Type_MapOf node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypingMap;
return ( (NODEPTR) _currn);
}/* MkrTypingMap */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTupleType (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrTupleType (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrTupleType _currn;
#ifdef __cplusplus
_currn = new _TPrTupleType;
#else
_currn = (_TPPrTupleType) TreeNodeAlloc (sizeof (struct _TPrTupleType));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTupleType;
_currn->_desc1 = (_TSPType_Construction) MkType_Construction (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTupleType: root of subtree no. 1 can not be made a Type_Construction node ", 0, _coordref);
_currn->_desc2 = (_TSPType_Constructions) MkType_Constructions (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rTupleType: root of subtree no. 2 can not be made a Type_Constructions node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rTupleType;
return ( (NODEPTR) _currn);
}/* MkrTupleType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypeConList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrTypeConList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrTypeConList2 _currn;
#ifdef __cplusplus
_currn = new _TPrTypeConList2;
#else
_currn = (_TPPrTypeConList2) TreeNodeAlloc (sizeof (struct _TPrTypeConList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypeConList2;
_currn->_desc1 = (_TSPType_Constructions) MkType_Constructions (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeConList2: root of subtree no. 1 can not be made a Type_Constructions node ", 0, _coordref);
_currn->_desc2 = (_TSPType_Construction) MkType_Construction (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeConList2: root of subtree no. 2 can not be made a Type_Construction node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rTypeConList2;
return ( (NODEPTR) _currn);
}/* MkrTypeConList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypeConList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypeConList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypeConList1 _currn;
#ifdef __cplusplus
_currn = new _TPrTypeConList1;
#else
_currn = (_TPPrTypeConList1) TreeNodeAlloc (sizeof (struct _TPrTypeConList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypeConList1;
_currn->_desc1 = (_TSPType_Construction) MkType_Construction (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeConList1: root of subtree no. 1 can not be made a Type_Construction node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypeConList1;
return ( (NODEPTR) _currn);
}/* MkrTypeConList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypeListConstruction (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypeListConstruction (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypeListConstruction _currn;
#ifdef __cplusplus
_currn = new _TPrTypeListConstruction;
#else
_currn = (_TPPrTypeListConstruction) TreeNodeAlloc (sizeof (struct _TPrTypeListConstruction));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypeListConstruction;
_currn->_desc1 = (_TSPType_Construction) MkType_Construction (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeListConstruction: root of subtree no. 1 can not be made a Type_Construction node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypeListConstruction;
return ( (NODEPTR) _currn);
}/* MkrTypeListConstruction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstructSimple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrConstructSimple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrConstructSimple _currn;
#ifdef __cplusplus
_currn = new _TPrConstructSimple;
#else
_currn = (_TPPrConstructSimple) TreeNodeAlloc (sizeof (struct _TPrConstructSimple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstructSimple;
_currn->_desc1 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstructSimple: root of subtree no. 1 can not be made a Simple_Typing node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rConstructSimple;
return ( (NODEPTR) _currn);
}/* MkrConstructSimple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypeMapOf (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrTypeMapOf (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrTypeMapOf _currn;
#ifdef __cplusplus
_currn = new _TPrTypeMapOf;
#else
_currn = (_TPPrTypeMapOf) TreeNodeAlloc (sizeof (struct _TPrTypeMapOf));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypeMapOf;
_currn->_desc1 = (_TSPType_Construction) MkType_Construction (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeMapOf: root of subtree no. 1 can not be made a Type_Construction node ", 0, _coordref);
_currn->_desc2 = (_TSPType_Construction) MkType_Construction (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeMapOf: root of subtree no. 2 can not be made a Type_Construction node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rTypeMapOf;
return ( (NODEPTR) _currn);
}/* MkrTypeMapOf */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclData (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclData (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclData _currn;
#ifdef __cplusplus
_currn = new _TPrDeclData;
#else
_currn = (_TPPrDeclData) TreeNodeAlloc (sizeof (struct _TPrDeclData));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclData;
_currn->_desc1 = (_TSPData_Decl) MkData_Decl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclData: root of subtree no. 1 can not be made a Data_Decl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclData;
return ( (NODEPTR) _currn);
}/* MkrDeclData */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDataDecl (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDataDecl _currn;
#ifdef __cplusplus
_currn = new _TPrDataDecl;
#else
_currn = (_TPPrDataDecl) TreeNodeAlloc (sizeof (struct _TPrDataDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataDecl;
_currn->_desc1 = (_TSPData_DefId) MkData_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataDecl: root of subtree no. 1 can not be made a Data_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPData_Constructors) MkData_Constructors (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataDecl: root of subtree no. 2 can not be made a Data_Constructors node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDataDecl;
return ( (NODEPTR) _currn);
}/* MkrDataDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataName (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataName (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataName _currn;
#ifdef __cplusplus
_currn = new _TPrDataName;
#else
_currn = (_TPPrDataName) TreeNodeAlloc (sizeof (struct _TPrDataName));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataName;
_currn->_desc1 = (_TSPData_DefId) MkData_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataName: root of subtree no. 1 can not be made a Data_DefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataName;
return ( (NODEPTR) _currn);
}/* MkrDataName */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDataConstructorList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDataConstructorList2 _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorList2;
#else
_currn = (_TPPrDataConstructorList2) TreeNodeAlloc (sizeof (struct _TPrDataConstructorList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorList2;
_currn->_desc1 = (_TSPData_Constructors) MkData_Constructors (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorList2: root of subtree no. 1 can not be made a Data_Constructors node ", 0, _coordref);
_currn->_desc2 = (_TSPData_Constructor) MkData_Constructor (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorList2: root of subtree no. 2 can not be made a Data_Constructor node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDataConstructorList2;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataConstructorList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataConstructorList1 _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorList1;
#else
_currn = (_TPPrDataConstructorList1) TreeNodeAlloc (sizeof (struct _TPrDataConstructorList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorList1;
_currn->_desc1 = (_TSPData_Constructor) MkData_Constructor (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorList1: root of subtree no. 1 can not be made a Data_Constructor node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataConstructorList1;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorIsSimple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataConstructorIsSimple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataConstructorIsSimple _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorIsSimple;
#else
_currn = (_TPPrDataConstructorIsSimple) TreeNodeAlloc (sizeof (struct _TPrDataConstructorIsSimple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorIsSimple;
_currn->_desc1 = (_TSPData_SimpleConstructor) MkData_SimpleConstructor (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorIsSimple: root of subtree no. 1 can not be made a Data_SimpleConstructor node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataConstructorIsSimple;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorIsSimple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorIsComplex (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataConstructorIsComplex (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataConstructorIsComplex _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorIsComplex;
#else
_currn = (_TPPrDataConstructorIsComplex) TreeNodeAlloc (sizeof (struct _TPrDataConstructorIsComplex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorIsComplex;
_currn->_desc1 = (_TSPData_ComplexConstructor) MkData_ComplexConstructor (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorIsComplex: root of subtree no. 1 can not be made a Data_ComplexConstructor node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataConstructorIsComplex;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorIsComplex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorComplex (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDataConstructorComplex (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDataConstructorComplex _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorComplex;
#else
_currn = (_TPPrDataConstructorComplex) TreeNodeAlloc (sizeof (struct _TPrDataConstructorComplex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorComplex;
_currn->_desc1 = (_TSPData_ConstructorDefId) MkData_ConstructorDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorComplex: root of subtree no. 1 can not be made a Data_ConstructorDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPData_ConstructorArguments) MkData_ConstructorArguments (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorComplex: root of subtree no. 2 can not be made a Data_ConstructorArguments node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDataConstructorComplex;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorComplex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorArgumentList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDataConstructorArgumentList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDataConstructorArgumentList2 _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorArgumentList2;
#else
_currn = (_TPPrDataConstructorArgumentList2) TreeNodeAlloc (sizeof (struct _TPrDataConstructorArgumentList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorArgumentList2;
_currn->_desc1 = (_TSPData_ConstructorArguments) MkData_ConstructorArguments (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorArgumentList2: root of subtree no. 1 can not be made a Data_ConstructorArguments node ", 0, _coordref);
_currn->_desc2 = (_TSPData_ConstructorArgument) MkData_ConstructorArgument (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorArgumentList2: root of subtree no. 2 can not be made a Data_ConstructorArgument node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDataConstructorArgumentList2;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorArgumentList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorArgumentList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataConstructorArgumentList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataConstructorArgumentList1 _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorArgumentList1;
#else
_currn = (_TPPrDataConstructorArgumentList1) TreeNodeAlloc (sizeof (struct _TPrDataConstructorArgumentList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorArgumentList1;
_currn->_desc1 = (_TSPData_ConstructorArgument) MkData_ConstructorArgument (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorArgumentList1: root of subtree no. 1 can not be made a Data_ConstructorArgument node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataConstructorArgumentList1;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorArgumentList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorArgument (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataConstructorArgument (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataConstructorArgument _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorArgument;
#else
_currn = (_TPPrDataConstructorArgument) TreeNodeAlloc (sizeof (struct _TPrDataConstructorArgument));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorArgument;
_currn->_desc1 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorArgument: root of subtree no. 1 can not be made a Simple_Typing node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataConstructorArgument;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorSimple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataConstructorSimple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataConstructorSimple _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorSimple;
#else
_currn = (_TPPrDataConstructorSimple) TreeNodeAlloc (sizeof (struct _TPrDataConstructorSimple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorSimple;
_currn->_desc1 = (_TSPData_ConstructorDefId) MkData_ConstructorDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorSimple: root of subtree no. 1 can not be made a Data_ConstructorDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataConstructorSimple;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorSimple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataConstructorDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataConstructorDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataConstructorDefId _currn;
#ifdef __cplusplus
_currn = new _TPrDataConstructorDefId;
#else
_currn = (_TPPrDataConstructorDefId) TreeNodeAlloc (sizeof (struct _TPrDataConstructorDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataConstructorDefId;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataConstructorDefId: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataConstructorDefId;
return ( (NODEPTR) _currn);
}/* MkrDataConstructorDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataDefId _currn;
#ifdef __cplusplus
_currn = new _TPrDataDefId;
#else
_currn = (_TPPrDataDefId) TreeNodeAlloc (sizeof (struct _TPrDataDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataDefId;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataDefId: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataDefId;
return ( (NODEPTR) _currn);
}/* MkrDataDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypeDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypeDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypeDefId _currn;
#ifdef __cplusplus
_currn = new _TPrTypeDefId;
#else
_currn = (_TPPrTypeDefId) TreeNodeAlloc (sizeof (struct _TPrTypeDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypeDefId;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeDefId: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypeDefId;
return ( (NODEPTR) _currn);
}/* MkrTypeDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataRecordDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDataRecordDecl (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDataRecordDecl _currn;
#ifdef __cplusplus
_currn = new _TPrDataRecordDecl;
#else
_currn = (_TPPrDataRecordDecl) TreeNodeAlloc (sizeof (struct _TPrDataRecordDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataRecordDecl;
_currn->_desc1 = (_TSPData_DefId) MkData_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataRecordDecl: root of subtree no. 1 can not be made a Data_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPData_RecordArguments) MkData_RecordArguments (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataRecordDecl: root of subtree no. 2 can not be made a Data_RecordArguments node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDataRecordDecl;
return ( (NODEPTR) _currn);
}/* MkrDataRecordDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataRecordArgumentList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDataRecordArgumentList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDataRecordArgumentList2 _currn;
#ifdef __cplusplus
_currn = new _TPrDataRecordArgumentList2;
#else
_currn = (_TPPrDataRecordArgumentList2) TreeNodeAlloc (sizeof (struct _TPrDataRecordArgumentList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataRecordArgumentList2;
_currn->_desc1 = (_TSPData_RecordArguments) MkData_RecordArguments (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataRecordArgumentList2: root of subtree no. 1 can not be made a Data_RecordArguments node ", 0, _coordref);
_currn->_desc2 = (_TSPData_RecordArgument) MkData_RecordArgument (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataRecordArgumentList2: root of subtree no. 2 can not be made a Data_RecordArgument node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDataRecordArgumentList2;
return ( (NODEPTR) _currn);
}/* MkrDataRecordArgumentList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataRecordArgumentList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataRecordArgumentList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataRecordArgumentList1 _currn;
#ifdef __cplusplus
_currn = new _TPrDataRecordArgumentList1;
#else
_currn = (_TPPrDataRecordArgumentList1) TreeNodeAlloc (sizeof (struct _TPrDataRecordArgumentList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataRecordArgumentList1;
_currn->_desc1 = (_TSPData_RecordArgument) MkData_RecordArgument (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataRecordArgumentList1: root of subtree no. 1 can not be made a Data_RecordArgument node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataRecordArgumentList1;
return ( (NODEPTR) _currn);
}/* MkrDataRecordArgumentList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataRecordArgument (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDataRecordArgument (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDataRecordArgument _currn;
#ifdef __cplusplus
_currn = new _TPrDataRecordArgument;
#else
_currn = (_TPPrDataRecordArgument) TreeNodeAlloc (sizeof (struct _TPrDataRecordArgument));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataRecordArgument;
_currn->_desc1 = (_TSPData_RecordArgumentDefId) MkData_RecordArgumentDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataRecordArgument: root of subtree no. 1 can not be made a Data_RecordArgumentDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataRecordArgument: root of subtree no. 2 can not be made a Simple_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDataRecordArgument;
return ( (NODEPTR) _currn);
}/* MkrDataRecordArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDataRecordArgumentDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDataRecordArgumentDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDataRecordArgumentDefId _currn;
#ifdef __cplusplus
_currn = new _TPrDataRecordArgumentDefId;
#else
_currn = (_TPPrDataRecordArgumentDefId) TreeNodeAlloc (sizeof (struct _TPrDataRecordArgumentDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDataRecordArgumentDefId;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDataRecordArgumentDefId: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDataRecordArgumentDefId;
return ( (NODEPTR) _currn);
}/* MkrDataRecordArgumentDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclFunctionType (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclFunctionType (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclFunctionType _currn;
#ifdef __cplusplus
_currn = new _TPrDeclFunctionType;
#else
_currn = (_TPPrDeclFunctionType) TreeNodeAlloc (sizeof (struct _TPrDeclFunctionType));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclFunctionType;
_currn->_desc1 = (_TSPFunction_TypeDecl) MkFunction_TypeDecl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclFunctionType: root of subtree no. 1 can not be made a Function_TypeDecl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclFunctionType;
return ( (NODEPTR) _currn);
}/* MkrDeclFunctionType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTypeDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionTypeDecl (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionTypeDecl _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTypeDecl;
#else
_currn = (_TPPrFunctionTypeDecl) TreeNodeAlloc (sizeof (struct _TPrFunctionTypeDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTypeDecl;
_currn->_desc1 = (_TSPFunction_DefId) MkFunction_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTypeDecl: root of subtree no. 1 can not be made a Function_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Typing) MkFunction_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTypeDecl: root of subtree no. 2 can not be made a Function_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionTypeDecl;
return ( (NODEPTR) _currn);
}/* MkrFunctionTypeDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionType (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionType (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionType _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionType;
#else
_currn = (_TPPrFunctionType) TreeNodeAlloc (sizeof (struct _TPrFunctionType));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionType;
_currn->_desc1 = (_TSPFunction_Types) MkFunction_Types (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionType: root of subtree no. 1 can not be made a Function_Types node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_ReturnType) MkFunction_ReturnType (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionType: root of subtree no. 2 can not be made a Function_ReturnType node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionType;
return ( (NODEPTR) _currn);
}/* MkrFunctionType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTypeList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionTypeList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionTypeList2 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTypeList2;
#else
_currn = (_TPPrFunctionTypeList2) TreeNodeAlloc (sizeof (struct _TPrFunctionTypeList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTypeList2;
_currn->_desc1 = (_TSPFunction_Types) MkFunction_Types (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTypeList2: root of subtree no. 1 can not be made a Function_Types node ", 0, _coordref);
_currn->_desc2 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTypeList2: root of subtree no. 2 can not be made a Simple_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionTypeList2;
return ( (NODEPTR) _currn);
}/* MkrFunctionTypeList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTypeList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionTypeList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionTypeList1 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTypeList1;
#else
_currn = (_TPPrFunctionTypeList1) TreeNodeAlloc (sizeof (struct _TPrFunctionTypeList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTypeList1;
_currn->_desc1 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTypeList1: root of subtree no. 1 can not be made a Simple_Typing node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionTypeList1;
return ( (NODEPTR) _currn);
}/* MkrFunctionTypeList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrReturnTypeSimple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrReturnTypeSimple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrReturnTypeSimple _currn;
#ifdef __cplusplus
_currn = new _TPrReturnTypeSimple;
#else
_currn = (_TPPrReturnTypeSimple) TreeNodeAlloc (sizeof (struct _TPrReturnTypeSimple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErReturnTypeSimple;
_currn->_desc1 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rReturnTypeSimple: root of subtree no. 1 can not be made a Simple_Typing node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rReturnTypeSimple;
return ( (NODEPTR) _currn);
}/* MkrReturnTypeSimple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionDefId _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDefId;
#else
_currn = (_TPPrFunctionDefId) TreeNodeAlloc (sizeof (struct _TPrFunctionDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDefId;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDefId: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionDefId;
return ( (NODEPTR) _currn);
}/* MkrFunctionDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypeOfFunction (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypeOfFunction (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypeOfFunction _currn;
#ifdef __cplusplus
_currn = new _TPrTypeOfFunction;
#else
_currn = (_TPPrTypeOfFunction) TreeNodeAlloc (sizeof (struct _TPrTypeOfFunction));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypeOfFunction;
_currn->_desc1 = (_TSPFunction_Typing) MkFunction_Typing (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeOfFunction: root of subtree no. 1 can not be made a Function_Typing node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypeOfFunction;
return ( (NODEPTR) _currn);
}/* MkrTypeOfFunction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrVoidTyping (POSITION *_coordref)
#else
NODEPTR MkrVoidTyping (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrVoidTyping _currn;
#ifdef __cplusplus
_currn = new _TPrVoidTyping;
#else
_currn = (_TPPrVoidTyping) TreeNodeAlloc (sizeof (struct _TPrVoidTyping));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErVoidTyping;
_SETCOORD(_currn)
_TERMACT_rVoidTyping;
return ( (NODEPTR) _currn);
}/* MkrVoidTyping */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypeTemplate (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypeTemplate (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypeTemplate _currn;
#ifdef __cplusplus
_currn = new _TPrTypeTemplate;
#else
_currn = (_TPPrTypeTemplate) TreeNodeAlloc (sizeof (struct _TPrTypeTemplate));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypeTemplate;
_currn->_desc1 = (_TSPType_VariableDefId) MkType_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeTemplate: root of subtree no. 1 can not be made a Type_VariableDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypeTemplate;
return ( (NODEPTR) _currn);
}/* MkrTypeTemplate */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTypeVariableDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTypeVariableDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTypeVariableDefId _currn;
#ifdef __cplusplus
_currn = new _TPrTypeVariableDefId;
#else
_currn = (_TPPrTypeVariableDefId) TreeNodeAlloc (sizeof (struct _TPrTypeVariableDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTypeVariableDefId;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTypeVariableDefId: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTypeVariableDefId;
return ( (NODEPTR) _currn);
}/* MkrTypeVariableDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclFunctionImplementation (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclFunctionImplementation (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclFunctionImplementation _currn;
#ifdef __cplusplus
_currn = new _TPrDeclFunctionImplementation;
#else
_currn = (_TPPrDeclFunctionImplementation) TreeNodeAlloc (sizeof (struct _TPrDeclFunctionImplementation));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclFunctionImplementation;
_currn->_desc1 = (_TSPFunction_Implementation) MkFunction_Implementation (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclFunctionImplementation: root of subtree no. 1 can not be made a Function_Implementation node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclFunctionImplementation;
return ( (NODEPTR) _currn);
}/* MkrDeclFunctionImplementation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionImplementationDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrFunctionImplementationDecl (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrFunctionImplementationDecl _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionImplementationDecl;
#else
_currn = (_TPPrFunctionImplementationDecl) TreeNodeAlloc (sizeof (struct _TPrFunctionImplementationDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionImplementationDecl;
_currn->_desc1 = (_TSPFunction_DefId) MkFunction_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionImplementationDecl: root of subtree no. 1 can not be made a Function_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Patterns) MkFunction_Patterns (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionImplementationDecl: root of subtree no. 2 can not be made a Function_Patterns node ", 0, _coordref);
_currn->_desc3 = (_TSPFunction_RightHandSide) MkFunction_RightHandSide (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionImplementationDecl: root of subtree no. 3 can not be made a Function_RightHandSide node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionImplementationDecl;
return ( (NODEPTR) _currn);
}/* MkrFunctionImplementationDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionPatternList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionPatternList2 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternList2;
#else
_currn = (_TPPrFunctionPatternList2) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternList2;
_currn->_desc1 = (_TSPFunction_Patterns) MkFunction_Patterns (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternList2: root of subtree no. 1 can not be made a Function_Patterns node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Pattern) MkFunction_Pattern (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternList2: root of subtree no. 2 can not be made a Function_Pattern node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionPatternList2;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternListEmpty (POSITION *_coordref)
#else
NODEPTR MkrFunctionPatternListEmpty (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrFunctionPatternListEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternListEmpty;
#else
_currn = (_TPPrFunctionPatternListEmpty) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternListEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternListEmpty;
_SETCOORD(_currn)
_TERMACT_rFunctionPatternListEmpty;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternListEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternVar (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternVar (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternVar _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternVar;
#else
_currn = (_TPPrFunctionPatternVar) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternVar));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternVar;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternVar: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternVar;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternVar */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternIsConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternIsConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternIsConstant _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternIsConstant;
#else
_currn = (_TPPrFunctionPatternIsConstant) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternIsConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternIsConstant;
_currn->_desc1 = (_TSPFunction_ConstantPattern) MkFunction_ConstantPattern (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternIsConstant: root of subtree no. 1 can not be made a Function_ConstantPattern node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternIsConstant;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternIsConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternIsListPattern (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternIsListPattern (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternIsListPattern _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternIsListPattern;
#else
_currn = (_TPPrFunctionPatternIsListPattern) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternIsListPattern));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternIsListPattern;
_currn->_desc1 = (_TSPFunction_ListPattern) MkFunction_ListPattern (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternIsListPattern: root of subtree no. 1 can not be made a Function_ListPattern node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternIsListPattern;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternIsListPattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternIsTuple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternIsTuple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternIsTuple _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternIsTuple;
#else
_currn = (_TPPrFunctionPatternIsTuple) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternIsTuple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternIsTuple;
_currn->_desc1 = (_TSPFunction_TuplePattern) MkFunction_TuplePattern (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternIsTuple: root of subtree no. 1 can not be made a Function_TuplePattern node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternIsTuple;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternIsTuple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternIsIgnored (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternIsIgnored (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternIsIgnored _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternIsIgnored;
#else
_currn = (_TPPrFunctionPatternIsIgnored) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternIsIgnored));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternIsIgnored;
_currn->_desc1 = (_TSPFunction_IgnorePattern) MkFunction_IgnorePattern (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternIsIgnored: root of subtree no. 1 can not be made a Function_IgnorePattern node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternIsIgnored;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternIsIgnored */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternIsSimpleCon (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternIsSimpleCon (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternIsSimpleCon _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternIsSimpleCon;
#else
_currn = (_TPPrFunctionPatternIsSimpleCon) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternIsSimpleCon));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternIsSimpleCon;
_currn->_desc1 = (_TSPFunction_ConstructorPatternSimple) MkFunction_ConstructorPatternSimple (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternIsSimpleCon: root of subtree no. 1 can not be made a Function_ConstructorPatternSimple node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternIsSimpleCon;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternIsSimpleCon */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternIsComplex (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternIsComplex (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternIsComplex _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternIsComplex;
#else
_currn = (_TPPrFunctionPatternIsComplex) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternIsComplex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternIsComplex;
_currn->_desc1 = (_TSPFunction_ConstructorPatternComplex) MkFunction_ConstructorPatternComplex (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternIsComplex: root of subtree no. 1 can not be made a Function_ConstructorPatternComplex node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternIsComplex;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternIsComplex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionConstructorPatternComplex (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionConstructorPatternComplex (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionConstructorPatternComplex _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionConstructorPatternComplex;
#else
_currn = (_TPPrFunctionConstructorPatternComplex) TreeNodeAlloc (sizeof (struct _TPrFunctionConstructorPatternComplex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionConstructorPatternComplex;
_currn->_desc1 = (_TSPType_Reference) MkType_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionConstructorPatternComplex: root of subtree no. 1 can not be made a Type_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_PatternArgBinds) MkFunction_PatternArgBinds (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionConstructorPatternComplex: root of subtree no. 2 can not be made a Function_PatternArgBinds node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionConstructorPatternComplex;
return ( (NODEPTR) _currn);
}/* MkrFunctionConstructorPatternComplex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternArgBindList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionPatternArgBindList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionPatternArgBindList2 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternArgBindList2;
#else
_currn = (_TPPrFunctionPatternArgBindList2) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternArgBindList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternArgBindList2;
_currn->_desc1 = (_TSPFunction_PatternArgBinds) MkFunction_PatternArgBinds (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternArgBindList2: root of subtree no. 1 can not be made a Function_PatternArgBinds node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_PatternArgBind) MkFunction_PatternArgBind (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternArgBindList2: root of subtree no. 2 can not be made a Function_PatternArgBind node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionPatternArgBindList2;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternArgBindList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternArgBindList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternArgBindList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternArgBindList1 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternArgBindList1;
#else
_currn = (_TPPrFunctionPatternArgBindList1) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternArgBindList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternArgBindList1;
_currn->_desc1 = (_TSPFunction_PatternArgBind) MkFunction_PatternArgBind (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternArgBindList1: root of subtree no. 1 can not be made a Function_PatternArgBind node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternArgBindList1;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternArgBindList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternArgBind (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternArgBind (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternArgBind _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternArgBind;
#else
_currn = (_TPPrFunctionPatternArgBind) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternArgBind));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternArgBind;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternArgBind: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternArgBind;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternArgBind */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternConstant _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternConstant;
#else
_currn = (_TPPrFunctionPatternConstant) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternConstant;
_currn->_desc1 = (_TSPConstant) MkConstant (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternConstant: root of subtree no. 1 can not be made a Constant node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternConstant;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionConstructorPatternSimple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionConstructorPatternSimple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionConstructorPatternSimple _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionConstructorPatternSimple;
#else
_currn = (_TPPrFunctionConstructorPatternSimple) TreeNodeAlloc (sizeof (struct _TPrFunctionConstructorPatternSimple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionConstructorPatternSimple;
_currn->_desc1 = (_TSPType_Reference) MkType_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionConstructorPatternSimple: root of subtree no. 1 can not be made a Type_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionConstructorPatternSimple;
return ( (NODEPTR) _currn);
}/* MkrFunctionConstructorPatternSimple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionIngorePattern (POSITION *_coordref)
#else
NODEPTR MkrFunctionIngorePattern (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrFunctionIngorePattern _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionIngorePattern;
#else
_currn = (_TPPrFunctionIngorePattern) TreeNodeAlloc (sizeof (struct _TPrFunctionIngorePattern));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionIngorePattern;
_SETCOORD(_currn)
_TERMACT_rFunctionIngorePattern;
return ( (NODEPTR) _currn);
}/* MkrFunctionIngorePattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTuplePattern (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionTuplePattern (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionTuplePattern _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTuplePattern;
#else
_currn = (_TPPrFunctionTuplePattern) TreeNodeAlloc (sizeof (struct _TPrFunctionTuplePattern));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTuplePattern;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTuplePattern: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_TuplePatternElements) MkFunction_TuplePatternElements (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTuplePattern: root of subtree no. 2 can not be made a Function_TuplePatternElements node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionTuplePattern;
return ( (NODEPTR) _currn);
}/* MkrFunctionTuplePattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTuplePatternElementList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionTuplePatternElementList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionTuplePatternElementList2 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTuplePatternElementList2;
#else
_currn = (_TPPrFunctionTuplePatternElementList2) TreeNodeAlloc (sizeof (struct _TPrFunctionTuplePatternElementList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTuplePatternElementList2;
_currn->_desc1 = (_TSPFunction_TuplePatternElements) MkFunction_TuplePatternElements (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTuplePatternElementList2: root of subtree no. 1 can not be made a Function_TuplePatternElements node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_TuplePatternElement) MkFunction_TuplePatternElement (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTuplePatternElementList2: root of subtree no. 2 can not be made a Function_TuplePatternElement node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionTuplePatternElementList2;
return ( (NODEPTR) _currn);
}/* MkrFunctionTuplePatternElementList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTuplePatternElementList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionTuplePatternElementList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionTuplePatternElementList1 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTuplePatternElementList1;
#else
_currn = (_TPPrFunctionTuplePatternElementList1) TreeNodeAlloc (sizeof (struct _TPrFunctionTuplePatternElementList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTuplePatternElementList1;
_currn->_desc1 = (_TSPFunction_TuplePatternElement) MkFunction_TuplePatternElement (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTuplePatternElementList1: root of subtree no. 1 can not be made a Function_TuplePatternElement node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionTuplePatternElementList1;
return ( (NODEPTR) _currn);
}/* MkrFunctionTuplePatternElementList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternListHead (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPatternListHead (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPatternListHead _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternListHead;
#else
_currn = (_TPPrFunctionPatternListHead) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternListHead));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternListHead;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternListHead: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPatternListHead;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternListHead */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPatternListHeadAndTail (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionPatternListHeadAndTail (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionPatternListHeadAndTail _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPatternListHeadAndTail;
#else
_currn = (_TPPrFunctionPatternListHeadAndTail) TreeNodeAlloc (sizeof (struct _TPrFunctionPatternListHeadAndTail));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPatternListHeadAndTail;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternListHeadAndTail: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPatternListHeadAndTail: root of subtree no. 2 can not be made a Function_VariableDefId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionPatternListHeadAndTail;
return ( (NODEPTR) _currn);
}/* MkrFunctionPatternListHeadAndTail */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTuplePatternElement (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionTuplePatternElement (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionTuplePatternElement _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTuplePatternElement;
#else
_currn = (_TPPrFunctionTuplePatternElement) TreeNodeAlloc (sizeof (struct _TPrFunctionTuplePatternElement));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTuplePatternElement;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTuplePatternElement: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionTuplePatternElement;
return ( (NODEPTR) _currn);
}/* MkrFunctionTuplePatternElement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRhsExpression (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRhsExpression (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRhsExpression _currn;
#ifdef __cplusplus
_currn = new _TPrRhsExpression;
#else
_currn = (_TPPrRhsExpression) TreeNodeAlloc (sizeof (struct _TPrRhsExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRhsExpression;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRhsExpression: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRhsExpression;
return ( (NODEPTR) _currn);
}/* MkrRhsExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionVariableDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionVariableDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionVariableDefId _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionVariableDefId;
#else
_currn = (_TPPrFunctionVariableDefId) TreeNodeAlloc (sizeof (struct _TPrFunctionVariableDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionVariableDefId;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionVariableDefId: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionVariableDefId;
return ( (NODEPTR) _currn);
}/* MkrFunctionVariableDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionChain (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionExpressionChain (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionExpressionChain _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionChain;
#else
_currn = (_TPPrFunctionExpressionChain) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionChain));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionChain;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionChain: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionExpressionChain;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionChain */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionIsBinop (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionExpressionIsBinop (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionExpressionIsBinop _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionIsBinop;
#else
_currn = (_TPPrFunctionExpressionIsBinop) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionIsBinop));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionIsBinop;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionIsBinop: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionExpressionIsBinop;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionIsBinop */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopChain (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionExpressionBinopChain (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionExpressionBinopChain _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopChain;
#else
_currn = (_TPPrFunctionExpressionBinopChain) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopChain));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopChain;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopChain: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionExpressionBinopChain;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopChain */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopOR (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopOR (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopOR _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopOR;
#else
_currn = (_TPPrFunctionExpressionBinopOR) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopOR));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopOR;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopOR: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopOR: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopOR;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopOR */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopAND (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopAND (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopAND _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopAND;
#else
_currn = (_TPPrFunctionExpressionBinopAND) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopAND));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopAND;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopAND: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopAND: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopAND;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopAND */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopEQ (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopEQ (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopEQ _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopEQ;
#else
_currn = (_TPPrFunctionExpressionBinopEQ) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopEQ));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopEQ;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopEQ: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopEQ: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopEQ;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopEQ */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopNE (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopNE (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopNE _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopNE;
#else
_currn = (_TPPrFunctionExpressionBinopNE) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopNE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopNE;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopNE: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopNE: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopNE;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopNE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopLE (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopLE (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopLE _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopLE;
#else
_currn = (_TPPrFunctionExpressionBinopLE) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopLE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopLE;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopLE: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopLE: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopLE;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopLE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopLT (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopLT (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopLT _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopLT;
#else
_currn = (_TPPrFunctionExpressionBinopLT) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopLT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopLT;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopLT: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopLT: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopLT;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopLT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopGT (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopGT (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopGT _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopGT;
#else
_currn = (_TPPrFunctionExpressionBinopGT) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopGT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopGT;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopGT: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopGT: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopGT;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopGT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopGE (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopGE (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopGE _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopGE;
#else
_currn = (_TPPrFunctionExpressionBinopGE) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopGE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopGE;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopGE: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopGE: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopGE;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopGE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopConcat (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopConcat (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopConcat _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopConcat;
#else
_currn = (_TPPrFunctionExpressionBinopConcat) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopConcat));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopConcat;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopConcat: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopConcat: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopConcat;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopConcat */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopADD (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopADD (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopADD _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopADD;
#else
_currn = (_TPPrFunctionExpressionBinopADD) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopADD));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopADD;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopADD: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopADD: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopADD;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopADD */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopSUB (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopSUB (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopSUB _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopSUB;
#else
_currn = (_TPPrFunctionExpressionBinopSUB) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopSUB));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopSUB;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopSUB: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopSUB: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopSUB;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopSUB */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopMUL (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopMUL (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopMUL _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopMUL;
#else
_currn = (_TPPrFunctionExpressionBinopMUL) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopMUL));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopMUL;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopMUL: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopMUL: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopMUL;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopMUL */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopDIV (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopDIV (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopDIV _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopDIV;
#else
_currn = (_TPPrFunctionExpressionBinopDIV) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopDIV));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopDIV;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopDIV: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopDIV: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopDIV;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopDIV */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopMOD (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionExpressionBinopMOD (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionExpressionBinopMOD _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopMOD;
#else
_currn = (_TPPrFunctionExpressionBinopMOD) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopMOD));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopMOD;
_currn->_desc1 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopMOD: root of subtree no. 1 can not be made a Function_BinaryExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_BinaryExpression) MkFunction_BinaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopMOD: root of subtree no. 2 can not be made a Function_BinaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionExpressionBinopMOD;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopMOD */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionBinopIsUnary (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionExpressionBinopIsUnary (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionExpressionBinopIsUnary _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionBinopIsUnary;
#else
_currn = (_TPPrFunctionExpressionBinopIsUnary) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionBinopIsUnary));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionBinopIsUnary;
_currn->_desc1 = (_TSPFunction_UnaryExpression) MkFunction_UnaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionBinopIsUnary: root of subtree no. 1 can not be made a Function_UnaryExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionExpressionBinopIsUnary;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionBinopIsUnary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionUnaryIsPostfix (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionUnaryIsPostfix (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionUnaryIsPostfix _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionUnaryIsPostfix;
#else
_currn = (_TPPrFunctionUnaryIsPostfix) TreeNodeAlloc (sizeof (struct _TPrFunctionUnaryIsPostfix));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionUnaryIsPostfix;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionUnaryIsPostfix: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionUnaryIsPostfix;
return ( (NODEPTR) _currn);
}/* MkrFunctionUnaryIsPostfix */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionUnaryIsIncrement (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionUnaryIsIncrement (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionUnaryIsIncrement _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionUnaryIsIncrement;
#else
_currn = (_TPPrFunctionUnaryIsIncrement) TreeNodeAlloc (sizeof (struct _TPrFunctionUnaryIsIncrement));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionUnaryIsIncrement;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionUnaryIsIncrement: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionUnaryIsIncrement;
return ( (NODEPTR) _currn);
}/* MkrFunctionUnaryIsIncrement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionUnaryIsMinus (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionUnaryIsMinus (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionUnaryIsMinus _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionUnaryIsMinus;
#else
_currn = (_TPPrFunctionUnaryIsMinus) TreeNodeAlloc (sizeof (struct _TPrFunctionUnaryIsMinus));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionUnaryIsMinus;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionUnaryIsMinus: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionUnaryIsMinus;
return ( (NODEPTR) _currn);
}/* MkrFunctionUnaryIsMinus */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionUnaryIsLocation (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionUnaryIsLocation (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionUnaryIsLocation _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionUnaryIsLocation;
#else
_currn = (_TPPrFunctionUnaryIsLocation) TreeNodeAlloc (sizeof (struct _TPrFunctionUnaryIsLocation));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionUnaryIsLocation;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionUnaryIsLocation: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionUnaryIsLocation;
return ( (NODEPTR) _currn);
}/* MkrFunctionUnaryIsLocation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionUnaryIsNot (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionUnaryIsNot (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionUnaryIsNot _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionUnaryIsNot;
#else
_currn = (_TPPrFunctionUnaryIsNot) TreeNodeAlloc (sizeof (struct _TPrFunctionUnaryIsNot));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionUnaryIsNot;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionUnaryIsNot: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionUnaryIsNot;
return ( (NODEPTR) _currn);
}/* MkrFunctionUnaryIsNot */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionUnaryIsPtr (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionUnaryIsPtr (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionUnaryIsPtr _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionUnaryIsPtr;
#else
_currn = (_TPPrFunctionUnaryIsPtr) TreeNodeAlloc (sizeof (struct _TPrFunctionUnaryIsPtr));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionUnaryIsPtr;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionUnaryIsPtr: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionUnaryIsPtr;
return ( (NODEPTR) _currn);
}/* MkrFunctionUnaryIsPtr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPostfixIsPrim (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPostfixIsPrim (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPostfixIsPrim _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPostfixIsPrim;
#else
_currn = (_TPPrFunctionPostfixIsPrim) TreeNodeAlloc (sizeof (struct _TPrFunctionPostfixIsPrim));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPostfixIsPrim;
_currn->_desc1 = (_TSPFunction_PrimaryExpression) MkFunction_PrimaryExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPostfixIsPrim: root of subtree no. 1 can not be made a Function_PrimaryExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPostfixIsPrim;
return ( (NODEPTR) _currn);
}/* MkrFunctionPostfixIsPrim */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPostfixIsIndex (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionPostfixIsIndex (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionPostfixIsIndex _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPostfixIsIndex;
#else
_currn = (_TPPrFunctionPostfixIsIndex) TreeNodeAlloc (sizeof (struct _TPrFunctionPostfixIsIndex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPostfixIsIndex;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPostfixIsIndex: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPostfixIsIndex: root of subtree no. 2 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionPostfixIsIndex;
return ( (NODEPTR) _currn);
}/* MkrFunctionPostfixIsIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPostfixIsAccess (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionPostfixIsAccess (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionPostfixIsAccess _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPostfixIsAccess;
#else
_currn = (_TPPrFunctionPostfixIsAccess) TreeNodeAlloc (sizeof (struct _TPrFunctionPostfixIsAccess));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPostfixIsAccess;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPostfixIsAccess: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_PrimaryExpression) MkFunction_PrimaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPostfixIsAccess: root of subtree no. 2 can not be made a Function_PrimaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionPostfixIsAccess;
return ( (NODEPTR) _currn);
}/* MkrFunctionPostfixIsAccess */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPostfixIsListcon (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionPostfixIsListcon (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionPostfixIsListcon _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPostfixIsListcon;
#else
_currn = (_TPPrFunctionPostfixIsListcon) TreeNodeAlloc (sizeof (struct _TPrFunctionPostfixIsListcon));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPostfixIsListcon;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPostfixIsListcon: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_PrimaryExpression) MkFunction_PrimaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPostfixIsListcon: root of subtree no. 2 can not be made a Function_PrimaryExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionPostfixIsListcon;
return ( (NODEPTR) _currn);
}/* MkrFunctionPostfixIsListcon */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPrimaryIsConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPrimaryIsConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPrimaryIsConstant _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPrimaryIsConstant;
#else
_currn = (_TPPrFunctionPrimaryIsConstant) TreeNodeAlloc (sizeof (struct _TPrFunctionPrimaryIsConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPrimaryIsConstant;
_currn->_desc1 = (_TSPFunction_Constant) MkFunction_Constant (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPrimaryIsConstant: root of subtree no. 1 can not be made a Function_Constant node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPrimaryIsConstant;
return ( (NODEPTR) _currn);
}/* MkrFunctionPrimaryIsConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPrimaryIsExpression (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPrimaryIsExpression (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPrimaryIsExpression _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPrimaryIsExpression;
#else
_currn = (_TPPrFunctionPrimaryIsExpression) TreeNodeAlloc (sizeof (struct _TPrFunctionPrimaryIsExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPrimaryIsExpression;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPrimaryIsExpression: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPrimaryIsExpression;
return ( (NODEPTR) _currn);
}/* MkrFunctionPrimaryIsExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionConstant _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionConstant;
#else
_currn = (_TPPrFunctionConstant) TreeNodeAlloc (sizeof (struct _TPrFunctionConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionConstant;
_currn->_desc1 = (_TSPConstant) MkConstant (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionConstant: root of subtree no. 1 can not be made a Constant node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionConstant;
return ( (NODEPTR) _currn);
}/* MkrFunctionConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantString (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrConstantString (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrConstantString _currn;
#ifdef __cplusplus
_currn = new _TPrConstantString;
#else
_currn = (_TPPrConstantString) TreeNodeAlloc (sizeof (struct _TPrConstantString));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantString;
_currn->_desc1 = (_TSPString) MkString (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstantString: root of subtree no. 1 can not be made a String node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rConstantString;
return ( (NODEPTR) _currn);
}/* MkrConstantString */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantNumber (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrConstantNumber (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrConstantNumber _currn;
#ifdef __cplusplus
_currn = new _TPrConstantNumber;
#else
_currn = (_TPPrConstantNumber) TreeNodeAlloc (sizeof (struct _TPrConstantNumber));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantNumber;
_currn->_desc1 = (_TSPNumber) MkNumber (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstantNumber: root of subtree no. 1 can not be made a Number node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rConstantNumber;
return ( (NODEPTR) _currn);
}/* MkrConstantNumber */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantTrue (POSITION *_coordref)
#else
NODEPTR MkrConstantTrue (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrConstantTrue _currn;
#ifdef __cplusplus
_currn = new _TPrConstantTrue;
#else
_currn = (_TPPrConstantTrue) TreeNodeAlloc (sizeof (struct _TPrConstantTrue));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantTrue;
_SETCOORD(_currn)
_TERMACT_rConstantTrue;
return ( (NODEPTR) _currn);
}/* MkrConstantTrue */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantFalse (POSITION *_coordref)
#else
NODEPTR MkrConstantFalse (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrConstantFalse _currn;
#ifdef __cplusplus
_currn = new _TPrConstantFalse;
#else
_currn = (_TPPrConstantFalse) TreeNodeAlloc (sizeof (struct _TPrConstantFalse));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantFalse;
_SETCOORD(_currn)
_TERMACT_rConstantFalse;
return ( (NODEPTR) _currn);
}/* MkrConstantFalse */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantPos (POSITION *_coordref)
#else
NODEPTR MkrConstantPos (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrConstantPos _currn;
#ifdef __cplusplus
_currn = new _TPrConstantPos;
#else
_currn = (_TPPrConstantPos) TreeNodeAlloc (sizeof (struct _TPrConstantPos));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantPos;
_SETCOORD(_currn)
_TERMACT_rConstantPos;
return ( (NODEPTR) _currn);
}/* MkrConstantPos */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantEmptyList (POSITION *_coordref)
#else
NODEPTR MkrConstantEmptyList (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrConstantEmptyList _currn;
#ifdef __cplusplus
_currn = new _TPrConstantEmptyList;
#else
_currn = (_TPPrConstantEmptyList) TreeNodeAlloc (sizeof (struct _TPrConstantEmptyList));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantEmptyList;
_SETCOORD(_currn)
_TERMACT_rConstantEmptyList;
return ( (NODEPTR) _currn);
}/* MkrConstantEmptyList */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPrimaryIsCall (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPrimaryIsCall (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPrimaryIsCall _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPrimaryIsCall;
#else
_currn = (_TPPrFunctionPrimaryIsCall) TreeNodeAlloc (sizeof (struct _TPrFunctionPrimaryIsCall));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPrimaryIsCall;
_currn->_desc1 = (_TSPFunction_Call) MkFunction_Call (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPrimaryIsCall: root of subtree no. 1 can not be made a Function_Call node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPrimaryIsCall;
return ( (NODEPTR) _currn);
}/* MkrFunctionPrimaryIsCall */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionCallParameters (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionCallParameters (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionCallParameters _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionCallParameters;
#else
_currn = (_TPPrFunctionCallParameters) TreeNodeAlloc (sizeof (struct _TPrFunctionCallParameters));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionCallParameters;
_currn->_desc1 = (_TSPCallable_Reference) MkCallable_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCallParameters: root of subtree no. 1 can not be made a Callable_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_CallParameters) MkFunction_CallParameters (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCallParameters: root of subtree no. 2 can not be made a Function_CallParameters node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionCallParameters;
return ( (NODEPTR) _currn);
}/* MkrFunctionCallParameters */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionCallEmpty (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionCallEmpty (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionCallEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionCallEmpty;
#else
_currn = (_TPPrFunctionCallEmpty) TreeNodeAlloc (sizeof (struct _TPrFunctionCallEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionCallEmpty;
_currn->_desc1 = (_TSPCallable_Reference) MkCallable_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCallEmpty: root of subtree no. 1 can not be made a Callable_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionCallEmpty;
return ( (NODEPTR) _currn);
}/* MkrFunctionCallEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionCallEmptyConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionCallEmptyConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionCallEmptyConstant _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionCallEmptyConstant;
#else
_currn = (_TPPrFunctionCallEmptyConstant) TreeNodeAlloc (sizeof (struct _TPrFunctionCallEmptyConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionCallEmptyConstant;
_currn->_desc1 = (_TSPCallable_Reference) MkCallable_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCallEmptyConstant: root of subtree no. 1 can not be made a Callable_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionCallEmptyConstant;
return ( (NODEPTR) _currn);
}/* MkrFunctionCallEmptyConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrCallIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrCallIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrCallIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrCallIdentifier;
#else
_currn = (_TPPrCallIdentifier) TreeNodeAlloc (sizeof (struct _TPrCallIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErCallIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rCallIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rCallIdentifier;
return ( (NODEPTR) _currn);
}/* MkrCallIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrCallType (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrCallType (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrCallType _currn;
#ifdef __cplusplus
_currn = new _TPrCallType;
#else
_currn = (_TPPrCallType) TreeNodeAlloc (sizeof (struct _TPrCallType));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErCallType;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rCallType: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rCallType;
return ( (NODEPTR) _currn);
}/* MkrCallType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrCallEmpty (POSITION *_coordref)
#else
NODEPTR MkrCallEmpty (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrCallEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrCallEmpty;
#else
_currn = (_TPPrCallEmpty) TreeNodeAlloc (sizeof (struct _TPrCallEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErCallEmpty;
_SETCOORD(_currn)
_TERMACT_rCallEmpty;
return ( (NODEPTR) _currn);
}/* MkrCallEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionCallParamList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionCallParamList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionCallParamList2 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionCallParamList2;
#else
_currn = (_TPPrFunctionCallParamList2) TreeNodeAlloc (sizeof (struct _TPrFunctionCallParamList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionCallParamList2;
_currn->_desc1 = (_TSPFunction_CallParameters) MkFunction_CallParameters (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCallParamList2: root of subtree no. 1 can not be made a Function_CallParameters node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_CallParameter) MkFunction_CallParameter (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCallParamList2: root of subtree no. 2 can not be made a Function_CallParameter node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionCallParamList2;
return ( (NODEPTR) _currn);
}/* MkrFunctionCallParamList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionCallParamList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionCallParamList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionCallParamList1 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionCallParamList1;
#else
_currn = (_TPPrFunctionCallParamList1) TreeNodeAlloc (sizeof (struct _TPrFunctionCallParamList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionCallParamList1;
_currn->_desc1 = (_TSPFunction_CallParameter) MkFunction_CallParameter (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCallParamList1: root of subtree no. 1 can not be made a Function_CallParameter node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionCallParamList1;
return ( (NODEPTR) _currn);
}/* MkrFunctionCallParamList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionCallParamIsExpr (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionCallParamIsExpr (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionCallParamIsExpr _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionCallParamIsExpr;
#else
_currn = (_TPPrFunctionCallParamIsExpr) TreeNodeAlloc (sizeof (struct _TPrFunctionCallParamIsExpr));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionCallParamIsExpr;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCallParamIsExpr: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionCallParamIsExpr;
return ( (NODEPTR) _currn);
}/* MkrFunctionCallParamIsExpr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExprIsError (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionExprIsError (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionExprIsError _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExprIsError;
#else
_currn = (_TPPrFunctionExprIsError) TreeNodeAlloc (sizeof (struct _TPrFunctionExprIsError));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExprIsError;
_currn->_desc1 = (_TSPFunction_ErrorExpression) MkFunction_ErrorExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExprIsError: root of subtree no. 1 can not be made a Function_ErrorExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionExprIsError;
return ( (NODEPTR) _currn);
}/* MkrFunctionExprIsError */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionErrorExpression (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionErrorExpression (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionErrorExpression _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionErrorExpression;
#else
_currn = (_TPPrFunctionErrorExpression) TreeNodeAlloc (sizeof (struct _TPrFunctionErrorExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionErrorExpression;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionErrorExpression: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionErrorExpression;
return ( (NODEPTR) _currn);
}/* MkrFunctionErrorExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionIsIf (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionExpressionIsIf (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionExpressionIsIf _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionIsIf;
#else
_currn = (_TPPrFunctionExpressionIsIf) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionIsIf));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionIsIf;
_currn->_desc1 = (_TSPFunction_IfExpression) MkFunction_IfExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionIsIf: root of subtree no. 1 can not be made a Function_IfExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionExpressionIsIf;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionIsIf */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionIfExpression (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrFunctionIfExpression (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrFunctionIfExpression _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionIfExpression;
#else
_currn = (_TPPrFunctionIfExpression) TreeNodeAlloc (sizeof (struct _TPrFunctionIfExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionIfExpression;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionIfExpression: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionIfExpression: root of subtree no. 2 can not be made a Function_Expression node ", 0, _coordref);
_currn->_desc3 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionIfExpression: root of subtree no. 3 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionIfExpression;
return ( (NODEPTR) _currn);
}/* MkrFunctionIfExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionRhsIsGuards (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionRhsIsGuards (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionRhsIsGuards _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionRhsIsGuards;
#else
_currn = (_TPPrFunctionRhsIsGuards) TreeNodeAlloc (sizeof (struct _TPrFunctionRhsIsGuards));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionRhsIsGuards;
_currn->_desc1 = (_TSPFunction_Guards) MkFunction_Guards (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionRhsIsGuards: root of subtree no. 1 can not be made a Function_Guards node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionRhsIsGuards;
return ( (NODEPTR) _currn);
}/* MkrFunctionRhsIsGuards */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionGuardList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionGuardList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionGuardList2 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionGuardList2;
#else
_currn = (_TPPrFunctionGuardList2) TreeNodeAlloc (sizeof (struct _TPrFunctionGuardList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionGuardList2;
_currn->_desc1 = (_TSPFunction_Guards) MkFunction_Guards (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionGuardList2: root of subtree no. 1 can not be made a Function_Guards node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Guard) MkFunction_Guard (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionGuardList2: root of subtree no. 2 can not be made a Function_Guard node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionGuardList2;
return ( (NODEPTR) _currn);
}/* MkrFunctionGuardList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionGuardList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionGuardList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionGuardList1 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionGuardList1;
#else
_currn = (_TPPrFunctionGuardList1) TreeNodeAlloc (sizeof (struct _TPrFunctionGuardList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionGuardList1;
_currn->_desc1 = (_TSPFunction_Guard) MkFunction_Guard (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionGuardList1: root of subtree no. 1 can not be made a Function_Guard node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionGuardList1;
return ( (NODEPTR) _currn);
}/* MkrFunctionGuardList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionGuard (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionGuard (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionGuard _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionGuard;
#else
_currn = (_TPPrFunctionGuard) TreeNodeAlloc (sizeof (struct _TPrFunctionGuard));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionGuard;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionGuard: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_RightHandSide) MkFunction_RightHandSide (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionGuard: root of subtree no. 2 can not be made a Function_RightHandSide node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionGuard;
return ( (NODEPTR) _currn);
}/* MkrFunctionGuard */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRhsStatements (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRhsStatements (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRhsStatements _currn;
#ifdef __cplusplus
_currn = new _TPrRhsStatements;
#else
_currn = (_TPPrRhsStatements) TreeNodeAlloc (sizeof (struct _TPrRhsStatements));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRhsStatements;
_currn->_desc1 = (_TSPFunction_DoStatements) MkFunction_DoStatements (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRhsStatements: root of subtree no. 1 can not be made a Function_DoStatements node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRhsStatements;
return ( (NODEPTR) _currn);
}/* MkrRhsStatements */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoStatementList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionDoStatementList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionDoStatementList2 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoStatementList2;
#else
_currn = (_TPPrFunctionDoStatementList2) TreeNodeAlloc (sizeof (struct _TPrFunctionDoStatementList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoStatementList2;
_currn->_desc1 = (_TSPFunction_DoStatements) MkFunction_DoStatements (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoStatementList2: root of subtree no. 1 can not be made a Function_DoStatements node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_DoStatement) MkFunction_DoStatement (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoStatementList2: root of subtree no. 2 can not be made a Function_DoStatement node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionDoStatementList2;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoStatementList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoStatementListEmpty (POSITION *_coordref)
#else
NODEPTR MkrFunctionDoStatementListEmpty (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrFunctionDoStatementListEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoStatementListEmpty;
#else
_currn = (_TPPrFunctionDoStatementListEmpty) TreeNodeAlloc (sizeof (struct _TPrFunctionDoStatementListEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoStatementListEmpty;
_SETCOORD(_currn)
_TERMACT_rFunctionDoStatementListEmpty;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoStatementListEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoExpression (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionDoExpression (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionDoExpression _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoExpression;
#else
_currn = (_TPPrFunctionDoExpression) TreeNodeAlloc (sizeof (struct _TPrFunctionDoExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoExpression;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoExpression: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionDoExpression;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoCondition (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionDoCondition (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionDoCondition _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoCondition;
#else
_currn = (_TPPrFunctionDoCondition) TreeNodeAlloc (sizeof (struct _TPrFunctionDoCondition));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoCondition;
_currn->_desc1 = (_TSPFunction_CondStatement) MkFunction_CondStatement (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoCondition: root of subtree no. 1 can not be made a Function_CondStatement node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionDoCondition;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoCondition */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoAssign (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionDoAssign (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionDoAssign _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoAssign;
#else
_currn = (_TPPrFunctionDoAssign) TreeNodeAlloc (sizeof (struct _TPrFunctionDoAssign));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoAssign;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoAssign: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoAssign: root of subtree no. 2 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionDoAssign;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoAssign */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoReturn (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionDoReturn (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionDoReturn _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoReturn;
#else
_currn = (_TPPrFunctionDoReturn) TreeNodeAlloc (sizeof (struct _TPrFunctionDoReturn));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoReturn;
_currn->_desc1 = (_TSPFunction_ReturnStatement) MkFunction_ReturnStatement (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoReturn: root of subtree no. 1 can not be made a Function_ReturnStatement node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionDoReturn;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoReturn */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionReturnStatement (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionReturnStatement (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionReturnStatement _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionReturnStatement;
#else
_currn = (_TPPrFunctionReturnStatement) TreeNodeAlloc (sizeof (struct _TPrFunctionReturnStatement));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionReturnStatement;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionReturnStatement: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionReturnStatement;
return ( (NODEPTR) _currn);
}/* MkrFunctionReturnStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionCondStatement (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrFunctionCondStatement (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrFunctionCondStatement _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionCondStatement;
#else
_currn = (_TPPrFunctionCondStatement) TreeNodeAlloc (sizeof (struct _TPrFunctionCondStatement));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionCondStatement;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCondStatement: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_ErrorExpression) MkFunction_ErrorExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCondStatement: root of subtree no. 2 can not be made a Function_ErrorExpression node ", 0, _coordref);
_currn->_desc3 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCondStatement: root of subtree no. 3 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionCondStatement;
return ( (NODEPTR) _currn);
}/* MkrFunctionCondStatement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionCondStatUnit (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionCondStatUnit (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionCondStatUnit _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionCondStatUnit;
#else
_currn = (_TPPrFunctionCondStatUnit) TreeNodeAlloc (sizeof (struct _TPrFunctionCondStatUnit));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionCondStatUnit;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCondStatUnit: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_ErrorExpression) MkFunction_ErrorExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionCondStatUnit: root of subtree no. 2 can not be made a Function_ErrorExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionCondStatUnit;
return ( (NODEPTR) _currn);
}/* MkrFunctionCondStatUnit */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionIsLet (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionExpressionIsLet (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionExpressionIsLet _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionIsLet;
#else
_currn = (_TPPrFunctionExpressionIsLet) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionIsLet));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionIsLet;
_currn->_desc1 = (_TSPFunction_LetExpression) MkFunction_LetExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionIsLet: root of subtree no. 1 can not be made a Function_LetExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionExpressionIsLet;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionIsLet */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionLetExpression (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionLetExpression (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionLetExpression _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionLetExpression;
#else
_currn = (_TPPrFunctionLetExpression) TreeNodeAlloc (sizeof (struct _TPrFunctionLetExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionLetExpression;
_currn->_desc1 = (_TSPFunction_LetVarDefs) MkFunction_LetVarDefs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLetExpression: root of subtree no. 1 can not be made a Function_LetVarDefs node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLetExpression: root of subtree no. 2 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionLetExpression;
return ( (NODEPTR) _currn);
}/* MkrFunctionLetExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionLetVarDefList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionLetVarDefList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionLetVarDefList2 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionLetVarDefList2;
#else
_currn = (_TPPrFunctionLetVarDefList2) TreeNodeAlloc (sizeof (struct _TPrFunctionLetVarDefList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionLetVarDefList2;
_currn->_desc1 = (_TSPFunction_LetVarDefs) MkFunction_LetVarDefs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLetVarDefList2: root of subtree no. 1 can not be made a Function_LetVarDefs node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_LetVarDef) MkFunction_LetVarDef (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLetVarDefList2: root of subtree no. 2 can not be made a Function_LetVarDef node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionLetVarDefList2;
return ( (NODEPTR) _currn);
}/* MkrFunctionLetVarDefList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionLetVarDefList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionLetVarDefList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionLetVarDefList1 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionLetVarDefList1;
#else
_currn = (_TPPrFunctionLetVarDefList1) TreeNodeAlloc (sizeof (struct _TPrFunctionLetVarDefList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionLetVarDefList1;
_currn->_desc1 = (_TSPFunction_LetVarDef) MkFunction_LetVarDef (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLetVarDefList1: root of subtree no. 1 can not be made a Function_LetVarDef node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionLetVarDefList1;
return ( (NODEPTR) _currn);
}/* MkrFunctionLetVarDefList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionLetVarDef (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrFunctionLetVarDef (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrFunctionLetVarDef _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionLetVarDef;
#else
_currn = (_TPPrFunctionLetVarDef) TreeNodeAlloc (sizeof (struct _TPrFunctionLetVarDef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionLetVarDef;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLetVarDef: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPOptionalType) MkOptionalType (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLetVarDef: root of subtree no. 2 can not be made a OptionalType node ", 0, _coordref);
_currn->_desc3 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLetVarDef: root of subtree no. 3 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionLetVarDef;
return ( (NODEPTR) _currn);
}/* MkrFunctionLetVarDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoIsMessage (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionDoIsMessage (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionDoIsMessage _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoIsMessage;
#else
_currn = (_TPPrFunctionDoIsMessage) TreeNodeAlloc (sizeof (struct _TPrFunctionDoIsMessage));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoIsMessage;
_currn->_desc1 = (_TSPFunction_DoMessage) MkFunction_DoMessage (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoIsMessage: root of subtree no. 1 can not be made a Function_DoMessage node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionDoIsMessage;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoIsMessage */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoMessage (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionDoMessage (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionDoMessage _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoMessage;
#else
_currn = (_TPPrFunctionDoMessage) TreeNodeAlloc (sizeof (struct _TPrFunctionDoMessage));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoMessage;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoMessage: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoMessage: root of subtree no. 2 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionDoMessage;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoMessage */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionExpressionIsLambda (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionExpressionIsLambda (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionExpressionIsLambda _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionExpressionIsLambda;
#else
_currn = (_TPPrFunctionExpressionIsLambda) TreeNodeAlloc (sizeof (struct _TPrFunctionExpressionIsLambda));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionExpressionIsLambda;
_currn->_desc1 = (_TSPFunction_LambdaExpr) MkFunction_LambdaExpr (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionExpressionIsLambda: root of subtree no. 1 can not be made a Function_LambdaExpr node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionExpressionIsLambda;
return ( (NODEPTR) _currn);
}/* MkrFunctionExpressionIsLambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionLambdaExpr (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3, NODEPTR _desc4)
#else
NODEPTR MkrFunctionLambdaExpr (_coordref,_desc1,_desc2,_desc3,_desc4)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
	NODEPTR _desc4;
#endif
{	_TPPrFunctionLambdaExpr _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionLambdaExpr;
#else
_currn = (_TPPrFunctionLambdaExpr) TreeNodeAlloc (sizeof (struct _TPrFunctionLambdaExpr));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionLambdaExpr;
_currn->_desc1 = (_TSPLambda) MkLambda (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLambdaExpr: root of subtree no. 1 can not be made a Lambda node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLambdaExpr: root of subtree no. 2 can not be made a Function_VariableDefId node ", 0, _coordref);
_currn->_desc3 = (_TSPOptionalType) MkOptionalType (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLambdaExpr: root of subtree no. 3 can not be made a OptionalType node ", 0, _coordref);
_currn->_desc4 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc4);	
if (((NODEPTR)_currn->_desc4) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionLambdaExpr: root of subtree no. 4 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionLambdaExpr;
return ( (NODEPTR) _currn);
}/* MkrFunctionLambdaExpr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoTyping (POSITION *_coordref)
#else
NODEPTR MkrNoTyping (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoTyping _currn;
#ifdef __cplusplus
_currn = new _TPrNoTyping;
#else
_currn = (_TPPrNoTyping) TreeNodeAlloc (sizeof (struct _TPrNoTyping));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoTyping;
_SETCOORD(_currn)
_TERMACT_rNoTyping;
return ( (NODEPTR) _currn);
}/* MkrNoTyping */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrOptionalType (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrOptionalType (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrOptionalType _currn;
#ifdef __cplusplus
_currn = new _TPrOptionalType;
#else
_currn = (_TPPrOptionalType) TreeNodeAlloc (sizeof (struct _TPrOptionalType));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErOptionalType;
_currn->_desc1 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rOptionalType: root of subtree no. 1 can not be made a Simple_Typing node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rOptionalType;
return ( (NODEPTR) _currn);
}/* MkrOptionalType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoIsAssignement (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionDoIsAssignement (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionDoIsAssignement _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoIsAssignement;
#else
_currn = (_TPPrFunctionDoIsAssignement) TreeNodeAlloc (sizeof (struct _TPrFunctionDoIsAssignement));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoIsAssignement;
_currn->_desc1 = (_TSPFunction_AssignStatement) MkFunction_AssignStatement (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoIsAssignement: root of subtree no. 1 can not be made a Function_AssignStatement node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionDoIsAssignement;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoIsAssignement */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoIsEach (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionDoIsEach (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionDoIsEach _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoIsEach;
#else
_currn = (_TPPrFunctionDoIsEach) TreeNodeAlloc (sizeof (struct _TPrFunctionDoIsEach));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoIsEach;
_currn->_desc1 = (_TSPFunction_EachStatement) MkFunction_EachStatement (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoIsEach: root of subtree no. 1 can not be made a Function_EachStatement node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionDoIsEach;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoIsEach */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionStatAssign (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionStatAssign (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionStatAssign _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionStatAssign;
#else
_currn = (_TPPrFunctionStatAssign) TreeNodeAlloc (sizeof (struct _TPrFunctionStatAssign));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionStatAssign;
_currn->_desc1 = (_TSPFunction_PostfixExpression) MkFunction_PostfixExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionStatAssign: root of subtree no. 1 can not be made a Function_PostfixExpression node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionStatAssign: root of subtree no. 2 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionStatAssign;
return ( (NODEPTR) _currn);
}/* MkrFunctionStatAssign */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionEachStatSingle (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrFunctionEachStatSingle (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrFunctionEachStatSingle _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionEachStatSingle;
#else
_currn = (_TPPrFunctionEachStatSingle) TreeNodeAlloc (sizeof (struct _TPrFunctionEachStatSingle));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionEachStatSingle;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionEachStatSingle: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_PrimaryExpression) MkFunction_PrimaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionEachStatSingle: root of subtree no. 2 can not be made a Function_PrimaryExpression node ", 0, _coordref);
_currn->_desc3 = (_TSPFunction_DoStatement) MkFunction_DoStatement (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionEachStatSingle: root of subtree no. 3 can not be made a Function_DoStatement node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionEachStatSingle;
return ( (NODEPTR) _currn);
}/* MkrFunctionEachStatSingle */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionEachStatMultiple (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrFunctionEachStatMultiple (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrFunctionEachStatMultiple _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionEachStatMultiple;
#else
_currn = (_TPPrFunctionEachStatMultiple) TreeNodeAlloc (sizeof (struct _TPrFunctionEachStatMultiple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionEachStatMultiple;
_currn->_desc1 = (_TSPFunction_VariableDefId) MkFunction_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionEachStatMultiple: root of subtree no. 1 can not be made a Function_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_PrimaryExpression) MkFunction_PrimaryExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionEachStatMultiple: root of subtree no. 2 can not be made a Function_PrimaryExpression node ", 0, _coordref);
_currn->_desc3 = (_TSPFunction_DoStatements) MkFunction_DoStatements (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionEachStatMultiple: root of subtree no. 3 can not be made a Function_DoStatements node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionEachStatMultiple;
return ( (NODEPTR) _currn);
}/* MkrFunctionEachStatMultiple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclConstant _currn;
#ifdef __cplusplus
_currn = new _TPrDeclConstant;
#else
_currn = (_TPPrDeclConstant) TreeNodeAlloc (sizeof (struct _TPrDeclConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclConstant;
_currn->_desc1 = (_TSPExternConstant) MkExternConstant (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclConstant: root of subtree no. 1 can not be made a ExternConstant node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclConstant;
return ( (NODEPTR) _currn);
}/* MkrDeclConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrConstantDecl (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrConstantDecl _currn;
#ifdef __cplusplus
_currn = new _TPrConstantDecl;
#else
_currn = (_TPPrConstantDecl) TreeNodeAlloc (sizeof (struct _TPrConstantDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantDecl;
_currn->_desc1 = (_TSPExternConstantDefId) MkExternConstantDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstantDecl: root of subtree no. 1 can not be made a ExternConstantDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstantDecl: root of subtree no. 2 can not be made a Simple_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rConstantDecl;
return ( (NODEPTR) _currn);
}/* MkrConstantDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantDefinition (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrConstantDefinition (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrConstantDefinition _currn;
#ifdef __cplusplus
_currn = new _TPrConstantDefinition;
#else
_currn = (_TPPrConstantDefinition) TreeNodeAlloc (sizeof (struct _TPrConstantDefinition));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantDefinition;
_currn->_desc1 = (_TSPExternConstantDefId) MkExternConstantDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstantDefinition: root of subtree no. 1 can not be made a ExternConstantDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstantDefinition: root of subtree no. 2 can not be made a Simple_Typing node ", 0, _coordref);
_currn->_desc3 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstantDefinition: root of subtree no. 3 can not be made a Function_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rConstantDefinition;
return ( (NODEPTR) _currn);
}/* MkrConstantDefinition */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantDefIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrConstantDefIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrConstantDefIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrConstantDefIdentifier;
#else
_currn = (_TPPrConstantDefIdentifier) TreeNodeAlloc (sizeof (struct _TPrConstantDefIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantDefIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstantDefIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rConstantDefIdentifier;
return ( (NODEPTR) _currn);
}/* MkrConstantDefIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConstantDefTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrConstantDefTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrConstantDefTypename _currn;
#ifdef __cplusplus
_currn = new _TPrConstantDefTypename;
#else
_currn = (_TPPrConstantDefTypename) TreeNodeAlloc (sizeof (struct _TPrConstantDefTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConstantDefTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rConstantDefTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rConstantDefTypename;
return ( (NODEPTR) _currn);
}/* MkrConstantDefTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionDoIsNoreturn (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionDoIsNoreturn (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionDoIsNoreturn _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionDoIsNoreturn;
#else
_currn = (_TPPrFunctionDoIsNoreturn) TreeNodeAlloc (sizeof (struct _TPrFunctionDoIsNoreturn));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionDoIsNoreturn;
_currn->_desc1 = (_TSPFunctionNoReturn) MkFunctionNoReturn (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionDoIsNoreturn: root of subtree no. 1 can not be made a FunctionNoReturn node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionDoIsNoreturn;
return ( (NODEPTR) _currn);
}/* MkrFunctionDoIsNoreturn */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionNoReturn (POSITION *_coordref)
#else
NODEPTR MkrFunctionNoReturn (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrFunctionNoReturn _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionNoReturn;
#else
_currn = (_TPPrFunctionNoReturn) TreeNodeAlloc (sizeof (struct _TPrFunctionNoReturn));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionNoReturn;
_SETCOORD(_currn)
_TERMACT_rFunctionNoReturn;
return ( (NODEPTR) _currn);
}/* MkrFunctionNoReturn */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionPrimaryIsTuple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionPrimaryIsTuple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionPrimaryIsTuple _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionPrimaryIsTuple;
#else
_currn = (_TPPrFunctionPrimaryIsTuple) TreeNodeAlloc (sizeof (struct _TPrFunctionPrimaryIsTuple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionPrimaryIsTuple;
_currn->_desc1 = (_TSPFunction_TupleConstruction) MkFunction_TupleConstruction (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionPrimaryIsTuple: root of subtree no. 1 can not be made a Function_TupleConstruction node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionPrimaryIsTuple;
return ( (NODEPTR) _currn);
}/* MkrFunctionPrimaryIsTuple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTupleConstruction (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionTupleConstruction (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionTupleConstruction _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTupleConstruction;
#else
_currn = (_TPPrFunctionTupleConstruction) TreeNodeAlloc (sizeof (struct _TPrFunctionTupleConstruction));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTupleConstruction;
_currn->_desc1 = (_TSPFunction_TupleArguments) MkFunction_TupleArguments (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTupleConstruction: root of subtree no. 1 can not be made a Function_TupleArguments node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_TupleArgument) MkFunction_TupleArgument (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTupleConstruction: root of subtree no. 2 can not be made a Function_TupleArgument node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionTupleConstruction;
return ( (NODEPTR) _currn);
}/* MkrFunctionTupleConstruction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTupleArgumentList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrFunctionTupleArgumentList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrFunctionTupleArgumentList2 _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTupleArgumentList2;
#else
_currn = (_TPPrFunctionTupleArgumentList2) TreeNodeAlloc (sizeof (struct _TPrFunctionTupleArgumentList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTupleArgumentList2;
_currn->_desc1 = (_TSPFunction_TupleArguments) MkFunction_TupleArguments (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTupleArgumentList2: root of subtree no. 1 can not be made a Function_TupleArguments node ", 0, _coordref);
_currn->_desc2 = (_TSPFunction_TupleArgument) MkFunction_TupleArgument (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTupleArgumentList2: root of subtree no. 2 can not be made a Function_TupleArgument node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rFunctionTupleArgumentList2;
return ( (NODEPTR) _currn);
}/* MkrFunctionTupleArgumentList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTupleArgumentList (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionTupleArgumentList (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionTupleArgumentList _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTupleArgumentList;
#else
_currn = (_TPPrFunctionTupleArgumentList) TreeNodeAlloc (sizeof (struct _TPrFunctionTupleArgumentList));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTupleArgumentList;
_currn->_desc1 = (_TSPFunction_TupleArgument) MkFunction_TupleArgument (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTupleArgumentList: root of subtree no. 1 can not be made a Function_TupleArgument node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionTupleArgumentList;
return ( (NODEPTR) _currn);
}/* MkrFunctionTupleArgumentList */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrFunctionTupleArgument (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrFunctionTupleArgument (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrFunctionTupleArgument _currn;
#ifdef __cplusplus
_currn = new _TPrFunctionTupleArgument;
#else
_currn = (_TPPrFunctionTupleArgument) TreeNodeAlloc (sizeof (struct _TPrFunctionTupleArgument));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErFunctionTupleArgument;
_currn->_desc1 = (_TSPFunction_Expression) MkFunction_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rFunctionTupleArgument: root of subtree no. 1 can not be made a Function_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rFunctionTupleArgument;
return ( (NODEPTR) _currn);
}/* MkrFunctionTupleArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolDefId _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolDefId;
#else
_currn = (_TPPrSymbolDefId) TreeNodeAlloc (sizeof (struct _TPrSymbolDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolDefId;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolDefId: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolDefId;
return ( (NODEPTR) _currn);
}/* MkrSymbolDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclAbstreeClass (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclAbstreeClass (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclAbstreeClass _currn;
#ifdef __cplusplus
_currn = new _TPrDeclAbstreeClass;
#else
_currn = (_TPPrDeclAbstreeClass) TreeNodeAlloc (sizeof (struct _TPrDeclAbstreeClass));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclAbstreeClass;
_currn->_desc1 = (_TSPAbstree_Decl_Class) MkAbstree_Decl_Class (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclAbstreeClass: root of subtree no. 1 can not be made a Abstree_Decl_Class node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclAbstreeClass;
return ( (NODEPTR) _currn);
}/* MkrDeclAbstreeClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclAbstree (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclAbstree (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclAbstree _currn;
#ifdef __cplusplus
_currn = new _TPrDeclAbstree;
#else
_currn = (_TPPrDeclAbstree) TreeNodeAlloc (sizeof (struct _TPrDeclAbstree));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclAbstree;
_currn->_desc1 = (_TSPAbstree_Decl) MkAbstree_Decl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclAbstree: root of subtree no. 1 can not be made a Abstree_Decl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclAbstree;
return ( (NODEPTR) _currn);
}/* MkrDeclAbstree */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeClassDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrAbstreeClassDecl (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrAbstreeClassDecl _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeClassDecl;
#else
_currn = (_TPPrAbstreeClassDecl) TreeNodeAlloc (sizeof (struct _TPrAbstreeClassDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeClassDecl;
_currn->_desc1 = (_TSPAbstree_DefId) MkAbstree_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeClassDecl: root of subtree no. 1 can not be made a Abstree_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPAbstree_Changes) MkAbstree_Changes (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeClassDecl: root of subtree no. 2 can not be made a Abstree_Changes node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_Abstree_Rules) MkOpt_Abstree_Rules (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeClassDecl: root of subtree no. 3 can not be made a Opt_Abstree_Rules node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeClassDecl;
return ( (NODEPTR) _currn);
}/* MkrAbstreeClassDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrAbstreeDecl (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrAbstreeDecl _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeDecl;
#else
_currn = (_TPPrAbstreeDecl) TreeNodeAlloc (sizeof (struct _TPrAbstreeDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeDecl;
_currn->_desc1 = (_TSPAbstree_DefId) MkAbstree_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeDecl: root of subtree no. 1 can not be made a Abstree_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPAbstree_Changes) MkAbstree_Changes (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeDecl: root of subtree no. 2 can not be made a Abstree_Changes node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_Abstree_Rules) MkOpt_Abstree_Rules (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeDecl: root of subtree no. 3 can not be made a Opt_Abstree_Rules node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeDecl;
return ( (NODEPTR) _currn);
}/* MkrAbstreeDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeDefIdIsTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeDefIdIsTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeDefIdIsTypename _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeDefIdIsTypename;
#else
_currn = (_TPPrAbstreeDefIdIsTypename) TreeNodeAlloc (sizeof (struct _TPrAbstreeDefIdIsTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeDefIdIsTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeDefIdIsTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeDefIdIsTypename;
return ( (NODEPTR) _currn);
}/* MkrAbstreeDefIdIsTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeDefIdIsIdent (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeDefIdIsIdent (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeDefIdIsIdent _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeDefIdIsIdent;
#else
_currn = (_TPPrAbstreeDefIdIsIdent) TreeNodeAlloc (sizeof (struct _TPrAbstreeDefIdIsIdent));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeDefIdIsIdent;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeDefIdIsIdent: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeDefIdIsIdent;
return ( (NODEPTR) _currn);
}/* MkrAbstreeDefIdIsIdent */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoAbstreeChanges (POSITION *_coordref)
#else
NODEPTR MkrNoAbstreeChanges (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoAbstreeChanges _currn;
#ifdef __cplusplus
_currn = new _TPrNoAbstreeChanges;
#else
_currn = (_TPPrNoAbstreeChanges) TreeNodeAlloc (sizeof (struct _TPrNoAbstreeChanges));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoAbstreeChanges;
_SETCOORD(_currn)
_TERMACT_rNoAbstreeChanges;
return ( (NODEPTR) _currn);
}/* MkrNoAbstreeChanges */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeChangeList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrAbstreeChangeList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrAbstreeChangeList2 _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeChangeList2;
#else
_currn = (_TPPrAbstreeChangeList2) TreeNodeAlloc (sizeof (struct _TPrAbstreeChangeList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeChangeList2;
_currn->_desc1 = (_TSPAbstree_Changes) MkAbstree_Changes (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeChangeList2: root of subtree no. 1 can not be made a Abstree_Changes node ", 0, _coordref);
_currn->_desc2 = (_TSPAbstree_Change) MkAbstree_Change (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeChangeList2: root of subtree no. 2 can not be made a Abstree_Change node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeChangeList2;
return ( (NODEPTR) _currn);
}/* MkrAbstreeChangeList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeChangeIsPrefix (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeChangeIsPrefix (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeChangeIsPrefix _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeChangeIsPrefix;
#else
_currn = (_TPPrAbstreeChangeIsPrefix) TreeNodeAlloc (sizeof (struct _TPrAbstreeChangeIsPrefix));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeChangeIsPrefix;
_currn->_desc1 = (_TSPAbstree_Prefix) MkAbstree_Prefix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeChangeIsPrefix: root of subtree no. 1 can not be made a Abstree_Prefix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeChangeIsPrefix;
return ( (NODEPTR) _currn);
}/* MkrAbstreeChangeIsPrefix */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeChangeIsCombination (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeChangeIsCombination (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeChangeIsCombination _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeChangeIsCombination;
#else
_currn = (_TPPrAbstreeChangeIsCombination) TreeNodeAlloc (sizeof (struct _TPrAbstreeChangeIsCombination));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeChangeIsCombination;
_currn->_desc1 = (_TSPAbstree_Combination) MkAbstree_Combination (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeChangeIsCombination: root of subtree no. 1 can not be made a Abstree_Combination node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeChangeIsCombination;
return ( (NODEPTR) _currn);
}/* MkrAbstreeChangeIsCombination */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeChangeIsInherit (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeChangeIsInherit (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeChangeIsInherit _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeChangeIsInherit;
#else
_currn = (_TPPrAbstreeChangeIsInherit) TreeNodeAlloc (sizeof (struct _TPrAbstreeChangeIsInherit));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeChangeIsInherit;
_currn->_desc1 = (_TSPAbstree_Inheritance) MkAbstree_Inheritance (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeChangeIsInherit: root of subtree no. 1 can not be made a Abstree_Inheritance node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeChangeIsInherit;
return ( (NODEPTR) _currn);
}/* MkrAbstreeChangeIsInherit */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeChangeIsTrafo (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeChangeIsTrafo (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeChangeIsTrafo _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeChangeIsTrafo;
#else
_currn = (_TPPrAbstreeChangeIsTrafo) TreeNodeAlloc (sizeof (struct _TPrAbstreeChangeIsTrafo));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeChangeIsTrafo;
_currn->_desc1 = (_TSPAbstree_Transformation) MkAbstree_Transformation (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeChangeIsTrafo: root of subtree no. 1 can not be made a Abstree_Transformation node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeChangeIsTrafo;
return ( (NODEPTR) _currn);
}/* MkrAbstreeChangeIsTrafo */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeChangeIsDependency (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeChangeIsDependency (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeChangeIsDependency _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeChangeIsDependency;
#else
_currn = (_TPPrAbstreeChangeIsDependency) TreeNodeAlloc (sizeof (struct _TPrAbstreeChangeIsDependency));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeChangeIsDependency;
_currn->_desc1 = (_TSPAbstree_Dependency) MkAbstree_Dependency (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeChangeIsDependency: root of subtree no. 1 can not be made a Abstree_Dependency node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeChangeIsDependency;
return ( (NODEPTR) _currn);
}/* MkrAbstreeChangeIsDependency */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreePrefix (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrAbstreePrefix (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrAbstreePrefix _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreePrefix;
#else
_currn = (_TPPrAbstreePrefix) TreeNodeAlloc (sizeof (struct _TPrAbstreePrefix));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreePrefix;
_currn->_desc1 = (_TSPAbstree_ReferenceList) MkAbstree_ReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreePrefix: root of subtree no. 1 can not be made a Abstree_ReferenceList node ", 0, _coordref);
_currn->_desc2 = (_TSPTypename) MkTypename (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreePrefix: root of subtree no. 2 can not be made a Typename node ", 0, _coordref);
_currn->_desc3 = (_TSPNew_Ruleprefix) MkNew_Ruleprefix (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreePrefix: root of subtree no. 3 can not be made a New_Ruleprefix node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreePrefix;
return ( (NODEPTR) _currn);
}/* MkrAbstreePrefix */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNewRuleprefixIsTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrNewRuleprefixIsTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrNewRuleprefixIsTypename _currn;
#ifdef __cplusplus
_currn = new _TPrNewRuleprefixIsTypename;
#else
_currn = (_TPPrNewRuleprefixIsTypename) TreeNodeAlloc (sizeof (struct _TPrNewRuleprefixIsTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNewRuleprefixIsTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rNewRuleprefixIsTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rNewRuleprefixIsTypename;
return ( (NODEPTR) _currn);
}/* MkrNewRuleprefixIsTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNewRuleprefixIsIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrNewRuleprefixIsIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrNewRuleprefixIsIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrNewRuleprefixIsIdentifier;
#else
_currn = (_TPPrNewRuleprefixIsIdentifier) TreeNodeAlloc (sizeof (struct _TPrNewRuleprefixIsIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNewRuleprefixIsIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rNewRuleprefixIsIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rNewRuleprefixIsIdentifier;
return ( (NODEPTR) _currn);
}/* MkrNewRuleprefixIsIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeCombination (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeCombination (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeCombination _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeCombination;
#else
_currn = (_TPPrAbstreeCombination) TreeNodeAlloc (sizeof (struct _TPrAbstreeCombination));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeCombination;
_currn->_desc1 = (_TSPAbstree_ReferenceList) MkAbstree_ReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeCombination: root of subtree no. 1 can not be made a Abstree_ReferenceList node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeCombination;
return ( (NODEPTR) _currn);
}/* MkrAbstreeCombination */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeReferenceList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrAbstreeReferenceList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrAbstreeReferenceList2 _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeReferenceList2;
#else
_currn = (_TPPrAbstreeReferenceList2) TreeNodeAlloc (sizeof (struct _TPrAbstreeReferenceList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeReferenceList2;
_currn->_desc1 = (_TSPAbstree_ReferenceList) MkAbstree_ReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeReferenceList2: root of subtree no. 1 can not be made a Abstree_ReferenceList node ", 0, _coordref);
_currn->_desc2 = (_TSPAbstree_Reference) MkAbstree_Reference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeReferenceList2: root of subtree no. 2 can not be made a Abstree_Reference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeReferenceList2;
return ( (NODEPTR) _currn);
}/* MkrAbstreeReferenceList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeReferenceList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeReferenceList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeReferenceList1 _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeReferenceList1;
#else
_currn = (_TPPrAbstreeReferenceList1) TreeNodeAlloc (sizeof (struct _TPrAbstreeReferenceList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeReferenceList1;
_currn->_desc1 = (_TSPAbstree_Reference) MkAbstree_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeReferenceList1: root of subtree no. 1 can not be made a Abstree_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeReferenceList1;
return ( (NODEPTR) _currn);
}/* MkrAbstreeReferenceList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeReferenceIsIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeReferenceIsIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeReferenceIsIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeReferenceIsIdentifier;
#else
_currn = (_TPPrAbstreeReferenceIsIdentifier) TreeNodeAlloc (sizeof (struct _TPrAbstreeReferenceIsIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeReferenceIsIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeReferenceIsIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeReferenceIsIdentifier;
return ( (NODEPTR) _currn);
}/* MkrAbstreeReferenceIsIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeReferenceIsTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeReferenceIsTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeReferenceIsTypename _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeReferenceIsTypename;
#else
_currn = (_TPPrAbstreeReferenceIsTypename) TreeNodeAlloc (sizeof (struct _TPrAbstreeReferenceIsTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeReferenceIsTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeReferenceIsTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeReferenceIsTypename;
return ( (NODEPTR) _currn);
}/* MkrAbstreeReferenceIsTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeInheritance (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeInheritance (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeInheritance _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeInheritance;
#else
_currn = (_TPPrAbstreeInheritance) TreeNodeAlloc (sizeof (struct _TPrAbstreeInheritance));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeInheritance;
_currn->_desc1 = (_TSPAbstree_ReferenceList) MkAbstree_ReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeInheritance: root of subtree no. 1 can not be made a Abstree_ReferenceList node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeInheritance;
return ( (NODEPTR) _currn);
}/* MkrAbstreeInheritance */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeTrafoTo (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrAbstreeTrafoTo (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrAbstreeTrafoTo _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeTrafoTo;
#else
_currn = (_TPPrAbstreeTrafoTo) TreeNodeAlloc (sizeof (struct _TPrAbstreeTrafoTo));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeTrafoTo;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTrafoTo: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_currn->_desc2 = (_TSPAbstree_Reference) MkAbstree_Reference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTrafoTo: root of subtree no. 2 can not be made a Abstree_Reference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeTrafoTo;
return ( (NODEPTR) _currn);
}/* MkrAbstreeTrafoTo */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeTrafoFrom (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrAbstreeTrafoFrom (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrAbstreeTrafoFrom _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeTrafoFrom;
#else
_currn = (_TPPrAbstreeTrafoFrom) TreeNodeAlloc (sizeof (struct _TPrAbstreeTrafoFrom));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeTrafoFrom;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTrafoFrom: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_currn->_desc2 = (_TSPAbstree_Reference) MkAbstree_Reference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTrafoFrom: root of subtree no. 2 can not be made a Abstree_Reference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeTrafoFrom;
return ( (NODEPTR) _currn);
}/* MkrAbstreeTrafoFrom */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeDependency (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeDependency (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeDependency _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeDependency;
#else
_currn = (_TPPrAbstreeDependency) TreeNodeAlloc (sizeof (struct _TPrAbstreeDependency));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeDependency;
_currn->_desc1 = (_TSPAbstree_ReferenceList) MkAbstree_ReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeDependency: root of subtree no. 1 can not be made a Abstree_ReferenceList node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeDependency;
return ( (NODEPTR) _currn);
}/* MkrAbstreeDependency */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoAbstreeRules (POSITION *_coordref)
#else
NODEPTR MkrNoAbstreeRules (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoAbstreeRules _currn;
#ifdef __cplusplus
_currn = new _TPrNoAbstreeRules;
#else
_currn = (_TPPrNoAbstreeRules) TreeNodeAlloc (sizeof (struct _TPrNoAbstreeRules));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoAbstreeRules;
_SETCOORD(_currn)
_TERMACT_rNoAbstreeRules;
return ( (NODEPTR) _currn);
}/* MkrNoAbstreeRules */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeRulesExist (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeRulesExist (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeRulesExist _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeRulesExist;
#else
_currn = (_TPPrAbstreeRulesExist) TreeNodeAlloc (sizeof (struct _TPrAbstreeRulesExist));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeRulesExist;
_currn->_desc1 = (_TSPAbstree_Rules) MkAbstree_Rules (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeRulesExist: root of subtree no. 1 can not be made a Abstree_Rules node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeRulesExist;
return ( (NODEPTR) _currn);
}/* MkrAbstreeRulesExist */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeRuleList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrAbstreeRuleList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrAbstreeRuleList2 _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeRuleList2;
#else
_currn = (_TPPrAbstreeRuleList2) TreeNodeAlloc (sizeof (struct _TPrAbstreeRuleList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeRuleList2;
_currn->_desc1 = (_TSPAbstree_Rules) MkAbstree_Rules (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeRuleList2: root of subtree no. 1 can not be made a Abstree_Rules node ", 0, _coordref);
_currn->_desc2 = (_TSPAbstree_Rule) MkAbstree_Rule (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeRuleList2: root of subtree no. 2 can not be made a Abstree_Rule node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeRuleList2;
return ( (NODEPTR) _currn);
}/* MkrAbstreeRuleList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeRuleList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeRuleList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeRuleList1 _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeRuleList1;
#else
_currn = (_TPPrAbstreeRuleList1) TreeNodeAlloc (sizeof (struct _TPrAbstreeRuleList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeRuleList1;
_currn->_desc1 = (_TSPAbstree_Rule) MkAbstree_Rule (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeRuleList1: root of subtree no. 1 can not be made a Abstree_Rule node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeRuleList1;
return ( (NODEPTR) _currn);
}/* MkrAbstreeRuleList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeRuleIsTermDecl (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeRuleIsTermDecl (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeRuleIsTermDecl _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeRuleIsTermDecl;
#else
_currn = (_TPPrAbstreeRuleIsTermDecl) TreeNodeAlloc (sizeof (struct _TPrAbstreeRuleIsTermDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeRuleIsTermDecl;
_currn->_desc1 = (_TSPAbstree_TermDecl) MkAbstree_TermDecl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeRuleIsTermDecl: root of subtree no. 1 can not be made a Abstree_TermDecl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeRuleIsTermDecl;
return ( (NODEPTR) _currn);
}/* MkrAbstreeRuleIsTermDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeTermDeclAs (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrAbstreeTermDeclAs (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrAbstreeTermDeclAs _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeTermDeclAs;
#else
_currn = (_TPPrAbstreeTermDeclAs) TreeNodeAlloc (sizeof (struct _TPrAbstreeTermDeclAs));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeTermDeclAs;
_currn->_desc1 = (_TSPTreeSymbol_DefId) MkTreeSymbol_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTermDeclAs: root of subtree no. 1 can not be made a TreeSymbol_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTermDeclAs: root of subtree no. 2 can not be made a Simple_Typing node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_ConversionReference) MkOpt_ConversionReference (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTermDeclAs: root of subtree no. 3 can not be made a Opt_ConversionReference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeTermDeclAs;
return ( (NODEPTR) _currn);
}/* MkrAbstreeTermDeclAs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeTermDeclIs (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrAbstreeTermDeclIs (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrAbstreeTermDeclIs _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeTermDeclIs;
#else
_currn = (_TPPrAbstreeTermDeclIs) TreeNodeAlloc (sizeof (struct _TPrAbstreeTermDeclIs));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeTermDeclIs;
_currn->_desc1 = (_TSPTreeSymbol_DefId) MkTreeSymbol_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTermDeclIs: root of subtree no. 1 can not be made a TreeSymbol_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTermDeclIs: root of subtree no. 2 can not be made a Simple_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeTermDeclIs;
return ( (NODEPTR) _currn);
}/* MkrAbstreeTermDeclIs */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeTermDeclName (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeTermDeclName (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeTermDeclName _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeTermDeclName;
#else
_currn = (_TPPrAbstreeTermDeclName) TreeNodeAlloc (sizeof (struct _TPrAbstreeTermDeclName));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeTermDeclName;
_currn->_desc1 = (_TSPTreeSymbol_DefId) MkTreeSymbol_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeTermDeclName: root of subtree no. 1 can not be made a TreeSymbol_DefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeTermDeclName;
return ( (NODEPTR) _currn);
}/* MkrAbstreeTermDeclName */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrTreeSymbolDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrTreeSymbolDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrTreeSymbolDefId _currn;
#ifdef __cplusplus
_currn = new _TPrTreeSymbolDefId;
#else
_currn = (_TPPrTreeSymbolDefId) TreeNodeAlloc (sizeof (struct _TPrTreeSymbolDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErTreeSymbolDefId;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rTreeSymbolDefId: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rTreeSymbolDefId;
return ( (NODEPTR) _currn);
}/* MkrTreeSymbolDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConversionReferenceTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrConversionReferenceTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrConversionReferenceTypename _currn;
#ifdef __cplusplus
_currn = new _TPrConversionReferenceTypename;
#else
_currn = (_TPPrConversionReferenceTypename) TreeNodeAlloc (sizeof (struct _TPrConversionReferenceTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConversionReferenceTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rConversionReferenceTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rConversionReferenceTypename;
return ( (NODEPTR) _currn);
}/* MkrConversionReferenceTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConversionReferenceIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrConversionReferenceIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrConversionReferenceIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrConversionReferenceIdentifier;
#else
_currn = (_TPPrConversionReferenceIdentifier) TreeNodeAlloc (sizeof (struct _TPrConversionReferenceIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConversionReferenceIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rConversionReferenceIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rConversionReferenceIdentifier;
return ( (NODEPTR) _currn);
}/* MkrConversionReferenceIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrConversionReferenceNone (POSITION *_coordref)
#else
NODEPTR MkrConversionReferenceNone (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrConversionReferenceNone _currn;
#ifdef __cplusplus
_currn = new _TPrConversionReferenceNone;
#else
_currn = (_TPPrConversionReferenceNone) TreeNodeAlloc (sizeof (struct _TPrConversionReferenceNone));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErConversionReferenceNone;
_SETCOORD(_currn)
_TERMACT_rConversionReferenceNone;
return ( (NODEPTR) _currn);
}/* MkrConversionReferenceNone */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeRuleIsProduction (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrAbstreeRuleIsProduction (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrAbstreeRuleIsProduction _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeRuleIsProduction;
#else
_currn = (_TPPrAbstreeRuleIsProduction) TreeNodeAlloc (sizeof (struct _TPrAbstreeRuleIsProduction));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeRuleIsProduction;
_currn->_desc1 = (_TSPAbstree_Production) MkAbstree_Production (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeRuleIsProduction: root of subtree no. 1 can not be made a Abstree_Production node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rAbstreeRuleIsProduction;
return ( (NODEPTR) _currn);
}/* MkrAbstreeRuleIsProduction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAbstreeProduction (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrAbstreeProduction (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrAbstreeProduction _currn;
#ifdef __cplusplus
_currn = new _TPrAbstreeProduction;
#else
_currn = (_TPPrAbstreeProduction) TreeNodeAlloc (sizeof (struct _TPrAbstreeProduction));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAbstreeProduction;
_currn->_desc1 = (_TSPTreeSymbol_DefId) MkTreeSymbol_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeProduction: root of subtree no. 1 can not be made a TreeSymbol_DefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbols) MkSymbols (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeProduction: root of subtree no. 2 can not be made a Symbols node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_Rulename) MkOpt_Rulename (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rAbstreeProduction: root of subtree no. 3 can not be made a Opt_Rulename node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rAbstreeProduction;
return ( (NODEPTR) _currn);
}/* MkrAbstreeProduction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolList2;
#else
_currn = (_TPPrSymbolList2) TreeNodeAlloc (sizeof (struct _TPrSymbolList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolList2;
_currn->_desc1 = (_TSPSymbols) MkSymbols (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolList2: root of subtree no. 1 can not be made a Symbols node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol) MkSymbol (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolList2: root of subtree no. 2 can not be made a Symbol node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoSymbols (POSITION *_coordref)
#else
NODEPTR MkrNoSymbols (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoSymbols _currn;
#ifdef __cplusplus
_currn = new _TPrNoSymbols;
#else
_currn = (_TPPrNoSymbols) TreeNodeAlloc (sizeof (struct _TPrNoSymbols));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoSymbols;
_SETCOORD(_currn)
_TERMACT_rNoSymbols;
return ( (NODEPTR) _currn);
}/* MkrNoSymbols */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolIsLiteral (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolIsLiteral (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolIsLiteral _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolIsLiteral;
#else
_currn = (_TPPrSymbolIsLiteral) TreeNodeAlloc (sizeof (struct _TPrSymbolIsLiteral));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolIsLiteral;
_currn->_desc1 = (_TSPLidoLiteral) MkLidoLiteral (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolIsLiteral: root of subtree no. 1 can not be made a LidoLiteral node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolIsLiteral;
return ( (NODEPTR) _currn);
}/* MkrSymbolIsLiteral */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolIsCreation (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolIsCreation (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolIsCreation _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolIsCreation;
#else
_currn = (_TPPrSymbolIsCreation) TreeNodeAlloc (sizeof (struct _TPrSymbolIsCreation));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolIsCreation;
_currn->_desc1 = (_TSPSymbol_Reference_OptTree) MkSymbol_Reference_OptTree (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolIsCreation: root of subtree no. 1 can not be made a Symbol_Reference_OptTree node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolIsCreation;
return ( (NODEPTR) _currn);
}/* MkrSymbolIsCreation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolIsReference (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolIsReference (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolIsReference _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolIsReference;
#else
_currn = (_TPPrSymbolIsReference) TreeNodeAlloc (sizeof (struct _TPrSymbolIsReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolIsReference;
_currn->_desc1 = (_TSPSymbol_Reference_OptTree) MkSymbol_Reference_OptTree (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolIsReference: root of subtree no. 1 can not be made a Symbol_Reference_OptTree node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolIsReference;
return ( (NODEPTR) _currn);
}/* MkrSymbolIsReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolReferenceOptTree (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolReferenceOptTree (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolReferenceOptTree _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolReferenceOptTree;
#else
_currn = (_TPPrSymbolReferenceOptTree) TreeNodeAlloc (sizeof (struct _TPrSymbolReferenceOptTree));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolReferenceOptTree;
_currn->_desc1 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolReferenceOptTree: root of subtree no. 1 can not be made a Symbol_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_TreeHint) MkOpt_TreeHint (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolReferenceOptTree: root of subtree no. 2 can not be made a Opt_TreeHint node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolReferenceOptTree;
return ( (NODEPTR) _currn);
}/* MkrSymbolReferenceOptTree */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolReference (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolReference (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolReference _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolReference;
#else
_currn = (_TPPrSymbolReference) TreeNodeAlloc (sizeof (struct _TPrSymbolReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolReference;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolReference: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolReference;
return ( (NODEPTR) _currn);
}/* MkrSymbolReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoTreeHint (POSITION *_coordref)
#else
NODEPTR MkrNoTreeHint (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoTreeHint _currn;
#ifdef __cplusplus
_currn = new _TPrNoTreeHint;
#else
_currn = (_TPPrNoTreeHint) TreeNodeAlloc (sizeof (struct _TPrNoTreeHint));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoTreeHint;
_SETCOORD(_currn)
_TERMACT_rNoTreeHint;
return ( (NODEPTR) _currn);
}/* MkrNoTreeHint */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrOptTreeHint (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrOptTreeHint (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrOptTreeHint _currn;
#ifdef __cplusplus
_currn = new _TPrOptTreeHint;
#else
_currn = (_TPPrOptTreeHint) TreeNodeAlloc (sizeof (struct _TPrOptTreeHint));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErOptTreeHint;
_currn->_desc1 = (_TSPAbstree_Reference) MkAbstree_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rOptTreeHint: root of subtree no. 1 can not be made a Abstree_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rOptTreeHint;
return ( (NODEPTR) _currn);
}/* MkrOptTreeHint */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulenameExists (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRulenameExists (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRulenameExists _currn;
#ifdef __cplusplus
_currn = new _TPrRulenameExists;
#else
_currn = (_TPPrRulenameExists) TreeNodeAlloc (sizeof (struct _TPrRulenameExists));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulenameExists;
_currn->_desc1 = (_TSPRulename_DefId) MkRulename_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulenameExists: root of subtree no. 1 can not be made a Rulename_DefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRulenameExists;
return ( (NODEPTR) _currn);
}/* MkrRulenameExists */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoRulename (POSITION *_coordref)
#else
NODEPTR MkrNoRulename (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoRulename _currn;
#ifdef __cplusplus
_currn = new _TPrNoRulename;
#else
_currn = (_TPPrNoRulename) TreeNodeAlloc (sizeof (struct _TPrNoRulename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoRulename;
_SETCOORD(_currn)
_TERMACT_rNoRulename;
return ( (NODEPTR) _currn);
}/* MkrNoRulename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulenameIsTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRulenameIsTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRulenameIsTypename _currn;
#ifdef __cplusplus
_currn = new _TPrRulenameIsTypename;
#else
_currn = (_TPPrRulenameIsTypename) TreeNodeAlloc (sizeof (struct _TPrRulenameIsTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulenameIsTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulenameIsTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRulenameIsTypename;
return ( (NODEPTR) _currn);
}/* MkrRulenameIsTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulenameIsIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRulenameIsIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRulenameIsIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrRulenameIsIdentifier;
#else
_currn = (_TPPrRulenameIsIdentifier) TreeNodeAlloc (sizeof (struct _TPrRulenameIsIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulenameIsIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulenameIsIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRulenameIsIdentifier;
return ( (NODEPTR) _currn);
}/* MkrRulenameIsIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclGlobalAttribute (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclGlobalAttribute (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclGlobalAttribute _currn;
#ifdef __cplusplus
_currn = new _TPrDeclGlobalAttribute;
#else
_currn = (_TPPrDeclGlobalAttribute) TreeNodeAlloc (sizeof (struct _TPrDeclGlobalAttribute));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclGlobalAttribute;
_currn->_desc1 = (_TSPGlobal_AttributeDecl) MkGlobal_AttributeDecl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclGlobalAttribute: root of subtree no. 1 can not be made a Global_AttributeDecl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclGlobalAttribute;
return ( (NODEPTR) _currn);
}/* MkrDeclGlobalAttribute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrGlobalAttributeDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrGlobalAttributeDecl (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrGlobalAttributeDecl _currn;
#ifdef __cplusplus
_currn = new _TPrGlobalAttributeDecl;
#else
_currn = (_TPPrGlobalAttributeDecl) TreeNodeAlloc (sizeof (struct _TPrGlobalAttributeDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErGlobalAttributeDecl;
_currn->_desc1 = (_TSPAttributeClass) MkAttributeClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rGlobalAttributeDecl: root of subtree no. 1 can not be made a AttributeClass node ", 0, _coordref);
_currn->_desc2 = (_TSPGlobal_AttributeDefIds) MkGlobal_AttributeDefIds (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rGlobalAttributeDecl: root of subtree no. 2 can not be made a Global_AttributeDefIds node ", 0, _coordref);
_currn->_desc3 = (_TSPOptionalType) MkOptionalType (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rGlobalAttributeDecl: root of subtree no. 3 can not be made a OptionalType node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rGlobalAttributeDecl;
return ( (NODEPTR) _currn);
}/* MkrGlobalAttributeDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAttributeClassInherited (POSITION *_coordref)
#else
NODEPTR MkrAttributeClassInherited (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrAttributeClassInherited _currn;
#ifdef __cplusplus
_currn = new _TPrAttributeClassInherited;
#else
_currn = (_TPPrAttributeClassInherited) TreeNodeAlloc (sizeof (struct _TPrAttributeClassInherited));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAttributeClassInherited;
_SETCOORD(_currn)
_TERMACT_rAttributeClassInherited;
return ( (NODEPTR) _currn);
}/* MkrAttributeClassInherited */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAttributeClassSynthesized (POSITION *_coordref)
#else
NODEPTR MkrAttributeClassSynthesized (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrAttributeClassSynthesized _currn;
#ifdef __cplusplus
_currn = new _TPrAttributeClassSynthesized;
#else
_currn = (_TPPrAttributeClassSynthesized) TreeNodeAlloc (sizeof (struct _TPrAttributeClassSynthesized));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAttributeClassSynthesized;
_SETCOORD(_currn)
_TERMACT_rAttributeClassSynthesized;
return ( (NODEPTR) _currn);
}/* MkrAttributeClassSynthesized */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAttributeClassThread (POSITION *_coordref)
#else
NODEPTR MkrAttributeClassThread (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrAttributeClassThread _currn;
#ifdef __cplusplus
_currn = new _TPrAttributeClassThread;
#else
_currn = (_TPPrAttributeClassThread) TreeNodeAlloc (sizeof (struct _TPrAttributeClassThread));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAttributeClassThread;
_SETCOORD(_currn)
_TERMACT_rAttributeClassThread;
return ( (NODEPTR) _currn);
}/* MkrAttributeClassThread */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAttributeClassChain (POSITION *_coordref)
#else
NODEPTR MkrAttributeClassChain (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrAttributeClassChain _currn;
#ifdef __cplusplus
_currn = new _TPrAttributeClassChain;
#else
_currn = (_TPPrAttributeClassChain) TreeNodeAlloc (sizeof (struct _TPrAttributeClassChain));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAttributeClassChain;
_SETCOORD(_currn)
_TERMACT_rAttributeClassChain;
return ( (NODEPTR) _currn);
}/* MkrAttributeClassChain */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrAttributeClassInfer (POSITION *_coordref)
#else
NODEPTR MkrAttributeClassInfer (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrAttributeClassInfer _currn;
#ifdef __cplusplus
_currn = new _TPrAttributeClassInfer;
#else
_currn = (_TPPrAttributeClassInfer) TreeNodeAlloc (sizeof (struct _TPrAttributeClassInfer));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErAttributeClassInfer;
_SETCOORD(_currn)
_TERMACT_rAttributeClassInfer;
return ( (NODEPTR) _currn);
}/* MkrAttributeClassInfer */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrGlobalAttributeDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrGlobalAttributeDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrGlobalAttributeDefId _currn;
#ifdef __cplusplus
_currn = new _TPrGlobalAttributeDefId;
#else
_currn = (_TPPrGlobalAttributeDefId) TreeNodeAlloc (sizeof (struct _TPrGlobalAttributeDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErGlobalAttributeDefId;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rGlobalAttributeDefId: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rGlobalAttributeDefId;
return ( (NODEPTR) _currn);
}/* MkrGlobalAttributeDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrGlobalAttributeDefIdList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrGlobalAttributeDefIdList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrGlobalAttributeDefIdList2 _currn;
#ifdef __cplusplus
_currn = new _TPrGlobalAttributeDefIdList2;
#else
_currn = (_TPPrGlobalAttributeDefIdList2) TreeNodeAlloc (sizeof (struct _TPrGlobalAttributeDefIdList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErGlobalAttributeDefIdList2;
_currn->_desc1 = (_TSPGlobal_AttributeDefIds) MkGlobal_AttributeDefIds (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rGlobalAttributeDefIdList2: root of subtree no. 1 can not be made a Global_AttributeDefIds node ", 0, _coordref);
_currn->_desc2 = (_TSPGlobal_AttributeDefId) MkGlobal_AttributeDefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rGlobalAttributeDefIdList2: root of subtree no. 2 can not be made a Global_AttributeDefId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rGlobalAttributeDefIdList2;
return ( (NODEPTR) _currn);
}/* MkrGlobalAttributeDefIdList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrGlobalAttributeDefIdList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrGlobalAttributeDefIdList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrGlobalAttributeDefIdList1 _currn;
#ifdef __cplusplus
_currn = new _TPrGlobalAttributeDefIdList1;
#else
_currn = (_TPPrGlobalAttributeDefIdList1) TreeNodeAlloc (sizeof (struct _TPrGlobalAttributeDefIdList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErGlobalAttributeDefIdList1;
_currn->_desc1 = (_TSPGlobal_AttributeDefId) MkGlobal_AttributeDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rGlobalAttributeDefIdList1: root of subtree no. 1 can not be made a Global_AttributeDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rGlobalAttributeDefIdList1;
return ( (NODEPTR) _currn);
}/* MkrGlobalAttributeDefIdList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclSymbol (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclSymbol (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclSymbol _currn;
#ifdef __cplusplus
_currn = new _TPrDeclSymbol;
#else
_currn = (_TPPrDeclSymbol) TreeNodeAlloc (sizeof (struct _TPrDeclSymbol));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclSymbol;
_currn->_desc1 = (_TSPSymbol_Decl) MkSymbol_Decl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclSymbol: root of subtree no. 1 can not be made a Symbol_Decl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclSymbol;
return ( (NODEPTR) _currn);
}/* MkrDeclSymbol */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3, NODEPTR _desc4, NODEPTR _desc5)
#else
NODEPTR MkrSymbolDecl (_coordref,_desc1,_desc2,_desc3,_desc4,_desc5)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
	NODEPTR _desc4;
	NODEPTR _desc5;
#endif
{	_TPPrSymbolDecl _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolDecl;
#else
_currn = (_TPPrSymbolDecl) TreeNodeAlloc (sizeof (struct _TPrSymbolDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolDecl;
_currn->_desc1 = (_TSPSymbol_Classification) MkSymbol_Classification (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolDecl: root of subtree no. 1 can not be made a Symbol_Classification node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_DefId) MkSymbol_DefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolDecl: root of subtree no. 2 can not be made a Symbol_DefId node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_TreeHint) MkOpt_TreeHint (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolDecl: root of subtree no. 3 can not be made a Opt_TreeHint node ", 0, _coordref);
_currn->_desc4 = (_TSPSymbol_Options) MkSymbol_Options (_coordref, _desc4);	
if (((NODEPTR)_currn->_desc4) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolDecl: root of subtree no. 4 can not be made a Symbol_Options node ", 0, _coordref);
_currn->_desc5 = (_TSPOpt_Symbol_Computations) MkOpt_Symbol_Computations (_coordref, _desc5);	
if (((NODEPTR)_currn->_desc5) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolDecl: root of subtree no. 5 can not be made a Opt_Symbol_Computations node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolDecl;
return ( (NODEPTR) _currn);
}/* MkrSymbolDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolClassificationSymbol (POSITION *_coordref)
#else
NODEPTR MkrSymbolClassificationSymbol (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrSymbolClassificationSymbol _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolClassificationSymbol;
#else
_currn = (_TPPrSymbolClassificationSymbol) TreeNodeAlloc (sizeof (struct _TPrSymbolClassificationSymbol));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolClassificationSymbol;
_SETCOORD(_currn)
_TERMACT_rSymbolClassificationSymbol;
return ( (NODEPTR) _currn);
}/* MkrSymbolClassificationSymbol */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolClassificationClassSymbol (POSITION *_coordref)
#else
NODEPTR MkrSymbolClassificationClassSymbol (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrSymbolClassificationClassSymbol _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolClassificationClassSymbol;
#else
_currn = (_TPPrSymbolClassificationClassSymbol) TreeNodeAlloc (sizeof (struct _TPrSymbolClassificationClassSymbol));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolClassificationClassSymbol;
_SETCOORD(_currn)
_TERMACT_rSymbolClassificationClassSymbol;
return ( (NODEPTR) _currn);
}/* MkrSymbolClassificationClassSymbol */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOptionList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolOptionList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolOptionList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOptionList2;
#else
_currn = (_TPPrSymbolOptionList2) TreeNodeAlloc (sizeof (struct _TPrSymbolOptionList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOptionList2;
_currn->_desc1 = (_TSPSymbol_Options) MkSymbol_Options (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOptionList2: root of subtree no. 1 can not be made a Symbol_Options node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Option) MkSymbol_Option (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOptionList2: root of subtree no. 2 can not be made a Symbol_Option node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolOptionList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolOptionList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoSymbolOptions (POSITION *_coordref)
#else
NODEPTR MkrNoSymbolOptions (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoSymbolOptions _currn;
#ifdef __cplusplus
_currn = new _TPrNoSymbolOptions;
#else
_currn = (_TPPrNoSymbolOptions) TreeNodeAlloc (sizeof (struct _TPrNoSymbolOptions));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoSymbolOptions;
_SETCOORD(_currn)
_TERMACT_rNoSymbolOptions;
return ( (NODEPTR) _currn);
}/* MkrNoSymbolOptions */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOptionAttributes (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolOptionAttributes (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolOptionAttributes _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOptionAttributes;
#else
_currn = (_TPPrSymbolOptionAttributes) TreeNodeAlloc (sizeof (struct _TPrSymbolOptionAttributes));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOptionAttributes;
_currn->_desc1 = (_TSPSymbol_LocalAttributes) MkSymbol_LocalAttributes (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOptionAttributes: root of subtree no. 1 can not be made a Symbol_LocalAttributes node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolOptionAttributes;
return ( (NODEPTR) _currn);
}/* MkrSymbolOptionAttributes */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOptionInheritance (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolOptionInheritance (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolOptionInheritance _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOptionInheritance;
#else
_currn = (_TPPrSymbolOptionInheritance) TreeNodeAlloc (sizeof (struct _TPrSymbolOptionInheritance));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOptionInheritance;
_currn->_desc1 = (_TSPSymbol_Inheritance) MkSymbol_Inheritance (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOptionInheritance: root of subtree no. 1 can not be made a Symbol_Inheritance node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolOptionInheritance;
return ( (NODEPTR) _currn);
}/* MkrSymbolOptionInheritance */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolInheritance (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolInheritance (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolInheritance _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolInheritance;
#else
_currn = (_TPPrSymbolInheritance) TreeNodeAlloc (sizeof (struct _TPrSymbolInheritance));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolInheritance;
_currn->_desc1 = (_TSPSymbol_Reference_OptTree_List) MkSymbol_Reference_OptTree_List (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolInheritance: root of subtree no. 1 can not be made a Symbol_Reference_OptTree_List node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolInheritance;
return ( (NODEPTR) _currn);
}/* MkrSymbolInheritance */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolReferenceOptTreeList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolReferenceOptTreeList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolReferenceOptTreeList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolReferenceOptTreeList2;
#else
_currn = (_TPPrSymbolReferenceOptTreeList2) TreeNodeAlloc (sizeof (struct _TPrSymbolReferenceOptTreeList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolReferenceOptTreeList2;
_currn->_desc1 = (_TSPSymbol_Reference_OptTree_List) MkSymbol_Reference_OptTree_List (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolReferenceOptTreeList2: root of subtree no. 1 can not be made a Symbol_Reference_OptTree_List node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Reference_OptTree) MkSymbol_Reference_OptTree (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolReferenceOptTreeList2: root of subtree no. 2 can not be made a Symbol_Reference_OptTree node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolReferenceOptTreeList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolReferenceOptTreeList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolReferenceOptTreeList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolReferenceOptTreeList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolReferenceOptTreeList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolReferenceOptTreeList1;
#else
_currn = (_TPPrSymbolReferenceOptTreeList1) TreeNodeAlloc (sizeof (struct _TPrSymbolReferenceOptTreeList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolReferenceOptTreeList1;
_currn->_desc1 = (_TSPSymbol_Reference_OptTree) MkSymbol_Reference_OptTree (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolReferenceOptTreeList1: root of subtree no. 1 can not be made a Symbol_Reference_OptTree node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolReferenceOptTreeList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolReferenceOptTreeList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributes (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLocalAttributes (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLocalAttributes _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributes;
#else
_currn = (_TPPrSymbolLocalAttributes) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributes));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributes;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Decls) MkSymbol_LocalAttribute_Decls (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributes: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Decls node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLocalAttributes;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributes */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDeclList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLocalAttributeDeclList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLocalAttributeDeclList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDeclList2;
#else
_currn = (_TPPrSymbolLocalAttributeDeclList2) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDeclList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDeclList2;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Decls) MkSymbol_LocalAttribute_Decls (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclList2: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Decls node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_LocalAttribute_Decl) MkSymbol_LocalAttribute_Decl (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclList2: root of subtree no. 2 can not be made a Symbol_LocalAttribute_Decl node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDeclList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDeclList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDeclList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLocalAttributeDeclList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLocalAttributeDeclList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDeclList1;
#else
_currn = (_TPPrSymbolLocalAttributeDeclList1) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDeclList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDeclList1;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Decl) MkSymbol_LocalAttribute_Decl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclList1: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Decl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDeclList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDeclList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDeclSameType (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrSymbolLocalAttributeDeclSameType (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrSymbolLocalAttributeDeclSameType _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDeclSameType;
#else
_currn = (_TPPrSymbolLocalAttributeDeclSameType) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDeclSameType));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDeclSameType;
_currn->_desc1 = (_TSPAttributeClass) MkAttributeClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclSameType: root of subtree no. 1 can not be made a AttributeClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_LocalAttribute_Defs) MkSymbol_LocalAttribute_Defs (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclSameType: root of subtree no. 2 can not be made a Symbol_LocalAttribute_Defs node ", 0, _coordref);
_currn->_desc3 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclSameType: root of subtree no. 3 can not be made a Simple_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDeclSameType;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDeclSameType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDeclDifferentType (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLocalAttributeDeclDifferentType (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLocalAttributeDeclDifferentType _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDeclDifferentType;
#else
_currn = (_TPPrSymbolLocalAttributeDeclDifferentType) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDeclDifferentType));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDeclDifferentType;
_currn->_desc1 = (_TSPAttributeClass) MkAttributeClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclDifferentType: root of subtree no. 1 can not be made a AttributeClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_LocalAttribute_Decls_NoClass) MkSymbol_LocalAttribute_Decls_NoClass (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclDifferentType: root of subtree no. 2 can not be made a Symbol_LocalAttribute_Decls_NoClass node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDeclDifferentType;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDeclDifferentType */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDeclNoClassList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLocalAttributeDeclNoClassList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLocalAttributeDeclNoClassList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDeclNoClassList2;
#else
_currn = (_TPPrSymbolLocalAttributeDeclNoClassList2) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDeclNoClassList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDeclNoClassList2;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Decls_NoClass) MkSymbol_LocalAttribute_Decls_NoClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclNoClassList2: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Decls_NoClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_LocalAttribute_Decl_NoClass) MkSymbol_LocalAttribute_Decl_NoClass (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclNoClassList2: root of subtree no. 2 can not be made a Symbol_LocalAttribute_Decl_NoClass node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDeclNoClassList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDeclNoClassList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDeclNoClassList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLocalAttributeDeclNoClassList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLocalAttributeDeclNoClassList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDeclNoClassList1;
#else
_currn = (_TPPrSymbolLocalAttributeDeclNoClassList1) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDeclNoClassList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDeclNoClassList1;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Decl_NoClass) MkSymbol_LocalAttribute_Decl_NoClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclNoClassList1: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Decl_NoClass node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDeclNoClassList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDeclNoClassList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDeclNoClass (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLocalAttributeDeclNoClass (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLocalAttributeDeclNoClass _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDeclNoClass;
#else
_currn = (_TPPrSymbolLocalAttributeDeclNoClass) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDeclNoClass));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDeclNoClass;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Defs) MkSymbol_LocalAttribute_Defs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclNoClass: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Defs node ", 0, _coordref);
_currn->_desc2 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDeclNoClass: root of subtree no. 2 can not be made a Simple_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDeclNoClass;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDeclNoClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDefIdList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLocalAttributeDefIdList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLocalAttributeDefIdList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDefIdList2;
#else
_currn = (_TPPrSymbolLocalAttributeDefIdList2) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDefIdList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDefIdList2;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Defs) MkSymbol_LocalAttribute_Defs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDefIdList2: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Defs node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_LocalAttribute_DefId) MkSymbol_LocalAttribute_DefId (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDefIdList2: root of subtree no. 2 can not be made a Symbol_LocalAttribute_DefId node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDefIdList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDefIdList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDefIdList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLocalAttributeDefIdList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLocalAttributeDefIdList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDefIdList1;
#else
_currn = (_TPPrSymbolLocalAttributeDefIdList1) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDefIdList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDefIdList1;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_DefId) MkSymbol_LocalAttribute_DefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDefIdList1: root of subtree no. 1 can not be made a Symbol_LocalAttribute_DefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDefIdList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDefIdList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLocalAttributeDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLocalAttributeDefId _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeDefId;
#else
_currn = (_TPPrSymbolLocalAttributeDefId) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeDefId;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeDefId: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLocalAttributeDefId;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeReference (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLocalAttributeReference (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLocalAttributeReference _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeReference;
#else
_currn = (_TPPrSymbolLocalAttributeReference) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeReference;
_currn->_desc1 = (_TSPLocal_AttributeClass) MkLocal_AttributeClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeReference: root of subtree no. 1 can not be made a Local_AttributeClass node ", 0, _coordref);
_currn->_desc2 = (_TSPLocal_AttributeReference) MkLocal_AttributeReference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeReference: root of subtree no. 2 can not be made a Local_AttributeReference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLocalAttributeReference;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrLocalAttributeClassUnknown (POSITION *_coordref)
#else
NODEPTR MkrLocalAttributeClassUnknown (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrLocalAttributeClassUnknown _currn;
#ifdef __cplusplus
_currn = new _TPrLocalAttributeClassUnknown;
#else
_currn = (_TPPrLocalAttributeClassUnknown) TreeNodeAlloc (sizeof (struct _TPrLocalAttributeClassUnknown));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErLocalAttributeClassUnknown;
_SETCOORD(_currn)
_TERMACT_rLocalAttributeClassUnknown;
return ( (NODEPTR) _currn);
}/* MkrLocalAttributeClassUnknown */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrLocalAttributeClassInherited (POSITION *_coordref)
#else
NODEPTR MkrLocalAttributeClassInherited (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrLocalAttributeClassInherited _currn;
#ifdef __cplusplus
_currn = new _TPrLocalAttributeClassInherited;
#else
_currn = (_TPPrLocalAttributeClassInherited) TreeNodeAlloc (sizeof (struct _TPrLocalAttributeClassInherited));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErLocalAttributeClassInherited;
_SETCOORD(_currn)
_TERMACT_rLocalAttributeClassInherited;
return ( (NODEPTR) _currn);
}/* MkrLocalAttributeClassInherited */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrLocalAttributeClassSynthesized (POSITION *_coordref)
#else
NODEPTR MkrLocalAttributeClassSynthesized (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrLocalAttributeClassSynthesized _currn;
#ifdef __cplusplus
_currn = new _TPrLocalAttributeClassSynthesized;
#else
_currn = (_TPPrLocalAttributeClassSynthesized) TreeNodeAlloc (sizeof (struct _TPrLocalAttributeClassSynthesized));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErLocalAttributeClassSynthesized;
_SETCOORD(_currn)
_TERMACT_rLocalAttributeClassSynthesized;
return ( (NODEPTR) _currn);
}/* MkrLocalAttributeClassSynthesized */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrLocalAttributeClassTail (POSITION *_coordref)
#else
NODEPTR MkrLocalAttributeClassTail (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrLocalAttributeClassTail _currn;
#ifdef __cplusplus
_currn = new _TPrLocalAttributeClassTail;
#else
_currn = (_TPPrLocalAttributeClassTail) TreeNodeAlloc (sizeof (struct _TPrLocalAttributeClassTail));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErLocalAttributeClassTail;
_SETCOORD(_currn)
_TERMACT_rLocalAttributeClassTail;
return ( (NODEPTR) _currn);
}/* MkrLocalAttributeClassTail */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrLocalAttributeClassHead (POSITION *_coordref)
#else
NODEPTR MkrLocalAttributeClassHead (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrLocalAttributeClassHead _currn;
#ifdef __cplusplus
_currn = new _TPrLocalAttributeClassHead;
#else
_currn = (_TPPrLocalAttributeClassHead) TreeNodeAlloc (sizeof (struct _TPrLocalAttributeClassHead));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErLocalAttributeClassHead;
_SETCOORD(_currn)
_TERMACT_rLocalAttributeClassHead;
return ( (NODEPTR) _currn);
}/* MkrLocalAttributeClassHead */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrLocalAttributeReference (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrLocalAttributeReference (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrLocalAttributeReference _currn;
#ifdef __cplusplus
_currn = new _TPrLocalAttributeReference;
#else
_currn = (_TPPrLocalAttributeReference) TreeNodeAlloc (sizeof (struct _TPrLocalAttributeReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErLocalAttributeReference;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rLocalAttributeReference: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rLocalAttributeReference;
return ( (NODEPTR) _currn);
}/* MkrLocalAttributeReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoSymbolComputations (POSITION *_coordref)
#else
NODEPTR MkrNoSymbolComputations (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoSymbolComputations _currn;
#ifdef __cplusplus
_currn = new _TPrNoSymbolComputations;
#else
_currn = (_TPPrNoSymbolComputations) TreeNodeAlloc (sizeof (struct _TPrNoSymbolComputations));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoSymbolComputations;
_SETCOORD(_currn)
_TERMACT_rNoSymbolComputations;
return ( (NODEPTR) _currn);
}/* MkrNoSymbolComputations */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolComputationsExist (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolComputationsExist (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolComputationsExist _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolComputationsExist;
#else
_currn = (_TPPrSymbolComputationsExist) TreeNodeAlloc (sizeof (struct _TPrSymbolComputationsExist));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolComputationsExist;
_currn->_desc1 = (_TSPSymbol_Computations) MkSymbol_Computations (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationsExist: root of subtree no. 1 can not be made a Symbol_Computations node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolComputationsExist;
return ( (NODEPTR) _currn);
}/* MkrSymbolComputationsExist */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolComputationList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolComputationList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolComputationList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolComputationList2;
#else
_currn = (_TPPrSymbolComputationList2) TreeNodeAlloc (sizeof (struct _TPrSymbolComputationList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolComputationList2;
_currn->_desc1 = (_TSPSymbol_Computations) MkSymbol_Computations (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationList2: root of subtree no. 1 can not be made a Symbol_Computations node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Computation) MkSymbol_Computation (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationList2: root of subtree no. 2 can not be made a Symbol_Computation node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolComputationList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolComputationList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolComputationListEmpty (POSITION *_coordref)
#else
NODEPTR MkrSymbolComputationListEmpty (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrSymbolComputationListEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolComputationListEmpty;
#else
_currn = (_TPPrSymbolComputationListEmpty) TreeNodeAlloc (sizeof (struct _TPrSymbolComputationListEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolComputationListEmpty;
_SETCOORD(_currn)
_TERMACT_rSymbolComputationListEmpty;
return ( (NODEPTR) _currn);
}/* MkrSymbolComputationListEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolComputationIsAssign (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolComputationIsAssign (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolComputationIsAssign _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolComputationIsAssign;
#else
_currn = (_TPPrSymbolComputationIsAssign) TreeNodeAlloc (sizeof (struct _TPrSymbolComputationIsAssign));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolComputationIsAssign;
_currn->_desc1 = (_TSPSymbol_AssignStatement) MkSymbol_AssignStatement (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationIsAssign: root of subtree no. 1 can not be made a Symbol_AssignStatement node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_SymbolDependency) MkOpt_SymbolDependency (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationIsAssign: root of subtree no. 2 can not be made a Opt_SymbolDependency node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolComputationIsAssign;
return ( (NODEPTR) _currn);
}/* MkrSymbolComputationIsAssign */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolComputationIsChainstart (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolComputationIsChainstart (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolComputationIsChainstart _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolComputationIsChainstart;
#else
_currn = (_TPPrSymbolComputationIsChainstart) TreeNodeAlloc (sizeof (struct _TPrSymbolComputationIsChainstart));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolComputationIsChainstart;
_currn->_desc1 = (_TSPSymbol_ChainStart) MkSymbol_ChainStart (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationIsChainstart: root of subtree no. 1 can not be made a Symbol_ChainStart node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_SymbolDependency) MkOpt_SymbolDependency (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationIsChainstart: root of subtree no. 2 can not be made a Opt_SymbolDependency node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolComputationIsChainstart;
return ( (NODEPTR) _currn);
}/* MkrSymbolComputationIsChainstart */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolComputationIsExpression (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolComputationIsExpression (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolComputationIsExpression _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolComputationIsExpression;
#else
_currn = (_TPPrSymbolComputationIsExpression) TreeNodeAlloc (sizeof (struct _TPrSymbolComputationIsExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolComputationIsExpression;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationIsExpression: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_SymbolDependency) MkOpt_SymbolDependency (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationIsExpression: root of subtree no. 2 can not be made a Opt_SymbolDependency node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolComputationIsExpression;
return ( (NODEPTR) _currn);
}/* MkrSymbolComputationIsExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolComputationIsOrder (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolComputationIsOrder (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolComputationIsOrder _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolComputationIsOrder;
#else
_currn = (_TPPrSymbolComputationIsOrder) TreeNodeAlloc (sizeof (struct _TPrSymbolComputationIsOrder));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolComputationIsOrder;
_currn->_desc1 = (_TSPSymbol_OrderedComputation) MkSymbol_OrderedComputation (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolComputationIsOrder: root of subtree no. 1 can not be made a Symbol_OrderedComputation node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolComputationIsOrder;
return ( (NODEPTR) _currn);
}/* MkrSymbolComputationIsOrder */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolChainStart (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolChainStart (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolChainStart _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolChainStart;
#else
_currn = (_TPPrSymbolChainStart) TreeNodeAlloc (sizeof (struct _TPrSymbolChainStart));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolChainStart;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Reference) MkSymbol_LocalAttribute_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolChainStart: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolChainStart: root of subtree no. 2 can not be made a Symbol_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolChainStart;
return ( (NODEPTR) _currn);
}/* MkrSymbolChainStart */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoSymbolDependency (POSITION *_coordref)
#else
NODEPTR MkrNoSymbolDependency (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoSymbolDependency _currn;
#ifdef __cplusplus
_currn = new _TPrNoSymbolDependency;
#else
_currn = (_TPPrNoSymbolDependency) TreeNodeAlloc (sizeof (struct _TPrNoSymbolDependency));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoSymbolDependency;
_SETCOORD(_currn)
_TERMACT_rNoSymbolDependency;
return ( (NODEPTR) _currn);
}/* MkrNoSymbolDependency */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolDependency (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolDependency (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolDependency _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolDependency;
#else
_currn = (_TPPrSymbolDependency) TreeNodeAlloc (sizeof (struct _TPrSymbolDependency));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolDependency;
_currn->_desc1 = (_TSPSymbol_Attribute_References) MkSymbol_Attribute_References (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolDependency: root of subtree no. 1 can not be made a Symbol_Attribute_References node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolDependency;
return ( (NODEPTR) _currn);
}/* MkrSymbolDependency */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolSymbolAttributeReferencesSingle (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolSymbolAttributeReferencesSingle (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolSymbolAttributeReferencesSingle _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolSymbolAttributeReferencesSingle;
#else
_currn = (_TPPrSymbolSymbolAttributeReferencesSingle) TreeNodeAlloc (sizeof (struct _TPrSymbolSymbolAttributeReferencesSingle));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolSymbolAttributeReferencesSingle;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Ref) MkSymbol_LocalAttribute_Ref (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolSymbolAttributeReferencesSingle: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Ref node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolSymbolAttributeReferencesSingle;
return ( (NODEPTR) _currn);
}/* MkrSymbolSymbolAttributeReferencesSingle */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolSymbolAttributeReferencesMulti (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolSymbolAttributeReferencesMulti (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolSymbolAttributeReferencesMulti _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolSymbolAttributeReferencesMulti;
#else
_currn = (_TPPrSymbolSymbolAttributeReferencesMulti) TreeNodeAlloc (sizeof (struct _TPrSymbolSymbolAttributeReferencesMulti));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolSymbolAttributeReferencesMulti;
_currn->_desc1 = (_TSPSymbol_Attribute_ReferenceList) MkSymbol_Attribute_ReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolSymbolAttributeReferencesMulti: root of subtree no. 1 can not be made a Symbol_Attribute_ReferenceList node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolSymbolAttributeReferencesMulti;
return ( (NODEPTR) _currn);
}/* MkrSymbolSymbolAttributeReferencesMulti */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeReferenceIsRemote (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLocalAttributeReferenceIsRemote (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLocalAttributeReferenceIsRemote _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeReferenceIsRemote;
#else
_currn = (_TPPrSymbolLocalAttributeReferenceIsRemote) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeReferenceIsRemote));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeReferenceIsRemote;
_currn->_desc1 = (_TSPRemote_Attribute) MkRemote_Attribute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeReferenceIsRemote: root of subtree no. 1 can not be made a Remote_Attribute node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLocalAttributeReferenceIsRemote;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeReferenceIsRemote */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeReferenceIsLocal (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLocalAttributeReferenceIsLocal (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLocalAttributeReferenceIsLocal _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeReferenceIsLocal;
#else
_currn = (_TPPrSymbolLocalAttributeReferenceIsLocal) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeReferenceIsLocal));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeReferenceIsLocal;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Reference) MkSymbol_LocalAttribute_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeReferenceIsLocal: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLocalAttributeReferenceIsLocal;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeReferenceIsLocal */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeRefList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLocalAttributeRefList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLocalAttributeRefList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeRefList2;
#else
_currn = (_TPPrSymbolLocalAttributeRefList2) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeRefList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeRefList2;
_currn->_desc1 = (_TSPSymbol_Attribute_ReferenceList) MkSymbol_Attribute_ReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeRefList2: root of subtree no. 1 can not be made a Symbol_Attribute_ReferenceList node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_LocalAttribute_Ref) MkSymbol_LocalAttribute_Ref (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeRefList2: root of subtree no. 2 can not be made a Symbol_LocalAttribute_Ref node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLocalAttributeRefList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeRefList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeRefList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLocalAttributeRefList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLocalAttributeRefList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeRefList1;
#else
_currn = (_TPPrSymbolLocalAttributeRefList1) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeRefList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeRefList1;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Ref) MkSymbol_LocalAttribute_Ref (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeRefList1: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Ref node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLocalAttributeRefList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeRefList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolDefOcc (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolDefOcc (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolDefOcc _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolDefOcc;
#else
_currn = (_TPPrSymbolDefOcc) TreeNodeAlloc (sizeof (struct _TPrSymbolDefOcc));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolDefOcc;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Reference) MkSymbol_LocalAttribute_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolDefOcc: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_RHS_Expression) MkSymbol_RHS_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolDefOcc: root of subtree no. 2 can not be made a Symbol_RHS_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolDefOcc;
return ( (NODEPTR) _currn);
}/* MkrSymbolDefOcc */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOrderedComputation (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolOrderedComputation (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolOrderedComputation _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOrderedComputation;
#else
_currn = (_TPPrSymbolOrderedComputation) TreeNodeAlloc (sizeof (struct _TPrSymbolOrderedComputation));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOrderedComputation;
_currn->_desc1 = (_TSPSymbol_OrderedStatements) MkSymbol_OrderedStatements (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOrderedComputation: root of subtree no. 1 can not be made a Symbol_OrderedStatements node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolOrderedComputation;
return ( (NODEPTR) _currn);
}/* MkrSymbolOrderedComputation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOrderedStatementList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolOrderedStatementList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolOrderedStatementList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOrderedStatementList2;
#else
_currn = (_TPPrSymbolOrderedStatementList2) TreeNodeAlloc (sizeof (struct _TPrSymbolOrderedStatementList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOrderedStatementList2;
_currn->_desc1 = (_TSPSymbol_OrderedStatements) MkSymbol_OrderedStatements (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOrderedStatementList2: root of subtree no. 1 can not be made a Symbol_OrderedStatements node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_OrderedStatement) MkSymbol_OrderedStatement (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOrderedStatementList2: root of subtree no. 2 can not be made a Symbol_OrderedStatement node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolOrderedStatementList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolOrderedStatementList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOrderedStatementList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolOrderedStatementList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolOrderedStatementList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOrderedStatementList1;
#else
_currn = (_TPPrSymbolOrderedStatementList1) TreeNodeAlloc (sizeof (struct _TPrSymbolOrderedStatementList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOrderedStatementList1;
_currn->_desc1 = (_TSPSymbol_OrderedStatement) MkSymbol_OrderedStatement (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOrderedStatementList1: root of subtree no. 1 can not be made a Symbol_OrderedStatement node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolOrderedStatementList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolOrderedStatementList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOrderedStatementIsExpression (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolOrderedStatementIsExpression (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolOrderedStatementIsExpression _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOrderedStatementIsExpression;
#else
_currn = (_TPPrSymbolOrderedStatementIsExpression) TreeNodeAlloc (sizeof (struct _TPrSymbolOrderedStatementIsExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOrderedStatementIsExpression;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOrderedStatementIsExpression: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolOrderedStatementIsExpression;
return ( (NODEPTR) _currn);
}/* MkrSymbolOrderedStatementIsExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOrderedStatementIsReturn (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolOrderedStatementIsReturn (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolOrderedStatementIsReturn _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOrderedStatementIsReturn;
#else
_currn = (_TPPrSymbolOrderedStatementIsReturn) TreeNodeAlloc (sizeof (struct _TPrSymbolOrderedStatementIsReturn));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOrderedStatementIsReturn;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOrderedStatementIsReturn: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolOrderedStatementIsReturn;
return ( (NODEPTR) _currn);
}/* MkrSymbolOrderedStatementIsReturn */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOrderedStatementIsAssign (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolOrderedStatementIsAssign (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolOrderedStatementIsAssign _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOrderedStatementIsAssign;
#else
_currn = (_TPPrSymbolOrderedStatementIsAssign) TreeNodeAlloc (sizeof (struct _TPrSymbolOrderedStatementIsAssign));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOrderedStatementIsAssign;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Reference) MkSymbol_LocalAttribute_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOrderedStatementIsAssign: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_RHS_Expression) MkSymbol_RHS_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOrderedStatementIsAssign: root of subtree no. 2 can not be made a Symbol_RHS_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolOrderedStatementIsAssign;
return ( (NODEPTR) _currn);
}/* MkrSymbolOrderedStatementIsAssign */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolRHSExpressionIsCompute (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolRHSExpressionIsCompute (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolRHSExpressionIsCompute _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolRHSExpressionIsCompute;
#else
_currn = (_TPPrSymbolRHSExpressionIsCompute) TreeNodeAlloc (sizeof (struct _TPrSymbolRHSExpressionIsCompute));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolRHSExpressionIsCompute;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolRHSExpressionIsCompute: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolRHSExpressionIsCompute;
return ( (NODEPTR) _currn);
}/* MkrSymbolRHSExpressionIsCompute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolRHSExpressionIsGuards (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolRHSExpressionIsGuards (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolRHSExpressionIsGuards _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolRHSExpressionIsGuards;
#else
_currn = (_TPPrSymbolRHSExpressionIsGuards) TreeNodeAlloc (sizeof (struct _TPrSymbolRHSExpressionIsGuards));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolRHSExpressionIsGuards;
_currn->_desc1 = (_TSPSymbol_Guard_Expressions) MkSymbol_Guard_Expressions (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolRHSExpressionIsGuards: root of subtree no. 1 can not be made a Symbol_Guard_Expressions node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolRHSExpressionIsGuards;
return ( (NODEPTR) _currn);
}/* MkrSymbolRHSExpressionIsGuards */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolGuardExpressionList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolGuardExpressionList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolGuardExpressionList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolGuardExpressionList2;
#else
_currn = (_TPPrSymbolGuardExpressionList2) TreeNodeAlloc (sizeof (struct _TPrSymbolGuardExpressionList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolGuardExpressionList2;
_currn->_desc1 = (_TSPSymbol_Guard_Expressions) MkSymbol_Guard_Expressions (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolGuardExpressionList2: root of subtree no. 1 can not be made a Symbol_Guard_Expressions node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Guard_Expression) MkSymbol_Guard_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolGuardExpressionList2: root of subtree no. 2 can not be made a Symbol_Guard_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolGuardExpressionList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolGuardExpressionList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolGuardExpressionList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolGuardExpressionList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolGuardExpressionList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolGuardExpressionList1;
#else
_currn = (_TPPrSymbolGuardExpressionList1) TreeNodeAlloc (sizeof (struct _TPrSymbolGuardExpressionList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolGuardExpressionList1;
_currn->_desc1 = (_TSPSymbol_Guard_Expression) MkSymbol_Guard_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolGuardExpressionList1: root of subtree no. 1 can not be made a Symbol_Guard_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolGuardExpressionList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolGuardExpressionList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolGuardExpressionIsGuard (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolGuardExpressionIsGuard (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolGuardExpressionIsGuard _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolGuardExpressionIsGuard;
#else
_currn = (_TPPrSymbolGuardExpressionIsGuard) TreeNodeAlloc (sizeof (struct _TPrSymbolGuardExpressionIsGuard));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolGuardExpressionIsGuard;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolGuardExpressionIsGuard: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Guard_Expressions) MkSymbol_Guard_Expressions (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolGuardExpressionIsGuard: root of subtree no. 2 can not be made a Symbol_Guard_Expressions node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolGuardExpressionIsGuard;
return ( (NODEPTR) _currn);
}/* MkrSymbolGuardExpressionIsGuard */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolGuardExpressionIsExpression (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolGuardExpressionIsExpression (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolGuardExpressionIsExpression _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolGuardExpressionIsExpression;
#else
_currn = (_TPPrSymbolGuardExpressionIsExpression) TreeNodeAlloc (sizeof (struct _TPrSymbolGuardExpressionIsExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolGuardExpressionIsExpression;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolGuardExpressionIsExpression: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolGuardExpressionIsExpression: root of subtree no. 2 can not be made a Symbol_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolGuardExpressionIsExpression;
return ( (NODEPTR) _currn);
}/* MkrSymbolGuardExpressionIsExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolGuardExpressionIsDefault (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolGuardExpressionIsDefault (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolGuardExpressionIsDefault _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolGuardExpressionIsDefault;
#else
_currn = (_TPPrSymbolGuardExpressionIsDefault) TreeNodeAlloc (sizeof (struct _TPrSymbolGuardExpressionIsDefault));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolGuardExpressionIsDefault;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolGuardExpressionIsDefault: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolGuardExpressionIsDefault;
return ( (NODEPTR) _currn);
}/* MkrSymbolGuardExpressionIsDefault */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionChain (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionChain (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionChain _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionChain;
#else
_currn = (_TPPrSymbolExpressionChain) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionChain));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionChain;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionChain: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionChain;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionChain */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionIsWhen (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionIsWhen (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionIsWhen _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionIsWhen;
#else
_currn = (_TPPrSymbolExpressionIsWhen) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionIsWhen));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionIsWhen;
_currn->_desc1 = (_TSPSymbol_Expression_When) MkSymbol_Expression_When (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIsWhen: root of subtree no. 1 can not be made a Symbol_Expression_When node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionIsWhen;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionIsWhen */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionIsError (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionIsError (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionIsError _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionIsError;
#else
_currn = (_TPPrSymbolExpressionIsError) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionIsError));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionIsError;
_currn->_desc1 = (_TSPSymbol_Expression_Error) MkSymbol_Expression_Error (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIsError: root of subtree no. 1 can not be made a Symbol_Expression_Error node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionIsError;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionIsError */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionIsIf (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionIsIf (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionIsIf _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionIsIf;
#else
_currn = (_TPPrSymbolExpressionIsIf) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionIsIf));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionIsIf;
_currn->_desc1 = (_TSPSymbol_Expression_If) MkSymbol_Expression_If (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIsIf: root of subtree no. 1 can not be made a Symbol_Expression_If node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionIsIf;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionIsIf */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionIsLet (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionIsLet (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionIsLet _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionIsLet;
#else
_currn = (_TPPrSymbolExpressionIsLet) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionIsLet));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionIsLet;
_currn->_desc1 = (_TSPSymbol_Expression_Let) MkSymbol_Expression_Let (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIsLet: root of subtree no. 1 can not be made a Symbol_Expression_Let node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionIsLet;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionIsLet */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionIsLambda (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionIsLambda (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionIsLambda _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionIsLambda;
#else
_currn = (_TPPrSymbolExpressionIsLambda) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionIsLambda));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionIsLambda;
_currn->_desc1 = (_TSPSymbol_Lambda) MkSymbol_Lambda (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIsLambda: root of subtree no. 1 can not be made a Symbol_Lambda node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionIsLambda;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionIsLambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionIsOrder (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionIsOrder (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionIsOrder _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionIsOrder;
#else
_currn = (_TPPrSymbolExpressionIsOrder) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionIsOrder));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionIsOrder;
_currn->_desc1 = (_TSPSymbol_OrderedComputation) MkSymbol_OrderedComputation (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIsOrder: root of subtree no. 1 can not be made a Symbol_OrderedComputation node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionIsOrder;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionIsOrder */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLambda (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrSymbolLambda (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrSymbolLambda _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLambda;
#else
_currn = (_TPPrSymbolLambda) TreeNodeAlloc (sizeof (struct _TPrSymbolLambda));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLambda;
_currn->_desc1 = (_TSPLambda) MkLambda (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLambda: root of subtree no. 1 can not be made a Lambda node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Lambda_VarDefs) MkSymbol_Lambda_VarDefs (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLambda: root of subtree no. 2 can not be made a Symbol_Lambda_VarDefs node ", 0, _coordref);
_currn->_desc3 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLambda: root of subtree no. 3 can not be made a Symbol_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLambda;
return ( (NODEPTR) _currn);
}/* MkrSymbolLambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLambdaVarDefList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLambdaVarDefList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLambdaVarDefList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLambdaVarDefList2;
#else
_currn = (_TPPrSymbolLambdaVarDefList2) TreeNodeAlloc (sizeof (struct _TPrSymbolLambdaVarDefList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLambdaVarDefList2;
_currn->_desc1 = (_TSPSymbol_Lambda_VarDefs) MkSymbol_Lambda_VarDefs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLambdaVarDefList2: root of subtree no. 1 can not be made a Symbol_Lambda_VarDefs node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Lambda_VarDef) MkSymbol_Lambda_VarDef (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLambdaVarDefList2: root of subtree no. 2 can not be made a Symbol_Lambda_VarDef node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLambdaVarDefList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolLambdaVarDefList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLambdaVarDefList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLambdaVarDefList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLambdaVarDefList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLambdaVarDefList1;
#else
_currn = (_TPPrSymbolLambdaVarDefList1) TreeNodeAlloc (sizeof (struct _TPrSymbolLambdaVarDefList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLambdaVarDefList1;
_currn->_desc1 = (_TSPSymbol_Lambda_VarDef) MkSymbol_Lambda_VarDef (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLambdaVarDefList1: root of subtree no. 1 can not be made a Symbol_Lambda_VarDef node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLambdaVarDefList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolLambdaVarDefList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLambdaVarDef (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLambdaVarDef (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLambdaVarDef _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLambdaVarDef;
#else
_currn = (_TPPrSymbolLambdaVarDef) TreeNodeAlloc (sizeof (struct _TPrSymbolLambdaVarDef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLambdaVarDef;
_currn->_desc1 = (_TSPSymbol_VariableDefId) MkSymbol_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLambdaVarDef: root of subtree no. 1 can not be made a Symbol_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPOptionalType) MkOptionalType (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLambdaVarDef: root of subtree no. 2 can not be made a OptionalType node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLambdaVarDef;
return ( (NODEPTR) _currn);
}/* MkrSymbolLambdaVarDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolVariableDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolVariableDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolVariableDefId _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolVariableDefId;
#else
_currn = (_TPPrSymbolVariableDefId) TreeNodeAlloc (sizeof (struct _TPrSymbolVariableDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolVariableDefId;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolVariableDefId: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolVariableDefId;
return ( (NODEPTR) _currn);
}/* MkrSymbolVariableDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionLet (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionLet (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionLet _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionLet;
#else
_currn = (_TPPrSymbolExpressionLet) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionLet));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionLet;
_currn->_desc1 = (_TSPSymbol_LetVar_Defs) MkSymbol_LetVar_Defs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionLet: root of subtree no. 1 can not be made a Symbol_LetVar_Defs node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionLet: root of subtree no. 2 can not be made a Symbol_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionLet;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionLet */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLetVarDefList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLetVarDefList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLetVarDefList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLetVarDefList2;
#else
_currn = (_TPPrSymbolLetVarDefList2) TreeNodeAlloc (sizeof (struct _TPrSymbolLetVarDefList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLetVarDefList2;
_currn->_desc1 = (_TSPSymbol_LetVar_Defs) MkSymbol_LetVar_Defs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLetVarDefList2: root of subtree no. 1 can not be made a Symbol_LetVar_Defs node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_LetVar_Def) MkSymbol_LetVar_Def (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLetVarDefList2: root of subtree no. 2 can not be made a Symbol_LetVar_Def node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLetVarDefList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolLetVarDefList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLetVarDefList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLetVarDefList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLetVarDefList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLetVarDefList1;
#else
_currn = (_TPPrSymbolLetVarDefList1) TreeNodeAlloc (sizeof (struct _TPrSymbolLetVarDefList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLetVarDefList1;
_currn->_desc1 = (_TSPSymbol_LetVar_Def) MkSymbol_LetVar_Def (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLetVarDefList1: root of subtree no. 1 can not be made a Symbol_LetVar_Def node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLetVarDefList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolLetVarDefList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLetVarDef (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrSymbolLetVarDef (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrSymbolLetVarDef _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLetVarDef;
#else
_currn = (_TPPrSymbolLetVarDef) TreeNodeAlloc (sizeof (struct _TPrSymbolLetVarDef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLetVarDef;
_currn->_desc1 = (_TSPSymbol_VariableDefId) MkSymbol_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLetVarDef: root of subtree no. 1 can not be made a Symbol_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPOptionalType) MkOptionalType (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLetVarDef: root of subtree no. 2 can not be made a OptionalType node ", 0, _coordref);
_currn->_desc3 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLetVarDef: root of subtree no. 3 can not be made a Symbol_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLetVarDef;
return ( (NODEPTR) _currn);
}/* MkrSymbolLetVarDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionIf (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrSymbolExpressionIf (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrSymbolExpressionIf _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionIf;
#else
_currn = (_TPPrSymbolExpressionIf) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionIf));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionIf;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIf: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIf: root of subtree no. 2 can not be made a Symbol_Expression node ", 0, _coordref);
_currn->_desc3 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIf: root of subtree no. 3 can not be made a Symbol_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionIf;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionIf */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionError (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionError (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionError _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionError;
#else
_currn = (_TPPrSymbolExpressionError) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionError));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionError;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionError: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionError;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionError */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionWhenCond (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionWhenCond (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionWhenCond _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionWhenCond;
#else
_currn = (_TPPrSymbolExpressionWhenCond) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionWhenCond));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionWhenCond;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionWhenCond: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionWhenCond: root of subtree no. 2 can not be made a Symbol_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionWhenCond;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionWhenCond */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionWhen (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionWhen (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionWhen _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionWhen;
#else
_currn = (_TPPrSymbolExpressionWhen) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionWhen));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionWhen;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionWhen: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionWhen: root of subtree no. 2 can not be made a Symbol_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionWhen;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionWhen */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionIsBinary (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionIsBinary (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionIsBinary _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionIsBinary;
#else
_currn = (_TPPrSymbolExpressionIsBinary) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionIsBinary));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionIsBinary;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionIsBinary: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionIsBinary;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionIsBinary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryChain (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionBinaryChain (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionBinaryChain _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryChain;
#else
_currn = (_TPPrSymbolExpressionBinaryChain) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryChain));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryChain;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryChain: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryChain;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryChain */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryOR (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryOR (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryOR _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryOR;
#else
_currn = (_TPPrSymbolExpressionBinaryOR) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryOR));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryOR;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryOR: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryOR: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryOR;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryOR */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryAND (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryAND (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryAND _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryAND;
#else
_currn = (_TPPrSymbolExpressionBinaryAND) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryAND));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryAND;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryAND: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryAND: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryAND;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryAND */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryEQ (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryEQ (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryEQ _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryEQ;
#else
_currn = (_TPPrSymbolExpressionBinaryEQ) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryEQ));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryEQ;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryEQ: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryEQ: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryEQ;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryEQ */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryNE (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryNE (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryNE _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryNE;
#else
_currn = (_TPPrSymbolExpressionBinaryNE) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryNE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryNE;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryNE: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryNE: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryNE;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryNE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryLT (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryLT (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryLT _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryLT;
#else
_currn = (_TPPrSymbolExpressionBinaryLT) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryLT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryLT;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryLT: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryLT: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryLT;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryLT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryGT (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryGT (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryGT _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryGT;
#else
_currn = (_TPPrSymbolExpressionBinaryGT) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryGT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryGT;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryGT: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryGT: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryGT;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryGT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryLE (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryLE (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryLE _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryLE;
#else
_currn = (_TPPrSymbolExpressionBinaryLE) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryLE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryLE;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryLE: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryLE: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryLE;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryLE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryGE (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryGE (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryGE _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryGE;
#else
_currn = (_TPPrSymbolExpressionBinaryGE) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryGE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryGE;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryGE: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryGE: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryGE;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryGE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryConcat (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryConcat (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryConcat _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryConcat;
#else
_currn = (_TPPrSymbolExpressionBinaryConcat) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryConcat));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryConcat;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryConcat: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryConcat: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryConcat;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryConcat */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryADD (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryADD (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryADD _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryADD;
#else
_currn = (_TPPrSymbolExpressionBinaryADD) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryADD));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryADD;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryADD: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryADD: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryADD;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryADD */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinarySUB (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinarySUB (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinarySUB _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinarySUB;
#else
_currn = (_TPPrSymbolExpressionBinarySUB) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinarySUB));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinarySUB;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinarySUB: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinarySUB: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinarySUB;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinarySUB */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryMUL (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryMUL (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryMUL _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryMUL;
#else
_currn = (_TPPrSymbolExpressionBinaryMUL) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryMUL));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryMUL;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryMUL: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryMUL: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryMUL;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryMUL */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryDIV (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryDIV (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryDIV _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryDIV;
#else
_currn = (_TPPrSymbolExpressionBinaryDIV) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryDIV));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryDIV;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryDIV: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryDIV: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryDIV;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryDIV */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryMOD (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionBinaryMOD (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionBinaryMOD _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryMOD;
#else
_currn = (_TPPrSymbolExpressionBinaryMOD) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryMOD));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryMOD;
_currn->_desc1 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryMOD: root of subtree no. 1 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Binary) MkSymbol_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryMOD: root of subtree no. 2 can not be made a Symbol_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryMOD;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryMOD */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionBinaryIsUnary (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionBinaryIsUnary (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionBinaryIsUnary _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionBinaryIsUnary;
#else
_currn = (_TPPrSymbolExpressionBinaryIsUnary) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionBinaryIsUnary));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionBinaryIsUnary;
_currn->_desc1 = (_TSPSymbol_Expression_Unary) MkSymbol_Expression_Unary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionBinaryIsUnary: root of subtree no. 1 can not be made a Symbol_Expression_Unary node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionBinaryIsUnary;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionBinaryIsUnary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionUnaryIsPostfix (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionUnaryIsPostfix (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionUnaryIsPostfix _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionUnaryIsPostfix;
#else
_currn = (_TPPrSymbolExpressionUnaryIsPostfix) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionUnaryIsPostfix));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionUnaryIsPostfix;
_currn->_desc1 = (_TSPSymbol_Expression_Postfix) MkSymbol_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionUnaryIsPostfix: root of subtree no. 1 can not be made a Symbol_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionUnaryIsPostfix;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionUnaryIsPostfix */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionUnaryIncr (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionUnaryIncr (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionUnaryIncr _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionUnaryIncr;
#else
_currn = (_TPPrSymbolExpressionUnaryIncr) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionUnaryIncr));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionUnaryIncr;
_currn->_desc1 = (_TSPSymbol_Expression_Postfix) MkSymbol_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionUnaryIncr: root of subtree no. 1 can not be made a Symbol_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionUnaryIncr;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionUnaryIncr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionUnaryNEG (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionUnaryNEG (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionUnaryNEG _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionUnaryNEG;
#else
_currn = (_TPPrSymbolExpressionUnaryNEG) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionUnaryNEG));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionUnaryNEG;
_currn->_desc1 = (_TSPSymbol_Expression_Postfix) MkSymbol_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionUnaryNEG: root of subtree no. 1 can not be made a Symbol_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionUnaryNEG;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionUnaryNEG */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionUnaryNOT (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionUnaryNOT (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionUnaryNOT _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionUnaryNOT;
#else
_currn = (_TPPrSymbolExpressionUnaryNOT) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionUnaryNOT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionUnaryNOT;
_currn->_desc1 = (_TSPSymbol_Expression_Postfix) MkSymbol_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionUnaryNOT: root of subtree no. 1 can not be made a Symbol_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionUnaryNOT;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionUnaryNOT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionUnaryAddress (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionUnaryAddress (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionUnaryAddress _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionUnaryAddress;
#else
_currn = (_TPPrSymbolExpressionUnaryAddress) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionUnaryAddress));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionUnaryAddress;
_currn->_desc1 = (_TSPSymbol_Expression_Postfix) MkSymbol_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionUnaryAddress: root of subtree no. 1 can not be made a Symbol_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionUnaryAddress;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionUnaryAddress */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionUnaryReference (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionUnaryReference (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionUnaryReference _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionUnaryReference;
#else
_currn = (_TPPrSymbolExpressionUnaryReference) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionUnaryReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionUnaryReference;
_currn->_desc1 = (_TSPSymbol_Expression_Postfix) MkSymbol_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionUnaryReference: root of subtree no. 1 can not be made a Symbol_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionUnaryReference;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionUnaryReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionPostfixIsPrimary (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionPostfixIsPrimary (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionPostfixIsPrimary _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionPostfixIsPrimary;
#else
_currn = (_TPPrSymbolExpressionPostfixIsPrimary) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionPostfixIsPrimary));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionPostfixIsPrimary;
_currn->_desc1 = (_TSPSymbol_Expression_Primary) MkSymbol_Expression_Primary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPostfixIsPrimary: root of subtree no. 1 can not be made a Symbol_Expression_Primary node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionPostfixIsPrimary;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionPostfixIsPrimary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionPostfixIsIndex (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionPostfixIsIndex (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionPostfixIsIndex _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionPostfixIsIndex;
#else
_currn = (_TPPrSymbolExpressionPostfixIsIndex) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionPostfixIsIndex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionPostfixIsIndex;
_currn->_desc1 = (_TSPSymbol_Expression_Postfix) MkSymbol_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPostfixIsIndex: root of subtree no. 1 can not be made a Symbol_Expression_Postfix node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPostfixIsIndex: root of subtree no. 2 can not be made a Symbol_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionPostfixIsIndex;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionPostfixIsIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionPostfixIsAccess (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionPostfixIsAccess (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionPostfixIsAccess _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionPostfixIsAccess;
#else
_currn = (_TPPrSymbolExpressionPostfixIsAccess) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionPostfixIsAccess));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionPostfixIsAccess;
_currn->_desc1 = (_TSPSymbol_Expression_Postfix) MkSymbol_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPostfixIsAccess: root of subtree no. 1 can not be made a Symbol_Expression_Postfix node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Primary) MkSymbol_Expression_Primary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPostfixIsAccess: root of subtree no. 2 can not be made a Symbol_Expression_Primary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionPostfixIsAccess;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionPostfixIsAccess */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionPostfixIsListcon (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionPostfixIsListcon (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionPostfixIsListcon _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionPostfixIsListcon;
#else
_currn = (_TPPrSymbolExpressionPostfixIsListcon) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionPostfixIsListcon));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionPostfixIsListcon;
_currn->_desc1 = (_TSPSymbol_Expression_Postfix) MkSymbol_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPostfixIsListcon: root of subtree no. 1 can not be made a Symbol_Expression_Postfix node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Expression_Primary) MkSymbol_Expression_Primary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPostfixIsListcon: root of subtree no. 2 can not be made a Symbol_Expression_Primary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionPostfixIsListcon;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionPostfixIsListcon */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionPrimaryIsConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionPrimaryIsConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionPrimaryIsConstant _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionPrimaryIsConstant;
#else
_currn = (_TPPrSymbolExpressionPrimaryIsConstant) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionPrimaryIsConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionPrimaryIsConstant;
_currn->_desc1 = (_TSPSymbol_Constant) MkSymbol_Constant (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPrimaryIsConstant: root of subtree no. 1 can not be made a Symbol_Constant node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionPrimaryIsConstant;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionPrimaryIsConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionPrimaryIsCall (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionPrimaryIsCall (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionPrimaryIsCall _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionPrimaryIsCall;
#else
_currn = (_TPPrSymbolExpressionPrimaryIsCall) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionPrimaryIsCall));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionPrimaryIsCall;
_currn->_desc1 = (_TSPSymbol_Expression_Call) MkSymbol_Expression_Call (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPrimaryIsCall: root of subtree no. 1 can not be made a Symbol_Expression_Call node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionPrimaryIsCall;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionPrimaryIsCall */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionPrimaryIsTuple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionPrimaryIsTuple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionPrimaryIsTuple _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionPrimaryIsTuple;
#else
_currn = (_TPPrSymbolExpressionPrimaryIsTuple) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionPrimaryIsTuple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionPrimaryIsTuple;
_currn->_desc1 = (_TSPSymbol_TupleConstruction) MkSymbol_TupleConstruction (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPrimaryIsTuple: root of subtree no. 1 can not be made a Symbol_TupleConstruction node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionPrimaryIsTuple;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionPrimaryIsTuple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionPrimaryIsWrap (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionPrimaryIsWrap (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionPrimaryIsWrap _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionPrimaryIsWrap;
#else
_currn = (_TPPrSymbolExpressionPrimaryIsWrap) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionPrimaryIsWrap));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionPrimaryIsWrap;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionPrimaryIsWrap: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionPrimaryIsWrap;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionPrimaryIsWrap */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolTupleConstruction (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolTupleConstruction (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolTupleConstruction _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolTupleConstruction;
#else
_currn = (_TPPrSymbolTupleConstruction) TreeNodeAlloc (sizeof (struct _TPrSymbolTupleConstruction));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolTupleConstruction;
_currn->_desc1 = (_TSPSymbol_TupleArguments) MkSymbol_TupleArguments (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolTupleConstruction: root of subtree no. 1 can not be made a Symbol_TupleArguments node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_TupleArgument) MkSymbol_TupleArgument (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolTupleConstruction: root of subtree no. 2 can not be made a Symbol_TupleArgument node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolTupleConstruction;
return ( (NODEPTR) _currn);
}/* MkrSymbolTupleConstruction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolTupleArgumentList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolTupleArgumentList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolTupleArgumentList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolTupleArgumentList2;
#else
_currn = (_TPPrSymbolTupleArgumentList2) TreeNodeAlloc (sizeof (struct _TPrSymbolTupleArgumentList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolTupleArgumentList2;
_currn->_desc1 = (_TSPSymbol_TupleArguments) MkSymbol_TupleArguments (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolTupleArgumentList2: root of subtree no. 1 can not be made a Symbol_TupleArguments node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_TupleArgument) MkSymbol_TupleArgument (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolTupleArgumentList2: root of subtree no. 2 can not be made a Symbol_TupleArgument node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolTupleArgumentList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolTupleArgumentList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolTupleArgumentList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolTupleArgumentList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolTupleArgumentList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolTupleArgumentList1;
#else
_currn = (_TPPrSymbolTupleArgumentList1) TreeNodeAlloc (sizeof (struct _TPrSymbolTupleArgumentList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolTupleArgumentList1;
_currn->_desc1 = (_TSPSymbol_TupleArgument) MkSymbol_TupleArgument (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolTupleArgumentList1: root of subtree no. 1 can not be made a Symbol_TupleArgument node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolTupleArgumentList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolTupleArgumentList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolTupleArgument (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolTupleArgument (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolTupleArgument _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolTupleArgument;
#else
_currn = (_TPPrSymbolTupleArgument) TreeNodeAlloc (sizeof (struct _TPrSymbolTupleArgument));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolTupleArgument;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolTupleArgument: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolTupleArgument;
return ( (NODEPTR) _currn);
}/* MkrSymbolTupleArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionCallApplied (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolExpressionCallApplied (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolExpressionCallApplied _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionCallApplied;
#else
_currn = (_TPPrSymbolExpressionCallApplied) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionCallApplied));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionCallApplied;
_currn->_desc1 = (_TSPSymbol_CallableReference) MkSymbol_CallableReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionCallApplied: root of subtree no. 1 can not be made a Symbol_CallableReference node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_CallParameters) MkSymbol_CallParameters (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionCallApplied: root of subtree no. 2 can not be made a Symbol_CallParameters node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolExpressionCallApplied;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionCallApplied */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionCallEmpty (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionCallEmpty (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionCallEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionCallEmpty;
#else
_currn = (_TPPrSymbolExpressionCallEmpty) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionCallEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionCallEmpty;
_currn->_desc1 = (_TSPSymbol_CallableReference) MkSymbol_CallableReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionCallEmpty: root of subtree no. 1 can not be made a Symbol_CallableReference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionCallEmpty;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionCallEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolExpressionCallVariable (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolExpressionCallVariable (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolExpressionCallVariable _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolExpressionCallVariable;
#else
_currn = (_TPPrSymbolExpressionCallVariable) TreeNodeAlloc (sizeof (struct _TPrSymbolExpressionCallVariable));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolExpressionCallVariable;
_currn->_desc1 = (_TSPSymbol_CallableReference) MkSymbol_CallableReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolExpressionCallVariable: root of subtree no. 1 can not be made a Symbol_CallableReference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolExpressionCallVariable;
return ( (NODEPTR) _currn);
}/* MkrSymbolExpressionCallVariable */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolCallableReferenceIsIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolCallableReferenceIsIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolCallableReferenceIsIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolCallableReferenceIsIdentifier;
#else
_currn = (_TPPrSymbolCallableReferenceIsIdentifier) TreeNodeAlloc (sizeof (struct _TPrSymbolCallableReferenceIsIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolCallableReferenceIsIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolCallableReferenceIsIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolCallableReferenceIsIdentifier;
return ( (NODEPTR) _currn);
}/* MkrSymbolCallableReferenceIsIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolCallableReferenceIsTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolCallableReferenceIsTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolCallableReferenceIsTypename _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolCallableReferenceIsTypename;
#else
_currn = (_TPPrSymbolCallableReferenceIsTypename) TreeNodeAlloc (sizeof (struct _TPrSymbolCallableReferenceIsTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolCallableReferenceIsTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolCallableReferenceIsTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolCallableReferenceIsTypename;
return ( (NODEPTR) _currn);
}/* MkrSymbolCallableReferenceIsTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolCallableReferenceIsTreeCon (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolCallableReferenceIsTreeCon (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolCallableReferenceIsTreeCon _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolCallableReferenceIsTreeCon;
#else
_currn = (_TPPrSymbolCallableReferenceIsTreeCon) TreeNodeAlloc (sizeof (struct _TPrSymbolCallableReferenceIsTreeCon));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolCallableReferenceIsTreeCon;
_currn->_desc1 = (_TSPRule_Reference) MkRule_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolCallableReferenceIsTreeCon: root of subtree no. 1 can not be made a Rule_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_TreeHint) MkOpt_TreeHint (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolCallableReferenceIsTreeCon: root of subtree no. 2 can not be made a Opt_TreeHint node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolCallableReferenceIsTreeCon;
return ( (NODEPTR) _currn);
}/* MkrSymbolCallableReferenceIsTreeCon */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolCallableReferenceIsEnd (POSITION *_coordref)
#else
NODEPTR MkrSymbolCallableReferenceIsEnd (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrSymbolCallableReferenceIsEnd _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolCallableReferenceIsEnd;
#else
_currn = (_TPPrSymbolCallableReferenceIsEnd) TreeNodeAlloc (sizeof (struct _TPrSymbolCallableReferenceIsEnd));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolCallableReferenceIsEnd;
_SETCOORD(_currn)
_TERMACT_rSymbolCallableReferenceIsEnd;
return ( (NODEPTR) _currn);
}/* MkrSymbolCallableReferenceIsEnd */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolCallableReferenceIsAttribute (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolCallableReferenceIsAttribute (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolCallableReferenceIsAttribute _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolCallableReferenceIsAttribute;
#else
_currn = (_TPPrSymbolCallableReferenceIsAttribute) TreeNodeAlloc (sizeof (struct _TPrSymbolCallableReferenceIsAttribute));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolCallableReferenceIsAttribute;
_currn->_desc1 = (_TSPSymbol_LocalAttribute_Ref) MkSymbol_LocalAttribute_Ref (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolCallableReferenceIsAttribute: root of subtree no. 1 can not be made a Symbol_LocalAttribute_Ref node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolCallableReferenceIsAttribute;
return ( (NODEPTR) _currn);
}/* MkrSymbolCallableReferenceIsAttribute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolCallParameterList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolCallParameterList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolCallParameterList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolCallParameterList2;
#else
_currn = (_TPPrSymbolCallParameterList2) TreeNodeAlloc (sizeof (struct _TPrSymbolCallParameterList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolCallParameterList2;
_currn->_desc1 = (_TSPSymbol_CallParameters) MkSymbol_CallParameters (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolCallParameterList2: root of subtree no. 1 can not be made a Symbol_CallParameters node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_CallParameter) MkSymbol_CallParameter (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolCallParameterList2: root of subtree no. 2 can not be made a Symbol_CallParameter node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolCallParameterList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolCallParameterList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolCallParameterList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolCallParameterList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolCallParameterList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolCallParameterList1;
#else
_currn = (_TPPrSymbolCallParameterList1) TreeNodeAlloc (sizeof (struct _TPrSymbolCallParameterList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolCallParameterList1;
_currn->_desc1 = (_TSPSymbol_CallParameter) MkSymbol_CallParameter (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolCallParameterList1: root of subtree no. 1 can not be made a Symbol_CallParameter node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolCallParameterList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolCallParameterList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolCallParameter (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolCallParameter (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolCallParameter _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolCallParameter;
#else
_currn = (_TPPrSymbolCallParameter) TreeNodeAlloc (sizeof (struct _TPrSymbolCallParameter));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolCallParameter;
_currn->_desc1 = (_TSPSymbol_Expression) MkSymbol_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolCallParameter: root of subtree no. 1 can not be made a Symbol_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolCallParameter;
return ( (NODEPTR) _currn);
}/* MkrSymbolCallParameter */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolConstant _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolConstant;
#else
_currn = (_TPPrSymbolConstant) TreeNodeAlloc (sizeof (struct _TPrSymbolConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolConstant;
_currn->_desc1 = (_TSPConstant) MkConstant (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolConstant: root of subtree no. 1 can not be made a Constant node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolConstant;
return ( (NODEPTR) _currn);
}/* MkrSymbolConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeclRuleSpecification (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeclRuleSpecification (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeclRuleSpecification _currn;
#ifdef __cplusplus
_currn = new _TPrDeclRuleSpecification;
#else
_currn = (_TPPrDeclRuleSpecification) TreeNodeAlloc (sizeof (struct _TPrDeclRuleSpecification));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeclRuleSpecification;
_currn->_desc1 = (_TSPRule_Specification) MkRule_Specification (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeclRuleSpecification: root of subtree no. 1 can not be made a Rule_Specification node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeclRuleSpecification;
return ( (NODEPTR) _currn);
}/* MkrDeclRuleSpecification */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleSpecification (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRuleSpecification (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRuleSpecification _currn;
#ifdef __cplusplus
_currn = new _TPrRuleSpecification;
#else
_currn = (_TPPrRuleSpecification) TreeNodeAlloc (sizeof (struct _TPrRuleSpecification));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleSpecification;
_currn->_desc1 = (_TSPRule_Classification) MkRule_Classification (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSpecification: root of subtree no. 1 can not be made a Rule_Classification node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Pattern) MkRule_Pattern (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSpecification: root of subtree no. 2 can not be made a Rule_Pattern node ", 0, _coordref);
_currn->_desc3 = (_TSPRule_GuardRhs) MkRule_GuardRhs (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSpecification: root of subtree no. 3 can not be made a Rule_GuardRhs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleSpecification;
return ( (NODEPTR) _currn);
}/* MkrRuleSpecification */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleGuardCondition (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleGuardCondition (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleGuardCondition _currn;
#ifdef __cplusplus
_currn = new _TPrRuleGuardCondition;
#else
_currn = (_TPPrRuleGuardCondition) TreeNodeAlloc (sizeof (struct _TPrRuleGuardCondition));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleGuardCondition;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardCondition: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_GuardRhs) MkRule_GuardRhs (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardCondition: root of subtree no. 2 can not be made a Rule_GuardRhs node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleGuardCondition;
return ( (NODEPTR) _currn);
}/* MkrRuleGuardCondition */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleGuardRhsGuards (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleGuardRhsGuards (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleGuardRhsGuards _currn;
#ifdef __cplusplus
_currn = new _TPrRuleGuardRhsGuards;
#else
_currn = (_TPPrRuleGuardRhsGuards) TreeNodeAlloc (sizeof (struct _TPrRuleGuardRhsGuards));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleGuardRhsGuards;
_currn->_desc1 = (_TSPRule_Guard) MkRule_Guard (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardRhsGuards: root of subtree no. 1 can not be made a Rule_Guard node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleGuardRhsGuards;
return ( (NODEPTR) _currn);
}/* MkrRuleGuardRhsGuards */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleGuardRhsComputes (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleGuardRhsComputes (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleGuardRhsComputes _currn;
#ifdef __cplusplus
_currn = new _TPrRuleGuardRhsComputes;
#else
_currn = (_TPPrRuleGuardRhsComputes) TreeNodeAlloc (sizeof (struct _TPrRuleGuardRhsComputes));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleGuardRhsComputes;
_currn->_desc1 = (_TSPRule_Computations) MkRule_Computations (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardRhsComputes: root of subtree no. 1 can not be made a Rule_Computations node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleGuardRhsComputes;
return ( (NODEPTR) _currn);
}/* MkrRuleGuardRhsComputes */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleClassificationSingle (POSITION *_coordref)
#else
NODEPTR MkrRuleClassificationSingle (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRuleClassificationSingle _currn;
#ifdef __cplusplus
_currn = new _TPrRuleClassificationSingle;
#else
_currn = (_TPPrRuleClassificationSingle) TreeNodeAlloc (sizeof (struct _TPrRuleClassificationSingle));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleClassificationSingle;
_SETCOORD(_currn)
_TERMACT_rRuleClassificationSingle;
return ( (NODEPTR) _currn);
}/* MkrRuleClassificationSingle */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleClassificationMultiple (POSITION *_coordref)
#else
NODEPTR MkrRuleClassificationMultiple (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRuleClassificationMultiple _currn;
#ifdef __cplusplus
_currn = new _TPrRuleClassificationMultiple;
#else
_currn = (_TPPrRuleClassificationMultiple) TreeNodeAlloc (sizeof (struct _TPrRuleClassificationMultiple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleClassificationMultiple;
_SETCOORD(_currn)
_TERMACT_rRuleClassificationMultiple;
return ( (NODEPTR) _currn);
}/* MkrRuleClassificationMultiple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleClassificationSingleClass (POSITION *_coordref)
#else
NODEPTR MkrRuleClassificationSingleClass (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRuleClassificationSingleClass _currn;
#ifdef __cplusplus
_currn = new _TPrRuleClassificationSingleClass;
#else
_currn = (_TPPrRuleClassificationSingleClass) TreeNodeAlloc (sizeof (struct _TPrRuleClassificationSingleClass));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleClassificationSingleClass;
_SETCOORD(_currn)
_TERMACT_rRuleClassificationSingleClass;
return ( (NODEPTR) _currn);
}/* MkrRuleClassificationSingleClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleClassificationMultipleClass (POSITION *_coordref)
#else
NODEPTR MkrRuleClassificationMultipleClass (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRuleClassificationMultipleClass _currn;
#ifdef __cplusplus
_currn = new _TPrRuleClassificationMultipleClass;
#else
_currn = (_TPPrRuleClassificationMultipleClass) TreeNodeAlloc (sizeof (struct _TPrRuleClassificationMultipleClass));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleClassificationMultipleClass;
_SETCOORD(_currn)
_TERMACT_rRuleClassificationMultipleClass;
return ( (NODEPTR) _currn);
}/* MkrRuleClassificationMultipleClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulePatternMultiple (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3, NODEPTR _desc4)
#else
NODEPTR MkrRulePatternMultiple (_coordref,_desc1,_desc2,_desc3,_desc4)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
	NODEPTR _desc4;
#endif
{	_TPPrRulePatternMultiple _currn;
#ifdef __cplusplus
_currn = new _TPrRulePatternMultiple;
#else
_currn = (_TPPrRulePatternMultiple) TreeNodeAlloc (sizeof (struct _TPrRulePatternMultiple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulePatternMultiple;
_currn->_desc1 = (_TSPRule_References) MkRule_References (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternMultiple: root of subtree no. 1 can not be made a Rule_References node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Reference) MkRule_Reference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternMultiple: root of subtree no. 2 can not be made a Rule_Reference node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_TreeHint) MkOpt_TreeHint (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternMultiple: root of subtree no. 3 can not be made a Opt_TreeHint node ", 0, _coordref);
_currn->_desc4 = (_TSPOpt_Production) MkOpt_Production (_coordref, _desc4);	
if (((NODEPTR)_currn->_desc4) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternMultiple: root of subtree no. 4 can not be made a Opt_Production node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRulePatternMultiple;
return ( (NODEPTR) _currn);
}/* MkrRulePatternMultiple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulePatternSingle (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRulePatternSingle (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRulePatternSingle _currn;
#ifdef __cplusplus
_currn = new _TPrRulePatternSingle;
#else
_currn = (_TPPrRulePatternSingle) TreeNodeAlloc (sizeof (struct _TPrRulePatternSingle));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulePatternSingle;
_currn->_desc1 = (_TSPRule_Reference) MkRule_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternSingle: root of subtree no. 1 can not be made a Rule_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_TreeHint) MkOpt_TreeHint (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternSingle: root of subtree no. 2 can not be made a Opt_TreeHint node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_Production) MkOpt_Production (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternSingle: root of subtree no. 3 can not be made a Opt_Production node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRulePatternSingle;
return ( (NODEPTR) _currn);
}/* MkrRulePatternSingle */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulePatternVarious (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRulePatternVarious (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRulePatternVarious _currn;
#ifdef __cplusplus
_currn = new _TPrRulePatternVarious;
#else
_currn = (_TPPrRulePatternVarious) TreeNodeAlloc (sizeof (struct _TPrRulePatternVarious));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulePatternVarious;
_currn->_desc1 = (_TSPRule_LHSPattern) MkRule_LHSPattern (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternVarious: root of subtree no. 1 can not be made a Rule_LHSPattern node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_RHSPattern) MkRule_RHSPattern (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternVarious: root of subtree no. 2 can not be made a Rule_RHSPattern node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRulePatternVarious;
return ( (NODEPTR) _currn);
}/* MkrRulePatternVarious */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleReferenceIsTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleReferenceIsTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleReferenceIsTypename _currn;
#ifdef __cplusplus
_currn = new _TPrRuleReferenceIsTypename;
#else
_currn = (_TPPrRuleReferenceIsTypename) TreeNodeAlloc (sizeof (struct _TPrRuleReferenceIsTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleReferenceIsTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleReferenceIsTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleReferenceIsTypename;
return ( (NODEPTR) _currn);
}/* MkrRuleReferenceIsTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleReferenceIsIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleReferenceIsIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleReferenceIsIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrRuleReferenceIsIdentifier;
#else
_currn = (_TPPrRuleReferenceIsIdentifier) TreeNodeAlloc (sizeof (struct _TPrRuleReferenceIsIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleReferenceIsIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleReferenceIsIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleReferenceIsIdentifier;
return ( (NODEPTR) _currn);
}/* MkrRuleReferenceIsIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleReferenceList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleReferenceList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleReferenceList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleReferenceList2;
#else
_currn = (_TPPrRuleReferenceList2) TreeNodeAlloc (sizeof (struct _TPrRuleReferenceList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleReferenceList2;
_currn->_desc1 = (_TSPRule_References) MkRule_References (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleReferenceList2: root of subtree no. 1 can not be made a Rule_References node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Reference) MkRule_Reference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleReferenceList2: root of subtree no. 2 can not be made a Rule_Reference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleReferenceList2;
return ( (NODEPTR) _currn);
}/* MkrRuleReferenceList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleReferenceList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleReferenceList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleReferenceList1 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleReferenceList1;
#else
_currn = (_TPPrRuleReferenceList1) TreeNodeAlloc (sizeof (struct _TPrRuleReferenceList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleReferenceList1;
_currn->_desc1 = (_TSPRule_Reference) MkRule_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleReferenceList1: root of subtree no. 1 can not be made a Rule_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleReferenceList1;
return ( (NODEPTR) _currn);
}/* MkrRuleReferenceList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleLhsPatternSpecific (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRuleLhsPatternSpecific (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRuleLhsPatternSpecific _currn;
#ifdef __cplusplus
_currn = new _TPrRuleLhsPatternSpecific;
#else
_currn = (_TPPrRuleLhsPatternSpecific) TreeNodeAlloc (sizeof (struct _TPrRuleLhsPatternSpecific));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleLhsPatternSpecific;
_currn->_desc1 = (_TSPOpt_RuleVariableDefId) MkOpt_RuleVariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLhsPatternSpecific: root of subtree no. 1 can not be made a Opt_RuleVariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLhsPatternSpecific: root of subtree no. 2 can not be made a Symbol_Reference node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_TreeHint) MkOpt_TreeHint (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLhsPatternSpecific: root of subtree no. 3 can not be made a Opt_TreeHint node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleLhsPatternSpecific;
return ( (NODEPTR) _currn);
}/* MkrRuleLhsPatternSpecific */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleLhsPatternDontCare (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleLhsPatternDontCare (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleLhsPatternDontCare _currn;
#ifdef __cplusplus
_currn = new _TPrRuleLhsPatternDontCare;
#else
_currn = (_TPPrRuleLhsPatternDontCare) TreeNodeAlloc (sizeof (struct _TPrRuleLhsPatternDontCare));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleLhsPatternDontCare;
_currn->_desc1 = (_TSPOpt_RuleVariableDefId) MkOpt_RuleVariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLhsPatternDontCare: root of subtree no. 1 can not be made a Opt_RuleVariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_TreeHint) MkOpt_TreeHint (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLhsPatternDontCare: root of subtree no. 2 can not be made a Opt_TreeHint node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleLhsPatternDontCare;
return ( (NODEPTR) _currn);
}/* MkrRuleLhsPatternDontCare */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoRuleVariableDef (POSITION *_coordref)
#else
NODEPTR MkrNoRuleVariableDef (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoRuleVariableDef _currn;
#ifdef __cplusplus
_currn = new _TPrNoRuleVariableDef;
#else
_currn = (_TPPrNoRuleVariableDef) TreeNodeAlloc (sizeof (struct _TPrNoRuleVariableDef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoRuleVariableDef;
_SETCOORD(_currn)
_TERMACT_rNoRuleVariableDef;
return ( (NODEPTR) _currn);
}/* MkrNoRuleVariableDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrCreateRuleVariable (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrCreateRuleVariable (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrCreateRuleVariable _currn;
#ifdef __cplusplus
_currn = new _TPrCreateRuleVariable;
#else
_currn = (_TPPrCreateRuleVariable) TreeNodeAlloc (sizeof (struct _TPrCreateRuleVariable));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErCreateRuleVariable;
_currn->_desc1 = (_TSPRule_VariableDefId) MkRule_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rCreateRuleVariable: root of subtree no. 1 can not be made a Rule_VariableDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rCreateRuleVariable;
return ( (NODEPTR) _currn);
}/* MkrCreateRuleVariable */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrOptProduction (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrOptProduction (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrOptProduction _currn;
#ifdef __cplusplus
_currn = new _TPrOptProduction;
#else
_currn = (_TPPrOptProduction) TreeNodeAlloc (sizeof (struct _TPrOptProduction));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErOptProduction;
_currn->_desc1 = (_TSPRule_LHSPattern) MkRule_LHSPattern (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rOptProduction: root of subtree no. 1 can not be made a Rule_LHSPattern node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_RHSPattern) MkRule_RHSPattern (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rOptProduction: root of subtree no. 2 can not be made a Rule_RHSPattern node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rOptProduction;
return ( (NODEPTR) _currn);
}/* MkrOptProduction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoProduction (POSITION *_coordref)
#else
NODEPTR MkrNoProduction (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoProduction _currn;
#ifdef __cplusplus
_currn = new _TPrNoProduction;
#else
_currn = (_TPPrNoProduction) TreeNodeAlloc (sizeof (struct _TPrNoProduction));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoProduction;
_SETCOORD(_currn)
_TERMACT_rNoProduction;
return ( (NODEPTR) _currn);
}/* MkrNoProduction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleRHSPattern (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleRHSPattern (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleRHSPattern _currn;
#ifdef __cplusplus
_currn = new _TPrRuleRHSPattern;
#else
_currn = (_TPPrRuleRHSPattern) TreeNodeAlloc (sizeof (struct _TPrRuleRHSPattern));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleRHSPattern;
_currn->_desc1 = (_TSPRule_RHSPatternSymbols) MkRule_RHSPatternSymbols (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleRHSPattern: root of subtree no. 1 can not be made a Rule_RHSPatternSymbols node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleRHSPattern;
return ( (NODEPTR) _currn);
}/* MkrRuleRHSPattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleRHSPatternSymbolList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleRHSPatternSymbolList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleRHSPatternSymbolList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleRHSPatternSymbolList2;
#else
_currn = (_TPPrRuleRHSPatternSymbolList2) TreeNodeAlloc (sizeof (struct _TPrRuleRHSPatternSymbolList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleRHSPatternSymbolList2;
_currn->_desc1 = (_TSPRule_RHSPatternSymbols) MkRule_RHSPatternSymbols (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleRHSPatternSymbolList2: root of subtree no. 1 can not be made a Rule_RHSPatternSymbols node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_RHSPatternSymbol) MkRule_RHSPatternSymbol (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleRHSPatternSymbolList2: root of subtree no. 2 can not be made a Rule_RHSPatternSymbol node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleRHSPatternSymbolList2;
return ( (NODEPTR) _currn);
}/* MkrRuleRHSPatternSymbolList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoRuleRHSPatternSymbols (POSITION *_coordref)
#else
NODEPTR MkrNoRuleRHSPatternSymbols (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoRuleRHSPatternSymbols _currn;
#ifdef __cplusplus
_currn = new _TPrNoRuleRHSPatternSymbols;
#else
_currn = (_TPPrNoRuleRHSPatternSymbols) TreeNodeAlloc (sizeof (struct _TPrNoRuleRHSPatternSymbols));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoRuleRHSPatternSymbols;
_SETCOORD(_currn)
_TERMACT_rNoRuleRHSPatternSymbols;
return ( (NODEPTR) _currn);
}/* MkrNoRuleRHSPatternSymbols */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulePatternSymbolAllRest (POSITION *_coordref)
#else
NODEPTR MkrRulePatternSymbolAllRest (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRulePatternSymbolAllRest _currn;
#ifdef __cplusplus
_currn = new _TPrRulePatternSymbolAllRest;
#else
_currn = (_TPPrRulePatternSymbolAllRest) TreeNodeAlloc (sizeof (struct _TPrRulePatternSymbolAllRest));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulePatternSymbolAllRest;
_SETCOORD(_currn)
_TERMACT_rRulePatternSymbolAllRest;
return ( (NODEPTR) _currn);
}/* MkrRulePatternSymbolAllRest */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulePatternSymbolDontCare (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRulePatternSymbolDontCare (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRulePatternSymbolDontCare _currn;
#ifdef __cplusplus
_currn = new _TPrRulePatternSymbolDontCare;
#else
_currn = (_TPPrRulePatternSymbolDontCare) TreeNodeAlloc (sizeof (struct _TPrRulePatternSymbolDontCare));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulePatternSymbolDontCare;
_currn->_desc1 = (_TSPOpt_RuleVariableDefId) MkOpt_RuleVariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternSymbolDontCare: root of subtree no. 1 can not be made a Opt_RuleVariableDefId node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRulePatternSymbolDontCare;
return ( (NODEPTR) _currn);
}/* MkrRulePatternSymbolDontCare */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulePatternSymbolConstructor (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRulePatternSymbolConstructor (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRulePatternSymbolConstructor _currn;
#ifdef __cplusplus
_currn = new _TPrRulePatternSymbolConstructor;
#else
_currn = (_TPPrRulePatternSymbolConstructor) TreeNodeAlloc (sizeof (struct _TPrRulePatternSymbolConstructor));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulePatternSymbolConstructor;
_currn->_desc1 = (_TSPOpt_RuleVariableDefId) MkOpt_RuleVariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternSymbolConstructor: root of subtree no. 1 can not be made a Opt_RuleVariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol) MkSymbol (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternSymbolConstructor: root of subtree no. 2 can not be made a Symbol node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_DeeperRulePattern) MkOpt_DeeperRulePattern (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternSymbolConstructor: root of subtree no. 3 can not be made a Opt_DeeperRulePattern node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRulePatternSymbolConstructor;
return ( (NODEPTR) _currn);
}/* MkrRulePatternSymbolConstructor */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulePatternSymbolMatchTermString (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRulePatternSymbolMatchTermString (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRulePatternSymbolMatchTermString _currn;
#ifdef __cplusplus
_currn = new _TPrRulePatternSymbolMatchTermString;
#else
_currn = (_TPPrRulePatternSymbolMatchTermString) TreeNodeAlloc (sizeof (struct _TPrRulePatternSymbolMatchTermString));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulePatternSymbolMatchTermString;
_currn->_desc1 = (_TSPString) MkString (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternSymbolMatchTermString: root of subtree no. 1 can not be made a String node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRulePatternSymbolMatchTermString;
return ( (NODEPTR) _currn);
}/* MkrRulePatternSymbolMatchTermString */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRulePatternSymbolMatchTermInt (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRulePatternSymbolMatchTermInt (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRulePatternSymbolMatchTermInt _currn;
#ifdef __cplusplus
_currn = new _TPrRulePatternSymbolMatchTermInt;
#else
_currn = (_TPPrRulePatternSymbolMatchTermInt) TreeNodeAlloc (sizeof (struct _TPrRulePatternSymbolMatchTermInt));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRulePatternSymbolMatchTermInt;
_currn->_desc1 = (_TSPNumber) MkNumber (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRulePatternSymbolMatchTermInt: root of subtree no. 1 can not be made a Number node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRulePatternSymbolMatchTermInt;
return ( (NODEPTR) _currn);
}/* MkrRulePatternSymbolMatchTermInt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoDeeperMatching (POSITION *_coordref)
#else
NODEPTR MkrNoDeeperMatching (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoDeeperMatching _currn;
#ifdef __cplusplus
_currn = new _TPrNoDeeperMatching;
#else
_currn = (_TPPrNoDeeperMatching) TreeNodeAlloc (sizeof (struct _TPrNoDeeperMatching));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoDeeperMatching;
_SETCOORD(_currn)
_TERMACT_rNoDeeperMatching;
return ( (NODEPTR) _currn);
}/* MkrNoDeeperMatching */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrOptDeeperRulePattern (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrOptDeeperRulePattern (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrOptDeeperRulePattern _currn;
#ifdef __cplusplus
_currn = new _TPrOptDeeperRulePattern;
#else
_currn = (_TPPrOptDeeperRulePattern) TreeNodeAlloc (sizeof (struct _TPrOptDeeperRulePattern));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErOptDeeperRulePattern;
_currn->_desc1 = (_TSPDeeper_RulePatternSymbols) MkDeeper_RulePatternSymbols (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rOptDeeperRulePattern: root of subtree no. 1 can not be made a Deeper_RulePatternSymbols node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rOptDeeperRulePattern;
return ( (NODEPTR) _currn);
}/* MkrOptDeeperRulePattern */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeeperRulePatternSymbolList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDeeperRulePatternSymbolList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDeeperRulePatternSymbolList2 _currn;
#ifdef __cplusplus
_currn = new _TPrDeeperRulePatternSymbolList2;
#else
_currn = (_TPPrDeeperRulePatternSymbolList2) TreeNodeAlloc (sizeof (struct _TPrDeeperRulePatternSymbolList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeeperRulePatternSymbolList2;
_currn->_desc1 = (_TSPDeeper_RulePatternSymbols) MkDeeper_RulePatternSymbols (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeeperRulePatternSymbolList2: root of subtree no. 1 can not be made a Deeper_RulePatternSymbols node ", 0, _coordref);
_currn->_desc2 = (_TSPDeeper_RulePatternSymbol) MkDeeper_RulePatternSymbol (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeeperRulePatternSymbolList2: root of subtree no. 2 can not be made a Deeper_RulePatternSymbol node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDeeperRulePatternSymbolList2;
return ( (NODEPTR) _currn);
}/* MkrDeeperRulePatternSymbolList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoDeeperRulePatternSymbols (POSITION *_coordref)
#else
NODEPTR MkrNoDeeperRulePatternSymbols (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoDeeperRulePatternSymbols _currn;
#ifdef __cplusplus
_currn = new _TPrNoDeeperRulePatternSymbols;
#else
_currn = (_TPPrNoDeeperRulePatternSymbols) TreeNodeAlloc (sizeof (struct _TPrNoDeeperRulePatternSymbols));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoDeeperRulePatternSymbols;
_SETCOORD(_currn)
_TERMACT_rNoDeeperRulePatternSymbols;
return ( (NODEPTR) _currn);
}/* MkrNoDeeperRulePatternSymbols */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeeperRulePatternSymbolDontCare (POSITION *_coordref)
#else
NODEPTR MkrDeeperRulePatternSymbolDontCare (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrDeeperRulePatternSymbolDontCare _currn;
#ifdef __cplusplus
_currn = new _TPrDeeperRulePatternSymbolDontCare;
#else
_currn = (_TPPrDeeperRulePatternSymbolDontCare) TreeNodeAlloc (sizeof (struct _TPrDeeperRulePatternSymbolDontCare));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeeperRulePatternSymbolDontCare;
_SETCOORD(_currn)
_TERMACT_rDeeperRulePatternSymbolDontCare;
return ( (NODEPTR) _currn);
}/* MkrDeeperRulePatternSymbolDontCare */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeeperRulePatternSymbolDontCareRest (POSITION *_coordref)
#else
NODEPTR MkrDeeperRulePatternSymbolDontCareRest (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrDeeperRulePatternSymbolDontCareRest _currn;
#ifdef __cplusplus
_currn = new _TPrDeeperRulePatternSymbolDontCareRest;
#else
_currn = (_TPPrDeeperRulePatternSymbolDontCareRest) TreeNodeAlloc (sizeof (struct _TPrDeeperRulePatternSymbolDontCareRest));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeeperRulePatternSymbolDontCareRest;
_SETCOORD(_currn)
_TERMACT_rDeeperRulePatternSymbolDontCareRest;
return ( (NODEPTR) _currn);
}/* MkrDeeperRulePatternSymbolDontCareRest */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeeperRulePatternSymbolMatchTermString (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeeperRulePatternSymbolMatchTermString (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeeperRulePatternSymbolMatchTermString _currn;
#ifdef __cplusplus
_currn = new _TPrDeeperRulePatternSymbolMatchTermString;
#else
_currn = (_TPPrDeeperRulePatternSymbolMatchTermString) TreeNodeAlloc (sizeof (struct _TPrDeeperRulePatternSymbolMatchTermString));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeeperRulePatternSymbolMatchTermString;
_currn->_desc1 = (_TSPString) MkString (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeeperRulePatternSymbolMatchTermString: root of subtree no. 1 can not be made a String node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeeperRulePatternSymbolMatchTermString;
return ( (NODEPTR) _currn);
}/* MkrDeeperRulePatternSymbolMatchTermString */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeeperRulePatternSymbolMatchTermInt (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrDeeperRulePatternSymbolMatchTermInt (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrDeeperRulePatternSymbolMatchTermInt _currn;
#ifdef __cplusplus
_currn = new _TPrDeeperRulePatternSymbolMatchTermInt;
#else
_currn = (_TPPrDeeperRulePatternSymbolMatchTermInt) TreeNodeAlloc (sizeof (struct _TPrDeeperRulePatternSymbolMatchTermInt));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeeperRulePatternSymbolMatchTermInt;
_currn->_desc1 = (_TSPNumber) MkNumber (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeeperRulePatternSymbolMatchTermInt: root of subtree no. 1 can not be made a Number node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rDeeperRulePatternSymbolMatchTermInt;
return ( (NODEPTR) _currn);
}/* MkrDeeperRulePatternSymbolMatchTermInt */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrDeeperMatchConstructor (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrDeeperMatchConstructor (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrDeeperMatchConstructor _currn;
#ifdef __cplusplus
_currn = new _TPrDeeperMatchConstructor;
#else
_currn = (_TPPrDeeperMatchConstructor) TreeNodeAlloc (sizeof (struct _TPrDeeperMatchConstructor));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErDeeperMatchConstructor;
_currn->_desc1 = (_TSPOpt_RuleVariableDefId) MkOpt_RuleVariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeeperMatchConstructor: root of subtree no. 1 can not be made a Opt_RuleVariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rDeeperMatchConstructor: root of subtree no. 2 can not be made a Symbol_Reference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rDeeperMatchConstructor;
return ( (NODEPTR) _currn);
}/* MkrDeeperMatchConstructor */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleComputationList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleComputationList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleComputationList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleComputationList2;
#else
_currn = (_TPPrRuleComputationList2) TreeNodeAlloc (sizeof (struct _TPrRuleComputationList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleComputationList2;
_currn->_desc1 = (_TSPRule_Computations) MkRule_Computations (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleComputationList2: root of subtree no. 1 can not be made a Rule_Computations node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Computation) MkRule_Computation (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleComputationList2: root of subtree no. 2 can not be made a Rule_Computation node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleComputationList2;
return ( (NODEPTR) _currn);
}/* MkrRuleComputationList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoRuleComputations (POSITION *_coordref)
#else
NODEPTR MkrNoRuleComputations (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoRuleComputations _currn;
#ifdef __cplusplus
_currn = new _TPrNoRuleComputations;
#else
_currn = (_TPPrNoRuleComputations) TreeNodeAlloc (sizeof (struct _TPrNoRuleComputations));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoRuleComputations;
_SETCOORD(_currn)
_TERMACT_rNoRuleComputations;
return ( (NODEPTR) _currn);
}/* MkrNoRuleComputations */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleComputeAssign (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleComputeAssign (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleComputeAssign _currn;
#ifdef __cplusplus
_currn = new _TPrRuleComputeAssign;
#else
_currn = (_TPPrRuleComputeAssign) TreeNodeAlloc (sizeof (struct _TPrRuleComputeAssign));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleComputeAssign;
_currn->_desc1 = (_TSPRule_AssignStatement) MkRule_AssignStatement (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleComputeAssign: root of subtree no. 1 can not be made a Rule_AssignStatement node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_RuleDependency) MkOpt_RuleDependency (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleComputeAssign: root of subtree no. 2 can not be made a Opt_RuleDependency node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleComputeAssign;
return ( (NODEPTR) _currn);
}/* MkrRuleComputeAssign */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleComputeExpr (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleComputeExpr (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleComputeExpr _currn;
#ifdef __cplusplus
_currn = new _TPrRuleComputeExpr;
#else
_currn = (_TPPrRuleComputeExpr) TreeNodeAlloc (sizeof (struct _TPrRuleComputeExpr));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleComputeExpr;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleComputeExpr: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_RuleDependency) MkOpt_RuleDependency (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleComputeExpr: root of subtree no. 2 can not be made a Opt_RuleDependency node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleComputeExpr;
return ( (NODEPTR) _currn);
}/* MkrRuleComputeExpr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleComputeChainStart (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleComputeChainStart (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleComputeChainStart _currn;
#ifdef __cplusplus
_currn = new _TPrRuleComputeChainStart;
#else
_currn = (_TPPrRuleComputeChainStart) TreeNodeAlloc (sizeof (struct _TPrRuleComputeChainStart));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleComputeChainStart;
_currn->_desc1 = (_TSPRule_ChainStart) MkRule_ChainStart (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleComputeChainStart: root of subtree no. 1 can not be made a Rule_ChainStart node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_RuleDependency) MkOpt_RuleDependency (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleComputeChainStart: root of subtree no. 2 can not be made a Opt_RuleDependency node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleComputeChainStart;
return ( (NODEPTR) _currn);
}/* MkrRuleComputeChainStart */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleComputeOrder (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleComputeOrder (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleComputeOrder _currn;
#ifdef __cplusplus
_currn = new _TPrRuleComputeOrder;
#else
_currn = (_TPPrRuleComputeOrder) TreeNodeAlloc (sizeof (struct _TPrRuleComputeOrder));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleComputeOrder;
_currn->_desc1 = (_TSPRule_OrderedComputation) MkRule_OrderedComputation (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleComputeOrder: root of subtree no. 1 can not be made a Rule_OrderedComputation node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleComputeOrder;
return ( (NODEPTR) _currn);
}/* MkrRuleComputeOrder */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleChainStart (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleChainStart (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleChainStart _currn;
#ifdef __cplusplus
_currn = new _TPrRuleChainStart;
#else
_currn = (_TPPrRuleChainStart) TreeNodeAlloc (sizeof (struct _TPrRuleChainStart));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleChainStart;
_currn->_desc1 = (_TSPRule_SymbolAttributeReference) MkRule_SymbolAttributeReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleChainStart: root of subtree no. 1 can not be made a Rule_SymbolAttributeReference node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleChainStart: root of subtree no. 2 can not be made a Rule_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleChainStart;
return ( (NODEPTR) _currn);
}/* MkrRuleChainStart */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleDefOcc (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleDefOcc (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleDefOcc _currn;
#ifdef __cplusplus
_currn = new _TPrRuleDefOcc;
#else
_currn = (_TPPrRuleDefOcc) TreeNodeAlloc (sizeof (struct _TPrRuleDefOcc));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleDefOcc;
_currn->_desc1 = (_TSPRule_SymbolAttributeReference) MkRule_SymbolAttributeReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleDefOcc: root of subtree no. 1 can not be made a Rule_SymbolAttributeReference node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_RHS_Expression) MkRule_RHS_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleDefOcc: root of subtree no. 2 can not be made a Rule_RHS_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleDefOcc;
return ( (NODEPTR) _currn);
}/* MkrRuleDefOcc */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoRuleDependency (POSITION *_coordref)
#else
NODEPTR MkrNoRuleDependency (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoRuleDependency _currn;
#ifdef __cplusplus
_currn = new _TPrNoRuleDependency;
#else
_currn = (_TPPrNoRuleDependency) TreeNodeAlloc (sizeof (struct _TPrNoRuleDependency));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoRuleDependency;
_SETCOORD(_currn)
_TERMACT_rNoRuleDependency;
return ( (NODEPTR) _currn);
}/* MkrNoRuleDependency */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleDependencies (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleDependencies (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleDependencies _currn;
#ifdef __cplusplus
_currn = new _TPrRuleDependencies;
#else
_currn = (_TPPrRuleDependencies) TreeNodeAlloc (sizeof (struct _TPrRuleDependencies));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleDependencies;
_currn->_desc1 = (_TSPRule_AttributeReferences) MkRule_AttributeReferences (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleDependencies: root of subtree no. 1 can not be made a Rule_AttributeReferences node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleDependencies;
return ( (NODEPTR) _currn);
}/* MkrRuleDependencies */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleAttributeReferenceSingle (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleAttributeReferenceSingle (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleAttributeReferenceSingle _currn;
#ifdef __cplusplus
_currn = new _TPrRuleAttributeReferenceSingle;
#else
_currn = (_TPPrRuleAttributeReferenceSingle) TreeNodeAlloc (sizeof (struct _TPrRuleAttributeReferenceSingle));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleAttributeReferenceSingle;
_currn->_desc1 = (_TSPRule_AttributeReference) MkRule_AttributeReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleAttributeReferenceSingle: root of subtree no. 1 can not be made a Rule_AttributeReference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleAttributeReferenceSingle;
return ( (NODEPTR) _currn);
}/* MkrRuleAttributeReferenceSingle */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleAttributeReferenceMulti (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleAttributeReferenceMulti (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleAttributeReferenceMulti _currn;
#ifdef __cplusplus
_currn = new _TPrRuleAttributeReferenceMulti;
#else
_currn = (_TPPrRuleAttributeReferenceMulti) TreeNodeAlloc (sizeof (struct _TPrRuleAttributeReferenceMulti));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleAttributeReferenceMulti;
_currn->_desc1 = (_TSPRule_AttributeReferenceList) MkRule_AttributeReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleAttributeReferenceMulti: root of subtree no. 1 can not be made a Rule_AttributeReferenceList node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleAttributeReferenceMulti;
return ( (NODEPTR) _currn);
}/* MkrRuleAttributeReferenceMulti */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleAttributeReferenceList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleAttributeReferenceList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleAttributeReferenceList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleAttributeReferenceList2;
#else
_currn = (_TPPrRuleAttributeReferenceList2) TreeNodeAlloc (sizeof (struct _TPrRuleAttributeReferenceList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleAttributeReferenceList2;
_currn->_desc1 = (_TSPRule_AttributeReferenceList) MkRule_AttributeReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleAttributeReferenceList2: root of subtree no. 1 can not be made a Rule_AttributeReferenceList node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_AttributeReference) MkRule_AttributeReference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleAttributeReferenceList2: root of subtree no. 2 can not be made a Rule_AttributeReference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleAttributeReferenceList2;
return ( (NODEPTR) _currn);
}/* MkrRuleAttributeReferenceList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleAttributeReferenceList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleAttributeReferenceList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleAttributeReferenceList1 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleAttributeReferenceList1;
#else
_currn = (_TPPrRuleAttributeReferenceList1) TreeNodeAlloc (sizeof (struct _TPrRuleAttributeReferenceList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleAttributeReferenceList1;
_currn->_desc1 = (_TSPRule_AttributeReference) MkRule_AttributeReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleAttributeReferenceList1: root of subtree no. 1 can not be made a Rule_AttributeReference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleAttributeReferenceList1;
return ( (NODEPTR) _currn);
}/* MkrRuleAttributeReferenceList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleAttributeReferenceIsSymbolAttrRef (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleAttributeReferenceIsSymbolAttrRef (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleAttributeReferenceIsSymbolAttrRef _currn;
#ifdef __cplusplus
_currn = new _TPrRuleAttributeReferenceIsSymbolAttrRef;
#else
_currn = (_TPPrRuleAttributeReferenceIsSymbolAttrRef) TreeNodeAlloc (sizeof (struct _TPrRuleAttributeReferenceIsSymbolAttrRef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleAttributeReferenceIsSymbolAttrRef;
_currn->_desc1 = (_TSPRule_SymbolAttributeReference) MkRule_SymbolAttributeReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleAttributeReferenceIsSymbolAttrRef: root of subtree no. 1 can not be made a Rule_SymbolAttributeReference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleAttributeReferenceIsSymbolAttrRef;
return ( (NODEPTR) _currn);
}/* MkrRuleAttributeReferenceIsSymbolAttrRef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleAttributeReferenceIsRemote (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleAttributeReferenceIsRemote (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleAttributeReferenceIsRemote _currn;
#ifdef __cplusplus
_currn = new _TPrRuleAttributeReferenceIsRemote;
#else
_currn = (_TPPrRuleAttributeReferenceIsRemote) TreeNodeAlloc (sizeof (struct _TPrRuleAttributeReferenceIsRemote));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleAttributeReferenceIsRemote;
_currn->_desc1 = (_TSPRemote_Attribute) MkRemote_Attribute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleAttributeReferenceIsRemote: root of subtree no. 1 can not be made a Remote_Attribute node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleAttributeReferenceIsRemote;
return ( (NODEPTR) _currn);
}/* MkrRuleAttributeReferenceIsRemote */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleSymbolAttrRefIsLidoStyle (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRuleSymbolAttrRefIsLidoStyle (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRuleSymbolAttrRefIsLidoStyle _currn;
#ifdef __cplusplus
_currn = new _TPrRuleSymbolAttrRefIsLidoStyle;
#else
_currn = (_TPPrRuleSymbolAttrRefIsLidoStyle) TreeNodeAlloc (sizeof (struct _TPrRuleSymbolAttrRefIsLidoStyle));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleSymbolAttrRefIsLidoStyle;
_currn->_desc1 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSymbolAttrRefIsLidoStyle: root of subtree no. 1 can not be made a Symbol_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_SymbolIndex) MkOpt_SymbolIndex (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSymbolAttrRefIsLidoStyle: root of subtree no. 2 can not be made a Opt_SymbolIndex node ", 0, _coordref);
_currn->_desc3 = (_TSPSymbol_AttributeReference) MkSymbol_AttributeReference (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSymbolAttrRefIsLidoStyle: root of subtree no. 3 can not be made a Symbol_AttributeReference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleSymbolAttrRefIsLidoStyle;
return ( (NODEPTR) _currn);
}/* MkrRuleSymbolAttrRefIsLidoStyle */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleSymbolAttrRefIsLidoListOfStyle (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleSymbolAttrRefIsLidoListOfStyle (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleSymbolAttrRefIsLidoListOfStyle _currn;
#ifdef __cplusplus
_currn = new _TPrRuleSymbolAttrRefIsLidoListOfStyle;
#else
_currn = (_TPPrRuleSymbolAttrRefIsLidoListOfStyle) TreeNodeAlloc (sizeof (struct _TPrRuleSymbolAttrRefIsLidoListOfStyle));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleSymbolAttrRefIsLidoListOfStyle;
_currn->_desc1 = (_TSPRule_AttributeClass) MkRule_AttributeClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSymbolAttrRefIsLidoListOfStyle: root of subtree no. 1 can not be made a Rule_AttributeClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_AttributeReference) MkSymbol_AttributeReference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSymbolAttrRefIsLidoListOfStyle: root of subtree no. 2 can not be made a Symbol_AttributeReference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleSymbolAttrRefIsLidoListOfStyle;
return ( (NODEPTR) _currn);
}/* MkrRuleSymbolAttrRefIsLidoListOfStyle */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleSymbolAttrRefIsVarReference (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleSymbolAttrRefIsVarReference (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleSymbolAttrRefIsVarReference _currn;
#ifdef __cplusplus
_currn = new _TPrRuleSymbolAttrRefIsVarReference;
#else
_currn = (_TPPrRuleSymbolAttrRefIsVarReference) TreeNodeAlloc (sizeof (struct _TPrRuleSymbolAttrRefIsVarReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleSymbolAttrRefIsVarReference;
_currn->_desc1 = (_TSPRule_VariableReference) MkRule_VariableReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSymbolAttrRefIsVarReference: root of subtree no. 1 can not be made a Rule_VariableReference node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_AttributeReference) MkSymbol_AttributeReference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleSymbolAttrRefIsVarReference: root of subtree no. 2 can not be made a Symbol_AttributeReference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleSymbolAttrRefIsVarReference;
return ( (NODEPTR) _currn);
}/* MkrRuleSymbolAttrRefIsVarReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleAttrClassIsTail (POSITION *_coordref)
#else
NODEPTR MkrRuleAttrClassIsTail (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRuleAttrClassIsTail _currn;
#ifdef __cplusplus
_currn = new _TPrRuleAttrClassIsTail;
#else
_currn = (_TPPrRuleAttrClassIsTail) TreeNodeAlloc (sizeof (struct _TPrRuleAttrClassIsTail));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleAttrClassIsTail;
_SETCOORD(_currn)
_TERMACT_rRuleAttrClassIsTail;
return ( (NODEPTR) _currn);
}/* MkrRuleAttrClassIsTail */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleAttrClassIsHead (POSITION *_coordref)
#else
NODEPTR MkrRuleAttrClassIsHead (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRuleAttrClassIsHead _currn;
#ifdef __cplusplus
_currn = new _TPrRuleAttrClassIsHead;
#else
_currn = (_TPPrRuleAttrClassIsHead) TreeNodeAlloc (sizeof (struct _TPrRuleAttrClassIsHead));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleAttrClassIsHead;
_SETCOORD(_currn)
_TERMACT_rRuleAttrClassIsHead;
return ( (NODEPTR) _currn);
}/* MkrRuleAttrClassIsHead */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleVariableReference (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleVariableReference (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleVariableReference _currn;
#ifdef __cplusplus
_currn = new _TPrRuleVariableReference;
#else
_currn = (_TPPrRuleVariableReference) TreeNodeAlloc (sizeof (struct _TPrRuleVariableReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleVariableReference;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleVariableReference: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleVariableReference;
return ( (NODEPTR) _currn);
}/* MkrRuleVariableReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoSymbolIndex (POSITION *_coordref)
#else
NODEPTR MkrNoSymbolIndex (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoSymbolIndex _currn;
#ifdef __cplusplus
_currn = new _TPrNoSymbolIndex;
#else
_currn = (_TPPrNoSymbolIndex) TreeNodeAlloc (sizeof (struct _TPrNoSymbolIndex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoSymbolIndex;
_SETCOORD(_currn)
_TERMACT_rNoSymbolIndex;
return ( (NODEPTR) _currn);
}/* MkrNoSymbolIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolIndex (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolIndex (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolIndex _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolIndex;
#else
_currn = (_TPPrSymbolIndex) TreeNodeAlloc (sizeof (struct _TPrSymbolIndex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolIndex;
_currn->_desc1 = (_TSPNumber) MkNumber (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolIndex: root of subtree no. 1 can not be made a Number node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolIndex;
return ( (NODEPTR) _currn);
}/* MkrSymbolIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolAttributeReference (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolAttributeReference (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolAttributeReference _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolAttributeReference;
#else
_currn = (_TPPrSymbolAttributeReference) TreeNodeAlloc (sizeof (struct _TPrSymbolAttributeReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolAttributeReference;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolAttributeReference: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolAttributeReference;
return ( (NODEPTR) _currn);
}/* MkrSymbolAttributeReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleOrderedComputation (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleOrderedComputation (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleOrderedComputation _currn;
#ifdef __cplusplus
_currn = new _TPrRuleOrderedComputation;
#else
_currn = (_TPPrRuleOrderedComputation) TreeNodeAlloc (sizeof (struct _TPrRuleOrderedComputation));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleOrderedComputation;
_currn->_desc1 = (_TSPRule_OrderedStatements) MkRule_OrderedStatements (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleOrderedComputation: root of subtree no. 1 can not be made a Rule_OrderedStatements node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleOrderedComputation;
return ( (NODEPTR) _currn);
}/* MkrRuleOrderedComputation */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleOrderedStatementList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleOrderedStatementList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleOrderedStatementList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleOrderedStatementList2;
#else
_currn = (_TPPrRuleOrderedStatementList2) TreeNodeAlloc (sizeof (struct _TPrRuleOrderedStatementList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleOrderedStatementList2;
_currn->_desc1 = (_TSPRule_OrderedStatements) MkRule_OrderedStatements (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleOrderedStatementList2: root of subtree no. 1 can not be made a Rule_OrderedStatements node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_OrderedStatement) MkRule_OrderedStatement (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleOrderedStatementList2: root of subtree no. 2 can not be made a Rule_OrderedStatement node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleOrderedStatementList2;
return ( (NODEPTR) _currn);
}/* MkrRuleOrderedStatementList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleOrderedStatementList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleOrderedStatementList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleOrderedStatementList1 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleOrderedStatementList1;
#else
_currn = (_TPPrRuleOrderedStatementList1) TreeNodeAlloc (sizeof (struct _TPrRuleOrderedStatementList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleOrderedStatementList1;
_currn->_desc1 = (_TSPRule_OrderedStatement) MkRule_OrderedStatement (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleOrderedStatementList1: root of subtree no. 1 can not be made a Rule_OrderedStatement node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleOrderedStatementList1;
return ( (NODEPTR) _currn);
}/* MkrRuleOrderedStatementList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleOrderedStatementIsExpression (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleOrderedStatementIsExpression (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleOrderedStatementIsExpression _currn;
#ifdef __cplusplus
_currn = new _TPrRuleOrderedStatementIsExpression;
#else
_currn = (_TPPrRuleOrderedStatementIsExpression) TreeNodeAlloc (sizeof (struct _TPrRuleOrderedStatementIsExpression));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleOrderedStatementIsExpression;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleOrderedStatementIsExpression: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleOrderedStatementIsExpression;
return ( (NODEPTR) _currn);
}/* MkrRuleOrderedStatementIsExpression */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleOrderedStatementIsAssign (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleOrderedStatementIsAssign (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleOrderedStatementIsAssign _currn;
#ifdef __cplusplus
_currn = new _TPrRuleOrderedStatementIsAssign;
#else
_currn = (_TPPrRuleOrderedStatementIsAssign) TreeNodeAlloc (sizeof (struct _TPrRuleOrderedStatementIsAssign));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleOrderedStatementIsAssign;
_currn->_desc1 = (_TSPRule_SymbolAttributeReference) MkRule_SymbolAttributeReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleOrderedStatementIsAssign: root of subtree no. 1 can not be made a Rule_SymbolAttributeReference node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_RHS_Expression) MkRule_RHS_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleOrderedStatementIsAssign: root of subtree no. 2 can not be made a Rule_RHS_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleOrderedStatementIsAssign;
return ( (NODEPTR) _currn);
}/* MkrRuleOrderedStatementIsAssign */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleOrderedStatementIsReturn (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleOrderedStatementIsReturn (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleOrderedStatementIsReturn _currn;
#ifdef __cplusplus
_currn = new _TPrRuleOrderedStatementIsReturn;
#else
_currn = (_TPPrRuleOrderedStatementIsReturn) TreeNodeAlloc (sizeof (struct _TPrRuleOrderedStatementIsReturn));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleOrderedStatementIsReturn;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleOrderedStatementIsReturn: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleOrderedStatementIsReturn;
return ( (NODEPTR) _currn);
}/* MkrRuleOrderedStatementIsReturn */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleRhsExprIsExpr (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleRhsExprIsExpr (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleRhsExprIsExpr _currn;
#ifdef __cplusplus
_currn = new _TPrRuleRhsExprIsExpr;
#else
_currn = (_TPPrRuleRhsExprIsExpr) TreeNodeAlloc (sizeof (struct _TPrRuleRhsExprIsExpr));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleRhsExprIsExpr;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleRhsExprIsExpr: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleRhsExprIsExpr;
return ( (NODEPTR) _currn);
}/* MkrRuleRhsExprIsExpr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleRhsExprIsGuards (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleRhsExprIsGuards (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleRhsExprIsGuards _currn;
#ifdef __cplusplus
_currn = new _TPrRuleRhsExprIsGuards;
#else
_currn = (_TPPrRuleRhsExprIsGuards) TreeNodeAlloc (sizeof (struct _TPrRuleRhsExprIsGuards));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleRhsExprIsGuards;
_currn->_desc1 = (_TSPRule_GuardExpressions) MkRule_GuardExpressions (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleRhsExprIsGuards: root of subtree no. 1 can not be made a Rule_GuardExpressions node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleRhsExprIsGuards;
return ( (NODEPTR) _currn);
}/* MkrRuleRhsExprIsGuards */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleGuardExpressionList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleGuardExpressionList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleGuardExpressionList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleGuardExpressionList2;
#else
_currn = (_TPPrRuleGuardExpressionList2) TreeNodeAlloc (sizeof (struct _TPrRuleGuardExpressionList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleGuardExpressionList2;
_currn->_desc1 = (_TSPRule_GuardExpressions) MkRule_GuardExpressions (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardExpressionList2: root of subtree no. 1 can not be made a Rule_GuardExpressions node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_GuardExpression) MkRule_GuardExpression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardExpressionList2: root of subtree no. 2 can not be made a Rule_GuardExpression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleGuardExpressionList2;
return ( (NODEPTR) _currn);
}/* MkrRuleGuardExpressionList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleGuardExpressionList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleGuardExpressionList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleGuardExpressionList1 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleGuardExpressionList1;
#else
_currn = (_TPPrRuleGuardExpressionList1) TreeNodeAlloc (sizeof (struct _TPrRuleGuardExpressionList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleGuardExpressionList1;
_currn->_desc1 = (_TSPRule_GuardExpression) MkRule_GuardExpression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardExpressionList1: root of subtree no. 1 can not be made a Rule_GuardExpression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleGuardExpressionList1;
return ( (NODEPTR) _currn);
}/* MkrRuleGuardExpressionList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleGuardIsOr (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleGuardIsOr (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleGuardIsOr _currn;
#ifdef __cplusplus
_currn = new _TPrRuleGuardIsOr;
#else
_currn = (_TPPrRuleGuardIsOr) TreeNodeAlloc (sizeof (struct _TPrRuleGuardIsOr));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleGuardIsOr;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardIsOr: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_GuardExpressions) MkRule_GuardExpressions (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardIsOr: root of subtree no. 2 can not be made a Rule_GuardExpressions node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleGuardIsOr;
return ( (NODEPTR) _currn);
}/* MkrRuleGuardIsOr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleGuardIsCompute (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleGuardIsCompute (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleGuardIsCompute _currn;
#ifdef __cplusplus
_currn = new _TPrRuleGuardIsCompute;
#else
_currn = (_TPPrRuleGuardIsCompute) TreeNodeAlloc (sizeof (struct _TPrRuleGuardIsCompute));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleGuardIsCompute;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardIsCompute: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardIsCompute: root of subtree no. 2 can not be made a Rule_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleGuardIsCompute;
return ( (NODEPTR) _currn);
}/* MkrRuleGuardIsCompute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleGuardIsDefault (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleGuardIsDefault (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleGuardIsDefault _currn;
#ifdef __cplusplus
_currn = new _TPrRuleGuardIsDefault;
#else
_currn = (_TPPrRuleGuardIsDefault) TreeNodeAlloc (sizeof (struct _TPrRuleGuardIsDefault));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleGuardIsDefault;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleGuardIsDefault: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleGuardIsDefault;
return ( (NODEPTR) _currn);
}/* MkrRuleGuardIsDefault */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionChain (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionChain (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionChain _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionChain;
#else
_currn = (_TPPrRuleExpressionChain) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionChain));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionChain;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionChain: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionChain;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionChain */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionIsWhen (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionIsWhen (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionIsWhen _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionIsWhen;
#else
_currn = (_TPPrRuleExpressionIsWhen) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionIsWhen));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionIsWhen;
_currn->_desc1 = (_TSPRule_Expression_When) MkRule_Expression_When (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIsWhen: root of subtree no. 1 can not be made a Rule_Expression_When node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionIsWhen;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionIsWhen */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionIsError (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionIsError (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionIsError _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionIsError;
#else
_currn = (_TPPrRuleExpressionIsError) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionIsError));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionIsError;
_currn->_desc1 = (_TSPRule_Expression_Error) MkRule_Expression_Error (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIsError: root of subtree no. 1 can not be made a Rule_Expression_Error node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionIsError;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionIsError */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionIsIf (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionIsIf (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionIsIf _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionIsIf;
#else
_currn = (_TPPrRuleExpressionIsIf) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionIsIf));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionIsIf;
_currn->_desc1 = (_TSPRule_Expression_If) MkRule_Expression_If (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIsIf: root of subtree no. 1 can not be made a Rule_Expression_If node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionIsIf;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionIsIf */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionIsLet (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionIsLet (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionIsLet _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionIsLet;
#else
_currn = (_TPPrRuleExpressionIsLet) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionIsLet));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionIsLet;
_currn->_desc1 = (_TSPRule_Expression_Let) MkRule_Expression_Let (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIsLet: root of subtree no. 1 can not be made a Rule_Expression_Let node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionIsLet;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionIsLet */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionIsLambda (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionIsLambda (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionIsLambda _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionIsLambda;
#else
_currn = (_TPPrRuleExpressionIsLambda) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionIsLambda));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionIsLambda;
_currn->_desc1 = (_TSPRule_Lambda) MkRule_Lambda (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIsLambda: root of subtree no. 1 can not be made a Rule_Lambda node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionIsLambda;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionIsLambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionIsOrder (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionIsOrder (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionIsOrder _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionIsOrder;
#else
_currn = (_TPPrRuleExpressionIsOrder) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionIsOrder));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionIsOrder;
_currn->_desc1 = (_TSPRule_OrderedComputation) MkRule_OrderedComputation (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIsOrder: root of subtree no. 1 can not be made a Rule_OrderedComputation node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionIsOrder;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionIsOrder */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleLambda (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRuleLambda (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRuleLambda _currn;
#ifdef __cplusplus
_currn = new _TPrRuleLambda;
#else
_currn = (_TPPrRuleLambda) TreeNodeAlloc (sizeof (struct _TPrRuleLambda));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleLambda;
_currn->_desc1 = (_TSPLambda) MkLambda (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLambda: root of subtree no. 1 can not be made a Lambda node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Lambda_VarDefs) MkRule_Lambda_VarDefs (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLambda: root of subtree no. 2 can not be made a Rule_Lambda_VarDefs node ", 0, _coordref);
_currn->_desc3 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLambda: root of subtree no. 3 can not be made a Rule_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleLambda;
return ( (NODEPTR) _currn);
}/* MkrRuleLambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleLambdaVarDefList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleLambdaVarDefList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleLambdaVarDefList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleLambdaVarDefList2;
#else
_currn = (_TPPrRuleLambdaVarDefList2) TreeNodeAlloc (sizeof (struct _TPrRuleLambdaVarDefList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleLambdaVarDefList2;
_currn->_desc1 = (_TSPRule_Lambda_VarDefs) MkRule_Lambda_VarDefs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLambdaVarDefList2: root of subtree no. 1 can not be made a Rule_Lambda_VarDefs node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Lambda_VarDef) MkRule_Lambda_VarDef (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLambdaVarDefList2: root of subtree no. 2 can not be made a Rule_Lambda_VarDef node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleLambdaVarDefList2;
return ( (NODEPTR) _currn);
}/* MkrRuleLambdaVarDefList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleLambdaVarDefList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleLambdaVarDefList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleLambdaVarDefList1 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleLambdaVarDefList1;
#else
_currn = (_TPPrRuleLambdaVarDefList1) TreeNodeAlloc (sizeof (struct _TPrRuleLambdaVarDefList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleLambdaVarDefList1;
_currn->_desc1 = (_TSPRule_Lambda_VarDef) MkRule_Lambda_VarDef (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLambdaVarDefList1: root of subtree no. 1 can not be made a Rule_Lambda_VarDef node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleLambdaVarDefList1;
return ( (NODEPTR) _currn);
}/* MkrRuleLambdaVarDefList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleLambdaVarDef (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleLambdaVarDef (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleLambdaVarDef _currn;
#ifdef __cplusplus
_currn = new _TPrRuleLambdaVarDef;
#else
_currn = (_TPPrRuleLambdaVarDef) TreeNodeAlloc (sizeof (struct _TPrRuleLambdaVarDef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleLambdaVarDef;
_currn->_desc1 = (_TSPRule_VariableDefId) MkRule_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLambdaVarDef: root of subtree no. 1 can not be made a Rule_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPOptionalType) MkOptionalType (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLambdaVarDef: root of subtree no. 2 can not be made a OptionalType node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleLambdaVarDef;
return ( (NODEPTR) _currn);
}/* MkrRuleLambdaVarDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleVariableDefId (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleVariableDefId (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleVariableDefId _currn;
#ifdef __cplusplus
_currn = new _TPrRuleVariableDefId;
#else
_currn = (_TPPrRuleVariableDefId) TreeNodeAlloc (sizeof (struct _TPrRuleVariableDefId));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleVariableDefId;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleVariableDefId: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleVariableDefId;
return ( (NODEPTR) _currn);
}/* MkrRuleVariableDefId */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionLet (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionLet (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionLet _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionLet;
#else
_currn = (_TPPrRuleExpressionLet) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionLet));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionLet;
_currn->_desc1 = (_TSPRule_LetVar_Defs) MkRule_LetVar_Defs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionLet: root of subtree no. 1 can not be made a Rule_LetVar_Defs node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionLet: root of subtree no. 2 can not be made a Rule_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionLet;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionLet */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleLetVarDefList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleLetVarDefList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleLetVarDefList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleLetVarDefList2;
#else
_currn = (_TPPrRuleLetVarDefList2) TreeNodeAlloc (sizeof (struct _TPrRuleLetVarDefList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleLetVarDefList2;
_currn->_desc1 = (_TSPRule_LetVar_Defs) MkRule_LetVar_Defs (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLetVarDefList2: root of subtree no. 1 can not be made a Rule_LetVar_Defs node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_LetVar_Def) MkRule_LetVar_Def (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLetVarDefList2: root of subtree no. 2 can not be made a Rule_LetVar_Def node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleLetVarDefList2;
return ( (NODEPTR) _currn);
}/* MkrRuleLetVarDefList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleLetVarDefList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleLetVarDefList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleLetVarDefList1 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleLetVarDefList1;
#else
_currn = (_TPPrRuleLetVarDefList1) TreeNodeAlloc (sizeof (struct _TPrRuleLetVarDefList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleLetVarDefList1;
_currn->_desc1 = (_TSPRule_LetVar_Def) MkRule_LetVar_Def (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLetVarDefList1: root of subtree no. 1 can not be made a Rule_LetVar_Def node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleLetVarDefList1;
return ( (NODEPTR) _currn);
}/* MkrRuleLetVarDefList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleLetVarDef (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRuleLetVarDef (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRuleLetVarDef _currn;
#ifdef __cplusplus
_currn = new _TPrRuleLetVarDef;
#else
_currn = (_TPPrRuleLetVarDef) TreeNodeAlloc (sizeof (struct _TPrRuleLetVarDef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleLetVarDef;
_currn->_desc1 = (_TSPRule_VariableDefId) MkRule_VariableDefId (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLetVarDef: root of subtree no. 1 can not be made a Rule_VariableDefId node ", 0, _coordref);
_currn->_desc2 = (_TSPOptionalType) MkOptionalType (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLetVarDef: root of subtree no. 2 can not be made a OptionalType node ", 0, _coordref);
_currn->_desc3 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleLetVarDef: root of subtree no. 3 can not be made a Rule_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleLetVarDef;
return ( (NODEPTR) _currn);
}/* MkrRuleLetVarDef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionIf (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRuleExpressionIf (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRuleExpressionIf _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionIf;
#else
_currn = (_TPPrRuleExpressionIf) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionIf));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionIf;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIf: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIf: root of subtree no. 2 can not be made a Rule_Expression node ", 0, _coordref);
_currn->_desc3 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIf: root of subtree no. 3 can not be made a Rule_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionIf;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionIf */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionError (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionError (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionError _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionError;
#else
_currn = (_TPPrRuleExpressionError) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionError));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionError;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionError: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionError;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionError */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionWhenCond (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionWhenCond (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionWhenCond _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionWhenCond;
#else
_currn = (_TPPrRuleExpressionWhenCond) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionWhenCond));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionWhenCond;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionWhenCond: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionWhenCond: root of subtree no. 2 can not be made a Rule_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionWhenCond;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionWhenCond */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionWhen (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionWhen (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionWhen _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionWhen;
#else
_currn = (_TPPrRuleExpressionWhen) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionWhen));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionWhen;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionWhen: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionWhen: root of subtree no. 2 can not be made a Rule_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionWhen;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionWhen */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionIsBinary (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionIsBinary (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionIsBinary _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionIsBinary;
#else
_currn = (_TPPrRuleExpressionIsBinary) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionIsBinary));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionIsBinary;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionIsBinary: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionIsBinary;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionIsBinary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryChain (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionBinaryChain (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionBinaryChain _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryChain;
#else
_currn = (_TPPrRuleExpressionBinaryChain) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryChain));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryChain;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryChain: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionBinaryChain;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryChain */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryOR (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryOR (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryOR _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryOR;
#else
_currn = (_TPPrRuleExpressionBinaryOR) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryOR));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryOR;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryOR: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryOR: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryOR;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryOR */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryAND (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryAND (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryAND _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryAND;
#else
_currn = (_TPPrRuleExpressionBinaryAND) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryAND));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryAND;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryAND: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryAND: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryAND;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryAND */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryEQ (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryEQ (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryEQ _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryEQ;
#else
_currn = (_TPPrRuleExpressionBinaryEQ) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryEQ));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryEQ;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryEQ: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryEQ: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryEQ;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryEQ */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryNE (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryNE (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryNE _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryNE;
#else
_currn = (_TPPrRuleExpressionBinaryNE) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryNE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryNE;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryNE: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryNE: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryNE;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryNE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryLT (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryLT (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryLT _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryLT;
#else
_currn = (_TPPrRuleExpressionBinaryLT) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryLT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryLT;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryLT: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryLT: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryLT;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryLT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryGT (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryGT (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryGT _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryGT;
#else
_currn = (_TPPrRuleExpressionBinaryGT) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryGT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryGT;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryGT: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryGT: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryGT;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryGT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryLE (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryLE (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryLE _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryLE;
#else
_currn = (_TPPrRuleExpressionBinaryLE) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryLE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryLE;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryLE: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryLE: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryLE;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryLE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryGE (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryGE (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryGE _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryGE;
#else
_currn = (_TPPrRuleExpressionBinaryGE) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryGE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryGE;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryGE: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryGE: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryGE;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryGE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryConcat (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryConcat (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryConcat _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryConcat;
#else
_currn = (_TPPrRuleExpressionBinaryConcat) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryConcat));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryConcat;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryConcat: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryConcat: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryConcat;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryConcat */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryADD (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryADD (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryADD _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryADD;
#else
_currn = (_TPPrRuleExpressionBinaryADD) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryADD));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryADD;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryADD: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryADD: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryADD;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryADD */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinarySUB (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinarySUB (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinarySUB _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinarySUB;
#else
_currn = (_TPPrRuleExpressionBinarySUB) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinarySUB));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinarySUB;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinarySUB: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinarySUB: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinarySUB;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinarySUB */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryMUL (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryMUL (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryMUL _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryMUL;
#else
_currn = (_TPPrRuleExpressionBinaryMUL) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryMUL));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryMUL;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryMUL: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryMUL: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryMUL;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryMUL */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryDIV (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryDIV (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryDIV _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryDIV;
#else
_currn = (_TPPrRuleExpressionBinaryDIV) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryDIV));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryDIV;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryDIV: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryDIV: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryDIV;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryDIV */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryMOD (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionBinaryMOD (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionBinaryMOD _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryMOD;
#else
_currn = (_TPPrRuleExpressionBinaryMOD) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryMOD));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryMOD;
_currn->_desc1 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryMOD: root of subtree no. 1 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Binary) MkRule_Expression_Binary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryMOD: root of subtree no. 2 can not be made a Rule_Expression_Binary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionBinaryMOD;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryMOD */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionBinaryIsUnary (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionBinaryIsUnary (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionBinaryIsUnary _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionBinaryIsUnary;
#else
_currn = (_TPPrRuleExpressionBinaryIsUnary) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionBinaryIsUnary));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionBinaryIsUnary;
_currn->_desc1 = (_TSPRule_Expression_Unary) MkRule_Expression_Unary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionBinaryIsUnary: root of subtree no. 1 can not be made a Rule_Expression_Unary node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionBinaryIsUnary;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionBinaryIsUnary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionUnaryIsPostfix (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionUnaryIsPostfix (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionUnaryIsPostfix _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionUnaryIsPostfix;
#else
_currn = (_TPPrRuleExpressionUnaryIsPostfix) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionUnaryIsPostfix));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionUnaryIsPostfix;
_currn->_desc1 = (_TSPRule_Expression_Postfix) MkRule_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionUnaryIsPostfix: root of subtree no. 1 can not be made a Rule_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionUnaryIsPostfix;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionUnaryIsPostfix */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionUnaryIncr (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionUnaryIncr (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionUnaryIncr _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionUnaryIncr;
#else
_currn = (_TPPrRuleExpressionUnaryIncr) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionUnaryIncr));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionUnaryIncr;
_currn->_desc1 = (_TSPRule_Expression_Postfix) MkRule_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionUnaryIncr: root of subtree no. 1 can not be made a Rule_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionUnaryIncr;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionUnaryIncr */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionUnaryNEG (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionUnaryNEG (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionUnaryNEG _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionUnaryNEG;
#else
_currn = (_TPPrRuleExpressionUnaryNEG) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionUnaryNEG));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionUnaryNEG;
_currn->_desc1 = (_TSPRule_Expression_Postfix) MkRule_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionUnaryNEG: root of subtree no. 1 can not be made a Rule_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionUnaryNEG;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionUnaryNEG */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionUnaryNOT (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionUnaryNOT (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionUnaryNOT _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionUnaryNOT;
#else
_currn = (_TPPrRuleExpressionUnaryNOT) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionUnaryNOT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionUnaryNOT;
_currn->_desc1 = (_TSPRule_Expression_Postfix) MkRule_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionUnaryNOT: root of subtree no. 1 can not be made a Rule_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionUnaryNOT;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionUnaryNOT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionUnaryAddress (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionUnaryAddress (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionUnaryAddress _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionUnaryAddress;
#else
_currn = (_TPPrRuleExpressionUnaryAddress) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionUnaryAddress));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionUnaryAddress;
_currn->_desc1 = (_TSPRule_Expression_Postfix) MkRule_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionUnaryAddress: root of subtree no. 1 can not be made a Rule_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionUnaryAddress;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionUnaryAddress */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionUnaryReference (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionUnaryReference (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionUnaryReference _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionUnaryReference;
#else
_currn = (_TPPrRuleExpressionUnaryReference) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionUnaryReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionUnaryReference;
_currn->_desc1 = (_TSPRule_Expression_Postfix) MkRule_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionUnaryReference: root of subtree no. 1 can not be made a Rule_Expression_Postfix node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionUnaryReference;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionUnaryReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionPostfixIsPrimary (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionPostfixIsPrimary (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionPostfixIsPrimary _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionPostfixIsPrimary;
#else
_currn = (_TPPrRuleExpressionPostfixIsPrimary) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionPostfixIsPrimary));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionPostfixIsPrimary;
_currn->_desc1 = (_TSPRule_Expression_Primary) MkRule_Expression_Primary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPostfixIsPrimary: root of subtree no. 1 can not be made a Rule_Expression_Primary node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionPostfixIsPrimary;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionPostfixIsPrimary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionPostfixIsIndex (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionPostfixIsIndex (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionPostfixIsIndex _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionPostfixIsIndex;
#else
_currn = (_TPPrRuleExpressionPostfixIsIndex) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionPostfixIsIndex));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionPostfixIsIndex;
_currn->_desc1 = (_TSPRule_Expression_Postfix) MkRule_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPostfixIsIndex: root of subtree no. 1 can not be made a Rule_Expression_Postfix node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPostfixIsIndex: root of subtree no. 2 can not be made a Rule_Expression node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionPostfixIsIndex;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionPostfixIsIndex */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionPostfixIsAccess (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionPostfixIsAccess (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionPostfixIsAccess _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionPostfixIsAccess;
#else
_currn = (_TPPrRuleExpressionPostfixIsAccess) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionPostfixIsAccess));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionPostfixIsAccess;
_currn->_desc1 = (_TSPRule_Expression_Postfix) MkRule_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPostfixIsAccess: root of subtree no. 1 can not be made a Rule_Expression_Postfix node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Primary) MkRule_Expression_Primary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPostfixIsAccess: root of subtree no. 2 can not be made a Rule_Expression_Primary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionPostfixIsAccess;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionPostfixIsAccess */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionPostfixIsListcon (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionPostfixIsListcon (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionPostfixIsListcon _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionPostfixIsListcon;
#else
_currn = (_TPPrRuleExpressionPostfixIsListcon) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionPostfixIsListcon));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionPostfixIsListcon;
_currn->_desc1 = (_TSPRule_Expression_Postfix) MkRule_Expression_Postfix (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPostfixIsListcon: root of subtree no. 1 can not be made a Rule_Expression_Postfix node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_Expression_Primary) MkRule_Expression_Primary (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPostfixIsListcon: root of subtree no. 2 can not be made a Rule_Expression_Primary node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionPostfixIsListcon;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionPostfixIsListcon */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionPrimaryIsConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionPrimaryIsConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionPrimaryIsConstant _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionPrimaryIsConstant;
#else
_currn = (_TPPrRuleExpressionPrimaryIsConstant) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionPrimaryIsConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionPrimaryIsConstant;
_currn->_desc1 = (_TSPRule_Constant) MkRule_Constant (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPrimaryIsConstant: root of subtree no. 1 can not be made a Rule_Constant node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionPrimaryIsConstant;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionPrimaryIsConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionPrimaryIsCall (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionPrimaryIsCall (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionPrimaryIsCall _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionPrimaryIsCall;
#else
_currn = (_TPPrRuleExpressionPrimaryIsCall) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionPrimaryIsCall));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionPrimaryIsCall;
_currn->_desc1 = (_TSPRule_Expression_Call) MkRule_Expression_Call (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPrimaryIsCall: root of subtree no. 1 can not be made a Rule_Expression_Call node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionPrimaryIsCall;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionPrimaryIsCall */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionPrimaryIsTuple (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionPrimaryIsTuple (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionPrimaryIsTuple _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionPrimaryIsTuple;
#else
_currn = (_TPPrRuleExpressionPrimaryIsTuple) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionPrimaryIsTuple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionPrimaryIsTuple;
_currn->_desc1 = (_TSPRule_TupleConstruction) MkRule_TupleConstruction (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPrimaryIsTuple: root of subtree no. 1 can not be made a Rule_TupleConstruction node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionPrimaryIsTuple;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionPrimaryIsTuple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionPrimaryIsRemoteAttribute (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionPrimaryIsRemoteAttribute (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionPrimaryIsRemoteAttribute _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionPrimaryIsRemoteAttribute;
#else
_currn = (_TPPrRuleExpressionPrimaryIsRemoteAttribute) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionPrimaryIsRemoteAttribute));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionPrimaryIsRemoteAttribute;
_currn->_desc1 = (_TSPRemote_Attribute) MkRemote_Attribute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPrimaryIsRemoteAttribute: root of subtree no. 1 can not be made a Remote_Attribute node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionPrimaryIsRemoteAttribute;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionPrimaryIsRemoteAttribute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionPrimaryIsWrap (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionPrimaryIsWrap (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionPrimaryIsWrap _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionPrimaryIsWrap;
#else
_currn = (_TPPrRuleExpressionPrimaryIsWrap) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionPrimaryIsWrap));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionPrimaryIsWrap;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionPrimaryIsWrap: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionPrimaryIsWrap;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionPrimaryIsWrap */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleTupleConstruction (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleTupleConstruction (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleTupleConstruction _currn;
#ifdef __cplusplus
_currn = new _TPrRuleTupleConstruction;
#else
_currn = (_TPPrRuleTupleConstruction) TreeNodeAlloc (sizeof (struct _TPrRuleTupleConstruction));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleTupleConstruction;
_currn->_desc1 = (_TSPRule_TupleArguments) MkRule_TupleArguments (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleTupleConstruction: root of subtree no. 1 can not be made a Rule_TupleArguments node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_TupleArgument) MkRule_TupleArgument (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleTupleConstruction: root of subtree no. 2 can not be made a Rule_TupleArgument node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleTupleConstruction;
return ( (NODEPTR) _currn);
}/* MkrRuleTupleConstruction */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleTupleArgumentList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleTupleArgumentList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleTupleArgumentList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleTupleArgumentList2;
#else
_currn = (_TPPrRuleTupleArgumentList2) TreeNodeAlloc (sizeof (struct _TPrRuleTupleArgumentList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleTupleArgumentList2;
_currn->_desc1 = (_TSPRule_TupleArguments) MkRule_TupleArguments (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleTupleArgumentList2: root of subtree no. 1 can not be made a Rule_TupleArguments node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_TupleArgument) MkRule_TupleArgument (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleTupleArgumentList2: root of subtree no. 2 can not be made a Rule_TupleArgument node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleTupleArgumentList2;
return ( (NODEPTR) _currn);
}/* MkrRuleTupleArgumentList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleTupleArgumentList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleTupleArgumentList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleTupleArgumentList1 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleTupleArgumentList1;
#else
_currn = (_TPPrRuleTupleArgumentList1) TreeNodeAlloc (sizeof (struct _TPrRuleTupleArgumentList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleTupleArgumentList1;
_currn->_desc1 = (_TSPRule_TupleArgument) MkRule_TupleArgument (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleTupleArgumentList1: root of subtree no. 1 can not be made a Rule_TupleArgument node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleTupleArgumentList1;
return ( (NODEPTR) _currn);
}/* MkrRuleTupleArgumentList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleTupleArgument (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleTupleArgument (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleTupleArgument _currn;
#ifdef __cplusplus
_currn = new _TPrRuleTupleArgument;
#else
_currn = (_TPPrRuleTupleArgument) TreeNodeAlloc (sizeof (struct _TPrRuleTupleArgument));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleTupleArgument;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleTupleArgument: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleTupleArgument;
return ( (NODEPTR) _currn);
}/* MkrRuleTupleArgument */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionCallApplied (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleExpressionCallApplied (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleExpressionCallApplied _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionCallApplied;
#else
_currn = (_TPPrRuleExpressionCallApplied) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionCallApplied));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionCallApplied;
_currn->_desc1 = (_TSPRule_CallableReference) MkRule_CallableReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionCallApplied: root of subtree no. 1 can not be made a Rule_CallableReference node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_CallParameters) MkRule_CallParameters (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionCallApplied: root of subtree no. 2 can not be made a Rule_CallParameters node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleExpressionCallApplied;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionCallApplied */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionCallEmpty (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionCallEmpty (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionCallEmpty _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionCallEmpty;
#else
_currn = (_TPPrRuleExpressionCallEmpty) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionCallEmpty));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionCallEmpty;
_currn->_desc1 = (_TSPRule_CallableReference) MkRule_CallableReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionCallEmpty: root of subtree no. 1 can not be made a Rule_CallableReference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionCallEmpty;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionCallEmpty */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleExpressionCallVariable (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleExpressionCallVariable (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleExpressionCallVariable _currn;
#ifdef __cplusplus
_currn = new _TPrRuleExpressionCallVariable;
#else
_currn = (_TPPrRuleExpressionCallVariable) TreeNodeAlloc (sizeof (struct _TPrRuleExpressionCallVariable));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleExpressionCallVariable;
_currn->_desc1 = (_TSPRule_CallableReference) MkRule_CallableReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleExpressionCallVariable: root of subtree no. 1 can not be made a Rule_CallableReference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleExpressionCallVariable;
return ( (NODEPTR) _currn);
}/* MkrRuleExpressionCallVariable */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleCallableReferenceIsIdentifier (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleCallableReferenceIsIdentifier (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleCallableReferenceIsIdentifier _currn;
#ifdef __cplusplus
_currn = new _TPrRuleCallableReferenceIsIdentifier;
#else
_currn = (_TPPrRuleCallableReferenceIsIdentifier) TreeNodeAlloc (sizeof (struct _TPrRuleCallableReferenceIsIdentifier));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleCallableReferenceIsIdentifier;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleCallableReferenceIsIdentifier: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleCallableReferenceIsIdentifier;
return ( (NODEPTR) _currn);
}/* MkrRuleCallableReferenceIsIdentifier */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleCallableReferenceIsTypename (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleCallableReferenceIsTypename (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleCallableReferenceIsTypename _currn;
#ifdef __cplusplus
_currn = new _TPrRuleCallableReferenceIsTypename;
#else
_currn = (_TPPrRuleCallableReferenceIsTypename) TreeNodeAlloc (sizeof (struct _TPrRuleCallableReferenceIsTypename));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleCallableReferenceIsTypename;
_currn->_desc1 = (_TSPTypename) MkTypename (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleCallableReferenceIsTypename: root of subtree no. 1 can not be made a Typename node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleCallableReferenceIsTypename;
return ( (NODEPTR) _currn);
}/* MkrRuleCallableReferenceIsTypename */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleCallableReferenceIsEnd (POSITION *_coordref)
#else
NODEPTR MkrRuleCallableReferenceIsEnd (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRuleCallableReferenceIsEnd _currn;
#ifdef __cplusplus
_currn = new _TPrRuleCallableReferenceIsEnd;
#else
_currn = (_TPPrRuleCallableReferenceIsEnd) TreeNodeAlloc (sizeof (struct _TPrRuleCallableReferenceIsEnd));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleCallableReferenceIsEnd;
_SETCOORD(_currn)
_TERMACT_rRuleCallableReferenceIsEnd;
return ( (NODEPTR) _currn);
}/* MkrRuleCallableReferenceIsEnd */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleCallableReferenceIsAttribute (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleCallableReferenceIsAttribute (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleCallableReferenceIsAttribute _currn;
#ifdef __cplusplus
_currn = new _TPrRuleCallableReferenceIsAttribute;
#else
_currn = (_TPPrRuleCallableReferenceIsAttribute) TreeNodeAlloc (sizeof (struct _TPrRuleCallableReferenceIsAttribute));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleCallableReferenceIsAttribute;
_currn->_desc1 = (_TSPRule_SymbolAttributeReference) MkRule_SymbolAttributeReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleCallableReferenceIsAttribute: root of subtree no. 1 can not be made a Rule_SymbolAttributeReference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleCallableReferenceIsAttribute;
return ( (NODEPTR) _currn);
}/* MkrRuleCallableReferenceIsAttribute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleCallableReferenceIsRule (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleCallableReferenceIsRule (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleCallableReferenceIsRule _currn;
#ifdef __cplusplus
_currn = new _TPrRuleCallableReferenceIsRule;
#else
_currn = (_TPPrRuleCallableReferenceIsRule) TreeNodeAlloc (sizeof (struct _TPrRuleCallableReferenceIsRule));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleCallableReferenceIsRule;
_currn->_desc1 = (_TSPRule_Reference) MkRule_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleCallableReferenceIsRule: root of subtree no. 1 can not be made a Rule_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_TreeHint) MkOpt_TreeHint (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleCallableReferenceIsRule: root of subtree no. 2 can not be made a Opt_TreeHint node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleCallableReferenceIsRule;
return ( (NODEPTR) _currn);
}/* MkrRuleCallableReferenceIsRule */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleCallParameterList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRuleCallParameterList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRuleCallParameterList2 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleCallParameterList2;
#else
_currn = (_TPPrRuleCallParameterList2) TreeNodeAlloc (sizeof (struct _TPrRuleCallParameterList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleCallParameterList2;
_currn->_desc1 = (_TSPRule_CallParameters) MkRule_CallParameters (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleCallParameterList2: root of subtree no. 1 can not be made a Rule_CallParameters node ", 0, _coordref);
_currn->_desc2 = (_TSPRule_CallParameter) MkRule_CallParameter (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleCallParameterList2: root of subtree no. 2 can not be made a Rule_CallParameter node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRuleCallParameterList2;
return ( (NODEPTR) _currn);
}/* MkrRuleCallParameterList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleCallParameterList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleCallParameterList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleCallParameterList1 _currn;
#ifdef __cplusplus
_currn = new _TPrRuleCallParameterList1;
#else
_currn = (_TPPrRuleCallParameterList1) TreeNodeAlloc (sizeof (struct _TPrRuleCallParameterList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleCallParameterList1;
_currn->_desc1 = (_TSPRule_CallParameter) MkRule_CallParameter (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleCallParameterList1: root of subtree no. 1 can not be made a Rule_CallParameter node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleCallParameterList1;
return ( (NODEPTR) _currn);
}/* MkrRuleCallParameterList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleCallParameter (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleCallParameter (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleCallParameter _currn;
#ifdef __cplusplus
_currn = new _TPrRuleCallParameter;
#else
_currn = (_TPPrRuleCallParameter) TreeNodeAlloc (sizeof (struct _TPrRuleCallParameter));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleCallParameter;
_currn->_desc1 = (_TSPRule_Expression) MkRule_Expression (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleCallParameter: root of subtree no. 1 can not be made a Rule_Expression node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleCallParameter;
return ( (NODEPTR) _currn);
}/* MkrRuleCallParameter */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRuleConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRuleConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRuleConstant _currn;
#ifdef __cplusplus
_currn = new _TPrRuleConstant;
#else
_currn = (_TPPrRuleConstant) TreeNodeAlloc (sizeof (struct _TPrRuleConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRuleConstant;
_currn->_desc1 = (_TSPConstant) MkConstant (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRuleConstant: root of subtree no. 1 can not be made a Constant node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRuleConstant;
return ( (NODEPTR) _currn);
}/* MkrRuleConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteUp (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteUp (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteUp _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteUp;
#else
_currn = (_TPPrRemoteUp) TreeNodeAlloc (sizeof (struct _TPrRemoteUp));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteUp;
_currn->_desc1 = (_TSPRemote_Including) MkRemote_Including (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteUp: root of subtree no. 1 can not be made a Remote_Including node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteUp;
return ( (NODEPTR) _currn);
}/* MkrRemoteUp */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteDown (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteDown (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteDown _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteDown;
#else
_currn = (_TPPrRemoteDown) TreeNodeAlloc (sizeof (struct _TPrRemoteDown));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteDown;
_currn->_desc1 = (_TSPRemote_Constituent) MkRemote_Constituent (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteDown: root of subtree no. 1 can not be made a Remote_Constituent node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteDown;
return ( (NODEPTR) _currn);
}/* MkrRemoteDown */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteIncluding (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteIncluding (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteIncluding _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteIncluding;
#else
_currn = (_TPPrRemoteIncluding) TreeNodeAlloc (sizeof (struct _TPrRemoteIncluding));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteIncluding;
_currn->_desc1 = (_TSPSymbol_AttributeReferences) MkSymbol_AttributeReferences (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteIncluding: root of subtree no. 1 can not be made a Symbol_AttributeReferences node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteIncluding;
return ( (NODEPTR) _currn);
}/* MkrRemoteIncluding */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolAttributeReferencesSingle (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolAttributeReferencesSingle (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolAttributeReferencesSingle _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolAttributeReferencesSingle;
#else
_currn = (_TPPrSymbolAttributeReferencesSingle) TreeNodeAlloc (sizeof (struct _TPrSymbolAttributeReferencesSingle));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolAttributeReferencesSingle;
_currn->_desc1 = (_TSPSymbolAttribute) MkSymbolAttribute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolAttributeReferencesSingle: root of subtree no. 1 can not be made a SymbolAttribute node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolAttributeReferencesSingle;
return ( (NODEPTR) _currn);
}/* MkrSymbolAttributeReferencesSingle */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolAttributeReferencesMultiple (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolAttributeReferencesMultiple (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolAttributeReferencesMultiple _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolAttributeReferencesMultiple;
#else
_currn = (_TPPrSymbolAttributeReferencesMultiple) TreeNodeAlloc (sizeof (struct _TPrSymbolAttributeReferencesMultiple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolAttributeReferencesMultiple;
_currn->_desc1 = (_TSPSymbol_AttributeReferenceList) MkSymbol_AttributeReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolAttributeReferencesMultiple: root of subtree no. 1 can not be made a Symbol_AttributeReferenceList node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbolAttribute) MkSymbolAttribute (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolAttributeReferencesMultiple: root of subtree no. 2 can not be made a SymbolAttribute node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolAttributeReferencesMultiple;
return ( (NODEPTR) _currn);
}/* MkrSymbolAttributeReferencesMultiple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolAttributeReferenceList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolAttributeReferenceList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolAttributeReferenceList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolAttributeReferenceList2;
#else
_currn = (_TPPrSymbolAttributeReferenceList2) TreeNodeAlloc (sizeof (struct _TPrSymbolAttributeReferenceList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolAttributeReferenceList2;
_currn->_desc1 = (_TSPSymbol_AttributeReferenceList) MkSymbol_AttributeReferenceList (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolAttributeReferenceList2: root of subtree no. 1 can not be made a Symbol_AttributeReferenceList node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbolAttribute) MkSymbolAttribute (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolAttributeReferenceList2: root of subtree no. 2 can not be made a SymbolAttribute node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolAttributeReferenceList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolAttributeReferenceList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolAttributeReferenceList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolAttributeReferenceList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolAttributeReferenceList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolAttributeReferenceList1;
#else
_currn = (_TPPrSymbolAttributeReferenceList1) TreeNodeAlloc (sizeof (struct _TPrSymbolAttributeReferenceList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolAttributeReferenceList1;
_currn->_desc1 = (_TSPSymbolAttribute) MkSymbolAttribute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolAttributeReferenceList1: root of subtree no. 1 can not be made a SymbolAttribute node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolAttributeReferenceList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolAttributeReferenceList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolAttributeRef (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolAttributeRef (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolAttributeRef _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolAttributeRef;
#else
_currn = (_TPPrSymbolAttributeRef) TreeNodeAlloc (sizeof (struct _TPrSymbolAttributeRef));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolAttributeRef;
_currn->_desc1 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolAttributeRef: root of subtree no. 1 can not be made a Symbol_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_AttributeReference) MkSymbol_AttributeReference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolAttributeRef: root of subtree no. 2 can not be made a Symbol_AttributeReference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolAttributeRef;
return ( (NODEPTR) _currn);
}/* MkrSymbolAttributeRef */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteConstituent (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRemoteConstituent (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRemoteConstituent _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteConstituent;
#else
_currn = (_TPPrRemoteConstituent) TreeNodeAlloc (sizeof (struct _TPrRemoteConstituent));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteConstituent;
_currn->_desc1 = (_TSPOpt_SymbolReference) MkOpt_SymbolReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteConstituent: root of subtree no. 1 can not be made a Opt_SymbolReference node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_AttributeReferences) MkSymbol_AttributeReferences (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteConstituent: root of subtree no. 2 can not be made a Symbol_AttributeReferences node ", 0, _coordref);
_currn->_desc3 = (_TSPRemote_Options) MkRemote_Options (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteConstituent: root of subtree no. 3 can not be made a Remote_Options node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRemoteConstituent;
return ( (NODEPTR) _currn);
}/* MkrRemoteConstituent */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrOptSymbolIsSymbol (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrOptSymbolIsSymbol (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrOptSymbolIsSymbol _currn;
#ifdef __cplusplus
_currn = new _TPrOptSymbolIsSymbol;
#else
_currn = (_TPPrOptSymbolIsSymbol) TreeNodeAlloc (sizeof (struct _TPrOptSymbolIsSymbol));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErOptSymbolIsSymbol;
_currn->_desc1 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rOptSymbolIsSymbol: root of subtree no. 1 can not be made a Symbol_Reference node ", 0, _coordref);
_currn->_desc2 = (_TSPOpt_SymbolIndex) MkOpt_SymbolIndex (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rOptSymbolIsSymbol: root of subtree no. 2 can not be made a Opt_SymbolIndex node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rOptSymbolIsSymbol;
return ( (NODEPTR) _currn);
}/* MkrOptSymbolIsSymbol */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoSymbolReference (POSITION *_coordref)
#else
NODEPTR MkrNoSymbolReference (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoSymbolReference _currn;
#ifdef __cplusplus
_currn = new _TPrNoSymbolReference;
#else
_currn = (_TPPrNoSymbolReference) TreeNodeAlloc (sizeof (struct _TPrNoSymbolReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoSymbolReference;
_SETCOORD(_currn)
_TERMACT_rNoSymbolReference;
return ( (NODEPTR) _currn);
}/* MkrNoSymbolReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteOptionsWith (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRemoteOptionsWith (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRemoteOptionsWith _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteOptionsWith;
#else
_currn = (_TPPrRemoteOptionsWith) TreeNodeAlloc (sizeof (struct _TPrRemoteOptionsWith));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteOptionsWith;
_currn->_desc1 = (_TSPOpt_Remote_Shield) MkOpt_Remote_Shield (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteOptionsWith: root of subtree no. 1 can not be made a Opt_Remote_Shield node ", 0, _coordref);
_currn->_desc2 = (_TSPRemote_With) MkRemote_With (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteOptionsWith: root of subtree no. 2 can not be made a Remote_With node ", 0, _coordref);
_currn->_desc3 = (_TSPOpt_Remote_Shield) MkOpt_Remote_Shield (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteOptionsWith: root of subtree no. 3 can not be made a Opt_Remote_Shield node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRemoteOptionsWith;
return ( (NODEPTR) _currn);
}/* MkrRemoteOptionsWith */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoRemoteOptions (POSITION *_coordref)
#else
NODEPTR MkrNoRemoteOptions (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoRemoteOptions _currn;
#ifdef __cplusplus
_currn = new _TPrNoRemoteOptions;
#else
_currn = (_TPPrNoRemoteOptions) TreeNodeAlloc (sizeof (struct _TPrNoRemoteOptions));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoRemoteOptions;
_SETCOORD(_currn)
_TERMACT_rNoRemoteOptions;
return ( (NODEPTR) _currn);
}/* MkrNoRemoteOptions */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteOptionsShield (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteOptionsShield (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteOptionsShield _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteOptionsShield;
#else
_currn = (_TPPrRemoteOptionsShield) TreeNodeAlloc (sizeof (struct _TPrRemoteOptionsShield));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteOptionsShield;
_currn->_desc1 = (_TSPRemote_Shield) MkRemote_Shield (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteOptionsShield: root of subtree no. 1 can not be made a Remote_Shield node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteOptionsShield;
return ( (NODEPTR) _currn);
}/* MkrRemoteOptionsShield */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrNoRemoteShield (POSITION *_coordref)
#else
NODEPTR MkrNoRemoteShield (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrNoRemoteShield _currn;
#ifdef __cplusplus
_currn = new _TPrNoRemoteShield;
#else
_currn = (_TPPrNoRemoteShield) TreeNodeAlloc (sizeof (struct _TPrNoRemoteShield));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErNoRemoteShield;
_SETCOORD(_currn)
_TERMACT_rNoRemoteShield;
return ( (NODEPTR) _currn);
}/* MkrNoRemoteShield */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrOptRemoteShield (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrOptRemoteShield (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrOptRemoteShield _currn;
#ifdef __cplusplus
_currn = new _TPrOptRemoteShield;
#else
_currn = (_TPPrOptRemoteShield) TreeNodeAlloc (sizeof (struct _TPrOptRemoteShield));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErOptRemoteShield;
_currn->_desc1 = (_TSPRemote_Shield) MkRemote_Shield (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rOptRemoteShield: root of subtree no. 1 can not be made a Remote_Shield node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rOptRemoteShield;
return ( (NODEPTR) _currn);
}/* MkrOptRemoteShield */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteShieldMultiple (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrRemoteShieldMultiple (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrRemoteShieldMultiple _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteShieldMultiple;
#else
_currn = (_TPPrRemoteShieldMultiple) TreeNodeAlloc (sizeof (struct _TPrRemoteShieldMultiple));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteShieldMultiple;
_currn->_desc1 = (_TSPSymbol_References) MkSymbol_References (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteShieldMultiple: root of subtree no. 1 can not be made a Symbol_References node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteShieldMultiple: root of subtree no. 2 can not be made a Symbol_Reference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRemoteShieldMultiple;
return ( (NODEPTR) _currn);
}/* MkrRemoteShieldMultiple */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteShieldSingle (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteShieldSingle (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteShieldSingle _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteShieldSingle;
#else
_currn = (_TPPrRemoteShieldSingle) TreeNodeAlloc (sizeof (struct _TPrRemoteShieldSingle));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteShieldSingle;
_currn->_desc1 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteShieldSingle: root of subtree no. 1 can not be made a Symbol_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteShieldSingle;
return ( (NODEPTR) _currn);
}/* MkrRemoteShieldSingle */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteUnshieldSelf (POSITION *_coordref)
#else
NODEPTR MkrRemoteUnshieldSelf (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteUnshieldSelf _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteUnshieldSelf;
#else
_currn = (_TPPrRemoteUnshieldSelf) TreeNodeAlloc (sizeof (struct _TPrRemoteUnshieldSelf));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteUnshieldSelf;
_SETCOORD(_currn)
_TERMACT_rRemoteUnshieldSelf;
return ( (NODEPTR) _currn);
}/* MkrRemoteUnshieldSelf */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolReferenceList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolReferenceList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolReferenceList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolReferenceList2;
#else
_currn = (_TPPrSymbolReferenceList2) TreeNodeAlloc (sizeof (struct _TPrSymbolReferenceList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolReferenceList2;
_currn->_desc1 = (_TSPSymbol_References) MkSymbol_References (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolReferenceList2: root of subtree no. 1 can not be made a Symbol_References node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolReferenceList2: root of subtree no. 2 can not be made a Symbol_Reference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolReferenceList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolReferenceList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolReferenceList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolReferenceList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolReferenceList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolReferenceList1;
#else
_currn = (_TPPrSymbolReferenceList1) TreeNodeAlloc (sizeof (struct _TPrSymbolReferenceList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolReferenceList1;
_currn->_desc1 = (_TSPSymbol_Reference) MkSymbol_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolReferenceList1: root of subtree no. 1 can not be made a Symbol_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolReferenceList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolReferenceList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendCall (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteAppendCall (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteAppendCall _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendCall;
#else
_currn = (_TPPrRemoteAppendCall) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendCall));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendCall;
_currn->_desc1 = (_TSPCallable_Reference) MkCallable_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteAppendCall: root of subtree no. 1 can not be made a Callable_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteAppendCall;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendCall */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendLambda (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteAppendLambda (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteAppendLambda _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendLambda;
#else
_currn = (_TPPrRemoteAppendLambda) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendLambda));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendLambda;
_currn->_desc1 = (_TSPRule_Lambda) MkRule_Lambda (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteAppendLambda: root of subtree no. 1 can not be made a Rule_Lambda node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteAppendLambda;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendLambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendIsBinary (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteAppendIsBinary (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteAppendIsBinary _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendIsBinary;
#else
_currn = (_TPPrRemoteAppendIsBinary) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendIsBinary));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendIsBinary;
_currn->_desc1 = (_TSPRemote_AppendBinary) MkRemote_AppendBinary (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteAppendIsBinary: root of subtree no. 1 can not be made a Remote_AppendBinary node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteAppendIsBinary;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendIsBinary */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendADD (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendADD (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendADD _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendADD;
#else
_currn = (_TPPrRemoteAppendADD) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendADD));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendADD;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendADD;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendADD */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendSUB (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendSUB (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendSUB _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendSUB;
#else
_currn = (_TPPrRemoteAppendSUB) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendSUB));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendSUB;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendSUB;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendSUB */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendDIV (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendDIV (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendDIV _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendDIV;
#else
_currn = (_TPPrRemoteAppendDIV) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendDIV));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendDIV;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendDIV;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendDIV */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendMUL (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendMUL (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendMUL _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendMUL;
#else
_currn = (_TPPrRemoteAppendMUL) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendMUL));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendMUL;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendMUL;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendMUL */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendConcat (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendConcat (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendConcat _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendConcat;
#else
_currn = (_TPPrRemoteAppendConcat) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendConcat));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendConcat;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendConcat;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendConcat */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendEQ (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendEQ (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendEQ _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendEQ;
#else
_currn = (_TPPrRemoteAppendEQ) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendEQ));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendEQ;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendEQ;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendEQ */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendNE (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendNE (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendNE _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendNE;
#else
_currn = (_TPPrRemoteAppendNE) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendNE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendNE;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendNE;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendNE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendLT (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendLT (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendLT _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendLT;
#else
_currn = (_TPPrRemoteAppendLT) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendLT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendLT;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendLT;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendLT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendGT (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendGT (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendGT _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendGT;
#else
_currn = (_TPPrRemoteAppendGT) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendGT));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendGT;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendGT;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendGT */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendLE (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendLE (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendLE _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendLE;
#else
_currn = (_TPPrRemoteAppendLE) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendLE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendLE;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendLE;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendLE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendGE (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendGE (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendGE _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendGE;
#else
_currn = (_TPPrRemoteAppendGE) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendGE));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendGE;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendGE;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendGE */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendOR (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendOR (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendOR _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendOR;
#else
_currn = (_TPPrRemoteAppendOR) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendOR));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendOR;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendOR;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendOR */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendAND (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendAND (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendAND _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendAND;
#else
_currn = (_TPPrRemoteAppendAND) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendAND));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendAND;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendAND;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendAND */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteAppendMOD (POSITION *_coordref)
#else
NODEPTR MkrRemoteAppendMOD (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteAppendMOD _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteAppendMOD;
#else
_currn = (_TPPrRemoteAppendMOD) TreeNodeAlloc (sizeof (struct _TPrRemoteAppendMOD));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteAppendMOD;
_SETCOORD(_currn)
_TERMACT_rRemoteAppendMOD;
return ( (NODEPTR) _currn);
}/* MkrRemoteAppendMOD */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteSingleCall (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteSingleCall (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteSingleCall _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteSingleCall;
#else
_currn = (_TPPrRemoteSingleCall) TreeNodeAlloc (sizeof (struct _TPrRemoteSingleCall));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteSingleCall;
_currn->_desc1 = (_TSPCallable_Reference) MkCallable_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteSingleCall: root of subtree no. 1 can not be made a Callable_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteSingleCall;
return ( (NODEPTR) _currn);
}/* MkrRemoteSingleCall */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteSingleInfer (POSITION *_coordref)
#else
NODEPTR MkrRemoteSingleInfer (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteSingleInfer _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteSingleInfer;
#else
_currn = (_TPPrRemoteSingleInfer) TreeNodeAlloc (sizeof (struct _TPrRemoteSingleInfer));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteSingleInfer;
_SETCOORD(_currn)
_TERMACT_rRemoteSingleInfer;
return ( (NODEPTR) _currn);
}/* MkrRemoteSingleInfer */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteSingleCreateList (POSITION *_coordref)
#else
NODEPTR MkrRemoteSingleCreateList (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteSingleCreateList _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteSingleCreateList;
#else
_currn = (_TPPrRemoteSingleCreateList) TreeNodeAlloc (sizeof (struct _TPrRemoteSingleCreateList));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteSingleCreateList;
_SETCOORD(_currn)
_TERMACT_rRemoteSingleCreateList;
return ( (NODEPTR) _currn);
}/* MkrRemoteSingleCreateList */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteSingleLambda (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteSingleLambda (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteSingleLambda _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteSingleLambda;
#else
_currn = (_TPPrRemoteSingleLambda) TreeNodeAlloc (sizeof (struct _TPrRemoteSingleLambda));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteSingleLambda;
_currn->_desc1 = (_TSPRule_Lambda) MkRule_Lambda (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteSingleLambda: root of subtree no. 1 can not be made a Rule_Lambda node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteSingleLambda;
return ( (NODEPTR) _currn);
}/* MkrRemoteSingleLambda */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteEmptyCall (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteEmptyCall (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteEmptyCall _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteEmptyCall;
#else
_currn = (_TPPrRemoteEmptyCall) TreeNodeAlloc (sizeof (struct _TPrRemoteEmptyCall));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteEmptyCall;
_currn->_desc1 = (_TSPCallable_Reference) MkCallable_Reference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteEmptyCall: root of subtree no. 1 can not be made a Callable_Reference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteEmptyCall;
return ( (NODEPTR) _currn);
}/* MkrRemoteEmptyCall */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteEmptyConstant (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrRemoteEmptyConstant (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrRemoteEmptyConstant _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteEmptyConstant;
#else
_currn = (_TPPrRemoteEmptyConstant) TreeNodeAlloc (sizeof (struct _TPrRemoteEmptyConstant));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteEmptyConstant;
_currn->_desc1 = (_TSPConstant) MkConstant (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteEmptyConstant: root of subtree no. 1 can not be made a Constant node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rRemoteEmptyConstant;
return ( (NODEPTR) _currn);
}/* MkrRemoteEmptyConstant */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteEmptyInfer (POSITION *_coordref)
#else
NODEPTR MkrRemoteEmptyInfer (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteEmptyInfer _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteEmptyInfer;
#else
_currn = (_TPPrRemoteEmptyInfer) TreeNodeAlloc (sizeof (struct _TPrRemoteEmptyInfer));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteEmptyInfer;
_SETCOORD(_currn)
_TERMACT_rRemoteEmptyInfer;
return ( (NODEPTR) _currn);
}/* MkrRemoteEmptyInfer */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteWithInfer (POSITION *_coordref)
#else
NODEPTR MkrRemoteWithInfer (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrRemoteWithInfer _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteWithInfer;
#else
_currn = (_TPPrRemoteWithInfer) TreeNodeAlloc (sizeof (struct _TPrRemoteWithInfer));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteWithInfer;
_SETCOORD(_currn)
_TERMACT_rRemoteWithInfer;
return ( (NODEPTR) _currn);
}/* MkrRemoteWithInfer */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteWithLido (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3, NODEPTR _desc4)
#else
NODEPTR MkrRemoteWithLido (_coordref,_desc1,_desc2,_desc3,_desc4)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
	NODEPTR _desc4;
#endif
{	_TPPrRemoteWithLido _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteWithLido;
#else
_currn = (_TPPrRemoteWithLido) TreeNodeAlloc (sizeof (struct _TPrRemoteWithLido));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteWithLido;
_currn->_desc1 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteWithLido: root of subtree no. 1 can not be made a Simple_Typing node ", 0, _coordref);
_currn->_desc2 = (_TSPRemote_Append) MkRemote_Append (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteWithLido: root of subtree no. 2 can not be made a Remote_Append node ", 0, _coordref);
_currn->_desc3 = (_TSPRemote_Single) MkRemote_Single (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteWithLido: root of subtree no. 3 can not be made a Remote_Single node ", 0, _coordref);
_currn->_desc4 = (_TSPRemote_Empty) MkRemote_Empty (_coordref, _desc4);	
if (((NODEPTR)_currn->_desc4) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteWithLido: root of subtree no. 4 can not be made a Remote_Empty node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRemoteWithLido;
return ( (NODEPTR) _currn);
}/* MkrRemoteWithLido */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrRemoteWithInferTypes (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrRemoteWithInferTypes (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrRemoteWithInferTypes _currn;
#ifdef __cplusplus
_currn = new _TPrRemoteWithInferTypes;
#else
_currn = (_TPPrRemoteWithInferTypes) TreeNodeAlloc (sizeof (struct _TPrRemoteWithInferTypes));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErRemoteWithInferTypes;
_currn->_desc1 = (_TSPRemote_Append) MkRemote_Append (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteWithInferTypes: root of subtree no. 1 can not be made a Remote_Append node ", 0, _coordref);
_currn->_desc2 = (_TSPRemote_Single) MkRemote_Single (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteWithInferTypes: root of subtree no. 2 can not be made a Remote_Single node ", 0, _coordref);
_currn->_desc3 = (_TSPRemote_Empty) MkRemote_Empty (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rRemoteWithInferTypes: root of subtree no. 3 can not be made a Remote_Empty node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rRemoteWithInferTypes;
return ( (NODEPTR) _currn);
}/* MkrRemoteWithInferTypes */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolOptionUsing (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolOptionUsing (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolOptionUsing _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolOptionUsing;
#else
_currn = (_TPPrSymbolOptionUsing) TreeNodeAlloc (sizeof (struct _TPrSymbolOptionUsing));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolOptionUsing;
_currn->_desc1 = (_TSPSymbol_UsingAttribute) MkSymbol_UsingAttribute (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolOptionUsing: root of subtree no. 1 can not be made a Symbol_UsingAttribute node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolOptionUsing;
return ( (NODEPTR) _currn);
}/* MkrSymbolOptionUsing */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolUsingAttribute (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolUsingAttribute (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolUsingAttribute _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolUsingAttribute;
#else
_currn = (_TPPrSymbolUsingAttribute) TreeNodeAlloc (sizeof (struct _TPrSymbolUsingAttribute));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolUsingAttribute;
_currn->_desc1 = (_TSPSymbol_UsingDecls) MkSymbol_UsingDecls (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingAttribute: root of subtree no. 1 can not be made a Symbol_UsingDecls node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolUsingAttribute;
return ( (NODEPTR) _currn);
}/* MkrSymbolUsingAttribute */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolUsingDeclList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolUsingDeclList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolUsingDeclList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolUsingDeclList2;
#else
_currn = (_TPPrSymbolUsingDeclList2) TreeNodeAlloc (sizeof (struct _TPrSymbolUsingDeclList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolUsingDeclList2;
_currn->_desc1 = (_TSPSymbol_UsingDecls) MkSymbol_UsingDecls (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclList2: root of subtree no. 1 can not be made a Symbol_UsingDecls node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_UsingDecl) MkSymbol_UsingDecl (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclList2: root of subtree no. 2 can not be made a Symbol_UsingDecl node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolUsingDeclList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolUsingDeclList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolUsingDeclList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolUsingDeclList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolUsingDeclList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolUsingDeclList1;
#else
_currn = (_TPPrSymbolUsingDeclList1) TreeNodeAlloc (sizeof (struct _TPrSymbolUsingDeclList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolUsingDeclList1;
_currn->_desc1 = (_TSPSymbol_UsingDecl) MkSymbol_UsingDecl (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclList1: root of subtree no. 1 can not be made a Symbol_UsingDecl node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolUsingDeclList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolUsingDeclList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolUsingDecl (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2, NODEPTR _desc3)
#else
NODEPTR MkrSymbolUsingDecl (_coordref,_desc1,_desc2,_desc3)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
	NODEPTR _desc3;
#endif
{	_TPPrSymbolUsingDecl _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolUsingDecl;
#else
_currn = (_TPPrSymbolUsingDecl) TreeNodeAlloc (sizeof (struct _TPrSymbolUsingDecl));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolUsingDecl;
_currn->_desc1 = (_TSPAttributeClass) MkAttributeClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDecl: root of subtree no. 1 can not be made a AttributeClass node ", 0, _coordref);
_currn->_desc2 = (_TSPLocal_UsingReferences) MkLocal_UsingReferences (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDecl: root of subtree no. 2 can not be made a Local_UsingReferences node ", 0, _coordref);
_currn->_desc3 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc3);	
if (((NODEPTR)_currn->_desc3) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDecl: root of subtree no. 3 can not be made a Simple_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolUsingDecl;
return ( (NODEPTR) _currn);
}/* MkrSymbolUsingDecl */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolUsingDeclWithoutClass (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolUsingDeclWithoutClass (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolUsingDeclWithoutClass _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolUsingDeclWithoutClass;
#else
_currn = (_TPPrSymbolUsingDeclWithoutClass) TreeNodeAlloc (sizeof (struct _TPrSymbolUsingDeclWithoutClass));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolUsingDeclWithoutClass;
_currn->_desc1 = (_TSPAttributeClass) MkAttributeClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclWithoutClass: root of subtree no. 1 can not be made a AttributeClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_UsingDeclsNoClass) MkSymbol_UsingDeclsNoClass (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclWithoutClass: root of subtree no. 2 can not be made a Symbol_UsingDeclsNoClass node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolUsingDeclWithoutClass;
return ( (NODEPTR) _currn);
}/* MkrSymbolUsingDeclWithoutClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolUsingDeclsNoClassList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolUsingDeclsNoClassList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolUsingDeclsNoClassList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolUsingDeclsNoClassList2;
#else
_currn = (_TPPrSymbolUsingDeclsNoClassList2) TreeNodeAlloc (sizeof (struct _TPrSymbolUsingDeclsNoClassList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolUsingDeclsNoClassList2;
_currn->_desc1 = (_TSPSymbol_UsingDeclsNoClass) MkSymbol_UsingDeclsNoClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclsNoClassList2: root of subtree no. 1 can not be made a Symbol_UsingDeclsNoClass node ", 0, _coordref);
_currn->_desc2 = (_TSPSymbol_UsingDeclNoClass) MkSymbol_UsingDeclNoClass (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclsNoClassList2: root of subtree no. 2 can not be made a Symbol_UsingDeclNoClass node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolUsingDeclsNoClassList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolUsingDeclsNoClassList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolUsingDeclsNoClassList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolUsingDeclsNoClassList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolUsingDeclsNoClassList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolUsingDeclsNoClassList1;
#else
_currn = (_TPPrSymbolUsingDeclsNoClassList1) TreeNodeAlloc (sizeof (struct _TPrSymbolUsingDeclsNoClassList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolUsingDeclsNoClassList1;
_currn->_desc1 = (_TSPSymbol_UsingDeclNoClass) MkSymbol_UsingDeclNoClass (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclsNoClassList1: root of subtree no. 1 can not be made a Symbol_UsingDeclNoClass node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolUsingDeclsNoClassList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolUsingDeclsNoClassList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolUsingDeclNoClass (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolUsingDeclNoClass (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolUsingDeclNoClass _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolUsingDeclNoClass;
#else
_currn = (_TPPrSymbolUsingDeclNoClass) TreeNodeAlloc (sizeof (struct _TPrSymbolUsingDeclNoClass));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolUsingDeclNoClass;
_currn->_desc1 = (_TSPLocal_UsingReference) MkLocal_UsingReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclNoClass: root of subtree no. 1 can not be made a Local_UsingReference node ", 0, _coordref);
_currn->_desc2 = (_TSPSimple_Typing) MkSimple_Typing (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolUsingDeclNoClass: root of subtree no. 2 can not be made a Simple_Typing node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolUsingDeclNoClass;
return ( (NODEPTR) _currn);
}/* MkrSymbolUsingDeclNoClass */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeReferenceList2 (POSITION *_coordref, NODEPTR _desc1, NODEPTR _desc2)
#else
NODEPTR MkrSymbolLocalAttributeReferenceList2 (_coordref,_desc1,_desc2)
	POSITION *_coordref;
	NODEPTR _desc1;
	NODEPTR _desc2;
#endif
{	_TPPrSymbolLocalAttributeReferenceList2 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeReferenceList2;
#else
_currn = (_TPPrSymbolLocalAttributeReferenceList2) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeReferenceList2));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeReferenceList2;
_currn->_desc1 = (_TSPLocal_UsingReferences) MkLocal_UsingReferences (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeReferenceList2: root of subtree no. 1 can not be made a Local_UsingReferences node ", 0, _coordref);
_currn->_desc2 = (_TSPLocal_UsingReference) MkLocal_UsingReference (_coordref, _desc2);	
if (((NODEPTR)_currn->_desc2) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeReferenceList2: root of subtree no. 2 can not be made a Local_UsingReference node ", 0, _coordref);
_SETCOORD(_currn)
_TERMACT_rSymbolLocalAttributeReferenceList2;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeReferenceList2 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolLocalAttributeReferenceList1 (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrSymbolLocalAttributeReferenceList1 (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrSymbolLocalAttributeReferenceList1 _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolLocalAttributeReferenceList1;
#else
_currn = (_TPPrSymbolLocalAttributeReferenceList1) TreeNodeAlloc (sizeof (struct _TPrSymbolLocalAttributeReferenceList1));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolLocalAttributeReferenceList1;
_currn->_desc1 = (_TSPLocal_UsingReference) MkLocal_UsingReference (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rSymbolLocalAttributeReferenceList1: root of subtree no. 1 can not be made a Local_UsingReference node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rSymbolLocalAttributeReferenceList1;
return ( (NODEPTR) _currn);
}/* MkrSymbolLocalAttributeReferenceList1 */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrLocalUsingReference (POSITION *_coordref, NODEPTR _desc1)
#else
NODEPTR MkrLocalUsingReference (_coordref,_desc1)
	POSITION *_coordref;
	NODEPTR _desc1;
#endif
{	_TPPrLocalUsingReference _currn;
#ifdef __cplusplus
_currn = new _TPrLocalUsingReference;
#else
_currn = (_TPPrLocalUsingReference) TreeNodeAlloc (sizeof (struct _TPrLocalUsingReference));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErLocalUsingReference;
_currn->_desc1 = (_TSPIdentifier) MkIdentifier (_coordref, _desc1);	
if (((NODEPTR)_currn->_desc1) == NULLNODEPTR)	
	message (DEADLY, "RULE rLocalUsingReference: root of subtree no. 1 can not be made a Identifier node ", 0, _coordref);
_COPYCOORD(_currn)
_TERMACT_rLocalUsingReference;
return ( (NODEPTR) _currn);
}/* MkrLocalUsingReference */

#if defined(__STDC__) || defined(__cplusplus)
NODEPTR MkrSymbolAttrReferenceIsTree (POSITION *_coordref)
#else
NODEPTR MkrSymbolAttrReferenceIsTree (_coordref)
	POSITION *_coordref;
#endif
{	_TPPrSymbolAttrReferenceIsTree _currn;
#ifdef __cplusplus
_currn = new _TPrSymbolAttrReferenceIsTree;
#else
_currn = (_TPPrSymbolAttrReferenceIsTree) TreeNodeAlloc (sizeof (struct _TPrSymbolAttrReferenceIsTree));
#endif
#ifdef MONITOR
_currn->_uid=MONTblStackSize; MONTblStackPush(((NODEPTR)_currn));
#endif
_currn->_prod = RULErSymbolAttrReferenceIsTree;
_SETCOORD(_currn)
_TERMACT_rSymbolAttrReferenceIsTree;
return ( (NODEPTR) _currn);
}/* MkrSymbolAttrReferenceIsTree */
