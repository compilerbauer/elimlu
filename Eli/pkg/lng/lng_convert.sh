#!/bin/sh
# 
# EXEC (lng_convert.sh) (:lng_combine) 
#   => (:lng_converted);
# :lng_converted 'LNG importing file' => :FILE; 

ODIN_input=$1; shift;

if test -s lng_converted 
then 
  rm lng_converted;
else :
fi 

touch lng_converted

if test -s $ODIN_input
then 
  prefix="load \""
  postfix="\""
  file=$ODIN_input
  while read -r line
  do
  echo "${prefix}$line${postfix}" 
  done <$file > lng_converted
else :
  
fi 
