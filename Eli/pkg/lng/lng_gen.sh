#!/bin/sh
# $Id: clp_gen.sh,v 2.14 2010/03/03 22:58:07 profw Exp $
# Copyright, 1990, The University of Colorado
# Copyright, 1995-1999, Anthony M. Sloane

# This file is part of the Eli translator construction system.

# Eli is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2, or (at your option) any later
# version.

# Eli is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License along
# with Eli; see the file COPYING.  If not, write to the Free Software
# Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
# 
# EXEC (lng_gen.sh) (lng.exe) (:lng_converted) 
#   => (:lng_gen); 
# 
# :lng_gen 'Objects generated from LNG specifications' => :DERIVED-DIRECTORY;

ODIN_exe=$1;shift; ODIN_cmpd=$1;shift; ODIN_cppcmd=$1;shift;

rm -rf lng_gen; 
mkdir -p lng_gen;
# cd lng_gen;

# run the lng compiler
if test -s $ODIN_cmpd
then
  echo "$ODIN_exe -o lng_gen $ODIN_cmpd"
  $ODIN_exe --force $ODIN_cmpd
else :
  rm $ODIN_cmpd
  exit 0 
fi 

# if test -s lng_gen/lng_gen.lido
# then :
# else
#   rm lng_gen/lng_gen.lido
# fi

# generated components
mv lng_gen.* lng_gen/
mv lng_ast.lido lng_gen/

touch $ODIN_cmpd;

