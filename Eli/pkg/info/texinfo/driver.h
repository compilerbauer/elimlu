
#if defined(__cplusplus) || defined(__STDC__)
extern void opsys_abort(char *s, int i);
#else
extern void opsys_abort();
#endif
