/* Counting operations */

#include <stdio.h>

static int count = 0;		/* Initially no words have been seen */

void
#if defined(__cplusplus)
CountWord(char *, int, int *, int *)
#else
CountWord()
#endif
/* Increment the counter when a word is found */
{ count++; }

void
CountFinl()
/* Counter finalization: Print the total */
{ (void)printf("%d words\n", count); }
